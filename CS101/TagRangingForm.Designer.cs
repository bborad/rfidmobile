namespace OnRamp
{
    partial class TagRangingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader EPCColHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRangingForm));
            System.Windows.Forms.ColumnHeader CntColHdr1;
            System.Windows.Forms.ColumnHeader RSSIColHdr;
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.EPCListV = new System.Windows.Forms.ListView();
            this.RowIdColHdr = new System.Windows.Forms.ColumnHeader();
            this.Asset = new System.Windows.Forms.ColumnHeader();
            this.StartButton = new System.Windows.Forms.Button();
            this.ClrButton = new System.Windows.Forms.Button();
            this.TagCntLbl = new System.Windows.Forms.Label();
            this.TagObsrvCntLbl = new System.Windows.Forms.Label();
            this.AmbTempLbl = new System.Windows.Forms.Label();
            this.XcvrTempLbl = new System.Windows.Forms.Label();
            this.PATempLbl = new System.Windows.Forms.Label();
            this.DutyCycTmr = new System.Windows.Forms.Timer();
            this.LimitLbl = new System.Windows.Forms.Label();
            this.AliveTmr = new System.Windows.Forms.Timer();
            this.ListVUpdateTmr = new System.Windows.Forms.Timer();
            this.btnSearch = new System.Windows.Forms.Button();
            EPCColHdr = new System.Windows.Forms.ColumnHeader();
            CntColHdr1 = new System.Windows.Forms.ColumnHeader();
            RSSIColHdr = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // EPCColHdr
            // 
            resources.ApplyResources(EPCColHdr, "EPCColHdr");
            // 
            // CntColHdr1
            // 
            resources.ApplyResources(CntColHdr1, "CntColHdr1");
            // 
            // RSSIColHdr
            // 
            resources.ApplyResources(RSSIColHdr, "RSSIColHdr");
            // 
            // EPCListV
            // 
            this.EPCListV.Columns.Add(this.RowIdColHdr);
            this.EPCListV.Columns.Add(this.Asset);
            this.EPCListV.Columns.Add(RSSIColHdr);
            this.EPCListV.Columns.Add(EPCColHdr);
            this.EPCListV.Columns.Add(CntColHdr1);
            this.EPCListV.FullRowSelect = true;
            this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.EPCListV, "EPCListV");
            this.EPCListV.Name = "EPCListV";
            this.EPCListV.View = System.Windows.Forms.View.Details;
            // 
            // RowIdColHdr
            // 
            resources.ApplyResources(this.RowIdColHdr, "RowIdColHdr");
            // 
            // Asset
            // 
            resources.ApplyResources(this.Asset, "Asset");
            // 
            // StartButton
            // 
            resources.ApplyResources(this.StartButton, "StartButton");
            this.StartButton.Name = "StartButton";
            this.StartButton.Click += new System.EventHandler(this.OnStartButtonClicked);
            // 
            // ClrButton
            // 
            resources.ApplyResources(this.ClrButton, "ClrButton");
            this.ClrButton.Name = "ClrButton";
            this.ClrButton.Click += new System.EventHandler(this.OnClrButtonClicked);
            // 
            // TagCntLbl
            // 
            resources.ApplyResources(this.TagCntLbl, "TagCntLbl");
            this.TagCntLbl.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLbl.Name = "TagCntLbl";
            // 
            // TagObsrvCntLbl
            // 
            resources.ApplyResources(this.TagObsrvCntLbl, "TagObsrvCntLbl");
            this.TagObsrvCntLbl.Name = "TagObsrvCntLbl";
            // 
            // AmbTempLbl
            // 
            resources.ApplyResources(this.AmbTempLbl, "AmbTempLbl");
            this.AmbTempLbl.Name = "AmbTempLbl";
            // 
            // XcvrTempLbl
            // 
            resources.ApplyResources(this.XcvrTempLbl, "XcvrTempLbl");
            this.XcvrTempLbl.Name = "XcvrTempLbl";
            // 
            // PATempLbl
            // 
            resources.ApplyResources(this.PATempLbl, "PATempLbl");
            this.PATempLbl.Name = "PATempLbl";
            // 
            // DutyCycTmr
            // 
            this.DutyCycTmr.Tick += new System.EventHandler(this.DutyCycTmr_Tick);
            // 
            // LimitLbl
            // 
            resources.ApplyResources(this.LimitLbl, "LimitLbl");
            this.LimitLbl.ForeColor = System.Drawing.Color.SaddleBrown;
            this.LimitLbl.Name = "LimitLbl";
            // 
            // AliveTmr
            // 
            this.AliveTmr.Interval = 60000;
            this.AliveTmr.Tick += new System.EventHandler(this.OnAliveTmrTick);
            // 
            // ListVUpdateTmr
            // 
            this.ListVUpdateTmr.Interval = 1000;
            this.ListVUpdateTmr.Tick += new System.EventHandler(this.OnListViewUpdateTmrTick);
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // TagRangingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.ClrButton);
            this.Controls.Add(this.LimitLbl);
            this.Controls.Add(this.PATempLbl);
            this.Controls.Add(this.XcvrTempLbl);
            this.Controls.Add(this.AmbTempLbl);
            this.Controls.Add(this.TagCntLbl);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.EPCListV);
            this.Controls.Add(this.TagObsrvCntLbl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagRangingForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView EPCListV;
        private System.Windows.Forms.ColumnHeader RowIdColHdr;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button ClrButton;
        private System.Windows.Forms.Label TagCntLbl;
        private System.Windows.Forms.Label TagObsrvCntLbl;
        private System.Windows.Forms.Label AmbTempLbl;
        private System.Windows.Forms.Label XcvrTempLbl;
        private System.Windows.Forms.Label PATempLbl;
        private System.Windows.Forms.Timer DutyCycTmr;
        private System.Windows.Forms.Label LimitLbl;
        private System.Windows.Forms.Timer AliveTmr;
        private System.Windows.Forms.Timer ListVUpdateTmr;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ColumnHeader Asset;
    }
}