namespace OnRamp
{
    partial class frmInveventorySync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.lblStatus = new System.Windows.Forms.Label();
            this.syncProgress = new System.Windows.Forms.ProgressBar();
            this.btnStart = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(20, 20);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(269, 20);
            this.lblStatus.Text = "Status";
            // 
            // syncProgress
            // 
            this.syncProgress.Location = new System.Drawing.Point(20, 72);
            this.syncProgress.Name = "syncProgress";
            this.syncProgress.Size = new System.Drawing.Size(269, 26);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(99, 117);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(104, 26);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // frmInveventorySync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(318, 195);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.syncProgress);
            this.Controls.Add(this.lblStatus);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInveventorySync";
            this.Text = "Inveventory Synchronize";
            this.Load += new System.EventHandler(this.frmInveventorySync_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmInveventorySync_Closing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ProgressBar syncProgress;
        private System.Windows.Forms.Button btnStart;
    }
}