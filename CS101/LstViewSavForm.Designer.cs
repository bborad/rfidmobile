namespace OnRamp
{
    partial class LstViewSavForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LstViewSavForm));
            this.label1 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.LstV = new System.Windows.Forms.ListView();
            this.SavBttn = new System.Windows.Forms.Button();
            this.RowCntLbl = new System.Windows.Forms.Label();
            this.DelBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // LstV
            // 
            resources.ApplyResources(this.LstV, "LstV");
            this.LstV.FullRowSelect = true;
            this.LstV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LstV.Name = "LstV";
            this.LstV.View = System.Windows.Forms.View.Details;
            // 
            // SavBttn
            // 
            resources.ApplyResources(this.SavBttn, "SavBttn");
            this.SavBttn.Name = "SavBttn";
            this.SavBttn.Click += new System.EventHandler(this.OnSavBttnClicked);
            // 
            // RowCntLbl
            // 
            resources.ApplyResources(this.RowCntLbl, "RowCntLbl");
            this.RowCntLbl.Name = "RowCntLbl";
            // 
            // DelBttn
            // 
            resources.ApplyResources(this.DelBttn, "DelBttn");
            this.DelBttn.Name = "DelBttn";
            this.DelBttn.Click += new System.EventHandler(this.OnDelBttnClicked);
            // 
            // LstViewSavForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.DelBttn);
            this.Controls.Add(this.RowCntLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SavBttn);
            this.Controls.Add(this.LstV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LstViewSavForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView LstV;
        private System.Windows.Forms.Button SavBttn;
        private System.Windows.Forms.Label RowCntLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button DelBttn;
    }
}