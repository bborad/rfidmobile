/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Asset Search List Display functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;


namespace OnRamp
{
    public partial class frmSearchlist : Form
    {
        private bool _IsUploadFromFile = false;
        String tagNo = "";
        String assetName = "";

        public frmSearchlist()
        {
            InitializeComponent();
        }

        private DataTable _searchedData;

        public DataTable searchedData
        {
            set
            {
                _searchedData = value;
            }
        }

        public bool IsUploadFromFile
        {
            set
            {
                _IsUploadFromFile = value;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        private void frmSearchlist_Load(object sender, EventArgs e)
        {
            DataColumn dc = new DataColumn("IsSelected", Type.GetType("System.Int32"));
            dc.DefaultValue = 0;

            _searchedData.Columns.Add(dc);

            recPerPage = UserPref.GetInstance().PageSize;
            SetupPaging(_searchedData.Rows.Count);
            curPage = 1;
            PopulateListView();

            if (_IsUploadFromFile)
            {
                lstAsset.Columns[1].Width = 0;
                lstAsset.Columns[2].Width = 275;
                lstAsset.Columns[1].Text = "Tag ID";
            }


            //ControlBindingsCollection dbbindsource = new ControlBindingsCollection() 
            //dbbindsource.dat .DataSource = _searchedData;
            //lstAsset.CheckBoxes = true;
            //lstAsset.DataBindings   

        }

        private void PopulateListView()
        {
            Cursor.Current = Cursors.WaitCursor;
            lstAsset.Items.Clear();
            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            //DataRow dr=new DataRow();
            lstAsset.CheckBoxes = true;
            lstAsset.FullRowSelect = true;

            int fromRec = (curPage - 1) * recPerPage;

            int toRec = (curPage * recPerPage);

            if (toRec >= _searchedData.Rows.Count)
                toRec = _searchedData.Rows.Count;

            DataRow dr;

            for (int index = fromRec; index < toRec; index++)
            {
                dr = _searchedData.Rows[index];
                lstItem = new ListViewItem();
                //Convert.ToString(dr["TagID"]) + "," + Convert.ToString(dr["Name"])

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["Name"]);
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["TagID"]);
                lstItem.SubItems.Add(ls);

                if (Convert.ToInt32(dr["IsSelected"]) == 1)
                    lstItem.Checked = true;

                lstAsset.Items.Add(lstItem);
            }

            //foreach (DataRow dr in _searchedData.Rows)
            //{
            //    lstItem = new ListViewItem();
            //    //Convert.ToString(dr["TagID"]) + "," + Convert.ToString(dr["Name"])

            //    ls = new ListViewItem.ListViewSubItem();
            //    ls.Text = Convert.ToString(dr["Name"]);
            //    lstItem.SubItems.Add(ls);

            //    ls = new ListViewItem.ListViewSubItem();
            //    ls.Text = Convert.ToString(dr["TagID"]);
            //    lstItem.SubItems.Add(ls);

            //    lstAsset.Items.Add(lstItem);
            //}

            lstAsset.Refresh();
            Cursor.Current = Cursors.Default;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openSearchForm(false);
        }

        private void openSearchForm(bool isSelectAll)
        {
            Cursor.Current = Cursors.WaitCursor; 

            Int32 SelectedItem = 0;
            System.Text.StringBuilder csvData = new System.Text.StringBuilder("");
         
            csvData.Length = 0;

            DataTable dtSelected = new DataTable("dtSelected");

            dtSelected.Columns.Add(new DataColumn("TagID", Type.GetType("System.String")));
            dtSelected.Columns.Add(new DataColumn("Name", Type.GetType("System.String")));

            if (isSelectAll)
            {                
                DataRow dr;
                foreach (ListViewItem lv in lstAsset.Items)
                {
                    dr = dtSelected.NewRow();
                    SelectedItem++;
                    tagNo = lv.SubItems[2].Text;
                    assetName = lv.SubItems[1].Text;
                  //  csvData.Append(", " + lv.SubItems[2].Text);
                    dr["Name"] = lv.SubItems[1].Text;
                    dr["TagID"] = lv.SubItems[2].Text;
                    dtSelected.Rows.Add(dr);

                }

                dtSelected.AcceptChanges();
            }
            else
            {
                DataView dv = new DataView(_searchedData);
                dv.RowFilter = "IsSelected = 1";

                if (dv.Count > 0)
                {
                    string[] colNames = new string[2];
                    colNames[0] = "TagID";
                    colNames[1] = "Name";
                    dtSelected = dv.ToTable(true, colNames);
                }               
            }

            SelectedItem = dtSelected.Rows.Count;

            //if (isSelectAll)
            //{
            //    SelectedItem = _searchedData.Rows.Count;
            //}
            //else
            //{
            //    DataRow dr;
            //    foreach (ListViewItem lv in lstAsset.Items)
            //    {
            //        if (lv.Checked)
            //        {
            //            dr = dtSelected.NewRow();
            //            SelectedItem++;
            //            tagNo = lv.SubItems[2].Text;
            //            assetName = lv.SubItems[1].Text;
            //           // csvData.Append(", " + lv.SubItems[2].Text);
            //            dr["Name"] = lv.SubItems[1].Text;
            //            dr["TagID"] = lv.SubItems[2].Text;
            //            dtSelected.Rows.Add(dr);
            //        }
            //    }

            //    dtSelected.AcceptChanges();
            //}

            Cursor.Current = Cursors.Default; 

            if (SelectedItem > 1)
            {
                // Open new Tag Invtry window
                TagRangingForm TagRangingFm = new TagRangingForm();   
                //if (isSelectAll)
                //    TagRangingFm.dtSelectedItems = _searchedData;
                //else
                //    TagRangingFm.dtSelectedItems = dtSelected;
                TagRangingFm.dtSelectedItems = dtSelected;
                TagRangingFm.Show();
                // disable form until this new form closed
                this.Enabled = false;
                TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else if (SelectedItem == 1)
            {
                // Open new Tag Search Window
                TagSearchForm TagSrchFm = new TagSearchForm();
                tagNo = dtSelected.Rows[0]["TagID"].ToString();
                assetName = dtSelected.Rows[0]["Name"].ToString();
                TagSrchFm.SearchTagNo = tagNo.Trim();
                TagSrchFm.AssetName = assetName;
                TagSrchFm.Show();
                this.Enabled = false;
                TagSrchFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else
            {
                MessageBox.Show("Please Select any Asset.");
            }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnSearch_Click_1(object sender, EventArgs e)
        {
            openSearchForm(true);
        }

        /// <summary>
        /// Select all checkBox of grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            lstAsset.ItemCheck -= lstAsset_ItemCheck;
            int index = (curPage - 1) * recPerPage;
            DataRow dr;
            foreach (ListViewItem lv in lstAsset.Items)
            {
                dr = _searchedData.Rows[index];
                dr["IsSelected"] = 1;
                lv.Checked = true;
                index++;
            }
            _searchedData.AcceptChanges();
            lstAsset.ItemCheck += lstAsset_ItemCheck;
        }

        #region Paging Code

        int curPage, totalPages;
        public int recPerPage;

        DataTable dtPages;
        private int SetupPaging(int totalRec)
        {
            //int totalRec = EPCListV.Items.Count;
            int totalPages = 0;

            dtPages = new DataTable();
            DataColumn dc = new DataColumn("Page");
            dtPages.Columns.Add(dc);

            DataColumn dcc = new DataColumn("Index");
            dtPages.Columns.Add(dcc);

            if (totalRec > 0)
            {
                totalPages = totalRec / recPerPage;
                if (totalRec % recPerPage != 0)
                    totalPages = totalPages + 1;

                //pageArray_DispatchedItems = new ArrayList();
            }

            //cmbPage.Items.Clear();

            DataRow dr;

            //dr = dtPages.NewRow();
            //dr["Page"] = "Select";
            //dr["Index"] = 0;
            //dtPages.Rows.Add(dr);

            for (int i = 1; i <= totalPages; i++)
            {
                dr = dtPages.NewRow();
                dr["Page"] = i.ToString();
                dr["Index"] = i;
                dtPages.Rows.Add(dr);
                //pageArray_DispatchedItems.Add("-1");
            }

            dtPages.AcceptChanges();

            cmbPage.DataSource = dtPages;
            cmbPage.DisplayMember = "Page";
            cmbPage.ValueMember = "Index";

            if (totalPages > 0)
                cmbPage.SelectedValue = 1;

            cmbPage.Visible = true;
            lblPage.Visible = true;
            cmbPage.SelectedValueChanged += new EventHandler(cmbPage_SelectedValueChanged);

            return totalPages;
        }

        private void cmbPage_SelectedValueChanged(object sender, EventArgs e)
        {
            if (curPage == Convert.ToInt32(cmbPage.SelectedValue))
                return;
            curPage = Convert.ToInt32(cmbPage.SelectedValue);
            //setScanTags();
            PopulateListView();
        }

        #endregion


        private void lstAsset_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            DataRow dr;
            int index = (curPage - 1) * recPerPage;
            index = index + e.Index;

            dr = _searchedData.Rows[index];

            if (lstAsset.Items[e.Index].Checked == true)
                dr["IsSelected"] = 0;
            else
            {
                dr["IsSelected"] = 1;                
            }

            _searchedData.AcceptChanges();

        }

        private void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            lstAsset.ItemCheck -= lstAsset_ItemCheck;
            int index = (curPage - 1) * recPerPage;
            DataRow dr;
            foreach (ListViewItem lv in lstAsset.Items)
            {
                dr = _searchedData.Rows[index];
                dr["IsSelected"] = 0;
                lv.Checked = false;
                index++;
            }
            _searchedData.AcceptChanges();
            lstAsset.ItemCheck += lstAsset_ItemCheck;
        }

    }
}