namespace OnRamp
{
    partial class HttpFileTransferForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HttpFileTransferForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.XfrPrgBr = new System.Windows.Forms.ProgressBar();
            this.InfoLbl = new System.Windows.Forms.Label();
            this.StatusLbl = new System.Windows.Forms.Label();
            this.DetailsLbl = new System.Windows.Forms.Label();
            this.StartBttn = new System.Windows.Forms.Button();
            this.CredPromptTmr = new System.Windows.Forms.Timer();
            this.UploadXferStartTmr = new System.Windows.Forms.Timer();
            this.SuspendLayout();
            // 
            // XfrPrgBr
            // 
            resources.ApplyResources(this.XfrPrgBr, "XfrPrgBr");
            this.XfrPrgBr.Name = "XfrPrgBr";
            this.XfrPrgBr.Value = 50;
            // 
            // InfoLbl
            // 
            resources.ApplyResources(this.InfoLbl, "InfoLbl");
            this.InfoLbl.Name = "InfoLbl";
            // 
            // StatusLbl
            // 
            resources.ApplyResources(this.StatusLbl, "StatusLbl");
            this.StatusLbl.Name = "StatusLbl";
            // 
            // DetailsLbl
            // 
            resources.ApplyResources(this.DetailsLbl, "DetailsLbl");
            this.DetailsLbl.Name = "DetailsLbl";
            // 
            // StartBttn
            // 
            resources.ApplyResources(this.StartBttn, "StartBttn");
            this.StartBttn.Name = "StartBttn";
            this.StartBttn.Click += new System.EventHandler(this.OnStartBttnClicked);
            // 
            // CredPromptTmr
            // 
            this.CredPromptTmr.Interval = 500;
            this.CredPromptTmr.Tick += new System.EventHandler(this.OnCredPromptTmrTick);
            // 
            // UploadXferStartTmr
            // 
            this.UploadXferStartTmr.Interval = 500;
            this.UploadXferStartTmr.Tick += new System.EventHandler(this.OnUploadXferStartTmrTick);
            // 
            // HttpFileTransferForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.StartBttn);
            this.Controls.Add(this.DetailsLbl);
            this.Controls.Add(this.StatusLbl);
            this.Controls.Add(this.InfoLbl);
            this.Controls.Add(this.XfrPrgBr);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "HttpFileTransferForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar XfrPrgBr;
        private System.Windows.Forms.Label InfoLbl;
        private System.Windows.Forms.Label StatusLbl;
        private System.Windows.Forms.Label DetailsLbl;
        private System.Windows.Forms.Button StartBttn;
        private System.Windows.Forms.Timer CredPromptTmr;
        private System.Windows.Forms.Timer UploadXferStartTmr;
    }
}