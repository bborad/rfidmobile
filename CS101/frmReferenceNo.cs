/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Dispatch functionality
 **************************************************************************************/
using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class frmReferenceNo : Form
    {
        public frmReferenceNo()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
                //Open the scan form which scan the document Tag and AssetTagID, and verify the document no
                TagConfirmForm frm  = new TagConfirmForm();
                frm.recPerPage = UserPref.GetInstance().PageSize;
                //TagInfo T = new TagInfo(lstDocs.Items[lstDocs.SelectedIndices[0]].SubItems[2].Text);
                frm.SelectedDocumentID=txtReferenceNo.Text;
                //frm.Text = "Reference No. " + txtReferenceNo.Text;
                frm.Show(); 
                // disable form until this new form closed
                this.Enabled = false;
                frm.Closed +=new EventHandler(frm_Closed);
        }

        void frm_Closed(object sender, EventArgs e)
        {
            this.Enabled = true;  
        }

        private void OnRead1ButtonClicked(object sender, EventArgs e)
        {

        }
    }
}