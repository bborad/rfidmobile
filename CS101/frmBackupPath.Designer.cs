﻿namespace OnRamp
{
    partial class frmBackupPath
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBackupDir = new System.Windows.Forms.TextBox();
            this.txtResetPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHardResetPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtDBFilePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(13, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 17);
            this.label1.Text = "Backup Directory Path";
            // 
            // txtBackupDir
            // 
            this.txtBackupDir.Location = new System.Drawing.Point(11, 21);
            this.txtBackupDir.Name = "txtBackupDir";
            this.txtBackupDir.Size = new System.Drawing.Size(216, 23);
            this.txtBackupDir.TabIndex = 1;
            // 
            // txtResetPath
            // 
            this.txtResetPath.Location = new System.Drawing.Point(9, 66);
            this.txtResetPath.Name = "txtResetPath";
            this.txtResetPath.Size = new System.Drawing.Size(216, 23);
            this.txtResetPath.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(11, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 17);
            this.label2.Text = "Auto Reset Path";
            // 
            // txtHardResetPath
            // 
            this.txtHardResetPath.Location = new System.Drawing.Point(9, 112);
            this.txtHardResetPath.Name = "txtHardResetPath";
            this.txtHardResetPath.Size = new System.Drawing.Size(216, 23);
            this.txtHardResetPath.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(11, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 18);
            this.label3.Text = "Auto Hard Reset Path";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(237, 133);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(67, 22);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "Ok";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(237, 159);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(67, 22);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtDBFilePath
            // 
            this.txtDBFilePath.Location = new System.Drawing.Point(10, 158);
            this.txtDBFilePath.Name = "txtDBFilePath";
            this.txtDBFilePath.Size = new System.Drawing.Size(216, 23);
            this.txtDBFilePath.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(12, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(181, 18);
            this.label4.Text = "Database File Path";
            // 
            // frmBackupPath
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(317, 195);
            this.Controls.Add(this.txtDBFilePath);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txtHardResetPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtResetPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBackupDir);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBackupPath";
            this.Text = "Select Backup Path";
            this.Load += new System.EventHandler(this.frmBackupPath_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBackupDir;
        private System.Windows.Forms.TextBox txtResetPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtHardResetPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtDBFilePath;
        private System.Windows.Forms.Label label4;
    }
}