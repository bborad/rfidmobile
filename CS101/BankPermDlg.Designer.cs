namespace OnRamp
{
    partial class BankPermDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BankPermDlg));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.PasswdPanel = new System.Windows.Forms.Panel();
            this.Pwd4Bttn = new System.Windows.Forms.RadioButton();
            this.Pwd3Bttn = new System.Windows.Forms.RadioButton();
            this.Pwd2Bttn = new System.Windows.Forms.RadioButton();
            this.Pwd1Bttn = new System.Windows.Forms.RadioButton();
            this.Pwd0Bttn = new System.Windows.Forms.RadioButton();
            this.MemBnkPanel = new System.Windows.Forms.Panel();
            this.Bnk4Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk3Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk2Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk1Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk0Bttn = new System.Windows.Forms.RadioButton();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.PasswdPanel.SuspendLayout();
            this.MemBnkPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // PasswdPanel
            // 
            this.PasswdPanel.BackColor = System.Drawing.Color.White;
            this.PasswdPanel.Controls.Add(this.Pwd4Bttn);
            this.PasswdPanel.Controls.Add(this.Pwd3Bttn);
            this.PasswdPanel.Controls.Add(this.Pwd2Bttn);
            this.PasswdPanel.Controls.Add(this.Pwd1Bttn);
            this.PasswdPanel.Controls.Add(this.Pwd0Bttn);
            this.PasswdPanel.Controls.Add(this.label1);
            resources.ApplyResources(this.PasswdPanel, "PasswdPanel");
            this.PasswdPanel.Name = "PasswdPanel";
            // 
            // Pwd4Bttn
            // 
            resources.ApplyResources(this.Pwd4Bttn, "Pwd4Bttn");
            this.Pwd4Bttn.Name = "Pwd4Bttn";
            // 
            // Pwd3Bttn
            // 
            resources.ApplyResources(this.Pwd3Bttn, "Pwd3Bttn");
            this.Pwd3Bttn.Name = "Pwd3Bttn";
            // 
            // Pwd2Bttn
            // 
            resources.ApplyResources(this.Pwd2Bttn, "Pwd2Bttn");
            this.Pwd2Bttn.Name = "Pwd2Bttn";
            // 
            // Pwd1Bttn
            // 
            resources.ApplyResources(this.Pwd1Bttn, "Pwd1Bttn");
            this.Pwd1Bttn.Name = "Pwd1Bttn";
            // 
            // Pwd0Bttn
            // 
            resources.ApplyResources(this.Pwd0Bttn, "Pwd0Bttn");
            this.Pwd0Bttn.Name = "Pwd0Bttn";
            // 
            // MemBnkPanel
            // 
            this.MemBnkPanel.BackColor = System.Drawing.Color.White;
            this.MemBnkPanel.Controls.Add(this.Bnk4Bttn);
            this.MemBnkPanel.Controls.Add(this.Bnk3Bttn);
            this.MemBnkPanel.Controls.Add(this.Bnk2Bttn);
            this.MemBnkPanel.Controls.Add(this.Bnk1Bttn);
            this.MemBnkPanel.Controls.Add(this.Bnk0Bttn);
            this.MemBnkPanel.Controls.Add(this.label2);
            resources.ApplyResources(this.MemBnkPanel, "MemBnkPanel");
            this.MemBnkPanel.Name = "MemBnkPanel";
            // 
            // Bnk4Bttn
            // 
            resources.ApplyResources(this.Bnk4Bttn, "Bnk4Bttn");
            this.Bnk4Bttn.Name = "Bnk4Bttn";
            // 
            // Bnk3Bttn
            // 
            resources.ApplyResources(this.Bnk3Bttn, "Bnk3Bttn");
            this.Bnk3Bttn.Name = "Bnk3Bttn";
            // 
            // Bnk2Bttn
            // 
            resources.ApplyResources(this.Bnk2Bttn, "Bnk2Bttn");
            this.Bnk2Bttn.Name = "Bnk2Bttn";
            // 
            // Bnk1Bttn
            // 
            resources.ApplyResources(this.Bnk1Bttn, "Bnk1Bttn");
            this.Bnk1Bttn.Name = "Bnk1Bttn";
            // 
            // Bnk0Bttn
            // 
            resources.ApplyResources(this.Bnk0Bttn, "Bnk0Bttn");
            this.Bnk0Bttn.Name = "Bnk0Bttn";
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // BankPermDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.MemBnkPanel);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.PasswdPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BankPermDlg";
            this.PasswdPanel.ResumeLayout(false);
            this.MemBnkPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PasswdPanel;
        private System.Windows.Forms.RadioButton Pwd4Bttn;
        private System.Windows.Forms.RadioButton Pwd3Bttn;
        private System.Windows.Forms.RadioButton Pwd2Bttn;
        private System.Windows.Forms.RadioButton Pwd1Bttn;
        private System.Windows.Forms.RadioButton Pwd0Bttn;
        private System.Windows.Forms.Panel MemBnkPanel;
        private System.Windows.Forms.RadioButton Bnk4Bttn;
        private System.Windows.Forms.RadioButton Bnk3Bttn;
        private System.Windows.Forms.RadioButton Bnk2Bttn;
        private System.Windows.Forms.RadioButton Bnk1Bttn;
        private System.Windows.Forms.RadioButton Bnk0Bttn;
        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}