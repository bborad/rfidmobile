﻿/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Key Registration functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using dbClass=ClsRampdb;
using ClslibSerialNo;
using System.Net;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmRegister : Form
    {
        Boolean IsAllClose = true;
        public frmRegister()
        {
            InitializeComponent();
        }

        private void frmRegister_Load(object sender, EventArgs e)
        {
            try
            {
                UserPref Pref = UserPref.GetInstance();
                //MacAddress M = new MacAddress();
                //M.GetAdapters(); 
                //List<MacAddress> addresses = MacAddress.GetMacAddresses();
                //foreach (MacAddress address in addresses)
                //{
                //    //Console.WriteLine(address);
                //    MessageBox.Show(address.ToString());   
                //    txtMac.Text=address.ToString(); 
                //}
                
                txtMac.Text = SerialNo.GetDeviceID();

                
                

                txtName.Text = Pref.RdrName;  
                txtURL.Text = Pref.ServiceURL;
                string strHostName = Dns.GetHostName();
                IPHostEntry hostInfo = Dns.GetHostByName(strHostName);
                string strIPAddress = hostInfo.AddressList[0].ToString();
                txtIP.Text = strIPAddress;
                if (txtMac.Text == "76246-98531-45678-52465" && Pref.Key.Trim().Length==0)
                {
                    txtKey.Text = "0622340AE6DC24CDA87008BE4";
                }
                else
                {
                    txtKey.Text = Pref.Key; //"0622370AE6DC24CDA37008BE4";// "31121085736E1266D838045F2"; 
                }
                //txtURL.Text =  Pref.ServiceURL.Replace("iibtest", strIPAddress);    
            }

            catch (ApplicationException AP)
            {
                Program.ShowError(AP.Message);
                Logger.LogError(AP.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                Program.ShowError("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                Program.ShowError("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Program.ShowError(ep.Message.ToString());
                Logger.LogError(ep.Message); 
            }
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                UserPref Pref = UserPref.GetInstance();
                String RefNo = txtMac.Text.Trim();
                String EnteredKey = txtKey.Text.Trim().ToUpper();

                int iClient = 0;

                string appCodeDecoded = dbClass.Encrypt.GetAppModuleCodes(ref EnteredKey, ref iClient);
                appCodeDecoded = Convert.ToString(Convert.ToInt32(appCodeDecoded, 16), 2);
                appCodeDecoded = appCodeDecoded.PadLeft(32, '0');

                Pref.allApplicationModules = appCodeDecoded;


                String appCode = dbClass.Encrypt.GetApplicationCode(EnteredKey, RefNo);
                //MessageBox.Show("AppCode" + appCode);  
                DateTime ExpiryDate = dbClass.Encrypt.GetExpiryDate(EnteredKey, RefNo);
                //MessageBox.Show("Exp Date" + ExpiryDate);  
                String NoOfItemsCode = dbClass.Encrypt.GetMaxNoofItemsCode(EnteredKey, RefNo);
                //MessageBox.Show("No of Items Code" + NoOfItemsCode);

                String TempKey = dbClass.Encrypt.Generate_KeyV2(RefNo, false, ExpiryDate, "D E E P @ R A M P", Convert.ToString(Convert.ToInt32(appCode, 2), 16), NoOfItemsCode);

                //MessageBox.Show("Temp Key" + TempKey);  

                if (!dbClass.Encrypt.CompareKey(TempKey, EnteredKey, RefNo))
                {
                    MessageBox.Show("You have entered Invalid Registration Key, Please contact your vendor.");
                    return;
                }
                else
                {
                    if (iClient == 1)
                        UserPref.forClient = UserPref.Client.OnRamp;
                    else
                        UserPref.forClient = UserPref.Client.SmartTrack;

                    if (UserPref.AppModuleImplementation)
                    { 
                        Pref.SetAppModules();
                    }
                }

                if (appCode.Length != 4 || appCode.Substring(1, 1) != "1")
                {
                    MessageBox.Show("You have entered Invalid Registration key, Please contact your vendor.");
                    return;
                }


                if (ExpiryDate <= DateTime.Now)
                {
                    //txtKey.Text = "";
                    MessageBox.Show("Your subscription has expired. Please contact your vendor.");
                    return;
                }

                Int32 NoOfItems = dbClass.Encrypt.GetMaxNoofItems(EnteredKey, RefNo);
                if (NoOfItems != 0)
                {
                    if (!dbClass.Login.OnLineMode)
                    {
                        dbClass.Login._ItemLimit = NoOfItems - dbClass.Assets.getNoOfItems();
                        if (dbClass.Login.ItemLimit < 0)
                        {
                            MessageBox.Show("You are crossing registered item limit(" + NoOfItems + "). Please upgrade your registration.");
                            return;
                        }
                    }
                    else
                    {
                        dbClass.Login._ItemLimit = NoOfItems;
                        if (dbClass.Assets.getNoOfItems() > NoOfItems)
                        {
                            MessageBox.Show("You are crossing registered item limit(" + NoOfItems + "). Please upgrade your registration.");
                            return;
                        }
                    }
                }
                else
                {
                    dbClass.Login._ItemLimit = 50000;
                }

                MainMenuForm MainMenu = new MainMenuForm();
                MainMenu.Show();
                IsAllClose = false;
                this.Close();
            }
            catch(Exception epx)
            {
                Program.ShowError(epx.Message);
                Logger.LogError(epx.Message); 
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                UserPref Pref = UserPref.GetInstance();
                String RefNo = txtMac.Text.Trim();
                String EnteredKey = txtKey.Text.Trim().ToUpper();

                int iClient = 0;

                string appCodeDecoded = dbClass.Encrypt.GetAppModuleCodes(ref EnteredKey, ref iClient);
                appCodeDecoded = Convert.ToString(Convert.ToInt32(appCodeDecoded, 16), 2);
                appCodeDecoded = appCodeDecoded.PadLeft(32, '0');

                Pref.allApplicationModules = appCodeDecoded;

                String appCode = dbClass.Encrypt.GetApplicationCode(EnteredKey, RefNo);
                //MessageBox.Show("AppCode " + appCode);  
                DateTime ExpiryDate = dbClass.Encrypt.GetExpiryDate(EnteredKey, RefNo);
                //MessageBox.Show("Expir " + ExpiryDate);  
                String NoOfItems = dbClass.Encrypt.GetMaxNoofItemsCode(EnteredKey, RefNo);
                //MessageBox.Show("no items " + NoOfItems);
                String TempKey = dbClass.Encrypt.Generate_KeyV2(RefNo, false, ExpiryDate, "D E E P @ R A M P", Convert.ToString(Convert.ToInt32(appCode, 2), 16), NoOfItems);
                //MessageBox.Show("temp key " + TempKey);  
                if (dbClass.Encrypt.CompareKey(TempKey, EnteredKey, RefNo))
                {
                    Pref.OnExpired = ExpiryDate.ToString("dd/MM/yyyy");
                    Pref.ItemLimit = NoOfItems; 
                    Pref.Key = txtKey.Text.Trim().ToUpper();
                    Pref.ServiceURL = txtURL.Text.Trim();
                    Pref.RdrName = txtName.Text.Trim();   
                    MessageBox.Show("Registered Successfully.");

                    if (iClient == 1)
                        UserPref.forClient = UserPref.Client.OnRamp;
                    else
                        UserPref.forClient = UserPref.Client.SmartTrack;

                    if (UserPref.AppModuleImplementation)
                    {
                        // depending on appcode value Set App Modules parameters
                       // Pref.RemoveDispatch = true;
                       // Pref.RemoveFieldService = true;
                        //Pref.ImmunoSearch.Enable = false;
                        //Pref.VernonFlag.Enable = true;

                        Pref.SetAppModules();
                    }


                }
                else
                {
                    MessageBox.Show("Invalid Registration Key.");
                }
            }
            catch (Exception ex)
            {
                Program.ShowError(ex.Message);
                Logger.LogError(ex.Message); 
            }
        }

        private void frmRegister_Closing(object sender, CancelEventArgs e)
        {
            if (IsAllClose)
            {
                Program.ReqEnd = true;  
            }
        }


    }
}