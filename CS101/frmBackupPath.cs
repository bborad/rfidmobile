﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class frmBackupPath : Form
    {
        public frmBackupPath()
        {
            InitializeComponent();
        }

        UserPref Pref;

        private void frmBackupPath_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;
              Pref = UserPref.GetInstance();

            txtBackupDir.Text = Pref.BackUpDirPath;
            txtResetPath.Text = Pref.AutoStartResetPath;
            txtHardResetPath.Text = Pref.AutoStartHardResetPath;

            btnOk.Focus();

        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try 
            {
                if (txtBackupDir.Text.Trim() != "" && txtResetPath.Text.Trim() != "" && txtHardResetPath.Text.Trim() != "" && txtDBFilePath.Text.Trim() != "")
                {
                    if(txtBackupDir.Text.LastIndexOf(@"\") == txtBackupDir.Text.Length - 1)
                         Pref.BackUpDirPath = txtBackupDir.Text;
                    else
                        Pref.BackUpDirPath = txtBackupDir.Text + @"\";

                    if (txtResetPath.Text.LastIndexOf(@"\") == txtResetPath.Text.Length - 1)
                        Pref.AutoStartResetPath = txtResetPath.Text;
                    else
                        Pref.AutoStartResetPath = txtResetPath.Text + @"\";

                    if (txtHardResetPath.Text.LastIndexOf(@"\") == txtHardResetPath.Text.Length - 1)
                        Pref.AutoStartHardResetPath = txtHardResetPath.Text;
                    else
                        Pref.AutoStartHardResetPath = txtHardResetPath.Text + @"\";

                    if (txtDBFilePath.Text.LastIndexOf(@"\") == txtDBFilePath.Text.Length - 1)
                        Pref.DBFilePath = txtDBFilePath.Text;
                    else
                        Pref.DBFilePath = txtDBFilePath.Text + @"\";


                    this.Close();
                }
                else
                {
                    MessageBox.Show("Path cannot be blank or null.");
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}