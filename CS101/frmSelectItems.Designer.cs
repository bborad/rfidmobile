namespace OnRamp
{
    partial class frmSelectItems
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectItems));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cboItem = new System.Windows.Forms.ComboBox();
            this.cboSubGroup = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.B0Label = new System.Windows.Forms.Label();
            this.uploadCSV = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.cboItem);
            this.panel1.Controls.Add(this.cboSubGroup);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cboGroup);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cboLoc);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.B0Label);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // cboItem
            // 
            resources.ApplyResources(this.cboItem, "cboItem");
            this.cboItem.Name = "cboItem";
            // 
            // cboSubGroup
            // 
            resources.ApplyResources(this.cboSubGroup, "cboSubGroup");
            this.cboSubGroup.Name = "cboSubGroup";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cboGroup
            // 
            resources.ApplyResources(this.cboGroup, "cboGroup");
            this.cboGroup.Name = "cboGroup";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // B0Label
            // 
            resources.ApplyResources(this.B0Label, "B0Label");
            this.B0Label.Name = "B0Label";
            // 
            // uploadCSV
            // 
            this.uploadCSV.FileName = "openFileDialog1";
            // 
            // frmSelectItems
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSelectItems";
            this.Load += new System.EventHandler(this.frmSelectItems_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label B0Label;
        private System.Windows.Forms.ComboBox cboGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboSubGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog uploadCSV;
        private System.Windows.Forms.ComboBox cboItem;
    }
}