namespace OnRamp
{
    partial class frmSearchAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearchAsset));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.btnSearchOptions = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLotNo = new System.Windows.Forms.TextBox();
            this.cboSubGroup = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.txtAssetName = new System.Windows.Forms.TextBox();
            this.txtAssetNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.B0Label = new System.Windows.Forms.Label();
            this.uploadCSV = new System.Windows.Forms.OpenFileDialog();
            this.pnlSearchOptions = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.rdbtnExactString = new System.Windows.Forms.RadioButton();
            this.rdbtnWholeString = new System.Windows.Forms.RadioButton();
            this.rdbtnAnyWordDigit = new System.Windows.Forms.RadioButton();
            this.pnlSearch.SuspendLayout();
            this.pnlSearchOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.White;
            this.pnlSearch.Controls.Add(this.btnSearchOptions);
            this.pnlSearch.Controls.Add(this.label5);
            this.pnlSearch.Controls.Add(this.txtLotNo);
            this.pnlSearch.Controls.Add(this.cboSubGroup);
            this.pnlSearch.Controls.Add(this.label4);
            this.pnlSearch.Controls.Add(this.cboGroup);
            this.pnlSearch.Controls.Add(this.label3);
            this.pnlSearch.Controls.Add(this.cboLoc);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.btnUpload);
            this.pnlSearch.Controls.Add(this.txtAssetName);
            this.pnlSearch.Controls.Add(this.txtAssetNo);
            this.pnlSearch.Controls.Add(this.label2);
            this.pnlSearch.Controls.Add(this.label1);
            this.pnlSearch.Controls.Add(this.B0Label);
            resources.ApplyResources(this.pnlSearch, "pnlSearch");
            this.pnlSearch.Name = "pnlSearch";
            // 
            // btnSearchOptions
            // 
            resources.ApplyResources(this.btnSearchOptions, "btnSearchOptions");
            this.btnSearchOptions.Name = "btnSearchOptions";
            this.btnSearchOptions.Click += new System.EventHandler(this.btnSearchOptions_Click);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // txtLotNo
            // 
            resources.ApplyResources(this.txtLotNo, "txtLotNo");
            this.txtLotNo.Name = "txtLotNo";
            // 
            // cboSubGroup
            // 
            resources.ApplyResources(this.cboSubGroup, "cboSubGroup");
            this.cboSubGroup.Name = "cboSubGroup";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cboGroup
            // 
            resources.ApplyResources(this.cboGroup, "cboGroup");
            this.cboGroup.Name = "cboGroup";
            this.cboGroup.SelectedIndexChanged += new System.EventHandler(this.cboGroup_SelectedIndexChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnUpload
            // 
            resources.ApplyResources(this.btnUpload, "btnUpload");
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // txtAssetName
            // 
            resources.ApplyResources(this.txtAssetName, "txtAssetName");
            this.txtAssetName.Name = "txtAssetName";
            // 
            // txtAssetNo
            // 
            resources.ApplyResources(this.txtAssetNo, "txtAssetNo");
            this.txtAssetNo.Name = "txtAssetNo";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // B0Label
            // 
            resources.ApplyResources(this.B0Label, "B0Label");
            this.B0Label.Name = "B0Label";
            // 
            // uploadCSV
            // 
            this.uploadCSV.FileName = "openFileDialog1";
            // 
            // pnlSearchOptions
            // 
            this.pnlSearchOptions.BackColor = System.Drawing.SystemColors.Window;
            this.pnlSearchOptions.Controls.Add(this.btnOk);
            this.pnlSearchOptions.Controls.Add(this.rdbtnExactString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnWholeString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnAnyWordDigit);
            resources.ApplyResources(this.pnlSearchOptions, "pnlSearchOptions");
            this.pnlSearchOptions.Name = "pnlSearchOptions";
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rdbtnExactString
            // 
            resources.ApplyResources(this.rdbtnExactString, "rdbtnExactString");
            this.rdbtnExactString.Name = "rdbtnExactString";
            this.rdbtnExactString.Click += new System.EventHandler(this.rdbtnExactString_Click);
            // 
            // rdbtnWholeString
            // 
            resources.ApplyResources(this.rdbtnWholeString, "rdbtnWholeString");
            this.rdbtnWholeString.Name = "rdbtnWholeString";
            this.rdbtnWholeString.Click += new System.EventHandler(this.rdbtnWholeString_Click);
            // 
            // rdbtnAnyWordDigit
            // 
            resources.ApplyResources(this.rdbtnAnyWordDigit, "rdbtnAnyWordDigit");
            this.rdbtnAnyWordDigit.Name = "rdbtnAnyWordDigit";
            this.rdbtnAnyWordDigit.Click += new System.EventHandler(this.rdbtnAnyWordDigit_Click);
            // 
            // frmSearchAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.pnlSearchOptions);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearchAsset";
            this.Load += new System.EventHandler(this.frmSearchAsset_Load);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearchOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.TextBox txtAssetName;
        private System.Windows.Forms.TextBox txtAssetNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label B0Label;
        private System.Windows.Forms.ComboBox cboGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboSubGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog uploadCSV;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtLotNo;
        private System.Windows.Forms.Button btnSearchOptions;
        private System.Windows.Forms.Panel pnlSearchOptions;
        private System.Windows.Forms.RadioButton rdbtnAnyWordDigit;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.RadioButton rdbtnExactString;
        private System.Windows.Forms.RadioButton rdbtnWholeString;
    }
}