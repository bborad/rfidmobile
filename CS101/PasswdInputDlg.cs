using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class PasswdInputDlg : Form
    {
        [Flags]
        public enum PasswdType
        {
            None = 0,
            CurrAcc = 1,
            CurrKill = 2,
            NewAcc = 4,
            NewKill = 8
        }

        public class Passwords
        {
            public UInt32 CurAccPasswd;
            public UInt32 CurKillPasswd;
            public UInt32 NewAccPasswd;
            public UInt32 NewKillPasswd;
        };

        private Passwords passwdNotifyCtxt;

        private PasswdType inputsShown;
        private int numInputsShown;

        #region Static Function for Public Use
        static private PasswdInputDlg.Passwords passwdsCtxt = new PasswdInputDlg.Passwords();
        public static bool GetPassword(out UInt32 curAccPwd)
        {
            curAccPwd = 0;

            PasswdInputDlg Dlg = new PasswdInputDlg(PasswdInputDlg.PasswdType.CurrAcc, passwdsCtxt);
            DialogResult Res = Dlg.ShowDialog();
            Dlg.Dispose();
            if (Res == DialogResult.OK)
            {
                curAccPwd = passwdsCtxt.CurAccPasswd;
            }
            return (Res == DialogResult.OK);
        }
        #endregion

        /// <summary>
        /// The 'passwds' class handle would be persistent until Form closes
        /// </summary>
        /// <param name="visibleInputs"></param>
        /// <param name="notify"></param>
        /// <param name="passwds"></param>
        public PasswdInputDlg(PasswdType visibleInputs, Passwords passwds)
        {
            InitializeComponent();
            
            inputsShown = visibleInputs;
            numInputsShown = 0;

            Point Location = new Point(1, 1);
           
            if ((visibleInputs & PasswdType.CurrAcc) == PasswdType.CurrAcc)
            {
                CurAccPWTxtBx.Text = EPCTag.PasswdUInt32ToStr(passwds.CurAccPasswd);
                CurAccPWPanel.Location = Location;
                CurAccPWPanel.Visible = true;

                Location.Y += CurAccPWPanel.Height;
                numInputsShown++;
            }
            if ((visibleInputs & PasswdType.CurrKill) == PasswdType.CurrKill)
            {
                CurKillPWTxtBx.Text = EPCTag.PasswdUInt32ToStr(passwds.CurKillPasswd);
                CurKillPWPanel.Location = Location;
                CurKillPWPanel.Visible = true;

                Location.Y += CurKillPWPanel.Height;
                numInputsShown++;
            }
            if ((visibleInputs & PasswdType.NewAcc) == PasswdType.NewAcc)
            {
                NewAccPWTxtBx.Text = EPCTag.PasswdUInt32ToStr(passwds.NewAccPasswd);
                NewAccPWPanel.Location = Location;
                NewAccPWPanel.Visible = true;

                Location.Y += NewAccPWPanel.Height;
                numInputsShown++;
            }
            if ((visibleInputs & PasswdType.NewKill) == PasswdType.NewKill)
            {
                NewKillPWTxtBx.Text = EPCTag.PasswdUInt32ToStr(passwds.NewKillPasswd);
                NewKillPWPanel.Location = Location;
                NewKillPWPanel.Visible = true;
                numInputsShown++;
            }

            if (numInputsShown == 1)
            {
                switch (visibleInputs)
                {
                    case PasswdType.CurrAcc:
                        PlaceSinglePanelLocation(CurAccPWPanel);
                        break;
                    case PasswdType.CurrKill:
                        PlaceSinglePanelLocation(CurKillPWPanel);
                        break;
                    case PasswdType.NewAcc:
                        PlaceSinglePanelLocation(NewAccPWPanel);
                        break;
                    case PasswdType.NewKill:
                        PlaceSinglePanelLocation(NewKillPWPanel);
                        break;
                }
            }
            passwdNotifyCtxt = passwds;
        }

        private void PlaceSinglePanelLocation(Panel panel)
        {
            Point Location = panel.Location;
            Location.Y = panel.Height;
            panel.Location = Location;
        }

        private void OnOKBttnClicked(object sender, EventArgs e)
        {
            Passwords passwds = (Passwords)passwdNotifyCtxt;

            int Idx = 0;
            if (CurAccPWPanel.Visible)
            {
                passwds.CurAccPasswd = EPCTag.PasswdStrToUInt32(CurAccPWTxtBx.Text);
                Idx++;
            }
            if (CurKillPWPanel.Visible)
            {
                passwds.CurKillPasswd = EPCTag.PasswdStrToUInt32(CurKillPWTxtBx.Text);
                Idx++;
            }
            if (NewAccPWPanel.Visible)
            {
                passwds.NewAccPasswd = EPCTag.PasswdStrToUInt32(NewAccPWTxtBx.Text);
                Idx++;
            }
            if (NewKillPWPanel.Visible)
            {
                passwds.NewKillPasswd = EPCTag.PasswdStrToUInt32(NewKillPWTxtBx.Text);
            }
        }

        private void OnTxtBxKeyPress(object sender, KeyPressEventArgs e)
        {
            Program.HexKeyPressChk(sender, e);
        }

        private void OnTxtBxFocusLost(object sender, EventArgs e)
        {
            // Pad TextBox with 0s
            if (sender is TextBox)
            {
                TextBox TxtBx = (TextBox)sender;
                int TxtLen = 0;
                if (TxtBx.Text == null)
                    TxtLen = 0;
                else
                    TxtLen = TxtBx.TextLength;
                int NumZerosToPad = TxtBx.MaxLength - TxtLen;
                if (NumZerosToPad > 0)
                {
                    String PaddedStr = TxtBx.Text == null ? "" : String.Copy(TxtBx.Text);
                    PaddedStr = PaddedStr.PadRight(TxtBx.MaxLength, '0');
                    TxtBx.Text = PaddedStr;
                }
            }
        }
    }
}