using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Data;
using System.Data.SqlServerCe;
using System.IO;
using System.Security;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;
using ClsReaderLib.Devices;
using DataSetFactory;
using CS101UILib;

namespace OnRamp
{
    public partial class DBMgmtForm : Form
    {
        
        public DBMgmtForm()
        {
            InitializeComponent();

            DBPrefs[(int)OperType.Invtry] = new DBPref(DBNames[(int)OperType.Invtry]);
            DBPrefs[(int)OperType.RdBnks] = new DBPref(DBNames[(int)OperType.RdBnks]);
            DBPrefs[(int)OperType.WrBnks] = new DBPref(DBNames[(int)OperType.WrBnks]);
            DBPrefs[(int)OperType.Authentication] = new DBPref(DBNames[(int)OperType.Authentication]);
            DBPrefs[(int)OperType.Commission] = new DBPref(DBNames[(int)OperType.Commission]);
            DBPrefs[(int)OperType.Barcodes] = new DBPref(DBNames[(int)OperType.Barcodes]);

            DBPrefInputs[(int)OperType.Invtry] = TagInvtryDBPrefInput;
            DBPrefInputs[(int)OperType.RdBnks] = TagRdDBPrefInput;
            DBPrefInputs[(int)OperType.WrBnks] = TagWrDBPrefInput;
            DBPrefInputs[(int)OperType.Authentication] = TagAuthDBPrefInput;
            DBPrefInputs[(int)OperType.Commission] = TagCommDBPrefInput;
            DBPrefInputs[(int)OperType.Barcodes] = BcdeDBPrefInput;

            InvtryDataSetFactory InvtryDSFctry = new InvtryDataSetFactory();
            InvtryDataSet InvtryDS = (InvtryDataSet)InvtryDSFctry.GetDataSet();
            DBDataSets[(int)OperType.Invtry] = InvtryDSFctry.GetDataSet();
            DBSelCmds[(int)OperType.Invtry] = InvtryDS.SelectAllFldsCmd;
            DBCreatTblCmds[(int)OperType.Invtry] = InvtryDS.CreateTableCmd;
        }

        private String RFIDDataBaseDir
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\RFID\RFID Data";
            }
        }

        private enum OperType
        {
            Invtry = 0,
            RdBnks,
            WrBnks,
            Authentication,
            Commission,
            Barcodes,
            NumOperType
        }

        private static String[] DataFolderNames = new string[(int)OperType.NumOperType] 
            {
                "Tags Inventoried",
                "Tags Read",
                "Tags Written",
                "Tags Authenticated",
                "Tags Commissioned",
                "Barcodes Scanned"
            };

        private static String[] DBNames = new String[(int)OperType.NumOperType]
            {
                "TagInvtry",
                "TagRd",
                "TagWrite",
                "TagAuthen",
                "TagCommission",
                "Barcodes",
            };
        private DBPref[] DBPrefs = new DBPref[(int)OperType.NumOperType];
        private DBPrefInput[] DBPrefInputs = new DBPrefInput[(int)OperType.NumOperType];
        private DataSet[] DBDataSets = new DataSet[(int)OperType.NumOperType];
        private SqlCeCommand[] DBSelCmds = new SqlCeCommand[(int)OperType.NumOperType];
        private SqlCeCommand[] DBCreatTblCmds = new SqlCeCommand[(int)OperType.NumOperType];
        
       private void OnFormLoad(object sender, EventArgs e)
        {
            for (int i = 0; i < (int)OperType.NumOperType; i++)
            {
                DBPrefInputs[i].SelectedFmt = DBPrefs[i].FileFormat;
                DBPrefInputs[i].RemotePath = DBPrefs[i].RemotePath;
                if (String.IsNullOrEmpty(DBPrefs[i].LocalPath))
                    DBPrefInputs[i].LocalPath = RFIDDataBaseDir
                    + Path.DirectorySeparatorChar + DataFolderNames[i]
                    + Path.DirectorySeparatorChar;
                else
                    DBPrefInputs[i].LocalPath = DBPrefs[i].LocalPath;
            }
#if false
            TagRdDBPrefInput.SelectedFmt = DBPrefs[RdOpIdx].FileFormat;
            TagRdDBPrefInput.RemotePath = DBPrefs[RdOpIdx].RemotePath;
            if (String.IsNullOrEmpty(DBPrefs[RdOpIdx].LocalPath))
                TagRdDBPrefInput.LocalPath = RFIDDataBaseDir 
                    + Path.DirectorySeparatorChar + DataFolderNames[RdOpIdx]
                    + Path.DirectorySeparatorChar;
            else
                TagRdDBPrefInput.LocalPath = DBPrefs[RdOpIdx].LocalPath;
#endif
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        private void ShowErrDlg(String msg, String title)
        {
            TransientMsgDlg Dlg = new TransientMsgDlg(0);

            Dlg.SetDpyMsg(msg, title);
            Dlg.Show();
        }

        private Uri CheckRemotePath(String rmtPath)
        {
            Uri RemoteUri = null;

            try
            {
                if (String.IsNullOrEmpty(rmtPath))
                {
                    throw new ApplicationException("Please input Remote Path first");
                }
                RemoteUri = new Uri(rmtPath);
            }
            catch (ApplicationException ae)
            {
                Program.ShowError(ae.Message);
            }
            // Check Local URI
            catch (ArgumentNullException)
            {
                Program.ShowError("Please input remote path");
            }
            catch (UriFormatException)
            {
                Program.ShowError("Remote Path is invalid.\nCannot make connection to remote host");
            }
            catch  (Exception e)// all
            {
                Program.ShowError("Unexpected Exception:\n" + e.GetBaseException().GetType().Name + "\n" + e.Message);
            }

            return RemoteUri;
        }

        // if the supplied localPath does not include "file", suggest to user
        // the 'SuggFileName'.
        private bool CheckDownloadFile(ref String localPath, String suggFileName)
        {
            bool UserCancel = false;

            try
            {
                // Check File Name
                String LocalFileName = Path.GetFileName(localPath);
                if (String.IsNullOrEmpty(LocalFileName))
                {
                    if (Program.AskUserConfirm("Local file name not supplied.\nSave to \"" + suggFileName + "\"?") == DialogResult.OK)
                    {
                        // Update GUI
                        localPath = Path.GetDirectoryName(localPath) + Path.DirectorySeparatorChar + suggFileName;
                        LocalFileName = suggFileName;
                    }
                    else
                    {
                        UserCancel = true;
                    }
                }
                // check if file already exists
                if ((UserCancel == false) && File.Exists(localPath))
                {
                    if (Program.AskUserConfirm("\"" + LocalFileName + "\" already exists.\nOverwrite?") != DialogResult.OK)
                        UserCancel = true;
                }
            }
            catch (ArgumentException)
            {
                Program.ShowError("Invalid characters in Local Path, please check.");
            }
            catch (Exception e)// all
            {
                Program.ShowError("Unexpected Exception:\n" + e.GetBaseException().GetType().Name + "\n" + e.Message);
            }

            return (! UserCancel);
        }

        private bool CheckDownloadDir(String localPath)
        {
            bool DirExists = false;
            try
            {
                DirectoryInfo DInfo = new DirectoryInfo(Path.GetDirectoryName(localPath));
                if (!DInfo.Exists)
                    throw new ApplicationException("Specified folder does not exist, please check");
                DirExists = true;
            }
            catch (ArgumentException)
            {
                Program.ShowError("Invalid characters in Local Path, please check.");
            }
            catch (PathTooLongException)
            {
                Program.ShowError("Too many characters in Local Path, please check.");
            }
            catch (UnauthorizedAccessException)
            {
                Program.ShowError("Unauthorized access to Local Path, please check.");
            }
            catch (SecurityException)
            {
                Program.ShowError("User has no permission to access Local Path, please check");
            }
            catch (ApplicationException ae)
            {
                Program.ShowError(ae.Message);
            }
            catch (Exception e)// all
            {
                Program.ShowError("Unexpected Exception:\n" + e.GetBaseException().GetType().Name + "\n" + e.Message);
            }

            return DirExists;
        }

        private bool CheckUploadFile(String localPath)
        {
            bool FileExists = false;

            try
            {
                FileExists = File.Exists(localPath);
            }
            catch (Exception e) // just in case
            {
                Program.ShowError("Unexpected Exception:\n" + e.GetBaseException().GetType().Name + "\n" + e.Message);
            }

            return FileExists;
        }

        private bool UploadData(DBPrefInput dbPrefInput)
        {
            bool Succ = false;
            try
            {
                if (CheckUploadFile(dbPrefInput.LocalPath) == true)
                {
                    Uri RemoteUri = CheckRemotePath(dbPrefInput.RemotePath);
                    if (RemoteUri != null)
                    {
                        String LocalDir = Path.GetDirectoryName(dbPrefInput.LocalPath);
                        String LocalFile = Path.GetFileName(dbPrefInput.LocalPath);
                        if ((Succ = HttpFileTransferForm.Upload(LocalDir, LocalFile, RemoteUri)) == false)
                        {
                            Program.ShowError("Input fields invalid. Unable to start transfer, please check.");
                        }
                    }
                }
                else
                {
                    Program.ShowError("Specified Local File does not exist, please check");
                }
            }
            catch (Exception e) // just in case
            {
                Program.ShowError("Unexpected Exception:\n" + e.GetBaseException().GetType().Name + "\n" + e.Message);
            }
            return Succ;
        }

        private bool UploadTagRdData()
        {
            bool Succ = false;
            try
            {
                if (CheckUploadFile(TagRdDBPrefInput.LocalPath) == true)
                {
                    Uri RemoteUri = CheckRemotePath(TagRdDBPrefInput.RemotePath);
                    if (RemoteUri != null)
                    {
                        String LocalDir = Path.GetDirectoryName(TagRdDBPrefInput.LocalPath);
                        String LocalFile = Path.GetFileName(TagRdDBPrefInput.LocalPath);
                         if ((Succ = HttpFileTransferForm.Upload(LocalDir, LocalFile, RemoteUri)) == false)
                         {
                                Program.ShowError("Input fields invalid. Unable to start transfer, please check.");
                         }
                    }
                }
                else
                {
                    Program.ShowError("Specified Local File does not exist, please check");
                }
            }
            catch (Exception e) // just in case
            {
                Program.ShowError("Unexpected Exception:\n" + e.GetBaseException().GetType().Name + "\n" + e.Message);
            }
            return Succ;
        }

        private bool DownloadData(DBPrefInput dbPrefInput)
        {
            bool Succ = false;
            try
            {
                Uri RemoteUri = CheckRemotePath(dbPrefInput.RemotePath);
                if (RemoteUri != null)
                {
                    if (CheckDownloadDir(dbPrefInput.LocalPath) == true)
                    {
                        String LocalPath = dbPrefInput.LocalPath;
                        String[] UriSegments = RemoteUri.Segments;
                        String SuggFileName = Uri.UnescapeDataString(UriSegments[UriSegments.Length - 1]);
                        if (CheckDownloadFile(ref LocalPath, SuggFileName) == true)
                        {
                            dbPrefInput.LocalPath = LocalPath;
                            if (HttpFileTransferForm.Download(RemoteUri, Path.GetDirectoryName(LocalPath),
                                Path.GetFileName(LocalPath)) == false)
                            {
                                throw new ApplicationException("Input fields invalid. Unable to start transfer, please check.");
                            }
                            Succ = true; // assuming that Download is ok.
                        }
                    }
                }
            }
            catch (ApplicationException ae)
            {
                Program.ShowError(ae.Message);
            }
            catch (Exception e)
            {
                Program.ShowError("Path incorrect: " + e.GetType().Name);
            }

            return Succ;
        }

        private bool DownloadTagRdData()
        {
            bool Succ = false;

            try
            {
                Uri RemoteUri = CheckRemotePath(TagRdDBPrefInput.RemotePath);
                if (RemoteUri != null)
                {
                    if (CheckDownloadDir(TagRdDBPrefInput.LocalPath) == true)
                    {
                        String LocalPath = TagRdDBPrefInput.LocalPath;
                        String[] UriSegments = RemoteUri.Segments;
                        String SuggFileName = Uri.UnescapeDataString(UriSegments[UriSegments.Length - 1]);
                        if (CheckDownloadFile(ref LocalPath, SuggFileName) == true)
                        {
                            TagRdDBPrefInput.LocalPath = LocalPath;
                            if (HttpFileTransferForm.Download(RemoteUri, Path.GetDirectoryName(LocalPath), 
                                Path.GetFileName(LocalPath)) == false)
                            {
                                throw new ApplicationException("Input fields invalid. Unable to start transfer, please check.");
                            }
                            Succ = true; // assuming that Download is ok.
                        }
                    }
                }
            }
            catch (ApplicationException ae)
            {
                Program.ShowError(ae.Message);
            }
            catch (Exception e)
            {
                Program.ShowError("Path incorrect: " + e.GetType().Name);
            }

            return Succ;
        }

        private void OnDLBttnClicked(object sender, EventArgs e)
        {
#if false
            if (DBTabCtrl.TabPages[DBTabCtrl.SelectedIndex] == TagRdPg)
            {
                if (DownloadTagRdData() == true)
                {
                    // Save as Preference
                    DBPrefs[(int)OperType.RdBnks].FileFormat = TagRdDBPrefInput.SelectedFmt;
                    DBPrefs[(int)OperType.RdBnks].LocalPath = TagRdDBPrefInput.LocalPath;
                    DBPrefs[(int)OperType.RdBnks].RemotePath = TagRdDBPrefInput.RemotePath;
                }
            }
#else
            int SelTabIdx = DBTabCtrl.SelectedIndex;
            if (SelTabIdx >= 0 && SelTabIdx < (int)OperType.NumOperType)
            {
                DBPrefInput Input = DBPrefInputs[SelTabIdx];
                if (DownloadData(Input) == true)
                {
                    // Save as Preference
                    DBPrefs[SelTabIdx].FileFormat = Input.SelectedFmt;
                    DBPrefs[SelTabIdx].LocalPath = Input.LocalPath;
                    DBPrefs[SelTabIdx].RemotePath = Input.RemotePath;
                }
            }
#endif
        }


        private void OnULBttnClicked(object sender, EventArgs e)
        {
#if false
            if (DBTabCtrl.TabPages[DBTabCtrl.SelectedIndex] == TagRdPg)
            {
                if (UploadTagRdData() == true)
                {
                    // Save as Preference
                    DBPrefs[(int)OperType.RdBnks].FileFormat = TagRdDBPrefInput.SelectedFmt;
                    DBPrefs[(int)OperType.RdBnks].LocalPath = TagRdDBPrefInput.LocalPath;
                    DBPrefs[(int)OperType.RdBnks].RemotePath = TagRdDBPrefInput.RemotePath;
                }
            }
#else
            int SelTabIdx = DBTabCtrl.SelectedIndex;
            if (SelTabIdx >= 0 && SelTabIdx < (int)OperType.NumOperType)
            {
                DBPrefInput Input = DBPrefInputs[SelTabIdx];
                if (UploadData(Input) == true)
                {
                    // Save as Preference
                    DBPrefs[SelTabIdx].FileFormat = Input.SelectedFmt;
                    DBPrefs[SelTabIdx].LocalPath = Input.LocalPath;
                    DBPrefs[SelTabIdx].RemotePath = Input.RemotePath;
                }
            }
#endif
        }


        #region F1/F4/F5 Hotkey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F1:
                    if (down)
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    break;
            }
        }
        #endregion

        private void OnViewBttnClicked(object sender, EventArgs e)
        {
            int SelTabIdx = DBTabCtrl.SelectedIndex;
            if (SelTabIdx >= 0 && SelTabIdx < (int)OperType.NumOperType)
            {
                DBPrefInput Input = DBPrefInputs[SelTabIdx];
                
                DBDataViewForm DtVwFm = new DBDataViewForm(
                    DBDataSets[SelTabIdx],
                    DBDataSets[SelTabIdx].Tables[0].TableName,
                    DBSelCmds[SelTabIdx], Input.LocalPath,
                    Input.SelectedFmt);
                DtVwFm.ShowDialog();
                DtVwFm.Dispose();
                // clear to avoid un-intentional accumulation
                DBDataSets[SelTabIdx].Tables[0].Clear();
            }
        }


        // Note: This function contains schema information.
        private void ConsolidateInvtryDtTbl(DataTable dtTbl)
        {
            // Add or Update Rows
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
           
            YesNoToAllDialog.DialogResult OverwriteExistingRec = YesNoToAllDialog.DialogResult.No;
            if (TagInvtryForm.TagList != null)
            {
                try
                {
                    if ((dtTbl.DataSet is InvtryDataSet) == false)
                        throw new ApplicationException ("DataTable not belongs to Inventory DataSet as expected");

                    InvtryDataSet InvtryDS = (InvtryDataSet)dtTbl.DataSet;
                    foreach (TagListItem Tag in TagInvtryForm.TagList)
                    {
                        String TagIDStr = Tag.PC.ToString("X4") + Tag.EPC.ToString();
                        long FT = (Tag.LastTm.dwLowDateTime & 0xFFFFFFFF) +
                             (((long)Tag.LastTm.dwHighDateTime) << 32); 
                        DataRow[] Rows = null;
                        bool Succ = InvtryDS.SelectRowsByTagID(TagIDStr, out Rows);
                        if (!Succ)
                        {
                            ShowErrDlg("Select Rows Failed:\n" + InvtryDS.LastErrMsg, "Software Error");
                            return;
                        }
                        // Expecting 0 or 1 row only
                        if (Rows.Length == 0)
                        {
                            DataRow NewRow = dtTbl.NewRow();
                            if (InvtryDS.AssignToDBRow(NewRow, TagIDStr, Tag.RSSI, DateTime.FromFileTime(FT)))
                                dtTbl.Rows.Add(NewRow);
                            else
                                throw new ApplicationException("Error assigning to new DataTable row.\n"
                                    + InvtryDS.LastErrMsg); // forget about the rest of the records
                        }
                        else if (Rows.Length >= 1)
                        {
                            if (Rows.Length > 1)
                                Program.ShowWarning("Duplicate EPC Rows in Inventory Database Table");
                                                      
                            bool Overwrite = false;
                           switch (OverwriteExistingRec)
                           {
                               case YesNoToAllDialog.DialogResult.YesToAll:
                                   Overwrite = true;
                                   break;
                               case YesNoToAllDialog.DialogResult.NoToAll:
                                   Overwrite = false;
                                   break;
                               default:
                                   OverwriteExistingRec = YesNoToAllDialog.Ask(
                                       "Tag ID " + TagIDStr + " already exist in current Inventory Database Table.\n"
                                        + "Overwrite?");
                                   switch (OverwriteExistingRec)
                                   {
                                       case YesNoToAllDialog.DialogResult.Yes:
                                       case YesNoToAllDialog.DialogResult.YesToAll:
                                           Overwrite = true;
                                           break;
                                       default:
                                           Overwrite = false;
                                           break;
                                   }
                                   break;
                           }
                            if (Overwrite)
                            {
                                for (int r = 0; r < Rows.Length; r++)
                                {
                                    Rows[r].BeginEdit();
                                    if (InvtryDS.AssignToDBRow(Rows[r], Tag.RSSI, DateTime.FromFileTime(FT)) == false)
                                        throw new ApplicationException("Error updating existing DataTable row.\n"
                                        + InvtryDS.LastErrMsg); // forget about the rest of the records
                                    Rows[r].EndEdit();
                                }
                            }
                        }
                    }
                }
                catch (ApplicationException ae)
                {
                    ShowErrDlg(ae.Message, "Error Notice");
                }
                catch (Exception ex)
                {
                    Program.ShowError("Unexpected Exception ("
                        + ex.GetBaseException().GetType().Name + ") caught:" + "\n"
                        + ex.GetBaseException().Message);
                }
            }
           
        }

        private void OnConsBttnClicked(object sender, EventArgs e)
        {
            int SelTabIdx = DBTabCtrl.SelectedIndex;
            if (SelTabIdx >= 0 && SelTabIdx < (int)OperType.NumOperType)
            {
                DBPrefInput Input = DBPrefInputs[SelTabIdx];

                DBDataViewForm DtVwFm = new DBDataViewForm(
                    DBDataSets[SelTabIdx],
                    DBDataSets[SelTabIdx].Tables[0].TableName,
                    DBSelCmds[SelTabIdx], DBCreatTblCmds[SelTabIdx],
                    Input.LocalPath, Input.SelectedFmt, ConsolidateInvtryDtTbl);
                DtVwFm.ShowDialog();
                DtVwFm.Dispose();
                // clear to avoid un-intentional accumulation
                DBDataSets[SelTabIdx].Tables[0].Clear();
               // Save as Preference
                DBPrefs[SelTabIdx].FileFormat = Input.SelectedFmt;
                DBPrefs[SelTabIdx].LocalPath = Input.LocalPath;
                // remote path is irrelevant to this action
            }
        }

    }
}