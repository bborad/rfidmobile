/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Inventory functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using System.Collections;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmInventory : Form
    {
        private DataTable _searchedData;

        TagSearchForm TagSrchFm;
        frmChangeLocation frmChngLoc;

        Int32 selectedIndex;

           string itemName, tagId, locName,locId;

        public Int64 checkLocationID;

        public DataTable searchedData
        {
            set
            {
                _searchedData = value;
            }
        }

        public frmInventory()
        {
            InitializeComponent();
            checkLocationID = 0;
        }

        private void frmInventory_Load(object sender, EventArgs e)
        {
            //Set Location Combo
            DataTable dtList = new DataTable();
            dtList = Reasons.getReasonsList();

            DataRow dr = dtList.NewRow();
            dr["ID_Reason"] = 0;
            dr["Name"] = "Select Reason";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboReason.ValueMember = "ID_Reason";
            cboReason.DisplayMember = "Name";
            cboReason.DataSource = dtList;
            cboReason.SelectedValue = 0;

            if(recPerPage == 0)
                recPerPage = UserPref.GetInstance().PageSize;

            SetupPaging(_searchedData.Rows.Count);
            curPage = 1;             

            BindListData();
        }

        private void BindListData()
        {

            lstAsset.Items.Clear();

            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            //DataRow dr=new DataRow();
            lstAsset.CheckBoxes = true;
            lstAsset.FullRowSelect = true;

            DataRow dr;

            int fromRec, toRec;

            fromRec = ((curPage - 1) * recPerPage) + 1;
            toRec = curPage * recPerPage;

            if (toRec > _searchedData.Rows.Count)
                toRec = _searchedData.Rows.Count;
           
            for(int i = fromRec - 1;i<toRec;i++)            
            {
                dr = _searchedData.Rows[i];
                lstItem = new ListViewItem();
                //Convert.ToString(dr["TagID"]) + "," + Convert.ToString(dr["Name"])

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["Check_Loc_Name"]);
                lstItem.SubItems.Add(ls);


                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["AssetName"]);
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["Db_Loc_Name"]);
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["ErrorCode"]);
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["TagID"]);
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["Db_Loc_ID"]);
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["Check_Loc_ID"]);
                lstItem.SubItems.Add(ls);

                if (checkLocationID == 0)
                {
                    checkLocationID = Convert.ToInt64(dr["Check_Loc_ID"]);
                }

                //ls = new ListViewItem.ListViewSubItem();
                //ls.Text = Convert.ToString(dr["RowStatus"]);
                //lstItem.SubItems.Add(ls);

                lstAsset.Items.Add(lstItem);
            }

            lstAsset.Refresh();
        }

        public void GetSearchedItem(Int32 index)
        {
            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            //DataRow dr=new DataRow();
           // lstAsset.CheckBoxes = true;
           // lstAsset.FullRowSelect = true;

            DataRow dr;

            dr = _searchedData.Rows[index];
            lstItem = new ListViewItem(); 

            itemName = Convert.ToString(dr["AssetName"]);
            locName =  Convert.ToString(dr["Check_Loc_Name"]);
            tagId = Convert.ToString(dr["TagID"]);
            locId = Convert.ToString(dr["Check_Loc_ID"]); 

        }

        private void btnIgnorAll_Click(object sender, EventArgs e)
        {
            lstAsset.Clear();
        }

        private void btnChangeLoc_Click(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;

            if (checkLocationID == 0)
            {
                MessageBox.Show("Location to change is not defined.");
                return;
            }

            ArrayList tmpList = new ArrayList();
            StringBuilder tags = new StringBuilder("'-1'");
            ArrayList csvArray = new ArrayList();

            int itemChecked = 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("");
            int iOther = 0, iCount = 0;
            Int32 ReasonID = 0;

            if (AppModules.VernonFlag)
            {
                if(cboReason.SelectedIndex == -1 || Convert.ToInt32(cboReason.SelectedValue) <= 0)
                {
                    MessageBox.Show("Please select Reason.");
                    return;
                }
                else
                {
                    ReasonID = Convert.ToInt32(cboReason.SelectedValue);
                }
            }

            Cursor.Current = Cursors.WaitCursor; 
            foreach (ListViewItem lv in lstAsset.Items)
            {
                if (lv.Checked)
                {
                    iCount++;
                    if (Convert.ToInt64(lv.SubItems[7].Text.ToString().Trim()) < 0)
                    {
                        MessageBox.Show("Please do Synchronisation first.");
                        return;
                    }
                    else
                    {
                        if (lv.SubItems[4].Text.ToString().Trim().ToLower() == "location_mismatch")
                        {
                            itemChecked = 1;
                            if (tags.Length >= 6000)
                            {
                                csvArray.Add(tags.ToString());
                                tags = new StringBuilder("'-1'");
                            }
                            tags.Append("," + "'" + lv.SubItems[5].Text + "'");
                            // success += 1;
                            tmpList.Add(lv.Index);

                           // Assets.UpdateAssetLocation(lv.SubItems[5].Text.ToString().Trim(), Convert.ToInt64(lv.SubItems[7].Text.ToString().Trim()), ReasonID);

                          //  if (sb.ToString() == "")
                             //   sb.Append(lv.SubItems[5].Text.ToString().Trim());
                           // else
                                //sb.Append("," + lv.SubItems[5].Text.ToString().Trim());
                        }
                        else
                            iOther++;
                    }
                }
            }

            if (iCount == 0)
            {
                MessageBox.Show("Please select items.");
            }
            else if (itemChecked == 1)
            {
                csvArray.Add(tags.ToString());
                foreach (string strCsvTag in csvArray)
                {
                    Assets.UpdateAssetLocation(strCsvTag, checkLocationID, ReasonID);
                }

                MessageBox.Show("Discrepancies removed successfully.");

                tmpList.Sort();
                int i;

                int page = (curPage - 1) * recPerPage;

                for (i = tmpList.Count - 1; i >= 0; i--)
                {
                    //lstAsset.Items.RemoveAt(Convert.ToInt32(tmpList[i]));
                    _searchedData.Rows[page + Convert.ToInt32(tmpList[i])].Delete();
                }

                _searchedData.AcceptChanges();

                SetupPaging(_searchedData.Rows.Count);

                BindListData();
               
            }
            else if (iOther > 0)
                MessageBox.Show("Discrepancies can only be removed for Location_Mismatch error code.");

            lstAsset.Refresh();

            Cursor.Current = Cursors.Default;

            //string[] stTag = sb.ToString().Split(',');
            //int iListcount = 0;
            //for (int i = 0; i < lstAsset.Items.Count; i++)
            //{
            //    foreach (string s in stTag)
            //    {
            //        if (lstAsset.Items[i].SubItems[5].Text.ToString().Trim() == s)
            //        {
            //            lstAsset.Items.RemoveAt(i);
            //            iListcount++;
            //        }
            //    }

            //}          

            //if (iOther > 0)
            //    MessageBox.Show("Discrepancies can only be removed for Location_Mismatch error code.");
            //else
            //    MessageBox.Show("Discrepancies removed successfully.");

            //searchedData.Rows[
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lv in lstAsset.Items)
            {
                if (lv.SubItems[4].Text.ToString().Trim().ToLower() == "location_mismatch")
                    lv.Checked = true;
            }
        }

        #region Paging Code

        int curPage, totalPages;
        public int recPerPage;

        DataTable dtPages;
        private int SetupPaging(int totalRec)
        {
            //int totalRec = EPCListV.Items.Count;
            int totalPages = 0;

            dtPages = new DataTable();
            DataColumn dc = new DataColumn("Page");
            dtPages.Columns.Add(dc);

            DataColumn dcc = new DataColumn("Index");
            dtPages.Columns.Add(dcc);

            if (totalRec > 0 && recPerPage > 0)
            {
                totalPages = totalRec / recPerPage;
                if (totalRec % recPerPage != 0)
                    totalPages = totalPages + 1;
            }

           // cmbPage.Items.Clear();

            DataRow dr;

            //dr = dtPages.NewRow();
            //dr["Page"] = "Select";
            //dr["Index"] = 0;
            //dtPages.Rows.Add(dr);

            for (int i = 1; i <= totalPages; i++)
            {
                dr = dtPages.NewRow();
                dr["Page"] = i.ToString();
                dr["Index"] = i;
                dtPages.Rows.Add(dr);
            }

            dtPages.AcceptChanges();

            cmbPage.DataSource = dtPages;
            cmbPage.DisplayMember = "Page";
            cmbPage.ValueMember = "Index";

            if (totalPages > 0)
                cmbPage.SelectedValue = 1;

            cmbPage.Visible = true;
            lblPage.Visible = true;
            cmbPage.SelectedValueChanged += new EventHandler(cmbPage_SelectedValueChanged);

            return totalPages;
        }

        private void cmbPage_SelectedValueChanged(object sender, EventArgs e)
        {
            if (curPage == Convert.ToInt32(cmbPage.SelectedValue))
                return;
            curPage = Convert.ToInt32(cmbPage.SelectedValue);

            Cursor.Current = Cursors.WaitCursor;
            BindListData();
            Cursor.Current = Cursors.Default;
        }

        #endregion

        private void btnChangeAll_Click(object sender, EventArgs e)
        {
            //Cursor.Current = Cursors.WaitCursor;

            if (checkLocationID == 0)
            {
                MessageBox.Show("Location to change is not defined.");
                return;
            }

            ArrayList tmpList = new ArrayList();
            StringBuilder tags = new StringBuilder("'-1'");
            ArrayList csvArray = new ArrayList();

            int itemChecked = 0;

            StringBuilder sb = new StringBuilder();
            sb.Append("");
            int iOther = 0, iCount = 0;
            Int32 ReasonID = 0;

            if (AppModules.VernonFlag)
            {
                if(cboReason.SelectedIndex == -1 || Convert.ToInt32(cboReason.SelectedValue) <= 0)
                {
                    MessageBox.Show("Please select Reason.");
                    return;
                }
                else
                {
                    ReasonID = Convert.ToInt32(cboReason.SelectedValue);
                }
            }

            DataRow dr;
            Cursor.Current = Cursors.WaitCursor;
            for (int i = 0; i < _searchedData.Rows.Count; i++)
            {
                dr = _searchedData.Rows[i];
               // if (lv.Checked)
               // {
                    iCount++;
                    if (Convert.ToInt64(dr["Check_Loc_ID"]) < 0)
                    {
                        MessageBox.Show("Please do Synchronisation first.");
                        return;
                    }
                    else
                    {
                        if (dr["ErrorCode"].ToString().ToLower() == "location_mismatch")
                        {
                            itemChecked = 1;
                            if (tags.Length >= 6000)
                            {
                                csvArray.Add(tags.ToString());
                                tags = new StringBuilder("'-1'");
                            }
                            tags.Append("," + "'" + dr["TagID"].ToString() + "'");
                            // success += 1;
                          //  tmpList.Add(lv.Index);
                            // Assets.UpdateAssetLocation(lv.SubItems[5].Text.ToString().Trim(), Convert.ToInt64(lv.SubItems[7].Text.ToString().Trim()), ReasonID);

                            //  if (sb.ToString() == "")
                            //   sb.Append(lv.SubItems[5].Text.ToString().Trim());
                            // else
                            //sb.Append("," + lv.SubItems[5].Text.ToString().Trim());
                        }
                        else
                            iOther++;
                    }
                //}
            }

            if (iCount == 0)
            {
                MessageBox.Show("Please select items.");
            }
            else if (itemChecked == 1)
            {
                csvArray.Add(tags.ToString());
                foreach (string strCsvTag in csvArray)
                {
                    Assets.UpdateAssetLocation(strCsvTag, checkLocationID, ReasonID);
                }

                MessageBox.Show("Discrepancies removed successfully.");

                _searchedData.Rows.Clear();

                SetupPaging(_searchedData.Rows.Count);

                BindListData();
                
              
            }
            else if (iOther > 0)
                MessageBox.Show("Discrepancies can only be removed for Location_Mismatch error code.");

            lstAsset.Refresh();

            Cursor.Current = Cursors.Default;

        }

        private void lstAsset_ItemActivate(object sender, EventArgs e)
        {           
            Assets Ast;
            Ast = null;
            if (lstAsset.SelectedIndices.Count == 1)
            {               
                if (lstAsset.Items[lstAsset.SelectedIndices[0]].SubItems[4].Text.ToString().Trim().ToLower() == "missingtags")
                {
                   // this.Enabled = false;
                    selectedIndex = lstAsset.SelectedIndices[0];
                    Ast = new Assets(lstAsset.Items[lstAsset.SelectedIndices[0]].SubItems[5].Text);
                    TagSrchFm = new TagSearchForm();
                    TagSrchFm.SearchTagNo = Ast.TagID;
                    TagSrchFm.callFrom = "frmInventory";
                    TagSrchFm.AssetName = Ast.Name;
                    TagSrchFm.AssetNo = Ast.AssetNo;
                    TagSrchFm.Closed += new EventHandler(TagSrchFm_Closed);
                    TagSrchFm.Show();  
                   
                }
                else
                {
                   // MessageBox.Show("Please select a single item.");
                    return;
                }
                
            }
            else
            {
                MessageBox.Show("Please select single item.");
                return;
            }
        }

        void TagSrchFm_Closed(object sender, EventArgs e)
        {
            if (selectedIndex >= 0 && TagSrchFm.changeLoc == true)
            {
                int page = (curPage - 1) * recPerPage;
                frmChngLoc = new frmChangeLocation();
                frmChngLoc.Closing += new CancelEventHandler(frmChngLoc_Closing);
                GetSearchedItem(page + selectedIndex);
                frmChngLoc.tagId = tagId;
                frmChngLoc.itemName = itemName;
                frmChngLoc.locName = locName;
                frmChngLoc.locId = locId;
                frmChngLoc.Show();
            }
            else
            {
               this.Enabled = true;
            }

        }

        void frmChngLoc_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                if (frmChngLoc.locChanged == true)
                {
                    int page = (curPage - 1) * recPerPage;
                    _searchedData.Rows[page + selectedIndex].Delete();
                    _searchedData.AcceptChanges();

                    SetupPaging(_searchedData.Rows.Count);

                    BindListData();
                }
                this.Enabled = true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message); 
            }
        }
        
    }
}