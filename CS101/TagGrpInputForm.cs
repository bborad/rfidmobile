using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class TagGrpInputForm : Form
    {
        private Byte[] EPCChosen;

        public TagGrpInputForm()
        {
            InitializeComponent();
            EPCChosen = new byte[0];
        }

        public Byte[] EPCMask
        {
            set
            {
                tagGrpMaskInput.SetPC(false, null);
                bool ena = (value != null && value.Length > 0);
                tagGrpMaskInput.SetEPC(ena, value);
            }
            get
            {
                return EPCChosen;
            }
        }

        public uint NumTagsToWr
        {
            get
            {
                uint NumTags = 0;
                String ErrMsg;
                if (NumTagsInputValue(out NumTags, out ErrMsg) == false)
                {
                    NumTags = 0;
                }
                return NumTags;
            }
        }

        public bool DupWrFilterOn
        {
            get
            {
                return DupWrFilterOnChkBx.Checked;
            }
        }

        private bool NumTagsInputValue(out uint numTags, out String errMsg)
        {
            bool Succ = false;
            numTags = 0;
            errMsg = String.Empty;

            try
            {
                numTags= UInt32.Parse(PopSzTxtBx.Text);
                Succ = true;
            }
            catch (ArgumentNullException ane)
            {
                errMsg = "Invalid input: " + ane.Message;
            }
            catch (ArgumentException ae)
            {
                errMsg = "Invalid input: " + ae.Message;
            }
            catch (FormatException fe)
            {
                errMsg = "Invalid input: " + fe.Message;
            }
            catch (OverflowException oe)
            {
                errMsg = "Invalid input: " + oe.Message;
            }

            return Succ;
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
#if false
                Byte[] EPC;
                if (!tagGrpMaskInput.GetEPC(out EPC))
                    EPC = null;
                if (EPC == null || EPC.Length == 0)
                {
                    MessageBox.Show("EPC Mask should not be empty");
                }
                if (EPC != null && EPC.Length == EPCTag.EPCFldSz * 2) // Full Mask, useless
                {
                    MessageBox.Show("EPC Mask should not be field size");
                    e.Cancel = true;
                }
                else
                {
                    EPCChosen = EPC;
                }
#else
                uint NumTags = 0;
                String ErrMsg = String.Empty;
                if (NumTagsInputValue(out NumTags, out ErrMsg) == false)
                {
                    e.Cancel = true;
                    Program.ShowError(ErrMsg);
                }
#endif
            }
            else
            {
                EPCChosen = new byte[0];
            }
        }

        private void OnPopSzTxtBxKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }
    }
}