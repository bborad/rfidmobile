using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using CS101UILib;
using System.Windows.Forms;
using System.Collections;
using ClsRampdb;
using ClsLibBKLogs;
using ClsReaderLib;
using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class TagRangingForm : Form
    {
        #region "sound and meolody variables"
        const int NumThresholdLvls = 8;
        //#if USE_WBRSSI
        //        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
        //            {
        //                0.0F, // within read range
        //                100.0F, // low
        //                100.0F, // 
        //                100.0F,
        //                110.0F, // medium
        //                110.0F,
        //                110.0F, // 
        //                120.0F, // high
        //            };
        //#else
        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
            {
                0.0F, // within read range
                50.0F, // low
                60.0F, // 
                65.0F,
                70.0F, // medium
                75.0F,
                80.0F, // 
                90.0F, // high
            };
        //#endif

        static uint[] PulsePeriods = new uint[NumThresholdLvls]
            {
                1000,
                1000,
                1000,
                600,
                400,
                200,
                100,
                100,
            };

        static uint[] LedRadius = new uint[NumThresholdLvls]
            {
                20,
                20,
                20,
                30,
                40,
                50,
                60,
                60,
            };

        static uint[] BuzzerPitch = new uint[NumThresholdLvls]
            {
                200,
                200,
                200,
                400,
                800,
                1600,
                3200,
                3200,
            };
        static RingTone[] RingMelody = new RingTone[NumThresholdLvls]
            {
                RingTone.T1,
                RingTone.T1,
                RingTone.T2,
                RingTone.T2,
                RingTone.T2,
                RingTone.T3,
                RingTone.T3,
                RingTone.T3,
                
            };
        #endregion

        object tagListLock;

        public enum dispCol
        {
            Count = 0,
            AssetName = 1,
            TagID = 3,
            RSSI = 2,
            Counts = 4
        }

        private Hashtable TagData;
        private ArrayList scannedTags;

        public DataTable dtSelectedItems;

        private TagAccListVHelper ListVHelper;

        private String _SearchTagList;
        //private DataTable _SearchTagData;

        public String SearchTagList
        {
            set
            {
                _SearchTagList = value;
            }
        }

        //public DataTable SearchTagData
        //{
        //    set
        //    {
        //        _SearchTagData = value; 
        //    }
        //}


        private Double MaxRSSI = 1;

        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion


        public TagRangingForm()
        {
            InitializeComponent();

            tagListLock = new object();

            InitStartButtonState();

            if (DutyCycleRequired())
            {
                DutyCycInit();
                DCStateInit();
            }
            else
            {
                InvtryRestartTmrInit();
            }

            // Temp Thrsh Display
            TempMonitor TempMon = TempMonitor.GetInstance();
            DisplayTempThreshold(TempMon.XcvrThrshTemp);

            // Temp continous update display
            // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //Rdr.TempUpdateEvt += TempUpdateEvtHandler;
            Rdr.RegisterTempUpdateEvt(TempUpdateEvtHandler);
            tagList = new TagRangeList();

            ListVHelper = new TagAccListVHelper(this.EPCListV);
#if BURN_IN
            AliveTmr.Tick += OnAliveTmrTick;
#endif
        }

        #region DutyCycle State
        private DutyCycleTmr dcTmr;
        private OpState dcState;

        private void DCStateInit()
        {
            dcState = OpState.Stopped;
        }

        private void DCStateSet(OpState newState)
        {
            dcState = newState;

            switch (newState)
            {
                case OpState.Started:
                    // start On Timer
                    dcTmr.OnCycStart(DutyOnCycExpired);
                    break;
                case OpState.Stopped:
                    // Not every time an Off timer needs to be started
                    // do nothing here
                    break;
            }
        }

        private OpState DCStateGet()
        {
            return dcState;
        }
        #endregion

        #region StartButton States
        private void InitStartButtonState()
        {
            OpState State = OpState.Stopped;
            this.StartButton.Tag = State; // boxing
        }

        private OpState StartButtonState()
        {
            OpState state = OpState.Stopped;
            if (StartButton.Tag is OpState)
            {
                state = (OpState)StartButton.Tag;
            }
            else
            {
                throw new ApplicationException("StartButton.Tag is not of type OpState");
            }
            return state;
        }

        // Affect StartButton, ClrButton and TitleBar display
        private void SetStartButtonState(OpState newState)
        {
            OpState CurState = StartButtonState();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRangingForm));

            switch (newState)
            {
                case OpState.Starting:
                    // disable Start button
                    StartButton.Enabled = false;
                    // disable Clear button
                    ClrButton.Enabled = false;
                    btnSearch.Enabled = false;
                    Program.RefreshStatusLabel(this, "Starting...");
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    StartButton.Text = "Stop";
                    StartButton.Enabled = true;
                    Program.RefreshStatusLabel(this, "Running...");
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    // RGB component reversed
                    int BlnkClr = Color.White.B << 16 | Color.White.G << 8 | Color.White.R;
                    PosSp.f_PosSp_LedBlink((uint)BlnkClr, 1000, 500);
                    break;
                case OpState.Stopping:
                    StartButton.Enabled = false;
                    Program.RefreshStatusLabel(this, "Stopping...");
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    PosSp.f_PosSp_LedSetOff();
                    break;
                case OpState.Stopped:
                    StartButton.Text = resources.GetString("StartButton.Text");
                    StartButton.Enabled = true;
                    ClrButton.Enabled = true;
                    btnSearch.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    OverTempAlertStop(); // Cancel Existing Flash Alerts
                    TempMonitor.GetInstance().PeriodicTempGetStop(); // user-stop case/catch-all
                    PosSp.f_PosSp_LedSetOff();
                    break;
            }
            StartButton.Tag = newState;
        }
        #endregion

        #region EPCListV routines
        private bool TagAlreadyInListV(ref UINT96_T tag, out ListViewItem foundItem)
        {
            bool Matched = false;
            foundItem = null;
            foreach (ListViewItem item in EPCListV.Items)
            {
                if (item.Tag is UINT96_T)
                {
                    UINT96_T EPC = (UINT96_T)item.Tag;
                    Matched = (EPC.m_CSB == tag.m_CSB
                        && EPC.m_LSB == tag.m_LSB
                        && EPC.m_MSB == tag.m_MSB);
                    if (Matched)
                    {
                        foundItem = item;
                        break; // from foreach
                    }
                }
                else
                {
                    throw new ApplicationException("ListViewItem Tag not of type UINT96_T");
                }
            }
            return Matched;
        }

        private bool TagListedAtRow(UINT96_T tag, int rowIdx)
        {
            if (rowIdx < 0 || EPCListV.Items.Count <= rowIdx)
                return false;

            bool Matched = false;

            ListViewItem Row = EPCListV.Items[rowIdx];

            if (Row.Tag is UINT96_T)
            {
                UINT96_T EPC = (UINT96_T)Row.Tag;
                Matched = (EPC.m_CSB == tag.m_CSB
                    && EPC.m_LSB == tag.m_LSB
                    && EPC.m_MSB == tag.m_MSB);
            }
            else
            {
                throw new ApplicationException("ListViewItemTag  not of type UINT96_T");
            }

            return Matched;
        }

        private void UpdateRangeFldsAtListV(ref ListViewItem row, UINT96_T EPC, UInt16 cnt, float rssi)
        {
            // Double check that the EPC is at that row
            if (row != null && row.ListView == EPCListV)
            {

                ListViewItem.ListViewSubItem Col = null;
                // update Cnt
                // Col = row.SubItems[Convert.ToInt32(dispCol.Counts)];  // 4th column is 'Cnt'
                // Col.Text = cnt.ToString();
                // update RSSI 
                Col = row.SubItems[Convert.ToInt32(dispCol.RSSI)]; // 3rd column is 'RSSI'

                row.BackColor = EPCListV.BackColor;
                try
                {
                    Col.Text = rssi.ToString("F0");
                    //if (float.Parse(Col.Text, System.Globalization.NumberStyles.Float) != rssi)
                    //{
                    //    Col.Text = rssi.ToString("F0"); // 1 decimal place
                    //    //if (Convert.ToDouble(Col.Text) > MaxRSSI)
                    //    //{
                    //    //    if (MaxRSSI > 0)
                    //    //        row.BackColor = Color.Green;
                    //    //    MaxRSSI = Convert.ToDouble(Col.Text);
                    //    //    RssiMelodyGenerate(MaxRSSI);
                    //    //}
                    //}
                }
                catch (ApplicationException ap)
                {
                    Program.ShowError(ap.Message);
                    Logger.LogError(ap.Message);
                }
                catch (System.Web.Services.Protocols.SoapException ex)
                {
                    if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                        Program.ShowError("Request from innvalid IP address.");
                    else
                        Program.ShowError("Network Protocol Failure.");
                    Logger.LogError(ex.Message);
                }
                catch (Exception) // FormatException, OverflowException, ArgumentException
                {
                    Col.Text = "0.0";
                }
                // Move this updated row to #1 in the EPCListV
                if (row.Index != 0)
                {
                    if (rssi > 0)
                    {
                        EPCListV.Items.RemoveAt(row.Index);
                        if (rssi > maxRSSIval)
                            row = EPCListV.Items.Insert(0, row); // in case 'row' object is changed
                        else
                            row = EPCListV.Items.Insert(1, row);
                    }
                }
            }
            else
                throw new ApplicationException("Software Error: Tag " + EPC.ToString() + " not found in ListView");

        }

        //private ListViewItem AddEPCToListV(UINT96_T EPC, UInt16 cnt, float rssi)
        //{
        //    // If not found, add new item + count
        //    int nRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
        //    ListViewItem Row = new ListViewItem(new String[] { (nRows+1).ToString(),
        //        EPC.ToString(), rssi.ToString("F0"), cnt.ToString(), });    
        //    // Insert at the beginning (make it visible)
        //    Row = EPCListV.Items.Insert(0, Row);
        //    Row.Tag = EPC;
        //    RefreshTagCntLabel();

        //    return Row;
        //}

        private ListViewItem AddEPCToListV(UINT96_T EPC, UInt16 cnt, float rssi, string assetName)
        {
            // If not found, add new item + count
            int nRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
            //  Assets A = new Assets(EPC.ToString()); 
            ListViewItem Row = new ListViewItem(new String[] { (nRows+1).ToString(),
                assetName, rssi.ToString("F0"),EPC.ToString(), cnt.ToString(), });
            // Insert at the beginning (make it visible)
            Row = EPCListV.Items.Insert(0, Row);
            Row.Tag = EPC;
            RefreshTagCntLabel();

            return Row;
        }


        private void ClearEPCListV()
        {
            EPCListV.Items.Clear();
            RefreshTagCntLabel();
        }
        #endregion

        #region Label Display
        private void RefreshTagCntLabel()
        {
            TagCntLbl.Text = EPCListV.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLbl.Refresh();
        }

        private void DisplayCurrTemp(UInt16 amb, UInt16 xcvr, UInt16 pa)
        {
            AmbTempLbl.Text = amb.ToString();
            XcvrTempLbl.Text = xcvr.ToString();
            PATempLbl.Text = pa.ToString();
        }

        private void DisplayTempThreshold(UInt16 xcvr)
        {
            LimitLbl.Text = xcvr.ToString();
        }
        #endregion

        #region F11/F4/F5 HotKey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11:
                    if (down)
                    {
                        // fake 'Start' key press if not already running
                        if (StartButtonState() == OpState.Stopped)
                        {
                            OnStartButtonClicked(this, null);
                        }
                    }
                    else // up
                    {
                        if (StartButtonState() == OpState.Started
                            || StartButtonState() == OpState.Starting)
                        {
                            // Stop!
                            OnStartButtonClicked(this, null);
                        }
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (down)
                    {
                        if (StartButtonState() == OpState.Stopped)
                        {
                            AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                        }
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && StartButtonState() == OpState.Stopped)
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }

        }
        #endregion

        #region Common Tag Event Handlers
        //bool firstTag = true;
        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {

            //while (inProcess)
            //{
            //    System.Threading.Thread.Sleep(100);
            //}
            //if (SearchTagList.IndexOf(e.EPC.ToString()) > 0)
            //    {
            //        //Check e.EPC here, if it is location tag, set the location combo, if it is not set, eles skip the tag.
            //        TagInfo T = new TagInfo(e.EPC.ToString());
            //        if (CheckedTag.IndexOf(e.EPC.ToString())<=0 && T.isLocationTag())  //If Tag is assosiated with any location we will not add it in List View.
            //        {
            //            CheckedlocationTag += "," + e.EPC.ToString();
            //            if (LocationRSSI > e.RSSI)
            //            {
            //                LocationRSSI = e.RSSI;
            //                if (Convert.ToInt32(cboLoc.SelectedValue) <= 0) 
            //                    cboLoc.SelectedValue = T.DataKey;
            //            }
            //            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            //        }
            //        else
            //        {
            //            if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
            //            {
            //                // New Tag (Note: missing CRC data)
            //                CheckedTag += "," + e.EPC.ToString();
            //                ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC);
            //                RefreshTagCntLabel();

            //                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            //            }
            //            else
            //            {

            //                Application.DoEvents();
            //            }
            //        }
            //    }
            //}


            if (_SearchTagList.IndexOf(e.EPC.ToString()) > 0)
            {
                // Add new TagID to Memory List for now. ListView would be
                // updated periodically

                if (!scannedTags.Contains(e.EPC))
                    scannedTags.Add(e.EPC);

                lock (tagListLock)
                {
                    tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI));
                }
                AddTagEvtCntIncr();
                tagEvtPerSec++;
            }
            else
            {
                //AddTagEvtCntIncr();
                //tagEvtPerSec++;

                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.NotValid));

            }

#if false
            // Test only
            if (firstTag)
            {
                RFIDRdr.GetInstance().TagInvtryStop();
                firstTag = false;
            }
#endif
        }
        #endregion

        #region Non-DC Tag Event Handlers
        //private int invtryCycRan = 0;
        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState state = StartButtonState();
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();

            switch (e.status)
            {
                case InvtryOpStatus.started:
                    Program.RefreshStatusLabel(this, "Running...");
                    SetStartButtonState(OpState.Started);
                    if (DutyCycleRequired())
                    {
                        DCStateSet(OpState.Started);
                        // Replace Inventory Status Handler until user-request stop
                        //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //rdr.InventoryOpStEvent += DCInvtryOpEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        rdr.RegisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                    }
#if BURN_IN
                    AliveTmrStart();
#endif
                    tagEvtPerSec = 0;
                    TagRateTimerStart();
                    ListVUpdateTmr.Enabled = true;
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    // Assuming that Duty-Cycle is not being used
                    //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                    //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
#if BURN_IN
                    AliveTmrStop();
#endif
                    TagRateTimerStop();
                    ListVUpdateTmr.Enabled = false;
                    UpdateListViewFromTagList();
                    bool ResetToStopStatus = true;
                    switch (e.status)
                    {
                        case InvtryOpStatus.stopped:
                            if (StartButtonState() != OpState.Stopping)
                            {
                                // involuntary stop, re-start again (needs further investigation)
                                Datalog.LogErr("Tag Ranging stop for no reason.");
                                TempMonitor TempMon = TempMonitor.GetInstance();
                                Datalog.LogErr("Temp: " + TempMon.LastRecAmbTemp + ", "
                                    + TempMon.LastRecXcvrTemp + ", " + TempMon.LastRecPATemp);
                                Program.RefreshStatusLabel(this, "Restarting in 2 secs...");
                                InvtryRestartTmrStart(2);
                                ResetToStopStatus = false;
                            }
                            break;
                        case InvtryOpStatus.errorStopped:
                            if (rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                                RestartRfidDevice();
                            else
                                Program.ShowError("Tag Ranging Stopped with Error: " + e.msg);
                            break;
                        case InvtryOpStatus.macErrorStopped:
                            //ushort MacErrCode = 0;
                            Reader.macerr = 0;
                            if (rdr.GetMacError() && Reader.macerr != 0)
                            {
                                if (rdr.MacErrorIsOverheat())
                                {
                                    OverTempAlertStart();
                                    Program.RefreshStatusLabel(this, "Over Temp! Restarting in 5 min...");
                                    OverTempLogMessage();
                                    InvtryRestartTmrStart(5 * 60);
                                    TempMonitor.GetInstance().PeriodicTempGetStart(3); // until Restart Timer Expired
                                    ResetToStopStatus = false;
                                }
                                else if (!rdr.MacErrorIsFatal())
                                {
                                    if (StartButtonState() != OpState.Stopping)
                                    {
#if BURN_IN
                                        // Attempt to re-start inventory after fatal error
                                        Program.ShowWarning("Tag Inventory stopped by error: " + e.msg);
                                        Program.RefreshStatusLabel(this, "Restarting in 3 secs...");
                                        InvtryRestartTmrStart(3);
                                        ResetToStopStatus = false;
#else
                                        Program.ShowError("Tag Inventory stopped. Hardware (mac) error code: " + Reader.macerr.ToString("F"));
#endif
                                    }
                                    // Do not display error message when mac error is not direct cause
                                    // of this stop. (user-stop)
                                }
                                else // Fatal Mac Error
                                {
                                    Program.ShowError(e.msg);
                                }
                            }
                            else
                                Program.ShowError("Unknown HW error. Abort");
                            break;
                    }
                    if (ResetToStopStatus)
                    {
                        // restore StartButton
                        SetStartButtonState(OpState.Stopped);
                        // Display total Time elapsed
                        PerfCounterStop();
                        Program.RefreshStatusLabel(this, "Done " + PerfCounterGetSecs() + "s taken");
                    }
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (state)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
#if BURN_IN
                    AliveTmrStop();
#endif
                    TagRateTimerStop();
                    ListVUpdateTmr.Enabled = false;
                    break;
                case InvtryOpStatus.cycBegin:
                    //invtryCycRan++;
                    //Program.RefreshStatusLabel(this, "InvCyc " + invtryCycRan + " Begins");
                    break;
                case InvtryOpStatus.cycEnd:
                    //Program.RefreshStatusLabel(this, "InvCyc " + invtryCycRan + " Ends");
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Tag Rate Timer
        private long tagRateStartTick = 0;
        private long tagRateEndTick = 0;

        private void TagRateTimerStart()
        {
            tagRateStartTick = DateTime.Now.Ticks;
            ListVUpdateTmr.Enabled = true;
        }

        private void TagRateTimerStop()
        {
            tagRateEndTick = DateTime.Now.Ticks;
            ListVUpdateTmr.Enabled = false;
        }

        private double TagRateTimerGetSecs()
        {
            return ((double)(tagRateEndTick - tagRateStartTick) / (double)TimeSpan.TicksPerSecond);
        }

        #endregion

        #region Tag Perf. Counter
        private long startTick = 0;
        private long endTick = 0;
        private int AddTagEvtCnt = 0;

        private void PerfCounterStart()
        {
            startTick = DateTime.Now.Ticks;
            endTick = startTick;
        }

        private void PerfCounterStop()
        {
            endTick = DateTime.Now.Ticks;
        }

        private long PerfCounterGetSecs()
        {
            return (endTick - startTick) / TimeSpan.TicksPerSecond;
        }

        private void AddTagEvtCntReset()
        {
            AddTagEvtCnt = 0;
            TagObsrvCntLbl.Text = AddTagEvtCnt.ToString();
            TagObsrvCntLbl.Update();
        }

        private void AddTagEvtCntIncr()
        {
            AddTagEvtCnt++;
        }

        private void RefreshAddTagEvtCntLbl()
        {
            TagObsrvCntLbl.Text = AddTagEvtCnt.ToString();
            TagObsrvCntLbl.Update();
        }

        #endregion

        #region Ranging/InvtryOp entry functions
        private void TagRangingAbort()
        {
            OpState state = StartButtonState();

            // Only perform abort when operation is running
            if (state == OpState.Started)
            {
                SetStartButtonState(OpState.Stopping);
                Program.RefreshStatusLabel(this, "Aborting...");
                //RFIDRdr rdr = RFIDRdr.GetInstance();
                Reader rdr = ReaderFactory.GetReader();
                if (!rdr.TagInventoryAbort())
                {
                    MessageBox.Show("Tag Ranging Abort Failed", "Error");
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                }

            }
        }
        #endregion

        #region Form Load/Closing/Closed
        private void OnFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);

            // Temp Update event
            // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //Rdr.TempUpdateEvt -= TempUpdateEvtHandler;
            Rdr.UnregisterTempUpdateEvt(TempUpdateEvtHandler);

        }

        private void AddTagData(String TagID)
        {
            Assets A = new Assets(TagID);
            TagData.Add(TagID, A.Name);
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            TagData = new Hashtable();
            String spr = ",";
            EPCListV.BeginUpdate();
            //foreach (String tgno in _SearchTagList.Split(spr.ToCharArray()))
            //{
            //    if (tgno.Trim().Length != 0)
            //    {
            //        ListViewItem Row;
            //        UINT96_T EPC = new UINT96_T();
            //        EPC.ParseString(tgno.ToString().Trim());
            //        if (EPC.ToString() != "000000000000000000000000")
            //        {
            //            UInt16 cnt = 0;
            //            float rssi = 0;
            //            TagListItem ti = new TagListItem(30, EPC, rssi);

            //            tagList.Add(ti);
            //            Row = AddEPCToListV(EPC, cnt, rssi,"");
            //            tagList.SetDisplayListRow(ti, Row);
            //            ti.Updated = TagListItem.UpdatedFlg.NoChange; // reset state
            //        }
            //    }

            //}



            string tgNo, assetName;
            StringBuilder csvData = new StringBuilder("-1");

            foreach (DataRow dr in dtSelectedItems.Rows)
            {
                if (dr["TagID"] != DBNull.Value)
                {
                    tgNo = dr["TagID"].ToString().Trim();
                }
                else
                {
                    tgNo = "";
                }

                if (dr["Name"] != DBNull.Value)
                {
                    assetName = dr["Name"].ToString().Trim();
                }
                else
                {
                    assetName = "NA";
                }

                if (tgNo.Length != 0)
                {
                    ListViewItem Row;
                    UINT96_T EPC = new UINT96_T();
                    EPC.ParseString(tgNo);
                    if (EPC.ToString() != "000000000000000000000000")
                    {
                        UInt16 cnt = 0;
                        float rssi = 0;
                        TagListItem ti = new TagListItem(30, EPC, rssi);
                        tagList.Add(ti);
                        Row = AddEPCToListV(EPC, cnt, rssi, assetName);
                        tagList.SetDisplayListRow(ti, Row);
                        ti.Updated = TagListItem.UpdatedFlg.NoChange; // reset state
                        csvData.Append("," + tgNo);
                    }
                }
            }

            EPCListV.EndUpdate();

            Cursor.Current = Cursors.Default;

            _SearchTagList = csvData.ToString();

            // Set up to receive HotKey event
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);

            // Warn user here instead of OnStartButtonClicked() because
            // the OnStartButtonClicked could be called (dirty hack) even
            // when user not pressing the button/pistol-trigger
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            UInt32 CurLnkPrf = 0;
            Rdr.ProfNum = CurLnkPrf;
            if (Rdr.LinkProfNumGet() && CurLnkPrf == 4)
            {
                Program.ShowWarning("Currently active Link Profile is #4. "
                + "High tag rate could make user interface irresponsive");
            }
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = false;

            if (StartButtonState() != OpState.Stopped)
            {
#if false
                DialogResult yesno = Program.AskUserConfirm("Abort Operation would require restarting application.\r\n Are you sure?");
                if (yesno == DialogResult.OK)
                {
                    TagRangingAbort();
                }
#else
                Program.ShowWarning("Tag Ranging operation still running!");
#endif
                e.Cancel = true;
            }
        }
        #endregion

        private void OnStartButtonClicked(object sender, EventArgs e)
        {
            Program.RefreshStatusLabel(this, null);
            OpState state = StartButtonState();

            // start or stop operation
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            switch (state)
            {
                case OpState.Started:
                    // dispatch stop request//TestStop();
                    // disable button until actually stopped
                    SetStartButtonState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    bool IssueStopCmd = false;
                    if (DutyCycleRequired())
                    {
                        OpState DCState = DCStateGet();
                        switch (DCState)
                        {
                            case OpState.Stopped: // In Off-Cycle
                                dcTmr.OffCycCancel();
                                SetStartButtonState(OpState.Stopped);
                                Program.RefreshStatusLabel(this, "Stopped");
                                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                                break;
                            case OpState.Started: // In On-Cycle
                                dcTmr.OnCycCancel();
                                // Replace handler with InvtryOpEvtHandler from this point on
                                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                //rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                                IssueStopCmd = true;
                                break;
                            case OpState.Starting:
                            case OpState.Stopping:
                                // cannot do anything, wait for DCState machine to check
                                // the StartButtonState() for hint
                                break;
                        }
                    }
                    else if (IntryRestartTmrRunning())
                    {
                        // AutoRestart Timer 
                        InvtryRestartTmrStop();
                        SetStartButtonState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Stopped");
                        IssueStopCmd = false;
                    }
                    else
                        IssueStopCmd = true;
                    if (IssueStopCmd)
                    {
                        if (!rdr.TagInventoryStop())
                        {
                            MessageBox.Show("Tag Inventory Stop Failed", "Error");
                            SetStartButtonState(OpState.Stopped);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        }
                    }
                    break;
                case OpState.Stopped:
                    scannedTags = new ArrayList();
                    AddTagEvtCntReset();
                    PerfCounterStart();
                    // get event handler ready
                    //rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                    //rdr.DscvrTagEvent += new EventHandler<DscvrTagEventArgs>(DscvrTagEvtHandler);
                    rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                    rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
#if CLEAR_INVTRYLIST_DISP_ON_START
                    // Clear List
                    ClearEPCListV();
#endif
                    // dispatch start request //TestStart();
                    // disable button until actually started
                    SetStartButtonState(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting...");
                    if (!rdr.SetRepeatedTagObsrvMode(true))
                        // Continue despite error
                        MessageBox.Show("Enable Repeated Tag Observation mode failed", "Error");
                    LogOperStartTemp();
                    UserPref Pref = UserPref.GetInstance();
                    if (!rdr.TagRangeStart())
                    {
                        MessageBox.Show("Tag Inventory Start Failed", "Error");
                        SetStartButtonState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        PerfCounterStop();
                    }
                    break;
                case OpState.Starting:
                    Program.RefreshStatusLabel(this, "Stopping...");
                    SetStartButtonState(OpState.Stopping);
                    if (!rdr.TagInventoryStop())
                    {
                        // Restore
                        Program.RefreshStatusLabel(this, "Starting...");
                        SetStartButtonState(OpState.Starting);
                        Program.ShowWarning("Tag Range Stop failed during Starting phase, please try again");
                    }
                    break;
                default:
                    // unexpected
                    break;
            } // switch
        }

        private void OnClrButtonClicked(object sender, EventArgs e)
        {
            // Clear UI
            ClearEPCListV();
            lock (tagListLock)
            {
                foreach (TagListItem item in tagList)
                {
                    tagList.SetDisplayListRow(item, null); // indicate that it's not in EPCListV
                    item.Updated = TagListItem.UpdatedFlg.NoChange; // reset
                }
            }
        }

        #region Duty Cycle Routines
        private bool DutyCycleRequired()
        {
            UserPref Pref = UserPref.GetInstance();

            return ((Pref.DCOnDuration != 0) && (Pref.DCOffDuration != 0));
        }

        private void DutyCycInit()
        {
            UserPref Pref = UserPref.GetInstance();
            this.dcTmr = new DutyCycleTmr(this.DutyCycTmr, (int)Pref.DCOnDuration, (int)Pref.DCOffDuration);
        }

        private void DCInvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started: // from the 2nd on-cycle starts
                    if (StartButtonState() == OpState.Stopping)
                    {
                        // User Requested stop during our DC Starting Phase
                        dcTmr.OnCycCancel(); // if already set up
                        // User Stop Request Pending
                        DCStateSet(OpState.Stopping);
                        // Replace InvtryOpState handler with InvtryOpEvtHandler
                        //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                        //rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                        rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                        if (!rdr.TagInventoryStop())
                        {
                            MessageBox.Show("Tag Inventory Stop Failed", "Error");
                            DCStateSet(OpState.Stopped);
                            SetStartButtonState(OpState.Stopped);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        }
                    }
                    else
                    {
                        DCStateSet(OpState.Started);
                        Program.RefreshStatusLabel(this, "Running(On-Cycle)");
#if BURN_IN
                    AliveTmrStart();
#endif
                        tagEvtPerSec = 0;
                        TagRateTimerStart();
                        ListVUpdateTmr.Enabled = true;
                    }
                    break;
                case InvtryOpStatus.error:
                    switch (DCStateGet())
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    DCStateSet(OpState.Stopped);
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
                case InvtryOpStatus.stopped:
                    if (DCStateGet() != OpState.Stopping) // involuntary stop
                    {
                        // do nothing, wait until On-Cycle ends
                        Program.RefreshStatusLabel(this, "Stopped!");
                        DCStateSet(OpState.Stopped);
                    }
                    else // On-Cycle Expired Stopping  and/or User Stopping
                    {
                        DCStateSet(OpState.Stopped);
                        dcTmr.OnCycCancel();
                        if (StartButtonState() == OpState.Stopping) // user-stopping
                        {
                            // User-Requested Stop, do not restart
                            Program.RefreshStatusLabel(this, "Stopped");
                            SetStartButtonState(OpState.Stopped);
                            //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        }
                        else // on-cycle expired stopping
                        {
                            OverTempAlertStop(); // Cancel any existing alert
                            Program.RefreshStatusLabel(this, "Paused(Off Cycle)");
                            // stop before start again to make sure we got the interval correct
                            TempMonitor.GetInstance().PeriodicTempGetStop(); // started from OverHeat
                            TempMonitor.GetInstance().PeriodicTempGetStart(3); // until offCycEnds
                            dcTmr.OffCycStart(DutyOffCycExpired);

                        }
                    }
#if BURN_IN
                    AliveTmrStop();
#endif
                    TagRateTimerStop();
                    ListVUpdateTmr.Enabled = false;
                    break;
                case InvtryOpStatus.errorStopped:
                    DCStateSet(OpState.Stopped);
                    dcTmr.OnCycCancel();
                    //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                    //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                    rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    if (rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                        RestartRfidDevice();
                    else
                    {
#if BURN_IN
                        Program.ShowWarning("Tag Ranging Stopped with Error: " + e.msg);
#else
                        MessageBox.Show(e.msg, "Tag Ranging Stopped with Error");
#endif
                    }
                    break;
                case InvtryOpStatus.macErrorStopped:
                    // ushort MacErrCode;
                    if (rdr.GetMacError() && Reader.macerr != 0)
                    {
                        if (DCStateGet() != OpState.Stopping) // involuntary stop
                        {
                            // if overheat,  flash error and wait til on-cycle ends
                            if (rdr.MacErrorIsOverheat())
                            {
                                OverTempAlertStart();
                                Program.RefreshStatusLabel(this, "Over Temp!");
                                OverTempLogMessage();
                                TempMonitor.GetInstance().PeriodicTempGetStart(3); // until On-Cycle ends
                            }
                            // if fatal error, stop everything
                            else if (rdr.MacErrorIsFatal())
                            {
                                dcTmr.OnCycCancel();
                                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                                Program.RefreshStatusLabel(this, "Stopped");
                                SetStartButtonState(OpState.Stopped);
                                Program.ShowError(e.msg);
                            }
                            else
                            {
                                // otherwise, wait til on-cycle ends
                                Program.RefreshStatusLabel(this, "HW error stopped");
                                Program.ShowWarning("Non-fatal HW error : " + Reader.macerr.ToString("X4"));
                            }
                            DCStateSet(OpState.Stopped);
                        }
                        else // On-Cycle Expired Stopping  and/or User Stopping
                        {
                            DCStateSet(OpState.Stopped);
                            dcTmr.OnCycCancel();
                            // if User Stopping, do not show error unless it's fatal
                            if (StartButtonState() == OpState.Stopping) // user-stopping
                            {
                                // User-Requested Stop, do not restart
                                Program.RefreshStatusLabel(this, "Stopped");
                                SetStartButtonState(OpState.Stopped);
                                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                                if (rdr.MacErrorIsFatal())
                                    Program.ShowError(e.msg);
                            }
                            else  // if on-Cycle expired stopping, not show error unless it's fatal and do not restart.
                            {
                                OverTempAlertStop(); // Cancel any existing alert
                                if (rdr.MacErrorIsFatal())
                                {
                                    Program.RefreshStatusLabel(this, "Stopped");
                                    SetStartButtonState(OpState.Stopped);
                                    //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                    //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                                    rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                    rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                                    Program.ShowError(e.msg);
                                }
                                else
                                {
                                    Program.RefreshStatusLabel(this, "Paused(Off Cycle)");
                                    // stop before start again to make sure we got the interval correct
                                    TempMonitor.GetInstance().PeriodicTempGetStop(); // started from OverHeat
                                    TempMonitor.GetInstance().PeriodicTempGetStart(3); // until offCycEnds
                                    dcTmr.OffCycStart(DutyOffCycExpired);
                                }
                            }
                        }
                    }
                    else
                    {
                        dcTmr.OnCycCancel();
                        //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                        //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                        rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        SetStartButtonState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "HW Error Stopped");
                        Program.ShowError("Unknown HW error. Abort");
                    }
                    break;
                case InvtryOpStatus.intervalTimeRpt:
                    UserPref Pref = UserPref.GetInstance();
                    switch (DCStateGet())
                    {
                        case OpState.Starting:
                        case OpState.Stopping:
                        case OpState.Stopped:
                            // ignore this then
                            break;
                        case OpState.Started:
                            if (e.ElapsedTime > (Pref.DCOnDuration * 60 * 1000))
                            {
                                // this comes earlier than the Tick?
                                dcTmr.OnCycCancel();
                                // Go to Off-Cycle
                                DutyOffCycStart();
                                //                             MessageBox.Show("On Cycle Expired (MAC timer)", "DEBUG ONLY(To-be-Removed)");
                            }
                            break;
                    }
                    break;
            }
        }


        private void DutyOffCycStart()
        {
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            if (DCStateGet() == OpState.Started)
            {
                DCStateSet(OpState.Stopping);
                Program.RefreshStatusLabel(this, "Going into Off-Cycle");
                if (!rdr.TagInventoryStop())
                {
                    MessageBox.Show("Tag Inventory Stop Failed", "Error");
                    DCStateSet(OpState.Stopped);
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                    //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                    rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                }
            }
            else if (DCStateGet() == OpState.Stopped)
            {
                DCStateSet(OpState.Stopping); // fake
                Program.RefreshStatusLabel(this, "Going into Off-Cycle");
                DCInvtryOpEvtHandler(this, new InvtryOpEventArgs(InvtryOpStatus.stopped));
            }
        }

        private void DutyOnCycStart()
        {
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            DCStateSet(OpState.Starting);
            Program.RefreshStatusLabel(this, "Going into On-Cycle...");
            LogOperStartTemp();
            UserPref Pref = UserPref.GetInstance();
            // if (!rdr.TagRangeStart(Pref.InvtrySession, Pref.QSize, Pref.QAlgo))
            if (!rdr.TagRangeStart())
            {
                MessageBox.Show("Tag Inventory Start Failed", "Error");
                // rewind previous setup
                SetStartButtonState(OpState.Stopped);
                Program.RefreshStatusLabel(this, "Error Stopped");
                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
            }
        }

        private void DutyOnCycExpired(Timer tmr)
        {
            OpState state = StartButtonState();
            switch (state)
            {
                case OpState.Started:
                    DutyOffCycStart();
                    break;
                case OpState.Stopping:
                case OpState.Stopped:
                case OpState.Starting:
                    throw new ApplicationException("Unexpected DCState: " + state.ToString()
                        + "in DutyOnCycExpired()");
            }
        }

        private void DutyOffCycExpired(Timer tmr)
        {
            TempMonitor.GetInstance().PeriodicTempGetStop(); // started from OffCycle
            switch (DCStateGet())
            {
                case OpState.Stopping:
                case OpState.Started:
                case OpState.Starting:
                    throw new ApplicationException("Unexpected DCState: " + DCStateGet().ToString()
                    + " in DutyOffCycExpired()");
                case OpState.Stopped:
                    DutyOnCycStart();
                    break;
            }
        }

        #endregion

        #region Temperature Routines
        bool OverTempAlertStarted = false;
        private void OverTempAlertStart()
        {
            TempMonitor TempMon = TempMonitor.GetInstance();
            if (!TempMon.OverHeatFlashAlertStart(this, EPCListV))
            {
                MessageBox.Show("Temp Alert Failed to Start");
            }
            else
                OverTempAlertStarted = true;

        }

        private void OverTempAlertStop()
        {
            if (OverTempAlertStarted)
            {
                TempMonitor TempMon = TempMonitor.GetInstance();
                TempMon.OverHeatFlashAlertStop();
            }
        }

        // Note: this not only serves as handling temperature data
        // coming from inventory. It also receives updates from
        // RFIDRdr.GetCurrTemp(...)
        private void TempUpdateEvtHandler(object sender, TempUpdateEventArgs e)
        {
            DisplayCurrTemp(e.Amb, e.Xcvr, e.PA);
        }

        private void OverTempLogMessage()
        {
            Datalog.LogErr(DateTime.Now.ToShortTimeString() + ":" + " Tag Ranging OverTemp at "
                + TempMonitor.GetInstance().LastRecXcvrTemp + "(Xcvr)"
                + " " + TempMonitor.GetInstance().LastRecPATemp + "(PA)");
        }

        #endregion

        #region InvtryRestart Timer (For continous inventory operation without DC)
        private System.Windows.Forms.Timer invtryRestartTmr;

        private void InvtryRestartTmrInit()
        {
            if (invtryRestartTmr == null)
            {
                invtryRestartTmr = new System.Windows.Forms.Timer();
                invtryRestartTmr.Tick += OnInvtryRestartTmrTick;
            }
        }

        private bool IntryRestartTmrRunning()
        {
            return (invtryRestartTmr != null) && (invtryRestartTmr.Enabled);
        }

        private void InvtryRestartTmrStop()
        {
            invtryRestartTmr.Enabled = false;
        }

        // return false if Timer is already running
        private bool InvtryRestartTmrStart(int secs)
        {
            bool Succ = false;

            if (invtryRestartTmr.Enabled == false)
            {
                invtryRestartTmr.Interval = secs * 1000;
                invtryRestartTmr.Enabled = true;
                Succ = true;
            }
            else
                Succ = false;

            return Succ;
        }

        private void OnInvtryRestartTmrTick(object sender, EventArgs e)
        {
            InvtryRestartTmrStop();// one shot
            TempMonitor.GetInstance().PeriodicTempGetStop(); // Started from RestartTmr

            switch (StartButtonState())
            {
                case OpState.Stopped:
                case OpState.Stopping:
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Stopped");
                    break;
                case OpState.Started:
                    //RFIDRdr Rdr = RFIDRdr.GetInstance();
                    Reader Rdr = ReaderFactory.GetReader();
                    // ushort amb, xcvr, pamp;
                    // Although the temperature returns here synchronously as
                    // 'out' parameter. We simply rely on the RFIDRdr.TempUpdateEvt
                    // handler to display the update. (lazy coding)
                    Rdr.GetCurrTemp(); // Reload Temp
                    SetStartButtonState(OpState.Stopped); // this allows the start button click handler to perform start
                    OnStartButtonClicked(this, null);
                    break;
                default:
                    MessageBox.Show("Unable to restart, Opstate is " + StartButtonState().ToString("F"));
                    break;
            }
        }
        #endregion

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        static void LogOperStartTemp()
        {
            TempMonitor TempMon = TempMonitor.GetInstance();

            Datalog.LogStr(String.Format("Tag Ranging Starts.  Amb Temp: {0}; Xcvr Temp: {1}; PA Temp: {2}", TempMon.LastRecAmbTemp, TempMon.LastRecXcvrTemp, TempMon.LastRecPATemp));
        }

        #region Alive Timer (Burn-in)
        private void AliveTmrStart()
        {
            AliveTmrLastTagEvtCnt = AddTagEvtCnt;
            AliveTmr.Enabled = true;
            Datalog.LogStr("Alive Timer (ranging) started.");
        }

        private void AliveTmrStop()
        {
            AliveTmr.Enabled = false;
            Datalog.LogStr("Alive Timer (ranging) stopped.");
        }

        private int AliveTmrLastTagEvtCnt = 0;
        private void OnAliveTmrTick(object sender, EventArgs e)
        {
            Datalog.LogStr("Alive Timer (ranging): " +
                (AddTagEvtCnt - AliveTmrLastTagEvtCnt) + " tag events since last recorded.");
            AliveTmrLastTagEvtCnt = AddTagEvtCnt;
        }
        #endregion

        #region Tag Range List
        TagRangeList tagList = null;
        #endregion

        int tagEvtPerSec = 0;

        bool inProcess = false;

        // Periodically update ListView
        private void OnListViewUpdateTmrTick(object sender, EventArgs e)
        {
            //if (inProcess)
            //    return;

            TagRateTimerStop();
            //Add the Color change code here. Deepanshu

            ArrayList scannedTemp = new ArrayList();
            scannedTemp.AddRange(scannedTags);
            scannedTags.Clear();

          //  List<TagListItem> list = null;

            lock (tagListLock)
            {
                //list = new List<TagListItem>(tagList.TagList);

                foreach (TagListItem tlItem in tagList.TagList)
                {
                    if (!scannedTemp.Contains(tlItem.EPC))
                    {
                        tlItem.RSSI = 0;
                        tlItem.Updated = TagListItem.UpdatedFlg.Updated;
                    }
                }
            }
            //Double SecondMaxRSSI = 0;
            //Int32 mRow = 0;
            //Int32 colorRow = 0;
            //foreach (ListViewItem LI in EPCListV.Items)
            //{
            //    //if (Convert.ToDouble(LI.SubItems[2].Text) > MaxRSSI)
            //    //{
            //    //    MaxRSSI = Convert.ToDouble(LI.SubItems[2].Text);
            //    //    LI.Selected = true;
            //    //    LI.BackColor = Color.Green;  
            //    //}
            //    LI.BackColor = Color.White; 
            //    if (Convert.ToDouble(LI.SubItems[Convert.ToInt32(dispCol.RSSI)].Text) > SecondMaxRSSI)
            //    {
            //        SecondMaxRSSI = Convert.ToDouble(LI.SubItems[Convert.ToInt32(dispCol.RSSI)].Text);
            //        colorRow = mRow; 
            //    }
            //    mRow++; 
            //}

            //if (EPCListV.Items.Count > 0)
            //{
            //    MaxRSSI = SecondMaxRSSI; 
            //    if (SecondMaxRSSI>0) 
            //        EPCListV.Items[colorRow].BackColor = Color.Green;
            //    RssiMelodyGenerate(SecondMaxRSSI);
            //}
            //EPCListV.Refresh();  

            ////////////////////////        

            bool ListViewChanged = UpdateListViewFromTagList() > 0;

            int TagsPerSec = 0;
            try
            {
                TagsPerSec = Convert.ToInt32(Convert.ToDouble(tagEvtPerSec) / TagRateTimerGetSecs());
            }
            catch
            {
                TagsPerSec = 0;
            }
            finally
            {
                tagEvtPerSec = 0; // reset
                TagRateTimerStart();
            }

            RefreshAddTagEvtCntLbl();
            Program.RefreshStatusLabel(this, TagsPerSec + " Tags/sec");
            // if (ListViewChanged)
            // TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        }

        float maxRSSIval;
        private int UpdateListViewFromTagList()
        {
            inProcess = true;
            int NumTagsUpdated = 0;

            try
            {
                EPCListV.BeginUpdate();
                // go through all the added and updated tags

                //   TagAddedList TagsAdded = new TagAddedList(tagList);

               // List<TagListItem> list = null;
                ListViewItem Row = null;


                maxRSSIval = 0;
                ListViewItem RowtoColor = null;

                lock (tagListLock)
                {
                    // list = new List<TagListItem>(tagList.TagList); 

                    //
                    // Note: Items from TagsAddedList or TagsUpdatedList does not
                    // represent state in EPCListV, therefore, needs to check the
                    // return value of GetDisplayListPos
                    //
                    foreach (TagListItem item in tagList.TagList)
                    {
                        Row = tagList.GetDisplayListRow(item);
                        if (Row == null)
                        {
                            Assets A = new Assets(item.EPC.ToString());
                            Row = AddEPCToListV(item.EPC, (ushort)item.Cnt, item.RSSI, A.Name);
                        }
                        else
                            UpdateRangeFldsAtListV(ref Row, item.EPC, (ushort)item.Cnt, item.RSSI);
                        tagList.SetDisplayListRow(item, Row);
                        item.Updated = TagListItem.UpdatedFlg.NoChange; // reset state
                        NumTagsUpdated++;
                        Row.BackColor = Color.White;
                        if (maxRSSIval < item.RSSI)
                        {
                            maxRSSIval = item.RSSI;
                            RowtoColor = Row;
                        }
                    }

                }
                //TagUpdatedList TagsUpdated = new TagUpdatedList(tagList);

                //foreach (TagListItem item in TagsUpdated)
                //{
                //    Row = tagList.GetDisplayListRow(item);
                //    if (Row == null)
                //    {
                //        Assets A = new Assets(item.EPC.ToString());
                //        Row = AddEPCToListV(item.EPC, (ushort)item.Cnt, item.RSSI, A.Name);
                //    }
                //    else
                //        UpdateRangeFldsAtListV(ref Row, item.EPC, (ushort)item.Cnt, item.RSSI);
                //    tagList.SetDisplayListRow(item, Row);
                //    item.Updated = TagListItem.UpdatedFlg.NoChange; // reset state

                //    Row.BackColor = Color.White;
                //    if (maxRSSIval < item.RSSI)
                //    {
                //        maxRSSIval = item.RSSI;
                //        RowtoColor = Row;
                //    }

                //    NumTagsUpdated++;
                //}

                if (RowtoColor != null)
                {
                    RowtoColor.BackColor = Color.Green;
                }

                EPCListV.EndUpdate();
                Application.DoEvents();
                if (maxRSSIval > 0)
                    RssiMelodyGenerate(maxRSSIval);
                else
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));

            }
            catch
            {
            }
            finally
            {
                inProcess = false;
            }

            return NumTagsUpdated;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (EPCListV.SelectedIndices.Count > 0)
            {
                String tagNo = EPCListV.Items[EPCListV.SelectedIndices[0]].SubItems[Convert.ToInt32(dispCol.TagID)].Text;
                String assetName = EPCListV.Items[EPCListV.SelectedIndices[0]].SubItems[Convert.ToInt32(dispCol.AssetName)].Text;
                if (tagNo.Trim().Length != 0)
                {
                    // Open new Tag Search Window
                    TagSearchForm TagSrchFm = new TagSearchForm();
                    TagSrchFm.SearchTagNo = tagNo.Trim();
                    TagSrchFm.AssetName = assetName;
                    TagSrchFm.Show();
                    this.Enabled = false;
                    TagSrchFm.Closed += new EventHandler(this.OnOperFrmClosed);
                }
            }
            else
            {
                MessageBox.Show("Please select any item.");
            }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void DutyCycTmr_Tick(object sender, EventArgs e)
        {

        }


        //Added to generate Sound and Melody
        #region Search-by-RSSI Routines
        // Relies on Tag Inventory
        //private void TagRssiInvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        //{
        //    OpState CurState = SrchBttnState();
        //    RFIDRdr Rdr = RFIDRdr.GetInstance();

        //    switch (e.status)
        //    {
        //        case InvtryOpStatus.started:
        //            Program.RefreshStatusLabel(this, "Searching...");
        //            SrchBttnSetState(OpState.Running);
        //            break;
        //        case InvtryOpStatus.stopped:
        //        case InvtryOpStatus.errorStopped:
        //        case InvtryOpStatus.macErrorStopped:
        //            StopOORDetectTmr();
        //            RssiLbl.Text = String.Empty;
        //            Rdr.TagRssiEvent -= TagRssiEvtHandler;
        //            Rdr.InvtryOpStEvent -= TagRssiInvtryOpEvtHandler;
        //            if (e.status == InvtryOpStatus.errorStopped)
        //            {
        //                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
        //                    RestartRfidDevice();
        //                else
        //                    MessageBox.Show(e.msg, "Tag Search Stopped with Error");
        //            }
        //            else if (e.status == InvtryOpStatus.macErrorStopped)
        //            {
        //                ushort MacErrCode;
        //                if (Rdr.GetMacError(out MacErrCode) && MacErrCode != 0)
        //                {
        //                    // Show error if fatal
        //                    if (RFIDRdr.MacErrorIsFatal(MacErrCode))
        //                    {
        //                        Program.ShowError(e.msg);
        //                    }
        //                    // Show message if non-fatal and non-user-stop
        //                    else if (SrchBttnState() != OpState.Stopping)
        //                    {
        //                        Program.ShowError(e.msg);
        //                    }
        //                }
        //                else
        //                    Program.ShowError("Unknown hardware (mac) error");
        //            }
        //            // restore Search Button
        //            SrchBttnSetState(OpState.Idle);
        //            Program.RefreshStatusLabel(this, "Ready");
        //            break;
        //        case InvtryOpStatus.error:
        //            switch (CurState)
        //            {
        //                case OpState.Starting:
        //                    MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
        //                    Rdr.TagRssiEvent -= TagRssiEvtHandler;
        //                    Rdr.InvtryOpStEvent -= TagRssiInvtryOpEvtHandler;
        //                    break;
        //                case OpState.Running:
        //                    // TBD
        //                    MessageBox.Show(e.msg, "Tag Search Error");
        //                    break;
        //                case OpState.Stopping:
        //                    MessageBox.Show(e.msg, "Tag Search Stop Error");
        //                    Rdr.TagRssiEvent -= TagRssiEvtHandler;
        //                    Rdr.InvtryOpStEvent -= TagRssiInvtryOpEvtHandler;
        //                    break;
        //                case OpState.Idle:
        //                    throw new ApplicationException("Unexpected error return during Idle State");
        //            }
        //            SrchBttnSetState(OpState.Idle);
        //            Program.RefreshStatusLabel(this, "Error Stopped");
        //            break;
        //    }
        //}

        // RSSI information updates
        private void RssiMelodyGenerate(Double RSSIval)
        {
            try
            {

                for (int i = (NumThresholdLvls - 1); i >= 1; i--)
                {
                    if (RSSIval >= RssiLvlThresholds[i])
                    {

                        //FlashBorder();
                        //#if DEBUG
                        //                      RFIDRdr.GetInstance().MelodyRing(RingMelody[i], UserPref.GetInstance().SndVol);
                        //#else
                        // RFIDRdr.GetInstance().MelodyRing(RingMelody[i], SoundVol.High);
                        Reader Rdr = ReaderFactory.GetReader();
                        Rdr.RingingID = RingMelody[i];
                        Rdr.Svol = SoundVol.High;
                        Rdr.MelodyRing();
                        //#endif
                        break;
                    }
                }

                //if (RSSIval < RssiLvlThresholds[0])
                //    RssiLbl.Text = String.Empty;

                //else
                //    RestartOORDetectTmr();


                //#if false // Testing Only
                //                if (firstTag)
                //                {
                //                    RFIDRdr.GetInstance().TagInvtryRssiStop();
                //                    firstTag = false;
                //                }
                //#endif
            }
            catch (ArithmeticException)
            {
                //RssiLbl.Text = "Arithmetic Exception caught";
                //RssiLbl.Refresh(); // must show to the user immediately
            }
            catch
            {
                //RssiLbl.Text = "Exception caught";
                //RssiLbl.Refresh(); // must show to the user immediately
            }
        }

        #endregion

    } // class

    public class TagListItem
    {
        public enum UpdatedFlg
        {
            NoChange,
            Added,
            Updated,
        }

        public UInt16 PC;
        public UINT96_T EPC;
        public float RSSI;
        public FileTime LastTm;
        public int Cnt;
        public UpdatedFlg Updated;
        public object UserData;

        public TagListItem(UInt16 pc, UINT96_T epc, float rssi)
        {
            PC = pc;
            EPC = epc;
            RSSI = rssi;

            Cnt = 1;
            UserData = null;
            Updated = UpdatedFlg.NoChange;
        }

        public TagListItem(UInt16 pc, UINT96_T epc, float rssi, FileTime ftime)
        {
            PC = pc;
            EPC = epc;
            RSSI = rssi;
            LastTm = ftime;

            Cnt = 1;
            UserData = null;
            Updated = UpdatedFlg.NoChange;
        }

        public bool IsSameTag(TagListItem anotherItem)
        {
            return (this.PC == anotherItem.PC && this.EPC.Equals(anotherItem.EPC));
        }
        public bool IsSameEPC(TagListItem anotherItem)
        {
            return (this.EPC.Equals(anotherItem.EPC));
        }


    }


    public class TagRangeList : IEnumerable
    {
        List<TagListItem> tagList = null;

        public TagRangeList()
        {
            tagList = new List<TagListItem>();
        }

        public List<TagListItem> TagList
        {
            get
            {
                return tagList;
            }
        }

        public bool Add(TagListItem item)
        {
            TagListItem FoundItem = null;

            for (int i = 0; i < tagList.Count; i++)
            {
                if (tagList[i].IsSameEPC(item))
                {
                    FoundItem = tagList[i];
                    break;
                }
            }
            if (FoundItem != null)
            {
                (FoundItem.Cnt)++;
                FoundItem.RSSI = item.RSSI;
                FoundItem.Updated = TagListItem.UpdatedFlg.Updated;
            }
            else
            {
                tagList.Add(item);
                item.Updated = TagListItem.UpdatedFlg.Added;
            }

            return (FoundItem == null); // brand new
        }

        public TagListItem Get(UInt16 pc, UINT96_T epc)
        {
            TagListItem Item = new TagListItem(pc, epc, 0.0F);

            for (int i = 0; i < tagList.Count; i++)
            {
                if (tagList[i].IsSameEPC(Item))
                {
                    return tagList[i];
                }
            }

            return null;
        }

        public void Clear()
        {
            tagList.Clear();
        }

        public void SetDisplayListRow(TagListItem item, ListViewItem row)
        {
            item.UserData = row;
        }

        public ListViewItem GetDisplayListRow(TagListItem item)
        {
            if (item.UserData is ListViewItem)
            {
                return (ListViewItem)item.UserData;
            }
            return null;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return tagList.GetEnumerator();
        }

        #endregion
    }

    public class TagAddedList : IEnumerable
    {
        TagRangeList tagList;

        public TagAddedList(TagRangeList tagList)
        {
            this.tagList = tagList;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return new UpdatedTagEnum(tagList.TagList, TagListItem.UpdatedFlg.Added);
        }

        #endregion
    }

    public class TagUpdatedList : IEnumerable
    {
        TagRangeList tagList = null;

        public TagUpdatedList(TagRangeList tagList)
        {
            this.tagList = tagList;
        }

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return new UpdatedTagEnum(tagList.TagList, TagListItem.UpdatedFlg.Updated);
        }

        #endregion
    }

    public class UpdatedTagEnum : IEnumerator
    {
        List<TagListItem> tagList;
        int pos = -1;
        TagListItem.UpdatedFlg enumFlg = TagListItem.UpdatedFlg.NoChange;

        public UpdatedTagEnum(List<TagListItem> tagList, TagListItem.UpdatedFlg flag)
        {
            this.tagList = tagList;
            enumFlg = flag;
            Reset();
        }

        #region IEnumerator Members

        public object Current
        {
            get
            {
                try
                {
                    return tagList[pos];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }

        }

        public bool MoveNext()
        {
            int NxtPos = pos < 0 ? 0 : pos;
            bool FoundNxt = false;
            for (int i = NxtPos; i < tagList.Count; i++)
            {
                if (tagList[i].Updated == enumFlg)
                {
                    FoundNxt = true;
                    NxtPos = i;
                    break;
                }
            }
            if (FoundNxt)
            {
                pos = NxtPos;
            }
            else
            {
                pos = -1;
            }

            return FoundNxt;
        }

        public void Reset()
        {
            pos = -1;
        }

        #endregion
    }
}