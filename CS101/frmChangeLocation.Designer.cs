﻿namespace OnRamp
{
    partial class frmChangeLocation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label1 = new System.Windows.Forms.Label();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.btnChangeLoc = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTagId = new System.Windows.Forms.TextBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.txtLocName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 19);
            this.label1.Text = "New Location";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cboLoc
            // 
            this.cboLoc.Location = new System.Drawing.Point(95, 125);
            this.cboLoc.Name = "cboLoc";
            this.cboLoc.Size = new System.Drawing.Size(190, 23);
            this.cboLoc.TabIndex = 59;
            // 
            // btnChangeLoc
            // 
            this.btnChangeLoc.Location = new System.Drawing.Point(3, 166);
            this.btnChangeLoc.Name = "btnChangeLoc";
            this.btnChangeLoc.Size = new System.Drawing.Size(111, 24);
            this.btnChangeLoc.TabIndex = 58;
            this.btnChangeLoc.Text = "Change Location";
            this.btnChangeLoc.Click += new System.EventHandler(this.btnChangeLoc_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(3, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 19);
            this.label2.Text = "Item Name";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(2, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 19);
            this.label4.Text = "Tag ID";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTagId
            // 
            this.txtTagId.Enabled = false;
            this.txtTagId.Location = new System.Drawing.Point(95, 16);
            this.txtTagId.Name = "txtTagId";
            this.txtTagId.Size = new System.Drawing.Size(190, 23);
            this.txtTagId.TabIndex = 66;
            // 
            // txtItemName
            // 
            this.txtItemName.Enabled = false;
            this.txtItemName.Location = new System.Drawing.Point(95, 50);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(190, 23);
            this.txtItemName.TabIndex = 67;
            // 
            // txtLocName
            // 
            this.txtLocName.Enabled = false;
            this.txtLocName.Location = new System.Drawing.Point(95, 87);
            this.txtLocName.Name = "txtLocName";
            this.txtLocName.Size = new System.Drawing.Size(190, 23);
            this.txtLocName.TabIndex = 70;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(3, 91);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 19);
            this.label5.Text = "Location";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // frmChangeLocation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(317, 195);
            this.Controls.Add(this.txtLocName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtItemName);
            this.Controls.Add(this.txtTagId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboLoc);
            this.Controls.Add(this.btnChangeLoc);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChangeLocation";
            this.Text = "Change Location";
            this.Load += new System.EventHandler(this.frmChangeLocation_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.Button btnChangeLoc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTagId;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.TextBox txtLocName;
        private System.Windows.Forms.Label label5;
    }
}