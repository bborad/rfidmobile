using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class AntPwrHtKyPopup : Form
    {
        private bool opRunning = false; // RfidRdr request made, but no reply yet
        private float curPwrLvl
        {
            set
            {
                AntPwrLbl.Tag = value;
                AntPwrLbl.Text = value.ToString("F1"); // one decimal place
            }
            get
            {
                float PwrLvl = (float)AntPwrLbl.Tag;
                return PwrLvl;
            }
        }
        private HandHeldHotKeyNotify callerHotKeyFunc;

        public AntPwrHtKyPopup(HandHeldHotKeyNotify callerHKNotify)
        {
            InitializeComponent();

            curPwrLvl = 0; // initialization

            callerHotKeyFunc = callerHKNotify;

        }

        private void FreqBandNumGetNotify(bool succ, UInt32 bandNum, String errMsg)
        {
            if (succ)
            {
                // Limit the Max. Antenna Power to 29.5dBm in certain bands
                if (bandNum == RfidSp.AntPwrLimitingBndNum)
                {
                    maxPwrAllowed = RfidSp.MAX_BAND4_ANTENNA_PWR;
                }
            }
            else
            {
                MessageBox.Show(errMsg, "Error getting Frequency band number");
                // deduce from frequency
            }

            // don't set UI until Frequency Profile/Channel information received
        }

        private void AntPort0CfgArrv(bool succ, uint portNum, ref RFID_ANTENNA_PORT_CONFIG cfg, String errMsg)
        {
            opRunning = false;
            if (portNum != 0)
            {
                MessageBox.Show("Unexpected Antenna Port Number " + portNum);
                this.Close();
            }
            else if (!succ)
            {
                MessageBox.Show (errMsg, "Get Antenna Config Failed");
                this.Close();
            }
            else
            {
                curPwrLvl = (float)(cfg.powerLevel * 0.1F);
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                //Reader Rdr = ReaderFactory.GetReader();
                Reader Rdr = ReaderFactory.GetReader();
               //UInt32 bandNum;

                if (!Rdr.CustomFreqBandNumGet())
                {
                    MessageBox.Show("Unable to get current Frequency band number", "Error");
                }
                else
                {
                    // Limit the Max. Antenna Power to 29.5dBm in certain bands
                    if (Rdr.bandNum == RfidSp.AntPwrLimitingBndNum)
                    {
                        maxPwrAllowed = RfidSp.MAX_BAND4_ANTENNA_PWR;
                    }
                }
            }
        }

        private void LoadCurrPwrLvl()
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            opRunning = true;
            if (!Rdr.AntPortCfgGetOne())
            {
                opRunning = false;
                MessageBox.Show("Unable to get Antenna Port Configuration");
            }
        }

        private void ShowRequestedPwrLvl()
        {
            AntPwrLbl.Text = (curPwrLvl + changeVal).ToString("F1");
        }

        private float changeVal = 0;
        private float minPwrAllowed = 0.0F;
        private float maxPwrAllowed = 30.0F;

        private void AntStrngthKeyHandler(eVKey keyCode, bool down)
        {
            //MessageBox.Show ("Hotkey " + keyCode.ToString("F") + (down? " down" : "  up"));
            if (down)
            {
                // cancel debounce timer
                DebounceTmr.Enabled = false;

                switch (keyCode)
                {
                    case eVKey.VK_F5: // Decrease
                        if ((curPwrLvl  + changeVal)> minPwrAllowed)
                        {
                            changeVal -= 0.5F;
                            ShowRequestedPwrLvl();
                        }
                        break;
                    case eVKey.VK_F4: // Increase
                        if ((curPwrLvl + changeVal) < maxPwrAllowed)
                        {
                            changeVal += 0.5F;
                            ShowRequestedPwrLvl();
                        }
                        break;
                }
            }
            else
            {
                if (changeVal != 0.0F)
                // set up debounce-timer
                    DebounceTmr.Enabled = true;
            }


        }

        private void OnLoad(object sender, EventArgs e)
        {
            ClsHotkey.AddHotKeyDelegate(AntStrngthKeyHandler);
            ClsHotkey.SubHotKeyDelegate(callerHotKeyFunc);
            LoadCurrPwrLvl();
            this.Capture = true; // Mouse Capture
            
        }

        private void OnClosed(object sender, EventArgs e)
        {
            if (DebounceTmr.Enabled == true)
            {
                DebounceTmr.Enabled = false; // in case the debounce timer is running
                Program.ShowWarning("Warning: power to be remained at  " + curPwrLvl.ToString("F1"));
            }

            ClsHotkey.AddHotKeyDelegate(callerHotKeyFunc);
            ClsHotkey.SubHotKeyDelegate(AntStrngthKeyHandler);
            this.Capture = false;
            // save last set power
            UserPref.GetInstance().AntennaPwr = curPwrLvl;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                case Keys.Enter:
                case Keys.Space:
                        this.Close(); // Checking done in OnClosing()
                        e.Handled = true;
                    break;
                default:
                    //MessageBox.Show("Unhandled: " + e.KeyCode.ToString(), "Key Down");
                    break;
            }
        }

        public void PwrSetNotify(bool succ, uint portNum, String errMsg)
        {
            if (succ)
            {
                curPwrLvl = curPwrLvl + changeVal;
            }
            else // revert to old value
            {
                curPwrLvl = curPwrLvl; // force display
                MessageBox.Show(errMsg, "Set Antenna Power failed");
            }

            changeVal = 0; // reset now
            this.Enabled = true;
            opRunning = false;
            
        }

        private void OnDebounceTmrTick(object sender, EventArgs e)
        {
            DebounceTmr.Enabled = false;

            this.Enabled = false;
            opRunning = true;
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.power=(int)((curPwrLvl + changeVal) * 10.0F);
            //Rdr.AntPotCfgSetonNotification+=PwrSetNotify;
            Rdr.RegisterAntPotCfgSetOneNotificationEvent(PwrSetNotify);
            Rdr.PortNum = 0;
            if (!Rdr.AntPortCfgSetPwr())
            {
                this.Enabled = true;
                opRunning = false;
                MessageBox.Show("Unable to set Antenna Power Level");
            }
            
            //MessageBox.Show("Set Antenna Power Now");
        }

        private Point penDnPt = new Point();

        private void OnMouseDown(object sender, MouseEventArgs e)
        {
            penDnPt.X =e.X;
            penDnPt.Y = e.Y;
        }

        private void OnMouseUp(object sender, MouseEventArgs e)
        {
            Point penUpPt = new Point(e.X, e.Y);
            //MessageBox.Show("Mouse Dn: (" + penDnPt.X + ", " + penDnPt.Y + ")\n"
            //       + "Mouse Up: (" + penUpPt.X + ", " + penUpPt.Y + ")");

            if ((penDnPt.X < 0 && penUpPt.X < 0)
                || (penDnPt.Y < 0 && penUpPt.Y < 0))
            {
                this.Close();
            }
            else if (this.Capture == false)
            {
                this.Capture = true; // the Capture flag would be off after mouse-up
            }
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            if (opRunning == true)
            {
                MessageBox.Show("Busy. Please try again later.");
                e.Cancel = true;
            }
        }

        private void OnEnabled(object sender, EventArgs e)
        {
            if (this.Capture == false) // keeping the mouse captured
                this.Capture = true;
        }

        public static void ShowPopup(HandHeldHotKeyNotify callerHKNotify)
        {
            AntPwrHtKyPopup Popup = new AntPwrHtKyPopup(callerHKNotify);
            Popup.ShowDialog();
            Popup.Dispose();
        }
    }
}