using System;
using System.Text;
//using ClslibRfidSp;
using Microsoft.WindowsCE.Forms;
//using System.Windows.Forms;

// aliases
using CF = ClslibRfidSp.CallFlow;
using SP = ReaderTypes.RfidSp;
using OnRamp;
//using Reader.RFIDTypes.RFIDEnums;
//using  Reader.RFIDTypes.RFIDStructs;
using ReaderTypes;

namespace CS101ReaderLib.UtilityTypes
{
    #region MemMgt_funcs
    public class MemMgt
    {
        //#region Memory Management
        const int LMEM_ZEROINIT = 0x40;
        const int LPTR = 0x40; /// LMEM_ZEROINIT|LMEM_FIXED

        public static IntPtr LocalAlloc(int flags, int size)
        {
            return ClslibRfidSp.MemMgt.LocalAlloc(flags, size);
        }

        public static IntPtr LocalFree(IntPtr pMem)
        {
            return ClslibRfidSp.MemMgt.LocalFree(pMem);
        }
        //#endregion
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
    #endregion
    #region SettingMgt_funcs
    public class SettingMgt
    {
        //private const String W_SettingMgtDLL_Path = "W_SettingMgt.dll";

        public SettingMgt() { }


        public static HRESULT_RFID f_ApplicationSetup()
        {
            return ClslibRfidSp.SettingMgt.f_ApplicationSetup();
        }

        /// HRESULT WINAPI f_SettingMgt_Initialize( HWND  hWnd ); //System.Runtime.InteropServices.

        public static HRESULT_RFID f_SettingMgt_Initialize(IntPtr hWnd)
        {
            return ClslibRfidSp.SettingMgt.f_SettingMgt_Initialize(hWnd);
        }
        /// HRESULT WINAPI f_SettingMgt_Uninitialize(); 

        public static HRESULT_RFID f_SettingMgt_Uninitialize()
        {
            return ClslibRfidSp.SettingMgt.f_SettingMgt_Uninitialize();
        }
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Read(); 

        public static HRESULT_RFID f_SettingMgt_RfidSpSetting_Read()
        {
            return ClslibRfidSp.SettingMgt.f_SettingMgt_RfidSpSetting_Read();
        }
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Write(); 

        public static HRESULT_RFID f_SettingMgt_RfidSpSetting_Write()
        {
            return ClslibRfidSp.SettingMgt.f_SettingMgt_RfidSpSetting_Write();
        }

        public static HRESULT_RFID f_SettingMgt_StartLogTrace(String logDirPath)
        {
            return ClslibRfidSp.SettingMgt.f_SettingMgt_StartLogTrace(logDirPath);
        }

        public static HRESULT_RFID f_SettingMgt_StopLogTrace()
        {
            return ClslibRfidSp.SettingMgt.f_SettingMgt_StopLogTrace();
        }

        public static HRESULT_RFID f_CollectWifiTCPIPStatus(
                               ref bool enabled, ref bool connected, StringBuilder hwaddr, StringBuilder ipaddr, StringBuilder ipmask,
                               StringBuilder gwaddr, StringBuilder dnsaddr1, StringBuilder dnsaddr2,
                               ref bool dhcp, StringBuilder dhcpaddr, SystemTime leaseTm, SystemTime expireTm)
        {
            return ClslibRfidSp.SettingMgt.f_CollectWifiTCPIPStatus(
                               ref  enabled, ref   connected, hwaddr, ipaddr, ipmask,
                                 gwaddr, dnsaddr1, dnsaddr2,
                               ref   dhcp, dhcpaddr, leaseTm, expireTm);
        }

        public static Int32 f_SetWINSHostName(String hostName)
        {
            return ClslibRfidSp.SettingMgt.f_SetWINSHostName(hostName);
        }

        public static Int32 f_GetWINSHostName(StringBuilder hostName, int bufSz)
        {
            return ClslibRfidSp.SettingMgt.f_GetWINSHostName(hostName, bufSz);
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    #endregion
}

namespace CS101ReaderLib
{
   

    //
    // Singleton class
    //
    public class RFIDRdr : IDisposable
    {
        private const UInt16 BitsPerTagWd = 16; // Tag Addressing Word
        // Tentative values
        private const uint TagStopCountUnlimited = 0; // max=1 (Intel doc)
        private const uint TagStopCount1 = 1; // max = 1 (Intel doc)

        public RFID_VERSION DriverVer
        {
            get
            {
                return Rfid.st_RfidSpReq_Startup.LibraryVersion;
            }
        }
        public RFID_VERSION MacVer
        {
            get
            {
                return Rfid.st_RfidSpReq_MacGetVersion.version;
            }
        }
        #region static member variables
        static private RFIDRdr theRdr;
        #endregion
        #region instance member variables
        private HRESULT_RFID errCode;
        private ushort macErrCode = 0;
        public HRESULT_RFID LastErrCode
        {
            get
            {
                return errCode;
            }
        }
        public ushort LastMacErrCode
        {
            get
            {
                return macErrCode;
            }
        }
        public event EventHandler<LckOpEventArgs> LckOpStEvent;
        public event EventHandler<DscvrTagEventArgs> TagPermSetEvent;
        public event EventHandler<WrOpEventArgs> WrOpStEvent;
        public event EventHandler<MemBnkWrEventArgs> MemBnkWrEvent;
        public event EventHandler<RdOpEventArgs> RdOpStEvent;
        public event EventHandler<MemBnkRdEventArgs> MemBnkRdEvent;
        public event EventHandler<InvtryOpEventArgs> InvtryOpStEvent;
        public event EventHandler<DscvrTagEventArgs> DscvrTagEvent;
        public event EventHandler<TagRateEventArgs> TagRateEvent;
        public event EventHandler<TagRssiEventArgs> TagRssiEvent;

        ClslibRfidSp.MsgWindow SpMsgWnd; // receive notification from unmanaged RfidSp library 
        // via Window Message

        // ask the callee for BnkNum, Read-Data (offset + cnt)
     
        // Bank and
        // DataField (in String)
        #endregion
     

        #region Initializers
        private void InitAntCfgArr()
        {
            for (int p = 0; p < Rfid.st_RfidSpReq_AntennaPortGetConfiguration.Length; p++)
            {
                Rfid.st_RfidSpReq_AntennaPortGetConfiguration[p].handle = ReaderTypes.RfidSp.RFID_INVALID_RADIO_HANDLE;
                Rfid.st_RfidSpReq_AntennaPortGetConfiguration[p].antennaPort = (uint)p;
                Rfid.st_RfidSpReq_AntennaPortGetConfiguration[p].config = new RFID_ANTENNA_PORT_CONFIG();
                Rfid.st_RfidSpReq_AntennaPortGetConfiguration[p].status = HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT;
            }
        }

        #endregion
        #region constructors
        static public RFIDRdr GetInstance()
        {
            if (theRdr == null)
            {
                theRdr = new RFIDRdr();

                theRdr.InitAntCfgArr();

            }

            return theRdr;
        }

        private RFIDRdr()
        {
            disposed = false;

            // Initialize Unmanaged RFID Library
            SpMsgWnd = new ClslibRfidSp.MsgWindow();
            if (!SP.SUCCEEDED(CF.f_CFlow_Initialize(SpMsgWnd.Hwnd)))
                throw new ApplicationException("CallFlow Init Failed");

            radioStatus = RadioStatus.Closed;

            InitTempUpdateEvt(); // Temperature messages
        }
        #endregion

        #region destructors
        private bool disposed; // prevent multiple disposal

        ~RFIDRdr()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // All the real clean-up work is done here
        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing) // calling from Dispose() directly
                {
                    // free managed resources
                }
                // free unmanaged resources
                CF.f_CFlow_Uninitialize();

                disposed = true;
            }
        }
        #endregion

        #region Radio On/Off/PowerSave
        //public enum RadioStatus : uint
        //{
        //    Closed,         // Radio handle Close, Shutdown
        //    Closing,        // Close, Shutdown in progress
        //    ClosePend,   // Close requested once current command finished
        //    Opening,      // Startup, Enumerate, Open
        //    Opened,       // Open success but unable to start configuration somehow
        //    Configuring, // Cont mode
        //    Configured, // Done configuration, doesn't mean config success
        //    PowerSaved, // In power-save mode
        //    ErrorStopped, // Radio Open failed
        //};

        //public enum RadioOpRes : uint
        //{
        //    // Only use in notification callback, not device state
        //    None,
        //    StateChanged,
        //    Error,
        //    Warning,
        //};

        private RadioStatus radioStatus = RadioStatus.Closed;
        public bool RadioReady()
        {
            return (radioStatus == RadioStatus.Opened)
                || (radioStatus == RadioStatus.Configured)
                || (radioStatus == RadioStatus.PowerSaved);
        }

        //public delegate void RadioStatusNotify(RadioOpRes res, RadioStatus status, String msg);

        private RadioStatusNotify radioOpenNotify, radioCloseNotify;

        public bool RadioOpen(RadioStatusNotify radOpnNotify)
        {
            bool rv = false;

            switch (radioStatus)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
                    if (DevStart())
                    {
                        radioStatus = RadioStatus.Opening;
                        radioOpenNotify += radOpnNotify;
                        rv = true;
                    }
                    else
                    {
                        radioStatus = RadioStatus.ErrorStopped;
                        radioOpenNotify = null;
                        rv = false;
                    }
                    break;
                case RadioStatus.ClosePend:
                    radOpnNotify(RadioOpRes.Error, radioStatus, "Close is pending, try later");
                    rv = false;
                    break;
                case RadioStatus.Opening:
                    radOpnNotify(RadioOpRes.Warning, radioStatus, "Still being open");
                    rv = true;
                    break;
                case RadioStatus.Opened: // but configured failed
                    radOpnNotify(RadioOpRes.Warning, radioStatus, "Already Opened");
                    rv = true;
                    break;
                case RadioStatus.Configuring:
                    radOpnNotify(RadioOpRes.Warning, radioStatus, "Configuring radio");
                    rv = true;
                    break;
                case RadioStatus.Configured:
                    radOpnNotify(RadioOpRes.Warning, radioStatus, "Already configured");
                    rv = true;
                    break;
            }

            return rv;
        }

        public bool RadioReOpen(RadioStatusNotify radReOpnNotify)
        {
            if (radioStatus != RadioStatus.Closed && radioStatus != RadioStatus.ErrorStopped)
                radioStatus = RadioStatus.Closed; // fake close OK?
            return RadioOpen(radReOpnNotify);
        }

        public bool RadioClose(RadioStatusNotify radClsNotify)
        {
            bool rv = false;

            switch (radioStatus)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
                    radClsNotify(RadioOpRes.Warning, radioStatus, "Already closed");
                    rv = true;
                    break;
                case RadioStatus.ClosePend:
                    radClsNotify(RadioOpRes.Warning, radioStatus, "Close is requested already");
                    rv = true;
                    break;
                case RadioStatus.Opening:
                case RadioStatus.Configuring:// No wait until Configured
                    radioStatus = RadioStatus.ClosePend;
                    radClsNotify(RadioOpRes.StateChanged, radioStatus, null);
                    rv = true;
                    break;
                case RadioStatus.Opened:
                case RadioStatus.Configured:
                    if (DevStop(true))
                    {
                        radioStatus = RadioStatus.Closing;
                        radioCloseNotify = radClsNotify;
                        rv = true;
                    }
                    else
                    {
                        radioStatus = RadioStatus.ErrorStopped;
                        radioCloseNotify = null;
                        rv = false;
                    }
                    break;
            }
            return rv;
        }

        #endregion
        #region common RFID CallFlow Routines dispatch request, wait  message, next

        private void RFIDMsgTopHandler(RFID_MSGID msgID)
        {
            String MsgIDStr = msgID.ToString("F");
            Console.WriteLine(MsgIDStr);
            // do nothing for now
        }
        #endregion
        #region common RFID Start, Stop Routines
        enum DevStartStatus
        {
            Starting,
            InitOK,
            RadioOpened,
            RadioConfigured,
            Error,
        };
        enum DevStopStatus
        {
            Stopping,
            RadioClosed,
            UninitOK,
            Error
        }

        // This function and DevStartMsgArrv works in pair
        private void DevStartMsgHResArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            RadioOpRes Res = RadioOpRes.None;
            String Msg = null;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomSetRadioProfile:
                    // Set Compact Mode here
                    if (SP.SUCCEEDED(hRes))
                    {
                        // leave RadioStatus as Configuring
                        // Set ResponseMode to Compact (reduce traffic)
                        hRes = _SetResponseDataMode(RFID_RESPONSE_MODE.RFID_RESPONSE_MODE_COMPACT);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            Res = RadioOpRes.Error;
                            Msg = "Set ResponseDataMode: " + hRes.ToString();
                            radioStatus = RadioStatus.Configured;
                        }
                    }
                    else
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Warning;
                        Msg = "Set CustomSetRadioProfile: " + hRes.ToString();
                        // keep going even though configuration failed
                    }
                    break;
                default:
                    break;
            }

            if (Res != RadioOpRes.None && radioOpenNotify != null)
                radioOpenNotify(Res, radioStatus, Msg);

            if (radioStatus == RadioStatus.Closing || radioStatus == RadioStatus.Configured
            || radioStatus == RadioStatus.Opened || radioStatus == RadioStatus.ErrorStopped)
            {
                SpMsgWnd.MsgNotify -= DevStartMsgArrv; // No more
                SpMsgWnd.MsgNotifyHRes -= DevStartMsgHResArrv;
                SpMsgWnd.MsgNotifyUINT32 -= DevStartMsgUINT32Arrv;
                radioOpenNotify = null; // finished
            }

        }

        // arg1 is portNum
        private void DevStartMsgUINT32Arrv(RFID_MSGID msgID, uint arg1)
        {
            RadioOpRes Res = RadioOpRes.None;
            String Msg = null;
            HRESULT_RFID hRes;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetConfiguration:
                    hRes = Rfid.st_RfidSpReq_AntennaPortGetConfiguration[arg1].status;
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Error;
                        Msg = "Get Antenna Port " + arg1 + " Error: " + hRes.ToString();
                        radioStatus = RadioStatus.Opened; // treat as Opened
                    }
                    else // start Configuring
                    {
                        Res = RadioOpRes.StateChanged;
                        radioStatus = RadioStatus.Opened;
                        // leave RadioStatus as Configuring
                        // Set Country Mode
                        // ***********Place holder: set something that won't do anything***********************************
                        hRes = CF.f_CFlow_RfidDev_CustomSetRadioProfile(Rfid.st_RfidSpReq_RadioOpen.handle, CustomFreqGrp.PlaceHolder, false);
                        if (SP.SUCCEEDED(hRes))
                        {
                            radioStatus = RadioStatus.Configuring;
                        }
                        else
                        {
                            errCode = hRes;
                            Res = RadioOpRes.Error;
                            Msg = "CustomSetRadioProfile: " + hRes.ToString();
                            radioStatus = RadioStatus.Configured;
                        }
                    }
                    break;
            }
            if (Res != RadioOpRes.None && radioOpenNotify != null)
                radioOpenNotify(Res, radioStatus, Msg);

            if (radioStatus == RadioStatus.Closing || radioStatus == RadioStatus.Configured
            || radioStatus == RadioStatus.Opened || radioStatus == RadioStatus.ErrorStopped)
            {
                SpMsgWnd.MsgNotify -= DevStartMsgArrv; // No more
                SpMsgWnd.MsgNotifyHRes -= DevStartMsgHResArrv;
                SpMsgWnd.MsgNotifyUINT32 -= DevStartMsgUINT32Arrv;
                radioOpenNotify = null; // finished
            }
        }

        // trigger radioOpenNotify delegate
        private void DevStartMsgArrv(RFID_MSGID msgID)
        {
             HRESULT_RFID hRes;
          RadioOpRes Res = RadioOpRes.None;
            String Msg = null;

            switch (msgID)
            {
                case  RFID_MSGID.RFID_REQEND_TYPE_MSGID_Startup:
                    hRes =  Rfid.st_RfidSpReq_Startup.status;
                    if (SP.SUCCEEDED(hRes))
                    {
                        // retrieve radio cmd already issued, defer handling
                        // of state ClosePend
                        // no state change, wait for retrieve radio list result status
                    }
                    else
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Error;
                        Msg = "RFID Library Start up: " + hRes.ToString("F");
                        radioStatus = RadioStatus.ErrorStopped;
                    }
                    break;
                case  RFID_MSGID.RFID_REQEND_TYPE_MSGID_RetrieveAttachedRadiosList:
                    hRes = Rfid.st_RfidSpReq_RetrieveAttachedRadiosList.status;
                    if (radioStatus == RadioStatus.ClosePend
                        || (!SP.SUCCEEDED(hRes)))
                    {

                        Res = (radioStatus == RadioStatus.ClosePend) ?
                            RadioOpRes.StateChanged : RadioOpRes.Error;
                        if (!SP.SUCCEEDED(hRes))
                        {
                            Msg = "Retreive Attached Radio List: "
                            + Rfid.st_RfidSpReq_RetrieveAttachedRadiosList.status.ToString();
                            errCode = hRes;
                        }
                        radioStatus = RadioStatus.Closing;
                        if (!DevStop(false))
                        {
                            Res = RadioOpRes.Error;
                            radioStatus = RadioStatus.ErrorStopped;
                            Msg = "DevStop() failed";
                        }
                    }
                    else
                    {
                        /* see if available no. of radio is > 0 */
                        if (Rfid.st_RfidSpReq_RetrieveAttachedRadiosList.radio_enum.countRadios == 0)
                        {
                            Res = RadioOpRes.Error;
                            Msg = "Cannot find Radio";
                            radioStatus = RadioStatus.ErrorStopped;
                        }
                        else
                        {
                            hRes = CF.f_CFlow_RfidDev_RadioOpen(ref Rfid.st_RfidSpReq_RadioOpen);
                            if (!SP.SUCCEEDED(hRes))
                            {
                                errCode = hRes;
                                Res = RadioOpRes.Error;
                                Msg = "Radio Open" + hRes.ToString("F");
                                radioStatus = RadioStatus.ErrorStopped;
                            }
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioOpen:
                    hRes = Rfid.st_RfidSpReq_RadioOpen.status;
                    if ((radioStatus == RadioStatus.ClosePend) || (!SP.SUCCEEDED(hRes)))
                    {
                        Res = (radioStatus == RadioStatus.ClosePend) ? RadioOpRes.StateChanged
                            : RadioOpRes.Error;
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            Msg = "Radio Open" + hRes.ToString("F");
                            Rfid.st_RfidSpReq_RadioOpen.handle = RfidSp.RFID_INVALID_RADIO_HANDLE;
                        }
                        radioStatus = RadioStatus.Closing;
                        if (!DevStop(SP.SUCCEEDED(hRes)))
                        {
                            Res = RadioOpRes.Error;
                            radioStatus = RadioStatus.ErrorStopped;
                            Msg = "DevStop() failed";
                        }
                    }
                    else
                    {
                        // Radio not considered 'Opened' state yet
                        Rfid.st_RfidSpReq_MacGetVersion.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                        hRes = CF.f_CFlow_RfidDev_MacGetVersion(ref Rfid.st_RfidSpReq_MacGetVersion);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            Res = RadioOpRes.Error;
                            Msg = "Unable to Get Mac Version: " + hRes.ToString();
                            radioStatus = RadioStatus.Opened;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacGetVersion:
                    // Won't make state change to Opened until
                    // Antenna Port 0 config is retrieved.
                    if (!AntPortCfgGetOne(0, out hRes))
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Error;
                        Msg = "Unable to Get Antenna Port 0 Configuration: " + hRes.ToString();
                        radioStatus = RadioStatus.Opened;
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetResponseDataMode:
                    hRes = Rfid.st_RfidSpReq_RadioSetResponseDataMode.status;
                    if (SP.SUCCEEDED(hRes))
                    {
                        Res = RadioOpRes.StateChanged;
                    }
                    else
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Error;
                        Msg = "SetResponseDataMode: " + hRes.ToString();
                    }
                    radioStatus = RadioStatus.Configured; // Done
                    break;
            }

            if (Res != RadioOpRes.None && radioOpenNotify != null)
                radioOpenNotify(Res, radioStatus, Msg);

            if (radioStatus == RadioStatus.Closing || radioStatus == RadioStatus.Configured
                || radioStatus == RadioStatus.Opened || radioStatus == RadioStatus.ErrorStopped)
            {
                SpMsgWnd.MsgNotify -= DevStartMsgArrv; // No more
                SpMsgWnd.MsgNotifyHRes -= DevStartMsgHResArrv;
                SpMsgWnd.MsgNotifyUINT32 -= DevStartMsgUINT32Arrv;
                radioOpenNotify = null; // finished
            }

        }

        // will not check or change radioStatus
        private bool DevStart()
        {
            HRESULT_RFID hRes;

            SpMsgWnd.MsgNotify += DevStartMsgArrv;
            SpMsgWnd.MsgNotifyHRes += DevStartMsgHResArrv;
            SpMsgWnd.MsgNotifyUINT32 += DevStartMsgUINT32Arrv;

            hRes = CF.f_CFlow_RfidDev_Initialize(ref Rfid.st_RfidSpReq_Startup,
                    ref Rfid.st_RfidSpReq_RetrieveAttachedRadiosList);

            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                SpMsgWnd.MsgNotify -= DevStartMsgArrv;
                SpMsgWnd.MsgNotifyHRes -= DevStartMsgHResArrv;
                SpMsgWnd.MsgNotifyUINT32 -= DevStartMsgUINT32Arrv;
                return false;
            }
            return true;
        }


        private void DevStopMsgArrv(RFID_MSGID msgID)
        {
            HRESULT_RFID hRes;
            RadioOpRes Res = RadioOpRes.None;
            String Msg = null;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioClose:
                    hRes = Rfid.st_RfidSpReq_RadioClose.status;
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Error;
                        Msg = "Radio Close: " + hRes.ToString();
                        // unable to close: no change in radioStatus
                    }
                    else
                        Rfid.st_RfidSpReq_RadioOpen.handle =RfidSp.RFID_INVALID_RADIO_HANDLE;

                    // Even though Radio Close fail, continue stopping device
                    hRes = CF.f_CFlow_RfidDev_Uninitialize(ref Rfid.st_RfidSpReq_Shutdown);
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Error; // nothing we could do
                        Msg = "RFID Library Shutdown: " + hRes.ToString();
                        radioStatus = RadioStatus.ErrorStopped; // for now
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_Shutdown:
                    hRes = Rfid.st_RfidSpReq_Shutdown.status;
                    if (SP.SUCCEEDED(hRes))
                    {
                        Res = RadioOpRes.StateChanged;
                        radioStatus = RadioStatus.Closed;
                    }
                    else
                    {
                        errCode = hRes;
                        Res = RadioOpRes.Error; // nothing we could do
                        Msg = "RFID Library Shutdown: " + hRes.ToString();
                        radioStatus = RadioStatus.ErrorStopped; // for now
                    }
                    break;
                default:
                    return;
            }

            if (Res != RadioOpRes.None && radioCloseNotify != null)
                radioCloseNotify(Res, radioStatus, Msg);

            if (radioStatus == RadioStatus.Closed || radioStatus == RadioStatus.ErrorStopped)
            {
                SpMsgWnd.MsgNotify -= DevStopMsgArrv; // No more
                radioCloseNotify = null;
            }
        }

        // will not check or change radioStatus
        private bool DevStop(bool closeRadio)
        {
            HRESULT_RFID hRes;

            SpMsgWnd.MsgNotify += DevStopMsgArrv;

            // Close Radio if opened
            if (closeRadio)
            {
                hRes = CF.f_CFlow_RfidDev_RadioClose(ref Rfid.st_RfidSpReq_RadioClose);
                if (SP.SUCCEEDED(hRes))
                    return true;
                // if fail, continue to shutdown the device below
            }
            // Shutdown
            hRes = CF.f_CFlow_RfidDev_Uninitialize(ref Rfid.st_RfidSpReq_Shutdown);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                SpMsgWnd.MsgNotify -= DevStopMsgArrv;
                return false;
            }
            return true;
        }

        #endregion

        #region ByteArray Routines
        public static void CopyHexStrToByteArr(String str, ref Byte[] byteArr)
        {
            int cnt = (str.Length < byteArr.Length) ? str.Length : byteArr.Length;
            int i, j;

            for (i = 0, j = 0; i < cnt; i += 2, j++)
            {
                byteArr[j] = Byte.Parse(str.Substring(i, 2),
                    System.Globalization.NumberStyles.HexNumber);
            }
        }

        public static String ByteArrToHexStr(Byte[] byteArr)
        {
            if (byteArr == null || byteArr.Length == 0)
                return null;
            StringBuilder Bldr = new StringBuilder(byteArr.Length * 2);
            for (int b = 0; b < byteArr.Length; b++)
                Bldr.Append(byteArr[b].ToString("X2"));
            return Bldr.ToString();
        }

        private Byte[] MakeEPCBnkMask(UInt16 PC, ref UINT96_T EPC)
        {
            Byte[] Mask = new Byte[EPCTag.PCFldSz * 2 + EPCTag.EPCFldSz * 2];
            Mask[0] = (Byte)((PC >> 8) & 0x00FF);
            Mask[1] = (Byte)(PC & 0x00FF);
            Byte[] EPCMask = EPC.ToByteArray();
            Array.Copy(EPCMask, 0, Mask, 2, EPCMask.Length);
            return Mask;
        }
        #endregion
        #region common RFID tag matching routines
        private void CopyHexStrToShortArr(String str, ushort[] wdArr)
        {
            int StrSz = str.Length;
            int DestBufSz = wdArr.Length;

            // caller is responsible for checking sizes
            for (int i = 0, j = 0; (i < StrSz) && (j < DestBufSz); i += 4, j++)
            {
                /* Word-by-word */
                wdArr[j] = UInt16.Parse(str.Substring(i, 4),
                    System.Globalization.NumberStyles.HexNumber);
            }
        }
        #endregion

        #region Integrated Operation Setup Routines (Singulation, Select Criteria)
        private RFID_18K6C_QUERY_PARMS tagOperQryParm;
        private RFID_18K6C_SELECT_CRITERION[] tagOperSelCrit;
        private RFID_18K6C_SINGULATION_CRITERION[] tagOperPostMatchCrit;


        private void TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM qAlgo,
            uint QSize, RFID_18K6C_INVENTORY_SESSION session)
        {
            // Disable Select
            tagOperSelCrit = null;
            // Disable Post Match
            tagOperPostMatchCrit = null;

            // Set up Singulation Algorithm + Parameters
            tagOperQryParm = new RFID_18K6C_QUERY_PARMS();
            tagOperQryParm.tagGroup.selected = RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ALL;
            tagOperQryParm.tagGroup.session = session;
            tagOperQryParm.tagGroup.target = RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A; // doesn't matter
            tagOperQryParm.singulationParms.singulationAlgorithm = qAlgo;
            switch (qAlgo)
            {
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ:
                    tagOperQryParm.singulationParms.fixedQ.qValue = QSize;
                    tagOperQryParm.singulationParms.fixedQ.toggleTarget = 1;
                    tagOperQryParm.singulationParms.fixedQ.repeatUntilNoTags = 1;
                    tagOperQryParm.singulationParms.fixedQ.retryCount = 10;
                    break;
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ:
                    tagOperQryParm.singulationParms.dynamicQ.startQValue = QSize;
                    tagOperQryParm.singulationParms.dynamicQ.toggleTarget = 1;
                    tagOperQryParm.singulationParms.dynamicQ.minQValue = 0;
                    tagOperQryParm.singulationParms.dynamicQ.maxQValue = 15;
                    tagOperQryParm.singulationParms.dynamicQ.maxQueryRepCount = 10; // default(?)
                    tagOperQryParm.singulationParms.dynamicQ.retryCount = 0;
                    break;
            }
        }

        // With choice of Session
        // Post-Singulation: epcMask argument : Not bankMask, but EPC mask
        private void TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM qAlgo,
            uint QSize, RFID_18K6C_INVENTORY_SESSION session,
            Byte[] epcMask, uint maskOffset, bool invMatch)
        {
            // Disable Select
            tagOperSelCrit = null;

            // Setup Post Match Criteria
            if (epcMask == null || epcMask.Length == 0)
            {
                tagOperPostMatchCrit = new RFID_18K6C_SINGULATION_CRITERION[0];
            }
            else
            {
                tagOperPostMatchCrit = new RFID_18K6C_SINGULATION_CRITERION[1];
                tagOperPostMatchCrit[0].match = (uint)(invMatch ? 0 : 1);
                tagOperPostMatchCrit[0].mask.count = (uint)epcMask.Length * 8; // in bits
                tagOperPostMatchCrit[0].mask.offset = maskOffset * 16; //bits-per-word
                tagOperPostMatchCrit[0].mask.mask = epcMask;
            }

            // Set up Singulation Algorithm + Parameters
            tagOperQryParm = new RFID_18K6C_QUERY_PARMS();
            tagOperQryParm.tagGroup.selected = RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ALL;
            tagOperQryParm.tagGroup.session = session;
            tagOperQryParm.tagGroup.target = RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A; // doesn't matter
            tagOperQryParm.singulationParms.singulationAlgorithm = qAlgo;
            switch (qAlgo)
            {
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ:
                    tagOperQryParm.singulationParms.fixedQ.qValue = QSize;
                    tagOperQryParm.singulationParms.fixedQ.toggleTarget = 1;
                    tagOperQryParm.singulationParms.fixedQ.repeatUntilNoTags = 1;
                    tagOperQryParm.singulationParms.fixedQ.retryCount = 10;
                    break;
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ:
                    tagOperQryParm.singulationParms.dynamicQ.startQValue = QSize;
                    tagOperQryParm.singulationParms.dynamicQ.toggleTarget = 1;
                    tagOperQryParm.singulationParms.dynamicQ.minQValue = 0;
                    tagOperQryParm.singulationParms.dynamicQ.maxQValue = 15;
                    tagOperQryParm.singulationParms.dynamicQ.maxQueryRepCount = 10; // default(?)
                    tagOperQryParm.singulationParms.dynamicQ.retryCount = 0;
                    break;
            }
        }

        // Post-Singulation: epcMask argument : Not bankMask, but EPC mask
        private void TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM qAlgo,
            uint QSize, Byte[] epcMask, uint maskOffset, bool invMatch)
        {
            TagOperSetup(qAlgo, QSize, RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0,
                epcMask, maskOffset, invMatch);
        }

        private void TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM qAlgo,
            uint QSize, RFID_18K6C_SELECTED SLTgt, Byte[] epcBnkMask, uint maskOffset,
            RFID_18K6C_ACTION action)
        {
            // Disable PostMatch
            tagOperPostMatchCrit = null;

            if (epcBnkMask == null || epcBnkMask.Length == 0)
            {
                tagOperSelCrit = new RFID_18K6C_SELECT_CRITERION[0];
            }
            else
            {
                // Set up Select Criteria
                tagOperSelCrit = new RFID_18K6C_SELECT_CRITERION[1];
                tagOperSelCrit[0].mask.offset = maskOffset * 16; // bits-per-word
                tagOperSelCrit[0].mask.count = (uint)((epcBnkMask != null) ? epcBnkMask.Length * 8 : 0);
                tagOperSelCrit[0].mask.mask = new Byte[epcBnkMask.Length];
                Array.Copy(epcBnkMask, tagOperSelCrit[0].mask.mask, epcBnkMask.Length);
                tagOperSelCrit[0].mask.bank = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_EPC;
                tagOperSelCrit[0].action.action = action;
                tagOperSelCrit[0].action.target = RFID_18K6C_TARGET.RFID_18K6C_TARGET_SELECTED;
                tagOperSelCrit[0].action.enableTruncate = 0;
            }

            // Set up Singulation Algorithm + Parameters
            tagOperQryParm = new RFID_18K6C_QUERY_PARMS();
            tagOperQryParm.tagGroup.selected = SLTgt;
            tagOperQryParm.tagGroup.session = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0; // value doesn't matter
            tagOperQryParm.tagGroup.target = RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A; // doesn't matter
            tagOperQryParm.singulationParms.singulationAlgorithm = qAlgo;
            switch (qAlgo)
            {
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ:
                    tagOperQryParm.singulationParms.fixedQ.qValue = QSize;
                    tagOperQryParm.singulationParms.fixedQ.toggleTarget = 1;
                    tagOperQryParm.singulationParms.fixedQ.repeatUntilNoTags = 0;
                    tagOperQryParm.singulationParms.fixedQ.retryCount = 0;
                    break;
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ:
                    tagOperQryParm.singulationParms.dynamicQ.startQValue = QSize;
                    tagOperQryParm.singulationParms.dynamicQ.toggleTarget = 1;
                    tagOperQryParm.singulationParms.dynamicQ.minQValue = 0;
                    tagOperQryParm.singulationParms.dynamicQ.maxQValue = 15;
                    tagOperQryParm.singulationParms.dynamicQ.maxQueryRepCount = 10; // default(?)
                    tagOperQryParm.singulationParms.dynamicQ.retryCount = 0;
                    break;
            }
        }

        private void TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM qAlgo,
            uint QSize, RFID_18K6C_INVENTORY_SESSION session,
            RFID_18K6C_INVENTORY_SESSION_TARGET target, bool toggleTgtPerCyc,
            Byte[] epcBnkMask, uint maskOffset, RFID_18K6C_ACTION action)
        {
            // Disable PostMatch
            tagOperPostMatchCrit = null;
            if (epcBnkMask == null || epcBnkMask.Length == 0)
            {
                tagOperSelCrit = new RFID_18K6C_SELECT_CRITERION[0];
            }
            else
            {
                // Set up Select Criteria
                tagOperSelCrit = new RFID_18K6C_SELECT_CRITERION[1];
                tagOperSelCrit[0].mask.offset = maskOffset * 16; // bits-per-word
                tagOperSelCrit[0].mask.count = (uint)((epcBnkMask != null) ? epcBnkMask.Length * 8 : 0);
                tagOperSelCrit[0].mask.mask = new Byte[epcBnkMask.Length];
                Array.Copy(epcBnkMask, tagOperSelCrit[0].mask.mask, epcBnkMask.Length);
                tagOperSelCrit[0].mask.bank = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_EPC;
                tagOperSelCrit[0].action.action = action;
                tagOperSelCrit[0].action.target = (RFID_18K6C_TARGET)session;
                tagOperSelCrit[0].action.enableTruncate = 0;
            }

            // Set up Singulation Algorithm + Parameters
            tagOperQryParm = new RFID_18K6C_QUERY_PARMS();
            tagOperQryParm.tagGroup.selected = RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ALL;
            tagOperQryParm.tagGroup.session = session;
            tagOperQryParm.tagGroup.target = target;
            tagOperQryParm.singulationParms.singulationAlgorithm = qAlgo;
            switch (qAlgo)
            {
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ:
                    tagOperQryParm.singulationParms.fixedQ.qValue = QSize;
                    tagOperQryParm.singulationParms.fixedQ.toggleTarget = (uint)(toggleTgtPerCyc ? 1 : 0);
                    tagOperQryParm.singulationParms.fixedQ.repeatUntilNoTags = 1;
                    tagOperQryParm.singulationParms.fixedQ.retryCount = 10;
                    break;
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ:
                    tagOperQryParm.singulationParms.dynamicQ.startQValue = QSize;
                    tagOperQryParm.singulationParms.dynamicQ.toggleTarget = (uint)(toggleTgtPerCyc ? 1 : 0);
                    tagOperQryParm.singulationParms.dynamicQ.minQValue = 0;
                    tagOperQryParm.singulationParms.dynamicQ.maxQValue = 15;
                    tagOperQryParm.singulationParms.dynamicQ.maxQueryRepCount = 10; // default(?)
                    tagOperQryParm.singulationParms.dynamicQ.retryCount = 0;
                    break;
            }
        }


        private bool TagIDSelCritMet(UInt16 pc, ref UINT96_T epc, byte[] epcBnkMask, ushort maskOffset)
        {
            if (maskOffset < 1)
            {
                return false; // not supported yet
            }
            else if (maskOffset == 1)
            {
                // Check PC
                UInt16 PCMask = (ushort)(epcBnkMask[0] << 8 | epcBnkMask[1]);
                if (PCMask != pc)
                    return false;
            }

            // epcBnkMask array index
            int mi = (maskOffset == 1) ? 2 : 0;
            // EPCArray
            Byte[] RdEPC = epc.ToByteArray();
            int si = (maskOffset < 2) ? 0 : (maskOffset - 2) * 2; // word aligned

            for (; mi < epcBnkMask.Length; mi++, si++)
            {
                if (epcBnkMask[mi] != RdEPC[si])
                    return false;
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="normalMatch"></param>
        /// <param name="epc"></param>
        /// <param name="epcMask"></param>
        /// <param name="maskOffset">offset starts from EPC address </param>
        /// <returns></returns>
        private bool TagIDPostMatchCritMet(bool normalMatch, ref UINT96_T epc, byte[] epcMask, ushort maskOffset)
        {
            // EPCArray
            Byte[] RdEPC = epc.ToByteArray();
            // Compare against epcMask starting at maskOffset
            int si = maskOffset * 2; // in byte
            if (normalMatch)
            {
                for (int mi = 0; mi < epcMask.Length; mi++, si++)
                {
                    if (RdEPC[si] != epcMask[mi])
                        return false;
                }
            }
            else // inverse match
            {
                // fully-match => fail
                bool diffFnd = false;
                for (int mi = 0; mi < epcMask.Length; mi++, si++)
                {
                    if (RdEPC[si] != epcMask[mi])
                    {
                        diffFnd = true;
                        break;
                    }
                }
                if (!diffFnd)
                    return false;
            }
            return true;
        }
        #endregion

        #region Radio  Operation Mode (Cont vs Non-Cont)
        private bool SetContOperMode(out HRESULT_RFID hRes)
        {
            bool Succ = true;

            Rfid.st_RfidSpReq_RadioSetOperationMode.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioSetOperationMode.mode = RFID_RADIO_OPERATION_MODE.RFID_RADIO_OPERATION_MODE_CONTINUOUS;
            hRes = CF.f_CFlow_RfidDev_RadioSetOperationMode(ref Rfid.st_RfidSpReq_RadioSetOperationMode);
            Succ = SP.SUCCEEDED(hRes);

            return Succ;
        }

        private bool SetNonContOperMode(out HRESULT_RFID hRes)
        {
            bool Succ = true;

            Rfid.st_RfidSpReq_RadioSetOperationMode.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioSetOperationMode.mode = RFID_RADIO_OPERATION_MODE.RFID_RADIO_OPERATION_MODE_NONCONTINUOUS;
            hRes = CF.f_CFlow_RfidDev_RadioSetOperationMode(ref Rfid.st_RfidSpReq_RadioSetOperationMode);
            Succ = SP.SUCCEEDED(hRes);

            return Succ;
        }
        #endregion

        #region Tag Inventory Request
        private UInt32 tagInvtryStopCnt = TagStopCountUnlimited;

        private void TagInvtryContStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            bool StartInvtryOp = false;

            switch (res)
            {
                case RadioOpRes.None:
                case RadioOpRes.Warning:
                case RadioOpRes.Error:
                    // somewhere else will take care of this
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            StartInvtryOp = true;
                            break;
                    }
                    break;
            }

            InvtryOpStatus OpStatus = InvtryOpStatus.none;
            bool RaiseEvt = false;
            if (CheckAndClear(ref InvtryStopReq))
            {
                RaiseEvt = true;
                OpStatus = InvtryOpStatus.stopped;
            }
            else if (StartInvtryOp)
            {
                HRESULT_RFID hRes;
                SpMsgWnd.MsgNotifyHRes += InvOpMsgArrv;
                SetContOperMode(out hRes);
                if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                {
                    hRes = TagSelectCritSet(tagOperSelCrit);
                }
                else // There is no SELECT criteria to set, go straight to QueryParameter setup
                {
                    hRes = QueryParmsSet(ref tagOperQryParm);
                }
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = InvtryOpStatus.errorStopped;
                    SpMsgWnd.MsgNotifyHRes -= InvOpMsgArrv;
                    throw new ApplicationException("CF.f_CFlow_RfidDev_SetSelectCriteria() returns: "
                        + hRes.ToString("F"));
                }
            }

            // raise InvtryOpStEvent if appropiate
            if ((RaiseEvt == true) && (InvtryOpStEvent != null))
                InvtryOpStEvent(this, new InvtryOpEventArgs(OpStatus));
            if ((RaiseEvt == true) && (OpStatus == InvtryOpStatus.errorStopped))
                CheckAndClear(ref InvtryStopReq);
        }

        private void TagInvtryOnceStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            bool StartInvtryOp = false;

            switch (res)
            {
                case RadioOpRes.None:
                case RadioOpRes.Warning:
                case RadioOpRes.Error:
                    // somewhere else will take care of this
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            StartInvtryOp = true;
                            break;
                    }
                    break;
            }

            InvtryOpStatus OpStatus = InvtryOpStatus.none;
            bool RaiseEvt = false;
            if (CheckAndClear(ref InvtryStopReq))
            {
                RaiseEvt = true;
                OpStatus = InvtryOpStatus.stopped;
            }
            else if (StartInvtryOp)
            {
                HRESULT_RFID hRes;
                SetNonContOperMode(out hRes);
                ClearMacError();
                Rfid.st_RfidSpReq_18K6CTagInventory.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                Rfid.st_RfidSpReq_18K6CTagInventory.flags = 0;
                if (invtryPerfSelCmd)
                    Rfid.st_RfidSpReq_18K6CTagInventory.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
                Rfid.st_RfidSpReq_18K6CTagInventory.invenParms.common.tagStopCount = tagInvtryStopCnt;
                SpMsgWnd.MsgNotify += InvOpMsgArrv;
                hRes = CF.f_CFlow_RfidDev_GetInventory(ref Rfid.st_RfidSpReq_18K6CTagInventory);
                if (SP.SUCCEEDED(hRes))
                {
                    OpStatus = InvtryOpStatus.started;
                    RaiseEvt = true;
                }
                else
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = InvtryOpStatus.errorStopped;
                    SpMsgWnd.MsgNotify -= InvOpMsgArrv;
                    throw new ApplicationException("CF.f_CFlow_RfidDev_GetInventory() returns: "
                        + hRes.ToString("F"));
                }
            }

            // raise InvtryOpStEvent if appropiate
            if ((RaiseEvt == true) && (InvtryOpStEvent != null))
                InvtryOpStEvent(this, new InvtryOpEventArgs(OpStatus));
            if ((RaiseEvt == true) && (OpStatus == InvtryOpStatus.errorStopped))
                CheckAndClear(ref InvtryStopReq);
        }

        private void TagInvtryDevStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            InvtryOpStatus OpStatus = InvtryOpStatus.none;
            bool RaiseEvt = false;
            //bool StartInvtryOp = false;

            // process notification from RadioOpen
            switch (res)
            {
                case RadioOpRes.None:
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opening:
                        case RadioStatus.Configuring:
                            // keep going
                            break;
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
#if false // leave the notification to other operation specific delegates
                            // Radio ready to use
                            OpStatus = InvtryOpStatus.started;
                            RaiseEvt = true;

                            // Start Inventory Oper
                            StartInvtryOp = true;
#endif
                            break;
                        case RadioStatus.Closed:
                        case RadioStatus.ErrorStopped:
                            OpStatus = InvtryOpStatus.stopped;
                            RaiseEvt = true;
                            break;
                        case RadioStatus.PowerSaved:
                        case RadioStatus.Closing:
                        case RadioStatus.ClosePend:
                            OpStatus = InvtryOpStatus.error;
                            RaiseEvt = true;
                            break;
                    }
                    break;
                case RadioOpRes.Warning:
                    // do nothing
                    RaiseEvt = false;
                    break;
                case RadioOpRes.Error:
                    if (radioStatus == RadioStatus.ErrorStopped)
                        OpStatus = InvtryOpStatus.errorStopped;
                    else
                        OpStatus = InvtryOpStatus.error;
                    RaiseEvt = true;
                    break;
            }

            // raise InvtryOpStEvent if appropiate
            if ((RaiseEvt == true) && (InvtryOpStEvent != null))
                InvtryOpStEvent(this, new InvtryOpEventArgs(OpStatus, msg));
        }


        private void UInt32ToByteArr(UInt32 val, Byte[] arr, int msbStart)
        {
            UInt32 Mask = 0x000000FF;
            // MSB to index 0
            for (int i = msbStart, shift = 24; i < (msbStart + 4); i++, shift -= 8)
            {
                arr[i] = (Byte)((val >> shift) & Mask);
            }
        }

        private void InvOpMsgArrv(RFID_MSGID msgID)
        {
            HRESULT_RFID hRes;

            switch (msgID)
            {
                case RFID_MSGID.RFIDMW_REQUEST_TYPE_MSGID_TagInv_AddATag:
                    if (DscvrTagEvent != null)
                        DscvrTagEvent(this, new DscvrTagEventArgs(
                            Rfid.st_RfidMw_AddATag_PecRec.m_Pc,
                            ref Rfid.st_RfidMw_AddATag_PecRec.m_Epc,
                            Rfid.st_RfidMw_AddATag_PecRec.m_Cnt,
                            ClslibRfidSp.RfidSp.RssiRegToDb(Rfid.st_RfidMw_AddATag_PecRec.m_Rssi),
                            Rfid.st_RfidMw_AddATag_PecRec.m_LastUpdated));
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_INVENTORY_CYCLE_BEGIN:
                    if (InvtryOpStEvent != null)
                        InvtryOpStEvent(this, new InvtryOpEventArgs(InvtryOpStatus.cycBegin));
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_INVENTORY_CYCLE_END:
                    if (InvtryOpStEvent != null)
                        InvtryOpStEvent(this, new InvtryOpEventArgs(InvtryOpStatus.cycEnd));
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory:
                    switch (msgID)
                    {
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                            hRes = Rfid.st_RfidSpReq_RadioCancelOperation.status; break;
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                            hRes = Rfid.st_RfidSpReq_RadioAbortOperation.status; break;
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory:
                            hRes = Rfid.st_RfidSpReq_18K6CTagInventory.status;
                            if (hRes == HRESULT_RFID.E_RFID_ERROR_OPERATION_CANCELLED)
                                hRes = HRESULT_RFID.S_OK; // override to avoid false positive
                            break;
                        default:
                            hRes = HRESULT_RFID.S_OK;
                            break;
                    }
                    // TBD: Power down the device
                    if (InvtryOpStEvent != null)
                    {
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            InvtryOpStEvent(this, new InvtryOpEventArgs(InvtryOpStatus.errorStopped,
                                hRes.ToString("F")));
                        }
                        else if (GetMacError(out macErrCode) && macErrCode != 0)
                        {
                            LogMacError();
                            InvtryOpStEvent(this, new InvtryOpEventArgs(InvtryOpStatus.macErrorStopped,
                               "Hardware Failure! (mac) Error code: 0x" + macErrCode.ToString("X4")));
                        }
                        else
                        {
                            InvtryOpStEvent(this, new InvtryOpEventArgs(InvtryOpStatus.stopped));
                        }
                    }
                    else if (msgID == RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory)
                        //MessageBox.Show("InvOpMsgArrv (" + msgID.ToString() + ") : but InvtryOpStEvent is NULL");
                        SpMsgWnd.MsgNotify -= InvOpMsgArrv;
                    SpMsgWnd.MsgNotifyUINT32 -= InvOpMsgArrv;
                    SpMsgWnd.MsgNotifyHRes -= InvOpMsgArrv;
                    CheckAndClear(ref InvtryStopReq); // Clear any pending Stop Request
                    break;
                default: // ignore the rest
                    break;
            }
        }

        private void InvOpMsgArrv(RFID_MSGID msgID, UInt32 arg1)
        {
            switch (msgID)
            {
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_ELAPSEDTM:
                    // arg1 is elapsed time since COMMAND-BEGIN
                    if (InvtryOpStEvent != null)
                        InvtryOpStEvent(this, new InvtryOpEventArgs(InvtryOpStatus.intervalTimeRpt, arg1));
                    break;
            }
        }

        private void InvOpMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            InvtryOpStatus status = InvtryOpStatus.none;
            bool RaiseEvt = false;
            String ErrMsg = null;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = InvtryOpStatus.errorStopped;
                        ErrMsg = "SetSelectCriteria : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref InvtryStopReq)) // pending stop request
                    {
                        status = InvtryOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = InvtryOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = InvtryOpStatus.errorStopped;
                        ErrMsg = "SetQueryParameters : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref InvtryStopReq)) // pending stop request
                    {
                        status = InvtryOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    else
                    {
                        status = InvtryOpStatus.configured;
                        RaiseEvt = true;
                        ClearMacError();
                        // Perform Inventory
                        Rfid.st_RfidSpReq_18K6CTagInventory.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                        Rfid.st_RfidSpReq_18K6CTagInventory.flags = 0;
                        if (invtryPerfSelCmd)
                            Rfid.st_RfidSpReq_18K6CTagInventory.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
                        Rfid.st_RfidSpReq_18K6CTagInventory.invenParms.common.tagStopCount = tagInvtryStopCnt;
                        SpMsgWnd.MsgNotify += InvOpMsgArrv;
                        SpMsgWnd.MsgNotifyUINT32 += InvOpMsgArrv;
                        hRes = CF.f_CFlow_RfidDev_GetInventory(ref Rfid.st_RfidSpReq_18K6CTagInventory);
                        if (SP.SUCCEEDED(hRes))
                        {
                            status = InvtryOpStatus.started;
                            RaiseEvt = true;
                        }
                        else
                        {
                            errCode = hRes;
                            RaiseEvt = true;
                            status = InvtryOpStatus.errorStopped;
                            SpMsgWnd.MsgNotify -= InvOpMsgArrv;
                            SpMsgWnd.MsgNotifyUINT32 -= InvOpMsgArrv;
                            throw new ApplicationException("CF.f_CFlow_RfidDev_GetInventory() returns: "
                                + hRes.ToString("F"));
                        }
                    }
                    break;
            }

            if (RaiseEvt && InvtryOpStEvent != null)
                InvtryOpStEvent(this, new InvtryOpEventArgs(status, ErrMsg));
            if (RaiseEvt && status == InvtryOpStatus.errorStopped)
                CheckAndClear(ref InvtryStopReq);
            if (status == InvtryOpStatus.errorStopped || status == InvtryOpStatus.configured
                || status == InvtryOpStatus.stopped || status == InvtryOpStatus.started)
                SpMsgWnd.MsgNotifyHRes -= InvOpMsgArrv; // will not continue
        }

        // Independent of RadioOpen or not as long as DLL is initialized
        public bool SetRepeatedTagObsrvMode(bool enable)
        {
            HRESULT_RFID hRes;

            hRes = (HRESULT_RFID)CF.f_CFlow_RfidMw_TagInv_SetMsgMode(enable ? 0 : 1);
            if (!SP.SUCCEEDED(hRes))
                errCode = hRes;
            return SP.SUCCEEDED(hRes);
        }

        private bool invtryPerfSelCmd = false;
        private bool TagInvtryStart(bool perfSelCmd, RadioStatusNotify radStatusNotify)
        {
            invtryPerfSelCmd = perfSelCmd;

            bool rv = false;
            // Open device first if not already done
            switch (radioStatus)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
                    if (!this.RadioOpen(TagInvtryDevStartNotify + radStatusNotify))
                        rv = false;
                    else
                        rv = true;
                    break;
                case RadioStatus.ClosePend:
                case RadioStatus.Closing:
                case RadioStatus.Opening:
                case RadioStatus.Configuring:
                    rv = false;
                    break;
                case RadioStatus.Opened:
                case RadioStatus.Configured:
                    radStatusNotify(RadioOpRes.StateChanged, radioStatus, null);
                    rv = true;
                    break;
            }
            return rv;
        }

        private bool TagInvtryStart(bool perfSelCmd)
        {
            return TagInvtryStart(perfSelCmd, TagInvtryContStartNotify);
        }

        public bool TagInvtryOnceStart(bool perfSelCmd)
        {
            tagInvtryStopCnt = TagStopCountUnlimited;
            return TagInvtryStart(perfSelCmd, TagInvtryOnceStartNotify);
        }

        public bool TagInvtryStart(RFID_18K6C_INVENTORY_SESSION session,
         uint qSize, Byte[] epcBankMask, uint maskOffset)
        {
            return TagInvtryStart(session, qSize, RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ, epcBankMask, maskOffset);
        }

        public bool TagInvtryStart(RFID_18K6C_INVENTORY_SESSION session,
            uint qSize, RFID_18K6C_SINGULATION_ALGORITHM qAlgo,
            Byte[] epcBankMask, uint maskOffset)
        {
            bool TargetSpecificGrp = (epcBankMask != null && epcBankMask.Length > 0);
            // Setup tagOperSelCrit and tagOperQryParm
            TagOperSetup(qAlgo, qSize, session,
                (TargetSpecificGrp) ?
                RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_B :
                RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A,
                /*(TargetSpecificGrp == false)*/ false, epcBankMask, maskOffset,
                (TargetSpecificGrp) ? RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA :
                RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);
            tagInvtryStopCnt = TagStopCountUnlimited;
            return TagInvtryStart(TargetSpecificGrp);
        }

        public bool TagInvtryFirstStart(RFID_18K6C_INVENTORY_SESSION session,
          uint qSize, Byte[] epcBankMask, uint maskOffset)
        {
            return TagInvtryFirstStart(session, qSize, RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ, epcBankMask, maskOffset);
        }

        public bool TagInvtryFirstStart(RFID_18K6C_INVENTORY_SESSION session,
           uint qSize, RFID_18K6C_SINGULATION_ALGORITHM qAlgo,
            Byte[] epcBankMask, uint maskOffset)
        {
            bool TargetSpecificGrp = (epcBankMask != null && epcBankMask.Length > 0);
            // Setup tagOperSelCrit and tagOperQryParm
            TagOperSetup(qAlgo, qSize, session,
                (TargetSpecificGrp) ?
                RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_B :
                RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A,
                false, epcBankMask, maskOffset,
                (TargetSpecificGrp) ? RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA :
                RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);
            tagInvtryStopCnt = TagStopCount1;
            return TagInvtryStart(TargetSpecificGrp);
        }

        public bool TagRangeStart(RFID_18K6C_INVENTORY_SESSION session,
            uint qSize)
        {
            return TagRangeStart(session, qSize, RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ);
        }

        public bool TagRangeStart(RFID_18K6C_INVENTORY_SESSION session,
            uint qSize, RFID_18K6C_SINGULATION_ALGORITHM qAlgo)
        {
            TagOperSetup(qAlgo, qSize, session, RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A,
                true, null, 0, RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);
            tagInvtryStopCnt = TagStopCountUnlimited;
            return TagInvtryStart(false);
        }

        public bool TagInvtryStop()
        {
            InvtryStopReq = true;
            return CancelCurrentOp();
        }

        public bool TagInvtryClr()
        {
            HRESULT_RFID hRes;

            hRes = CF.f_CFlow_RfidMw_TagInv_ClearAllTaglist();
            // Note: There is a return message that this code does not handle.
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                return false;
            }
            return true;
        }

        private void MwTagArrMsgArrv(RFID_MSGID msgID, UInt16[] PCArr, UINT96_T[] EPCArr,
            float[] RSSIArr, FileTime[] FTArr)
        {
            switch (msgID)
            {
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_GetAllTaglist:
                    this.SpMsgWnd.MsgNotifyTagArr -= MwTagArrMsgArrv;
                    DscvrTagEvent(this, new DscvrTagEventArgs(PCArr, EPCArr, RSSIArr, FTArr));
                    break;
                default:
                    break;
            }
        }
        // events are delegated through DscvTagEvent
        public bool TagInvtryLoad()
        {
            HRESULT_RFID hRes;

            this.SpMsgWnd.MsgNotifyTagArr += MwTagArrMsgArrv;
            hRes = CF.f_CFlow_RfidMw_TagInv_GetAllTaglist();
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                this.SpMsgWnd.MsgNotifyTagArr -= MwTagArrMsgArrv;
                return false;
            }
            return true;
        }

        public bool TagInvtryLoad(out UInt16[] PCArr, out UINT96_T[] EPCArr, out float[] RSSIArr,
            out FileTime[] FTArr)
        {
            HRESULT_RFID hRes = CF.f_CFlow_RfidMw_TagInv_GetAllTaglist(out PCArr, out EPCArr,
                out RSSIArr, out FTArr);
            if (!SP.SUCCEEDED(hRes))
                errCode = hRes;
            return SP.SUCCEEDED(hRes);
        }

        public bool TagInvtryAbort()
        {
            HRESULT_RFID hRes;

            Rfid.st_RfidSpReq_RadioAbortOperation.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioAbortOperation.flags = 0; // reserved
            hRes = CF.f_CFlow_RfidDev_Abort(ref Rfid.st_RfidSpReq_RadioAbortOperation);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                // TBD: Go to Power-Save mode?
                return false;
            }
            return true;
        }
        #endregion

        #region Tag Read Request
        private UInt32 TagReadPasswd = 0;
        private UInt32 TagReadStopCnt = TagStopCountUnlimited;
        private ReqTagRdBnkInfo NxtBnkToRd;

        private bool TagRdStart()
        {
            bool rv = false;
            // Open device first if not already done
            switch (radioStatus)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
                    if (!this.RadioOpen(TagRdDevStartNotify))
                    {
                        rv = false;
                    }
                    else
                        rv = true;
                    break;
                case RadioStatus.ClosePend:
                case RadioStatus.Closing:
                case RadioStatus.Opening:
                case RadioStatus.Configuring:
                    rv = false;
                    break;
                case RadioStatus.Opened:
                case RadioStatus.Configured:
                    TagRdDevStartNotify(RadioOpRes.StateChanged, radioStatus, null);
                    rv = true;
                    break;
            }
            return rv;
        }

        public bool TagReadBanksStart(uint qSize, ref UINT96_T epc, ReqTagRdBnkInfo cb, UInt32 accPwd)
        {
            this.TagReadStopCnt = TagStopCount1;
            this.TagReadPasswd = accPwd;
            this.NxtBnkToRd = cb;

            // SingulationSettingMatrix.xls for reason
            RFID_18K6C_INVENTORY_SESSION sess = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0;

#if TAGRD_USES_SELECT
             // Try to use SL flag instead of session: drawback, only one reader is available
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                1, RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ON, epc.ToByteArray(), 2, RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);
#else
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                qSize, sess, epc.ToByteArray(), 0, false); // offset is 0 (because the offset starts from EPC for singulation mask)
#endif
            return TagRdStart();
        }

        public bool TagScanRdStart(uint qSize, ReqTagRdBnkInfo cb)
        {
            this.TagReadStopCnt = TagStopCountUnlimited;
            this.TagReadPasswd = 0;
            this.NxtBnkToRd = cb;

            // SingulationSettingMatrix.xls for reason
            RFID_18K6C_INVENTORY_SESSION sess = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0;

            // Tag Read Operation Only support Fixed Q
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                qSize, sess, RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A,
                true, null, 0, RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);

            return TagRdStart();
        }

        public bool TagScanRdStart(uint qSize, Byte[] epcBnkMask, uint maskOffset, ReqTagRdBnkInfo cb)
        {
            if (epcBnkMask == null || epcBnkMask.Length == 0)
                throw new ApplicationException("Mask is Empty");
            this.TagReadStopCnt = TagStopCountUnlimited;
            this.TagReadPasswd = 0;
            this.NxtBnkToRd = cb;

#if TAGRD_USES_SELECT
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ, qSize, sess, RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_B, false, epcBnkMask, maskOffset, RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA);
#else
            // Convert EPCBnkMask to EPCMask
            Byte[] EPCMask = null;
            uint Wdoffset = 0; // from the beginning of EPC Data Start
            if (maskOffset == 0 && epcBnkMask.Length > 4)
            {
                Wdoffset = 0;
                EPCMask = new Byte[epcBnkMask.Length - 4];
            }
            else if (maskOffset == 1 && epcBnkMask.Length > 2) // starts from
            {
                Wdoffset = 0;
                EPCMask = new Byte[epcBnkMask.Length - 2];
            }
            else if (maskOffset >= 2 && epcBnkMask.Length > 0)
            {
                Wdoffset = maskOffset - 2;
                EPCMask = epcBnkMask;
            }
            // SingulationSettingMatrix.xls for reason
            RFID_18K6C_INVENTORY_SESSION sess = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0;

            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                qSize, sess, EPCMask, Wdoffset, false);

#endif
            return TagRdStart();

        }

        private bool CancelCurrentOp()
        {
            HRESULT_RFID hRes;

            Rfid.st_RfidSpReq_RadioCancelOperation.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioCancelOperation.flags = 0;
            hRes = CF.f_CFlow_RfidDev_Cancel(ref Rfid.st_RfidSpReq_RadioCancelOperation);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                // TBD: Go to Power-Save mode?
                return false;
            }
            return true;
        }

        #region  CancelOperation() after Tag Inventory stopped already (Testing only)

        private void CancelRunningOpMsgArrv(RFID_MSGID msgID)
        {
            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                    //MessageBox.Show("Cancel Running Op: " + Rfid.st_RfidSpReq_RadioCancelOperation.status.ToString());
                    SpMsgWnd.MsgNotify -= CancelRunningOpMsgArrv;
                    break;
            }
        }

        public bool CancelRunningOp()
        {
            if (!RadioReady())
                return false;
            SpMsgWnd.MsgNotify += CancelRunningOpMsgArrv;
            bool Dispatched = CancelCurrentOp();
            if (!Dispatched)
            {
                SpMsgWnd.MsgNotify -= CancelRunningOpMsgArrv;
            }
            return Dispatched;
        }

        #endregion

        public bool TagReadStop()
        {
            TagRdStopReq = true;
            return CancelCurrentOp();
        }

        private void TagRdDevStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
           AccErrorTypes TAErr =  AccErrorTypes.None;
            RdOpStatus OpStatus = RdOpStatus.error;
            String ErrMsg = msg;
            bool RaiseEvt = false;
            bool StartRdOp = false;

            // process notification from RadioOpen
            switch (res)
            {
                case RadioOpRes.None:
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opening:
                        case RadioStatus.Configuring:
                            // keep going
                            break;
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            // Radio ready to use
#if false //not really started until SELECT/QUERY options set
                            OpStatus = RdOpStatus.started;
                            RaiseEvt = true;
#endif
                            StartRdOp = true;
                            break;
                        case RadioStatus.Closed:
                        case RadioStatus.ErrorStopped:
                            OpStatus = RdOpStatus.stopped;
                            RaiseEvt = true;
                            break;
                        case RadioStatus.PowerSaved:
                        case RadioStatus.Closing:
                        case RadioStatus.ClosePend:
                            OpStatus = RdOpStatus.error;
                            RaiseEvt = true;
                            break;
                    }
                    break;
                case RadioOpRes.Warning:
                    // do nothing
                    RaiseEvt = false;
                    break;
                case RadioOpRes.Error:
                    if (radioStatus == RadioStatus.ErrorStopped)
                        OpStatus = RdOpStatus.errorStopped;
                    else
                        OpStatus = RdOpStatus.error;
                    RaiseEvt = true;
                    break;
            }//switch
            if (CheckAndClear(ref TagRdStopReq))
            {
                OpStatus = RdOpStatus.stopped;
                RaiseEvt = true;
            }
            else if (StartRdOp)
            {
                // Set up SELECT/QUERY options for ReadOp 
                HRESULT_RFID hRes;
                SpMsgWnd.MsgNotifyHRes += RdOpMsgArrv;
                SetNonContOperMode(out hRes); // Non-Continuous Operation mode
#if TAGRD_USES_SELECT
                if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                {
                    hRes = TagSelectCritSet(tagOperSelCrit);
                }
#else
                if (tagOperPostMatchCrit != null && tagOperPostMatchCrit.Length > 0)
                {
                    hRes = TagPostMatchCritSet(tagOperPostMatchCrit);
                }
#endif
                else // There is no SELECT/Post-Match criteria to set, go straight to Query Parameter setup
                {
                    hRes = QueryParmsSet(ref tagOperQryParm);
                }
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = RdOpStatus.errorStopped;
                    SpMsgWnd.MsgNotifyHRes -= RdOpMsgArrv;
#if TAGRD_USES_SELECT
                    throw new ApplicationException("CF.f_CFlow_RfidDev_SetSelectCriteria() returns: "
                                           + hRes.ToString("F"));
#else
                    throw new ApplicationException("CF.f_CFlow_RfidDev_SetPostMatchCriteria() returns: "
                                   + hRes.ToString("F"));
#endif
                }
            }

            // raise RdOpStEvent if appropiate
            if (RaiseEvt && (RdOpStEvent != null))
                RdOpStEvent(this, new RdOpEventArgs(OpStatus, ErrMsg, TAErr));
            if (RaiseEvt && (OpStatus == RdOpStatus.errorStopped))
                CheckAndClear(ref TagRdStopReq);
        }


        /// <summary>
        /// Perform Inventory to issue SELECT command. Assuming that
        /// 'Non-Continuous' Operation mode is already set by caller
        /// </summary>
        /// <returns></returns>
        private HRESULT_RFID TagAccMissingSelectGetAround()
        {
            HRESULT_RFID hRes;

            Rfid.st_RfidSpReq_18K6CTagInventory.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CTagInventory.flags = (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
            Rfid.st_RfidSpReq_18K6CTagInventory.invenParms.common.tagStopCount = tagInvtryStopCnt;
            hRes = CF.f_CFlow_RfidDev_GetInventory(ref Rfid.st_RfidSpReq_18K6CTagInventory);

            return hRes;
        }


        /// <summary>
        ///  Issue actual TagRead command. Assuming that the caller 
        /// has already Setup SELECT/QUERY options
        /// </summary>
        /// <param name="bnk"></param>
        /// <param name="perfSelCmd"></param>
        /// <param name="hRes"></param>
        /// <returns></returns>
        private bool ReadBank(RFID_18K6C_MEMORY_BANK bnk, bool UseSelOrPostMatchCrit, out HRESULT_RFID hRes)
        {
            ClearMacError();

            Rfid.st_RfidSpReq_18K6CTagRead.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CTagRead.flags = 0; //default to 0

            if (UseSelOrPostMatchCrit)
            {
#if TAGRD_USES_SELECT
                Rfid.st_RfidSpReq_18K6CTagRead.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
#else
                Rfid.st_RfidSpReq_18K6CTagRead.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_POST_MATCH;
#endif
            }

            Rfid.st_RfidSpReq_18K6CTagRead.readParms.accessPassword = TagReadPasswd;
            // Setting count = 0 : cause problem (such as no INVENTORY 
            // packet before TAG_ACCESS packet, Error bits always on)
            // This is in contradiction to Intel Doc.
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.common.tagStopCount = TagReadStopCnt;
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.bank = bnk;
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.count = EPCTag.MaxBnkMemSizeInWds(bnk);
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.offset = 0;

            hRes = CF.f_CFlow_RfidDev_18K6CTagRead(ref Rfid.st_RfidSpReq_18K6CTagRead);
            return SP.SUCCEEDED(hRes);
        }

        private bool ReadBank(RFID_18K6C_MEMORY_BANK bnk, ushort wdOffset, ushort wdCnt,
            bool UseSelOrPostMatchCrit, out HRESULT_RFID hRes)
        {
            ClearMacError();

            Rfid.st_RfidSpReq_18K6CTagRead.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CTagRead.flags = 0; //default to 0

            if (UseSelOrPostMatchCrit)
            {
#if TAGRD_USES_SELECT
                Rfid.st_RfidSpReq_18K6CTagRead.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
#else
                Rfid.st_RfidSpReq_18K6CTagRead.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_POST_MATCH;
#endif
            }
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.accessPassword = TagReadPasswd;
            // Setting count = 0 : cause problem (such as no INVENTORY 
            // packet before TAG_ACCESS packet, Error bits always on)
            // This is in contradiction to Intel Doc.
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.common.tagStopCount = TagReadStopCnt;
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.bank = bnk;
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.count = wdCnt;
            Rfid.st_RfidSpReq_18K6CTagRead.readParms.offset = wdOffset;

            hRes = CF.f_CFlow_RfidDev_18K6CTagRead(ref Rfid.st_RfidSpReq_18K6CTagRead);
            return SP.SUCCEEDED(hRes);


        }

        private void RdOpMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            bool RaiseEvt = false;
            RdOpStatus status = RdOpStatus.error;
           AccErrorTypes TAErr = AccErrorTypes.None;
            String ErrMsg = null;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = RdOpStatus.errorStopped;
                        RaiseEvt = true;
                        ErrMsg = "SetSelectCrit: " + hRes.ToString("F");
                    }
                    else if (CheckAndClear(ref TagRdStopReq))
                    {
                        RaiseEvt = true;
                        status = RdOpStatus.stopped;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = RdOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetPostMatchCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = RdOpStatus.errorStopped;
                        RaiseEvt = true;
                        ErrMsg = "SetPostMatchCrit: " + hRes.ToString("F");
                    }
                    else if (CheckAndClear(ref TagRdStopReq))
                    {
                        RaiseEvt = true;
                        status = RdOpStatus.stopped;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = RdOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = RdOpStatus.errorStopped;
                        ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagRdStopReq))
                    {
                        RaiseEvt = true;
                        status = RdOpStatus.stopped;
                    }
                    else
                    {
                        status = RdOpStatus.configured;
                        RaiseEvt = true;
                        // Perform GetAround to issue SELECT command if necessary
                        bool PerfSelect = (tagOperSelCrit != null) && (tagOperSelCrit.Length > 0);
                        if (PerfSelect)
                        {
                            SpMsgWnd.MsgNotify += RdOpMsgArrv;
                            hRes = TagAccMissingSelectGetAround();
                            if (!SP.SUCCEEDED(hRes))
                            {
                                errCode = hRes;
                                status = RdOpStatus.errorStopped;
                                ErrMsg = "TagAccMissingSelectGetAround: " + hRes.ToString("F");
                                RaiseEvt = true;
                                SpMsgWnd.MsgNotify -= RdOpMsgArrv;
                            }
                        }
                        else // or with Post Match
                        {
                            // Issue Tag Read (for the first bank)
                            MemoryBanks4Op NxtBnk;
                            ushort RdWdOffset = 0, RdWdCnt = 0;
                            // ask for it
                            if ((NxtBnkToRd == null) || (NxtBnkToRd(out NxtBnk, out RdWdOffset, out RdWdCnt) == false))
                            {
                                NxtBnk = MemoryBanks4Op.None;
                            }
                            if (NxtBnk == MemoryBanks4Op.None)
                            {
                                ErrMsg = "No bank selected to Read";
                                status = RdOpStatus.errorStopped;
                                RaiseEvt = true;
                            }
                            else
                            {
                                RFID_18K6C_MEMORY_BANK bnk = EPCTag.MemoryBanks4OpTo18K6CBank(NxtBnk);
                                SpMsgWnd.MsgNotify += RdOpMsgArrv;
                                bool PerfPostMatch = (tagOperPostMatchCrit != null) && (tagOperPostMatchCrit.Length > 0);
                                if (!ReadBank(bnk, RdWdOffset, RdWdCnt, PerfPostMatch, out hRes))
                                {
                                    errCode = hRes;
                                    status = RdOpStatus.errorStopped;
                                    ErrMsg = "CF.f_CFlow_RfidDev_TagRead() returns: "
                                        + hRes.ToString("F");
                                    RaiseEvt = true;
                                    SpMsgWnd.MsgNotify -= RdOpMsgArrv;
                                }
                                else
                                {
                                    status = RdOpStatus.started;
                                    RaiseEvt = true;
                                }
                            }
                        }
                    }
                    break;
            }
            if (RaiseEvt && (RdOpStEvent != null))
                RdOpStEvent(this, new RdOpEventArgs(status, ErrMsg, TAErr));
            if (status == RdOpStatus.errorStopped || status == RdOpStatus.configured
                || status == RdOpStatus.stopped || status == RdOpStatus.started)
                SpMsgWnd.MsgNotifyHRes -= RdOpMsgArrv;
            if (RaiseEvt && (status == RdOpStatus.errorStopped || status == RdOpStatus.stopped))
                CheckAndClear(ref TagRdStopReq);
        }

        private void RdOpMsgArrv(RFID_MSGID msgID)
        {
            String ErrMsg = null;
            AccErrorTypes TAErr =  AccErrorTypes.None;
            RdOpStatus OpStatus = RdOpStatus.error;
            bool RaiseEvt = false;
            UINT96_T EPCRead;
            UInt16 PCRead;
            HRESULT_RFID hRes;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory:
                    hRes = Rfid.st_RfidSpReq_18K6CTagInventory.status;
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        OpStatus = RdOpStatus.errorStopped;
                        ErrMsg = "Tag Inventory (AccSelectGetAround) failed: " + hRes.ToString();
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagRdStopReq))
                    {
                        OpStatus = RdOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    else
                    {
                        // Issue Tag Read (for the first bank)
                        MemoryBanks4Op NxtBnk;
                        ushort RdWdOffset = 0, RdWdCnt = 0;
                        if ((NxtBnkToRd == null) || (NxtBnkToRd(out NxtBnk, out RdWdOffset, out RdWdCnt) == false))
                        {
                            NxtBnk = MemoryBanks4Op.None;
                        }
                        if (NxtBnk == MemoryBanks4Op.None)
                        {
                            ErrMsg = "No bank selected to Read";
                            OpStatus = RdOpStatus.errorStopped;
                            RaiseEvt = true;
                        }
                        else
                        {
                            RFID_18K6C_MEMORY_BANK bnk = EPCTag.MemoryBanks4OpTo18K6CBank(NxtBnk);
                            bool PerfSelCmd = (tagOperSelCrit != null) && (tagOperSelCrit.Length > 0);
                            if (!ReadBank(bnk, RdWdOffset, RdWdCnt, PerfSelCmd, out hRes))
                            {
                                errCode = hRes;
                                OpStatus = RdOpStatus.errorStopped;
                                ErrMsg = "CF.f_CFlow_RfidDev_TagRead() returns: "
                                    + hRes.ToString("F");
                                RaiseEvt = true;
                            }
                            else
                            {
                                OpStatus = RdOpStatus.started;
                                RaiseEvt = true;
                            }
                        }
                    }
                    if (RaiseEvt == true && OpStatus == RdOpStatus.errorStopped)
                        SpMsgWnd.MsgNotify -= RdOpMsgArrv;
                    break;
                case RFID_MSGID.RFIDMW_REQUEST_TYPE_MSGID_TagInv_AddATag:
                    // Singulated Tag, Do nothing at this point.
                    RaiseEvt = false;
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_TAG_ACCESS:
                    EPCRead = Rfid.st_RfidMw_AddATag_PecRec.m_Epc;
                    PCRead = Rfid.st_RfidMw_AddATag_PecRec.m_Pc;
                    try
                    {
#if TAGWR_USES_SELECT
                        if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                        {
                            // Assuming that 'SELECT' was issued and completed successfully
                            if (!TagIDSelCritMet(PCRead, ref EPCRead, tagOperSelCrit[0].mask.mask,
                                (ushort)(tagOperSelCrit[0].mask.offset/16)))
                            {
                                throw new ApplicationException("ID of read Tag does not match SELECT mask\r\n:"
                                                + "Mask@offset( " + tagOperSelCrit[0].mask.offset
                                                + "): " + ByteArrToHexStr(tagOperSelCrit[0].mask.mask) + "\r\n"
                                                + "Accessed PC: " + PCRead.ToString("X4") + "\r\n"
                                                + "Accessed EPC: " + EPCRead.ToString());
                            }
                        }
#else
                        if (tagOperPostMatchCrit != null && tagOperPostMatchCrit.Length > 0)
                        {
                            // Assuming that 'Post Match Criteria' is setup without error
                            if (!TagIDPostMatchCritMet((tagOperPostMatchCrit[0].match != 0),
                                ref EPCRead, tagOperPostMatchCrit[0].mask.mask,
                                (ushort)(tagOperPostMatchCrit[0].mask.offset / 16)))
                            {
                                throw new ApplicationException("ID of read Tag does not match PostMatch mask\r\n:"
                                               + "Mask@offset( " + tagOperPostMatchCrit[0].mask.offset
                                               + "): " + ByteArrToHexStr(tagOperPostMatchCrit[0].mask.mask) + "\r\n"
                                               + "Accessed PC: " + PCRead.ToString("X4") + "\r\n"
                                               + "Accessed EPC: " + EPCRead.ToString());
                            }
                        }
#endif
                        // Beam the event to UI thread
                        TAErr = EPCTag.TagAccPktGetErrType(ref Rfid.st_RfidSpPkt_TagAccessData);
                        if (TAErr != AccErrorTypes.None) // temp implementation
                        {
                            // Report Error
                            ErrMsg = EPCRead.ToString();
                            OpStatus = RdOpStatus.tagAccError;
                            RaiseEvt = true;
                        }
                        else
                        {
                            MemBnkRdEvent(this, new MemBnkRdEventArgs(
                                ref Rfid.st_RfidMw_AddATag_PecRec.m_Crc,
                                ref Rfid.st_RfidMw_AddATag_PecRec.m_Pc,
                                ref Rfid.st_RfidMw_AddATag_PecRec.m_Epc,
                                Rfid.st_RfidSpReq_18K6CTagRead.readParms.bank,
                                EPCTag.TagAccPktGetDataStr(ref Rfid.st_RfidSpPkt_TagAccessData),
                                Rfid.st_RfidMw_AddATag_PecRec.m_Rssi));
                        }
                    }
                    catch (ApplicationException e)
                    {
                        // Exception could have come from deeper in the stack
                        ErrMsg = e.Message + "\r\n"
                                        + "Accessed PC: " + PCRead.ToString("X4") + "\r\n"
                                        + "Accessed EPC: " + EPCRead.ToString();
                        OpStatus = RdOpStatus.error;
                        RaiseEvt = true;
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagRead:
                    hRes = Rfid.st_RfidSpReq_18K6CTagRead.status;
                    bool UserCancel = false;
                    if (hRes == HRESULT_RFID.E_RFID_ERROR_OPERATION_CANCELLED)
                    {
                        UserCancel = true;
                        hRes = HRESULT_RFID.S_OK; // override to avoid false positive
                    }
                    if (SP.SUCCEEDED(hRes))
                    {
                        bool HasFatalErr = false;
                        if (this.GetMacError(out macErrCode) && macErrCode != 0)
                        {
                            LogMacError();
                            // filter out the non-fatal error here (instead of caller object, such as TagInvtry)
                            if (RFIDRdr.MacErrorIsFatal(macErrCode))
                            {
                                OpStatus = RdOpStatus.errorStopped;
                                ErrMsg = "Hardware Failure! (mac) Error code: 0x: " + macErrCode.ToString("X4");
                                RaiseEvt = true;
                                HasFatalErr = true;
                            }
                        }
                        if (!HasFatalErr)
                        {
                            // Assuming that error is already detected in RFID_PACKET_TYPE_MSGID_18K6C_TAG_ACCESS
                            // There is no point repeating here.
                            // Emit Read complete event now 
                            OpStatus = RdOpStatus.completed;
                            RdOpStEvent(this, new RdOpEventArgs(OpStatus, null, AccErrorTypes.None));
                            // Set up Next Field to Read, if necessary
                            MemoryBanks4Op NxtBnk;
                            ushort RdWdOffset = 0, RdWdCnt = 0;
                            if ((NxtBnkToRd == null) || (NxtBnkToRd(out NxtBnk, out RdWdOffset, out RdWdCnt) == false))
                            {
                                NxtBnk = MemoryBanks4Op.None;
                            }
                            if ((UserCancel == false) && (NxtBnk != MemoryBanks4Op.None))
                            {
                                // Add this Handler to Event Again to continue handling Tag Read Op
                                SpMsgWnd.MsgNotify += RdOpMsgArrv;
                                // Events
                                RFID_18K6C_MEMORY_BANK Bnk = EPCTag.MemoryBanks4OpTo18K6CBank(NxtBnk);
                                if (!ReadBank(Bnk, RdWdOffset, RdWdCnt, true, out hRes)) // SELECT is already setup and performed
                                {
                                    errCode = hRes;
                                    // fatal error
                                    SpMsgWnd.MsgNotify -= RdOpMsgArrv; // rollback
                                    ErrMsg = "CF.f_CFlow_RfidDev_TagRead() returns: "
                                                           + hRes.ToString("F");
                                    OpStatus = RdOpStatus.errorStopped;
                                    RaiseEvt = true;
                                }
                            }
                            else // no more banks to read, stop device
                            {
                                // TBD: Power down device to save power
                                OpStatus = RdOpStatus.stopped;
                                RaiseEvt = true;
                            }
                        }
                    }
                    else
                    {
                        // TagRead error, report error
                        errCode = hRes;
                        OpStatus = RdOpStatus.errorStopped;
                        ErrMsg = "Tag Read Error: " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    // Match the Addition with each ReadBank()
                    SpMsgWnd.MsgNotify -= RdOpMsgArrv;
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                    hRes = HRESULT_RFID.E_FAIL;
                    switch (msgID)
                    {
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                            hRes = Rfid.st_RfidSpReq_RadioCancelOperation.status; break;
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                            hRes = Rfid.st_RfidSpReq_RadioAbortOperation.status; break;
                    }
                    // Stop the device
                    // TBD: Power down device to save power
                    if (!SP.SUCCEEDED(hRes))
                    {
                        ErrMsg = "Tag Read Error: " + hRes.ToString("F");
                        OpStatus = RdOpStatus.errorStopped;
                    }
                    else
                        OpStatus = RdOpStatus.stopped;
                    RaiseEvt = true;
                    // In case RFID_REQEND_TYPE_MSGID_18K6CTagRead was not received
                    SpMsgWnd.MsgNotify -= RdOpMsgArrv;
                    break;
                default: // ignore the rest
                    break;
            }

            if (RaiseEvt && (RdOpStEvent != null))
                RdOpStEvent(this, new RdOpEventArgs(OpStatus, ErrMsg, TAErr));
            if (RaiseEvt && (OpStatus == RdOpStatus.errorStopped || OpStatus == RdOpStatus.stopped))
            {
                CheckAndClear(ref TagRdStopReq);
                SpMsgWnd.MsgNotify -= RdOpMsgArrv;
            }

        }
        #endregion

        #region Tag Write Routines
        UInt32 TagWritePasswd = 0;
        UInt32 TagWriteStopCnt = TagStopCountUnlimited;

        private ReqTagWrBnkData NxtBnkDataToWr;


        private bool TagWrStart(ReqTagWrBnkData tagWrDel)
        {
            NxtBnkDataToWr = tagWrDel;

            bool rv = false;
            switch (radioStatus)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
                    if (!this.RadioOpen(TagWrDevStartNotify))
                    {
                        NxtBnkDataToWr = null;
                        rv = false;
                    }
                    else
                        rv = true;
                    break;
                case RadioStatus.ClosePend:
                case RadioStatus.Closing:
                case RadioStatus.Opening:
                case RadioStatus.Configuring:
                    rv = false;
                    break;
                case RadioStatus.Opened:
                case RadioStatus.Configured:
                    TagWrDevStartNotify(RadioOpRes.StateChanged, radioStatus, null);
                    rv = true;
                    break;
            }
            return rv;
        }

        /// <summary>
        /// Write to a Tag of particular EPC
        /// </summary>
        /// <param name="epc"></param>
        /// <param name="tagWrDel"></param>
        /// <returns></returns>
        public bool TagWriteStart(uint qSize,
            ref UINT96_T epc, ReqTagWrBnkData tagWrDel)
        {
            return TagWriteStart(qSize, ref epc, tagWrDel, 0);
        }

        /// <summary>
        /// Write to 'one' Tag of particular EPC with Password
        /// Supposedly only one of the duplicate tags would be written
        /// </summary>
        /// <param name="epc"></param>
        /// <param name="tagWrDel"></param>
        /// <param name="accPwd"></param>
        /// <returns></returns>
        public bool TagWriteStart(uint qSize, ref UINT96_T epc, ReqTagWrBnkData tagWrDel, UInt32 accPwd)
        {
            // SingulationSettingMatrix.xls for reason
            RFID_18K6C_INVENTORY_SESSION sess = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0;

#if TAGWR_USES_SELECT
          TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
            1, RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ON, epc.ToByteArray(), 
            (uint)(EPCTag.CRC16FldSz + EPCTag.PCFldSz), 
            RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);
#else
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                qSize, sess, epc.ToByteArray(), 0, false);
#endif
            TagWriteStopCnt = TagStopCount1; // to avoid 2 tags of the same EPC being written
            TagWritePasswd = accPwd;
            NxtBnkDataToWr = tagWrDel;
            return TagWrStart(tagWrDel);
        }

        /// <summary>
        /// Write to any Tag (Stop after One)
        /// </summary>
        /// <param name="tagWrDel"></param>
        /// <returns></returns>
        public bool TagWriteStart(ReqTagWrBnkData tagWrDel)
        {
            return TagWriteStart(tagWrDel, 0);
        }

        /// <summary>
        /// Write to any Tag (Stop after One) with password
        /// </summary>
        /// <param name="tagWrDel"></param>
        /// <param name="accPwd"></param>
        /// <returns></returns>
        public bool TagWriteStart(ReqTagWrBnkData tagWrDel, UInt32 accPwd)
        {
            // SingulationSettingMatrix.xls for reason
            RFID_18K6C_INVENTORY_SESSION sess = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0;
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                1, sess);
            TagWriteStopCnt = TagStopCount1;
            TagWritePasswd = accPwd;
            NxtBnkDataToWr = tagWrDel;
            return TagWrStart(tagWrDel);
        }

        /// <summary>
        /// Write to any Tags (as many as singulated)
        /// </summary>
        /// <param name="tagWrDel"></param>
        /// <param name="qSize"></param>
        /// <returns></returns>
        public bool TagWriteStart(ReqTagWrBnkData tagWrDel, UInt16 qSize)
        {
            return TagWriteStart(tagWrDel, qSize, 0);
        }

        /// <summary>
        /// Write to any Tags (as many as singulated) with password
        /// </summary>
        /// <param name="tagWrDel"></param>
        /// <param name="qSize"></param>
        /// <param name="accPwd"></param>
        /// <returns></returns>
        public bool TagWriteStart(ReqTagWrBnkData tagWrDel, UInt16 qSize, UInt32 accPwd)
        {
            // SingulationSettingMatrix.xls for reason
            RFID_18K6C_INVENTORY_SESSION sess = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0;
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                qSize, sess);
            TagWriteStopCnt = TagStopCountUnlimited;
            TagWritePasswd = accPwd;
            NxtBnkDataToWr = tagWrDel;
            return TagWrStart(tagWrDel);
        }

        /// <summary>
        /// Write Tag with Inverse-Post-Match Criteria
        /// </summary>
        /// <param name="bnk1InvMatchMask"></param>
        /// <param name="maskOffset">position from start of bank 1</param>
        /// <param name="tagWrDel"></param>
        /// <param name="qSize"></param>
        /// <returns></returns>
        public bool TagWriteStart(Byte[] epcInvMatchMask, ushort maskOffset,
            ReqTagWrBnkData tagWrDel, UInt16 qSize)
        {
            return TagWriteStart(epcInvMatchMask, maskOffset, tagWrDel, qSize, 0);
        }

        /// <summary>
        /// Write Tag with Inverse-Post-Match Criteria (and Access Password)
        /// </summary>
        /// <param name="bnk1InvMatchMask"></param>
        /// <param name="maskOffset">position from start of bank 1</param>
        /// <param name="tagWrDel"></param>
        /// <param name="qSize"></param>
        /// <param name="accPwd"></param>
        /// <returns></returns>
        public bool TagWriteStart(Byte[] epcInvMatchMask, ushort maskOffset,
            ReqTagWrBnkData tagWrDel, UInt16 qSize, UInt32 accPwd)
        {
            // SingulationSettingMatrix.xls for reason
            RFID_18K6C_INVENTORY_SESSION sess = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0;
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                qSize, sess, epcInvMatchMask, maskOffset, true);
            TagWriteStopCnt = TagStopCount1;
            TagWritePasswd = accPwd;
            NxtBnkDataToWr = tagWrDel;
            return TagWrStart(tagWrDel);
        }


        public bool TagWriteStop()
        {
            TagWrStopReq = true;
            return CancelCurrentOp();
        }

        private void TagWrDevStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            WrOpStatus OpStatus = WrOpStatus.error;
            String ErrMsg = msg;
            bool RaiseEvt = false;
            bool StartWrOp = false;

            // process notification from DevStart
            switch (res)
            {
                case RadioOpRes.None:
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opening:
                        case RadioStatus.Configuring:
                            // keep going
                            break;
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            // Radio ready to use
                            // Defer 'started' status until TagWrite is really issued
                            //OpStatus = WrOpStatus.started;
                            //RaiseEvt = true;
                            // Start Write Operation
                            StartWrOp = true;
                            break;
                        case RadioStatus.Closed:
                        case RadioStatus.ErrorStopped:
                            OpStatus = WrOpStatus.stopped;
                            RaiseEvt = true;
                            break;
                        case RadioStatus.PowerSaved:
                        case RadioStatus.Closing:
                        case RadioStatus.ClosePend:
                            OpStatus = WrOpStatus.error;
                            RaiseEvt = true;
                            break;
                    }
                    break;
                case RadioOpRes.Warning:
                    // do nothing
                    RaiseEvt = false;
                    break;
                case RadioOpRes.Error:
                    if (radioStatus == RadioStatus.ErrorStopped)
                        OpStatus = WrOpStatus.errorStopped;
                    else
                        OpStatus = WrOpStatus.error;
                    RaiseEvt = true;
                    break;
            } // switch
            if (CheckAndClear(ref TagWrStopReq))
            {
                RaiseEvt = true;
                OpStatus = WrOpStatus.stopped;
            }
            else if (StartWrOp)
            {
                // Start Actual Write Process (1st Bank)
                String WrDataStr;
                MemoryBanks4Op BnkOp;
                ushort WrWdOffset;
                if (NxtBnkDataToWr(out BnkOp, out WrWdOffset, out WrDataStr))
                {
                    // Set up SELECT/QUERY options for WriteOp 
                    HRESULT_RFID hRes;
                    SpMsgWnd.MsgNotifyHRes += WrOpMsgArrv;
                    SetNonContOperMode(out hRes); // Non-Continuous Operation mode
#if TAGWR_USES_SELECT
                     if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                    {
                        hRes = TagSelectCritSet(tagOperSelCrit);
                    }
#else
                    if (tagOperPostMatchCrit != null && tagOperPostMatchCrit.Length > 0)
                    {
                        hRes = TagPostMatchCritSet(tagOperPostMatchCrit);
                    }
#endif
                    else// There is no SELECT/Post-Match criteria to set, go straight to Query Parameter setup
                    {
                        hRes = QueryParmsSet(ref tagOperQryParm);
                    }
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        RaiseEvt = true;
                        OpStatus = WrOpStatus.errorStopped;
                        SpMsgWnd.MsgNotifyHRes -= WrOpMsgArrv;
#if TAGRD_USES_SELECT
                        throw new ApplicationException("CF.f_CFlow_RfidDev_SetSelectCriteria() returns: "
                                           + hRes.ToString("F"));
#else
                        throw new ApplicationException("CF.f_CFlow_RfidDev_SetPostMatchCriteria() returns: "
                                       + hRes.ToString("F"));
#endif
                    }
                }
                else
                {
                    OpStatus = WrOpStatus.errorStopped;
                    ErrMsg = "No suitable banks to write";
                    RaiseEvt = true;
                }
            }

            // Raise WrOpStEvent if appropiate
            if (RaiseEvt && (WrOpStEvent != null))
                WrOpStEvent(this, new WrOpEventArgs(OpStatus, ErrMsg));
            if (RaiseEvt && (OpStatus == WrOpStatus.errorStopped || OpStatus == WrOpStatus.stopped))
                CheckAndClear(ref TagWrStopReq);

        }

        private bool TagWriteMarshalingParamTest(out HRESULT_RFID hRes)
        {
            RFID_18K6C_MEMORY_BANK bnk;

            Rfid.st_RfidSpReq_18K6CTagWrite.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.common.tagStopCount = 9998887;
            Rfid.st_RfidSpReq_18K6CTagWrite.flags = 0xabcdef01; // TBD: if bnk != None, do Post-Match
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.accessPassword = 30624700; // TBD
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.verify = 1; // read-after-write
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.verifyRetryCount = 99;

#if false
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.writeType = RFID_18K6C_WRITE_TYPE.RFID_18K6C_WRITE_TYPE_SEQUENTIAL;
    
            bnk = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_TID;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.bank = bnk;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.count = 8;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.offset = 0xa596;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData = new ushort[SP.RFID_18K6C_MAX_TAG_WRITE_WORDS];
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[0] = 0xbeef;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[1] = 0x3062;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[2] = 0x4770;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[3] = 0x5342;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[4] = 0x0213;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[5] = 0x9431;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[6] = 0x3424;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData[7] = 0xcafe;
#else
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.writeType = RFID_18K6C_WRITE_TYPE.RFID_18K6C_WRITE_TYPE_RANDOM;
            // debug only: 
            bnk = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_TID;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.bank = bnk;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.count = 8;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset = new ushort[RfidSp.RFID_18K6C_MAX_TAG_WRITE_WORDS];
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[0] = 0xfeed;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[1] = 0x1111;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[2] = 0x3333;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[3] = 0x5555;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[4] = 0x7777;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[5] = 0x9999;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[6] = 0xaaaa;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pOffset[7] = 0xcccc;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData = new ushort[RfidSp.RFID_18K6C_MAX_TAG_WRITE_WORDS];
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[0] = 0xbeef;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[1] = 0x3062;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[2] = 0x4770;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[3] = 0x5342;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[4] = 0x0213;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[5] = 0x9431;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[6] = 0x3424;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.pData[7] = 0xcafe;
#endif

            hRes = CF.f_CFlow_RfidDev_18K6CTagWrite(ref Rfid.st_RfidSpReq_18K6CTagWrite);
            return SP.SUCCEEDED(hRes);
        }

        // 16-bit word (unsigned short)
        private void DataStrToWdBuf(String dataStr, UInt16[] wdBuf)
        {
            // Must make sure that the dataStr length in multiple of Word 
            // ==> 2byte-binary or 4-byte-ascii

            CopyHexStrToShortArr(dataStr, wdBuf);
        }

        //
        // Simple version: only support sequential (no offset, only data from the beginning
        // of bank memory
        // verify = true (retry = 3)
        //
        private bool WriteBank(bool UseSelOrPostMatchCrit, RFID_18K6C_MEMORY_BANK bnk, ushort wdOffset,
            String dataStr, out HRESULT_RFID hRes)
        {
            ClearMacError();

            Rfid.st_RfidSpReq_18K6CTagWrite.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CTagWrite.flags = 0;
            if (UseSelOrPostMatchCrit)
            {
#if TAGWR_USES_SELECT
                Rfid.st_RfidSpReq_18K6CTagWrite.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
#else
                Rfid.st_RfidSpReq_18K6CTagWrite.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_POST_MATCH;
#endif
            }
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.common.tagStopCount = TagWriteStopCnt;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.accessPassword = TagWritePasswd;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.verify = 1; // read-after-write
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.verifyRetryCount = 3; // 0 - 7
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.writeType = RFID_18K6C_WRITE_TYPE.RFID_18K6C_WRITE_TYPE_SEQUENTIAL;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.bank = bnk;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.count =
                (ushort)((dataStr.Length / 4) + ((dataStr.Length % 4 > 0) ? 1 : 0));
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.offset = wdOffset;
            Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData = new ushort[RfidSp.RFID_18K6C_MAX_TAG_WRITE_WORDS];
            DataStrToWdBuf(dataStr, Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.pData);
            hRes = CF.f_CFlow_RfidDev_18K6CTagWrite(ref Rfid.st_RfidSpReq_18K6CTagWrite);
            return SP.SUCCEEDED(hRes);
        }

        private void WrOpMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            bool RaiseEvt = false;
            WrOpStatus status = WrOpStatus.error;
            String ErrMsg = null;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        status = WrOpStatus.errorStopped;
                        ErrMsg = "SetSelectCrit: " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagWrStopReq))
                    {
                        RaiseEvt = true;
                        status = WrOpStatus.stopped;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = WrOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetPostMatchCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        status = WrOpStatus.errorStopped;
                        ErrMsg = "SetPostMatchCrit: " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagWrStopReq))
                    {
                        RaiseEvt = true;
                        status = WrOpStatus.stopped;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = WrOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = WrOpStatus.errorStopped;
                        ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagWrStopReq))
                    {
                        RaiseEvt = true;
                        status = WrOpStatus.stopped;
                    }
                    else
                    {
                        status = WrOpStatus.configured;
                        RaiseEvt = true;
                        // Perform GetAround to issue SELECT command if necessary
                        bool PerfSelect = (tagOperSelCrit != null) && (tagOperSelCrit.Length > 0);
                        bool PerfPostMatch = (tagOperPostMatchCrit != null) && (tagOperPostMatchCrit.Length > 0);
                        if (PerfSelect)
                        {
                            SpMsgWnd.MsgNotify += WrOpMsgArrv;
                            hRes = TagAccMissingSelectGetAround();
                            if (!SP.SUCCEEDED(hRes))
                            {
                                errCode = hRes;
                                status = WrOpStatus.errorStopped;
                                ErrMsg = "TagAccMissingSelectGetAround: " + hRes.ToString("F");
                                RaiseEvt = true;
                                SpMsgWnd.MsgNotify -= WrOpMsgArrv;
                            }
                        }
                        else // Post-Match or no Select/Post-Match at all
                        {
                            // Start Write now
                            String WrDataStr;
                            MemoryBanks4Op BnkOp;
                            ushort WrWdOffset;
                            if (!NxtBnkDataToWr(out BnkOp, out WrWdOffset, out WrDataStr))
                            {
                                // Assuming that there are banks to write before issuing
                                // PostMatch criteria change
                                throw new ApplicationException("Programming Error");
                            }
                            else
                            {
                                RFID_18K6C_MEMORY_BANK Bnk = EPCTag.MemoryBanks4OpTo18K6CBank(BnkOp);
                                SpMsgWnd.MsgNotify += WrOpMsgArrv;
                                if (!WriteBank(PerfPostMatch, Bnk, WrWdOffset, WrDataStr, out hRes))
                                {
                                    errCode = hRes;
                                    status = WrOpStatus.errorStopped;
                                    ErrMsg = "CF.f_CFlow_RfidDev_TagWrite() returns: "
                                    + hRes.ToString("F");
                                    RaiseEvt = true;
                                    SpMsgWnd.MsgNotify -= WrOpMsgArrv;
                                }
                                else
                                {
                                    status = WrOpStatus.started;
                                    RaiseEvt = true;
                                }
                            }
                        }
                    }
                    break;
            }
            if (status == WrOpStatus.configured || status == WrOpStatus.started
                || status == WrOpStatus.stopped || status == WrOpStatus.errorStopped)
                SpMsgWnd.MsgNotifyHRes -= WrOpMsgArrv;
            if (RaiseEvt == true && WrOpStEvent != null)
                WrOpStEvent(this, new WrOpEventArgs(status, ErrMsg));
            if (RaiseEvt == true && (status == WrOpStatus.errorStopped || status == WrOpStatus.stopped))
                CheckAndClear(ref TagWrStopReq);
        }

        private void WrOpMsgArrv(RFID_MSGID msgID)
        {
            String ErrMsg = null;
            AccErrorTypes TAErr = AccErrorTypes.None;
            WrOpStatus OpStatus = WrOpStatus.error;
            bool RaiseEvt = false;
            UINT96_T EPCWritten = new UINT96_T();
            UInt16 PCWritten = 0;
            String TgtDataStr = null;
            MemoryBanks4Op TgtBnk = MemoryBanks4Op.None;
            ushort TgtWdOffset;
            HRESULT_RFID hRes;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory:
                    // This comes from TagAccMissingSelectGetAround()
                    String WrDataStr;
                    MemoryBanks4Op BnkOp;
                    ushort WrWdOffset;
                    if (!NxtBnkDataToWr(out BnkOp, out WrWdOffset, out WrDataStr))
                    {
                        // Assuming that there are banks to write before issuing
                        // PostMatch criteria change
                        throw new ApplicationException("Programming Error");
                    }
                    else
                    {
                        RFID_18K6C_MEMORY_BANK Bnk = EPCTag.MemoryBanks4OpTo18K6CBank(BnkOp);
                        if (!WriteBank(true, Bnk, WrWdOffset, WrDataStr, out hRes))
                        {
                            errCode = hRes;
                            OpStatus = WrOpStatus.errorStopped;
                            ErrMsg = "CF.f_CFlow_RfidDev_TagWrite() returns: "
                            + hRes.ToString("F");
                            RaiseEvt = true;
                            SpMsgWnd.MsgNotify -= WrOpMsgArrv;
                        }
                        else
                        {
                            OpStatus = WrOpStatus.started;
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFIDMW_REQUEST_TYPE_MSGID_TagInv_AddATag:
                    // Do Nothing for now
                    RaiseEvt = false;
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_TAG_ACCESS:
                    // Check Against Intended EPC Tag
                    EPCWritten = Rfid.st_RfidMw_AddATag_PecRec.m_Epc;
                    PCWritten = Rfid.st_RfidMw_AddATag_PecRec.m_Pc;
                    NxtBnkDataToWr(out TgtBnk, out TgtWdOffset, out TgtDataStr);
#if TAGWR_USES_SELECT
                    if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                    {
                        // Assuming that 'SELECT' was issued and completed successfully
                        if (!TagIDSelCritMet(PCWritten, ref EPCWritten, tagOperSelCrit[0].mask.mask,
                            (ushort)(tagOperSelCrit[0].mask.offset/16)))
                        {
                            ErrMsg = "ID of written Tag does not match SELECT mask\r\n:"
                                            + "Mask@offset( " + tagOperSelCrit[0].mask.offset
                                            + "): " + ByteArrToHexStr(tagOperSelCrit[0].mask.mask) + "\r\n"
                                            + "Accessed PC: " + PCWritten.ToString("X4") + "\r\n"
                                            + "Accessed EPC: " + EPCWritten.ToString();
                            OpStatus = WrOpStatus.error;
                            RaiseEvt = true;
                        }
                    }
#else
                    if (tagOperPostMatchCrit != null && tagOperPostMatchCrit.Length > 0)
                    {
                        // Assuming that 'Post Match Criteria' is setup without error
                        if (!TagIDPostMatchCritMet((tagOperPostMatchCrit[0].match != 0),
                            ref EPCWritten, tagOperPostMatchCrit[0].mask.mask,
                            (ushort)(tagOperPostMatchCrit[0].mask.offset / 16)))
                        {
                            ErrMsg = "ID of written Tag does not match PostMatch mask\r\n:"
                                           + "Mask@offset( " + tagOperPostMatchCrit[0].mask.offset
                                           + "): " + ByteArrToHexStr(tagOperPostMatchCrit[0].mask.mask) + "\r\n"
                                           + "Accessed PC: " + PCWritten.ToString("X4") + "\r\n"
                                           + "Accessed EPC: " + EPCWritten.ToString();
                            OpStatus = WrOpStatus.error;
                            RaiseEvt = true;
                        }
                    }
#endif
                    if (!(OpStatus == WrOpStatus.error && RaiseEvt == true)) // Report Success(MemBnkWrEvent) or Error(WrOpStEvent)
                    {
                        try
                        {
                            TAErr = EPCTag.TagAccPktGetErrType(ref Rfid.st_RfidSpPkt_TagAccessData);
                            if (TAErr != AccErrorTypes.None) // temp implementation
                            {
                                // Drop the packet as if nothing happened
                                ErrMsg = EPCWritten.ToString();
                                OpStatus = WrOpStatus.tagAccError;
                                RaiseEvt = true;
                            }
                            else
                            {
                                RFID_18K6C_WRITE_TYPE WriteType =
                                        Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.writeType;
                                RFID_18K6C_MEMORY_BANK MemBnk =
                                        WriteType == RFID_18K6C_WRITE_TYPE.RFID_18K6C_WRITE_TYPE_SEQUENTIAL ?
                                                        Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.bank
                                                        : Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.random.bank;
                                if (WriteType == RFID_18K6C_WRITE_TYPE.RFID_18K6C_WRITE_TYPE_SEQUENTIAL)
                                    MemBnkWrEvent(this, new MemBnkWrEventArgs(ref EPCWritten, EPCTag.T18K6CBankToMemoryBanks4Op(MemBnk), Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.offset,
                                        Rfid.st_RfidSpReq_18K6CTagWrite.writeParms.sequential.count));
                                else
                                    MemBnkWrEvent(this, new MemBnkWrEventArgs(
                                        ref EPCWritten, EPCTag.T18K6CBankToMemoryBanks4Op(MemBnk)));
                            }
                        }
                        catch (Exception e)
                        {
                            // Exception could have come from deeper in the stack
                            ErrMsg = e.Message + "\r\n"
                                            + "Accessed PC: " + PCWritten.ToString("X4") + "\r\n"
                                            + "Accessed EPC: " + EPCWritten.ToString();
                            OpStatus = WrOpStatus.error;
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagWrite:
                    bool UserCancel = false;
                    hRes = Rfid.st_RfidSpReq_18K6CTagWrite.status;
                    if (hRes == HRESULT_RFID.E_RFID_ERROR_OPERATION_CANCELLED)
                    {
                        UserCancel = true;
                        hRes = HRESULT_RFID.S_OK; // override to avoid false positive
                    }
                    if (SP.SUCCEEDED(hRes))
                    {
                        bool HasFatalErr = false;
                        if (GetMacError(out macErrCode) && macErrCode != 0)
                        {
                            LogMacError();
                            if (RFIDRdr.MacErrorIsFatal(macErrCode))
                            {
                                OpStatus = WrOpStatus.errorStopped;
                                ErrMsg = "Hardware Failure! (mac) Error code: 0x: " + macErrCode.ToString("X4");
                                RaiseEvt = true;
                                HasFatalErr = true;
                            }
                        }
                        if (!HasFatalErr)
                        {
                            //
                            // Emit Write complete event now (this could affect
                            // the result for NxtBnkDataToWr() call
                            //
                            OpStatus = WrOpStatus.completed;
                            WrOpStEvent(this, new WrOpEventArgs(OpStatus, String.Empty));

                            // Set up Next Bank to Write, if necessary
                            if ((!UserCancel) && NxtBnkDataToWr(out TgtBnk, out TgtWdOffset, out TgtDataStr))
                            {
                                // Add this Handler to Event Again to continue handling Tag Write Op
                                SpMsgWnd.MsgNotify += WrOpMsgArrv;
                                // Events
                                RFID_18K6C_MEMORY_BANK Bnk = EPCTag.MemoryBanks4OpTo18K6CBank(TgtBnk);
                                bool PerfSelCmd = (tagOperSelCrit != null) && (tagOperSelCrit.Length > 0);
                                bool PerfPostMatch = (tagOperPostMatchCrit != null) && (tagOperPostMatchCrit.Length > 0);
                                if (!WriteBank((PerfSelCmd || PerfPostMatch), Bnk, TgtWdOffset, TgtDataStr, out hRes)) //Might need to run SELECT(by way of inventory again)
                                {
                                    errCode = hRes;
                                    // fatal error
                                    SpMsgWnd.MsgNotify -= WrOpMsgArrv; // rollback
                                    ErrMsg = "CF.f_CFlow_RfidDev_TagWrite() returns: "
                                                           + hRes.ToString("F");
                                    OpStatus = WrOpStatus.errorStopped;
                                    RaiseEvt = true;
                                }
                            }
                            else // No more banks to write, Stop the device
                            {
                                OpStatus = WrOpStatus.stopped;
                                RaiseEvt = true;
                            }
                        }
                    }
                    else // TagWrite status is NG
                    {
                        // Assuming that error is already detected and handled in 
                        // RFID_PACKET_TYPE_MSGID_18K6C_TAG_ACCESS case
                        // There is no point repeating here.                
                        errCode = hRes;
                        ErrMsg = "Tag Write Error: " + hRes;
                        OpStatus = WrOpStatus.errorStopped;
                        RaiseEvt = true;
                    }
                    // Match the Addition with each WriteBank()
                    SpMsgWnd.MsgNotify -= WrOpMsgArrv;
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                    hRes = HRESULT_RFID.E_FAIL;
                    switch (msgID)
                    {
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                            hRes = Rfid.st_RfidSpReq_RadioCancelOperation.status; break;
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                            hRes = Rfid.st_RfidSpReq_RadioAbortOperation.status; break;
                    }
                    if (!SP.SUCCEEDED(hRes))
                    {
                        ErrMsg = "Tag Write Error: " + hRes.ToString();
                        OpStatus = WrOpStatus.errorStopped;
                    }
                    else
                        OpStatus = WrOpStatus.stopped;
                    RaiseEvt = true;
                    // In case RFID_REQEND_TYPE_MSGID_18K6CTagWrite was not received
                    SpMsgWnd.MsgNotify -= WrOpMsgArrv;
                    break;
                default:
                    break;
            }
            if (RaiseEvt && (WrOpStEvent != null))
            {
                if (TAErr != AccErrorTypes.None)
                    WrOpStEvent(this, new WrOpEventArgs(OpStatus, ErrMsg, TAErr, PCWritten, EPCWritten));
                else
                    WrOpStEvent(this, new WrOpEventArgs(OpStatus, ErrMsg));
            }
            if (RaiseEvt && (OpStatus == WrOpStatus.errorStopped || OpStatus == WrOpStatus.stopped))
            {
                SpMsgWnd.MsgNotify -= WrOpMsgArrv;
                CheckAndClear(ref TagWrStopReq);
            }
        }
        #endregion

        #region Tag Lock Routines
        private RFID_18K6C_TAG_PERM TagPermToSet = new RFID_18K6C_TAG_PERM();
        private UInt32 TagLockPasswd = 0;
        // Set Permission to One Specific Tag
        public bool TagSetPermStart(
            ref RFID_18K6C_TAG_PERM perm, UInt16 PC, ref UINT96_T EPC, UInt32 accPasswd)
        {
            TagPermToSet = perm;
            TagLockPasswd = accPasswd;
            // Try to use SL flag instead of session: drawback, only one reader
            // could be used at a time
#if TAGLCK_USES_SELECT
            Byte[] bnkMask = MakeEPCBnkMask(PC, ref EPC);
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ,
                1, RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ON, bnkMask, EPCTag.CRC16FldSz,
                RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);
#else
            TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ,
                1, EPC.ToByteArray(), 0, false); // Offset is 0 because Singulation Mask starts at EPC portion of Bank1
#endif
            return TagLockStart();
        }

        public bool TagSetPermStop()
        {
            TagLockStopReq = true;
            return CancelCurrentOp();
        }

        private bool TagLockStart()
        {
            bool rv = false;
            // Open device first if not already done
            switch (radioStatus)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
                    rv = this.RadioOpen(TagLckDevStartNotify);
                    break;
                case RadioStatus.ClosePend:
                case RadioStatus.Closing:
                case RadioStatus.Opening:
                case RadioStatus.Configuring:
                    rv = false;
                    break;
                case RadioStatus.Opened:
                case RadioStatus.Configured:
                    TagLckDevStartNotify(RadioOpRes.StateChanged, radioStatus, null);
                    rv = true;
                    break;
            }
            return rv;
        }

        private void TagLckDevStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            LckOpStatus OpStatus = LckOpStatus.error;
            String ErrMsg = msg;
            bool RaiseEvt = false;
            bool StartLckOp = false;
            // process notification from RadioOpen
            switch (res)
            {
                case RadioOpRes.None:
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opening:
                        case RadioStatus.Configuring:
                            // keep going
                            break;
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            // wait until SELECT/QUERY options set
                            // before actually setting LckOpStatus to 'configured'
                            StartLckOp = true;
                            break;
                        case RadioStatus.Closed:
                        case RadioStatus.ErrorStopped:
                            OpStatus = LckOpStatus.stopped;
                            RaiseEvt = true;
                            break;
                        case RadioStatus.PowerSaved:
                        case RadioStatus.Closing:
                        case RadioStatus.ClosePend:
                            OpStatus = LckOpStatus.error;
                            RaiseEvt = true;
                            break;
                    }
                    break;
                case RadioOpRes.Warning:
                    // do nothing
                    RaiseEvt = false;
                    break;
                case RadioOpRes.Error:
                    if (radioStatus == RadioStatus.ErrorStopped)
                        OpStatus = LckOpStatus.errorStopped;
                    else
                        OpStatus = LckOpStatus.error;
                    RaiseEvt = true;
                    break;
            }//switch
            if (CheckAndClear(ref TagLockStopReq))
            {
                RaiseEvt = true;
                OpStatus = LckOpStatus.stopped;
            }
            else if (StartLckOp)
            {
                // Set up SELECT/QUERY options for LckOp 
                HRESULT_RFID hRes;
                SpMsgWnd.MsgNotifyHRes += LckOpMsgArrv;
                SetNonContOperMode(out hRes); // Non-Continuous Operation mode
#if TAGLCK_USES_SELECT
                if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                {
                    hRes = TagSelectCritSet(tagOperSelCrit);
                }
#else
                if (tagOperPostMatchCrit != null && tagOperPostMatchCrit.Length > 0)
                {
                    hRes = TagPostMatchCritSet(tagOperPostMatchCrit);
                }
#endif
                else
                {
                    // Only expects SELECT criteria set
                    OpStatus = LckOpStatus.errorStopped;
                    SpMsgWnd.MsgNotifyHRes -= LckOpMsgArrv;
                    ErrMsg = "Locking tag All/Group not currently supported yet";
                    RaiseEvt = true;
                }
                // Check Select/PostMatch Result
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = LckOpStatus.errorStopped;
                    SpMsgWnd.MsgNotifyHRes -= LckOpMsgArrv;
#if TAGLCK_USES_SELECT
                    throw new ApplicationException("CF.f_CFlow_RfidDev_SetSelectCriteria() returns: "
                                           + hRes.ToString("F"));
#else
                    throw new ApplicationException("CF.f_CFlow_RfidDev_SetPostMatchCriteria() returns: "
                                   + hRes.ToString("F"));
#endif
                }
            }
            // raise LckOpStEvent if appropiate
            if (RaiseEvt && (LckOpStEvent != null))
                LckOpStEvent(this, new LckOpEventArgs(OpStatus, ErrMsg));
            if ((RaiseEvt == true) && (OpStatus == LckOpStatus.errorStopped || OpStatus == LckOpStatus.stopped))
                CheckAndClear(ref TagLockStopReq);
        }

        private void LckOpMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            bool RaiseEvt = false;
            LckOpStatus status = LckOpStatus.error;
            String ErrMsg = null;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetPostMatchCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = LckOpStatus.errorStopped;
                        RaiseEvt = true;
                        if (msgID == RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetPostMatchCriteria)
                            ErrMsg = "SetPostMatchCrit: " + hRes.ToString("F");
                        else
                            ErrMsg = "SetSelectCrit: " + hRes.ToString("F");
                    }
                    else if (CheckAndClear(ref TagLockStopReq))
                    {
                        status = LckOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = LckOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = LckOpStatus.errorStopped;
                        ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagLockStopReq))
                    {
                        status = LckOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    else
                    {
                        status = LckOpStatus.configured;
                        RaiseEvt = true;

                        // Perform GetAround to issue SELECT command if necessary
                        bool PerfSelect = (tagOperSelCrit != null) && (tagOperSelCrit.Length > 0);
                        bool PerfPostMatch = (tagOperPostMatchCrit != null && tagOperPostMatchCrit.Length > 0);
                        if (PerfSelect)
                        {
                            SpMsgWnd.MsgNotify += LckOpMsgArrv;
                            hRes = TagAccMissingSelectGetAround();
                            if (!SP.SUCCEEDED(hRes))
                            {
                                errCode = hRes;
                                status = LckOpStatus.errorStopped;
                                ErrMsg = "TagAccMissingSelectGetAround: " + hRes.ToString("F");
                                RaiseEvt = true;
                                SpMsgWnd.MsgNotify -= LckOpMsgArrv;
                            }
                        }
                        else if (PerfPostMatch)
                        {
                            SpMsgWnd.MsgNotify += LckOpMsgArrv;
                            if (!TagLock(ref TagPermToSet, true, out hRes))
                            {
                                errCode = hRes;
                                status = LckOpStatus.errorStopped;
                                ErrMsg = "CF.f_CFlow_RfidDev_TagLock() returns: "
                                   + hRes.ToString("F");
                                RaiseEvt = true;
                                SpMsgWnd.MsgNotify -= LckOpMsgArrv;
                            }
                            else
                            {
                                status = LckOpStatus.started;
                                RaiseEvt = true;
                            }
                        }
                        else // neither Select nor Post-Match was set to use
                        {
                            status = LckOpStatus.errorStopped;
                            ErrMsg = "Tag Lock to All/Group Tags are not yet supported";
                            RaiseEvt = true;
                        }
                    }
                    break;
            }
            if (RaiseEvt == true && LckOpStEvent != null)
                LckOpStEvent(this, new LckOpEventArgs(status, ErrMsg));
            if (status == LckOpStatus.errorStopped || status == LckOpStatus.configured
                || status == LckOpStatus.stopped || status == LckOpStatus.started)
                SpMsgWnd.MsgNotifyHRes -= LckOpMsgArrv;
            if ((RaiseEvt == true) && (status == LckOpStatus.errorStopped || status == LckOpStatus.stopped))
                CheckAndClear(ref TagLockStopReq);
        }

        private bool TagLock(ref RFID_18K6C_TAG_PERM perm, bool UseSelOrPostMatchCrit, out HRESULT_RFID hRes)
        {
            ClearMacError();

            // Issue Tag Lock
            Rfid.st_RfidSpReq_18K6CTagLock.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CTagLock.flags = 0;
            // Assuming SELECT Criteria is set if we go this far
            if (UseSelOrPostMatchCrit)
            {
#if TAGLCK_USES_SELECT
                Rfid.st_RfidSpReq_18K6CTagLock.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
#else
                Rfid.st_RfidSpReq_18K6CTagLock.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_POST_MATCH;
#endif
            }
            Rfid.st_RfidSpReq_18K6CTagLock.lockParms.accessPassword = TagLockPasswd;
            // Setting count = 0 : cause problem (such as no INVENTORY 
            // packet before TAG_ACCESS packet, Error bits always on)
            // This is in contradiction to Intel Doc.
            Rfid.st_RfidSpReq_18K6CTagLock.lockParms.common.tagStopCount = 0;
            Rfid.st_RfidSpReq_18K6CTagLock.lockParms.permissions = perm;

            hRes = CF.f_CFlow_RfidDev_18K6CTagLock(ref Rfid.st_RfidSpReq_18K6CTagLock);
            return (SP.SUCCEEDED(hRes));
        }

        private void LckOpMsgArrv(RFID_MSGID msgID)
        {
            String ErrMsg = null;
            AccErrorTypes TAErr = AccErrorTypes.None;
            LckOpStatus Status = LckOpStatus.error;
            bool RaiseEvt = false;
            HRESULT_RFID hRes;
            UInt16 PCLocked;
            UINT96_T EPCLocked;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory:
                    hRes = Rfid.st_RfidSpReq_18K6CTagInventory.status;
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        Status = LckOpStatus.errorStopped;
                        ErrMsg = "Tag Inventory (AccSelectGetAround) failed: " + hRes.ToString();
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagLockStopReq))
                    {
                        RaiseEvt = true;
                        Status = LckOpStatus.stopped;
                    }
                    else
                    {
                        if (!TagLock(ref TagPermToSet, true, out hRes))
                        {
                            errCode = hRes;
                            Status = LckOpStatus.errorStopped;
                            ErrMsg = "CF.f_CFlow_RfidDev_TagLock() returns: "
                               + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                        else
                        {
                            Status = LckOpStatus.started;
                            RaiseEvt = true;
                        }
                    }
                    if (RaiseEvt == true && Status == LckOpStatus.errorStopped)
                        SpMsgWnd.MsgNotify -= LckOpMsgArrv;
                    break;
                case RFID_MSGID.RFIDMW_REQUEST_TYPE_MSGID_TagInv_AddATag:
                    // Singulated Tag, Do nothing at this point.
                    RaiseEvt = false;
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_TAG_ACCESS:
                    EPCLocked = Rfid.st_RfidMw_AddATag_PecRec.m_Epc;
                    PCLocked = Rfid.st_RfidMw_AddATag_PecRec.m_Pc;
#if TAGLCK_USES_SELECT
                    if (!TagIDSelCritMet(PCLocked, ref EPCLocked, tagOperSelCrit[0].mask.mask,
                        (ushort)(tagOperSelCrit[0].mask.offset / 16)))
                    {
                        ErrMsg = "ID of locked Tag does not match SELECT mask\r\n:"
                                        + "Mask@offset( " + tagOperSelCrit[0].mask.offset
                                        + "): " + ByteArrToHexStr(tagOperSelCrit[0].mask.mask) + "\r\n"
                                        + "Accessed PC: " + PCLocked.ToString("X4") + "\r\n"
                                        + "Accessed EPC: " + EPCLocked.ToString();
                        Status = LckOpStatus.error; ;
                        RaiseEvt = true;
                    }
#else
                    if (!TagIDPostMatchCritMet((tagOperPostMatchCrit[0].match != 0),
                        ref EPCLocked, tagOperPostMatchCrit[0].mask.mask,
                        (ushort)(tagOperPostMatchCrit[0].mask.offset / 16)))
                    {
                        ErrMsg = "ID of locked Tag does not match PostMatch mask\r\n:"
                                      + "Mask@offset( " + tagOperPostMatchCrit[0].mask.offset
                                      + "): " + ByteArrToHexStr(tagOperPostMatchCrit[0].mask.mask) + "\r\n"
                                      + "Accessed PC: " + PCLocked.ToString("X4") + "\r\n"
                                      + "Accessed EPC: " + EPCLocked.ToString();
                        Status = LckOpStatus.error; ;
                        RaiseEvt = true;
                    }
#endif
                    else // Report Tag Access Error if any
                    {
                        try
                        {
                            TAErr = EPCTag.TagAccPktGetErrType(ref Rfid.st_RfidSpPkt_TagAccessData);
                            if (TAErr != AccErrorTypes.None) // temp implementation
                            {
                                // Drop the packet as if nothing happened
                                ErrMsg = PCLocked.ToString("X4") + "-" + EPCLocked.ToString();
                                Status = LckOpStatus.tagAccError;
                                RaiseEvt = true;
                            }
                            else // No Error, Great!
                            {
                                if (TagPermSetEvent != null)
                                    TagPermSetEvent(this, new DscvrTagEventArgs(PCLocked, ref EPCLocked));
                            }
                        }
                        catch (Exception e)
                        {
                            // Exception could have come from deeper in the stack
                            ErrMsg = e.Message + "\r\n"
                                            + "Accessed PC: " + PCLocked.ToString("X4") + "\r\n"
                                            + "Accessed EPC: " + EPCLocked.ToString();
                            Status = LckOpStatus.error;
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagLock:
                    hRes = Rfid.st_RfidSpReq_18K6CTagLock.status;
                    if (hRes == HRESULT_RFID.E_RFID_ERROR_OPERATION_CANCELLED)
                        hRes = HRESULT_RFID.S_OK; // override to avoid false positive
                    if (SP.SUCCEEDED(hRes))
                    {
                        bool HasFatalErr = false;
                        if (GetMacError(out macErrCode) && macErrCode != 0)
                        {
                            LogMacError();
                            if (RFIDRdr.MacErrorIsFatal(macErrCode))
                            {
                                Status = LckOpStatus.errorStopped;
                                ErrMsg = "Hardware Failure! (mac) Error code: 0x: " + macErrCode.ToString("X4");
                                RaiseEvt = true;
                                HasFatalErr = true;
                            }
                        }
                        if (!HasFatalErr)
                        {
                            Status = LckOpStatus.stopped;
                            RaiseEvt = true;
                        }
                    }
                    else
                    {
                        ErrMsg = "Tag Lock Error: " + hRes.ToString("F");
                        Status = LckOpStatus.errorStopped;
                        RaiseEvt = true;
                    }
                    SpMsgWnd.MsgNotify -= LckOpMsgArrv;
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                    hRes = HRESULT_RFID.E_FAIL;
                    switch (msgID)
                    {
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                            hRes = Rfid.st_RfidSpReq_RadioCancelOperation.status; break;
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                            hRes = Rfid.st_RfidSpReq_RadioAbortOperation.status; break;
                    }
                    if (!SP.SUCCEEDED(hRes))
                    {
                        ErrMsg = "Tag Lock Error: " + hRes.ToString("F");
                        Status = LckOpStatus.errorStopped;
                    }
                    else
                        Status = LckOpStatus.stopped;
                    RaiseEvt = true;
                    SpMsgWnd.MsgNotify -= LckOpMsgArrv;
                    break;
                default:
                    break;
            }
            if (RaiseEvt == true && LckOpStEvent != null)
                LckOpStEvent(this, new LckOpEventArgs(Status, ErrMsg, TAErr));
            if ((RaiseEvt == true) && (Status == LckOpStatus.errorStopped || Status == LckOpStatus.stopped))
                CheckAndClear(ref TagLockStopReq);
        }
        #endregion

        #region Tag Search Routines
        private void TagInvtryRdRateStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            bool StartInvtryOp = false;

            switch (res)
            {
                case RadioOpRes.None:
                case RadioOpRes.Warning:
                case RadioOpRes.Error:
                    // somewhere else will take care of this
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            StartInvtryOp = true;
                            break;
                    }
                    break;
            }

            InvtryOpStatus OpStatus = InvtryOpStatus.none;
            bool RaiseEvt = false;
            if (CheckAndClear(ref TagSrchRdRateStopReq))
            {
                RaiseEvt = true;
                OpStatus = InvtryOpStatus.stopped;
            }
            else if (StartInvtryOp)
            {
                // Setup SELECT and Singulation Options
                HRESULT_RFID hRes;
                SpMsgWnd.MsgNotifyHRes += TagInvtryRdRateMsgArrv;
                SetContOperMode(out hRes);
                if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                {
                    hRes = TagSelectCritSet(tagOperSelCrit);
                }
                else // There is no SELECT criteria to set, go straight to QueryParameter setup
                {
                    hRes = QueryParmsSet(ref tagOperQryParm);
                }
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = InvtryOpStatus.errorStopped;
                    SpMsgWnd.MsgNotifyHRes -= TagInvtryRdRateMsgArrv;
                    throw new ApplicationException("CF.f_CFlow_RfidDev_SetSelectCriteria() returns: "
                        + hRes.ToString("F"));
                }
#if false
                HRESULT_RFID hRes;
                SetContOperMode(out hRes);
                Rfid.st_RfidSpReq_18K6CTagInventory.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                Rfid.st_RfidSpReq_18K6CTagInventory.flags = 0;
                if (invtryPerfSelCmd)
                    Rfid.st_RfidSpReq_18K6CTagInventory.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
                SpMsgWnd.MsgNotifyHRes += TagInvtryRdRateMsgArrv;
                SpMsgWnd.MsgNotifyTagRate += TagRateMsgArrv;
                hRes = CF.f_CFlow_RfidDev_CustomGetTagInvtryRdRate(ref Rfid.st_RfidSpReq_18K6CTagInventory);
                if (SP.SUCCEEDED(hRes))
                {
                    OpStatus = InvtryOpStatus.started;
                    RaiseEvt = true;
                }
                else
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = InvtryOpStatus.error;
                    SpMsgWnd.MsgNotifyHRes -= TagInvtryRdRateMsgArrv;
                    SpMsgWnd.MsgNotifyTagRate -= TagRateMsgArrv;
                    throw new ApplicationException("CF.f_CFlow_RfidDev_CustoMGetTagInvtryRdRate() returns: "
                        + hRes.ToString("F"));
                }
#endif
            }

            // raise InvtryOpStEvent if appropiate
            if ((RaiseEvt == true) && (InvtryOpStEvent != null))
                InvtryOpStEvent(this, new InvtryOpEventArgs(OpStatus));
            if ((RaiseEvt == true) && (OpStatus == InvtryOpStatus.errorStopped || OpStatus == InvtryOpStatus.stopped))
                CheckAndClear(ref TagSrchRdRateStopReq);
        }

        private void TagInvtryRdRateMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            InvtryOpStatus status = InvtryOpStatus.none;
            bool RaiseEvt = false;
            String ErrMsg = HRESULT_RFID.S_OK.ToString("F");

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = InvtryOpStatus.errorStopped;
                        ErrMsg = "SetSelectCriteria : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagSrchRdRateStopReq))
                    {
                        status = InvtryOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = InvtryOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = InvtryOpStatus.errorStopped;
                        ErrMsg = "SetQueryParameters : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagSrchRdRateStopReq))
                    {
                        status = InvtryOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    else
                    {
                        status = InvtryOpStatus.configured;
                        RaiseEvt = true;
                        Rfid.st_RfidSpReq_18K6CTagInventory.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                        Rfid.st_RfidSpReq_18K6CTagInventory.flags = 0;
                        if (invtryPerfSelCmd)
                            Rfid.st_RfidSpReq_18K6CTagInventory.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
                        Rfid.st_RfidSpReq_18K6CTagInventory.invenParms.common.tagStopCount = tagInvtryStopCnt;
                        SpMsgWnd.MsgNotifyTagRate += TagRateMsgArrv;
                        hRes = CF.f_CFlow_RfidDev_CustomGetTagInvtryRdRate(ref Rfid.st_RfidSpReq_18K6CTagInventory);
                        if (SP.SUCCEEDED(hRes))
                        {
                            status = InvtryOpStatus.started;
                            RaiseEvt = true;
                        }
                        else
                        {
                            errCode = hRes;
                            RaiseEvt = true;
                            status = InvtryOpStatus.errorStopped;
                            SpMsgWnd.MsgNotifyTagRate -= TagRateMsgArrv;
                            throw new ApplicationException("CF.f_CFlow_RfidDev_CustoMGetTagInvtryRdRate() returns: "
                                + hRes.ToString("F"));
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomTagInvtryRdRate:
                    if (hRes == HRESULT_RFID.E_RFID_ERROR_OPERATION_CANCELLED)
                        hRes = HRESULT_RFID.S_OK; // override to avoid false positive
                    // TBD: Power down the device
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        status = InvtryOpStatus.errorStopped;
                        ErrMsg = hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (GetMacError(out macErrCode) && macErrCode != 0)
                    {
                        LogMacError();
                        status = InvtryOpStatus.macErrorStopped;
                        ErrMsg = "Hardware Failure! (mac) Error code: 0x" + macErrCode.ToString("X4");
                        RaiseEvt = true;
                    }
                    else
                    {
                        status = InvtryOpStatus.stopped;
                        RaiseEvt = true;
                    }
                    SpMsgWnd.MsgNotifyTagRate -= TagRateMsgArrv;
                    break;
                default: // ignore the rest
                    break;
            }

            if (RaiseEvt == true && InvtryOpStEvent != null)
            {
                InvtryOpStEvent(this, new InvtryOpEventArgs(status, ErrMsg));
            }
            if (status == InvtryOpStatus.errorStopped || status == InvtryOpStatus.stopped
                || status == InvtryOpStatus.macErrorStopped)
            {
                SpMsgWnd.MsgNotifyHRes -= TagInvtryRdRateMsgArrv;
                CheckAndClear(ref TagSrchRdRateStopReq);
            }
        }

        private void TagRateMsgArrv(RFID_MSGID msgID, UInt32 period, UInt32 cnt)
        {
            switch (msgID)
            {
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_TAGRATE:
                    if (TagRateEvent != null)
                    {
                        TagRateEvent(this, new TagRateEventArgs(period, cnt));
                    }
                    break;
            }
        }

        public bool TagInvtryRdRateStart(RFID_18K6C_INVENTORY_SESSION session,
            bool search1Only, Byte[] epcBnkMask, UInt32 maskOffset)
        {
            TagOperSetup(
                search1Only ? RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ
                : RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ,
                (uint)(search1Only ? 1 : 7), session,
                RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_B,
                false, epcBnkMask, maskOffset, RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA);
            tagInvtryStopCnt = TagStopCountUnlimited;
            return TagInvtryStart(true, TagInvtryRdRateStartNotify);
        }

        public bool TagInvtryRdRateStop()
        {
            TagSrchRdRateStopReq = true;
            return CancelCurrentOp();
        }

        private void TagInvtryRssiStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            bool StartInvtryOp = false;

            switch (res)
            {
                case RadioOpRes.None:
                case RadioOpRes.Warning:
                case RadioOpRes.Error:
                    // somewhere else will take care of this
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            StartInvtryOp = true;
                            break;
                    }
                    break;
            }

            InvtryOpStatus OpStatus = InvtryOpStatus.none;
            bool RaiseEvt = false;
            if (CheckAndClear(ref TagSrchRssiStopReq))
            {
                RaiseEvt = true;
                OpStatus = InvtryOpStatus.stopped;
            }
            else if (StartInvtryOp)
            {
                // Setup SELECT and Singulation Options
                HRESULT_RFID hRes;
                SpMsgWnd.MsgNotifyHRes += TagInvtryRssiMsgArrv;
                SetContOperMode(out hRes);
                if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                {
                    hRes = TagSelectCritSet(tagOperSelCrit);
                }
                else // There is no SELECT criteria to set, go straight to QueryParameter setup
                {
                    hRes = QueryParmsSet(ref tagOperQryParm);
                }
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = InvtryOpStatus.errorStopped;
                    SpMsgWnd.MsgNotifyHRes -= TagInvtryRssiMsgArrv;
                    throw new ApplicationException("CF.f_CFlow_RfidDev_SetSelectCriteria() returns: "
                        + hRes.ToString("F"));
                }
            }

            // raise InvtryOpStEvent if appropiate
            if ((RaiseEvt == true) && (InvtryOpStEvent != null))
                InvtryOpStEvent(this, new InvtryOpEventArgs(OpStatus));
            if ((RaiseEvt == true) && (OpStatus == InvtryOpStatus.errorStopped || OpStatus == InvtryOpStatus.stopped))
                CheckAndClear(ref TagSrchRssiStopReq);
        }

        private void TagInvtryRssiMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            InvtryOpStatus status = InvtryOpStatus.none;
            bool RaiseEvt = false;
            String ErrMsg = HRESULT_RFID.S_OK.ToString("F");

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = InvtryOpStatus.errorStopped;
                        ErrMsg = "SetSelectCriteria : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagSrchRssiStopReq))
                    {
                        RaiseEvt = true;
                        status = InvtryOpStatus.stopped;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            status = InvtryOpStatus.errorStopped;
                            ErrMsg = "SetQueryParameters: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        status = InvtryOpStatus.errorStopped;
                        ErrMsg = "SetQueryParameters : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref TagSrchRssiStopReq))
                    {
                        RaiseEvt = true;
                        status = InvtryOpStatus.stopped;
                    }
                    else
                    {
                        status = InvtryOpStatus.configured;
                        RaiseEvt = true;
                        Rfid.st_RfidSpReq_18K6CTagInventory.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                        Rfid.st_RfidSpReq_18K6CTagInventory.flags = 0;
                        if (invtryPerfSelCmd)
                            Rfid.st_RfidSpReq_18K6CTagInventory.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
                        Rfid.st_RfidSpReq_18K6CTagInventory.invenParms.common.tagStopCount = tagInvtryStopCnt;
                        SpMsgWnd.MsgNotifyFloat += TagRssiMsgArrv;
                        hRes = CF.f_CFlow_RfidDev_CustomGetTagInvtryRssi(ref Rfid.st_RfidSpReq_18K6CTagInventory, rssiFilterRes);
                        if (SP.SUCCEEDED(hRes))
                        {
                            status = InvtryOpStatus.started;
                            RaiseEvt = true;
                        }
                        else
                        {
                            errCode = hRes;
                            RaiseEvt = true;
                            status = InvtryOpStatus.errorStopped;
                            SpMsgWnd.MsgNotifyFloat -= TagRssiMsgArrv;
                            throw new ApplicationException("CF.f_CFlow_RfidDev_CustoMGetTagInvtryRssi() returns: "
                                + hRes.ToString("F"));
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomTagInvtryRssi:
                    if (hRes == HRESULT_RFID.E_RFID_ERROR_OPERATION_CANCELLED)
                        hRes = HRESULT_RFID.S_OK; // override to avoid false positive
                    // TBD: Power down the device
                    if (InvtryOpStEvent != null)
                    {
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            status = InvtryOpStatus.errorStopped;
                            ErrMsg = hRes.ToString("F");
                            RaiseEvt = true;
                        }
                        else if (GetMacError(out macErrCode) && macErrCode != 0)
                        {
                            LogMacError();
                            status = InvtryOpStatus.macErrorStopped;
                            ErrMsg = "Hardware Failure! (mac) Error code: 0x" + macErrCode.ToString("X4");
                            RaiseEvt = true;
                        }
                        else
                        {
                            status = InvtryOpStatus.stopped;
                            RaiseEvt = true;
                        }
                    }
                    SpMsgWnd.MsgNotifyFloat -= TagRssiMsgArrv;
                    break;
                default: // ignore the rest
                    break;
            }
            if (RaiseEvt == true && InvtryOpStEvent != null)
            {
                InvtryOpStEvent(this, new InvtryOpEventArgs(status, ErrMsg));
            }
            if (status == InvtryOpStatus.errorStopped || status == InvtryOpStatus.stopped
                || status == InvtryOpStatus.macErrorStopped)
            {
                SpMsgWnd.MsgNotifyHRes -= TagInvtryRssiMsgArrv;
                CheckAndClear(ref TagSrchRssiStopReq);
            }
        }

        private void TagRssiMsgArrv(RFID_MSGID msgID, float Rssi)
        {
            switch (msgID)
            {
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_RSSI:
                    if (TagRssiEvent != null)
                    {
                        TagRssiEvent(this, new TagRssiEventArgs(Rssi));
                    }
                    break;
            }
        }

        private bool rssiFilterRes = false;
        public bool TagInvtryRssiStart(RFID_18K6C_INVENTORY_SESSION session,
            bool search1Only, Byte[] epcBnkMask, UInt32 maskOffset, bool filterRes)
        {
            rssiFilterRes = filterRes;
            TagOperSetup(
                search1Only ? RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ
                : RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ,
                (uint)(search1Only ? 1 : 7), session,
                RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_B,
                false, epcBnkMask, maskOffset, RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA);
            tagInvtryStopCnt = TagStopCountUnlimited;
            return TagInvtryStart(true, TagInvtryRssiStartNotify);
        }
#if false
        public bool TagInvtryRssiStart()
        {
            return TagInvtryStart(true, TagInvtryRssiStartNotify);
        }
#endif
        public bool TagInvtryRssiStop()
        {
            TagSrchRssiStopReq = true;
            return CancelCurrentOp();
        }
        #endregion

        #region Misc Certification Test Routines
        private bool SetTildenRegister(UInt32 addr, UInt32 value)
        {
            HRESULT_RFID hRes;

            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
        set_addr:
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0x0400;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = addr;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                if (errCode == HRESULT_RFID.E_RFID_ERROR_RADIO_BUSY)
                    goto set_addr;
                return false;
            }
        //Thread.Sleep(100);
        set_value:
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0xf000;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = value;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            //Thread.Sleep(100);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                if (errCode == HRESULT_RFID.E_RFID_ERROR_RADIO_BUSY)
                    goto set_value;
                return false;
            }

            return true;
        }

        private bool SetTildenFifo(UInt32 addr, UInt32 addr2, UInt32 value)
        {
            HRESULT_RFID hRes;

            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
        set_addr1:
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0x0400;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = addr;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            //Thread.Sleep(100);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                if (errCode == HRESULT_RFID.E_RFID_ERROR_RADIO_BUSY)
                    goto set_addr1;
                return false;
            }
        set_addr2:
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0x0401;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = addr2;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            //Thread.Sleep(100);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                if (errCode == HRESULT_RFID.E_RFID_ERROR_RADIO_BUSY)
                    goto set_addr2;
                return false;
            }
        set_value:
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0xf000;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = value;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            //Thread.Sleep(100);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                if (errCode == HRESULT_RFID.E_RFID_ERROR_RADIO_BUSY)
                    goto set_value;
                return false;
            }
            return true;
        }

        public bool CarrierWaveOn()
        {
            if (!RadioReady())
                return false;
            // Turn on CW
            HRESULT_RFID hRes;
            hRes = CF.f_CFlow_RfidDev_RadioTurnCarrierWaveOn(Rfid.st_RfidSpReq_RadioOpen.handle);
            return SP.SUCCEEDED(hRes);
        }

        public bool CarrierWaveOff()
        {
            if (!RadioReady())
                return false;
            // Turn off CW
            HRESULT_RFID hRes;
            hRes = CF.f_CFlow_RfidDev_RadioTurnCarrierWaveOff(Rfid.st_RfidSpReq_RadioOpen.handle);
            return SP.SUCCEEDED(hRes);
        }

        public bool RFGenerateRandomDataSetup()
        {
            if (!RadioReady())
                return false;

            // Other stuff
            if (!SetTildenRegister(0x10b, 0x05))
                return false;
            if (!SetTildenRegister(0x10c, 0x05))
                return false;

            return true;
        }

        public bool RFGenerateRandomData(uint size)
        {
            bool Succ = true;
            int i;
            for (i = 0; i < size; i++)
            {
                if (!SetTildenFifo(0x105, 0x0e, 0x06))
                {
                    Succ = false;
                    break;
                }
            }
            return Succ;
        }

        public bool RFStopRandomData()
        {
            // is there any other way besides turning the carrier wave off?

            return true;
        }
        #endregion

        #region Temperature Monitoring Routines
        // Only Transeiver Threshold uses user argument
        // The other 3 hardcoded to 0x7D (125)


        private void CustomThrshTempMsgArrv(RFID_MSGID msgID)
        {
            bool Arrived = false;
            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetThrshTemperature:
                    if (thrshTempGetNotify != null)
                    {
                        thrshTempGetNotify(Rfid.st_RfidSpPkt_CustomTemp.amb,
                            Rfid.st_RfidSpPkt_CustomTemp.xceiver,
                            Rfid.st_RfidSpPkt_CustomTemp.pow_amp,
                            Rfid.st_RfidSpPkt_CustomTemp.pow_amp_delta);
                    }
                    Arrived = true;
                    break;
            }
            if (Arrived)
                SpMsgWnd.MsgNotify -= CustomThrshTempMsgArrv;
        }

        public bool GetCurrTemp(out ushort amb, out ushort xcvr, out ushort pamp)
        {
            amb = 0; xcvr = 0; pamp = 0;
            if (!RadioReady())
                return false;
            HRESULT_RFID HRes = CF.f_CFlow_RfidDev_CustomGetTemperature(
                Rfid.st_RfidSpReq_RadioOpen.handle, out amb, out xcvr, out pamp);
            if (!SP.SUCCEEDED(HRes))
            {
                errCode = HRes;
                return false;
            }
            else
            {
                // TempMonitor and other TempUpdateEvent listener needs this
                if (TempUpdateEvt != null)
                    TempUpdateEvt(this, new TempUpdateEventArgs(
                        amb, xcvr, pamp));
            }
            return true;
        }

        public bool SetTempThreshold(ushort xcvrLmt, ushort ambLmt, ushort paLmt, ushort deltaLmt)
        {
            if (!RadioReady())
                return false;

            // initialize common parameters
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            HRESULT_RFID hRes;
            //Ambient
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0x0B07;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = (uint)ambLmt;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                return false;
            }
            // Power Amp
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0x0B0B;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = (uint)paLmt;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                return false;
            }
            // Delta (PA)
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0x0B0C;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = (uint)deltaLmt;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            if (!SP.SUCCEEDED(hRes))
                return false;

            // Xcvr
            return SetTempThreshold(xcvrLmt);
        }

        public bool SetTempThreshold(ushort xcvrLmt)
        {
            if (!RadioReady())
                return false;

            HRESULT_RFID hRes;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.parameter = 0x0B09;
            Rfid.st_RfidSpReq_RadioSetConfigurationParameter.value = (uint)xcvrLmt;
            hRes = CF.f_CFlow_RfidDev_RadioSetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioSetConfigurationParameter);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                return false;
            }
            return true;
        }

     
        public event EventHandler<TempUpdateEventArgs> TempUpdateEvt;

        private void TempUpdateMsgArrv(RFID_MSGID msgID)
        {
            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetTemperature:
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_TEMP:
                    if (TempUpdateEvt != null)
                        TempUpdateEvt(this, new TempUpdateEventArgs(
                            Rfid.st_RfidSpPkt_CustomTemp.amb,
                            Rfid.st_RfidSpPkt_CustomTemp.xceiver,
                            Rfid.st_RfidSpPkt_CustomTemp.pow_amp));
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetThrshTemperature:
                    break;
            }

        }
        private void InitTempUpdateEvt()
        {
            SpMsgWnd.MsgNotify += TempUpdateMsgArrv;
        }

       // public delegate void ThrshTempGetNotify(UInt16 amb, UInt16 xcvr, UInt16 pwrAmp, UInt16 paDetla);
        private ThrshTempGetNotify thrshTempGetNotify;

        public bool GetTempThreshold(ThrshTempGetNotify notify)
        {
            if (!RadioReady())
                return false;

            thrshTempGetNotify = notify;
            SpMsgWnd.MsgNotify += CustomThrshTempMsgArrv;
            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_CustomGetThrshTemperature(Rfid.st_RfidSpReq_RadioOpen.handle);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                SpMsgWnd.MsgNotify -= CustomThrshTempMsgArrv;
            }
            return SP.SUCCEEDED(hRes);
        }
        #endregion

        #region Mac Error Routines
        public static bool MacErrorIsOverheat(ushort macErrCode)
        {
            return (macErrCode == 0x0305 // RFTC_ERR_AMBIENTTEMPTOOHOT
                || macErrCode == 0x0306 // RFTC_ERR_XCVRTEMPTOOHOT
                || macErrCode == 0x0307 // RFTC_ERR_PATEMPTOOHOT
                || macErrCode == 0x0308 // PADELTATEMPTOOBIG
            );
        }

        public static bool MacErrorIsFatal(ushort macErrCode)
        {
            return (macErrCode == 0x0305 // RFTC_ERR_AMBIENTTEMPTOOHOT
               || macErrCode == 0x0306 // RFTC_ERR_XCVRTEMPTOOHOT
               || macErrCode == 0x0307 // RFTC_ERR_PATEMPTOOHOT
               || macErrCode == 0x0308 // RFTC_ERR_PADELTATEMPTOOBIG
               || macErrCode == 0x0309 // RFTC_ERR_REVPWRLEVTOOHIGH   
           );
        }

        public static bool MacErrorIsNegligible(ushort macErrCode)
        {
            // According to Release notes 1.1, 1.2
            return (macErrCode == 0x0310
                || macErrCode == 0x0312);
        }

        public bool GetMacError(out ushort macerr)
        {
            macerr = 0x0000;
            if (!RadioReady())
                return false;

            Rfid.st_RfidSpReq_RadioGetConfigurationParameter.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioGetConfigurationParameter.parameter = 0x0005;
            HRESULT_RFID HRes = CF.f_CFlow_RfidDev_RadioGetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioGetConfigurationParameter);

            if (!SP.SUCCEEDED(HRes))
            {
                errCode = HRes;
                return false;
            }
#if true
            macerr = (ushort)(Rfid.st_RfidSpReq_RadioGetConfigurationParameter.value & 0x0000FFFF);
#else
            macerr = 0x0309; // testing
#endif

            return true;
        }

        public bool ClearMacError()
        {
            if (!RadioReady())
                return false;

            Rfid.st_RfidSpReq_MacClearError.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            HRESULT_RFID HRes = CF.f_CFlow_RfidDev_MacClearError(ref Rfid.st_RfidSpReq_MacClearError);
            if (SP.SUCCEEDED(HRes) == false)
            {
                errCode = HRes;
                return false;
            }
            return true;
        }

        public bool GetPwrLvlRegVals(out UInt32 pwrLvl, out UInt32 revPwr, out UInt32 pwrThrsh)
        {
            pwrLvl = 0; revPwr = 0; pwrThrsh = 0;

            if (!RadioReady())
                return false;

            HRESULT_RFID HRes;
#if false // for checking current setting in case the forward,reverse pwr register returns 0.
            // See if Forward Power Detector parameters would make these power registers value
            // available
            Rfid.st_RfidSpReq_MacReadOemData.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_MacReadOemData.address = 0x0000009F;
            Rfid.st_RfidSpReq_MacReadOemData.count = 1;
            Rfid.st_RfidSpReq_MacReadOemData.pData = new uint[1];

            HRes = CF.f_CFlow_RfidDev_MacReadOemData(ref Rfid.st_RfidSpReq_MacReadOemData);
            if (!SP.SUCCEEDED(HRes))
            {
                errCode = HRes;
                return false;
            }
#endif
            Rfid.st_RfidSpReq_RadioGetConfigurationParameter.handle = Rfid.st_RfidSpReq_RadioOpen.handle;

            // MAC_RFTC_PAPWRLEV : 0x0B00
            Rfid.st_RfidSpReq_RadioGetConfigurationParameter.parameter = 0x0B00;
            HRes = CF.f_CFlow_RfidDev_RadioGetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioGetConfigurationParameter);
            if (!SP.SUCCEEDED(HRes))
            {
                errCode = HRes;
                return false;
            }
            pwrLvl = Rfid.st_RfidSpReq_RadioGetConfigurationParameter.value;

            // MAC_RFTC_REVPWRLEV: 0x0B04
            Rfid.st_RfidSpReq_RadioGetConfigurationParameter.parameter = 0x0B04;
            HRes = CF.f_CFlow_RfidDev_RadioGetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioGetConfigurationParameter);
            if (!SP.SUCCEEDED(HRes))
            {
                errCode = HRes;
                return false;
            }
            revPwr = Rfid.st_RfidSpReq_RadioGetConfigurationParameter.value;

            // HST_RFTC_REVPWRTHRSH: 0x0B05
            Rfid.st_RfidSpReq_RadioGetConfigurationParameter.parameter = 0x0B05;
            HRes = CF.f_CFlow_RfidDev_RadioGetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioGetConfigurationParameter);
            if (!SP.SUCCEEDED(HRes))
            {
                errCode = HRes;
                return false;
            }
            pwrThrsh = Rfid.st_RfidSpReq_RadioGetConfigurationParameter.value;

            return true;
        }

        private void LogMacError()
        {
            String MacErrDetailMsg = String.Empty;

            if (macErrCode == 0x0309) //RFTC_ERR_REVPWRLEVTOOHIGH
            {
                // Read the Power Level Registers (Fwd, Rev) + Threshold
                UInt32 PwrLevRegVal = 0;
                UInt32 RevPwrLevRegVal = 0;
                UInt32 RevPwrThrshRegVal = 0;
                if (GetPwrLvlRegVals(out PwrLevRegVal, out RevPwrLevRegVal, out RevPwrThrshRegVal) == true)
                {
                    MacErrDetailMsg = "MAC_RFTC_PAPWRLEV: 0x" + PwrLevRegVal.ToString("X8") + " " + ((float)(PwrLevRegVal * 0.1F)).ToString("F1") + " dBm"
                    + "; MAC_RFTC_REVPWRLEV: 0x" + RevPwrLevRegVal.ToString("X8") + " " + ((float)(RevPwrLevRegVal * 0.1F)).ToString("F1") + " dBm"
                    + "; HST_RFTC_REVPWRTHRSH: 0x" + RevPwrThrshRegVal.ToString("X8") + " " + ((float)(RevPwrThrshRegVal * 0.1F)).ToString("F1") + " dBm";
                }
            }

            String MacErrMsg = "Hardware Failure! (mac) Error code: 0x" + macErrCode.ToString("X4");
            Datalog.LogErr(MacErrMsg);
            if (String.IsNullOrEmpty(MacErrDetailMsg) == false)
                Datalog.LogErr(MacErrDetailMsg);

        }

        #endregion

        #region Individual Settings/Configuration Routines
        private HRESULT_RFID _SetResponseDataMode(RFID_RESPONSE_MODE mode)
        {
            Rfid.st_RfidSpReq_RadioSetResponseDataMode.handle
                = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioSetResponseDataMode.responseMode
                = (uint)mode;
            Rfid.st_RfidSpReq_RadioSetResponseDataMode.responseType
                = (uint)RFID_RESPONSE_TYPE.RFID_RESPONSE_TYPE_DATA;
            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_RadioSetResponseDataMode(ref Rfid.st_RfidSpReq_RadioSetResponseDataMode);
            if (!SP.SUCCEEDED(hRes))
                errCode = hRes;
            return hRes;
        }

       // public delegate void RespDatModeSetNotify(bool succ, String errMsg);
       private RespDatModeSetNotify respDatModeSetNotify;

        public bool SetResponseDataMode(RFID_RESPONSE_MODE mode)
        {
            if (!RadioReady())
                return false;
            HRESULT_RFID hRes = _SetResponseDataMode(mode);
            return SP.SUCCEEDED(hRes);
        }

        private void RespDatModeMsgArrv(RFID_MSGID msgID)
        {
            bool Handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetResponseDataMode:
                    Handled = true;
                    HRESULT_RFID HRes = Rfid.st_RfidSpReq_RadioSetResponseDataMode.status;
                    if (!SP.SUCCEEDED(HRes))
                    {
                        errCode = HRes;
                    }
                    if (respDatModeSetNotify != null)
                        respDatModeSetNotify(SP.SUCCEEDED(HRes), HRes.ToString("F"));
                    break;
            }
            if (Handled)
                SpMsgWnd.MsgNotify -= RespDatModeMsgArrv;
        }

        public bool SetResponseDataMode(RFID_RESPONSE_MODE mode, RespDatModeSetNotify notify)
        {
            if (!RadioReady())
                return false;
            respDatModeSetNotify = notify;
            SpMsgWnd.MsgNotify += RespDatModeMsgArrv;
            HRESULT_RFID hRes = _SetResponseDataMode(mode);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                respDatModeSetNotify = null;
                SpMsgWnd.MsgNotify -= RespDatModeMsgArrv;
            }
            return SP.SUCCEEDED(hRes);
        }


        public delegate void QueryParmGetNotify(bool succ,
            ref RFID_18K6C_QUERY_PARMS parms, String errMsg);
        public delegate void QueryParmSetNotify(bool err, String errMsg);
        private QueryParmGetNotify qryParmGetNotify;
        private QueryParmSetNotify qryParmSetNotify;

        private void QueryParmsMsgArrv(RFID_MSGID msgID)
        {
            bool handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CGetQueryParameters:
                    HRESULT_RFID hRes = Rfid.st_RfidSpReq_18K6CGetQueryParameters.status;
                    bool Succ = SP.SUCCEEDED(hRes);
                    if (qryParmGetNotify != null)
                    {
                        qryParmGetNotify(Succ, ref Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms,
                            (Succ ? null : hRes.ToString("F")));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    break;
            }
            if (handled)
            {
                SpMsgWnd.MsgNotify -= QueryParmsMsgArrv;
                qryParmGetNotify = null;
            }
        }

        private void QueryParmsHResMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            bool handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    bool Succ = SP.SUCCEEDED(hRes);
                    if (qryParmSetNotify != null)
                    {
                        qryParmSetNotify(Succ, (Succ ? null : hRes.ToString("F")));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    break;
            }
            if (handled)
            {
                SpMsgWnd.MsgNotifyHRes -= QueryParmsHResMsgArrv;
                qryParmSetNotify = null;
            }
        }

        public bool QueryParmsGet(QueryParmGetNotify notifyCb)
        {
            if (!RadioReady())
                return false;
            qryParmGetNotify = notifyCb;
            SpMsgWnd.MsgNotify += QueryParmsMsgArrv;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            // Note: tagGroup and singulation method/parms are not initialized
#if false
            // Iniitialize TagGroup and Singulation Method/Param to show that it 'really' works
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.tagGroup.selected
                = RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ON;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.tagGroup.session
                = RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S3;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.tagGroup.target
                = RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_B;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.singulationParms.singulationAlgorithm
                = RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.singulationParms.dynamicQ.startQValue
                = 12;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.singulationParms.dynamicQ.minQValue
                = 9;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.singulationParms.dynamicQ.maxQValue
                = 14;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.singulationParms.dynamicQ.retryCount
                = 33;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.singulationParms.dynamicQ.maxQueryRepCount
                = 127;
            Rfid.st_RfidSpReq_18K6CGetQueryParameters.parms.singulationParms.dynamicQ.toggleTarget
                = 23;
#endif
            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_18K6CGetQueryParameters(ref Rfid.st_RfidSpReq_18K6CGetQueryParameters);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                qryParmGetNotify = null;
                SpMsgWnd.MsgNotify -= QueryParmsMsgArrv;
                return false;
            }
            return true;
        }

        private HRESULT_RFID QueryParmsSet(ref RFID_18K6C_QUERY_PARMS parms)
        {
            Rfid.st_RfidSpReq_18K6CSetQueryParameters.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CSetQueryParameters.flags = 0;  // RESERVED
            // Note: tagGroup and singulation method/parms are not relying on GetQuery, but instead
            //            uses default value (as tried on the unit)
            // deep copy
            Rfid.st_RfidSpReq_18K6CSetQueryParameters.parms.tagGroup = parms.tagGroup;
            Rfid.st_RfidSpReq_18K6CSetQueryParameters.parms.singulationParms.singulationAlgorithm = parms.singulationParms.singulationAlgorithm;
            switch (parms.singulationParms.singulationAlgorithm)
            {
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ:
                    Rfid.st_RfidSpReq_18K6CSetQueryParameters.parms.singulationParms.fixedQ = parms.singulationParms.fixedQ;
                    break;
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ:
                    Rfid.st_RfidSpReq_18K6CSetQueryParameters.parms.singulationParms.dynamicQ = parms.singulationParms.dynamicQ;
                    break;
            }
            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_18K6CSetQueryParameters(ref Rfid.st_RfidSpReq_18K6CSetQueryParameters);
            return hRes;
        }

        public bool QueryParmsSet(ref RFID_18K6C_QUERY_PARMS parms,
            QueryParmSetNotify notifyCb)
        {
            if (!RadioReady())
                return false;
            qryParmSetNotify = notifyCb;
            SpMsgWnd.MsgNotifyHRes += QueryParmsHResMsgArrv;
            HRESULT_RFID hRes = QueryParmsSet(ref parms);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                qryParmSetNotify = null;
                SpMsgWnd.MsgNotifyHRes -= QueryParmsHResMsgArrv;
                return false;
            }
            return true;
        }



      //  public delegate void CustomFreqSetNotify(bool succ, String errMsg);
       // public delegate void CustomFreqGetNotify(bool succ, CustomFreqGrp freqGrp, int chn, bool enLBT);
        private CustomFreqSetNotify custFreqSetNotify;
        private CustomFreqGetNotify custFreqGetNotify;

        private void CustomFreqGetMsgArrv(RFID_MSGID msgID, CustomFreqGrp freqGrp, int chn, bool enLBT)
        {
            bool handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetRadioProfile:
                    if (custFreqGetNotify != null)
                    {
                        custFreqGetNotify(true, freqGrp, chn, enLBT);
                    }
                    handled = true;
                    break;
                default:
                    break;
            }

            if (handled)
            {
                custFreqSetNotify = null;
                SpMsgWnd.MsgNotifyHRes -= CustomFreqSetHResArrv;
            }

        }


        private void CustomFreqSetHResArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            bool handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomSetRadioChn:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomSetRadioProfile:
                    bool Succ = SP.SUCCEEDED(hRes);
                    if (custFreqSetNotify != null)
                    {
                        custFreqSetNotify(Succ, Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    break;
            }

            if (handled)
            {
                custFreqSetNotify = null;
                SpMsgWnd.MsgNotifyHRes -= CustomFreqSetHResArrv;
            }

        }

        public bool CustomFreqGet(CustomFreqGetNotify notify)
        {
            HRESULT_RFID hRes;

            custFreqGetNotify = notify;
            SpMsgWnd.MsgNotifyFreqGrp += CustomFreqGetMsgArrv;
            hRes = CF.f_CFlow_RfidDev_CustomGetRadioProfile(Rfid.st_RfidSpReq_RadioOpen.handle);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                SpMsgWnd.MsgNotifyFreqGrp -= CustomFreqGetMsgArrv;
                custFreqGetNotify = null;
                return false;
            }
            return true;
        }


        public bool CustomFreqSet(CustomFreqGrp freqSet, bool enLBT, CustomFreqSetNotify notify)
        {
            HRESULT_RFID hRes;

            custFreqSetNotify = notify;
            SpMsgWnd.MsgNotifyHRes += CustomFreqSetHResArrv;
            hRes = CF.f_CFlow_RfidDev_CustomSetRadioProfile(Rfid.st_RfidSpReq_RadioOpen.handle, freqSet, enLBT);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                SpMsgWnd.MsgNotifyHRes -= CustomFreqSetHResArrv;
                custFreqSetNotify = null;
                return false;
            }
            return true;
        }

        public bool CustomFreqSet(CustomFreqGrp freqSet, CustomFreqSetNotify notify)
        {
            HRESULT_RFID hRes;

            custFreqSetNotify = notify;
            SpMsgWnd.MsgNotifyHRes += CustomFreqSetHResArrv;
            hRes = CF.f_CFlow_RfidDev_CustomSetRadioProfile(Rfid.st_RfidSpReq_RadioOpen.handle, freqSet, false);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                SpMsgWnd.MsgNotifyHRes -= CustomFreqSetHResArrv;
                custFreqSetNotify = null;
                return false;
            }
            return true;
        }

        // channel of a specific CustomFreqGrp
        public bool CustomFreqSet(CustomFreqGrp freqSet, int chn, CustomFreqSetNotify notify)
        {
            return CustomFreqSet(freqSet, chn, false, notify);
        }

        public bool CustomFreqSet(CustomFreqGrp freqSet, int chn, bool enLBT, CustomFreqSetNotify notify)
        {
            HRESULT_RFID hRes;
            custFreqSetNotify = notify;
            SpMsgWnd.MsgNotifyHRes += CustomFreqSetHResArrv;
            hRes = CF.f_CFlow_RfidDev_CustomSetRadioChn(Rfid.st_RfidSpReq_RadioOpen.handle, freqSet, chn, enLBT);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                SpMsgWnd.MsgNotifyHRes -= CustomFreqSetHResArrv;
                custFreqSetNotify = null;
                return false;
            }
            return true;
        }


        public bool CustomFreqBandNumGet(out uint bandNum)
        {
            bandNum = 2;

            if (!RadioReady())
                return false;

            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_CustomGetFreqBandNum(Rfid.st_RfidSpReq_RadioOpen.handle, out bandNum);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                return false;
            }
            return true;
        }

        public bool CustomFreqBandNumSet(UInt32 bandNum)
        {
            if (!RadioReady())
                return false;
            return (SP.SUCCEEDED(
                CF.f_CFlow_RfidDev_CustomSetFreqBandNum(Rfid.st_RfidSpReq_RadioOpen.handle, bandNum)));
        }

        public delegate void LnkProfNumGetNotify(bool succ, UInt32 profNum, String errMsg);
        //public delegate void LnkProfNumSetNotify(bool succ, String errMsg);
        LnkProfNumGetNotify lnkProfNumGetNotify;
        LnkProfNumSetNotify lnkProfNumSetNotify;

        private void CurrLnkProfMsgArrv(RFID_MSGID msgID)
        {
            HRESULT_RFID hRes;
            bool handled = false;
            bool Succ = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetCurrentLinkProfile:
                    hRes = Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile.status;
                    Succ = SP.SUCCEEDED(hRes);
                    if (lnkProfNumGetNotify != null)
                    {
                        lnkProfNumGetNotify(Succ, Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile.profile,
                            Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetCurrentLinkProfile:
                    hRes = Rfid.st_RfidSpReq_RadioSetCurrentLinkProfile.status;
                    Succ = SP.SUCCEEDED(hRes);
                    if (lnkProfNumSetNotify != null)
                    {
                        lnkProfNumSetNotify(Succ, Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                lnkProfNumGetNotify = null;
                SpMsgWnd.MsgNotify -= CurrLnkProfMsgArrv;
            }
        }

        public bool LinkProfNumGet(out UInt32 profNum)
        {
            profNum = 2;
            if (!RadioReady())
                return false;
            Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_RadioGetCurrentLinkProfile(
                ref Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                return false;
            }
            profNum = Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile.profile;
            return true;
        }

        public bool LinkProfNumSet(UInt32 lnkProfNum, LnkProfNumSetNotify notify)
        {
            if (!RadioReady())
                return false;
            lnkProfNumSetNotify = notify;
            SpMsgWnd.MsgNotify += CurrLnkProfMsgArrv;
            Rfid.st_RfidSpReq_RadioSetCurrentLinkProfile.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioSetCurrentLinkProfile.profile = lnkProfNum;
            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_RadioSetCurrentLinkProfile(
                ref Rfid.st_RfidSpReq_RadioSetCurrentLinkProfile);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                lnkProfNumSetNotify = null;
                SpMsgWnd.MsgNotify -= CurrLnkProfMsgArrv;
                return false;
            }
            return true;
        }

       // public delegate void LnkProfInfoGetNotify(bool succ, ref RFID_RADIO_LINK_PROFILE lnkPrf, String errMsg);
        private LnkProfInfoGetNotify lnkProfInfoGetNotify;

        private void LnkProfInfoMsgArrv(RFID_MSGID msgID)
        {
            HRESULT_RFID hRes;
            bool handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetLinkProfile:
                    hRes = Rfid.st_RfidSpReq_RadioGetLinkProfile.status;
                    bool Succ = SP.SUCCEEDED(hRes);
                    if (lnkProfInfoGetNotify != null)
                    {
                        lnkProfInfoGetNotify(Succ, ref Rfid.st_RfidSpReq_RadioGetLinkProfile.linkProfileInfo,
                            Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    handled = false;
                    break;
            }

            if (handled)
            {
                lnkProfInfoGetNotify = null;
                SpMsgWnd.MsgNotify -= LnkProfInfoMsgArrv;
            }
        }

        public bool LinkProfInfoGet(UInt32 lnkProfNum, LnkProfInfoGetNotify notify)
        {
            if (!RadioReady())
                return false;
            lnkProfInfoGetNotify = notify;
            SpMsgWnd.MsgNotify += LnkProfInfoMsgArrv;
            Rfid.st_RfidSpReq_RadioGetLinkProfile.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_RadioGetLinkProfile.profile = lnkProfNum;
            HRESULT_RFID hRes = CF.f_CFlow_RfidDev_RadioGetLinkProfile(ref Rfid.st_RfidSpReq_RadioGetLinkProfile);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                lnkProfInfoGetNotify = null;
                SpMsgWnd.MsgNotify -= LnkProfInfoMsgArrv;
                return false;
            }
            return true;
        }

        #region Antenna Port (Get Status, Set State, Get-Set PortConfiguration)
        public delegate void AntPortStatusGetAllNotify(bool succ,
            RFID_ANTENNA_PORT_STATUS[] ports, String errMsg);
        public delegate void AntPortCfgGetAllNotify(bool succ,
            RFID_ANTENNA_PORT_CONFIG[] cfgs, String errMsg);
        public delegate void AntPortStatusGetOneNotify(bool succ,
            uint portNum, ref RFID_ANTENNA_PORT_STATUS status, String errMsg);
        //public delegate void AntPortCfgGetOneNotify(bool succ,
        //    uint portNum, ref RFID_ANTENNA_PORT_CONFIG cfg, String errMsg);
        //public delegate void AntPortCfgSetOneNotify(bool succ, uint portNum,
        //    String errMsg);

        private AntPortStatusGetAllNotify antPortStatusGetAllNotify;
        private AntPortCfgGetAllNotify antPortCfgGetAllNotify;
        private RFID_ANTENNA_PORT_STATUS[] antPortStatusArr = null;
        private RFID_ANTENNA_PORT_CONFIG[] antPortCfgArr = null;
        private AntPortStatusGetOneNotify antPortStatusGetOneNotify;
        private AntPortCfgGetOneNotify antPortCfgGetOneNotify;
        private AntPortCfgSetOneNotify antPortCfgSetOneNotify;

        private void AntPortStatusGetOneMsgArrv(RFID_MSGID msgID, uint antNum)
        {
            HRESULT_RFID hRes;
            bool handled = false;
            bool Succ = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetStatus:
                    hRes = Rfid.st_RfidSpReq_AntennaPortGetStatus[antNum].status;
                    Succ = SP.SUCCEEDED(hRes);
                    if (antPortStatusGetOneNotify != null)
                    {
                        antPortStatusGetOneNotify(Succ, antNum, ref Rfid.st_RfidSpReq_AntennaPortGetStatus[antNum].portStatus, Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                antPortStatusGetOneNotify = null;
                SpMsgWnd.MsgNotifyUINT32 -= AntPortStatusGetOneMsgArrv;
            }

        }

        private void AntPortCfgGetOneMsgArrv(RFID_MSGID msgID, uint antNum)
        {
            HRESULT_RFID hRes;
            bool handled = false;
            bool Succ = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetConfiguration:
                    hRes = Rfid.st_RfidSpReq_AntennaPortGetConfiguration[antNum].status;
                    Succ = SP.SUCCEEDED(hRes);
                    if (antPortCfgGetOneNotify != null)
                    {
                        antPortCfgGetOneNotify(Succ, antNum, ref Rfid.st_RfidSpReq_AntennaPortGetConfiguration[antNum].config, Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                antPortCfgGetOneNotify = null;
                SpMsgWnd.MsgNotifyUINT32 -= AntPortCfgGetOneMsgArrv;
            }

        }

        // Expects LOGICAL_ANTENNA_COUNT consecutive Status message
        // or
        // Expects LOGICAL_ANTENNA_COUNT consecutive Port Config message
        private void AntPortStatusGetAllMsgArrv(RFID_MSGID msgID, uint antNum)
        {
            HRESULT_RFID hRes;
            bool handled = false;
            bool Succ = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetStatus:
                    if (antPortStatusArr == null)
                        antPortStatusArr = new RFID_ANTENNA_PORT_STATUS[Rfid.LOGICAL_ANTENNA_COUNT];
                    hRes = Rfid.st_RfidSpReq_AntennaPortGetStatus[antNum].status;
                    Succ = SP.SUCCEEDED(hRes);
                    if (Succ)
                    {
                        // shallow copy
                        antPortStatusArr[antNum] = Rfid.st_RfidSpReq_AntennaPortGetStatus[antNum].portStatus;
                    }
                    if (antNum == (Rfid.LOGICAL_ANTENNA_COUNT - 1))
                    {
                        // last one. no more coming, notify delegates
                        if (antPortStatusGetAllNotify != null)
                            antPortStatusGetAllNotify(Succ, antPortStatusArr, Succ ? null : hRes.ToString("F"));
                        antPortStatusGetAllNotify = null;
                        handled = true;
                    }
                    if (!Succ)
                        errCode = hRes;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                SpMsgWnd.MsgNotifyUINT32 -= AntPortStatusGetAllMsgArrv;
            }
        }

        private void AntPortCfgGetAllMsgArrv(RFID_MSGID msgID, uint antNum)
        {
            HRESULT_RFID hRes;
            bool handled = false;
            bool Succ = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetConfiguration:
                    if (antPortCfgArr == null)
                        antPortCfgArr = new RFID_ANTENNA_PORT_CONFIG[Rfid.LOGICAL_ANTENNA_COUNT];
                    Succ = false;
                    hRes = Rfid.st_RfidSpReq_AntennaPortGetConfiguration[antNum].status;
                    Succ = SP.SUCCEEDED(hRes);
                    if (Succ)
                    {
                        // shallow copy
                        antPortCfgArr[antNum] = Rfid.st_RfidSpReq_AntennaPortGetConfiguration[antNum].config;
                    }
                    if (antNum == (Rfid.LOGICAL_ANTENNA_COUNT - 1))
                    {
                        // last one. no more coming, notify delegates
                        if (antPortCfgGetAllNotify != null)
                            antPortCfgGetAllNotify(Succ, antPortCfgArr, Succ ? null : hRes.ToString("F"));
                        antPortCfgGetAllNotify = null;
                        handled = true;
                    }
                    if (!Succ)
                        errCode = hRes;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                SpMsgWnd.MsgNotifyUINT32 -= AntPortCfgGetAllMsgArrv;
            }
        }

        public bool AntPortStatusGetOne(uint portNum, AntPortStatusGetOneNotify notify)
        {
            if (!RadioReady())
                return false;

            antPortStatusGetOneNotify = notify;
            SpMsgWnd.MsgNotifyUINT32 += AntPortStatusGetOneMsgArrv;
            HRESULT_RFID hRes;
            Rfid.st_RfidSpReq_AntennaPortGetStatus[portNum].handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_AntennaPortGetStatus[portNum].antennaPort = portNum;
            hRes = CF.f_CFlow_RfidDev_AntennaPortGetStatus(
                ref Rfid.st_RfidSpReq_AntennaPortGetStatus[portNum]);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                antPortStatusGetOneNotify = null;
                SpMsgWnd.MsgNotifyUINT32 -= AntPortStatusGetOneMsgArrv;
                return false;
            }
            return true;
        }

        public bool AntPortStatusGetAll(AntPortStatusGetAllNotify notify)
        {
            if (!RadioReady())
                return false;

            antPortStatusGetAllNotify = notify;
            SpMsgWnd.MsgNotifyUINT32 += AntPortStatusGetAllMsgArrv;
            // return false even if 'only' one of the fail
            HRESULT_RFID hRes;
            for (int i = 0; i < Rfid.LOGICAL_ANTENNA_COUNT; i++)
            {
                Rfid.st_RfidSpReq_AntennaPortGetStatus[i].handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                Rfid.st_RfidSpReq_AntennaPortGetStatus[i].antennaPort = (uint)i;
                hRes = CF.f_CFlow_RfidDev_AntennaPortGetStatus(
                 ref Rfid.st_RfidSpReq_AntennaPortGetStatus[i]);
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    antPortStatusGetAllNotify = null;
                    SpMsgWnd.MsgNotifyUINT32 -= AntPortStatusGetAllMsgArrv;
                    return false;
                }
            }
            return true;
        }

        private bool AntPortCfgGetOne(uint portNum, out HRESULT_RFID hRes)
        {
            Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum].handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum].antennaPort = portNum;
            hRes = CF.f_CFlow_RfidDev_AntennaPortGetConfiguration(
                ref Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum]);
            bool Succ = SP.SUCCEEDED(hRes);
            if (!Succ)
            {
                errCode = hRes;
            }
            return Succ;
        }

        public bool AntPortCfgGetOne(uint portNum, AntPortCfgGetOneNotify notify)
        {
            if (!RadioReady())
                return false;

            // If already loaded, then no need to ask for the Radio
            if ((Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum].handle != RfidSp.RFID_INVALID_RADIO_HANDLE)
                && (SP.SUCCEEDED(Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum].status)))
            {
                if (notify != null)
                    notify(true, portNum, ref Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum].config, null);
            }
            else
            {
                antPortCfgGetOneNotify = notify;
                SpMsgWnd.MsgNotifyUINT32 += AntPortCfgGetOneMsgArrv;
                HRESULT_RFID hRes;
                if (!AntPortCfgGetOne(portNum, out hRes))
                {
                    errCode = hRes;
                    antPortCfgGetOneNotify = null;
                    SpMsgWnd.MsgNotifyUINT32 -= AntPortCfgGetOneMsgArrv;
                    return false;
                }
            }
            return true;
        }

        public bool AntPortCfgGetAll(AntPortCfgGetAllNotify notify)
        {
            if (!RadioReady())
                return false;

            antPortCfgGetAllNotify = notify;
            SpMsgWnd.MsgNotifyUINT32 += AntPortCfgGetAllMsgArrv;
            // return false even if 'only' one of the fail
            HRESULT_RFID hRes;
            for (int i = 0; i < Rfid.LOGICAL_ANTENNA_COUNT; i++)
            {
                Rfid.st_RfidSpReq_AntennaPortGetConfiguration[i].handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                Rfid.st_RfidSpReq_AntennaPortGetConfiguration[i].antennaPort = (uint)i;
                hRes = CF.f_CFlow_RfidDev_AntennaPortGetConfiguration(
                 ref Rfid.st_RfidSpReq_AntennaPortGetConfiguration[i]);
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    antPortCfgGetAllNotify = null;
                    SpMsgWnd.MsgNotifyUINT32 -= AntPortCfgGetAllMsgArrv;
                    return false;
                }
            }
            return true;
        }

        private void AntPortCfgSetOneMsgArrv(RFID_MSGID msgID, uint antNum)
        {
            HRESULT_RFID hRes;
            bool handled = false;
            bool Succ = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortSetConfiguration:
                    Succ = false;
                    hRes = Rfid.st_RfidSpReq_AntennaPortSetConfiguration.status;
                    Succ = SP.SUCCEEDED(hRes);
                    if (antPortCfgSetOneNotify != null)
                    {
                        antPortCfgSetOneNotify(Succ, Rfid.st_RfidSpReq_AntennaPortSetConfiguration.antennaPort, Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    else
                    {
                        // Copy the parameters in global PortSetConfig to the global PortGetConfig
                        Rfid.st_RfidSpReq_AntennaPortGetConfiguration[antNum].config = Rfid.st_RfidSpReq_AntennaPortSetConfiguration.config; // shallow copy
                    }
                    handled = true;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                antPortCfgSetOneNotify = null;
                SpMsgWnd.MsgNotifyUINT32 -= AntPortCfgSetOneMsgArrv;
            }

        }

        public bool AntPortCfgSetOne(uint portNum, ref RFID_ANTENNA_PORT_CONFIG cfg,
            AntPortCfgSetOneNotify notify)
        {
            if (!RadioReady())
                return false;

            antPortCfgSetOneNotify = notify;
            SpMsgWnd.MsgNotifyUINT32 += AntPortCfgSetOneMsgArrv;
            Rfid.st_RfidSpReq_AntennaPortSetConfiguration.antennaPort = portNum;
            Rfid.st_RfidSpReq_AntennaPortSetConfiguration.config = cfg;
            Rfid.st_RfidSpReq_AntennaPortSetConfiguration.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            HRESULT_RFID hRes;
            hRes = CF.f_CFlow_RfidDev_AntennaPortSetConfiguration(ref Rfid.st_RfidSpReq_AntennaPortSetConfiguration);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                antPortCfgSetOneNotify = null;
                SpMsgWnd.MsgNotifyUINT32 -= AntPortCfgSetOneMsgArrv;
                return false;
            }
            return true;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="portNum"></param>
        /// <param name="pwr">in multiples of 0.1dB</param>
        /// <param name="notify"></param>
        /// <returns></returns>
        public bool AntPortCfgSetPwr(uint portNum, int pwr, AntPortCfgSetOneNotify notify)
        {
            RFID_ANTENNA_PORT_CONFIG cfg = Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum].config;
            cfg.powerLevel = (uint)pwr;
            return AntPortCfgSetOne(portNum, ref cfg, notify);
        }

        #endregion

        #region Tag Selection Criteria
        public delegate void TagSelectCritGetNotify(bool succ, uint cntCrit,
            RFID_18K6C_SELECT_CRITERION[] criteria, String errMsg);
        public delegate void TagSelectCritSetNotify(bool succ, String errMsg);
        TagSelectCritGetNotify tagSelectCritGetNotify;
        TagSelectCritSetNotify tagSelectCritSetNotify;

        private void TagSelectCritMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes,
                                            RFID_18K6C_SELECT_CRITERION[] criteria)
        {
            bool handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CGetSelectCriteria:
                    bool Succ = SP.SUCCEEDED(hRes);
                    if (tagSelectCritGetNotify != null)
                    {
                        tagSelectCritGetNotify(Succ, (uint)(criteria != null ? criteria.Length : 0),
                            criteria, Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                tagSelectCritGetNotify = null;
                SpMsgWnd.MsgNotifySelCrit -= TagSelectCritMsgArrv;
            }
        }

        //overloaded
        private void TagSelectCritMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            bool handled = false;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                    bool Succ = SP.SUCCEEDED(hRes);
                    if (tagSelectCritSetNotify != null)
                    {
                        tagSelectCritSetNotify(Succ, Succ ? null : hRes.ToString("F"));
                    }
                    if (!Succ)
                        errCode = hRes;
                    handled = true;
                    break;
                default:
                    handled = false;
                    break;
            }
            if (handled)
            {
                tagSelectCritSetNotify = null;
                SpMsgWnd.MsgNotifyHRes -= TagSelectCritMsgArrv;
            }
        }

        public bool TagSelectCritGet(TagSelectCritGetNotify notify)
        {
            if (!RadioReady())
                return false;
            tagSelectCritGetNotify = notify;
            SpMsgWnd.MsgNotifySelCrit += TagSelectCritMsgArrv;
            Rfid.st_RfidSpReq_18K6CGetSelectCriteria.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CGetSelectCriteria.criteria.countCriteria = RfidSp.SELECTCRITERIA_COUNT;
            // Let the CallFlow take care of the CRITERION array
            HRESULT_RFID hRes;
            hRes = CF.f_CFlow_RfidDev_18K6CGetSelectCriteria(ref Rfid.st_RfidSpReq_18K6CGetSelectCriteria);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                tagSelectCritGetNotify = null;
                SpMsgWnd.MsgNotifySelCrit -= TagSelectCritMsgArrv;
                return false;
            }
            return true;
        }

        private HRESULT_RFID TagSelectCritSet(RFID_18K6C_SELECT_CRITERION[] criteria)
        {
            HRESULT_RFID hRes;
            Rfid.st_RfidSpReq_18K6CSetSelectCriteria.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CSetSelectCriteria.criteria.countCriteria = (uint)criteria.Length;
            Rfid.st_RfidSpReq_18K6CSetSelectCriteria.flags = 0;
            hRes = CF.f_CFlow_RfidDev_18K6CSetSelectCriteria(ref Rfid.st_RfidSpReq_18K6CSetSelectCriteria,
                criteria);
            return hRes;
        }

        public bool TagSelectCritSet(RFID_18K6C_SELECT_CRITERION[] criteria, TagSelectCritSetNotify notify)
        {
            if (!RadioReady())
                return false;
            tagSelectCritSetNotify = notify;
            SpMsgWnd.MsgNotifyHRes += TagSelectCritMsgArrv;

            HRESULT_RFID hRes = TagSelectCritSet(criteria);
            if (!SP.SUCCEEDED(hRes))
            {
                errCode = hRes;
                tagSelectCritSetNotify = null;
                SpMsgWnd.MsgNotifyHRes -= TagSelectCritMsgArrv;
                return false;
            }
            return true;
        }

        public static HRESULT_RFID f_CFlow_RfidDev_RadioGetConfigurationParameter()
        {
           return CF.f_CFlow_RfidDev_RadioGetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioGetConfigurationParameter);
        }

        #endregion

        #region Tag Singulation Post Match Criteria
        private HRESULT_RFID TagPostMatchCritSet(RFID_18K6C_SINGULATION_CRITERION[] criteria)
        {
            HRESULT_RFID hRes;
            Rfid.st_RfidSpReq_18K6CSetPostMatchCriteria.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
            Rfid.st_RfidSpReq_18K6CSetPostMatchCriteria.flags = 0;
            Rfid.st_RfidSpReq_18K6CSetPostMatchCriteria.criteria.countCriteria = (uint)criteria.Length;
            hRes = (HRESULT_RFID)CF.f_CFlow_RfidDev_18K6CSetPostMatchCriteria(ref Rfid.st_RfidSpReq_18K6CSetPostMatchCriteria, criteria);
            return hRes;
        }
        #endregion

        #endregion

        #region Register/Packet value conversion routines
        static public float RssiRegToDb(UInt16 rssiReg)
        {
            return ClslibRfidSp.RfidSp.RssiRegToDb(rssiReg);
        }
        #endregion

        #region Hardware Functions not related to RFID
        public void BuzzerBeep(SoundVol vol)
        {
#if true
            CF.f_CFlow_Beep(vol);
#else
            const int Duration = 10; // millisec
            const int ToneFreq = 200; // Hz(kHz?)
#if false // C# Tick is too low in resolution 1/18th of second and too much inaccurate
            // at least 100 ms past since last Beep
            long ThisTick = DateTime.UtcNow.Ticks;
            long TicksPast = (ThisTick - lastBeepTick);

            if ((TicksPast / TimeSpan.TicksPerMillisecond) > 30)
            {
#endif
                BUZZER_SOUND SndLvl = BUZZER_SOUND.BUZZER_MIDDLE_SOUND;
                switch (vol)
                {
                    case SoundVol.Low:
                        SndLvl = BUZZER_SOUND.BUZZER_LOW_SOUND;
                        break;
                    case SoundVol.Med:
                        SndLvl = BUZZER_SOUND.BUZZER_MIDDLE_SOUND;
                        break;
                    case SoundVol.High:
                        SndLvl = BUZZER_SOUND.BUZZER_HIGH_SOUND;
                        break;
                    case SoundVol.Mute:
                        return;
                }
                PS.f_PosSp_ToneOn(ToneFreq, Duration, (uint)SndLvl);
#if false // inaccurate Tick
                lastBeepTick = DateTime.UtcNow.Ticks;
            }
            else
                MessageBox.Show("Last: " + lastBeepTick + "; This: " + ThisTick + " (" + TimeSpan.TicksPerMillisecond + ")");
#endif

#endif
        }

        public void BuzzerBeep(int freq, SoundVol vol)
        {
            CF.f_CFlow_BeepAtFreq(freq, vol);
        }


        public void MelodyRing(RingTone toneID, SoundVol vol)
        {
#if true
            CF.f_CFlow_Ring(toneID, vol);
#else
            BUZZER_SOUND SndLvl = BUZZER_SOUND.BUZZER_MIDDLE_SOUND;
            switch (vol)
            {
                case SoundVol.Low:
                    SndLvl = BUZZER_SOUND.BUZZER_LOW_SOUND;
                    break;
                case SoundVol.Med:
                    SndLvl = BUZZER_SOUND.BUZZER_MIDDLE_SOUND;
                    break;
                case SoundVol.High:
                    SndLvl = BUZZER_SOUND.BUZZER_HIGH_SOUND;
                    break;
                case SoundVol.Mute:
                    return;
            }
            PS.f_PosSp_MelodyPlay((int)toneID, 1, (uint)SndLvl);
#endif
        }
        #endregion

        #region Stop Pending Variables/Routines
        private bool InvtryStopReq = false;
        private bool TagRdStopReq = false;
        private bool TagWrStopReq = false;
        private bool TagSrchRdRateStopReq = false;
        private bool TagSrchRssiStopReq = false;
        private bool TagLockStopReq = false;
        private bool CheckAndClear(ref bool pendingFlg)
        {
            bool IsPending = pendingFlg;
            pendingFlg = false;
            return IsPending;
        }
        #endregion

        #region Health Check
       // public delegate void HealthCheckStatusNotify(HealthChkOpStatus status, String msg);
        HealthCheckStatusNotify hlthChkNotify = null;

        private void HlthChkInvOpMsgArrv(RFID_MSGID msgID)
        {
            HRESULT_RFID hRes;

            switch (msgID)
            {
                case RFID_MSGID.RFIDMW_REQUEST_TYPE_MSGID_TagInv_AddATag:
                    if (hlthChkNotify != null)
                        hlthChkNotify(HealthChkOpStatus.TagInvtryGotTag, "Success! Tag " +
                            Rfid.st_RfidMw_AddATag_PecRec.m_Pc.ToString("X4") + "-"
                            + Rfid.st_RfidMw_AddATag_PecRec.m_Epc.ToString() + " Read");
                    // Stop now
                    if (this.CancelCurrentOp() != true)
                    {
                        if (hlthChkNotify != null)
                            hlthChkNotify(HealthChkOpStatus.TagInvtryStarted, "Error: Unable to Stop Tag Inventory!!!");
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory:
                    switch (msgID)
                    {
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation:
                            hRes = Rfid.st_RfidSpReq_RadioCancelOperation.status; break;
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation:
                            hRes = Rfid.st_RfidSpReq_RadioAbortOperation.status; break;
                        case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory:
                            hRes = Rfid.st_RfidSpReq_18K6CTagInventory.status;
                            if (hRes == HRESULT_RFID.E_RFID_ERROR_OPERATION_CANCELLED)
                                hRes = HRESULT_RFID.S_OK; // override to avoid false positive
                            break;
                        default:
                            hRes = HRESULT_RFID.S_OK;
                            break;
                    }
                    // TBD: Power down the device
                    if (hlthChkNotify != null)
                    {
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            hlthChkNotify(HealthChkOpStatus.errorStopped,
                                "Tag Inventory stopped: " + hRes.ToString("F"));
                        }
                        else if (GetMacError(out macErrCode) && macErrCode != 0)
                        {
                            LogMacError();
                            hlthChkNotify(HealthChkOpStatus.errorStopped,
                               "Hardware Failure! (mac) Error code: 0x" + macErrCode.ToString("X4"));
                        }
                        else
                        {
                            hlthChkNotify(HealthChkOpStatus.TagInvtryStopped, "Health Check Finished");
                        }
                    }
                    else if (msgID == RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory)
                        // MessageBox.Show("hlthChkNotify (" + msgID.ToString() + ") : but hlthChkNotify is NULL");
                        SpMsgWnd.MsgNotify -= HlthChkInvOpMsgArrv;
                    SpMsgWnd.MsgNotifyHRes -= HlthChkInvOpMsgArrv;
                    CheckAndClear(ref InvtryStopReq); // Clear any pending Stop Request
                    break;
                default: // ignore the rest
                    break;
            }
        }

        private void HlthChkInvOpMsgArrv(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            HealthChkOpStatus Status = HealthChkOpStatus.TagInvtrySetup;
            bool RaiseEvt = false;
            String StatusMsg = null;

            switch (msgID)
            {
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        Status = HealthChkOpStatus.errorStopped;
                        StatusMsg = "SetSelectCriteria failed : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref InvtryStopReq)) // pending stop request
                    {
                        Status = HealthChkOpStatus.errorStopped;
                        StatusMsg = "User Canceled";
                        RaiseEvt = true;
                    }
                    else
                    {
                        // Continue the setup
                        hRes = QueryParmsSet(ref tagOperQryParm);
                        if (!SP.SUCCEEDED(hRes))
                        {
                            errCode = hRes;
                            // Raise errorStopped event
                            Status = HealthChkOpStatus.errorStopped;
                            StatusMsg = "SetQueryParameters failed: " + hRes.ToString("F");
                            RaiseEvt = true;
                        }
                        else
                        {
                            Status = HealthChkOpStatus.TagInvtrySetup;
                            StatusMsg = "Setting Query Parameters";
                            RaiseEvt = true;
                        }
                    }
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters:
                    if (!SP.SUCCEEDED(hRes))
                    {
                        errCode = hRes;
                        // Raise errorStopped event
                        Status = HealthChkOpStatus.errorStopped;
                        StatusMsg = "SetQueryParameters failed : " + hRes.ToString("F");
                        RaiseEvt = true;
                    }
                    else if (CheckAndClear(ref InvtryStopReq)) // pending stop request
                    {
                        Status = HealthChkOpStatus.errorStopped;
                        StatusMsg = "User Canceled";
                        RaiseEvt = true;
                    }
                    else
                    {
                        Status = HealthChkOpStatus.TagInvtryStarted;
                        StatusMsg = "Starting Tag Inventory.";
                        RaiseEvt = true;

                        // Perform Inventory
                        ClearMacError();
                        Rfid.st_RfidSpReq_18K6CTagInventory.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                        Rfid.st_RfidSpReq_18K6CTagInventory.flags = 0;
                        if (invtryPerfSelCmd)
                            Rfid.st_RfidSpReq_18K6CTagInventory.flags |= (uint)RFID_18K6CTag_FLAG.RFID_FLAG_PERFORM_SELECT;
                        Rfid.st_RfidSpReq_18K6CTagInventory.invenParms.common.tagStopCount = tagInvtryStopCnt;
                        SpMsgWnd.MsgNotify += HlthChkInvOpMsgArrv;
                        hRes = CF.f_CFlow_RfidDev_GetInventory(ref Rfid.st_RfidSpReq_18K6CTagInventory);
                        if (SP.SUCCEEDED(hRes))
                        {
                            Status = HealthChkOpStatus.TagInvtryStarted;
                            StatusMsg = "Running Inventory. Please Bring a RFID Tag in range.";
                            RaiseEvt = true;
                        }
                        else
                        {
                            errCode = hRes;
                            RaiseEvt = true;
                            Status = HealthChkOpStatus.errorStopped;
                            StatusMsg = "Tag Inventory Failed to start: " + hRes.ToString("F");
                            SpMsgWnd.MsgNotify -= HlthChkInvOpMsgArrv;
                        }
                    }
                    break;
            }

            if (RaiseEvt && hlthChkNotify != null)
                hlthChkNotify(Status, StatusMsg);
            if (RaiseEvt && Status == HealthChkOpStatus.errorStopped)
                CheckAndClear(ref InvtryStopReq);
            if (Status == HealthChkOpStatus.errorStopped)
                SpMsgWnd.MsgNotifyHRes -= HlthChkInvOpMsgArrv; // will not continue
        }

        private void HlthChkTagInvtryContStartNotify(RadioOpRes res, RadioStatus status, String msg)
        {
            bool StartInvtryOp = false;

            switch (res)
            {
                case RadioOpRes.None:
                case RadioOpRes.Warning:
                case RadioOpRes.Error:
                    // somewhere else will take care of this
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opened:
                        case RadioStatus.Configured:
                            StartInvtryOp = true;
                            break;
                    }
                    break;
            }

            HealthChkOpStatus OpStatus = HealthChkOpStatus.TagInvtrySetup;
            String StatusMsg = "Setting up Tag Inventory Parameters";
            bool RaiseEvt = false;
            if (CheckAndClear(ref InvtryStopReq))
            {
                RaiseEvt = true;
                OpStatus = HealthChkOpStatus.TagInvtryStopped;
                StatusMsg = "User Canceled";

            }
            else if (StartInvtryOp)
            {
                HRESULT_RFID hRes;
                SpMsgWnd.MsgNotifyHRes += HlthChkInvOpMsgArrv;
                SetContOperMode(out hRes);
                if (tagOperSelCrit != null && tagOperSelCrit.Length > 0)
                {
                    StatusMsg = "Setting up Tag Inventory Parameters: SELECT Criteria";
                    hRes = TagSelectCritSet(tagOperSelCrit);
                    RaiseEvt = true;
                }
                else // There is no SELECT criteria to set, go straight to QueryParameter setup
                {
                    StatusMsg = "Setting up Tag Inventory Parameters: Query Parameters";
                    hRes = QueryParmsSet(ref tagOperQryParm);
                    RaiseEvt = true;
                }
                if (!SP.SUCCEEDED(hRes))
                {
                    errCode = hRes;
                    RaiseEvt = true;
                    OpStatus = HealthChkOpStatus.errorStopped;
                    StatusMsg = "Setting up parameters failed: " + hRes.ToString("F");
                    SpMsgWnd.MsgNotifyHRes -= HlthChkInvOpMsgArrv;
                }
            }

            // raise InvtryOpStEvent if appropiate
            if ((RaiseEvt == true) && (hlthChkNotify != null))
                hlthChkNotify(OpStatus, StatusMsg);
            if ((RaiseEvt == true) && (OpStatus == HealthChkOpStatus.errorStopped))
                CheckAndClear(ref InvtryStopReq);
        }

        private void HlthChkAntPwrSet(bool succ, uint portNum, String errMsg)
        {
            if (!succ)
            {
                if (hlthChkNotify != null)
                    hlthChkNotify(HealthChkOpStatus.errorStopped, "Set Antenna Power failed: " + errMsg);
            }
            else
            {
                // Setup tagOperSelCrit and tagOperQryParm
                TagOperSetup(RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ, 7, RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S1,
                    RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A,
                    true, null, 0, RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB);
                tagInvtryStopCnt = TagStopCount1; // does it work?
                // Start Inventory
                if (TagInvtryStart(false, HlthChkTagInvtryContStartNotify) == false)
                {
                    if (hlthChkNotify != null)
                        hlthChkNotify(HealthChkOpStatus.errorStopped, "Unable to start Tag Inventory");
                }
            }
        }

        private void HlthChkFreqSet(bool succ, String errMsg)
        {
            if (!succ)
            {
                if (hlthChkNotify != null)
                    hlthChkNotify(HealthChkOpStatus.errorStopped, "Set Frequency failed: " + errMsg);
            }
            else
            {
                UserPref Pref = UserPref.GetInstance();
                if (this.AntPortCfgSetPwr(0, 300, HlthChkAntPwrSet) == false)
                {
                    if (hlthChkNotify != null)
                        hlthChkNotify(HealthChkOpStatus.AntPwr, "Unable to set Antenna Power");
                }
                else
                {
                    if (hlthChkNotify != null)
                        hlthChkNotify(HealthChkOpStatus.AntPwr, "Setting Maximum Antenna Power");
                }
            }
        }

        private void HlthChkLnkPrfNumSet(bool succ, String errMsg)
        {
            if (!succ)
            {
                if (hlthChkNotify != null)
                    hlthChkNotify(HealthChkOpStatus.errorStopped, "Set Link Profile failed: " + errMsg);
            }
            else
            {
                UserPref Pref = UserPref.GetInstance();
                if (this.CustomFreqSet(Pref.FreqProf, false, HlthChkFreqSet) == false)
                {
                    if (hlthChkNotify != null)
                        hlthChkNotify(HealthChkOpStatus.FreqChns, "Failed to set Frequency to channels to 'All' and Disabling LBT");
                }
                else
                {
                    if (hlthChkNotify != null)
                        hlthChkNotify(HealthChkOpStatus.FreqChns, "Setting Frequency to channels to 'All' and Disabling LBT");
                }
            }
        }

        public bool PerformHealthCheck(HealthCheckStatusNotify notify)
        {
            if (!RadioReady())
                return false;

            hlthChkNotify = notify;

            // Link Profile
            if (this.LinkProfNumSet(2, HlthChkLnkPrfNumSet) == true)
            {
                if (hlthChkNotify != null)
                    hlthChkNotify(HealthChkOpStatus.LnkProf, "Setting Link Profile to 2");
            }
            else
            {
                hlthChkNotify = null;
                return false;
            }

            return true;
        }

        public bool StopHealthCheck()
        {
            if (!RadioReady())
                return false;

            InvtryStopReq = true;
            return CancelCurrentOp();
        }
        #endregion
    }

 

}
