﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

/// using HRESULT_RFID    = System.Int32;
using HRESULT_RFID_STATUS = ReaderTypes.HRESULT_RFID; //System.Int32;
using RFID_RADIO_HANDLE = System.UInt32;

using CF = ClslibRfidSp.CallFlow;
using SP = ClslibRfidSp.RfidSp;
//using Reader.RFIDTypes.RFIDEnums;
using ReaderTypes;

namespace CS101ReaderLib
{
    public partial class _Datalog
    {
        private static StreamWriter logWri;
        private static StreamWriter errWri;
        public static String ErrMsg = null;
        public _Datalog() { }
        private const String ClslibLogFileName = "CsLog.txt";
        private const String ClslibErrLogFileName = "CsErrorLog.txt";
        private const String DefaultLogDir = @"\RfidLog";

        public static bool TraceIsOn
        {
            get
            {
                return logWri != null;
            }
        }

        public static bool LogInit(string logDirPath)
        {
            try
            {
                if (!Directory.Exists(logDirPath))
                    Directory.CreateDirectory(logDirPath);
                /// Open ClsLib Log File
                // use Create FileMode to 'truncate' existing file
                FileStream logFileStm = File.Open(logDirPath + ClslibLogFileName,
                    FileMode.Create, FileAccess.Write, FileShare.Read);
                logWri = new StreamWriter(logFileStm);
                logWri.AutoFlush = true;
                logWri.NewLine = "\r\n";
                logWri.WriteLine("Open log");
                /// Open W_RfidSp Log File
              //  HRESULT_RFID hRes;
               // hRes = CF.f_CFlow_StartLogTrace(logDirPath);
               // CF.f_CFlow_StartLogTrace(logDirPath);
                //if (!SP.SUCCEEDED(hRes))
                //    throw new ApplicationException("W_RfidSp Log Trace error - " + hRes.ToString());
            }
            catch (Exception e)
            {
                ErrMsg = ("Unable to start Log Trace:\r\n" + e.Message).ToString();
                if (logWri != null)
                    logWri.Close();
                logWri = null;
            }
            return (logWri != null);
        }

        private static bool ErrLogInit(string logDirPath)  /// "\\Rfid\\CsLog.txt"
        {
            try
            {
                if (!Directory.Exists(logDirPath))
                    Directory.CreateDirectory(logDirPath);
                /// Open ClsLib ErrLog File
                // use Create FileMode to 'truncate' existing file
                FileStream logFileStm = File.Open(logDirPath + Path.DirectorySeparatorChar + ClslibErrLogFileName,
                    FileMode.Create, FileAccess.Write, FileShare.Read);
                errWri = new StreamWriter(logFileStm);
                errWri.AutoFlush = true;
                errWri.NewLine = "\r\n";
                errWri.WriteLine("Open error log");
            }
            catch (Exception e)
            {
                ErrMsg = ("Unable to start Error Log Trace:\r\n" + e.Message).ToString();
                if (errWri != null)
                    errWri.Close();
                errWri = null;
            }
            return (errWri != null);
        }

        public static int LogUninit()
        {
            if (logWri != null)
            {
                try
                {
                    logWri.WriteLine("Close log");
                    logWri.Close();
                   // CF.f_CFlow_StopLogTrace();
                }
                catch (Exception e)
                {
                    ErrMsg = ("Unable to stop Log Trace:\r\n" + e.Message).ToString();
                }
                finally
                {
                    logWri = null;
                }
            }
            return 0;
        }

        public static int ErrLogUninit()
        {
            if (errWri != null)
            {
                try
                {
                    errWri.WriteLine("Close Error log");
                    errWri.Close();
                }
                catch (Exception e)
                {
                    ErrMsg = ("Unable to stop Log Trace:\r\n" + e.Message).ToString();
                }
                finally
                {
                    errWri = null;
                }
            }
            return 0;
        }

        public static void LogStr(string str)
        {
            if (logWri != null)
            {
                logWri.WriteLine("(" + DateTime.Now.ToShortDateString()
                    + " " + DateTime.Now.ToShortTimeString() + ") " + str);
            }
        }

        public static void LogStr1(string str)
        {
            if (logWri != null)
            {
                LogStr(str);
            }
        }

        // create Error Log File on demand (regardless of UserPref.TraceLog option)
        public static void LogErr(string str)
        {
            if (errWri == null)
            {
                if (ErrLogInit(DefaultLogDir) == false)
                    return;
            }
            errWri.WriteLine("(" + DateTime.Now.ToShortDateString()
                + " " + DateTime.Now.ToShortTimeString() + ") " + str);
        }
    }
}
