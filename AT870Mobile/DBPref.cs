using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.Text;using CS101UILib;

namespace OnRamp
{
    public class DBPref
    {
        public enum DBFileFmt
        {
            MSSQLCE_SDF,
            XML,
            CSV
        };

        private static AppDataFile DBPrefFile;
        private const String DBPrefFileName = "dbpref.dat";
        private String DBName;

        // Attributes
        private DBFileFmt _fmt;
        private String _remotePath;
        private String _localPath;

        // Defaults
        private readonly DBFileFmt defFmt;
        private readonly String defRemotePath;
        private readonly String defLocalPath;

        // DB keys (to be prepended with DBName when applied to avoid collision)
        private const String DBFmtKeyStr = "dbfmt";
        private const String RemPathKeyStr = "dbremotepath";
        private const String LocalPathKeyStr = "dblocalpath";

        static DBPref()
        {
            if (DBPref.DBPrefFile == null)
                DBPref.DBPrefFile = new AppDataFile(DBPrefFileName);
        }

        // provide unique name to be distinguished from other DB
        public DBPref(String name)
        {
            if (String.IsNullOrEmpty(name))
                throw new ApplicationException("Invalid Argument: empty or null");
            DBName = name;

            // setup default fvalues
            {
                defFmt = DBFileFmt.MSSQLCE_SDF;
                defLocalPath = String.Empty;
                defRemotePath = String.Empty;
            }
            // initialize attributes
            AssignDefaultValues();
            // load preferences 
            LoadPrefFromFile();
        }

        private void AssignDefaultValues()
        {
            _fmt = defFmt;
            _localPath = defLocalPath;
            _remotePath = defRemotePath;
        }

        private String makeDBKey(String attrKeyStr)
        {
            return DBName + "_" + attrKeyStr;
        }

        private void LoadPrefFromFile()
        {
            Int32 I32Val;
            String StrVal;

            if (DBPrefFile.GetInt32(makeDBKey(DBFmtKeyStr), out I32Val))
                _fmt = (DBFileFmt)I32Val;
            if (DBPrefFile.GetString(makeDBKey(RemPathKeyStr), out StrVal))
                _remotePath = StrVal;
            if (DBPrefFile.GetString(makeDBKey(LocalPathKeyStr), out StrVal))
                _localPath = StrVal;
        }

        public DBFileFmt FileFormat
        {
            set
            {
                bool Modified = (_fmt != value);
                _fmt = value;
                if (Modified)
                    DBPrefFile.AddRec(makeDBKey(DBFmtKeyStr), (Int32)value);
            }
            get
            {
                return _fmt;
            }
        }

        public String RemotePath
        {
            set
            {
                // case insensitive
                bool Modified = (String.Compare(_remotePath, value, true) != 0);
                _remotePath = value;
                if (Modified)
                    DBPrefFile.AddRec(makeDBKey(RemPathKeyStr), value);
            }
            get
            {
                return (_remotePath == null)? String.Empty : _remotePath;
            }
        }

        public String LocalPath
        {
            set
            {
                bool Modified = (String.Compare(_localPath, value, true) != 0);
                _localPath = value;
                if (Modified)
                    DBPrefFile.AddRec(makeDBKey(LocalPathKeyStr), value);
            }
            get
            {
                return (_localPath == null)? String.Empty : _localPath;
            }
        }

    }
}
