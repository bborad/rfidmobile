namespace OnRamp
{
    partial class NtwkCredDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NtwkCredDlg));
            System.Windows.Forms.Label label2;
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.UnameTxtBx = new System.Windows.Forms.TextBox();
            this.PasswdTxtBx = new System.Windows.Forms.TextBox();
            this.InfoLbl = new System.Windows.Forms.Label();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(label1, "label1");
            label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(label2, "label2");
            label2.Name = "label2";
            // 
            // UnameTxtBx
            // 
            resources.ApplyResources(this.UnameTxtBx, "UnameTxtBx");
            this.UnameTxtBx.Name = "UnameTxtBx";
            // 
            // PasswdTxtBx
            // 
            resources.ApplyResources(this.PasswdTxtBx, "PasswdTxtBx");
            this.PasswdTxtBx.Name = "PasswdTxtBx";
            // 
            // InfoLbl
            // 
            resources.ApplyResources(this.InfoLbl, "InfoLbl");
            this.InfoLbl.Name = "InfoLbl";
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // NtwkCredDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(label2);
            this.Controls.Add(label1);
            this.Controls.Add(this.InfoLbl);
            this.Controls.Add(this.PasswdTxtBx);
            this.Controls.Add(this.UnameTxtBx);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NtwkCredDlg";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox UnameTxtBx;
        private System.Windows.Forms.TextBox PasswdTxtBx;
        private System.Windows.Forms.Label InfoLbl;
        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Button CancelBttn;
    }
}