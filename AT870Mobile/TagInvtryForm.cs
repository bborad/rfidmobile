using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class TagInvtryForm : Form
    {

        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler (object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped,
        }
        #endregion

        public TagInvtryForm()
        {
            InitializeComponent();

            bool ListIsLong = false;
            this.LngLstButton.Tag = ListIsLong; // boxing : creates an object of type bool in heap

            InitStartButtonState();

            if (DutyCycleRequired())
            {
                DutyCycInit();
                DCStateInit();
            }
            else
            {
                InvtryRestartTmrInit();
            }

            // Temp Thrsh Display
            TempMonitor TempMon = TempMonitor.GetInstance();
            DisplayTempThreshold(TempMon.XcvrThrshTemp);

            // Temp Update Event Handler
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //Rdr.TempUpdateEvt += TempUpdateEvtHandler;
            Rdr.RegisterTempUpdateEvt(TempUpdateEvtHandler);
        }

        static TagInvtryForm()
        {
            TagList = new TagRangeList();
        }

        #region Duty Cycle State
        private DutyCycleTmr dcTmr;
        private OpState dcState;

        private void DCStateInit()
        {
            dcState = OpState.Stopped;
        }

        private void DCStateSet(OpState newState)
        {
            dcState = newState;

            switch (newState)
            {
                case OpState.Started:
                    // start On Timer
                    dcTmr.OnCycStart(DutyOnCycExpired);
                    break;
                case OpState.Stopped:
                    // Not every time an Off timer needs to be started
                    // do nothing here
                    break;
            }
        }

        private OpState DCStateGet()
        {
            return dcState;
        }
        #endregion

        #region StartButton State
        private void InitStartButtonState()
        {
            OpState State = OpState.Stopped;
            this.StartButton.Tag = State; // boxing

        }

        private OpState StartButtonState()
        {
            OpState state = OpState.Stopped;
            if (StartButton.Tag is OpState)
            {
                state = (OpState)StartButton.Tag;
            }
            else
            {
                throw new ApplicationException("StartButton Tag is not OpState");
            }
            return state;
        }

        // Affect StartButton, ClrButton
        private void SetStartButtonState(OpState newState)
        {
            OpState CurState = StartButtonState();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagInvtryForm));

            switch (newState)
            {
                case OpState.Starting:
                    // disable Start button
                    StartButton.Enabled = false;
                    ClrButton.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    StartButton.Text = "Stop";
                    StartButton.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    // RGB component reversed
                    int BlnkClr = Color.White.B << 16 | Color.White.G << 8 | Color.White.R;
                    PosSp.f_PosSp_LedBlink((uint)BlnkClr, 1000, 500);
                    break;
                case OpState.Stopping:
                    StartButton.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    PosSp.f_PosSp_LedSetOff();
                    break;
                case OpState.Stopped:
                    StartButton.Text = resources.GetString("StartButton.Text");
                    StartButton.Enabled = true;
                    ClrButton.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    PosSp.f_PosSp_LedSetOff();
                    OverTempAlertStop(); // Cancel Existing Flash Alerts
                    TempMonitor.GetInstance().PeriodicTempGetStop(); // user-stop case/catch-all
                    break;
            }
            StartButton.Tag = newState;
        }


        #endregion

        #region EPCListView+TagCnt Management
        private void RefreshTagCntLabel()
        {
            TagCntLabel.Text = EPCListV.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLabel.Refresh();
        }

        private void AddEPCToListV(UInt16 PC, UINT96_T EPC)
        {
            // Assuming that the Tag of this EPC is not already in list
            int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
            ListViewItem item = new ListViewItem(new String[] {
                 (CurNumRows+1).ToString(), EPC.ToString() });
            // Insert new item to the top of the list for easy viewing
            EPCListV.Items.Insert(0, item);
            item.Tag = PC;
            EPCListV.Refresh();
            RefreshTagCntLabel();
        }

        private void AddEPCToListV(UInt16[] PCArr, UINT96_T[] EPCArr)
        {
            EPCListV.SuspendLayout();
            int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
            for (int i = 0; i < EPCArr.Length; i++, CurNumRows++)
            {
                ListViewItem item = new ListViewItem(new String[] {
                     (CurNumRows+1).ToString(), EPCArr[i].ToString() });
                EPCListV.Items.Add(item);
                item.Tag = PCArr[i];
            }
            EPCListV.ResumeLayout();
            RefreshTagCntLabel();
        }

        private void LoadTagListToListV()
        {
            EPCListV.SuspendLayout();
            // Load TagList to ListView
            foreach (TagListItem Tag in TagList)
            {
                AddEPCToListV(Tag.PC, Tag.EPC);
            }
            EPCListV.ResumeLayout();
        }

        private void ClearEPCListV()
        {
            TagList.Clear();
            EPCListV.Items.Clear();
            RefreshTagCntLabel();
        }

        private void OnSelIdxChanged(object sender, EventArgs e)
        {
            if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
            {
                // Selected
                ListViewItem SelRow = EPCListV.Items[EPCListV.SelectedIndices[0]];
                if (SelRow.Tag is UInt16)
                {
                    UInt16 PC = (UInt16)SelRow.Tag;
                    TagIDLbl.Text = PC.ToString("X4") + "-" + SelRow.SubItems[1].Text; // Column 2 is EPC
                }
                else
                {
                    TagIDLbl.Text = "XXXX" + "-" + SelRow.SubItems[1].Text; // Column 2 is EPC
                }
            }
            else
            {
                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(this.GetType());

                TagIDLbl.Text = resources.GetString("TagIDLbl.Text"); // display design-time string
            }
        }

        private void OnItemActivated(object sender, EventArgs e)
        {
            if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
            {
                // Selected
                ListViewItem SelRow = EPCListV.Items[EPCListV.SelectedIndices[0]];
                String TagIDStr = String.Empty;
                if (SelRow.Tag is UInt16)
                {
                    UInt16 PC = (UInt16)SelRow.Tag;
                    TagIDStr = PC.ToString("X4") + "-" + SelRow.SubItems[1].Text; // Column 2 is EPC
                }
                else
                {
                    TagIDStr = "XXXX" + "-" + SelRow.SubItems[1].Text; // Column 2 is EPC
                }
                CS101UILib.TransientMsgDlg Dlg = new CS101UILib.TransientMsgDlg(3);
                Dlg.SetDpyMsg(TagIDStr, "Tag Information");
                Dlg.Show();
            }
        }
        #endregion

        #region common Tag Event Handlers
        //bool firstTag = true;
        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            // Add new TagID to list (or update occurrence)
            if (e.Cnt != 0)
            {
#if false
                AddEPCToListV(e.PC, e.EPC);
#else
                if (TagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI, e.UpdateTm)))
                {
                    // New Tag
                    AddEPCToListV(e.PC, e.EPC);
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                }
#endif
            }
            else if (e.EPCArr != null)
                AddEPCToListV(e.PCArr, e.EPCArr);
            
#if false
            if (firstTag)
            {
                RFIDRdr.GetInstance().TagInvtryStop();
                firstTag = false;
            }
#endif
        }
        #endregion

        #region Non-DC Tag Event Handlers
        private int invtryCycRan = 0;
        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState state = StartButtonState();
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    Program.RefreshStatusLabel(this, "Running...");
                    SetStartButtonState(OpState.Started);
                    // maximize the list
                    if (this.LngLstButton.Tag is Boolean
                        && (!(Boolean)this.LngLstButton.Tag))
                        OnLongListButtonClicked(null, null);
                    if (DutyCycleRequired())
                    {
                        DCStateSet(OpState.Started);
                        // Replace Inventory Status Handler until user-request stop
                        //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //rdr.InventoryOpStEvent += DCInvtryOpEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        rdr.RegisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                    }
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                   // rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                   rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                 
                    bool ResetToStopStatus = true;
                    switch (e.status)
                    {
                        case InvtryOpStatus.stopped:
                            if (StartButtonState() != OpState.Stopping)
                            {
                                // involuntary stop, re-start again (needs further investigation)
                                Datalog.LogErr("Tag Inventory stop for no reason.");
                                TempMonitor TempMon = TempMonitor.GetInstance();
                                Datalog.LogErr("Temp: " + TempMon.LastRecAmbTemp + ", "
                                    + TempMon.LastRecXcvrTemp + ", " + TempMon.LastRecPATemp);
                                Program.RefreshStatusLabel(this, "Restarting in 2 secs...");
                                InvtryRestartTmrStart(2);
                                ResetToStopStatus = false;
                            }
                            break;
                        case InvtryOpStatus.errorStopped:
                            if (rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                                RestartRfidDevice();
                            else
                                Program.ShowError("Tag Inventory Stopped with Error: " + e.msg);
                            break;
                        case InvtryOpStatus.macErrorStopped:
                           // ushort MacErrCode = 0;
                            Reader.macerr = 0;
                            if (rdr.GetMacError() && Reader.macerr != 0)
                            {
                                if (rdr.MacErrorIsOverheat())
                                {
                                    OverTempAlertStart();
                                    Program.RefreshStatusLabel(this, "Over Temp! Restarting in 5 min...");
                                    OverTempLogMessage();
                                    InvtryRestartTmrStart(5 * 60);
                                    TempMonitor.GetInstance().PeriodicTempGetStart(3); // until Restart Timer Expired
                                    ResetToStopStatus = false;
                                }
                                else if (!rdr.MacErrorIsFatal())
                                {
                                    if (StartButtonState() != OpState.Stopping)
                                    {
#if BURN_IN
                                        // Attempt to re-start inventory after fatal error
                                        Program.ShowWarning("Tag Inventory stopped by error: " + e.msg);
                                        Program.RefreshStatusLabel(this, "Restarting in 3 secs...");
                                        InvtryRestartTmrStart(3);
                                        ResetToStopStatus = false;
#else
                                        Program.ShowError("Tag Inventory stopped. Hardware (mac) error code: " + Reader.macerr.ToString("F"));
#endif
                                    }
                                    // Do not display error message when mac error is not direct cause
                                    // of this stop. (user-stop)
                                }
                                else // Fatal Mac Error
                                {
                                    Program.ShowError(e.msg);
                                }
                            }
                            else
                                Program.ShowError("Unknown HW error. Abort");
                            break;
                    }
                    if (ResetToStopStatus)
                    {
                        // restore StartButton
                        SetStartButtonState(OpState.Stopped);
                        // Display total Time elapsed
                        PerfCounterStop();
                        Program.RefreshStatusLabel(this, "Done " + PerfCounterGetSecs() + "s taken");
                    }
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (state)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
                case InvtryOpStatus.intervalTimeRpt:
                    //Program.RefreshStatusLabel(this, "Running for " + e.ElapsedTime + " ms");
                    break;
                case InvtryOpStatus.cycBegin:
                    invtryCycRan++;
                    Program.RefreshStatusLabel(this, "InvCyc " + invtryCycRan + " Begins");
                    break;
                case InvtryOpStatus.cycEnd:
                    Program.RefreshStatusLabel(this, "InvCyc " + invtryCycRan + " Ends");
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Button event handlers
      
        private void OnLongListButtonClicked(object sender, EventArgs e)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagInvtryForm));
            System.Drawing.Size size;
            // 'Toggle':
            // Hide/Show Panel; Short/Long List; Button Label
            if (this.LngLstButton.Tag is Boolean)
            {
                Boolean ListIsLong = (Boolean)this.LngLstButton.Tag;
                size = (System.Drawing.Size)resources.GetObject("EPCListV.Size");
                this.EPCListV.BeginUpdate();
                if (ListIsLong.Equals((bool)true))
                {
                    // restore to original size
                    this.LngLstButton.Text = resources.GetString("LngLstButton.Text");
                    this.EPCListV.Size = size;
                    BttnHlfPanel.Visible = true;   
                }
                else
                {
                    BttnHlfPanel.Visible = false;
                    // TBD: find a cleaner method of storing these alternative settings.
                    this.LngLstButton.Text = "Short";
                    size.Height = StartButton.Location.Y - EPCListV.Location.Y;
                    this.EPCListV.Size = size;
                }
                this.EPCListV.EndUpdate();
                ListIsLong = !ListIsLong;
                this.LngLstButton.Tag = ListIsLong;
            }
        }

        private void OnStartButtonClicked(object sender, EventArgs e)
        {
            Program.RefreshStatusLabel(this, null);
            OpState state = StartButtonState();

            // start or stop operation
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            switch (state)
            {         
                case OpState.Started:
                    // dispatch stop request//TestStop();
                    // disable button until actually stopped
                    SetStartButtonState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    bool IssueStopCmd = false;
                    if (DutyCycleRequired())
                    {
                        OpState DCState = DCStateGet();
                        switch (DCState)
                        {
                            case OpState.Stopped: // In Off-Cycle
                                dcTmr.OffCycCancel();
                                SetStartButtonState(OpState.Stopped);
                                Program.RefreshStatusLabel(this, "Stopped");
                                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                                break;
                            case OpState.Started: // In On-Cycle
                                dcTmr.OnCycCancel();
                                // Replace handler with InvtryOpEvtHandler from this point on
                                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                //rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                                IssueStopCmd = true;
                                break;
                            case OpState.Starting:
                            case OpState.Stopping:
                                // cannot do anything, wait for DCState machine to check
                                // the StartButtonState() for hint
                                break;
                        }
                    }
                    else if (IntryRestartTmrRunning())
                    {
                        // AutoRestart Timer 
                        InvtryRestartTmrStop();
                        SetStartButtonState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Stopped");
                        IssueStopCmd = false;
                    }
                    else
                        IssueStopCmd = true;
                    if (IssueStopCmd)
                    {
                        if (!rdr.TagInventoryStop())
                        {
                            MessageBox.Show("Tag Inventory Stop Failed", "Error");
                            SetStartButtonState(OpState.Stopped);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        }
                    }
                    break;
                case OpState.Stopped:
                    invtryCycRan = 0;
                    PerfCounterStart();
                    // get event handler ready
                    //rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                    //rdr.DscvrTagEvent += new EventHandler<DscvrTagEventArgs>(DscvrTagEvtHandler);
                    rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                    rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
#if CLEAR_INVTRYLIST_DISP_ON_START
                    // Clear List
                    ClearEPCListV();
#endif
                    // dispatch start request //TestStart();
                    // disable button until actually started
                    SetStartButtonState(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting...");
                    if (!rdr.SetRepeatedTagObsrvMode(true))
                        // Continue despite error
                        MessageBox.Show("Disable Repeated Tag Observation mode failed", "Error");
                    //UserPref Pref = UserPref.GetInstance();
                    //Byte[] Mask; uint MaskOffset;
                    //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                    LogOperStartTemp();
                    if (! rdr.TagInventoryStart(5))
                    {
                        MessageBox.Show("Tag Inventory Start Failed", "Error");
                        // rewind previous setup
                        SetStartButtonState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        PerfCounterStop();
                    }
                    break;
                case OpState.Starting:
                    Program.RefreshStatusLabel(this, "Stopping...");
                    SetStartButtonState(OpState.Stopping);
                    if (!rdr.TagInventoryStop())
                    {
                        // Restore
                        Program.RefreshStatusLabel(this, "Starting...");
                        SetStartButtonState(OpState.Starting);
                        Program.ShowWarning("Tag Inventory Stop failed during Starting phase, please try again");
                    }
                    break;
                default:
                    // unexpected
                    break;
            } // switch
        }

        private void OnAbortButtonClicked(object sender, EventArgs e)
        {
            OpState state = StartButtonState();

            // Only perform abort when operation is running
            if (state == OpState.Started)
            {
                SetStartButtonState(OpState.Stopping);
                Program.RefreshStatusLabel(this, "Aborting...");
                //RFIDRdr rdr = RFIDRdr.GetInstance();
                Reader rdr = ReaderFactory.GetReader();
                if (!rdr.TagInventoryAbort())
                {
                    MessageBox.Show("Tag Inventory Abort Failed", "Error");
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                }

            }
        }

        private void OnClrButtonClicked(object sender, EventArgs e)
        {
            // Clear UI
            ClearEPCListV();
            // Clear List in Mw
           // RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            if (!rdr.TagInvtryClr())
            {
                MessageBox.Show("Clear Tag Inventory Failed.", "Error");
            }
        }

        #endregion

        #region F11/F4/F5 HotKey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11:
                case eVKey.VK_F19: 
                    if (down)
                    {
                        // fake 'Start' key press if not already running
                        if (StartButtonState() == OpState.Stopped)
                        {
                            OnStartButtonClicked(this, null);
                        }
                    }
                    else // up
                    {
                        if (StartButtonState() == OpState.Started
                            || StartButtonState() == OpState.Starting)
                        {
                            // Stop!
                            OnStartButtonClicked(this, null);
                        }
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (down)
                    {
                        if (StartButtonState() == OpState.Stopped)
                        {
                            AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                        }
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && StartButtonState() == OpState.Stopped)
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }
        #endregion

        #region Form Routines

        private void OnFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);

            // Temp Update Event Handler
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //Rdr.TempUpdateEvt -= TempUpdateEvtHandler;
            Rdr.UnregisterTempUpdateEvt(TempUpdateEvtHandler);
        }

        private void OnFormLoad(object sender, EventArgs e)
        {        
            LoadTagListToListV();

            // Set up to receive HotKey event
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);

#if false // re-enable this code when KeyXXX events are required for Form
            // Form receive key first
            this.KeyPreview = true;
#endif
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = false;

            if (StartButtonState() != OpState.Stopped)
            {
                Program.ShowWarning("Tag Inventory operation still running!");

                e.Cancel = true;
            }
        }

        #endregion

        #region Tag Perf. Counter
        private long startTick = 0;
        private long endTick = 0;

        private void PerfCounterStart()
        {
            startTick = DateTime.Now.Ticks;
            endTick = startTick;
        }

        private void PerfCounterStop()
        {
            endTick = DateTime.Now.Ticks;
        }

        private long PerfCounterGetSecs()
        {
            return (endTick - startTick) / TimeSpan.TicksPerSecond;
        }
#if false
        private int AddTagEvtCnt = 0;
        private void AddTagEvtCntReset()
        {
            AddTagEvtCnt = 0;
            TagHitCntLbl.Text = AddTagEvtCnt.ToString();
            TagHitCntLbl.Update();
        }

        private void AddTagEvtCntIncr()
        {
            AddTagEvtCnt++;
            TagHitCntLbl.Text = AddTagEvtCnt.ToString();
            TagHitCntLbl.Update();

        }
#endif
        #endregion
       
         #region Duty Cycle Timer
        private void DCInvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:// from the 2nd on-cycle starts
                    if (StartButtonState() == OpState.Stopping)
                    {
                        // User Requested stop during our DC Starting Phase
                        dcTmr.OnCycCancel(); // if already set up
                        // User Stop Request Pending
                        DCStateSet(OpState.Stopping);
                        // Replace InvtryOpState handler with InvtryOpEvtHandler
                        //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                        //rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                        rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                        if (!rdr.TagInventoryStop())
                        {
                            MessageBox.Show("Tag Inventory Stop Failed", "Error");
                            DCStateSet(OpState.Stopped);
                            SetStartButtonState(OpState.Stopped);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        }
                    }
                    else
                    {
                        DCStateSet(OpState.Started);
                        Program.RefreshStatusLabel(this, "Running(On-Cycle)");
                    }
                    break;
                case InvtryOpStatus.error:
                    switch (DCStateGet())
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    DCStateSet(OpState.Stopped);
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
                case InvtryOpStatus.stopped:
                    if (DCStateGet() != OpState.Stopping) // involuntary stop
                    {
                        // do nothing, wait until On-Cycle ends
                        Program.RefreshStatusLabel(this, "Stopped!"); 
                        DCStateSet(OpState.Stopped);
                    } 
                    else // On-Cycle Expired Stopping  and/or User Stopping
                    {
                        DCStateSet(OpState.Stopped);
                        dcTmr.OnCycCancel();
                        if (StartButtonState() == OpState.Stopping) // user-stopping
                        {
                            // User-Requested Stop, do not restart
                            Program.RefreshStatusLabel(this, "Stopped");
                            SetStartButtonState(OpState.Stopped);
                            //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        }
                        else // on-cycle expired stopping
                        {
                            OverTempAlertStop(); // Cancel any existing alert
                            Program.RefreshStatusLabel(this, "Paused(Off Cycle)");
                            // stop before start again to make sure we got the interval correct
                            TempMonitor.GetInstance().PeriodicTempGetStop(); // started from OverHeat
                            TempMonitor.GetInstance().PeriodicTempGetStart(3); // until offCycEnds
                            dcTmr.OffCycStart(DutyOffCycExpired);
                        }
                    }
                    break;
                case InvtryOpStatus.errorStopped:
                    DCStateSet(OpState.Stopped);
                    dcTmr.OnCycCancel();
                    //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                    //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                    rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    if (rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                        RestartRfidDevice();
                    else
                    {
#if BURN_IN
                        Program.ShowWarning("Tag Inventory Stopped with Error: " + e.msg);
#else
                        MessageBox.Show(e.msg, "Tag Inventory Stopped with Error");
#endif
                    }
                    break;
                case InvtryOpStatus.macErrorStopped:
                   // ushort MacErrCode = 0;
                    DCStateSet(OpState.Stopped);
                    if (rdr.GetMacError() && Reader.macerr != 0)
                    {
                        if (StartButtonState() == OpState.Stopping)
                        {
                            dcTmr.OnCycCancel();
                            //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            if (rdr.MacErrorIsFatal())
                            {
                                Program.ShowError(e.msg);
                            }
                            SetStartButtonState(OpState.Stopped);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                        }
                        else
                        {
                            if (rdr.MacErrorIsOverheat())
                            {
                                OverTempAlertStart();
                                Program.RefreshStatusLabel(this, "Over Temp!");
                                OverTempLogMessage();
                                TempMonitor.GetInstance().PeriodicTempGetStart(3); // until On-Cycle ends
                            }
                            else if (!rdr.MacErrorIsFatal())
                            {
                                Program.ShowWarning(e.msg); // until On-Cycle ends
                            }
                            else // fatal mac error
                            {
                                dcTmr.OnCycCancel();
                                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                                //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                                rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                                SetStartButtonState(OpState.Stopped);
                                Program.RefreshStatusLabel(this, "Error Stopped");
                                Program.ShowError(e.msg);
                            }
                        }
                    }
                    else
                    {
                        dcTmr.OnCycCancel();
                        //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                        //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                        rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        SetStartButtonState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        Program.ShowError("Unknown HW error. Abort");
                    }
                    break;
                case InvtryOpStatus.intervalTimeRpt:
                    UserPref Pref = UserPref.GetInstance();
                    switch (DCStateGet())
                    {
                        case OpState.Starting:
                        case OpState.Stopping:
                        case OpState.Stopped:
                            // ignore this then
                            break;
                        case OpState.Started:
                            if (e.ElapsedTime >( Pref.DCOnDuration * 60 * 1000))
                            {
                                // this comes earlier than the Tick?
                                dcTmr.OnCycCancel();
                                // Go to Off-Cycle
                                DutyOffCycStart();
                                //MessageBox.Show("On Cycle Expired (MAC timer)", "DEBUG ONLY(To-be-Removed)");
                            }
                            break;
                    }
                    break;
            }
        }

        private bool DutyCycleRequired()
        {
            UserPref Pref = UserPref.GetInstance();

            return  ((Pref.DCOnDuration != 0) && (Pref.DCOffDuration != 0));
        }

        private void DutyCycInit()
        {
            UserPref Pref = UserPref.GetInstance();
            this.dcTmr = new DutyCycleTmr(this.DutyCycTmr, (int)Pref.DCOnDuration, (int)Pref.DCOffDuration);
        }

        private void DutyOffCycStart()
        {
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            if (DCStateGet() == OpState.Started)
            {
                DCStateSet(OpState.Stopping);
                Program.RefreshStatusLabel(this, "Going into Off-Cycle");
                if (!rdr.TagInventoryStop())
                {
                    MessageBox.Show("Tag Inventory Stop Failed", "Error");
                    DCStateSet(OpState.Stopped);
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                    //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                    rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                }
            }
            else if (DCStateGet() == OpState.Stopped)
            {
                DCStateSet(OpState.Stopping); // fake
                Program.RefreshStatusLabel(this, "Going into Off-Cycle");
                DCInvtryOpEvtHandler(this, new InvtryOpEventArgs(InvtryOpStatus.stopped));
            }
        }

        private void DutyOnCycStart()
        {
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            Reader rdr = ReaderFactory.GetReader();
            DCStateSet(OpState.Starting);
            Program.RefreshStatusLabel(this, "Going into On-Cycle...");
            //UserPref Pref = UserPref.GetInstance();

            //Byte[] Mask; uint MaskOffset;
            //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
            LogOperStartTemp();
            if (!rdr.TagInventoryStart(5))
            {
                MessageBox.Show("Tag Inventory Start Failed", "Error");
                // rewind previous setup
                SetStartButtonState(OpState.Stopped);
                Program.RefreshStatusLabel(this, "Error Stopped");
                //rdr.InventoryOpStEvent -= DCInvtryOpEvtHandler;
                //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                rdr.UnregisterInventoryOpStEvent(DCInvtryOpEvtHandler);
                rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
            }
        }


        private void DutyOnCycExpired(Timer tmr)
        {
            OpState state = StartButtonState();
            switch (state)
            {
                case OpState.Started:
                    DutyOffCycStart();
                    break;
                case OpState.Stopping:
                case OpState.Stopped:
                case OpState.Starting:
                    throw new ApplicationException ("Unexpected DCState: " + state.ToString() 
                        + "in DutyOnCycExpired()");
            }
        }

        
        private void DutyOffCycExpired(Timer tmr)
        {
            TempMonitor.GetInstance().PeriodicTempGetStop(); // started from OffCycle
            switch (DCStateGet())
            {
                case OpState.Stopping:
                case OpState.Started:
                case OpState.Starting:
                    throw new ApplicationException ("Unexpected DCState: " + DCStateGet().ToString() 
                    + "in DutyOffCycExpired()");
                case OpState.Stopped:
                    DutyOnCycStart();
                    break;
            }
        }

        
        #endregion

        #region Temperature Routines
        bool OverTempAlertStarted = false;

        private void OverTempAlertStart()
        {
            TempMonitor TempMon = TempMonitor.GetInstance();
            if (!TempMon.OverHeatFlashAlertStart(this, EPCListV, TempPanel))
            {
                MessageBox.Show("Temp Alert Failed to Start");
            }
            else
                OverTempAlertStarted = true;

        }

        private void OverTempAlertStop()
        {
            if (OverTempAlertStarted)
            {
                TempMonitor TempMon = TempMonitor.GetInstance();
                TempMon.OverHeatFlashAlertStop();
            }
        }
        // Note: this not only serves as handling temperature data
        // coming from inventory. It also receives updates from
        // RFIDRdr.GetCurrTemp(...)
        private void TempUpdateEvtHandler(object sender, TempUpdateEventArgs e)
        {
            DisplayCurrTemp(e.Amb, e.Xcvr, e.PA);
        }

        private void DisplayCurrTemp(UInt16 amb, UInt16 xcvr, UInt16 pa)
        {
            AmbTempLbl.Text = amb.ToString();
            XcvrTempLbl.Text = xcvr.ToString();
            PATempLbl.Text = pa.ToString();
        }

        private void DisplayTempThreshold(UInt16 xcvr)
        {
            LimitLbl.Text = xcvr.ToString();
        }

        private void OverTempLogMessage()
        {
            Datalog.LogErr(DateTime.Now.ToShortTimeString() + ":" + " Tag Inventory OverTemp at "
                + TempMonitor.GetInstance().LastRecXcvrTemp + "(Xcvr)"
                + " " + TempMonitor.GetInstance().LastRecPATemp + "(PA)");
        }

        #endregion

        #region InvtryRestart Timer (For continous inventory operation without DC)
        private System.Windows.Forms.Timer invtryRestartTmr;

        private void InvtryRestartTmrInit()
        {
            if (invtryRestartTmr == null)
            {
                invtryRestartTmr = new System.Windows.Forms.Timer();
                invtryRestartTmr.Tick += OnInvtryRestartTmrTick;
            }
        }

        private bool IntryRestartTmrRunning()
        {
            return (invtryRestartTmr != null) && (invtryRestartTmr.Enabled);
        }

        private void InvtryRestartTmrStop()
        {
            invtryRestartTmr.Enabled = false;
        }

        // return false if Timer is already running
        private bool InvtryRestartTmrStart(int secs)
        {
            bool Succ = false;

            if (invtryRestartTmr.Enabled == false)
            {
                invtryRestartTmr.Interval = secs * 1000;
                invtryRestartTmr.Enabled = true;
                Succ = true;
            }
            else
                Succ = false;

            return Succ;
        }

        private void OnInvtryRestartTmrTick(object sender, EventArgs e)
        {
            InvtryRestartTmrStop();// one shot
            TempMonitor.GetInstance().PeriodicTempGetStop(); // Started from RestartTmr

            switch (StartButtonState())
            {
                case OpState.Stopping:
                    SetStartButtonState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Stopped");
                    break;
                case OpState.Started:
                    //RFIDRdr Rdr = RFIDRdr.GetInstance();
                    Reader Rdr = ReaderFactory.GetReader();
                    //ushort amb, xcvr, pamp;
                    // Although the temperature returns here synchronously as
                    // 'out' parameter. We simply rely on the RFIDRdr.TempUpdateEvt
                    // handler to display the update. (lazy coding)
                    Rdr.GetCurrTemp(); // Reload Temp
                    SetStartButtonState(OpState.Stopped); // this allows the start button click handler to perform start
                    OnStartButtonClicked(this, null);
                    break;
                default:
                    MessageBox.Show("Unable to restart, Opstate is " + StartButtonState().ToString("F"));
                    break;
            }
        }
        #endregion


        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        static void LogOperStartTemp()
        {
            TempMonitor TempMon = TempMonitor.GetInstance();

            Datalog.LogStr(String.Format("Tag Inventory Starts.  Amb Temp: {0}; Xcvr Temp: {1}; PA Temp: {2}", TempMon.LastRecAmbTemp, TempMon.LastRecXcvrTemp, TempMon.LastRecPATemp));
        }


        #region Test Only (not in active use)
        private void OnFormKeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'a' || e.KeyChar == 'A')
            {
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                if (Rdr.CancelRunningOp() == false)
                {
                    Program.ShowError("Rdr Failed to Cancel Running Op");
                }
            }
        }
        #endregion

        #region Tag List (static public)

        public static TagRangeList TagList = null;

        #endregion

    }
    

}
