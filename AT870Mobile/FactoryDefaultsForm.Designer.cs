namespace OnRamp
{
    partial class FactoryDefaultsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FactoryDefaultsForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.RFIDCfgChkBx = new System.Windows.Forms.CheckBox();
            this.SysCfgChkBx = new System.Windows.Forms.CheckBox();
            this.DataDirChkBx = new System.Windows.Forms.CheckBox();
            this.RestoreBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RFIDCfgChkBx
            // 
            this.RFIDCfgChkBx.Checked = true;
            this.RFIDCfgChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.RFIDCfgChkBx, "RFIDCfgChkBx");
            this.RFIDCfgChkBx.Name = "RFIDCfgChkBx";
            // 
            // SysCfgChkBx
            // 
            this.SysCfgChkBx.Checked = true;
            this.SysCfgChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.SysCfgChkBx, "SysCfgChkBx");
            this.SysCfgChkBx.Name = "SysCfgChkBx";
            // 
            // DataDirChkBx
            // 
            resources.ApplyResources(this.DataDirChkBx, "DataDirChkBx");
            this.DataDirChkBx.Name = "DataDirChkBx";
            // 
            // RestoreBttn
            // 
            this.RestoreBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.RestoreBttn, "RestoreBttn");
            this.RestoreBttn.Name = "RestoreBttn";
            this.RestoreBttn.Click += new System.EventHandler(this.OnRestoreBttnClicked);
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            this.CancelBttn.Click += new System.EventHandler(this.OnCancelBttnClicked);
            // 
            // FactoryDefaultsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.RestoreBttn);
            this.Controls.Add(this.DataDirChkBx);
            this.Controls.Add(this.SysCfgChkBx);
            this.Controls.Add(this.RFIDCfgChkBx);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FactoryDefaultsForm";
            this.Load += new System.EventHandler(this.FactoryDefaultsForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox RFIDCfgChkBx;
        private System.Windows.Forms.CheckBox SysCfgChkBx;
        private System.Windows.Forms.CheckBox DataDirChkBx;
        private System.Windows.Forms.Button RestoreBttn;
        private System.Windows.Forms.Button CancelBttn;
    }
}