namespace OnRamp
{
    partial class TagGrpInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagGrpInputForm));
            this.label1 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.tagGrpMaskInput = new OnRamp.EPCMaskInput();
            this.PopSzTxtBx = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.DupWrFilterOnChkBx = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // tagGrpMaskInput
            // 
            resources.ApplyResources(this.tagGrpMaskInput, "tagGrpMaskInput");
            this.tagGrpMaskInput.Name = "tagGrpMaskInput";
            // 
            // PopSzTxtBx
            // 
            resources.ApplyResources(this.PopSzTxtBx, "PopSzTxtBx");
            this.PopSzTxtBx.Name = "PopSzTxtBx";
            this.PopSzTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPopSzTxtBxKeyPressed);
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // DupWrFilterOnChkBx
            // 
            resources.ApplyResources(this.DupWrFilterOnChkBx, "DupWrFilterOnChkBx");
            this.DupWrFilterOnChkBx.Name = "DupWrFilterOnChkBx";
            // 
            // TagGrpInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.DupWrFilterOnChkBx);
            this.Controls.Add(this.PopSzTxtBx);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.tagGrpMaskInput);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagGrpInputForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private EPCMaskInput tagGrpMaskInput;
        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PopSzTxtBx;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.CheckBox DupWrFilterOnChkBx;
    }
}