namespace OnRamp
{
    partial class frmPerformTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPerformTask));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkComplete = new System.Windows.Forms.CheckBox();
            this.lstTask = new System.Windows.Forms.ListView();
            this.Mark = new System.Windows.Forms.ColumnHeader();
            this.Tasks = new System.Windows.Forms.ColumnHeader();
            this.DueDate = new System.Windows.Forms.ColumnHeader();
            this.txtComments = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboTemplate = new System.Windows.Forms.ComboBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.B0Label = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.chkComplete);
            this.panel1.Controls.Add(this.lstTask);
            this.panel1.Controls.Add(this.txtComments);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cboTemplate);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.btnCancel);
            this.panel1.Controls.Add(this.txtName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.B0Label);
            this.panel1.Controls.Add(this.label3);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // chkComplete
            // 
            resources.ApplyResources(this.chkComplete, "chkComplete");
            this.chkComplete.Name = "chkComplete";
            this.chkComplete.CheckStateChanged += new System.EventHandler(this.chkComplete_CheckStateChanged);
            // 
            // lstTask
            // 
            this.lstTask.Columns.Add(this.Mark);
            this.lstTask.Columns.Add(this.Tasks);
            this.lstTask.Columns.Add(this.DueDate);
            this.lstTask.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            resources.ApplyResources(this.lstTask, "lstTask");
            this.lstTask.Name = "lstTask";
            this.lstTask.View = System.Windows.Forms.View.Details;
            // 
            // Mark
            // 
            resources.ApplyResources(this.Mark, "Mark");
            // 
            // Tasks
            // 
            resources.ApplyResources(this.Tasks, "Tasks");
            // 
            // DueDate
            // 
            resources.ApplyResources(this.DueDate, "DueDate");
            // 
            // txtComments
            // 
            resources.ApplyResources(this.txtComments, "txtComments");
            this.txtComments.Name = "txtComments";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // cboTemplate
            // 
            resources.ApplyResources(this.cboTemplate, "cboTemplate");
            this.cboTemplate.Name = "cboTemplate";
            this.cboTemplate.SelectedIndexChanged += new System.EventHandler(this.cboTemplate_SelectedIndexChanged);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            this.txtName.ReadOnly = true;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // B0Label
            // 
            resources.ApplyResources(this.B0Label, "B0Label");
            this.B0Label.Name = "B0Label";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // frmPerformTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPerformTask";
            this.Load += new System.EventHandler(this.frmPerformTask_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label B0Label;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cboTemplate;
        private System.Windows.Forms.TextBox txtComments;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lstTask;
        private System.Windows.Forms.ColumnHeader Mark;
        private System.Windows.Forms.ColumnHeader Tasks;
        private System.Windows.Forms.CheckBox chkComplete;
        private System.Windows.Forms.ColumnHeader DueDate;

    }
}