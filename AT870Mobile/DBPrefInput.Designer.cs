namespace OnRamp
{
    partial class DBPrefInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBPrefInput));
            this.LocalBrwseBttn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.CSVBttn = new System.Windows.Forms.RadioButton();
            this.XMLBttn = new System.Windows.Forms.RadioButton();
            this.SDFBttn = new System.Windows.Forms.RadioButton();
            this.LclPathTxtBx = new System.Windows.Forms.TextBox();
            this.RmtURITxtBx = new System.Windows.Forms.TextBox();
            this.RemFileSetupBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LocalBrwseBttn
            // 
            resources.ApplyResources(this.LocalBrwseBttn, "LocalBrwseBttn");
            this.LocalBrwseBttn.Name = "LocalBrwseBttn";
            this.LocalBrwseBttn.Click += new System.EventHandler(this.OnBrwseBttnClicked);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // CSVBttn
            // 
            resources.ApplyResources(this.CSVBttn, "CSVBttn");
            this.CSVBttn.Name = "CSVBttn";
            this.CSVBttn.TabStop = false;
            // 
            // XMLBttn
            // 
            resources.ApplyResources(this.XMLBttn, "XMLBttn");
            this.XMLBttn.Name = "XMLBttn";
            this.XMLBttn.TabStop = false;
            // 
            // SDFBttn
            // 
            this.SDFBttn.Checked = true;
            resources.ApplyResources(this.SDFBttn, "SDFBttn");
            this.SDFBttn.Name = "SDFBttn";
            // 
            // LclPathTxtBx
            // 
            resources.ApplyResources(this.LclPathTxtBx, "LclPathTxtBx");
            this.LclPathTxtBx.Name = "LclPathTxtBx";
            // 
            // RmtURITxtBx
            // 
            resources.ApplyResources(this.RmtURITxtBx, "RmtURITxtBx");
            this.RmtURITxtBx.Name = "RmtURITxtBx";
            // 
            // RemFileSetupBttn
            // 
            resources.ApplyResources(this.RemFileSetupBttn, "RemFileSetupBttn");
            this.RemFileSetupBttn.Name = "RemFileSetupBttn";
            this.RemFileSetupBttn.Click += new System.EventHandler(this.OnRemoteBttnClicked);
            // 
            // DBPrefInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.RemFileSetupBttn);
            this.Controls.Add(this.LocalBrwseBttn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CSVBttn);
            this.Controls.Add(this.XMLBttn);
            this.Controls.Add(this.SDFBttn);
            this.Controls.Add(this.LclPathTxtBx);
            this.Controls.Add(this.RmtURITxtBx);
            this.Name = "DBPrefInput";
            resources.ApplyResources(this, "$this");
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button LocalBrwseBttn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton CSVBttn;
        private System.Windows.Forms.RadioButton XMLBttn;
        private System.Windows.Forms.RadioButton SDFBttn;
        private System.Windows.Forms.TextBox LclPathTxtBx;
        private System.Windows.Forms.TextBox RmtURITxtBx;
        private System.Windows.Forms.Button RemFileSetupBttn;
    }
}
