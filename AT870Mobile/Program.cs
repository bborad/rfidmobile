using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Text;using CS101UILib; 
using System.Threading;
using System.Windows.Forms;
using ClsRampdb;
using ClsReaderLib;using ClsReaderLib.Devices;
using ClsReaderLib.Devices.Barcode;


namespace OnRamp
{

    static partial class Program
    {
#region "BKey Variables"
        public static String localDBFile;
        public static Int16 NoOfThreads=0;
#endregion

        
        private static AppExclLock ExclLck;
        private static SplashScreen SplScrn;
        //public static bool VernonFlag = true;
        public static bool ReqEnd;
        public static bool RadioClosed;
        public const String RfidLogFilePath = "\\RfidLog\\";
        public static bool FinalMsgDone;
        private const int finalMsgDlgVisTime = 1; // 1 sec enough
        public static TagOperSound SystemSnd;
        private static System.Windows.Forms.Timer RetryOpenTmr = null;
        private static System.Windows.Forms.Timer PwrOnResetTmr = null;
        private static TransientMsgDlg appExitDlg;
        private static void FinalMsgDlgClosed(object sender, EventArgs e)
        {
            Program.FinalMsgDone = true;
        }

        #region RetryOpen Timer
        // RetryOpen Timer
        static void InitRetryOpenTmr()
        {
            RetryOpenTmr = new System.Windows.Forms.Timer();
            RetryOpenTmr.Interval = 500;
            RetryOpenTmr.Tick += OnRetryOpenTmrTick;
            RetryOpenTmr.Enabled = false;
        }

        private static void OnRetryOpenTmrTick(object sender, EventArgs e)
        {
            System.Windows.Forms.Timer Tmr = (System.Windows.Forms.Timer)sender;
            Tmr.Enabled = false; // one-shot

            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
           // Rdr.RadioStatusNotification += RadioOpenResultNotify;
            Rdr.RegisterRadioStatusNotificationEvent(RadioOpenResultNotify);
            if (!Rdr.RadioOpen())
                throw new ApplicationException("RadioOpen denied!");

        }
        #endregion

        private static void RadioOpenResultNotify(RadioOpRes res, RadioStatus status, String msg)
        {
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (res)
            {
                case RadioOpRes.Error:
                    SimpleModalDialog dlg = new SimpleModalDialog("Radio Open Error: " + msg +  "\n" + "Press OK to try again");
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        RetryOpenTmr.Enabled = true;
                    }
                    else
                    {
                        Program.ReqEnd = true;
                        SplScrn.Closing -= OnSplshScrnClosing;
                        SplScrn.Close();
                    }
                    break;
                case RadioOpRes.Warning:
                    MessageBox.Show(msg, "Radio Open Warning");
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opened: // assuming that Opened won't be followed by Configured
                        case RadioStatus.Configured:
                            UserPref Pref = UserPref.GetInstance();
                            if (!Rdr.SetTempThreshold((ushort)Pref.XcvrTempLmt, (ushort)Pref.AmbTempLmt,
                               (ushort)Pref.PATempLmt, (ushort)Pref.DeltaTempLmt))
                                throw new ApplicationException("SetTempThreshold return error!");
                            else
                            {
                                TempMonitor.GetInstance(); // to load temp threshold early
                                SplScrn.Close();
                            }
                        break;
                    }
                    break;
            }
        }

        private static void RadioCloseResultNotify(RadioOpRes Res, RadioStatus status, String msg)
        {
            TransientMsgDlg dlg;
            
            switch (status)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
#if false
                    dlg = new TransientMsgDlg(finalMsgDlgVisTime);
#else
                    dlg = appExitDlg;
                    dlg.SetTimeout(finalMsgDlgVisTime);
#endif
                    if (Res == RadioOpRes.Error)
                        dlg.SetDpyMsg(msg, "Radio Closed with Error");
                    else if (Res == RadioOpRes.Warning)
                        dlg.SetDpyMsg(msg, "Radio Closed with Warning");
                    else
                        dlg.SetDpyMsg("Library Shutdown Properly", "Notice");
                    dlg.Closed += new EventHandler(FinalMsgDlgClosed);
                    dlg.Show();
                    Program.RadioClosed = true;
                    break;
                default:
#if false
                    dlg = new TransientMsgDlg(finalMsgDlgVisTime);
#else
                    dlg = appExitDlg;
                    dlg.SetTimeout(finalMsgDlgVisTime);
#endif
                    if (Res == RadioOpRes.Error)
                        dlg.SetDpyMsg(msg, "Radio unable to Close Error");
                    else if (Res == RadioOpRes.Warning)
                        dlg.SetDpyMsg(msg, "Radio unable to Close Warning");
                    dlg.Closed += new EventHandler(FinalMsgDlgClosed);
                    dlg.Show();
                     Program.RadioClosed = true;
                    break;
            }
        }

      


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            #region "bkey"

            UserPref Pref = UserPref.GetInstance();

            if (Pref.SelectedHardware == HardwareSelection.None)
            {
                frmHardwareSelection objFrmHS = new frmHardwareSelection();
                objFrmHS.ShowDialog();

                if (!objFrmHS.hwSelected)
                {
                    MessageBox.Show("Hardware not selected! Cannot start application.","Hardware Selection.");
                    objFrmHS.Dispose();
                    return;
                }
                objFrmHS.Dispose();
            }

            localDBFile = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            localDBFile = localDBFile + "\\Data\\ReaderDB.sdf";
            CEConn.dbFilePath = localDBFile;  

            #endregion


            // Quit if another instance is running
            String AppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            if (ExclLck.Acquire(AppName) == false)
            {
                TransientMsgDlg DyingMsg = new TransientMsgDlg(2);
                DyingMsg.SetDpyMsg("Another instance of  " + AppName + " is already running", "Notice");
                DyingMsg.ShowDialog();
                return; // Done after Msg closed
            }

            Program.ReqEnd = false;
            Program.RadioClosed = true;

            Reader rRdr = null;
            HHDeviceInterface.BarCode.BarCodeReader bRdr = null;

            SplScrn = new SplashScreen();
            SplScrn.Closing += new CancelEventHandler(OnSplshScrnClosing);

            SplScrn.Show();

            try
            {
                // Load User Preference from File
               
                
                // Log Trace Turn On
                if (Pref.LogTraceOn)
                {
                    if (!Datalog.LogInit(RfidLogFilePath))
                    {
                        TransientMsgDlg dlg = new TransientMsgDlg(2);
                        dlg.SetDpyMsg(Datalog.ErrMsg, "Trace Log Error");
                        dlg.Show();
                        dlg.TopMost = true;
                    }
                }
#if ! GENERIC_PDA_HW
                // Application Initialization (registry : drivers)
                // Safer then setting it from CAB (because of un-installation issue)
                if (SettingMgt.f_ApplicationSetup() != 0)
                {
                    throw new ApplicationException("Application Setup failed, cannot continue");
                }
#endif
                rRdr = ReaderFactory.GetReader();

#if !GENERIC_PDA_HW
                PosSp.RadioPwrOn();
                // wait for a while before Initializing Radio
                Thread.Sleep(1000); // long enough?
#endif
                // Initialize Radio
               // rRdr.RadioStatusNotification += RadioOpenResultNotify;
                rRdr.RegisterRadioStatusNotificationEvent(RadioOpenResultNotify);
                if (!rRdr.RadioOpen())
                    throw new ApplicationException("RadioOpen denied!");
                Program.RadioClosed = false;

                // Program Loop
                while (!Program.ReqEnd) /* keep the application running after Splash Screen closes */
                    Application.DoEvents();
            }
            catch (Exception e)
            {
                SplScrn.Close();
                MessageBox.Show(e.Message + "\r\n" + "StackTrace:\r\n" 
                    + e.StackTrace, "Exception Caught");
            }

            appExitDlg = new TransientMsgDlg(-1);
            appExitDlg.SetDpyMsg("Closing application. Please wait...", "Notice");
            appExitDlg.Show();
            //Application.DoEvents();
#if !GENERIC_PDA_HW
            // Close Serial Port
            bRdr = BarCodeFactory.GetBarCodeRdr();
            bRdr.Dispose();
#endif
            // Close Radio
            //rRdr = RFIDRdr.GetInstance();
            rRdr = ReaderFactory.GetReader();
            //rRdr.RadioStatusNotification += RadioCloseResultNotify;
            rRdr.RegisterRadioStatusNotificationEvent(RadioCloseResultNotify);
            //if (rRdr.RadioClose(RadioCloseResultNotify))
            if (rRdr.RadioClose())
            {
                while (!Program.RadioClosed)
                    Application.DoEvents();
            }

            // Does not wait for RadioClose finish status before unloading library (Problem?)
            ClsHotkey.Dispose();
            
            rRdr.Dispose();
#if !GENERIC_PDA_HW
            PosSp.RadioPwrOff();

            PosSp.ScnrLaserOff();

            PosSp.Dispose();
#endif
            Console.WriteLine("This is the End");
            // Close Log
            Datalog.LogUninit();
            Datalog.ErrLogUninit();

            while (!Program.FinalMsgDone)
                Application.DoEvents();
            ExclLck.Dispose(); // Release Excl Lock
        }

        static Program()
        {
            ExclLck = new AppExclLock();
            Program.ReqEnd = false;
            Program.FinalMsgDone = false;
             SystemSnd= new TagOperSound(ref SettingsForm.SndTabChanged);
             InitRetryOpenTmr();
             InitPwrOnResetTmr();
        }

        private static void OnSplshScrnClosing(Object sender, CancelEventArgs e)
        {
            UserPref Pref = UserPref.GetInstance();            

            if (Pref.UserSignOnRequired)
            {
                // Open Login Form
                LoginForm LoginFm = new LoginForm();
                LoginFm.Show();
            }
            else
            {
                // Open Main Menu
                MainMenuForm MenuFm = new MainMenuForm();
                MenuFm.Show();
            }
            // ensure closing of splashscreen
            e.Cancel = false;

        }

      
        // Common GUI routines to be shared by all functions

        #region Reset + Open Radio (without user intervention)
        private static TransientMsgDlg autoRestartStatusDlg = null;

        private static void InitPwrOnResetTmr()
        {
            PwrOnResetTmr = new System.Windows.Forms.Timer();
            PwrOnResetTmr.Interval = 3 * 1000;
            PwrOnResetTmr.Enabled = false;
            PwrOnResetTmr.Tick += OnPwrOnResetTmrTick;
        }

        private static void OnPwrOnResetTmrTick(object sender, EventArgs e)
        {
            System.Windows.Forms.Timer thisTmr = (System.Windows.Forms.Timer)sender;
            thisTmr.Enabled = false; // one-shot timer

            Thread.Sleep(3 * 1000);
            PosSp.RadioPwrOn();
            autoRestartStatusDlg.SetDpyMsg("Opening RFID Device...", "Notice");
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //Rdr.RadioStatusNotification += RadioReOpenResultNotify;
            Rdr.RegisterRadioStatusNotificationEvent(RadioReOpenResultNotify);
            Rdr.RadioReOpen(); // Don't care about status
        }

        private static void RadioLostCloseResultNotify(RadioOpRes res,
            RadioStatus status, String msg)
        {
            switch (status)
            {
                case RadioStatus.Closed:
                case RadioStatus.ErrorStopped:
                // Radio closed with Error/Warning, display message and continue
                    switch (res)
                    {
                        case RadioOpRes.Error:
                            autoRestartStatusDlg.SetDpyMsg("Resetting...\nRadio Closed with Error: " + msg, "Error Notice");
                            break;
                        case RadioOpRes.Warning:
                            autoRestartStatusDlg.SetDpyMsg("Resetting...\nRadio Closed with Warning: " + msg,  "Warning Notice");
                            break;
                        default:
                            autoRestartStatusDlg.SetDpyMsg("Resetting...\nRadio Closed", "Notice");
                            break;
                    }
                    PosSp.RadioPwrOff();
                    PwrOnResetTmr.Enabled = true; // Power On Radio after a while
                    break;
                default: // Radio Library not shutdown
                    switch (res)
                    {
                        case RadioOpRes.Error:
                            autoRestartStatusDlg.SetDpyMsg("Reset Failed\nRadio unable to Close Error: " + msg, "Error Notice");
                            break;
                        case RadioOpRes.Warning:
                            autoRestartStatusDlg.SetDpyMsg("Reset Failed\nunable to Close Warning: " + msg, "Warning Notice");
                            break;
                        default:
                            autoRestartStatusDlg.SetDpyMsg("Reset Failed\nRadio unable to Close.", "Error Notice");
                            break;
                    }
                    autoRestartStatusDlg.Closing -= OnAutoRestartStatusDlgClosing; // let user close the dialog
                    autoRestartStatusDlg.SetTimeout (-1);
                    if (radioRestartCallerNotify != null)
                        radioRestartCallerNotify(false);
                    break;
            }
        }

        private static void RadioReOpenResultNotify(RadioOpRes res,
            RadioStatus status, String msg)
        {
            switch (res)
            {
                case RadioOpRes.Error:
                    autoRestartStatusDlg.Closing -= OnAutoRestartStatusDlgClosing;
                    autoRestartStatusDlg.SetDpyMsg("Open Failed: " + msg, "Error Notice");
                    autoRestartStatusDlg.SetTimeout(3); // close after 3 seconds
                    if (radioRestartCallerNotify != null)
                        radioRestartCallerNotify(false);
                    break;
                case RadioOpRes.Warning:
                    autoRestartStatusDlg.SetDpyMsg("Warning: " + msg, "Warning Notice");
                    break;
                case RadioOpRes.StateChanged:
                    switch (status)
                    {
                        case RadioStatus.Opened: // assuming that Opened won't be followed by Configured
                        case RadioStatus.Configured:
                            UserPref Pref = UserPref.GetInstance();
                            Reader Rdr = ReaderFactory.GetReader();
                            if (!Rdr.SetTempThreshold((ushort)Pref.XcvrTempLmt, (ushort)Pref.AmbTempLmt,
                                (ushort)Pref.PATempLmt, (ushort)Pref.DeltaTempLmt))
                                throw new ApplicationException("SetTempThreshold return error!");
                            else
                            {
                                TempMonitor.GetInstance(); // to load temp threshold early
                                autoRestartStatusDlg.SetDpyMsg("Open Success! " , "Notice");
                                autoRestartStatusDlg.SetTimeout(1);
                                autoRestartStatusDlg.Closing -= OnAutoRestartStatusDlgClosing;
                            }
                            if (radioRestartCallerNotify != null)
                                radioRestartCallerNotify(true);
                            break;
                        default:
                            autoRestartStatusDlg.SetDpyMsg("Opening status : " + status.ToString("F"), "Notice");
                            break;
                    }
                    break;
            }
        }

        public delegate void RadioRestartNotify(bool succ);
        private static RadioRestartNotify radioRestartCallerNotify;

        public static void PromptUserRestartRadio()
        {
            Program.PromptUserRestartRadio(null);
        }

        public static void PromptUserRestartRadio(RadioRestartNotify notify)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            
#if false
             // Prompt User to restart
            SimpleModalDialog restartDlg = new SimpleModalDialog("Radio handle is lost, Re-Open?");
            DialogResult res = restartDlg.ShowDialog();
            if (res == DialogResult.OK)
            {
                Program.RadioPwrOn();
               // Rdr.RadioReOpen(null); // Don't care about status
            Rdr.RadioReOpen();
            }
#else
            radioRestartCallerNotify = notify;
            if (autoRestartStatusDlg == null)
            {
                autoRestartStatusDlg = new TransientMsgDlg(-1);
                autoRestartStatusDlg.TopMost = true;
                autoRestartStatusDlg.Closing += new CancelEventHandler(OnAutoRestartStatusDlgClosing);
                autoRestartStatusDlg.Closed += new EventHandler(OnAutoRestartStatusDlgClosed);
            }
            autoRestartStatusDlg.SetTimeout(-1);
            //Rdr.RadioStatusNotification += RadioLostCloseResultNotify;
            Rdr.RegisterRadioStatusNotificationEvent(RadioLostCloseResultNotify);
            Rdr.RadioClose();
            autoRestartStatusDlg.SetDpyMsg("Resetting RFID Device...", "Notice");
            autoRestartStatusDlg.Show();
#endif
        }

        static void OnAutoRestartStatusDlgClosed(object sender, EventArgs e)
        {
            autoRestartStatusDlg = null; // resources are freed
        }

        static void OnAutoRestartStatusDlgClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = true; // do not allow user close this dialog
        }

        #endregion


        #region Common UI routines
        public static void AlphaNumKeyPressChk(object sender, KeyPressEventArgs e)
        {
            // From documentation: e.KeyChar won't deliver DELETE and a bunch of
            // other editing keys
            if ((e.KeyChar >= '0'  /* or NumPad0? */ && e.KeyChar <= '9')
              || (e.KeyChar >= 'a' && e.KeyChar <= 'z')
              || (e.KeyChar >= 'A' && e.KeyChar <= 'Z')
              || (e.KeyChar == (char)Keys.Back) /*|| (e.KeyChar == (char)Keys.Delete)*/)
            // Arrow keys, Home, End?
            {
                e.Handled = false; // accept
            }
            else
            {
                Program.ErrorFlashTxtBx((TextBox)sender);
                e.Handled = true; // reject
            }
        }

        public static void PrintableNoSpaceKeyPressChk(object sender, KeyPressEventArgs e)
        {
            if (Char.IsWhiteSpace(e.KeyChar) || (Char.IsControl(e.KeyChar) && e.KeyChar != (char)Keys.Back))
            {
                Program.ErrorFlashTxtBx((TextBox)sender);
                e.Handled = true;
            }
            else
            {
                e.Handled = false; // accept
            }
        }

        public static void HexKeyPressChk(object sender, KeyPressEventArgs e)
        {
            // From documentation: e.KeyChar won't deliver DELETE and a bunch of
            // other editing keys
            if ((e.KeyChar >= '0'  /* or NumPad0? */ && e.KeyChar <= '9')
                || (e.KeyChar >= 'a' && e.KeyChar <= 'f')
                || (e.KeyChar >= 'A' && e.KeyChar <= 'F')
                || (e.KeyChar == (char)Keys.Back) /*|| (e.KeyChar == (char)Keys.Delete)*/)
            // Arrow keys, Home, End?
            {
                e.Handled = false; // accept
            }
            else
            {
                Program.ErrorFlashTxtBx((TextBox)sender);
                e.Handled = true; // reject
            }
        }

        public static void NumKeyPressChk(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (e.KeyChar == (char)Keys.Back))
            {
                e.Handled = false; // accept
            }
            else
            {
                Program.ErrorFlashTxtBx((TextBox)sender);
                e.Handled = true;
            }
        }

        
        public static void ErrorFlashTxtBx(TextBox txtBx)
        {
            txtBx.BackColor = Color.Red;
            txtBx.Refresh();

            Thread.Sleep(500);

            txtBx.BackColor = Color.White;
            txtBx.Refresh();
        }

        public static void RefreshStatusLabel(Form fm, String dpyStr)
        {
            // Append 'dpyStr' to Window Title Text
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(fm.GetType());
            String Base = resources.GetString("$this.Text");
            if (! String.IsNullOrEmpty(dpyStr))
                fm.Text = Base + "(" + dpyStr + ")";
            else
                fm.Text = Base;
        }

        public static DialogResult AskUserConfirm(String cfmMsg)
        {
            SimpleModalDialog CfmDlg = new SimpleModalDialog(cfmMsg);
            DialogResult Res = CfmDlg.ShowDialog();
            CfmDlg.Dispose();
            return Res;
        }

        public static void ShowWarning(String warnMsg)
        {
            TransientMsgDlg WarnDlg = new TransientMsgDlg(5);
            WarnDlg.SetDpyMsg(warnMsg, "Warning Notice");
            WarnDlg.TopMost = true;
            WarnDlg.Show();
        }

        public static void ShowError(String errMsg)
        {
            TransientMsgDlg ErrDlg = new TransientMsgDlg(0);
            ErrDlg.SetDpyMsg(errMsg, "Error Notice");
            ErrDlg.ShowDialog();
            ErrDlg.Dispose();
        }

        public static void ShowSuccess(String succMsg)
        {
            TransientMsgDlg SuccDlg = new TransientMsgDlg(5);
            SuccDlg.SetDpyMsg(succMsg, "Success");
            SuccDlg.Show();
        }

        #region Reader Summary Info Display
        static RdrInfoSummaryForm RdrSumFm = null;
        
        private static void OnRdrSumFmClosed(object sender, EventArgs e)
        {
            RdrSumFm = null;
        }
        
        public static void ShowRdrSummaryDisplayWindow(HandHeldHotKeyNotify hkNotify)
        {
            // Show/Hide ReaderInfoSummary
            if (RdrSumFm == null)
            {
                RdrSumFm = new RdrInfoSummaryForm(hkNotify);
                RdrSumFm.Closed += new EventHandler(OnRdrSumFmClosed);
            }
            if (RdrSumFm.Visible)
                RdrSumFm.Close();
            else
                RdrSumFm.Show();
        }
        #endregion

        #endregion

        #region Mac Error Display
        static TransientMsgDlg MacErrFm = null;

        private static void OnMacErrFmClosed(object sender, EventArgs e)
        {
            MacErrFm = null;
        }

        private static void OnMacErrFmClosing(object sender, CancelEventArgs e)
        {
            if (Program.AskUserConfirm("Clear Mac Error Register?") == DialogResult.OK)
            {
               // RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                if (Rdr.ClearMacError() == false)
                {
                    Program.ShowError("Clear Mac Error Failed: " + Rdr.LastErrCode.ToString("F"));
                }
            }
        }

        public static void ShowMacError(HandHeldHotKeyNotify hkNotify)
        {
            if (MacErrFm == null)
            {
               // RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                ushort MacErrCode = 0;
                if (Rdr.GetMacError() == true)
                {
                    MacErrFm = new TransientMsgDlg(10);
                    StringBuilder Sb = new StringBuilder("Mac Error Code: 0x" + MacErrCode.ToString("X4"));
                    if (MacErrCode == 0x0309)
                    {
                        //UInt32 PwrLevRegVal = 0;
                        //UInt32 RevPwrLevRegVal = 0;
                        //UInt32 RevPwrThrshRegVal = 0;
                        if (Rdr.GetPwrLvlRegVals() == true)
                        {
                            Sb = Sb.Append("\n" + "FwdPwr: " + ((float)(Rdr.PwrLevRegVal * 0.1F)).ToString("F1") + " dBm"
                                + "; " + "Rev: " + ((float)(Rdr.RevPwrLevRegVal * 0.1F)).ToString("F1") + " dBm"
                                + "; " + "Thrsh: " + ((float)(Rdr.RevPwrThrshRegVal * 0.1F)).ToString("F1") + " dBm");
                        }
                    }
                    else if (Rdr.MacErrorIsNegligible())
                    {
                        Sb = Sb.Append("\n" + "This error could be ignored");
                    }
                    MacErrFm.SetDpyMsg(Sb.ToString(), "Firmware Error Register");
                    if (MacErrCode != 0)
                    {
                        MacErrFm.Closing += OnMacErrFmClosing;
                    }
                    MacErrFm.Closed += OnMacErrFmClosed;
                }
            }
            if (MacErrFm != null)
            {
                if (MacErrFm.Visible)
                    MacErrFm.Close();
                else
                    MacErrFm.Show();
            }
        }
        #endregion
    }

}