namespace OnRamp
{
    partial class DevStatForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevStatForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.RstButton = new System.Windows.Forms.Button();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.TabCtrl = new System.Windows.Forms.TabControl();
            this.InvtrySetupTab = new System.Windows.Forms.TabPage();
            this.FixedQBttn = new System.Windows.Forms.RadioButton();
            this.DynQBttn = new System.Windows.Forms.RadioButton();
            this.QLbl = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.PopSzTxtBx = new System.Windows.Forms.TextBox();
            this.MaskInput = new OnRamp.EPCMaskInput();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.InvtrySessCmbBx = new System.Windows.Forms.ComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.TagAttrTab = new System.Windows.Forms.TabPage();
            this.TagVndrCmbBx = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.UsrBnkSzLbl = new System.Windows.Forms.Label();
            this.TIDBnkSzLbl = new System.Windows.Forms.Label();
            this.UserMemSzUD = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.TIDMemSzUD = new System.Windows.Forms.NumericUpDown();
            this.TagUserMemChkBx = new System.Windows.Forms.CheckBox();
            this.TagTIDChkBx = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.FreqTab = new System.Windows.Forms.TabPage();
            this.FreqBndNumLbl = new System.Windows.Forms.Label();
            this.SingleChnLnkLbl = new System.Windows.Forms.LinkLabel();
            this.SingleChnChkBx = new System.Windows.Forms.CheckBox();
            this.LBTPanel = new System.Windows.Forms.Panel();
            this.LBTNoBttn = new System.Windows.Forms.RadioButton();
            this.LBTYesBttn = new System.Windows.Forms.RadioButton();
            this.label44 = new System.Windows.Forms.Label();
            this.CntryFreqRangeLbl = new System.Windows.Forms.Label();
            this.FreqCntryCmbBx = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.BandFrqRangeLbl = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.FreqBndCmbBx = new System.Windows.Forms.ComboBox();
            this.LnkPrfTab = new System.Windows.Forms.TabPage();
            this.LnkPrfMdfyDetailsLnk = new System.Windows.Forms.LinkLabel();
            this.LnkPrfDetailHdrLbl = new System.Windows.Forms.Label();
            this.LnkPrfDetailsLnk = new System.Windows.Forms.LinkLabel();
            this.LnkPrfDetailLbl = new System.Windows.Forms.Label();
            this.LnkPrf5Button = new System.Windows.Forms.RadioButton();
            this.LnkPrf4Button = new System.Windows.Forms.RadioButton();
            this.LnkPrf3Button = new System.Windows.Forms.RadioButton();
            this.LnkPrf2Button = new System.Windows.Forms.RadioButton();
            this.LnkPrf1Button = new System.Windows.Forms.RadioButton();
            this.LnkPrf0Button = new System.Windows.Forms.RadioButton();
            this.AntTab = new System.Windows.Forms.TabPage();
            this.label53 = new System.Windows.Forms.Label();
            this.txtHighValue = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtMedValue = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.txtLowValue = new System.Windows.Forms.TextBox();
            this.NoLmtLbl = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.InvtryCycUD = new System.Windows.Forms.NumericUpDown();
            this.DwellTimeUD = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.PwrLvlValLbl = new System.Windows.Forms.Label();
            this.PwrLvlTrkBr = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.TagSelTab = new System.Windows.Forms.TabPage();
            this.SelCrit3Lnk = new System.Windows.Forms.LinkLabel();
            this.SelCrit2Lnk = new System.Windows.Forms.LinkLabel();
            this.SelCrit1Lnk = new System.Windows.Forms.LinkLabel();
            this.SelCrit0Lnk = new System.Windows.Forms.LinkLabel();
            this.label17 = new System.Windows.Forms.Label();
            this.SelCritCntUD = new System.Windows.Forms.NumericUpDown();
            this.QryPrmTab = new System.Windows.Forms.TabPage();
            this.DQPanel = new System.Windows.Forms.Panel();
            this.DQMxQryCntUD = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.QMxLbl = new System.Windows.Forms.Label();
            this.MaxQTrkBr = new System.Windows.Forms.TrackBar();
            this.QMnLbl = new System.Windows.Forms.Label();
            this.MinQTrkBr = new System.Windows.Forms.TrackBar();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.DQRetryCntUD = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.QStLbl = new System.Windows.Forms.Label();
            this.StartQTrkBr = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.FQPanel = new System.Windows.Forms.Panel();
            this.FQRetryCntUD = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.RptChkBx = new System.Windows.Forms.CheckBox();
            this.QSzLbl = new System.Windows.Forms.Label();
            this.FQSzTrkBr = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.DQButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.FQButton = new System.Windows.Forms.RadioButton();
            this.TagGrpPanel = new System.Windows.Forms.Panel();
            this.SLAllButton = new System.Windows.Forms.RadioButton();
            this.SLOffButton = new System.Windows.Forms.RadioButton();
            this.SLOnButton = new System.Windows.Forms.RadioButton();
            this.label18 = new System.Windows.Forms.Label();
            this.ToggleTgtChkBx = new System.Windows.Forms.CheckBox();
            this.SessValCmbBx = new System.Windows.Forms.ComboBox();
            this.SessCmbBx = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.OvrHtProtTab = new System.Windows.Forms.TabPage();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.XcvrTempUD = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.OnfrvrLbl = new System.Windows.Forms.Label();
            this.DCOffUD = new System.Windows.Forms.NumericUpDown();
            this.DCOnUD = new System.Windows.Forms.NumericUpDown();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.TempTab = new System.Windows.Forms.TabPage();
            this.DeltaTempLbl = new System.Windows.Forms.Label();
            this.PATempLbl = new System.Windows.Forms.Label();
            this.AmbTempLbl = new System.Windows.Forms.Label();
            this.XcvrTempLbl = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.ServiceSettings = new System.Windows.Forms.TabPage();
            this.label50 = new System.Windows.Forms.Label();
            this.txtMsgDisplayTimeOut = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.rbErrLogDisable = new System.Windows.Forms.RadioButton();
            this.rbErrLogEnable = new System.Windows.Forms.RadioButton();
            this.label48 = new System.Windows.Forms.Label();
            this.txtPageSize = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.tabBarcodeSettings = new System.Windows.Forms.TabPage();
            this.rb2DScanner = new System.Windows.Forms.RadioButton();
            this.rb1DScanner = new System.Windows.Forms.RadioButton();
            this.TabCtrl.SuspendLayout();
            this.InvtrySetupTab.SuspendLayout();
            this.TagAttrTab.SuspendLayout();
            this.FreqTab.SuspendLayout();
            this.LBTPanel.SuspendLayout();
            this.LnkPrfTab.SuspendLayout();
            this.AntTab.SuspendLayout();
            this.TagSelTab.SuspendLayout();
            this.QryPrmTab.SuspendLayout();
            this.DQPanel.SuspendLayout();
            this.FQPanel.SuspendLayout();
            this.TagGrpPanel.SuspendLayout();
            this.OvrHtProtTab.SuspendLayout();
            this.TempTab.SuspendLayout();
            this.ServiceSettings.SuspendLayout();
            this.tabBarcodeSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // RstButton
            // 
            resources.ApplyResources(this.RstButton, "RstButton");
            this.RstButton.Name = "RstButton";
            this.RstButton.Click += new System.EventHandler(this.OnRstButtonClicked);
            // 
            // ApplyButton
            // 
            resources.ApplyResources(this.ApplyButton, "ApplyButton");
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Click += new System.EventHandler(this.OnApplyButtonClicked);
            // 
            // TabCtrl
            // 
            this.TabCtrl.Controls.Add(this.InvtrySetupTab);
            this.TabCtrl.Controls.Add(this.TagAttrTab);
            this.TabCtrl.Controls.Add(this.FreqTab);
            this.TabCtrl.Controls.Add(this.LnkPrfTab);
            this.TabCtrl.Controls.Add(this.AntTab);
            this.TabCtrl.Controls.Add(this.TagSelTab);
            this.TabCtrl.Controls.Add(this.QryPrmTab);
            this.TabCtrl.Controls.Add(this.OvrHtProtTab);
            this.TabCtrl.Controls.Add(this.TempTab);
            this.TabCtrl.Controls.Add(this.ServiceSettings);
            this.TabCtrl.Controls.Add(this.tabBarcodeSettings);
            resources.ApplyResources(this.TabCtrl, "TabCtrl");
            this.TabCtrl.Name = "TabCtrl";
            this.TabCtrl.SelectedIndex = 0;
            // 
            // InvtrySetupTab
            // 
            this.InvtrySetupTab.BackColor = System.Drawing.Color.White;
            this.InvtrySetupTab.Controls.Add(this.FixedQBttn);
            this.InvtrySetupTab.Controls.Add(this.DynQBttn);
            this.InvtrySetupTab.Controls.Add(this.QLbl);
            this.InvtrySetupTab.Controls.Add(this.label45);
            this.InvtrySetupTab.Controls.Add(this.PopSzTxtBx);
            this.InvtrySetupTab.Controls.Add(this.MaskInput);
            this.InvtrySetupTab.Controls.Add(this.label26);
            this.InvtrySetupTab.Controls.Add(this.label27);
            this.InvtrySetupTab.Controls.Add(this.InvtrySessCmbBx);
            this.InvtrySetupTab.Controls.Add(this.label28);
            resources.ApplyResources(this.InvtrySetupTab, "InvtrySetupTab");
            this.InvtrySetupTab.Name = "InvtrySetupTab";
            // 
            // FixedQBttn
            // 
            resources.ApplyResources(this.FixedQBttn, "FixedQBttn");
            this.FixedQBttn.Name = "FixedQBttn";
            this.FixedQBttn.TabStop = false;
            // 
            // DynQBttn
            // 
            this.DynQBttn.Checked = true;
            resources.ApplyResources(this.DynQBttn, "DynQBttn");
            this.DynQBttn.Name = "DynQBttn";
            // 
            // QLbl
            // 
            resources.ApplyResources(this.QLbl, "QLbl");
            this.QLbl.Name = "QLbl";
            // 
            // label45
            // 
            resources.ApplyResources(this.label45, "label45");
            this.label45.Name = "label45";
            // 
            // PopSzTxtBx
            // 
            resources.ApplyResources(this.PopSzTxtBx, "PopSzTxtBx");
            this.PopSzTxtBx.Name = "PopSzTxtBx";
            // 
            // MaskInput
            // 
            this.MaskInput.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.MaskInput, "MaskInput");
            this.MaskInput.Name = "MaskInput";
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.Name = "label26";
            // 
            // label27
            // 
            resources.ApplyResources(this.label27, "label27");
            this.label27.Name = "label27";
            // 
            // InvtrySessCmbBx
            // 
            this.InvtrySessCmbBx.Items.Add(resources.GetString("InvtrySessCmbBx.Items"));
            this.InvtrySessCmbBx.Items.Add(resources.GetString("InvtrySessCmbBx.Items1"));
            this.InvtrySessCmbBx.Items.Add(resources.GetString("InvtrySessCmbBx.Items2"));
            resources.ApplyResources(this.InvtrySessCmbBx, "InvtrySessCmbBx");
            this.InvtrySessCmbBx.Name = "InvtrySessCmbBx";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // TagAttrTab
            // 
            this.TagAttrTab.BackColor = System.Drawing.Color.White;
            this.TagAttrTab.Controls.Add(this.TagVndrCmbBx);
            this.TagAttrTab.Controls.Add(this.label29);
            this.TagAttrTab.Controls.Add(this.label30);
            this.TagAttrTab.Controls.Add(this.label31);
            this.TagAttrTab.Controls.Add(this.UsrBnkSzLbl);
            this.TagAttrTab.Controls.Add(this.TIDBnkSzLbl);
            this.TagAttrTab.Controls.Add(this.UserMemSzUD);
            this.TagAttrTab.Controls.Add(this.label32);
            this.TagAttrTab.Controls.Add(this.label33);
            this.TagAttrTab.Controls.Add(this.TIDMemSzUD);
            this.TagAttrTab.Controls.Add(this.TagUserMemChkBx);
            this.TagAttrTab.Controls.Add(this.TagTIDChkBx);
            this.TagAttrTab.Controls.Add(this.label34);
            resources.ApplyResources(this.TagAttrTab, "TagAttrTab");
            this.TagAttrTab.Name = "TagAttrTab";
            // 
            // TagVndrCmbBx
            // 
            this.TagVndrCmbBx.Items.Add(resources.GetString("TagVndrCmbBx.Items"));
            this.TagVndrCmbBx.Items.Add(resources.GetString("TagVndrCmbBx.Items1"));
            resources.ApplyResources(this.TagVndrCmbBx, "TagVndrCmbBx");
            this.TagVndrCmbBx.Name = "TagVndrCmbBx";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // label30
            // 
            resources.ApplyResources(this.label30, "label30");
            this.label30.Name = "label30";
            // 
            // label31
            // 
            resources.ApplyResources(this.label31, "label31");
            this.label31.Name = "label31";
            // 
            // UsrBnkSzLbl
            // 
            resources.ApplyResources(this.UsrBnkSzLbl, "UsrBnkSzLbl");
            this.UsrBnkSzLbl.Name = "UsrBnkSzLbl";
            // 
            // TIDBnkSzLbl
            // 
            resources.ApplyResources(this.TIDBnkSzLbl, "TIDBnkSzLbl");
            this.TIDBnkSzLbl.Name = "TIDBnkSzLbl";
            // 
            // UserMemSzUD
            // 
            resources.ApplyResources(this.UserMemSzUD, "UserMemSzUD");
            this.UserMemSzUD.Maximum = new decimal(new int[] {
            32,
            0,
            0,
            0});
            this.UserMemSzUD.Name = "UserMemSzUD";
            this.UserMemSzUD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label32
            // 
            resources.ApplyResources(this.label32, "label32");
            this.label32.Name = "label32";
            // 
            // label33
            // 
            resources.ApplyResources(this.label33, "label33");
            this.label33.Name = "label33";
            // 
            // TIDMemSzUD
            // 
            resources.ApplyResources(this.TIDMemSzUD, "TIDMemSzUD");
            this.TIDMemSzUD.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.TIDMemSzUD.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.TIDMemSzUD.Name = "TIDMemSzUD";
            this.TIDMemSzUD.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // TagUserMemChkBx
            // 
            resources.ApplyResources(this.TagUserMemChkBx, "TagUserMemChkBx");
            this.TagUserMemChkBx.Name = "TagUserMemChkBx";
            // 
            // TagTIDChkBx
            // 
            resources.ApplyResources(this.TagTIDChkBx, "TagTIDChkBx");
            this.TagTIDChkBx.Name = "TagTIDChkBx";
            // 
            // label34
            // 
            resources.ApplyResources(this.label34, "label34");
            this.label34.Name = "label34";
            // 
            // FreqTab
            // 
            this.FreqTab.BackColor = System.Drawing.Color.White;
            this.FreqTab.Controls.Add(this.FreqBndNumLbl);
            this.FreqTab.Controls.Add(this.SingleChnLnkLbl);
            this.FreqTab.Controls.Add(this.SingleChnChkBx);
            this.FreqTab.Controls.Add(this.LBTPanel);
            this.FreqTab.Controls.Add(this.CntryFreqRangeLbl);
            this.FreqTab.Controls.Add(this.FreqCntryCmbBx);
            this.FreqTab.Controls.Add(this.label43);
            this.FreqTab.Controls.Add(this.BandFrqRangeLbl);
            this.FreqTab.Controls.Add(this.label10);
            this.FreqTab.Controls.Add(this.FreqBndCmbBx);
            resources.ApplyResources(this.FreqTab, "FreqTab");
            this.FreqTab.Name = "FreqTab";
            // 
            // FreqBndNumLbl
            // 
            resources.ApplyResources(this.FreqBndNumLbl, "FreqBndNumLbl");
            this.FreqBndNumLbl.Name = "FreqBndNumLbl";
            // 
            // SingleChnLnkLbl
            // 
            resources.ApplyResources(this.SingleChnLnkLbl, "SingleChnLnkLbl");
            this.SingleChnLnkLbl.Name = "SingleChnLnkLbl";
            // 
            // SingleChnChkBx
            // 
            resources.ApplyResources(this.SingleChnChkBx, "SingleChnChkBx");
            this.SingleChnChkBx.Name = "SingleChnChkBx";
            // 
            // LBTPanel
            // 
            this.LBTPanel.BackColor = System.Drawing.Color.White;
            this.LBTPanel.Controls.Add(this.LBTNoBttn);
            this.LBTPanel.Controls.Add(this.LBTYesBttn);
            this.LBTPanel.Controls.Add(this.label44);
            resources.ApplyResources(this.LBTPanel, "LBTPanel");
            this.LBTPanel.Name = "LBTPanel";
            // 
            // LBTNoBttn
            // 
            resources.ApplyResources(this.LBTNoBttn, "LBTNoBttn");
            this.LBTNoBttn.Name = "LBTNoBttn";
            // 
            // LBTYesBttn
            // 
            resources.ApplyResources(this.LBTYesBttn, "LBTYesBttn");
            this.LBTYesBttn.Name = "LBTYesBttn";
            // 
            // label44
            // 
            resources.ApplyResources(this.label44, "label44");
            this.label44.Name = "label44";
            // 
            // CntryFreqRangeLbl
            // 
            resources.ApplyResources(this.CntryFreqRangeLbl, "CntryFreqRangeLbl");
            this.CntryFreqRangeLbl.Name = "CntryFreqRangeLbl";
            // 
            // FreqCntryCmbBx
            // 
            resources.ApplyResources(this.FreqCntryCmbBx, "FreqCntryCmbBx");
            this.FreqCntryCmbBx.Name = "FreqCntryCmbBx";
            // 
            // label43
            // 
            resources.ApplyResources(this.label43, "label43");
            this.label43.Name = "label43";
            // 
            // BandFrqRangeLbl
            // 
            resources.ApplyResources(this.BandFrqRangeLbl, "BandFrqRangeLbl");
            this.BandFrqRangeLbl.Name = "BandFrqRangeLbl";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // FreqBndCmbBx
            // 
            this.FreqBndCmbBx.Items.Add(resources.GetString("FreqBndCmbBx.Items"));
            this.FreqBndCmbBx.Items.Add(resources.GetString("FreqBndCmbBx.Items1"));
            this.FreqBndCmbBx.Items.Add(resources.GetString("FreqBndCmbBx.Items2"));
            this.FreqBndCmbBx.Items.Add(resources.GetString("FreqBndCmbBx.Items3"));
            this.FreqBndCmbBx.Items.Add(resources.GetString("FreqBndCmbBx.Items4"));
            resources.ApplyResources(this.FreqBndCmbBx, "FreqBndCmbBx");
            this.FreqBndCmbBx.Name = "FreqBndCmbBx";
            // 
            // LnkPrfTab
            // 
            resources.ApplyResources(this.LnkPrfTab, "LnkPrfTab");
            this.LnkPrfTab.BackColor = System.Drawing.Color.White;
            this.LnkPrfTab.Controls.Add(this.LnkPrfMdfyDetailsLnk);
            this.LnkPrfTab.Controls.Add(this.LnkPrfDetailHdrLbl);
            this.LnkPrfTab.Controls.Add(this.LnkPrfDetailsLnk);
            this.LnkPrfTab.Controls.Add(this.LnkPrfDetailLbl);
            this.LnkPrfTab.Controls.Add(this.LnkPrf5Button);
            this.LnkPrfTab.Controls.Add(this.LnkPrf4Button);
            this.LnkPrfTab.Controls.Add(this.LnkPrf3Button);
            this.LnkPrfTab.Controls.Add(this.LnkPrf2Button);
            this.LnkPrfTab.Controls.Add(this.LnkPrf1Button);
            this.LnkPrfTab.Controls.Add(this.LnkPrf0Button);
            this.LnkPrfTab.Name = "LnkPrfTab";
            // 
            // LnkPrfMdfyDetailsLnk
            // 
            resources.ApplyResources(this.LnkPrfMdfyDetailsLnk, "LnkPrfMdfyDetailsLnk");
            this.LnkPrfMdfyDetailsLnk.Name = "LnkPrfMdfyDetailsLnk";
            // 
            // LnkPrfDetailHdrLbl
            // 
            resources.ApplyResources(this.LnkPrfDetailHdrLbl, "LnkPrfDetailHdrLbl");
            this.LnkPrfDetailHdrLbl.Name = "LnkPrfDetailHdrLbl";
            // 
            // LnkPrfDetailsLnk
            // 
            resources.ApplyResources(this.LnkPrfDetailsLnk, "LnkPrfDetailsLnk");
            this.LnkPrfDetailsLnk.Name = "LnkPrfDetailsLnk";
            // 
            // LnkPrfDetailLbl
            // 
            resources.ApplyResources(this.LnkPrfDetailLbl, "LnkPrfDetailLbl");
            this.LnkPrfDetailLbl.Name = "LnkPrfDetailLbl";
            // 
            // LnkPrf5Button
            // 
            resources.ApplyResources(this.LnkPrf5Button, "LnkPrf5Button");
            this.LnkPrf5Button.Name = "LnkPrf5Button";
            this.LnkPrf5Button.TabStop = false;
            // 
            // LnkPrf4Button
            // 
            resources.ApplyResources(this.LnkPrf4Button, "LnkPrf4Button");
            this.LnkPrf4Button.Name = "LnkPrf4Button";
            this.LnkPrf4Button.TabStop = false;
            // 
            // LnkPrf3Button
            // 
            resources.ApplyResources(this.LnkPrf3Button, "LnkPrf3Button");
            this.LnkPrf3Button.Name = "LnkPrf3Button";
            this.LnkPrf3Button.TabStop = false;
            // 
            // LnkPrf2Button
            // 
            resources.ApplyResources(this.LnkPrf2Button, "LnkPrf2Button");
            this.LnkPrf2Button.Name = "LnkPrf2Button";
            this.LnkPrf2Button.TabStop = false;
            // 
            // LnkPrf1Button
            // 
            resources.ApplyResources(this.LnkPrf1Button, "LnkPrf1Button");
            this.LnkPrf1Button.Name = "LnkPrf1Button";
            this.LnkPrf1Button.TabStop = false;
            // 
            // LnkPrf0Button
            // 
            resources.ApplyResources(this.LnkPrf0Button, "LnkPrf0Button");
            this.LnkPrf0Button.Name = "LnkPrf0Button";
            this.LnkPrf0Button.TabStop = false;
            // 
            // AntTab
            // 
            this.AntTab.BackColor = System.Drawing.Color.White;
            this.AntTab.Controls.Add(this.label53);
            this.AntTab.Controls.Add(this.txtHighValue);
            this.AntTab.Controls.Add(this.label52);
            this.AntTab.Controls.Add(this.txtMedValue);
            this.AntTab.Controls.Add(this.label51);
            this.AntTab.Controls.Add(this.txtLowValue);
            this.AntTab.Controls.Add(this.NoLmtLbl);
            this.AntTab.Controls.Add(this.label19);
            this.AntTab.Controls.Add(this.InvtryCycUD);
            this.AntTab.Controls.Add(this.DwellTimeUD);
            this.AntTab.Controls.Add(this.label14);
            this.AntTab.Controls.Add(this.label13);
            this.AntTab.Controls.Add(this.label12);
            this.AntTab.Controls.Add(this.PwrLvlValLbl);
            this.AntTab.Controls.Add(this.PwrLvlTrkBr);
            this.AntTab.Controls.Add(this.label11);
            resources.ApplyResources(this.AntTab, "AntTab");
            this.AntTab.Name = "AntTab";
            // 
            // label53
            // 
            resources.ApplyResources(this.label53, "label53");
            this.label53.Name = "label53";
            // 
            // txtHighValue
            // 
            resources.ApplyResources(this.txtHighValue, "txtHighValue");
            this.txtHighValue.Name = "txtHighValue";
            // 
            // label52
            // 
            resources.ApplyResources(this.label52, "label52");
            this.label52.Name = "label52";
            // 
            // txtMedValue
            // 
            resources.ApplyResources(this.txtMedValue, "txtMedValue");
            this.txtMedValue.Name = "txtMedValue";
            // 
            // label51
            // 
            resources.ApplyResources(this.label51, "label51");
            this.label51.Name = "label51";
            // 
            // txtLowValue
            // 
            resources.ApplyResources(this.txtLowValue, "txtLowValue");
            this.txtLowValue.Name = "txtLowValue";
            // 
            // NoLmtLbl
            // 
            resources.ApplyResources(this.NoLmtLbl, "NoLmtLbl");
            this.NoLmtLbl.Name = "NoLmtLbl";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // InvtryCycUD
            // 
            resources.ApplyResources(this.InvtryCycUD, "InvtryCycUD");
            this.InvtryCycUD.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.InvtryCycUD.Name = "InvtryCycUD";
            // 
            // DwellTimeUD
            // 
            this.DwellTimeUD.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            resources.ApplyResources(this.DwellTimeUD, "DwellTimeUD");
            this.DwellTimeUD.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.DwellTimeUD.Name = "DwellTimeUD";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // PwrLvlValLbl
            // 
            this.PwrLvlValLbl.ForeColor = System.Drawing.Color.Indigo;
            resources.ApplyResources(this.PwrLvlValLbl, "PwrLvlValLbl");
            this.PwrLvlValLbl.Name = "PwrLvlValLbl";
            // 
            // PwrLvlTrkBr
            // 
            this.PwrLvlTrkBr.LargeChange = 1;
            resources.ApplyResources(this.PwrLvlTrkBr, "PwrLvlTrkBr");
            this.PwrLvlTrkBr.Maximum = 3;
            this.PwrLvlTrkBr.Minimum = 1;
            this.PwrLvlTrkBr.Name = "PwrLvlTrkBr";
            this.PwrLvlTrkBr.Value = 2;
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // TagSelTab
            // 
            this.TagSelTab.BackColor = System.Drawing.Color.White;
            this.TagSelTab.Controls.Add(this.SelCrit3Lnk);
            this.TagSelTab.Controls.Add(this.SelCrit2Lnk);
            this.TagSelTab.Controls.Add(this.SelCrit1Lnk);
            this.TagSelTab.Controls.Add(this.SelCrit0Lnk);
            this.TagSelTab.Controls.Add(this.label17);
            this.TagSelTab.Controls.Add(this.SelCritCntUD);
            resources.ApplyResources(this.TagSelTab, "TagSelTab");
            this.TagSelTab.Name = "TagSelTab";
            // 
            // SelCrit3Lnk
            // 
            resources.ApplyResources(this.SelCrit3Lnk, "SelCrit3Lnk");
            this.SelCrit3Lnk.Name = "SelCrit3Lnk";
            // 
            // SelCrit2Lnk
            // 
            resources.ApplyResources(this.SelCrit2Lnk, "SelCrit2Lnk");
            this.SelCrit2Lnk.Name = "SelCrit2Lnk";
            // 
            // SelCrit1Lnk
            // 
            resources.ApplyResources(this.SelCrit1Lnk, "SelCrit1Lnk");
            this.SelCrit1Lnk.Name = "SelCrit1Lnk";
            // 
            // SelCrit0Lnk
            // 
            resources.ApplyResources(this.SelCrit0Lnk, "SelCrit0Lnk");
            this.SelCrit0Lnk.Name = "SelCrit0Lnk";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // SelCritCntUD
            // 
            resources.ApplyResources(this.SelCritCntUD, "SelCritCntUD");
            this.SelCritCntUD.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SelCritCntUD.Name = "SelCritCntUD";
            this.SelCritCntUD.ReadOnly = true;
            // 
            // QryPrmTab
            // 
            resources.ApplyResources(this.QryPrmTab, "QryPrmTab");
            this.QryPrmTab.BackColor = System.Drawing.Color.White;
            this.QryPrmTab.Controls.Add(this.DQPanel);
            this.QryPrmTab.Controls.Add(this.FQPanel);
            this.QryPrmTab.Controls.Add(this.DQButton);
            this.QryPrmTab.Controls.Add(this.label1);
            this.QryPrmTab.Controls.Add(this.FQButton);
            this.QryPrmTab.Controls.Add(this.TagGrpPanel);
            this.QryPrmTab.Name = "QryPrmTab";
            // 
            // DQPanel
            // 
            this.DQPanel.BackColor = System.Drawing.Color.White;
            this.DQPanel.Controls.Add(this.DQMxQryCntUD);
            this.DQPanel.Controls.Add(this.label5);
            this.DQPanel.Controls.Add(this.QMxLbl);
            this.DQPanel.Controls.Add(this.MaxQTrkBr);
            this.DQPanel.Controls.Add(this.QMnLbl);
            this.DQPanel.Controls.Add(this.MinQTrkBr);
            this.DQPanel.Controls.Add(this.label9);
            this.DQPanel.Controls.Add(this.label8);
            this.DQPanel.Controls.Add(this.label7);
            this.DQPanel.Controls.Add(this.DQRetryCntUD);
            this.DQPanel.Controls.Add(this.label4);
            this.DQPanel.Controls.Add(this.QStLbl);
            this.DQPanel.Controls.Add(this.StartQTrkBr);
            this.DQPanel.Controls.Add(this.label6);
            resources.ApplyResources(this.DQPanel, "DQPanel");
            this.DQPanel.Name = "DQPanel";
            // 
            // DQMxQryCntUD
            // 
            resources.ApplyResources(this.DQMxQryCntUD, "DQMxQryCntUD");
            this.DQMxQryCntUD.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.DQMxQryCntUD.Name = "DQMxQryCntUD";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // QMxLbl
            // 
            this.QMxLbl.ForeColor = System.Drawing.Color.Indigo;
            resources.ApplyResources(this.QMxLbl, "QMxLbl");
            this.QMxLbl.Name = "QMxLbl";
            // 
            // MaxQTrkBr
            // 
            this.MaxQTrkBr.LargeChange = 1;
            resources.ApplyResources(this.MaxQTrkBr, "MaxQTrkBr");
            this.MaxQTrkBr.Maximum = 15;
            this.MaxQTrkBr.Name = "MaxQTrkBr";
            this.MaxQTrkBr.Value = 1;
            // 
            // QMnLbl
            // 
            this.QMnLbl.ForeColor = System.Drawing.Color.Indigo;
            resources.ApplyResources(this.QMnLbl, "QMnLbl");
            this.QMnLbl.Name = "QMnLbl";
            // 
            // MinQTrkBr
            // 
            this.MinQTrkBr.LargeChange = 1;
            resources.ApplyResources(this.MinQTrkBr, "MinQTrkBr");
            this.MinQTrkBr.Maximum = 15;
            this.MinQTrkBr.Name = "MinQTrkBr";
            this.MinQTrkBr.Value = 1;
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // DQRetryCntUD
            // 
            resources.ApplyResources(this.DQRetryCntUD, "DQRetryCntUD");
            this.DQRetryCntUD.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.DQRetryCntUD.Name = "DQRetryCntUD";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // QStLbl
            // 
            this.QStLbl.ForeColor = System.Drawing.Color.Indigo;
            resources.ApplyResources(this.QStLbl, "QStLbl");
            this.QStLbl.Name = "QStLbl";
            // 
            // StartQTrkBr
            // 
            this.StartQTrkBr.LargeChange = 1;
            resources.ApplyResources(this.StartQTrkBr, "StartQTrkBr");
            this.StartQTrkBr.Maximum = 15;
            this.StartQTrkBr.Name = "StartQTrkBr";
            this.StartQTrkBr.Value = 1;
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // FQPanel
            // 
            this.FQPanel.BackColor = System.Drawing.Color.White;
            this.FQPanel.Controls.Add(this.FQRetryCntUD);
            this.FQPanel.Controls.Add(this.label3);
            this.FQPanel.Controls.Add(this.RptChkBx);
            this.FQPanel.Controls.Add(this.QSzLbl);
            this.FQPanel.Controls.Add(this.FQSzTrkBr);
            this.FQPanel.Controls.Add(this.label2);
            resources.ApplyResources(this.FQPanel, "FQPanel");
            this.FQPanel.Name = "FQPanel";
            // 
            // FQRetryCntUD
            // 
            resources.ApplyResources(this.FQRetryCntUD, "FQRetryCntUD");
            this.FQRetryCntUD.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.FQRetryCntUD.Name = "FQRetryCntUD";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // RptChkBx
            // 
            resources.ApplyResources(this.RptChkBx, "RptChkBx");
            this.RptChkBx.Name = "RptChkBx";
            // 
            // QSzLbl
            // 
            this.QSzLbl.ForeColor = System.Drawing.Color.Indigo;
            resources.ApplyResources(this.QSzLbl, "QSzLbl");
            this.QSzLbl.Name = "QSzLbl";
            // 
            // FQSzTrkBr
            // 
            this.FQSzTrkBr.LargeChange = 1;
            resources.ApplyResources(this.FQSzTrkBr, "FQSzTrkBr");
            this.FQSzTrkBr.Maximum = 15;
            this.FQSzTrkBr.Name = "FQSzTrkBr";
            this.FQSzTrkBr.Value = 1;
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // DQButton
            // 
            this.DQButton.Checked = true;
            resources.ApplyResources(this.DQButton, "DQButton");
            this.DQButton.Name = "DQButton";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // FQButton
            // 
            resources.ApplyResources(this.FQButton, "FQButton");
            this.FQButton.Name = "FQButton";
            this.FQButton.TabStop = false;
            // 
            // TagGrpPanel
            // 
            this.TagGrpPanel.BackColor = System.Drawing.Color.White;
            this.TagGrpPanel.Controls.Add(this.SLAllButton);
            this.TagGrpPanel.Controls.Add(this.SLOffButton);
            this.TagGrpPanel.Controls.Add(this.SLOnButton);
            this.TagGrpPanel.Controls.Add(this.label18);
            this.TagGrpPanel.Controls.Add(this.ToggleTgtChkBx);
            this.TagGrpPanel.Controls.Add(this.SessValCmbBx);
            this.TagGrpPanel.Controls.Add(this.SessCmbBx);
            this.TagGrpPanel.Controls.Add(this.label16);
            this.TagGrpPanel.Controls.Add(this.label15);
            resources.ApplyResources(this.TagGrpPanel, "TagGrpPanel");
            this.TagGrpPanel.Name = "TagGrpPanel";
            // 
            // SLAllButton
            // 
            resources.ApplyResources(this.SLAllButton, "SLAllButton");
            this.SLAllButton.Name = "SLAllButton";
            this.SLAllButton.TabStop = false;
            // 
            // SLOffButton
            // 
            resources.ApplyResources(this.SLOffButton, "SLOffButton");
            this.SLOffButton.Name = "SLOffButton";
            this.SLOffButton.TabStop = false;
            // 
            // SLOnButton
            // 
            resources.ApplyResources(this.SLOnButton, "SLOnButton");
            this.SLOnButton.Name = "SLOnButton";
            this.SLOnButton.TabStop = false;
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // ToggleTgtChkBx
            // 
            resources.ApplyResources(this.ToggleTgtChkBx, "ToggleTgtChkBx");
            this.ToggleTgtChkBx.Name = "ToggleTgtChkBx";
            // 
            // SessValCmbBx
            // 
            this.SessValCmbBx.Items.Add(resources.GetString("SessValCmbBx.Items"));
            this.SessValCmbBx.Items.Add(resources.GetString("SessValCmbBx.Items1"));
            resources.ApplyResources(this.SessValCmbBx, "SessValCmbBx");
            this.SessValCmbBx.Name = "SessValCmbBx";
            // 
            // SessCmbBx
            // 
            this.SessCmbBx.Items.Add(resources.GetString("SessCmbBx.Items"));
            this.SessCmbBx.Items.Add(resources.GetString("SessCmbBx.Items1"));
            this.SessCmbBx.Items.Add(resources.GetString("SessCmbBx.Items2"));
            this.SessCmbBx.Items.Add(resources.GetString("SessCmbBx.Items3"));
            resources.ApplyResources(this.SessCmbBx, "SessCmbBx");
            this.SessCmbBx.Name = "SessCmbBx";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // OvrHtProtTab
            // 
            this.OvrHtProtTab.BackColor = System.Drawing.Color.White;
            this.OvrHtProtTab.Controls.Add(this.label35);
            this.OvrHtProtTab.Controls.Add(this.label36);
            this.OvrHtProtTab.Controls.Add(this.label37);
            this.OvrHtProtTab.Controls.Add(this.XcvrTempUD);
            this.OvrHtProtTab.Controls.Add(this.label38);
            this.OvrHtProtTab.Controls.Add(this.label39);
            this.OvrHtProtTab.Controls.Add(this.OnfrvrLbl);
            this.OvrHtProtTab.Controls.Add(this.DCOffUD);
            this.OvrHtProtTab.Controls.Add(this.DCOnUD);
            this.OvrHtProtTab.Controls.Add(this.label42);
            this.OvrHtProtTab.Controls.Add(this.label41);
            this.OvrHtProtTab.Controls.Add(this.label40);
            resources.ApplyResources(this.OvrHtProtTab, "OvrHtProtTab");
            this.OvrHtProtTab.Name = "OvrHtProtTab";
            // 
            // label35
            // 
            resources.ApplyResources(this.label35, "label35");
            this.label35.Name = "label35";
            // 
            // label36
            // 
            resources.ApplyResources(this.label36, "label36");
            this.label36.Name = "label36";
            // 
            // label37
            // 
            resources.ApplyResources(this.label37, "label37");
            this.label37.Name = "label37";
            // 
            // XcvrTempUD
            // 
            resources.ApplyResources(this.XcvrTempUD, "XcvrTempUD");
            this.XcvrTempUD.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.XcvrTempUD.Minimum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.XcvrTempUD.Name = "XcvrTempUD";
            this.XcvrTempUD.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // label38
            // 
            resources.ApplyResources(this.label38, "label38");
            this.label38.Name = "label38";
            // 
            // label39
            // 
            resources.ApplyResources(this.label39, "label39");
            this.label39.Name = "label39";
            // 
            // OnfrvrLbl
            // 
            resources.ApplyResources(this.OnfrvrLbl, "OnfrvrLbl");
            this.OnfrvrLbl.Name = "OnfrvrLbl";
            // 
            // DCOffUD
            // 
            resources.ApplyResources(this.DCOffUD, "DCOffUD");
            this.DCOffUD.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.DCOffUD.Name = "DCOffUD";
            // 
            // DCOnUD
            // 
            resources.ApplyResources(this.DCOnUD, "DCOnUD");
            this.DCOnUD.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.DCOnUD.Name = "DCOnUD";
            // 
            // label42
            // 
            resources.ApplyResources(this.label42, "label42");
            this.label42.Name = "label42";
            // 
            // label41
            // 
            resources.ApplyResources(this.label41, "label41");
            this.label41.Name = "label41";
            // 
            // label40
            // 
            resources.ApplyResources(this.label40, "label40");
            this.label40.Name = "label40";
            // 
            // TempTab
            // 
            this.TempTab.BackColor = System.Drawing.Color.White;
            this.TempTab.Controls.Add(this.DeltaTempLbl);
            this.TempTab.Controls.Add(this.PATempLbl);
            this.TempTab.Controls.Add(this.AmbTempLbl);
            this.TempTab.Controls.Add(this.XcvrTempLbl);
            this.TempTab.Controls.Add(this.label25);
            this.TempTab.Controls.Add(this.label24);
            this.TempTab.Controls.Add(this.label23);
            this.TempTab.Controls.Add(this.label22);
            this.TempTab.Controls.Add(this.label21);
            this.TempTab.Controls.Add(this.label20);
            resources.ApplyResources(this.TempTab, "TempTab");
            this.TempTab.Name = "TempTab";
            // 
            // DeltaTempLbl
            // 
            resources.ApplyResources(this.DeltaTempLbl, "DeltaTempLbl");
            this.DeltaTempLbl.Name = "DeltaTempLbl";
            // 
            // PATempLbl
            // 
            resources.ApplyResources(this.PATempLbl, "PATempLbl");
            this.PATempLbl.Name = "PATempLbl";
            // 
            // AmbTempLbl
            // 
            resources.ApplyResources(this.AmbTempLbl, "AmbTempLbl");
            this.AmbTempLbl.Name = "AmbTempLbl";
            // 
            // XcvrTempLbl
            // 
            resources.ApplyResources(this.XcvrTempLbl, "XcvrTempLbl");
            this.XcvrTempLbl.Name = "XcvrTempLbl";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.Name = "label25";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.Name = "label24";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.Name = "label23";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // ServiceSettings
            // 
            this.ServiceSettings.BackColor = System.Drawing.Color.White;
            this.ServiceSettings.Controls.Add(this.label50);
            this.ServiceSettings.Controls.Add(this.txtMsgDisplayTimeOut);
            this.ServiceSettings.Controls.Add(this.label49);
            this.ServiceSettings.Controls.Add(this.rbErrLogDisable);
            this.ServiceSettings.Controls.Add(this.rbErrLogEnable);
            this.ServiceSettings.Controls.Add(this.label48);
            this.ServiceSettings.Controls.Add(this.txtPageSize);
            this.ServiceSettings.Controls.Add(this.label47);
            this.ServiceSettings.Controls.Add(this.textBox1);
            this.ServiceSettings.Controls.Add(this.label46);
            resources.ApplyResources(this.ServiceSettings, "ServiceSettings");
            this.ServiceSettings.Name = "ServiceSettings";
            // 
            // label50
            // 
            resources.ApplyResources(this.label50, "label50");
            this.label50.Name = "label50";
            // 
            // txtMsgDisplayTimeOut
            // 
            resources.ApplyResources(this.txtMsgDisplayTimeOut, "txtMsgDisplayTimeOut");
            this.txtMsgDisplayTimeOut.Name = "txtMsgDisplayTimeOut";
            // 
            // label49
            // 
            resources.ApplyResources(this.label49, "label49");
            this.label49.Name = "label49";
            // 
            // rbErrLogDisable
            // 
            resources.ApplyResources(this.rbErrLogDisable, "rbErrLogDisable");
            this.rbErrLogDisable.Name = "rbErrLogDisable";
            // 
            // rbErrLogEnable
            // 
            resources.ApplyResources(this.rbErrLogEnable, "rbErrLogEnable");
            this.rbErrLogEnable.Name = "rbErrLogEnable";
            // 
            // label48
            // 
            resources.ApplyResources(this.label48, "label48");
            this.label48.Name = "label48";
            // 
            // txtPageSize
            // 
            resources.ApplyResources(this.txtPageSize, "txtPageSize");
            this.txtPageSize.Name = "txtPageSize";
            // 
            // label47
            // 
            resources.ApplyResources(this.label47, "label47");
            this.label47.Name = "label47";
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            // 
            // label46
            // 
            resources.ApplyResources(this.label46, "label46");
            this.label46.Name = "label46";
            // 
            // tabBarcodeSettings
            // 
            this.tabBarcodeSettings.BackColor = System.Drawing.Color.White;
            this.tabBarcodeSettings.Controls.Add(this.rb2DScanner);
            this.tabBarcodeSettings.Controls.Add(this.rb1DScanner);
            resources.ApplyResources(this.tabBarcodeSettings, "tabBarcodeSettings");
            this.tabBarcodeSettings.Name = "tabBarcodeSettings";
            // 
            // rb2DScanner
            // 
            resources.ApplyResources(this.rb2DScanner, "rb2DScanner");
            this.rb2DScanner.Name = "rb2DScanner";
            // 
            // rb1DScanner
            // 
            resources.ApplyResources(this.rb1DScanner, "rb1DScanner");
            this.rb1DScanner.Name = "rb1DScanner";
            // 
            // DevStatForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.TabCtrl);
            this.Controls.Add(this.RstButton);
            this.Controls.Add(this.ApplyButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DevStatForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.TabCtrl.ResumeLayout(false);
            this.InvtrySetupTab.ResumeLayout(false);
            this.TagAttrTab.ResumeLayout(false);
            this.FreqTab.ResumeLayout(false);
            this.LBTPanel.ResumeLayout(false);
            this.LnkPrfTab.ResumeLayout(false);
            this.AntTab.ResumeLayout(false);
            this.TagSelTab.ResumeLayout(false);
            this.QryPrmTab.ResumeLayout(false);
            this.DQPanel.ResumeLayout(false);
            this.FQPanel.ResumeLayout(false);
            this.TagGrpPanel.ResumeLayout(false);
            this.OvrHtProtTab.ResumeLayout(false);
            this.TempTab.ResumeLayout(false);
            this.ServiceSettings.ResumeLayout(false);
            this.tabBarcodeSettings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RstButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.TabControl TabCtrl;
        private System.Windows.Forms.TabPage InvtrySetupTab;
        private System.Windows.Forms.RadioButton FixedQBttn;
        private System.Windows.Forms.RadioButton DynQBttn;
        private System.Windows.Forms.Label QLbl;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TextBox PopSzTxtBx;
        private EPCMaskInput MaskInput;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.ComboBox InvtrySessCmbBx;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TabPage TagAttrTab;
        private System.Windows.Forms.ComboBox TagVndrCmbBx;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label UsrBnkSzLbl;
        private System.Windows.Forms.Label TIDBnkSzLbl;
        private System.Windows.Forms.NumericUpDown UserMemSzUD;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.NumericUpDown TIDMemSzUD;
        private System.Windows.Forms.CheckBox TagUserMemChkBx;
        private System.Windows.Forms.CheckBox TagTIDChkBx;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TabPage FreqTab;
        private System.Windows.Forms.Label FreqBndNumLbl;
        private System.Windows.Forms.LinkLabel SingleChnLnkLbl;
        private System.Windows.Forms.CheckBox SingleChnChkBx;
        private System.Windows.Forms.Panel LBTPanel;
        private System.Windows.Forms.RadioButton LBTNoBttn;
        private System.Windows.Forms.RadioButton LBTYesBttn;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label CntryFreqRangeLbl;
        private System.Windows.Forms.ComboBox FreqCntryCmbBx;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label BandFrqRangeLbl;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox FreqBndCmbBx;
        private System.Windows.Forms.TabPage LnkPrfTab;
        private System.Windows.Forms.LinkLabel LnkPrfMdfyDetailsLnk;
        private System.Windows.Forms.Label LnkPrfDetailHdrLbl;
        private System.Windows.Forms.LinkLabel LnkPrfDetailsLnk;
        private System.Windows.Forms.Label LnkPrfDetailLbl;
        private System.Windows.Forms.RadioButton LnkPrf5Button;
        private System.Windows.Forms.RadioButton LnkPrf4Button;
        private System.Windows.Forms.RadioButton LnkPrf3Button;
        private System.Windows.Forms.RadioButton LnkPrf2Button;
        private System.Windows.Forms.RadioButton LnkPrf1Button;
        private System.Windows.Forms.RadioButton LnkPrf0Button;
        private System.Windows.Forms.TabPage AntTab;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtHighValue;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtMedValue;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox txtLowValue;
        private System.Windows.Forms.Label NoLmtLbl;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown InvtryCycUD;
        private System.Windows.Forms.NumericUpDown DwellTimeUD;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label PwrLvlValLbl;
        private System.Windows.Forms.TrackBar PwrLvlTrkBr;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage TagSelTab;
        private System.Windows.Forms.LinkLabel SelCrit3Lnk;
        private System.Windows.Forms.LinkLabel SelCrit2Lnk;
        private System.Windows.Forms.LinkLabel SelCrit1Lnk;
        private System.Windows.Forms.LinkLabel SelCrit0Lnk;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown SelCritCntUD;
        private System.Windows.Forms.TabPage QryPrmTab;
        private System.Windows.Forms.Panel DQPanel;
        private System.Windows.Forms.NumericUpDown DQMxQryCntUD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label QMxLbl;
        private System.Windows.Forms.TrackBar MaxQTrkBr;
        private System.Windows.Forms.Label QMnLbl;
        private System.Windows.Forms.TrackBar MinQTrkBr;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown DQRetryCntUD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label QStLbl;
        private System.Windows.Forms.TrackBar StartQTrkBr;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel FQPanel;
        private System.Windows.Forms.NumericUpDown FQRetryCntUD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox RptChkBx;
        private System.Windows.Forms.Label QSzLbl;
        private System.Windows.Forms.TrackBar FQSzTrkBr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton DQButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton FQButton;
        private System.Windows.Forms.Panel TagGrpPanel;
        private System.Windows.Forms.RadioButton SLAllButton;
        private System.Windows.Forms.RadioButton SLOffButton;
        private System.Windows.Forms.RadioButton SLOnButton;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox ToggleTgtChkBx;
        private System.Windows.Forms.ComboBox SessValCmbBx;
        private System.Windows.Forms.ComboBox SessCmbBx;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage OvrHtProtTab;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.NumericUpDown XcvrTempUD;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label OnfrvrLbl;
        private System.Windows.Forms.NumericUpDown DCOffUD;
        private System.Windows.Forms.NumericUpDown DCOnUD;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TabPage TempTab;
        private System.Windows.Forms.Label DeltaTempLbl;
        private System.Windows.Forms.Label PATempLbl;
        private System.Windows.Forms.Label AmbTempLbl;
        private System.Windows.Forms.Label XcvrTempLbl;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage ServiceSettings;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtMsgDisplayTimeOut;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.RadioButton rbErrLogDisable;
        private System.Windows.Forms.RadioButton rbErrLogEnable;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtPageSize;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabBarcodeSettings;
        private System.Windows.Forms.RadioButton rb2DScanner;
        private System.Windows.Forms.RadioButton rb1DScanner;
    }
}