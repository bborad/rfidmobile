/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Add Asset functionality
 **************************************************************************************/
using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmAddAsset : Form
    {
        public bool newAsset;

       public enum FormType
        {
            Add,
            OverWiteTag
        };
        private FormType _OpenMode;

        public FormType OpenMode
        {
            get 
            {
                return _OpenMode;
            }
            set
            {
                _OpenMode = value;
            }
        }

        private string _OldAssetNo;

        public string OldAssetNo
        {
            get { return _OldAssetNo; }
            set { _OldAssetNo = value; }
        }

        public frmAddAsset()
        {
            InitializeComponent();
            newAsset = true;
        }

        private string _LocationID;

        public String LocationID
        {
            get
            {
                return _LocationID;
            }
            set
            {
                _LocationID = value;
               // cboLoc.SelectedValue = Convert.ToInt32(value);
            }
        }

        private string _Description;

        public String Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
                txtdesc.Text = value;
            }
        }

        public String AssetName
        {
            set
            {
                txtName.Text = value;
            }
        }

        public String RefNo
        {
            set
            {
                txtRefNo.Text = value;
            }
        }

        public Int32 ServerKey { get; set; }
         

        public String TagNo
        {
            set
            {
                txtTag.Text = value;
            }
        }
        private void txtLoc_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!(txtTag.Text.Trim().Length <= 24 && txtTag.Text.Trim().Length >= 0))
            {
                MessageBox.Show("Invalid Tag No.");
                return;
            }
            //if (Convert.ToInt32(cboLoc.SelectedValue) == 0)
            //{
            //    MessageBox.Show("Please Select Location");
            //    return;
            //}

            try
            {
                if (newAsset == true)
                {
                    Assets.AddAsset(txtTag.Text.Trim(), txtName.Text.Trim(), txtName.Text.Trim(),0/* Convert.ToInt32(cboLoc.SelectedValue)*/, txtdesc.Text.Trim(), txtRefNo.Text.Trim());
                }
                else
                {
                    Assets.EditAsset(txtTag.Text.Trim(), txtName.Text.Trim(), txtName.Text.Trim(), 0/* Convert.ToInt32(cboLoc.SelectedValue)*/, ServerKey, txtdesc.Text.Trim(), txtRefNo.Text.Trim());
                }

                MessageBox.Show("Item Sucessfully Saved.");
                this.Close();
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");

                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {

                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
           
        }

        private void cboLoc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAddAsset_Load(object sender, EventArgs e)
        {
            //Set Location Combo
            DataTable dtList = new DataTable();
            dtList = Locations.getLocationList();

            DataRow dr = dtList.NewRow();
            dr["ID_location"] = 0;
            dr["Name"] = "Select Location";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            //cboLoc.ValueMember = "ID_Location";
            //cboLoc.DisplayMember = "Name";
            //cboLoc.DataSource = dtList;
            
            //cboLoc.SelectedValue = 0;

            //DataTable dtStatus = AssetStatus.GetAssetStatus();

            //cbAssetStatus.ValueMember = "ID_AssetStatus";
            //cbAssetStatus.DisplayMember = "Status";
            //cbAssetStatus.DataSource = dtStatus;
            

            if (newAsset == false)
            {
                txtTag.Enabled = false;
               // cboLoc.SelectedValue = Convert.ToInt32(LocationID);
                txtdesc.Text = Description;
            }

        }


    }
}