using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlServerCe;

namespace OnRamp
{
    public partial class DBDataViewForm : Form
    {
        // Allow user to Update Rows in the DataTable
        public delegate void UpdateRows (DataTable dtTbl);

        private DataTable ConsolidatedTbl = null;
        private DataTable ChangesOnlyTbl = null; // Note: could be NULL

        String DBFile = String.Empty;
        SqlCeCommand DBSelCmd = null;
        SqlCeCommand DBCreateTblCmd = null;

        public DBDataViewForm(DataSet dataSet, String tblName, 
            SqlCeCommand selCmd, String dataFile, DBPref.DBFileFmt fileFmt)
        {
            InitializeComponent();

            switch (fileFmt)
            {
                case DBPref.DBFileFmt.CSV:
                case DBPref.DBFileFmt.XML:
                    // Display Error Label
                    DisplayErrLabel("Unsupported Data File Format");
                    break;
                case DBPref.DBFileFmt.MSSQLCE_SDF:
                    if (! File.Exists(dataFile))
                    {
                        DisplayErrLabel(dataFile + " does not exist");
                    }
                    else if (! dataSet.Tables.Contains (tblName))
                    {
                        DisplayErrLabel("Database Table  '" + tblName + "' does not exist");
                    }
                    else 
                    {
                        try
                        {
                            DataTable DataTbl = dataSet.Tables[tblName];
                            if (FillDataTable(DataTbl, selCmd, dataFile))
                            {
                                DtGrd.DataSource = DataTbl;
                                SetColumnWidths(DataTbl);
                            }
                        }
                        catch (Exception ex)
                        {
                            DisplayErrLabel("Exception (" + ex.GetBaseException().GetType().Name 
                                + ") when Binding DataTable to Grid:" + "\n"
                                + ex.Message);
                        }
                    }
                    break;
            }
        }

        public DBDataViewForm(DataSet dataSet, String tblName,
            SqlCeCommand selCmd, SqlCeCommand createTblCmd, 
            String dataFile, DBPref.DBFileFmt fileFmt,
            UpdateRows callerUpdateRows)
        {
            InitializeComponent();

            // Show Buttons
            DtGrd.Height =ChngsOnlyBttn.Location.Y - 1;
            ChngsOnlyBttn.Visible = true;
            VwAllBttn.Visible = true;
            AccptBttn.Visible = true;

            switch (fileFmt)
            {
                case DBPref.DBFileFmt.CSV:
                case DBPref.DBFileFmt.XML:
                    // Display Error Label
                    DisplayErrLabel("Unsupported Data File Format");
                    break;
                case DBPref.DBFileFmt.MSSQLCE_SDF:
                    if (!dataSet.Tables.Contains(tblName))
                    {
                        DisplayErrLabel("Database Table  '" + tblName + "' does not exist");
                    }
                    else
                    {
                        try
                        {
                            DataTable DataTbl = dataSet.Tables[tblName];
                            if (!File.Exists(dataFile))
                            {
                                DataTbl.Clear(); // New Table
                            }
                            else
                            {
                                FillDataTable(DataTbl, selCmd, dataFile);
                            }
                            // Caller Add/Update rows in the table before display
                            if (callerUpdateRows != null)
                                callerUpdateRows(DataTbl);

                            DtGrd.DataSource = DataTbl;

                            SetColumnWidths(DataTbl);

                            DBFile = dataFile;
                            DBCreateTblCmd = createTblCmd;
                            DBSelCmd = selCmd;
                            ConsolidatedTbl = DataTbl; // Save it
                            ChangesOnlyTbl = DataTbl.GetChanges();
                            if (ChangesOnlyTbl != null) // Changes are made
                                EnableButtons(); // Buttons are disabled on load (until DataTbl is setup)      
                            else
                                Program.ShowWarning("There are no changes to the DataTable");
                        }
                        catch (Exception ex)
                        {
                            DisplayErrLabel("Exception (" + ex.GetBaseException().GetType().Name
                                + ") when Binding DataTable to Grid:" + "\n"
                                + ex.Message);
                        }
                    }
                    break;
            }
        }

        private void EnableButtons()
        {
            VwAllBttn.Enabled = true;
            ChngsOnlyBttn.Enabled = true;
            AccptBttn.Enabled = true;
        }

        private bool CreateDataBase(String FileName)
        {
            SqlCeEngine engine = new SqlCeEngine(DBConnString(FileName));
            engine.CreateDatabase();

            return true;
        }

        private bool CreateTable(SqlCeCommand creatTblCmd, String FileName)
        {
            bool Succ = false;

            SqlCeCommand Cmd = creatTblCmd;
            Cmd.Connection = new SqlCeConnection(DBConnString(FileName));
            try
            {
                Cmd.Connection.Open();
                Cmd.ExecuteNonQuery();
                Succ = true;
            }
            catch (InvalidOperationException ioe)
            {
                Succ = false;
                Program.ShowError("Error Creating DB Table:\n" + ioe.Message);
            }
            catch (SqlCeException sqcee)
            {
                Succ = false;
                Program.ShowError("Error Creating DB Table:\n" + sqcee.Message);
            }
            finally
            {
                Cmd.Connection.Close();
            }

            return Succ;
        }

        private bool CreateDataTable(SqlCeCommand creatTblCmd, String FileName)
        {
            // This would create the file also
            CreateDataBase(FileName);
            // Build Table?
            if (CreateTable(creatTblCmd, FileName) == false)
            {
                // Delete Database
                File.Delete(FileName);
                return false;
            }

            return true;
        }

        private bool UpdateDataTable(DataTable dataTbl, SqlCeCommand selCmd, String FileName, 
            out int numRowsUpdated)
        {
           bool Succ = false;
           numRowsUpdated = 0;

            SqlCeConnection DbConn = null;
            try
            {
                DbConn = new SqlCeConnection(DBConnString(FileName));
                DbConn.Open();
                SqlCeDataAdapter DbAdptr = new SqlCeDataAdapter(selCmd);
                SqlCeCommandBuilder CmdBldr = new SqlCeCommandBuilder(DbAdptr); // enough?
                selCmd.Connection = DbConn;
                numRowsUpdated = DbAdptr.Update(dataTbl);
                
                Succ = true;
            }
            catch (InvalidOperationException ioe)
            {
                DisplayErrLabel("Invalid Operation Exception:\n" + ioe.Message);
            }
            catch (DBConcurrencyException cone)
            {
                DisplayErrLabel("DB Concurrency Exception:\n" + cone.Message);
            }
            catch (SqlCeException sqlcee)
            {
                DisplayErrLabel("SQLCE Exception:\n" + sqlcee.Message);
            }
            catch (Exception ex)
            {
                DisplayErrLabel(ex.GetBaseException().GetType().Name + ":\n" + ex.Message);
            }
            finally
            {
                if (DbConn != null)
                    DbConn.Close();
            }

            return Succ;

        }

        private bool FillDataTable (DataTable dataTbl, SqlCeCommand selCmd,  String FileName)
        {
            bool Succ = false;

            SqlCeConnection DbConn = null;
            try
            {
                DbConn = new SqlCeConnection(DBConnString(FileName));
                DbConn.Open();
                SqlCeDataAdapter DbAdptr = new SqlCeDataAdapter(selCmd);
                selCmd.Connection = DbConn;
                DbAdptr.Fill(dataTbl);
                Succ = true;
            }
            catch (InvalidOperationException ioe)
            {
                DisplayErrLabel("Invalid Operation Exception:\n" + ioe.Message);
            }
            catch (DBConcurrencyException cone)
            {
                DisplayErrLabel("DB Concurrency Exception:\n" + cone.Message);
            }
            catch (SqlCeException sqlcee)
            {
                DisplayErrLabel("SQLCE Exception:\n" + sqlcee.Message);
            }
            catch (Exception ex)
            {
                DisplayErrLabel(ex.GetBaseException().GetType().Name + ":\n" + ex.Message);
            }
            finally
            {
                if (DbConn != null)
                    DbConn.Close();
            }

            return Succ;
        }

        private String DBConnString(String FileName)
        {
            // more options?
            return "DataSource='" + FileName + "'";
        }

        private void DisplayErrLabel(String errMsg)
        {
            // Hide DataGrid
            DtGrd.Visible = false;
            Label ErrMsgLbl = new Label();
            ErrMsgLbl.Location = DtGrd.Location;
            ErrMsgLbl.Size = DtGrd.Size;
            ErrMsgLbl.ForeColor = Color.Red;
            this.Controls.Add (ErrMsgLbl);
            ErrMsgLbl.Text = errMsg;
        }

        #region Column Widths

        // maxTxtLen only relevant when Type is Char or String?
        private int DataColSuggestedTextLength(Type dataType, int maxTxtLen)
        {
            int NumChars = 0;
            if (maxTxtLen > 0)
                NumChars = maxTxtLen;
            else if (dataType.IsArray)
            {
                NumChars = -1;
            }
            else
            {
                if (dataType == typeof(Boolean))
                {
                    NumChars = 5;
                }
                else if (dataType == typeof(DateTime))
                {
                    NumChars = 20;
                }
                else if (dataType == typeof(Byte)
               || dataType == typeof(SByte)
               || dataType == typeof(Char))
                {
                    NumChars = 1;
                }
                else
                {/*
                    case Decimal:
                    case Int16:
                    case Int32:
                    case Int64:
                    case UInt16:
                    case UInt32:
                    case UInt64:
                    case Single:
                    case Double:
                    case String:
                  */
                    NumChars = -1;
                }
            }
            return NumChars;
        }

        // Based on DataColumn MaxLength (if > 0), 
        private void SetColumnWidths(DataTable dataTbl)
        {
            Graphics g = DtGrd.CreateGraphics();
            int TypicalChrWid = (int)g.MeasureString("0", DtGrd.Font).Width;
            int SpaceChrWid = (int)g.MeasureString(" ", DtGrd.Font).Width;
            DataGridTableStyle TblStyle = new DataGridTableStyle();

            for (int i = 0; i < dataTbl.Columns.Count; i++)
            {
                DataColumn dataCol = dataTbl.Columns[i];
                int SuggNumChrs = DataColSuggestedTextLength(dataCol.DataType, dataCol.MaxLength);
                int DspyLen = TypicalChrWid * SuggNumChrs;
                DataGridColumnStyle ColStyle = new DataGridTextBoxColumn(); // only choice in CF
                ColStyle.HeaderText = dataCol.ColumnName;
                ColStyle.MappingName = dataCol.ColumnName;
                ColStyle.Width = (DspyLen > 0 ?
                     DspyLen : LongestField(dataTbl, dataCol))
                + SpaceChrWid; // space
                TblStyle.GridColumnStyles.Add(ColStyle);
            }
            TblStyle.MappingName = dataTbl.TableName;
            DtGrd.TableStyles.Add(TblStyle);

            g.Dispose();
        }

        // This could be time-consuming if the number of rows is huge!
        private Int32 LongestField(DataTable dataTbl, DataColumn col)
        {
            Graphics g = DtGrd.CreateGraphics();
            int MaxLen = Convert.ToInt32(Math.Ceiling(g.MeasureString(col.ColumnName, DtGrd.Font).Width));
            for (int i = 0; i < dataTbl.Rows.Count; i++)
            {
                DataRow dataRow = dataTbl.Rows[i];
                MaxLen = Math.Max(
                    Convert.ToInt32(Math.Ceiling(g.MeasureString(dataRow[col].ToString(), DtGrd.Font).Width)),
                    MaxLen);
            }

            return MaxLen;
        }

        #endregion

        private void OnChngsOnlyBttnClicked(object sender, EventArgs e)
        {
            if (ChangesOnlyTbl.Rows.Count <= 0)
            {
                Program.ShowWarning("There are no changes");
            }
            else
            {
                DtGrd.DataSource = ChangesOnlyTbl;
            }
            
        }

        private void OnVwAllBttnClicked(object sender, EventArgs e)
        {
            DtGrd.DataSource = ConsolidatedTbl;
        }

        private void OnAccptBttnClicked(object sender, EventArgs e)
        {
            // Check for DataTable Errors in ChangesOnlyTbl (instantiate if not already did)
            if (ChangesOnlyTbl.Rows.Count <= 0)
            {
                Program.ShowWarning("There are no changes from existing table.\n"
                    + "No action to be taken");
                return;
            }
            if (ChangesOnlyTbl.HasErrors)
            {
                if (Program.AskUserConfirm("There are errors marked in table,\n"
                    + "Ignore?") != DialogResult.OK)
                {
                    Program.ShowWarning("User canceled");
                    return;
                }
            }
            else
            {
                if (Program.AskUserConfirm("Commit changes to '" + DBFile + "'.\n"
                    + "Proceed?") != DialogResult.OK)
                {
                    Program.ShowWarning("User canceled");
                    return;
                }
            }
            // Update ChangesOnlyTbl to DataAdaptor
                // Create New DB File if necessary
            if (File.Exists(DBFile) == false)
            {
                if (Program.AskUserConfirm("'" + DBFile + "' does not exist.\n"
                    + "Create?") != DialogResult.OK)
                {
                    Program.ShowWarning("User canceled");
                    return;
                }
                else
                {
                    if (CreateDataTable(DBCreateTblCmd, DBFile) == false)
                        return;
                }
            }
            // if success, Merge ChangesOnlyTbl to Consolidated Table, AcceptChanges
            // other wise, RejectChanges?
            int NumRowsUpdated;
            try
            {
                if (UpdateDataTable(ChangesOnlyTbl, DBSelCmd, DBFile, out NumRowsUpdated) == true)
                {
                    //ConsolidatedTbl.Merge(ChangesOnlyTbl); // really necessary?
                    ConsolidatedTbl.AcceptChanges();
                    Program.ShowSuccess("Successful completion!\n" + NumRowsUpdated + " rows changed.");
                    AccptBttn.Enabled = false; // Accept once is enough
                }
                else
                    ConsolidatedTbl.RejectChanges();
            }
            catch (ConstraintException ce) // could throw from Merge()
            {
                DisplayErrLabel("DataTable Merge with Exception caught:" + "\n"
                    + ce.Message);
            }
        }
    }
}