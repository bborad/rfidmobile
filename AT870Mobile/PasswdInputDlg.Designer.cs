namespace OnRamp
{
    partial class PasswdInputDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PasswdInputDlg));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.CurAccPWPanel = new System.Windows.Forms.Panel();
            this.CurAccPWTxtBx = new System.Windows.Forms.TextBox();
            this.CurKillPWPanel = new System.Windows.Forms.Panel();
            this.CurKillPWTxtBx = new System.Windows.Forms.TextBox();
            this.NewKillPWPanel = new System.Windows.Forms.Panel();
            this.NewKillPWTxtBx = new System.Windows.Forms.TextBox();
            this.NewAccPWPanel = new System.Windows.Forms.Panel();
            this.NewAccPWTxtBx = new System.Windows.Forms.TextBox();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.CurAccPWPanel.SuspendLayout();
            this.CurKillPWPanel.SuspendLayout();
            this.NewKillPWPanel.SuspendLayout();
            this.NewAccPWPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // CurAccPWPanel
            // 
            this.CurAccPWPanel.BackColor = System.Drawing.Color.White;
            this.CurAccPWPanel.Controls.Add(this.CurAccPWTxtBx);
            this.CurAccPWPanel.Controls.Add(this.label1);
            resources.ApplyResources(this.CurAccPWPanel, "CurAccPWPanel");
            this.CurAccPWPanel.Name = "CurAccPWPanel";
            // 
            // CurAccPWTxtBx
            // 
            resources.ApplyResources(this.CurAccPWTxtBx, "CurAccPWTxtBx");
            this.CurAccPWTxtBx.Name = "CurAccPWTxtBx";
            this.CurAccPWTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnTxtBxKeyPress);
            this.CurAccPWTxtBx.LostFocus += new System.EventHandler(this.OnTxtBxFocusLost);
            // 
            // CurKillPWPanel
            // 
            this.CurKillPWPanel.BackColor = System.Drawing.Color.White;
            this.CurKillPWPanel.Controls.Add(this.CurKillPWTxtBx);
            this.CurKillPWPanel.Controls.Add(this.label2);
            resources.ApplyResources(this.CurKillPWPanel, "CurKillPWPanel");
            this.CurKillPWPanel.Name = "CurKillPWPanel";
            // 
            // CurKillPWTxtBx
            // 
            resources.ApplyResources(this.CurKillPWTxtBx, "CurKillPWTxtBx");
            this.CurKillPWTxtBx.Name = "CurKillPWTxtBx";
            this.CurKillPWTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnTxtBxKeyPress);
            this.CurKillPWTxtBx.LostFocus += new System.EventHandler(this.OnTxtBxFocusLost);
            // 
            // NewKillPWPanel
            // 
            this.NewKillPWPanel.BackColor = System.Drawing.Color.White;
            this.NewKillPWPanel.Controls.Add(this.NewKillPWTxtBx);
            this.NewKillPWPanel.Controls.Add(this.label3);
            resources.ApplyResources(this.NewKillPWPanel, "NewKillPWPanel");
            this.NewKillPWPanel.Name = "NewKillPWPanel";
            // 
            // NewKillPWTxtBx
            // 
            resources.ApplyResources(this.NewKillPWTxtBx, "NewKillPWTxtBx");
            this.NewKillPWTxtBx.Name = "NewKillPWTxtBx";
            this.NewKillPWTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnTxtBxKeyPress);
            this.NewKillPWTxtBx.LostFocus += new System.EventHandler(this.OnTxtBxFocusLost);
            // 
            // NewAccPWPanel
            // 
            this.NewAccPWPanel.BackColor = System.Drawing.Color.White;
            this.NewAccPWPanel.Controls.Add(this.NewAccPWTxtBx);
            this.NewAccPWPanel.Controls.Add(this.label4);
            resources.ApplyResources(this.NewAccPWPanel, "NewAccPWPanel");
            this.NewAccPWPanel.Name = "NewAccPWPanel";
            // 
            // NewAccPWTxtBx
            // 
            resources.ApplyResources(this.NewAccPWTxtBx, "NewAccPWTxtBx");
            this.NewAccPWTxtBx.Name = "NewAccPWTxtBx";
            this.NewAccPWTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnTxtBxKeyPress);
            this.NewAccPWTxtBx.LostFocus += new System.EventHandler(this.OnTxtBxFocusLost);
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            this.OKBttn.Click += new System.EventHandler(this.OnOKBttnClicked);
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // PasswdInputDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.NewAccPWPanel);
            this.Controls.Add(this.NewKillPWPanel);
            this.Controls.Add(this.CurKillPWPanel);
            this.Controls.Add(this.CurAccPWPanel);
            this.Name = "PasswdInputDlg";
            this.CurAccPWPanel.ResumeLayout(false);
            this.CurKillPWPanel.ResumeLayout(false);
            this.NewKillPWPanel.ResumeLayout(false);
            this.NewAccPWPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel CurAccPWPanel;
        private System.Windows.Forms.TextBox CurAccPWTxtBx;
        private System.Windows.Forms.Panel CurKillPWPanel;
        private System.Windows.Forms.TextBox CurKillPWTxtBx;
        private System.Windows.Forms.Panel NewKillPWPanel;
        private System.Windows.Forms.TextBox NewKillPWTxtBx;
        private System.Windows.Forms.Panel NewAccPWPanel;
        private System.Windows.Forms.TextBox NewAccPWTxtBx;
        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}