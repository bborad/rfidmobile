using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using System.Threading;

namespace OnRamp
{
    public partial class EPCMaskInput : UserControl
    {
        public EPCMaskInput()
        {
            InitializeComponent();
        }

        public string searchTagNo
        {
            set
            {
                EPCTxtBx.Text = value;
                EPCChkBx.Checked = true;  
            }
        }
        

        #region TextBox/CheckBox management routines
        private void OnEPCChkBxChecked(object sender, EventArgs e)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EPCMaskInput));

            EPCTxtBx.Enabled = EPCChkBx.Checked;
            //if (EPCTxtBx.Enabled == false)
            //    EPCTxtBx.Text = resources.GetString("EPCTxtBx.Text");
            if (EPCTxtBx.Enabled == true)
            {
                if (String.Compare(resources.GetString("EPCTxtBx.Text"), EPCTxtBx.Text, true) == 0)
                    EPCTxtBx.Text = String.Empty;
                EPCTxtBx.Focus();
            }

            if (FieldChangedEvent != null)
                FieldChangedEvent (this, new FieldChangedEventArgs(InputField.EPC, EPCChkBx.Checked,
                    EPCTxtBx.Text));
        }

        private void OnPCChkBxChecked(object sender, EventArgs e)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EPCMaskInput));

            PCTxtBx.Enabled = PCChkBx.Checked;
            //if (PCTxtBx.Enabled == false)
            //    PCTxtBx.Text = resources.GetString("PCTxtBx.Text");
            if (PCTxtBx.Enabled == true)
            {
                if (String.Compare(resources.GetString("PCTxtBx.Text"), PCTxtBx.Text, true) == 0)
                    PCTxtBx.Text = String.Empty;
                PCTxtBx.Focus();
            }

            if (FieldChangedEvent != null)
                FieldChangedEvent(this, new FieldChangedEventArgs(InputField.PC, PCChkBx.Checked,
                    PCChkBx.Text));
        }

        private void HexKeyPressChk(object sender, KeyPressEventArgs e)
        {
            // From documentation: e.KeyChar won't deliver DELETE and a bunch of
            // other editing keys
            if ((e.KeyChar >= '0'  /* or NumPad0? */ && e.KeyChar <= '9')
                || (e.KeyChar >= 'a' && e.KeyChar <= 'f')
                || (e.KeyChar >= 'A' && e.KeyChar <= 'F')
                || (e.KeyChar == (char)Keys.Back) /*|| (e.KeyChar == (char)Keys.Delete)*/)
            // Arrow keys, Home, End?
            {
                e.Handled = false; // accept
            }
            else
            {
                ErrorFlashTxtBx((TextBox)sender);
                e.Handled = true; // reject
            }
        }

        private void ErrorFlashTxtBx(TextBox txtBx)
        {
            txtBx.BackColor = Color.Red;
            txtBx.Refresh();

            Thread.Sleep(500);

            txtBx.BackColor = Color.White;
            txtBx.Refresh();
        }

        private void EnsureTxtBxContentInWordMultiple(TextBox txtBx)
        {
            if (txtBx.Text != null && txtBx.Text.Length > 0)
            {
                uint mod4;
                mod4 = (uint)(txtBx.Text.Length & 0x3);
                if (mod4 > 0)
                    txtBx.Text = txtBx.Text.PadRight((int)(txtBx.Text.Length + (4 - mod4)), (char)'0');
            }
        }

        private void OnEPCTxtBxFocusLost(object sender, EventArgs e)
        {
            // make sure that it's in multiple of Word (16-bit)
            EnsureTxtBxContentInWordMultiple((TextBox)sender);
        }

        private void OnPCTxtBxFocusLost(object sender, EventArgs e)
        {
            // make sure that it's in multiple of Word (16-bit)
            EnsureTxtBxContentInWordMultiple((TextBox)sender);
        }

        #endregion

        #region Get/Set routines
        /// <summary>
        ///  Get the PC CheckBox/TextBox content
        /// </summary>
        /// <param name="byteArr"></param>
        /// <returns>Checked value of PC CheckBox</returns>
        public bool GetPC(out Byte[] byteArr)
        {
            byteArr = null;
            if (PCChkBx.Checked == false)
                return false;
            byteArr = new Byte[PCTxtBx.Text == null ? 0 : PCTxtBx.Text.Length/2];
            if (byteArr.Length > 0)
            {
                for (int bi = 0, si = 0; bi < byteArr.Length; bi++, si += 2)
                    byteArr[bi] = Byte.Parse(PCTxtBx.Text.Substring(si, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return true;
        }

        public bool GetEPC(out Byte[] byteArr)
        {
            byteArr = null;
            if (EPCChkBx.Checked == false)
                return false;
            byteArr = new Byte[EPCTxtBx.Text == null ? 0 : EPCTxtBx.Text.Length/2];
            if (byteArr.Length > 0)
            {
                for (int bi = 0, si = 0; bi < byteArr.Length; bi++, si += 2)
                    byteArr[bi] = Byte.Parse(EPCTxtBx.Text.Substring(si, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return true;
        }

        private void SetTxtBx(CheckBox chkBx, TextBox txtBx, bool enable, Byte[] byteArr, int maxBytes)
        {
            if (!enable)
                chkBx.Checked = false;
            else if (byteArr == null)
                throw new ApplicationException("Byte[] Argument must not be null");
            else
            {
                chkBx.Checked = true;
                StringBuilder txtBxStr = new StringBuilder(maxBytes * 2);
                // Check ByteArr Size
                for (int bi = 0; bi < byteArr.Length && bi < maxBytes; bi++)
                    txtBxStr.Append(byteArr[bi].ToString("X2"));
                if (byteArr.Length < maxBytes)
                {
                    int NumBytes2Pad = byteArr.Length & 0x01;
                    if (NumBytes2Pad == 1)
                        txtBxStr.Append((Char)'0');   
                }
                txtBx.Text = txtBxStr.ToString();
            }
        }

        public void SetPC(bool enable, Byte[] byteArr)
        {
            SetTxtBx(PCChkBx, PCTxtBx, enable, byteArr, EPCTag.PCFldSz * 2);
        }

        public void SetEPC(bool enable, Byte[] byteArr)
        {
            SetTxtBx(EPCChkBx, EPCTxtBx, enable, byteArr, EPCTag.EPCFldSz * 2);
        }

        #endregion

        #region Events
        public enum InputField
        {
            PC,
            EPC
        }
        public class FieldChangedEventArgs : EventArgs
        {
            public readonly InputField Field;
            public readonly bool Enabled;
            public readonly Byte[] Data;

            public FieldChangedEventArgs (InputField field, bool enabled, String txtBxText)
            {
                this.Field = field;
                this.Enabled = enabled;
                if (this.Enabled == true)
                {
                    if (txtBxText == null)
                    {
                        Data = new Byte[0];
                    }
                    else
                    {
                        Data = new Byte[txtBxText.Length / 2];
                        for (int bi = 0, si = 0; bi < Data.Length; bi++, si += 2)
                            Data[bi] = Byte.Parse(txtBxText.Substring(si, 2), System.Globalization.NumberStyles.HexNumber);
                    }
                }
            }
        }

        public event EventHandler<FieldChangedEventArgs> FieldChangedEvent;
        #endregion

        private void OnPCTxtBxChanged(object sender, EventArgs e)
        {
            if ((PCChkBx.Checked == true) && (FieldChangedEvent != null))
            {
                FieldChangedEvent(this, new FieldChangedEventArgs(InputField.PC, true, PCTxtBx.Text));
            }
        }

        private void OnEPCTxtBxChanged(object sender, EventArgs e)
        {
            if ((EPCChkBx.Checked == true) && (FieldChangedEvent != null))
            {
                FieldChangedEvent(this, new FieldChangedEventArgs(InputField.EPC, true, EPCChkBx.Text));
            }
        }

    }
}
