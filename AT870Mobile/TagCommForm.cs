using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;
//using ClsReaderLib.Devices.Barcode;
using HHDeviceInterface.BarCode;

namespace OnRamp
{
    public partial class TagCommForm : Form
    {
        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        #region Run State Enums
        enum BCRdOpState
        {
            Idle,
            Scanning,
            Stopping,
        }
        enum RfidRdOpState
        {
            Idle,
            Starting,
            Running,
            Stopping,
        }
        enum CommOpState
        {
            Idle,
            Starting,
            Running,
            Stopping,
        }
        #endregion

        public TagCommForm()
        {
            InitializeComponent();

            ScanBttnInitState();
        }

        #region Scan Button State Routines
        private void ScanBttnInitState()
        {
            CommOpState State = CommOpState.Idle;

            ScanBttn.Tag = State;
        }

        private CommOpState ScanBttnState()
        {
            CommOpState State = CommOpState.Idle;

            if (ScanBttn.Tag is CommOpState)
            {
                State = (CommOpState)ScanBttn.Tag;
            }
            else
            {
                throw new ApplicationException("ScanBttn Tag is not CommOpState");
            }
            return State;
        }

        private void ScanBttnSetState(CommOpState newState)
        {
            ScanBttn.Tag = newState; // boxing
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagCommForm));
            switch (newState)
            {
                case CommOpState.Idle:
                    ScanBttn.Text = resources.GetString("ScanBttn.Text");
                    // re-enable button
                    ScanBttn.Enabled = true;
                    ClrBttn.Enabled = true;
                    VewSavBttn.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
                case CommOpState.Starting:
                    ScanBttn.Enabled = false;
                    ClrBttn.Enabled = false;
                    VewSavBttn.Enabled = false;
                    UserInputsFreeze();
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case CommOpState.Running:
                    ScanBttn.Text = "Stop";
                    ScanBttn.Enabled = true;
                    ClrBttn.Enabled = false;
                    VewSavBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case CommOpState.Stopping:
                    ScanBttn.Enabled = false;
                    ClrBttn.Enabled = false;
                    VewSavBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
            }
        }

        private void UserInputsFreeze()
        {
           
        }

        private void UserInputsThaw()
        {
           
        }
        #endregion

        #region Scan Button

        private void OnScanBttnClicked(object sender, EventArgs e)
        {
            switch (ScanBttnState())
            {
                case CommOpState.Idle:
                    Program.RefreshStatusLabel(this, "Starting...");
                    ScanBttnSetState(CommOpState.Starting);
                    // Start Operation
                    bool RfidStarted, BCStarted;
                    if (StartScanning(out RfidStarted, out BCStarted))
                    {
                        if (!RfidStarted && BCStarted)
                        {
                            // go to Running state (BC does not have Starting...)
                            Program.RefreshStatusLabel(this, "Running...");
                            ScanBttnSetState(CommOpState.Running);
                        }
                    }
                    else
                    {
                        Program.RefreshStatusLabel(this, "Canceled");
                        ScanBttnSetState(CommOpState.Idle);
                    }
                    break;
                case CommOpState.Running:
                    Program.RefreshStatusLabel(this, "Stopping..");
                    ScanBttnSetState(CommOpState.Stopping);
                    // Stop Operation
                    bool RfidStopping, BCStopping, BCStopped;
                    if (StopScanning(out RfidStopping, out BCStopped, out BCStopping))
                    {
                        if (!RfidStopping && BCStopped)
                        {
                            // Go directly to Idle state
                            Program.RefreshStatusLabel(this, "Finished");
                            ScanBttnSetState(CommOpState.Idle);
                        }
                    }
                    else
                    {
                        Program.RefreshStatusLabel(this, "Canceled");
                        ScanBttnSetState(CommOpState.Idle);
                    }
                    break;
                case CommOpState.Starting:
                    Program.RefreshStatusLabel(this, "Stopping..");
                    ScanBttnSetState(CommOpState.Stopping);
                    if (StopScanning(out RfidStopping, out BCStopped, out BCStopping))
                    {
                        if (!RfidStopping && BCStopped)
                        {
                            // Restore, try later
                            Program.RefreshStatusLabel(this, "Starting...");
                            ScanBttnSetState(CommOpState.Starting);
                            Program.ShowWarning("Tag Commissioning Stop failed during Starting phase, please try again");
                        }
                    }
                    else
                    {
                        // Restore, try later
                        Program.RefreshStatusLabel(this, "Starting...");
                        ScanBttnSetState(CommOpState.Starting);
                        Program.ShowWarning("Tag Commissioning Stop failed during Starting phase, please try again");
                    }
                    break;
                default:
                    // Do nothing
                    break;
            }
        }

        private Commission commissioning;
        private bool StartScanning(out bool RfidScanStarting, out bool BCScanStarted)
        {
            CommStatusLbl.Text = String.Empty;

            RfidScanStarting = false;
            BCScanStarted = false;

            if (commissioning == null)
                commissioning = new Commission();

            BCScanStarted = BCScanOnceStart();
            if (!BCScanStarted)
            {
                Program.ShowError("Barcode Scan Failed to Start");
            }
            RfidScanStarting = RfidScanStart();
            if (!RfidScanStarting)
            {
                Program.ShowError("RFID Scan Failed to Start");
            }
            return (BCScanStarted || RfidScanStarting);
        }

        private bool StopScanning(out bool RfidScanStopping, out bool BCScanStopped,
         out bool BCScanStopping)
        {
            RfidScanStopping = false;
            BCScanStopped = false;
            BCScanStopping = false;

            if (BCScanStop())
                BCScanStopped = true;
            else
                BCScanStopping = true;

            if (RfidScanStop())
                RfidScanStopping = true;

            return true;
        }

        private void CommissionCurrPair()
        {
            if (commissioning.CommitCurrPair())
            {
                CommStatusLbl.Text = "Commissioned";
                ClrTxtBxes();
            }
            else
            {
                CommStatusLbl.Text = "Missing information";
            }
        }
        #endregion

        #region Barcode Rdr (new)
        private BCRdOpState _BCRdrState = BCRdOpState.Idle;
        private BCRdOpState BCRdrState
        {
            set
            {
                _BCRdrState = value;
                if (BCRdrState == BCRdOpState.Idle)
                {
                    // StartBttn to 'Idle' state?
                    if (RfidRdrState == RfidRdOpState.Idle)
                    {
                        ScanBttnSetState(CommOpState.Idle);
                        Program.RefreshStatusLabel(this, "Finished");
                        CommissionCurrPair();
                    }
                }
            }
            get
            {
                return _BCRdrState;
            }
        }
    
        private bool BCScanOnceStart()
        {
            //BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
            bool Succ = false;
            switch (BCRdrState)
            {
                case BCRdOpState.Idle:
                    BCRdrState = BCRdOpState.Scanning;
                    if (this.InvokeRequired)
                    {
                        Rdr.notifyee = this;
                    }
                    Rdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
                    //Succ = Rdr.ScanStart(BarCodeNotify, this);
                    Succ = Rdr.ScanStart();
                    if (!Succ)
                        BCRdrState = BCRdOpState.Idle;
                    break;
            }
            return Succ;
        }

        private bool BarCodeNotify(bool succ, String bcStr, String errMsg)
        {
            bool Added = false;
            if (succ)
            {
                BCTxtBx.Text = bcStr;
                Added = commissioning.AddBarcodeID(bcStr);
                TextBoxFlashGreen(BCTxtBx); 
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            }
            else
            {
                Program.RefreshStatusLabel(this, "Barcode Capture Error...");
                MessageBox.Show(errMsg, "Bar Code Reader Error");
            }

            if (Added)
                BCRdrState = BCRdOpState.Idle;

            return (!Added); // continue if not added
        }

        private bool BCScanStop()
        {
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
            //BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            bool Succ = false;
            if (BCRdrState == BCRdOpState.Scanning)
            {
                Program.RefreshStatusLabel(this, "Stopping Barcode Scan...");
                BCRdrState = BCRdOpState.Stopping;
                if (this.InvokeRequired)
                {
                    Rdr.stopNotifee = this;
                }
                Rdr.RegisterStopNotificationEvent(BCScanStopped);
                //if ((Succ = Rdr.ScanTryStop(BCScanStopped, this)))
                if ((Succ = Rdr.ScanTryStop()))
                {
                    Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
                    BCRdrState = BCRdOpState.Idle;
                }
                else
                {
                    // Scanner busy at the moment
                    // Leave the 'Stopping' State as to return 'not-to-continue'
                    //  on the next bar-code read notification
                    // MessageBox.Show("Scanner Busy", "Stop Denied");
                }
            }
            else if (BCRdrState == BCRdOpState.Idle)
                Succ = true; // it's already stopped
            return Succ;
        }

        private void BCScanStopped()
        {
            Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
            BCRdrState = BCRdOpState.Idle;
        }
        #endregion

        #region Text Boxes
        private void ClrTxtBxes()
        {
            BCTxtBx.Text = String.Empty;
            PCTxtBx.Text = String.Empty;
            EPCTxtBx.Text = String.Empty;
        }

        private void TextBoxFlashGreen(params TextBox[] txtBxes)
        {
#if false
            if (txtBxes != null && txtBxes.Length > 0)
            {
                Color OrigBackColor = txtBxes[0].BackColor;
                for (int i = 0; i < txtBxes.Length; i++)
                {
                    txtBxes[i].BackColor = Color.Green;
                    txtBxes[i].Refresh();
                }
                // instead of sleep, do counting to avoid (serial port)thread switch
                //System.Threading.Thread.Sleep(150);
                for (int i = 0; i < 1000000; i++) ;
                for (int i = 0; i < txtBxes.Length; i++)
                {
                    txtBxes[i].BackColor = OrigBackColor;
                }
            }
#endif
        }
        #endregion

        #region Status Labels
        #endregion

        #region RFID Rdr(new)
        private RfidRdOpState _RfidRdrState = RfidRdOpState.Idle;
        private RfidRdOpState RfidRdrState
        {
            set
            {
                _RfidRdrState = value;
                if (RfidRdrState == RfidRdOpState.Idle)
                {
                    // ScanBttn to 'Idle' state?
                    if (BCRdrState == BCRdOpState.Idle)
                    {
                        ScanBttnSetState(CommOpState.Idle);
                        Program.RefreshStatusLabel(this, "Finished");
                        CommissionCurrPair();
                    }
                    else if (BCRdrState == BCRdOpState.Scanning)
                    {
                        ScanBttnSetState(CommOpState.Running);
                    }
                }
                else if ((RfidRdrState == RfidRdOpState.Running)
                    && (ScanBttnState() == CommOpState.Starting))
                {
                    Program.RefreshStatusLabel(this, "Running...");
                    ScanBttnSetState(CommOpState.Running);
                }
            }
            get
            {
                return _RfidRdrState;
            }
        }

        private bool RfidScanStart()
        {
            bool Succ = false;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            switch (RfidRdrState)
            {
                case RfidRdOpState.Idle:
                    Program.RefreshStatusLabel(this, "RFID Scan Starting...");
                    RfidRdrState = RfidRdOpState.Starting;
                    //Rdr.InventoryOpStEvent += new EventHandler<InvtryOpEventArgs>(InvtryOpEvtHandler);
                    //Rdr.DscvrTagEvent += new EventHandler<DscvrTagEventArgs>(DscvrTagEvtHandler);
                    Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                    // Perform continous inventory until stop requested
                    //UserPref Pref = UserPref.GetInstance();
                    //Byte[] BnkMask; uint WdOffset;
                    //Pref.GetEPCBnkSelMask(out BnkMask, out WdOffset);
                    // Qsize of 0 to limit to tags in close proximity
                    if (Rdr.SetRepeatedTagObsrvMode(true) == false)
                        Program.ShowError("Failed to disable Duplicate-Tag-Filtering");
                    TagDiscovered = false;
                    if ((Succ = Rdr.TagInventoryFirstStart()) == false)
                    {
                        // rewind previous setup
                        RfidRdrState = RfidRdOpState.Idle;
                        //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    }
                    break;
                case RfidRdOpState.Running:
                    Succ = true; // Already running
                    break;
            }
            return Succ;
        }

        private bool RfidScanStop()
        {
            bool Succ = false;
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();

            switch (RfidRdrState)
            {
                case RfidRdOpState.Running:
                    RfidRdrState = RfidRdOpState.Stopping;
                    if ((Succ = Rdr.TagInventoryStop()) == false)
                    {
                        RfidRdrState = RfidRdOpState.Idle;
                        //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    }
                    break;
                case RfidRdOpState.Starting:
                    RfidRdrState = RfidRdOpState.Stopping;
                    if ((Succ = Rdr.TagInventoryStop()) == false)
                    {
                        RfidRdrState = RfidRdOpState.Starting; // keep current state
                    }
                    break;
                case RfidRdOpState.Idle:
                    Succ = true; // Already stopped
                    break;
            }
            return Succ;
        }

        private bool TagDiscovered = false;
        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            PCTxtBx.Text = e.PC.ToString("X4");
            EPCTxtBx.Text = e.EPC.ToString();
            commissioning.AddRfidTagID(e.PC, e.EPC);
            TextBoxFlashGreen(PCTxtBx, EPCTxtBx);

            TagDiscovered = true;
        }

        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            RfidRdOpState state = RfidRdrState;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    RfidRdrState = RfidRdOpState.Running;
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    Program.RefreshStatusLabel(this, "RFID Tag ID Captured...");
                    //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            Program.PromptUserRestartRadio();
                        else
                            MessageBox.Show(e.msg, "Tag Inventory Stopped with Error");
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped && 
                        RfidRdrState != RfidRdOpState.Stopping && TagDiscovered == false )
                    {
                        // Assuming that TagStopCount=1 works. The most probable case
                        // for serious HW error is when Inventory stopped without any tag read
                        Program.ShowError(e.msg);
                    }
                    // restore StartButton
                    RfidRdrState = RfidRdOpState.Idle;
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (state)
                    {
                        case RfidRdOpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case RfidRdOpState.Running:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case RfidRdOpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case RfidRdOpState.Idle:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    RfidRdrState = RfidRdOpState.Idle;
                    break;
                default:
                    break;
            }
        }
        #endregion

        struct RfidBarcodePair
        {
            private readonly UInt16 PC;
            private readonly UINT96_T EPC;
            private readonly String BCStr;

            public RfidBarcodePair(UInt16 PC, UINT96_T EPC, String bcStr)
            {
                this.PC = PC;
                this.EPC = EPC;
                this.BCStr = bcStr;
            }

            public String BC
            {
                get
                {
                    return BCStr;
                }
            }

            public String PCEPC
            {
                get
                {
                    return PC.ToString("X4") + "-" + EPC.ToString();
                }
            }
        }

        class Commission
        {
            struct RfidTag
            {
                public bool Valid;
                public UInt16 PC;
                public UINT96_T EPC;
            }

            private List<RfidBarcodePair> pairs;
            
            // RFID
           private RfidTag curTag;
            // Barcode
            private String curBC;

            public Commission()
            {
                pairs = new List<RfidBarcodePair>();
                ResetCurIDs();
            }

            private void ResetCurIDs()
            {
                curBC = String.Empty;
                curTag = new RfidTag();
                curTag.PC = 0;
                curTag.EPC = new UINT96_T();
            }

            private bool CurrPairReady
            {
                get
                {
                    return ((curTag.Valid) && (String.IsNullOrEmpty(curBC) == false));
                }
            }

            public bool CommitCurrPair()
            {
                if (!CurrPairReady)
                    return false;

                // both RfidTag and Barcode ID present
                pairs.Add(new RfidBarcodePair(curTag.PC, curTag.EPC, curBC));
                ResetCurIDs(); // ready for the next one
                return true;
            }

            /// <summary>
            /// Return true if both Barcode and RFID available
            /// </summary>
            /// <param name="bcStr"></param>
            /// <returns></returns>
            public bool AddBarcodeID(String bcStr)
            {
                if (String.IsNullOrEmpty(bcStr))
                    return false;
                this.curBC = bcStr;
                return CurrPairReady;
            }

            /// <summary>
            /// Return true if both Barcode and RFID available
            /// </summary>
            /// <param name="PC"></param>
            /// <param name="EPC"></param>
            /// <returns></returns>
            public bool AddRfidTagID(UInt16 PC, UINT96_T EPC)
            {
                curTag.PC = PC;
                curTag.EPC = EPC;
                curTag.Valid = true; // validate later?
                return CurrPairReady;
            }

            public int PairsCnt
            {
                get
                {
                    return pairs.Count;
                }
            }

            public bool GetPair(int idx, out RfidBarcodePair pair)
            {
                pair = new RfidBarcodePair();

                if (idx < 0 || idx >= pairs.Count)
                    return false;

                pair = pairs[idx]; // shallow copy
                return true;
            }

            public bool RemovePair(int idx)
            {
                if (idx < 0 || idx >= pairs.Count)
                    return false;

                pairs.RemoveAt(idx);
                return true;
            }
        }

        // Assuming that comissioning operation is not running
        private void LstVwRowDeletedNotify(int pairIdx)
        {
            // Remove from commissioning
            if (!commissioning.RemovePair(pairIdx))
            {
                Program.ShowError("Could not find selected item from data.");
                return;
            }
        }

        private void OnResBttnClicked(object sender, EventArgs e)
        {
            if (commissioning == null)
            {
                Program.ShowWarning("Please scan items first");
                return;
            }
            // Show a List of currently commissioned pairs
            LstViewSavForm LstFm = new LstViewSavForm(true, LstVwRowDeletedNotify);
            LstFm.Title = "Scanned Pairs for commissioning";
            LstFm.SetColumns(new String[2] { "RFID", "BarCode" }, new int[2] { -2, -2 });
            for (int i = 0; i < commissioning.PairsCnt; i++)
            {
                RfidBarcodePair pair;
                if (commissioning.GetPair(i, out pair))
                {
                    LstFm.AddRow(new String[2] { pair.PCEPC, pair.BC });
                }
            }
            LstFm.ShowDialog();
            LstFm.Dispose();
        }

        /// <summary>
        /// Clear All current results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnClrBttnClicked(object sender, EventArgs e)
        {
            if (commissioning == null || commissioning.PairsCnt <= 0)
            {
                Program.ShowWarning("Please scan items first");
                return;
            }
            // Confirmation Dialog
            if (Program.AskUserConfirm("This would delete any unsaved results.\n Proceed?") == DialogResult.OK)
            {
                commissioning = null;
                ClrTxtBxes();
            }
        }

     

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (ScanBttnState() != CommOpState.Idle)
            {
                MessageBox.Show("Please stop commissioning first");
                e.Cancel = true;
            }
            else if (commissioning != null)
            {
                e.Cancel = (Program.AskUserConfirm("This would delete all unsaved results.\nProceed?") != DialogResult.OK);
            }
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            // Set up to receive HotKey event
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        #region F4/F5/F11 Hotkey
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11:
                case eVKey.VK_F19: 
                    if (down)
                    {
                        // fake 'Start' key press if not already running
                        if (!F11Depressed && ScanBttnState() == CommOpState.Idle)
                        {
                            OnScanBttnClicked(this, null);
                        }
                        F11Depressed = true;
                    }
                    else // up
                    {
                        if (ScanBttnState() == CommOpState.Starting
                            || ScanBttnState() == CommOpState.Running)
                        {
                            // Stop!
                            OnScanBttnClicked(this, null);
                        }
                        F11Depressed = false;
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (down)
                    {
                        if (ScanBttnState() == CommOpState.Idle)
                        {
                            AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                        }
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && (ScanBttnState() == CommOpState.Idle))
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    break;
            }
        }

        #endregion

    }
}