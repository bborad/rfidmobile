namespace OnRamp
{
    partial class RnDSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RnDSettingsForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label1 = new System.Windows.Forms.Label();
            this.Bnd1Bttn = new System.Windows.Forms.RadioButton();
            this.Bnd2Bttn = new System.Windows.Forms.RadioButton();
            this.Bnd3Bttn = new System.Windows.Forms.RadioButton();
            this.Bnd4Bttn = new System.Windows.Forms.RadioButton();
            this.Bnd6Bttn = new System.Windows.Forms.RadioButton();
            this.ApplyBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // Bnd1Bttn
            // 
            resources.ApplyResources(this.Bnd1Bttn, "Bnd1Bttn");
            this.Bnd1Bttn.Name = "Bnd1Bttn";
            // 
            // Bnd2Bttn
            // 
            resources.ApplyResources(this.Bnd2Bttn, "Bnd2Bttn");
            this.Bnd2Bttn.Name = "Bnd2Bttn";
            // 
            // Bnd3Bttn
            // 
            resources.ApplyResources(this.Bnd3Bttn, "Bnd3Bttn");
            this.Bnd3Bttn.Name = "Bnd3Bttn";
            // 
            // Bnd4Bttn
            // 
            resources.ApplyResources(this.Bnd4Bttn, "Bnd4Bttn");
            this.Bnd4Bttn.Name = "Bnd4Bttn";
            // 
            // Bnd6Bttn
            // 
            resources.ApplyResources(this.Bnd6Bttn, "Bnd6Bttn");
            this.Bnd6Bttn.Name = "Bnd6Bttn";
            // 
            // ApplyBttn
            // 
            resources.ApplyResources(this.ApplyBttn, "ApplyBttn");
            this.ApplyBttn.Name = "ApplyBttn";
            this.ApplyBttn.Click += new System.EventHandler(this.OnApplyBttnClicked);
            // 
            // RnDSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ApplyBttn);
            this.Controls.Add(this.Bnd6Bttn);
            this.Controls.Add(this.Bnd4Bttn);
            this.Controls.Add(this.Bnd3Bttn);
            this.Controls.Add(this.Bnd2Bttn);
            this.Controls.Add(this.Bnd1Bttn);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RnDSettingsForm";
            this.Load += new System.EventHandler(this.OnLoad);
            this.Closed += new System.EventHandler(this.OnClosed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton Bnd1Bttn;
        private System.Windows.Forms.RadioButton Bnd2Bttn;
        private System.Windows.Forms.RadioButton Bnd3Bttn;
        private System.Windows.Forms.RadioButton Bnd4Bttn;
        private System.Windows.Forms.RadioButton Bnd6Bttn;
        private System.Windows.Forms.Button ApplyBttn;
    }
}