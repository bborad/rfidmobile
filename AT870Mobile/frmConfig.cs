﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class frmConfig : Form
    {
        public frmConfig()
        {
            InitializeComponent();
        }

        private void OnFixedQBttnChecked(object sender, EventArgs e)
        {

        }

        private void OnDynQBttnChecked(object sender, EventArgs e)
        {

        }

        private void OnPopSzChanged(object sender, EventArgs e)
        {

        }

        private void OnPopSzKeyPressed(object sender, KeyPressEventArgs e)
        {

        }

        private void OnPopSzValidating(object sender, CancelEventArgs e)
        {

        }

        private void OnMaskInputFieldChanged(object sender, EPCMaskInput.FieldChangedEventArgs e)
        {

        }

        private void OnTagVndrChanged(object sender, EventArgs e)
        {

        }

        private void OnUserMemSzChanged(object sender, EventArgs e)
        {

        }

        private void OnUserMemSzEnabled(object sender, EventArgs e)
        {

        }

        private void OnTIDExtraSzChanged(object sender, EventArgs e)
        {

        }

        private void OnTIDExtrSzEnabled(object sender, EventArgs e)
        {

        }

        private void OnTagUserMemChkBxChanged(object sender, EventArgs e)
        {

        }

        private void OnTagTIDChkBxChanged(object sender, EventArgs e)
        {

        }

        private void OnSingleChnLnkClicked(object sender, EventArgs e)
        {

        }

        private void OnSingleChnChecked(object sender, EventArgs e)
        {

        }

        private void OnLBTNoBttnClicked(object sender, EventArgs e)
        {

        }

        private void OnLBTYesBttnClicked(object sender, EventArgs e)
        {

        }

        private void OnCntryCmbBxChanged(object sender, EventArgs e)
        {

        }

        private void OnFqBndChanged(object sender, EventArgs e)
        {

        }

        private void OnLnkPrfDetailsClicked(object sender, EventArgs e)
        {

        }

        private void OnLnkPrf5ChkChanged(object sender, EventArgs e)
        {

        }

        private void OnLnkPrf4ChkChanged(object sender, EventArgs e)
        {

        }

        private void OnLnkPrf3ChkChanged(object sender, EventArgs e)
        {

        }

        private void OnLnkPrf2ChkChanged(object sender, EventArgs e)
        {

        }

        private void OnLnkPrf1ChkChanged(object sender, EventArgs e)
        {

        }

        private void OnLnkPrf0ChkChanged(object sender, EventArgs e)
        {

        }

        private void OnInvtryCycUDChanged(object sender, EventArgs e)
        {

        }

        private void OnDwellTimeUDChanged(object sender, EventArgs e)
        {

        }

        private void OnPwrLvlValChanged(object sender, EventArgs e)
        {

        }

        private void OnSelCrit3LnkClicked(object sender, EventArgs e)
        {

        }

        private void OnSelCrit2LnkClicked(object sender, EventArgs e)
        {

        }

        private void OnSelCrit1LnkClicked(object sender, EventArgs e)
        {

        }

        private void OnSelCrit0LnkClicked(object sender, EventArgs e)
        {

        }

        private void OnSelCritCntValChanged(object sender, EventArgs e)
        {

        }

        private void OnDQMxQryCntValChanged(object sender, EventArgs e)
        {

        }

        private void OnMxQValChanged(object sender, EventArgs e)
        {

        }

        private void OnMnQValChanged(object sender, EventArgs e)
        {

        }

        private void OnDQRetryCntValChanged(object sender, EventArgs e)
        {

        }

        private void OnStQValChanged(object sender, EventArgs e)
        {

        }

        private void OnFQRetryCntValChanged(object sender, EventArgs e)
        {

        }

        private void OnRptChkBxChkChanged(object sender, EventArgs e)
        {

        }

        private void OnFxQValChanged(object sender, EventArgs e)
        {

        }

        private void OnDQChkChanged(object sender, EventArgs e)
        {

        }

        private void OnFQChkChanged(object sender, EventArgs e)
        {

        }

        private void OnALAllChkChanged(object sender, EventArgs e)
        {

        }

        private void OnSLOffChkChanged(object sender, EventArgs e)
        {

        }

        private void OnSLOnChkChanged(object sender, EventArgs e)
        {

        }

        private void OnToggleTgtChkBxChanged(object sender, EventArgs e)
        {

        }

        private void OnSessValIdxChanged(object sender, EventArgs e)
        {

        }

        private void OnSessIdxChanged(object sender, EventArgs e)
        {

        }

        private void OnXcvrTempChanged(object sender, EventArgs e)
        {

        }

        private void OnXverTempUDKeyUp(object sender, KeyEventArgs e)
        {

        }

        private void OnDCOffChanged(object sender, EventArgs e)
        {

        }

        private void OnDCOnChanged(object sender, EventArgs e)
        {

        }

        private void txtMsgDisplayTimeOut_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMsgDisplayTimeOut_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtPageSize_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPageSize_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label46_ParentChanged(object sender, EventArgs e)
        {

        }

        private void RampSettings_Click(object sender, EventArgs e)
        {

        }

        private void OnInvtrySessChanged(object sender, EventArgs e)
        {

        }

        private void OnSelectedPageChanged(object sender, EventArgs e)
        {

        }

        private void frmConfig_Load(object sender, EventArgs e)
        {

        }
    }
}