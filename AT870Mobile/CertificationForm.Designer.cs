namespace OnRamp
{
    partial class CertificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CertificationForm));
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.RndDatStartBttn = new System.Windows.Forms.Button();
            this.RndDatStatusLbl = new System.Windows.Forms.Label();
            this.RndDatTmr = new System.Windows.Forms.Timer();
            this.debounceTmr = new System.Windows.Forms.Timer();
            this.PATempLbl = new System.Windows.Forms.Label();
            this.XcvrTempLbl = new System.Windows.Forms.Label();
            this.AmbTempLbl = new System.Windows.Forms.Label();
            this.CWOnOffBttn = new System.Windows.Forms.Button();
            this.TempDspyTmr = new System.Windows.Forms.Timer();
            this.PrfNumLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // RndDatStartBttn
            // 
            resources.ApplyResources(this.RndDatStartBttn, "RndDatStartBttn");
            this.RndDatStartBttn.Name = "RndDatStartBttn";
            this.RndDatStartBttn.Click += new System.EventHandler(this.OnRndDatStartClicked);
            // 
            // RndDatStatusLbl
            // 
            this.RndDatStatusLbl.ForeColor = System.Drawing.Color.Indigo;
            resources.ApplyResources(this.RndDatStatusLbl, "RndDatStatusLbl");
            this.RndDatStatusLbl.Name = "RndDatStatusLbl";
            // 
            // RndDatTmr
            // 
            this.RndDatTmr.Tick += new System.EventHandler(this.OnRndDatTmrTick);
            // 
            // debounceTmr
            // 
            this.debounceTmr.Interval = 3000;
            this.debounceTmr.Tick += new System.EventHandler(this.OnDebounceTmrTick);
            // 
            // PATempLbl
            // 
            resources.ApplyResources(this.PATempLbl, "PATempLbl");
            this.PATempLbl.Name = "PATempLbl";
            // 
            // XcvrTempLbl
            // 
            resources.ApplyResources(this.XcvrTempLbl, "XcvrTempLbl");
            this.XcvrTempLbl.Name = "XcvrTempLbl";
            // 
            // AmbTempLbl
            // 
            resources.ApplyResources(this.AmbTempLbl, "AmbTempLbl");
            this.AmbTempLbl.Name = "AmbTempLbl";
            // 
            // CWOnOffBttn
            // 
            resources.ApplyResources(this.CWOnOffBttn, "CWOnOffBttn");
            this.CWOnOffBttn.Name = "CWOnOffBttn";
            this.CWOnOffBttn.Click += new System.EventHandler(this.OnCWOnOffBttnClicked);
            // 
            // TempDspyTmr
            // 
            this.TempDspyTmr.Interval = 3000;
            this.TempDspyTmr.Tick += new System.EventHandler(this.OnTempTmrTick);
            // 
            // PrfNumLbl
            // 
            resources.ApplyResources(this.PrfNumLbl, "PrfNumLbl");
            this.PrfNumLbl.Name = "PrfNumLbl";
            // 
            // CertificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.PrfNumLbl);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.CWOnOffBttn);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PATempLbl);
            this.Controls.Add(this.XcvrTempLbl);
            this.Controls.Add(this.AmbTempLbl);
            this.Controls.Add(this.RndDatStatusLbl);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RndDatStartBttn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CertificationForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RndDatStartBttn;
        private System.Windows.Forms.Label RndDatStatusLbl;
        private System.Windows.Forms.Timer RndDatTmr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer debounceTmr;
        private System.Windows.Forms.Label PATempLbl;
        private System.Windows.Forms.Label XcvrTempLbl;
        private System.Windows.Forms.Label AmbTempLbl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button CWOnOffBttn;
        private System.Windows.Forms.Timer TempDspyTmr;
        private System.Windows.Forms.Label PrfNumLbl;
        private System.Windows.Forms.Label label5;
    }
}