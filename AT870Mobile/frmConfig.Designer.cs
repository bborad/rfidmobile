﻿namespace OnRamp
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.RstButton = new System.Windows.Forms.Button();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.TabCtrl = new System.Windows.Forms.TabControl();
            this.tpControlSettings = new System.Windows.Forms.TabPage();
            this.txtHoppingMode = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtProtocolVer = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLBTTime = new System.Windows.Forms.TextBox();
            this.txtSession = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtScantime = new System.Windows.Forms.TextBox();
            this.txtQValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.AntTab = new System.Windows.Forms.TabPage();
            this.label53 = new System.Windows.Forms.Label();
            this.txtHighValue = new System.Windows.Forms.TextBox();
            this.txtMedValue = new System.Windows.Forms.TextBox();
            this.txtLowValue = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.PwrLvlValLbl = new System.Windows.Forms.Label();
            this.PwrLvlTrkBr = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.ServiceSettings = new System.Windows.Forms.TabPage();
            this.label50 = new System.Windows.Forms.Label();
            this.txtMsgDisplayTimeOut = new System.Windows.Forms.TextBox();
            this.txtPageSize = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.rbErrLogDisable = new System.Windows.Forms.RadioButton();
            this.rbErrLogEnable = new System.Windows.Forms.RadioButton();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tabBarcodeSettings = new System.Windows.Forms.TabPage();
            this.rb2DScanner = new System.Windows.Forms.RadioButton();
            this.rb1DScanner = new System.Windows.Forms.RadioButton();
            this.TabCtrl.SuspendLayout();
            this.tpControlSettings.SuspendLayout();
            this.AntTab.SuspendLayout();
            this.ServiceSettings.SuspendLayout();
            this.tabBarcodeSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // RstButton
            // 
            this.RstButton.Location = new System.Drawing.Point(102, 245);
            this.RstButton.Name = "RstButton";
            this.RstButton.Size = new System.Drawing.Size(102, 23);
            this.RstButton.TabIndex = 11;
            this.RstButton.Text = "Undo Changes";
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(9, 245);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(91, 23);
            this.ApplyButton.TabIndex = 10;
            this.ApplyButton.Text = "Apply";
            // 
            // TabCtrl
            // 
            this.TabCtrl.Controls.Add(this.tpControlSettings);
            this.TabCtrl.Controls.Add(this.AntTab);
            this.TabCtrl.Controls.Add(this.ServiceSettings);
            this.TabCtrl.Controls.Add(this.tabBarcodeSettings);
            this.TabCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.TabCtrl.Location = new System.Drawing.Point(0, 0);
            this.TabCtrl.Name = "TabCtrl";
            this.TabCtrl.SelectedIndex = 0;
            this.TabCtrl.Size = new System.Drawing.Size(238, 232);
            this.TabCtrl.TabIndex = 9;
            // 
            // tpControlSettings
            // 
            this.tpControlSettings.BackColor = System.Drawing.SystemColors.Window;
            this.tpControlSettings.Controls.Add(this.txtHoppingMode);
            this.tpControlSettings.Controls.Add(this.label14);
            this.tpControlSettings.Controls.Add(this.txtProtocolVer);
            this.tpControlSettings.Controls.Add(this.label13);
            this.tpControlSettings.Controls.Add(this.label1);
            this.tpControlSettings.Controls.Add(this.label9);
            this.tpControlSettings.Controls.Add(this.label8);
            this.tpControlSettings.Controls.Add(this.txtLBTTime);
            this.tpControlSettings.Controls.Add(this.txtSession);
            this.tpControlSettings.Controls.Add(this.label7);
            this.tpControlSettings.Controls.Add(this.label5);
            this.tpControlSettings.Controls.Add(this.label2);
            this.tpControlSettings.Controls.Add(this.txtScantime);
            this.tpControlSettings.Controls.Add(this.txtQValue);
            this.tpControlSettings.Controls.Add(this.label6);
            this.tpControlSettings.Controls.Add(this.button8);
            this.tpControlSettings.Controls.Add(this.button6);
            this.tpControlSettings.Controls.Add(this.button3);
            this.tpControlSettings.Controls.Add(this.button2);
            this.tpControlSettings.Controls.Add(this.label12);
            this.tpControlSettings.Location = new System.Drawing.Point(4, 25);
            this.tpControlSettings.Name = "tpControlSettings";
            this.tpControlSettings.Size = new System.Drawing.Size(230, 203);
            this.tpControlSettings.Text = "Control Settings";
            // 
            // txtHoppingMode
            // 
            this.txtHoppingMode.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtHoppingMode.Location = new System.Drawing.Point(89, 31);
            this.txtHoppingMode.Name = "txtHoppingMode";
            this.txtHoppingMode.ReadOnly = true;
            this.txtHoppingMode.Size = new System.Drawing.Size(118, 19);
            this.txtHoppingMode.TabIndex = 81;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(0, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 16);
            this.label14.Text = "Hopping Mode";
            // 
            // txtProtocolVer
            // 
            this.txtProtocolVer.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtProtocolVer.Location = new System.Drawing.Point(89, 5);
            this.txtProtocolVer.Name = "txtProtocolVer";
            this.txtProtocolVer.ReadOnly = true;
            this.txtProtocolVer.Size = new System.Drawing.Size(118, 19);
            this.txtProtocolVer.TabIndex = 80;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(5, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 16);
            this.label13.Text = "Protocol Ver.";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(131, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.Text = "1 ~ 3";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(132, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 16);
            this.label9.Text = "0 ~ 60";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(132, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.Text = "0 ~ 15";
            // 
            // txtLBTTime
            // 
            this.txtLBTTime.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtLBTTime.Location = new System.Drawing.Point(84, 133);
            this.txtLBTTime.Name = "txtLBTTime";
            this.txtLBTTime.Size = new System.Drawing.Size(36, 19);
            this.txtLBTTime.TabIndex = 78;
            // 
            // txtSession
            // 
            this.txtSession.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtSession.Location = new System.Drawing.Point(84, 108);
            this.txtSession.Name = "txtSession";
            this.txtSession.Size = new System.Drawing.Size(36, 19);
            this.txtSession.TabIndex = 77;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(14, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 16);
            this.label7.Text = "LBT Time";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(21, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 16);
            this.label5.Text = "Session";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(14, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.Text = "ScanTime";
            // 
            // txtScantime
            // 
            this.txtScantime.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtScantime.Location = new System.Drawing.Point(84, 83);
            this.txtScantime.Name = "txtScantime";
            this.txtScantime.Size = new System.Drawing.Size(36, 19);
            this.txtScantime.TabIndex = 75;
            // 
            // txtQValue
            // 
            this.txtQValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtQValue.Location = new System.Drawing.Point(84, 57);
            this.txtQValue.Name = "txtQValue";
            this.txtQValue.Size = new System.Drawing.Size(36, 19);
            this.txtQValue.TabIndex = 74;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(24, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 16);
            this.label6.Text = "QValue";
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button8.Location = new System.Drawing.Point(182, 133);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(25, 19);
            this.button8.TabIndex = 73;
            this.button8.Text = "SET";
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button6.Location = new System.Drawing.Point(182, 108);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(25, 19);
            this.button6.TabIndex = 72;
            this.button6.Text = "SET";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button3.Location = new System.Drawing.Point(182, 82);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(25, 19);
            this.button3.TabIndex = 69;
            this.button3.Text = "SET";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.button2.Location = new System.Drawing.Point(182, 56);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(25, 19);
            this.button2.TabIndex = 68;
            this.button2.Text = "SET";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(125, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 16);
            this.label12.Text = "0 ~ 4000";
            // 
            // AntTab
            // 
            this.AntTab.BackColor = System.Drawing.Color.White;
            this.AntTab.Controls.Add(this.label53);
            this.AntTab.Controls.Add(this.txtHighValue);
            this.AntTab.Controls.Add(this.txtMedValue);
            this.AntTab.Controls.Add(this.txtLowValue);
            this.AntTab.Controls.Add(this.label52);
            this.AntTab.Controls.Add(this.label51);
            this.AntTab.Controls.Add(this.PwrLvlValLbl);
            this.AntTab.Controls.Add(this.PwrLvlTrkBr);
            this.AntTab.Controls.Add(this.label11);
            this.AntTab.Location = new System.Drawing.Point(4, 25);
            this.AntTab.Name = "AntTab";
            this.AntTab.Size = new System.Drawing.Size(230, 203);
            this.AntTab.Text = "Antenna";
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label53.Location = new System.Drawing.Point(16, 120);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(80, 18);
            this.label53.Text = "High Value";
            this.label53.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label53.Visible = false;
            // 
            // txtHighValue
            // 
            this.txtHighValue.Location = new System.Drawing.Point(105, 115);
            this.txtHighValue.Name = "txtHighValue";
            this.txtHighValue.Size = new System.Drawing.Size(61, 23);
            this.txtHighValue.TabIndex = 32;
            // 
            // txtMedValue
            // 
            this.txtMedValue.Location = new System.Drawing.Point(105, 89);
            this.txtMedValue.Name = "txtMedValue";
            this.txtMedValue.Size = new System.Drawing.Size(61, 23);
            this.txtMedValue.TabIndex = 29;
            // 
            // txtLowValue
            // 
            this.txtLowValue.Location = new System.Drawing.Point(105, 63);
            this.txtLowValue.Name = "txtLowValue";
            this.txtLowValue.Size = new System.Drawing.Size(61, 23);
            this.txtLowValue.TabIndex = 25;
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label52.Location = new System.Drawing.Point(16, 94);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(80, 18);
            this.label52.Text = "Medium Value";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label52.Visible = false;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label51.Location = new System.Drawing.Point(16, 68);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(80, 18);
            this.label51.Text = "Low Value";
            this.label51.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label51.Visible = false;
            // 
            // PwrLvlValLbl
            // 
            this.PwrLvlValLbl.ForeColor = System.Drawing.Color.Indigo;
            this.PwrLvlValLbl.Location = new System.Drawing.Point(160, 28);
            this.PwrLvlValLbl.Name = "PwrLvlValLbl";
            this.PwrLvlValLbl.Size = new System.Drawing.Size(70, 25);
            this.PwrLvlValLbl.Text = "MEDIUM";
            this.PwrLvlValLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PwrLvlTrkBr
            // 
            this.PwrLvlTrkBr.LargeChange = 1;
            this.PwrLvlTrkBr.Location = new System.Drawing.Point(2, 25);
            this.PwrLvlTrkBr.Maximum = 3;
            this.PwrLvlTrkBr.Minimum = 1;
            this.PwrLvlTrkBr.Name = "PwrLvlTrkBr";
            this.PwrLvlTrkBr.Size = new System.Drawing.Size(162, 28);
            this.PwrLvlTrkBr.TabIndex = 11;
            this.PwrLvlTrkBr.Value = 2;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(209, 22);
            this.label11.Text = "Antenna Power (0 - 30 dBm):";
            // 
            // ServiceSettings
            // 
            this.ServiceSettings.BackColor = System.Drawing.Color.White;
            this.ServiceSettings.Controls.Add(this.label50);
            this.ServiceSettings.Controls.Add(this.txtMsgDisplayTimeOut);
            this.ServiceSettings.Controls.Add(this.txtPageSize);
            this.ServiceSettings.Controls.Add(this.textBox1);
            this.ServiceSettings.Controls.Add(this.label49);
            this.ServiceSettings.Controls.Add(this.rbErrLogDisable);
            this.ServiceSettings.Controls.Add(this.rbErrLogEnable);
            this.ServiceSettings.Controls.Add(this.label48);
            this.ServiceSettings.Controls.Add(this.label47);
            this.ServiceSettings.Controls.Add(this.label46);
            this.ServiceSettings.Location = new System.Drawing.Point(4, 25);
            this.ServiceSettings.Name = "ServiceSettings";
            this.ServiceSettings.Size = new System.Drawing.Size(230, 203);
            this.ServiceSettings.Text = "Service Settings";
            // 
            // label50
            // 
            this.label50.Location = new System.Drawing.Point(163, 72);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(39, 20);
            this.label50.Text = "sec";
            // 
            // txtMsgDisplayTimeOut
            // 
            this.txtMsgDisplayTimeOut.Location = new System.Drawing.Point(82, 69);
            this.txtMsgDisplayTimeOut.MaxLength = 4;
            this.txtMsgDisplayTimeOut.Name = "txtMsgDisplayTimeOut";
            this.txtMsgDisplayTimeOut.Size = new System.Drawing.Size(81, 23);
            this.txtMsgDisplayTimeOut.TabIndex = 16;
            // 
            // txtPageSize
            // 
            this.txtPageSize.Location = new System.Drawing.Point(3, 68);
            this.txtPageSize.MaxLength = 4;
            this.txtPageSize.Name = "txtPageSize";
            this.txtPageSize.Size = new System.Drawing.Size(64, 23);
            this.txtPageSize.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(3, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(224, 23);
            this.textBox1.TabIndex = 1;
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(82, 48);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(134, 20);
            this.label49.Text = "Msg Display TimeOut";
            // 
            // rbErrLogDisable
            // 
            this.rbErrLogDisable.Location = new System.Drawing.Point(98, 117);
            this.rbErrLogDisable.Name = "rbErrLogDisable";
            this.rbErrLogDisable.Size = new System.Drawing.Size(98, 20);
            this.rbErrLogDisable.TabIndex = 11;
            this.rbErrLogDisable.Text = "Disable";
            // 
            // rbErrLogEnable
            // 
            this.rbErrLogEnable.Location = new System.Drawing.Point(12, 115);
            this.rbErrLogEnable.Name = "rbErrLogEnable";
            this.rbErrLogEnable.Size = new System.Drawing.Size(84, 20);
            this.rbErrLogEnable.TabIndex = 10;
            this.rbErrLogEnable.Text = "Enable";
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(3, 94);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(110, 20);
            this.label48.Text = "Error Logging ";
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(3, 47);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(64, 20);
            this.label47.Text = "Page Size";
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(3, 4);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(110, 20);
            this.label46.Text = "Service URL";
            // 
            // tabBarcodeSettings
            // 
            this.tabBarcodeSettings.BackColor = System.Drawing.Color.White;
            this.tabBarcodeSettings.Controls.Add(this.rb2DScanner);
            this.tabBarcodeSettings.Controls.Add(this.rb1DScanner);
            this.tabBarcodeSettings.Location = new System.Drawing.Point(4, 25);
            this.tabBarcodeSettings.Name = "tabBarcodeSettings";
            this.tabBarcodeSettings.Size = new System.Drawing.Size(230, 203);
            this.tabBarcodeSettings.Text = "Barcode Settings";
            // 
            // rb2DScanner
            // 
            this.rb2DScanner.Location = new System.Drawing.Point(26, 62);
            this.rb2DScanner.Name = "rb2DScanner";
            this.rb2DScanner.Size = new System.Drawing.Size(146, 20);
            this.rb2DScanner.TabIndex = 13;
            this.rb2DScanner.Text = "2D Scanner";
            // 
            // rb1DScanner
            // 
            this.rb1DScanner.Location = new System.Drawing.Point(26, 25);
            this.rb1DScanner.Name = "rb1DScanner";
            this.rb1DScanner.Size = new System.Drawing.Size(146, 20);
            this.rb1DScanner.TabIndex = 12;
            this.rb1DScanner.Text = "1D Scanner";
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.RstButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.TabCtrl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfig";
            this.Text = "RFID Config";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.TabCtrl.ResumeLayout(false);
            this.tpControlSettings.ResumeLayout(false);
            this.AntTab.ResumeLayout(false);
            this.ServiceSettings.ResumeLayout(false);
            this.tabBarcodeSettings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RstButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.TabControl TabCtrl;
        private System.Windows.Forms.TabPage tpControlSettings;
        private System.Windows.Forms.TextBox txtHoppingMode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtProtocolVer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLBTTime;
        private System.Windows.Forms.TextBox txtSession;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtScantime;
        private System.Windows.Forms.TextBox txtQValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage AntTab;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtHighValue;
        private System.Windows.Forms.TextBox txtMedValue;
        private System.Windows.Forms.TextBox txtLowValue;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label PwrLvlValLbl;
        private System.Windows.Forms.TrackBar PwrLvlTrkBr;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage ServiceSettings;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtMsgDisplayTimeOut;
        private System.Windows.Forms.TextBox txtPageSize;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.RadioButton rbErrLogDisable;
        private System.Windows.Forms.RadioButton rbErrLogEnable;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabBarcodeSettings;
        private System.Windows.Forms.RadioButton rb2DScanner;
        private System.Windows.Forms.RadioButton rb1DScanner;

    }
}