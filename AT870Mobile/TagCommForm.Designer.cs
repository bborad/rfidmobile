namespace OnRamp
{
    partial class TagCommForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagCommForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.label7 = new System.Windows.Forms.Label();
            this.EPCTxtBx = new System.Windows.Forms.TextBox();
            this.PCTxtBx = new System.Windows.Forms.TextBox();
            this.BCTxtBx = new System.Windows.Forms.TextBox();
            this.VewSavBttn = new System.Windows.Forms.Button();
            this.ClrBttn = new System.Windows.Forms.Button();
            this.ScanBttn = new System.Windows.Forms.Button();
            this.CommStatusLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // EPCTxtBx
            // 
            resources.ApplyResources(this.EPCTxtBx, "EPCTxtBx");
            this.EPCTxtBx.Name = "EPCTxtBx";
            // 
            // PCTxtBx
            // 
            resources.ApplyResources(this.PCTxtBx, "PCTxtBx");
            this.PCTxtBx.Name = "PCTxtBx";
            // 
            // BCTxtBx
            // 
            resources.ApplyResources(this.BCTxtBx, "BCTxtBx");
            this.BCTxtBx.Name = "BCTxtBx";
            // 
            // VewSavBttn
            // 
            resources.ApplyResources(this.VewSavBttn, "VewSavBttn");
            this.VewSavBttn.Name = "VewSavBttn";
            this.VewSavBttn.Click += new System.EventHandler(this.OnResBttnClicked);
            // 
            // ClrBttn
            // 
            resources.ApplyResources(this.ClrBttn, "ClrBttn");
            this.ClrBttn.Name = "ClrBttn";
            this.ClrBttn.Click += new System.EventHandler(this.OnClrBttnClicked);
            // 
            // ScanBttn
            // 
            resources.ApplyResources(this.ScanBttn, "ScanBttn");
            this.ScanBttn.Name = "ScanBttn";
            this.ScanBttn.Click += new System.EventHandler(this.OnScanBttnClicked);
            // 
            // CommStatusLbl
            // 
            this.CommStatusLbl.ForeColor = System.Drawing.Color.Blue;
            resources.ApplyResources(this.CommStatusLbl, "CommStatusLbl");
            this.CommStatusLbl.Name = "CommStatusLbl";
            // 
            // TagCommForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CommStatusLbl);
            this.Controls.Add(this.ScanBttn);
            this.Controls.Add(this.ClrBttn);
            this.Controls.Add(this.VewSavBttn);
            this.Controls.Add(this.BCTxtBx);
            this.Controls.Add(this.EPCTxtBx);
            this.Controls.Add(this.PCTxtBx);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label7);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagCommForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox EPCTxtBx;
        private System.Windows.Forms.TextBox PCTxtBx;
        private System.Windows.Forms.TextBox BCTxtBx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button VewSavBttn;
        private System.Windows.Forms.Button ClrBttn;
        private System.Windows.Forms.Button ScanBttn;
        private System.Windows.Forms.Label CommStatusLbl;
    }
}