namespace OnRamp
{
    partial class MainMenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenuForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BuildCfgLbl = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.TagRdBttn = new System.Windows.Forms.Button();
            this.TagWrBttn = new System.Windows.Forms.Button();
            this.TagInvtryBttn = new System.Windows.Forms.Button();
            this.TagRangeBttn = new System.Windows.Forms.Button();
            this.TagSrchBttn = new System.Windows.Forms.Button();
            this.TagCommBttn = new System.Windows.Forms.Button();
            this.TagAuthBttn = new System.Windows.Forms.Button();
            this.DBMgmtBttn = new System.Windows.Forms.Button();
            this.SysCfgBttn = new System.Windows.Forms.Button();
            this.MoreBttn = new System.Windows.Forms.Button();
            this.CertBttn = new System.Windows.Forms.Button();
            this.BCScnBttn = new System.Windows.Forms.Button();
            this.TagPermBttn = new System.Windows.Forms.Button();
            this.DevStatBttn = new System.Windows.Forms.Button();
            this.DefSettingBttn = new System.Windows.Forms.Button();
            this.VerLbl = new System.Windows.Forms.Label();
            this.RndBttn = new System.Windows.Forms.Button();
            this.btnAddLocation = new System.Windows.Forms.Button();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.btnSync = new System.Windows.Forms.Button();
            this.btnInventory = new System.Windows.Forms.Button();
            this.btnDispatch = new System.Windows.Forms.Button();
            this.btnFieldService = new System.Windows.Forms.Button();
            this.btnSyncFieldService = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnRecieveDispatch = new System.Windows.Forms.Button();
            this.cbGoOnline = new System.Windows.Forms.CheckBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Name = "pictureBox1";
            // 
            // BuildCfgLbl
            // 
            resources.ApplyResources(this.BuildCfgLbl, "BuildCfgLbl");
            this.BuildCfgLbl.Name = "BuildCfgLbl";
            // 
            // TagRdBttn
            // 
            resources.ApplyResources(this.TagRdBttn, "TagRdBttn");
            this.TagRdBttn.Name = "TagRdBttn";
            this.TagRdBttn.Tag = "3";
            this.TagRdBttn.Click += new System.EventHandler(this.OnTagRdClicked);
            this.TagRdBttn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // TagWrBttn
            // 
            resources.ApplyResources(this.TagWrBttn, "TagWrBttn");
            this.TagWrBttn.Name = "TagWrBttn";
            this.TagWrBttn.Tag = "9";
            this.TagWrBttn.Click += new System.EventHandler(this.OnTagWrClicked);
            this.TagWrBttn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // TagInvtryBttn
            // 
            resources.ApplyResources(this.TagInvtryBttn, "TagInvtryBttn");
            this.TagInvtryBttn.Name = "TagInvtryBttn";
            this.TagInvtryBttn.Tag = "6";
            this.TagInvtryBttn.Click += new System.EventHandler(this.OnTagInvtryClicked);
            this.TagInvtryBttn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // TagRangeBttn
            // 
            resources.ApplyResources(this.TagRangeBttn, "TagRangeBttn");
            this.TagRangeBttn.Name = "TagRangeBttn";
            this.TagRangeBttn.Click += new System.EventHandler(this.OnTagRangeClicked);
            // 
            // TagSrchBttn
            // 
            resources.ApplyResources(this.TagSrchBttn, "TagSrchBttn");
            this.TagSrchBttn.Name = "TagSrchBttn";
            this.TagSrchBttn.Tag = "4";
            this.TagSrchBttn.Click += new System.EventHandler(this.OnTagSearchClicked);
            this.TagSrchBttn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // TagCommBttn
            // 
            resources.ApplyResources(this.TagCommBttn, "TagCommBttn");
            this.TagCommBttn.Name = "TagCommBttn";
            this.TagCommBttn.Click += new System.EventHandler(this.OnTagCommClicked);
            // 
            // TagAuthBttn
            // 
            resources.ApplyResources(this.TagAuthBttn, "TagAuthBttn");
            this.TagAuthBttn.Name = "TagAuthBttn";
            this.TagAuthBttn.Click += new System.EventHandler(this.OnTagAuthenClicked);
            // 
            // DBMgmtBttn
            // 
            resources.ApplyResources(this.DBMgmtBttn, "DBMgmtBttn");
            this.DBMgmtBttn.Name = "DBMgmtBttn";
            this.DBMgmtBttn.Click += new System.EventHandler(this.OnDBMgmtClicked);
            // 
            // SysCfgBttn
            // 
            resources.ApplyResources(this.SysCfgBttn, "SysCfgBttn");
            this.SysCfgBttn.Name = "SysCfgBttn";
            this.SysCfgBttn.Click += new System.EventHandler(this.OnSysCfgClicked);
            // 
            // MoreBttn
            // 
            resources.ApplyResources(this.MoreBttn, "MoreBttn");
            this.MoreBttn.Name = "MoreBttn";
            this.MoreBttn.Tag = "";
            this.MoreBttn.Click += new System.EventHandler(this.OnMoreClicked);
            this.MoreBttn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // CertBttn
            // 
            resources.ApplyResources(this.CertBttn, "CertBttn");
            this.CertBttn.Name = "CertBttn";
            this.CertBttn.Click += new System.EventHandler(this.OnCertClicked);
            // 
            // BCScnBttn
            // 
            resources.ApplyResources(this.BCScnBttn, "BCScnBttn");
            this.BCScnBttn.Name = "BCScnBttn";
            this.BCScnBttn.Tag = "12";
            this.BCScnBttn.Click += new System.EventHandler(this.OnBCScnClicked);
            this.BCScnBttn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // TagPermBttn
            // 
            resources.ApplyResources(this.TagPermBttn, "TagPermBttn");
            this.TagPermBttn.Name = "TagPermBttn";
            this.TagPermBttn.Click += new System.EventHandler(this.OnTagPermClicked);
            // 
            // DevStatBttn
            // 
            resources.ApplyResources(this.DevStatBttn, "DevStatBttn");
            this.DevStatBttn.Name = "DevStatBttn";
            this.DevStatBttn.Tag = "13";
            this.DevStatBttn.Click += new System.EventHandler(this.OnDevStatClicked);
            this.DevStatBttn.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // DefSettingBttn
            // 
            resources.ApplyResources(this.DefSettingBttn, "DefSettingBttn");
            this.DefSettingBttn.Name = "DefSettingBttn";
            this.DefSettingBttn.Click += new System.EventHandler(this.OnDefSettingClicked);
            // 
            // VerLbl
            // 
            resources.ApplyResources(this.VerLbl, "VerLbl");
            this.VerLbl.Name = "VerLbl";
            // 
            // RndBttn
            // 
            resources.ApplyResources(this.RndBttn, "RndBttn");
            this.RndBttn.Name = "RndBttn";
            this.RndBttn.Click += new System.EventHandler(this.OnRnDClicked);
            // 
            // btnAddLocation
            // 
            resources.ApplyResources(this.btnAddLocation, "btnAddLocation");
            this.btnAddLocation.Name = "btnAddLocation";
            this.btnAddLocation.Tag = "7";
            this.btnAddLocation.Click += new System.EventHandler(this.btnAddLocation_Click);
            this.btnAddLocation.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnAddEmployee
            // 
            resources.ApplyResources(this.btnAddEmployee, "btnAddEmployee");
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Tag = "8";
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            this.btnAddEmployee.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnSync
            // 
            resources.ApplyResources(this.btnSync, "btnSync");
            this.btnSync.Name = "btnSync";
            this.btnSync.Tag = "5";
            this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
            this.btnSync.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnInventory
            // 
            resources.ApplyResources(this.btnInventory, "btnInventory");
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Tag = "11";
            this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
            this.btnInventory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnDispatch
            // 
            resources.ApplyResources(this.btnDispatch, "btnDispatch");
            this.btnDispatch.Name = "btnDispatch";
            this.btnDispatch.Tag = "2";
            this.btnDispatch.Click += new System.EventHandler(this.button1_Click);
            this.btnDispatch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnFieldService
            // 
            resources.ApplyResources(this.btnFieldService, "btnFieldService");
            this.btnFieldService.Name = "btnFieldService";
            this.btnFieldService.Tag = "1";
            this.btnFieldService.Click += new System.EventHandler(this.btnFieldService_Click);
            this.btnFieldService.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnSyncFieldService
            // 
            resources.ApplyResources(this.btnSyncFieldService, "btnSyncFieldService");
            this.btnSyncFieldService.Name = "btnSyncFieldService";
            this.btnSyncFieldService.Tag = "10";
            this.btnSyncFieldService.Click += new System.EventHandler(this.btnSyncFieldService_Click);
            this.btnSyncFieldService.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnBack
            // 
            resources.ApplyResources(this.btnBack, "btnBack");
            this.btnBack.Name = "btnBack";
            this.btnBack.Tag = "";
            this.btnBack.Click += new System.EventHandler(this.OnMoreClicked);
            this.btnBack.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            // 
            // btnRecieveDispatch
            // 
            resources.ApplyResources(this.btnRecieveDispatch, "btnRecieveDispatch");
            this.btnRecieveDispatch.Name = "btnRecieveDispatch";
            this.btnRecieveDispatch.Tag = "14";
            this.btnRecieveDispatch.Click += new System.EventHandler(this.btnRecieveDispatch_Click);
            // 
            // cbGoOnline
            // 
            resources.ApplyResources(this.cbGoOnline, "cbGoOnline");
            this.cbGoOnline.Name = "cbGoOnline";
            this.cbGoOnline.CheckStateChanged += new System.EventHandler(this.cbGoOnline_CheckStateChanged);
            // 
            // lblVersion
            // 
            resources.ApplyResources(this.lblVersion, "lblVersion");
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Location = new System.Drawing.Point(132, 2);
            this.lblVersion.Size = new System.Drawing.Size(100, 17);
            // 
            // MainMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.cbGoOnline);
            this.Controls.Add(this.btnRecieveDispatch);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.SysCfgBttn);
            this.Controls.Add(this.CertBttn);
            this.Controls.Add(this.TagPermBttn);
            this.Controls.Add(this.DBMgmtBttn);
            this.Controls.Add(this.TagCommBttn);
            this.Controls.Add(this.TagRangeBttn);
            this.Controls.Add(this.TagAuthBttn);
            this.Controls.Add(this.VerLbl);
            this.Controls.Add(this.BuildCfgLbl);
            this.Controls.Add(this.RndBttn);
            this.Controls.Add(this.DefSettingBttn);
            this.Controls.Add(this.MoreBttn);
            this.Controls.Add(this.BCScnBttn);
            this.Controls.Add(this.btnSync);
            this.Controls.Add(this.TagRdBttn);
            this.Controls.Add(this.TagSrchBttn);
            this.Controls.Add(this.btnDispatch);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnInventory);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.DevStatBttn);
            this.Controls.Add(this.btnSyncFieldService);
            this.Controls.Add(this.btnFieldService);
            this.Controls.Add(this.TagInvtryBttn);
            this.Controls.Add(this.TagWrBttn);
            this.Controls.Add(this.btnAddLocation);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainMenuForm";
            this.Load += new System.EventHandler(this.OnMainMenuLoad);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnMainMenuClosing);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnKeyUp);
            this.Resize += new System.EventHandler(this.OnFormResize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label BuildCfgLbl;
        private System.Windows.Forms.Button TagRdBttn;
        private System.Windows.Forms.Button TagWrBttn;
        private System.Windows.Forms.Button TagInvtryBttn;
        private System.Windows.Forms.Button TagRangeBttn;
        private System.Windows.Forms.Button TagSrchBttn;
        private System.Windows.Forms.Button TagCommBttn;
        private System.Windows.Forms.Button TagAuthBttn;
        private System.Windows.Forms.Button DBMgmtBttn;
        private System.Windows.Forms.Button SysCfgBttn;
        private System.Windows.Forms.Button MoreBttn;
        private System.Windows.Forms.Button CertBttn;
        private System.Windows.Forms.Button BCScnBttn;
        private System.Windows.Forms.Button TagPermBttn;
        private System.Windows.Forms.Button DevStatBttn;
        private System.Windows.Forms.Button DefSettingBttn;
        private System.Windows.Forms.Label VerLbl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button RndBttn;
        private System.Windows.Forms.Button btnAddLocation;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Button btnSync;
        private System.Windows.Forms.Button btnInventory;
        private System.Windows.Forms.Button btnDispatch;
        private System.Windows.Forms.Button btnFieldService;
        private System.Windows.Forms.Button btnSyncFieldService;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnRecieveDispatch;
        private System.Windows.Forms.CheckBox cbGoOnline;
        private System.Windows.Forms.Label lblVersion;
    }
}