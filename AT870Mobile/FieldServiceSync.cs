using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class FieldServiceSync : Form
    {
        public delegate void stateHandler(Int64 status);

        public FieldServiceSync()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt64(cboLoc.SelectedValue) != 0)
                {

                    Sync ss = new Sync();
                    ss.SyncFieldServiceData(Convert.ToInt64(cboLoc.SelectedValue));   


                    // Open new Tag Write window
                    //frmInventory InvFm = new frmInventory();
                    //InvFm.searchedData = dtResult;
                    //InvFm.Show();
                    // disable form until this new form closed
                    syncProgress.Value = syncProgress.Maximum;

                    if (ss.strError.Length != 0)
                    {
                        MessageBox.Show("Synchronization found Following errors. \r" + ss.strError);
                    }
                    else
                    {
                        MessageBox.Show("Synchronization completed sucessfully.");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Please select location first.");
                }
                //this.Enabled = false;
                //InvFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                MessageBox.Show("Error occured " + ep.Message.ToString());
                Logger.LogError(ep.Message); 
            }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void FieldServiceSync_Load(object sender, EventArgs e)
        {            
            Cursor.Current = Cursors.WaitCursor;
            #region"Set Location Combo"
            DataTable dtList = new DataTable();
            dtList = Locations.getLocationList();   

            DataRow dr = dtList.NewRow();
            dr["ID_Location"] = 0;
            dr["Name"] = "--Select--";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            dr = dtList.NewRow();
            dr["ID_Location"] = -1;
            dr["Name"] = "--ALL--";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboLoc.ValueMember = "ID_Location";
            cboLoc.DisplayMember = "Name";
            cboLoc.DataSource = dtList;

            cboLoc.SelectedValue = 0;
            #endregion
            Cursor.Current = Cursors.Default;

            ProgressStatus.progressState += new ProgressStatus.progressStateHandler(ProgressStatus_progressState);

        }

        void ProgressStatus_progressState(long status)
        {
            if (this.InvokeRequired)
            {
                //ProgressStatus_progressState(status);
                this.Invoke(new stateHandler(this.ProgressStatus_progressState), new Object[] { status });
                return;
            }
            try
            {
                syncProgress.Value = (int)status;
            }
            catch { }
            return;
        }

        private void FieldServiceSync_Closing(object sender, CancelEventArgs e)
        {
            ProgressStatus.progressState -= new ProgressStatus.progressStateHandler(ProgressStatus_progressState);
        }

        private void label2_ParentChanged(object sender, EventArgs e)
        {

        }

        private void lnkLocName_Click(object sender, EventArgs e)
        {

        }

        private void btnSelLoc_Click(object sender, EventArgs e)
        {

        }

        

    }
}