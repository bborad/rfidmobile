using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;
using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class CertificationForm : Form
    {
        enum OpState
        {
            idle,
            running,
        }
        const uint refMilliSecsPerElem = 4000;
        const uint refLnkFreq = 120000;
        uint MilliSecsPerElem = refMilliSecsPerElem;
#if false
        const uint FullBufSz = 16;
        const uint RefillBufSz = 8;
#else
        const uint FullBufSz = 3;
        const uint RefillBufSz = 1;
#endif
        uint RndDatCycTime
        {
            get
            {
                return MilliSecsPerElem * RefillBufSz; // secs
            }
        }

        public CertificationForm()
        {
            InitializeComponent();

            InitRndDatState();
            CWBttnInitState();
        }

        private void InitRndDatState()
        {
            OpState state = OpState.idle;

            RndDatStartBttn.Tag = state; // boxing
        }

        private OpState RndDatState()
        {
            OpState State = OpState.idle;

            if (RndDatStartBttn.Tag is OpState)
            {
                State = (OpState)RndDatStartBttn.Tag;
            }
            else
                throw new ApplicationException("RndData Start button Tag not of OpState type");

            return State;
        }

        private void SetRndDatState(OpState state)
        {
            switch (state)
            {
                case OpState.idle:
                    RndDatStartBttn.Text = "Start";
                    break;
                case OpState.running:
                    RndDatStartBttn.Text = "Running";
                    RndDatStartBttn.Enabled = false; // cannot stop Random Data generation alone
                    break;
            }
            RndDatStartBttn.Tag = state; // boxing

        }

        private uint CntLeftToRestartRndData = 0;

        private void OnRndDatTmrTick(object sender, EventArgs e)
        {
            System.Windows.Forms.Timer Tmr = (System.Windows.Forms.Timer)sender;
            CntLeftToRestartRndData--;
            RndDatStatusLbl.Text = ((double)(CntLeftToRestartRndData * Tmr.Interval/1000D)).ToString("N2") + " secs";
            if (CntLeftToRestartRndData == 0)
            {
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                //Reader Rdr = ReaderFactory.GetReader();
                Reader Rdr = ReaderFactory.GetReader();
            gen_rand_data:
                Rdr.Size = RefillBufSz * 2;
                if (!Rdr.RFGenerateRandomData()) // Refill more to compensate(?)
                {
                    if ((uint)Rdr.LastErrCode == 0x80040000)
                    {
                        Rdr.RingingID=RingTone.T5;
                        Rdr.Svol=SoundVol.High;
                        Rdr.MelodyRing();
                        Program.ShowWarning("Strange error code: " + Rdr.LastErrCode + "\nTry again");
                        goto gen_rand_data;
                    }
                    MessageBox.Show("Generate Random Data Failed!" + "\n" + Rdr.LastErrCode.ToString("F"));
                    RndDatTmr.Enabled = false; // stop the timer
                    SetRndDatState(OpState.idle);
                }
                else
                    CntLeftToRestartRndData = (uint)(RndDatCycTime / Tmr.Interval); // Restart count
#if DEBUG
                Rdr.Svol = SoundVol.High;
                Rdr.BuzzerBeep(1);
#endif
            }
        }

        private void OnRndDatStartClicked(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.Size = FullBufSz;
            switch (RndDatState())
            {
                case OpState.idle:
                    if (Rdr.RFGenerateRandomDataSetup())
                    {
                        if (Rdr.RFGenerateRandomData())
                        {
                            CntLeftToRestartRndData = (uint)(RndDatCycTime / RndDatTmr.Interval); // Restart count
                            RndDatTmr.Enabled = true;
                            SetRndDatState(OpState.running);
                        }
                        else
                        {
                            MessageBox.Show("Start Failed!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Setup failed. Please try again later");
                    }
                    break;
                case OpState.running:
#if false
                    if (!Rdr.RFStopRandomData())
                    {
                        MessageBox.Show("Stop Failed!");
                    }
                    else
                    {
                        SetRndDatState(OpState.idle);
                        RndDatTmr.Enabled = false;
                    }
#else
                    MessageBox.Show("Button supposed to be disabled", "Programming Error");
#endif
                    break;
            }
            // It takes a while (esp. Stop) to actually
            // take effect (based on observation from
            // Spectrum-Analyzer
            CWOnOffBttn.Enabled = false; // debounce
            debounceTmr.Enabled = true;
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (RndDatState() != OpState.idle || CWBttnState() != CWState.Off)
            {
                MessageBox.Show("Please stop running operation first");
                e.Cancel = true;
            }
        }

        private void OnDebounceTmrTick(object sender, EventArgs e)
        {
            CWOnOffBttn.Enabled = true;
            debounceTmr.Enabled = false;
        }

        #region Temp Display
        private void DisplayCurrTemp(UInt16 amb, UInt16 xcvr, UInt16 pwramp)
        {
            AmbTempLbl.Text = amb.ToString();
            XcvrTempLbl.Text = xcvr.ToString();
            PATempLbl.Text = pwramp.ToString();
        }
        #endregion

        #region CW Button State
        enum CWState
        {
            Off,
            On
        }
        private void CWBttnInitState()
        {
            CWState state = CWState.Off;

            CWOnOffBttn.Tag = state;
        }

        private CWState CWBttnState()
        {
            CWState state = CWState.Off;

            if (CWOnOffBttn.Tag is CWState)
            {
                state = (CWState)CWOnOffBttn.Tag;
            }
            else
                throw new ApplicationException("CWOnOffButton Tag is not CWState type");

            return state;
        }

        private void CWBttnSetState(CWState newState)
        {
            CWOnOffBttn.Tag = newState;

            switch (newState)
            {
                case CWState.Off:
                    CWOnOffBttn.Text = "Turn On CW";
                    SetRndDatState(OpState.idle);
                    break;
                case CWState.On:
                    CWOnOffBttn.Text = "Turn Off CW";
                    break;
            }
        }

        #endregion
        private void OnCWOnOffBttnClicked(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
           // Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            switch (CWBttnState())
            {
                case CWState.Off:
                    if (RndDatState() == OpState.running)
                        MessageBox.Show("Rand Data is still generating!", "Error");
                    else
                    {
                        Rdr.CarrierWaveOn();
                        CWBttnSetState(CWState.On);
                        RndDatStartBttn.Enabled = true;
                        TempDspyTmr.Enabled = true;
                    }
                    break;
                case CWState.On:
                    RndDatTmr.Enabled = false;
                    Rdr.CarrierWaveOff();
                    TempDspyTmr.Enabled = false;
                    CWBttnSetState(CWState.Off);
                    CWOnOffBttn.Enabled = false; // debounce
                    debounceTmr.Enabled = true;
                    break;
            }
        }

        private void OnTempTmrTick(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            //ushort amb, xcvr, pamp;
            if (!Rdr.GetCurrTemp())
                MessageBox.Show("GetCurrTemp() return error", "Error");
            else
                DisplayCurrTemp(Rdr.amb, Rdr.xcvr, Rdr.pamp);
        }

        
        private void OnFormClosed(object sender, EventArgs e)
        {
            debounceTmr.Enabled = false; // just in case

            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            ApplyCurrFreq();
        }


        /// <summary>
        /// Setting Frequency (Change frequency table) without running 
        /// TagOperation does not actually 'load' the frequency into use.
        /// We get around this by running an inventory-once operation
        /// </summary>
        private void ApplyCurrFreq()
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
           // Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            Program.RefreshStatusLabel(this, "Applying selected frequency...");
            //Rdr.InventoryOpStEvent += InvtryOpStEvtHandler;
            Rdr.RegisterInventoryOpStEvent(InvtryOpStEvtHandler);
            this.Enabled = false; // freeze the form
            if (!Rdr.TagInventoryOnceStart(false))
            {
                this.Enabled = true; // thaw the form
                Program.RefreshStatusLabel(this, null);
                //Rdr.InventoryOpStEvent -= InvtryOpStEvtHandler;
                Rdr.UnregisterInventoryOpStEvent(InvtryOpStEvtHandler);
                Program.ShowError( "Tag Inventory Once Failed\n"
                    + " Frequency table changes might not be applied");
            }
        }

        private void InvtryOpStEvtHandler(object sender, InvtryOpEventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    this.Enabled = true; // thaw the form
                    Program.RefreshStatusLabel(this, "Ready");
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            MessageBox.Show(e.msg, "Tag Inventory Stopped with Error");
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped)
                    {
                        Program.ShowMacError(HotKeyEvtHandler);
                    }
                    break;
                case InvtryOpStatus.error:
                    Program.ShowWarning("Tag Inventory Error:\n" + e.msg);
                    break;
            }

            if (e.status == InvtryOpStatus.stopped || e.status == InvtryOpStatus.errorStopped 
                || e.status == InvtryOpStatus.macErrorStopped)
            {
                //Rdr.InventoryOpStEvent -= InvtryOpStEvtHandler;
                Rdr.UnregisterInventoryOpStEvent(InvtryOpStEvtHandler);
                // Get Current Profile Num
                this.Enabled = false; // freeze until Profile Number Received
                Program.RefreshStatusLabel(this, "Getting Link Profile Information...");
                UInt32 ProfNum = 0;
                Rdr.ProfNum = ProfNum;
                if (!Rdr.LinkProfNumGet())
                {
                    this.Enabled = true;
                    Program.RefreshStatusLabel(this, "Ready");
                    Program.ShowError("Error getting Link Profile number currently in use: " + Rdr.LastErrCode.ToString("F"));
                }
                else
                {
                    PrfNumLbl.Text = Rdr.ProfNum.ToString();
                    // Get LnkPrf Details
                    //Rdr.LnkProfInfoGetNotification +=LnkPrfInfoNotify;
                    Rdr.RegisterLnkProfInfoGetNotificationEvent(LnkPrfInfoNotify);
                    if (!Rdr.LinkProfInfoGet())
                    {
                        this.Enabled = true;
                        Program.RefreshStatusLabel(this, "Ready");
                        Program.ShowError("Unable to get Link Profile(" + ProfNum.ToString() + ") Details");
                    }
                }
            }
        }

        private void LnkPrfInfoNotify(bool succ, ref RFID_RADIO_LINK_PROFILE lnkPrf, String errMsg)
        {
            if (succ)
            {
                // The Higher the Freq, the Shorter the period, inversely proportional
                // to SecsPerElm
                double recipRatio = (double)refLnkFreq / (double)lnkPrf.iso18K6C.trLinkFrequency;
                MilliSecsPerElem = (uint)(refMilliSecsPerElem * recipRatio);
            }
            else
            {
                Program.ShowError("Error getting Link Profile(" + lnkPrf.profileId.ToString() + ") Details.\n" + errMsg);
            }

            this.Enabled = true;
            Program.RefreshStatusLabel(this, "Ready");
        }

        #region F1/F4/F5 Hotkey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (down)
                    {
                        if (RndDatState() == OpState.idle && CWBttnState() == CWState.Off)
                        {
                            AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                        }
                    }
                    break;
                case eVKey.VK_F1:
                    if ((down) && (RndDatState() == OpState.idle && CWBttnState() == CWState.Off))
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    break;
            }
        }
        #endregion

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion
    }
}