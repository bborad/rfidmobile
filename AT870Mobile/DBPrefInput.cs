using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using System.IO;

namespace OnRamp
{
    public partial class DBPrefInput : UserControl
    {
        private String DefaultRFIDDataDir = String.Empty;

        public DBPrefInput()
        {
            InitializeComponent();
            DefaultRFIDDataDir = Environment.GetFolderPath(
                Environment.SpecialFolder.ApplicationData) + @"\RFID\RFID Data";
        }

        public DBPref.DBFileFmt SelectedFmt
        {
            set
            {
                switch (value)
                {
                    case DBPref.DBFileFmt.CSV:
                        CSVBttn.Checked = true;
                        break;
                    case DBPref.DBFileFmt.MSSQLCE_SDF:
                        SDFBttn.Checked = true;
                        break;
                    case DBPref.DBFileFmt.XML:
                        XMLBttn.Checked = true;
                        break;
                }
            }
            get
            {
                DBPref.DBFileFmt Fmt = DBPref.DBFileFmt.MSSQLCE_SDF;
                if (SDFBttn.Checked)
                    Fmt = DBPref.DBFileFmt.MSSQLCE_SDF;
                else if (XMLBttn.Checked)
                    Fmt = DBPref.DBFileFmt.XML;
                else if (CSVBttn.Checked)
                    Fmt = DBPref.DBFileFmt.CSV;
                return Fmt;
            }
        }
    
        public String LocalPath
        {
            set
            {
                LclPathTxtBx.Text = value != null ? value : String.Empty;
            }
            get
            {
                return LclPathTxtBx.Text;
            }
        }

        public String RemotePath
        {
            set
            {
                RmtURITxtBx.Text = value != null ? value : String.Empty;
            }
            get
            {
                return RmtURITxtBx.Text;
            }
        }

        // User choose a Local File (Both Open/SavFileDialog are not totally
        // matching our needs. TBD (Write custom File/Folder chooser)
        private void OnBrwseBttnClicked(object sender, EventArgs e)
        {
            SaveFileDialog Dlg = new SaveFileDialog();
            Dlg.InitialDirectory = (String.IsNullOrEmpty(LclPathTxtBx.Text)) ?
                DefaultRFIDDataDir : Path.GetDirectoryName(LclPathTxtBx.Text);
            Dlg.FileName = (String.IsNullOrEmpty(LclPathTxtBx.Text)) ?
                String.Empty : Path.GetFileName(LclPathTxtBx.Text);
            Dlg.Filter =  "CSV Files (*.csv)|*.csv|SDF Files (*.sdf)|*.sdf|XML Files (*.xml)|*.xml|All Files (*.*)|*.*";
            switch (SelectedFmt)
            {
                case DBPref.DBFileFmt.CSV:
                    Dlg.FilterIndex = 1;
                    break;
                case DBPref.DBFileFmt.MSSQLCE_SDF:
                    Dlg.FilterIndex = 2;
                    break;
                case DBPref.DBFileFmt.XML:
                    Dlg.FilterIndex = 3;
                    break;
            }
            if (Dlg.ShowDialog() == DialogResult.OK)
            {
                // Update the TextBox with user input
                LclPathTxtBx.Text = Dlg.FileName;
            }
            Dlg.Dispose();
        }

        // User choose a Remote URI (could be a Dir or File or CGI)
        private void OnRemoteBttnClicked(object sender, EventArgs e)
        {
            // Parse current string in 'Remote Path TextBox'
            URIComposeForm URIFm = new URIComposeForm();
            // Setup URIComposeForm accordingly
            URIFm.URIStr = RmtURITxtBx.Text;
            // Ask user to input
            if (URIFm.ShowDialog() == DialogResult.OK)
            {
                // Update 'RemotePath Text Box'
                String ComposedURIStr = URIFm.URIStr;
                if (String.IsNullOrEmpty(ComposedURIStr))
                {
                    Program.ShowWarning("Composed URI string is incomplete/incorrect.\nKeeping Remote Path unchanged.");
                }
                else
                    RmtURITxtBx.Text = ComposedURIStr;
            }
            URIFm.Dispose(); // the form was just 'hidden' (even if the Form is 'X' closed)
        }


    }
}
