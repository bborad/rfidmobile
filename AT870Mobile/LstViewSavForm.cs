using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using System.IO;

namespace OnRamp
{
    public partial class LstViewSavForm : Form
    {

        public delegate void RowRemovedNotify (int rowIdx);
        private RowRemovedNotify callerRowRemovedNotify = null;
        
        public LstViewSavForm(bool showSavBttn)
        {
            InitializeComponent();

            SavBttn.Visible = showSavBttn;
        }

        public LstViewSavForm(bool showSavBttn, RowRemovedNotify removeNotify)
        {
            InitializeComponent();

            SavBttn.Visible = showSavBttn;

            callerRowRemovedNotify = removeNotify;

            DelBttn.Visible = true;
        }

        public String Title
        {
            set
            {
                this.Text = value;
            }
            get
            {
                return this.Text;
            }
        }


        public void SetColumns(String[] hdrs, int[] widths)
        {
            LstV.Columns.Clear();
            // Index Column
            LstV.Columns.Add(String.Empty, 25, HorizontalAlignment.Left);
            // User Columns
            uint NumCols = (uint)Math.Min(hdrs.Length, widths.Length);
            for (int c = 0; c < NumCols; c++)
                LstV.Columns.Add(hdrs[c], widths[c], HorizontalAlignment.Left);
        }

        public void AddRow(String[] cells)
        {
            AddRow(cells, Color.White);
        }

        public void AddRow(String[] cells, Color backColor)
        {
            // Assuming that there is no removal of rows
            int Idx = LstV.Items.Count + 1;
            String[] PrependCells = new String[1 + cells.Length];
            PrependCells[0] = Idx.ToString();
            cells.CopyTo(PrependCells, 1);
            ListViewItem Row =  LstV.Items.Add(new ListViewItem(PrependCells));
            Row.BackColor = backColor;
            RefreshCntLbl();
        }

        private void RefreshCntLbl()
        {
            RowCntLbl.Text = LstV.Items.Count.ToString();
        }

        #region Save To File
        private static String LastSaveDir = null;
        private void OnSavBttnClicked(object sender, EventArgs e)
        {
            if (LstV.Items != null && LstV.Items.Count > 0)
            {
                SaveFileDialog SaveFileDlg = new SaveFileDialog();

                SaveFileDlg.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
                SaveFileDlg.FilterIndex = 1;
                if (LastSaveDir != null)
                    SaveFileDlg.InitialDirectory = LastSaveDir;
                DialogResult Res = SaveFileDlg.ShowDialog();
                if (Res == DialogResult.OK)
                {
                    try
                    {
                        LastSaveDir = Path.GetDirectoryName(SaveFileDlg.FileName);
                        SaveListToCSVFile(SaveFileDlg.FileName);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Save to File Failed");
                    }
                }
            }
            else
                MessageBox.Show("Please scan items first.");
        }

        // return false if not written
        private bool SaveListToCSVFile(String DestFile)
        {
            bool Written = false;
            const String TxtIndicator = "'"; // Single-Quotation Mark

            using (StreamWriter sw = File.CreateText(DestFile))
            {
                foreach (ListViewItem item in LstV.Items)
                {
                    // Do not write Index Column: sw.Write (TxtIndicator + item.Text + TxtIndicator);
                    for (int i = 1; i < item.SubItems.Count; i++)
                        sw.Write(TxtIndicator + item.SubItems[i].Text + TxtIndicator + ",");
                    sw.Write("\r\n");
                }
                sw.Close();
                Written = true;
            }
            return Written;
        }

        #endregion

        private void OnDelBttnClicked(object sender, EventArgs e)
        {
            if (LstV.SelectedIndices != null && LstV.SelectedIndices.Count > 0)
            {
                // TBD: will values in SelectedIndices be adjusted when
                // one of the selected row is removed from ListView?
                for (int i = 0; i < LstV.SelectedIndices.Count; i++)
                {
                    // save to local var because SelectedIndices changes after RemovedAt()
                    int SelIdx = LstV.SelectedIndices[i]; 
                    LstV.Items.RemoveAt(SelIdx);
                    if (callerRowRemovedNotify != null)
                        callerRowRemovedNotify(SelIdx);
                }
            }
            else
            {
                Program.ShowWarning("Please select a row to delete");
            }
        }

    }
}