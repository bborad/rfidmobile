using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;
//using ClsReaderLib.Devices.Barcode;
using HHDeviceInterface.BarCode;

namespace OnRamp
{
    public partial class BarCodeScanSelectDlg : Form
    {
        private static bool ContScan = true; // Continue scanning after capture

        public delegate void BarCodeScannedNotify(String[] barcodes, int selIdx);
        private BarCodeScannedNotify closeNotify;

        private void InitializeDlg()
        {
            InitializeComponent();
            ScanBttnInitState();
        }

        public BarCodeScanSelectDlg(BarCodeScannedNotify closeNotify)
        {
            InitializeDlg();
            
            this.closeNotify = closeNotify;

            // List is Empty
        }

        public BarCodeScanSelectDlg(String[] barcodes, int selIdx, BarCodeScannedNotify closeNotify)
        {
            InitializeDlg();
            
            this.closeNotify = closeNotify;

            // Load BarCode Strings to List
            BarCodeListLoad(barcodes, selIdx);
        }

        #region Scan Button routines
        enum ScnState
        {
            Idle,
            Running,
            Stopping,
        }

        private void ScanBttnInitState()
        {
            ScnState state = ScnState.Idle;

            ScanBttn.Tag = state;
        }

        private ScnState ScanBttnState()
        {
            ScnState state = ScnState.Idle;

            if (ScanBttn.Tag is ScnState)
            {
                state = (ScnState)ScanBttn.Tag;
            }
            else
                throw new ApplicationException("Scan Button Tag not of  type ScnState");

            return state;
        }

        private void ScanBttnSetState(ScnState newState)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BCScanForm));

            ScanBttn.Tag = newState;
            switch (newState)
            {
                case ScnState.Idle:
                    ScanBttn.Text = resources.GetString("ScanBttn.Text");
                    ScanBttn.Enabled = true;
                    OKBttn.Enabled = true;
                    CancelBttn.Enabled = true;
                    break;
                case ScnState.Running:
                    ScanBttn.Text = "Stop";
                    OKBttn.Enabled = false;
                    CancelBttn.Enabled = false;
                    break;
                case ScnState.Stopping:
                    ScanBttn.Enabled = false;
                    break;
            }

        }

        private void OnScanBttnClicked(object sender, EventArgs e)
        {
            //BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
            switch (ScanBttnState())
            {
                case ScnState.Idle:
                    BarCodeListClr(); // Clear List
                    Program.RefreshStatusLabel(this, "Scanning...");
                    ScanBttnSetState(ScnState.Running);
                    if (this.InvokeRequired)
                    {
                        Rdr.notifyee = this;
                    }
                    Rdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
                    //ClsReaderLib.Devices.Barcode.BarCodeRdr.CodeRcvdNotify aa = new BarCodeRdr.CodeRcvdNotify(BarCodeNotify);
                   // Form f = (Form)this;
                    //Rdr.ScanStart(aa, f);
                    Rdr.ScanStart();
                    break;

                case ScnState.Running:
                    Program.RefreshStatusLabel(this, "Stopping Barcode Scan...");
                    ScanBttnSetState(ScnState.Stopping);
                    if (this.InvokeRequired)
                    {
                        Rdr.stopNotifee = this;
                    }
                    Rdr.RegisterStopNotificationEvent(ScanStopped);
                   // if (Rdr.ScanTryStop(ScanStopped, this)) // use TryStop to avoid deadlock
                    if (Rdr.ScanTryStop())
                    {
                        Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
                        ScanBttnSetState(ScnState.Idle);
                    }
                    else
                    {
                        // Scanner busy at the moment
                        // Leave the 'Stopping' State as to return 'not-to-continue'
                        //  on the next bar-code read notification
                        // MessageBox.Show("Scanner Busy", "Stop Denied");
                    }
                    break;
            }
        }

        #endregion

        #region BarCode List

        private void BarCodeListToArray(out String[] barcodes, out int selIdx)
        {
            barcodes = new String[0];
            selIdx = -1;
            if (BCListV.Items != null || BCListV.Items.Count > 0)
            {

                barcodes = new String[BCListV.Items.Count];
                for (int i = 0; i < BCListV.Items.Count; i++)
                    barcodes[i] = String.Copy (BCListV.Items[i].Text);
                selIdx = (BCListV.SelectedIndices != null && BCListV.SelectedIndices.Count > 0)? BCListV.SelectedIndices[0] : -1;
            }
        }

        private void BarCodeListLoad(String[] barcodes, int selIdx)
        {
            // Assuming that the List is Empty
            BCListV.BeginUpdate();
            for (int i = 0; i < barcodes.Length; i++)
            {
                ListViewItem item = new ListViewItem(barcodes[i]);
                BCListV.Items.Add(item);
                if (i == selIdx)
                    item.Selected = true;
            }
            
            BCListV.EndUpdate();
            RefreshCntLabel(BCListV);
        }


        private ListViewItem BarCodeListAdd(String BarCode)
        {
            ListViewItem item = null;

            foreach (ListViewItem row in BCListV.Items)
            {
                if (String.Compare(BarCode, row.Text, false) == 0) // Case Matters(?)
                {
                    item = row;
                    break;
                }
            }

            if (item != null) // If already exists, move to beginning
            {
                BCListV.BeginUpdate();
                if (item.Index > 0)
                {
                    BCListV.Items.Remove(item); // assuming just detach
                    BCListV.Items.Insert(0, item);
                }
                BCListV.EndUpdate();
            }
            else // Otherwise, Add to ListView (Top)
            {
                item = new ListViewItem(BarCode);
                BCListV.Items.Insert(0, item);
                RefreshCntLabel(BCListV);
            }

            return item;
        }

        private void BarCodeListClr()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BCScanForm));
            BCListV.Clear(); // re-initialize columns afterwards
            if (BCListV.Columns.Count == 0)
            {
                BCListV.Columns.Add(resources.GetString("columnHeader2.Text"), -2, HorizontalAlignment.Left);
            }
            RefreshCntLabel(BCListV);
        }

        private void FlashRow(ListViewItem row)
        {
            Color OrigColor = row.BackColor;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            row.BackColor = Color.Green;
            row.ListView.Refresh();
            Rdr.Svol = UserPref.GetInstance().SndVol;
            Rdr.BuzzerBeep(1);
            System.Threading.Thread.Sleep(150);
            row.BackColor = OrigColor;
        }

        #endregion

        #region Cnt Label
        private void RefreshCntLabel(ListView listV)
        {
            int NumRows =  listV.Items != null ? listV.Items.Count : 0;
            CntLbl.Text = NumRows.ToString();
        }
        #endregion
        #region BCodeRdr i/f routines
        private bool BarCodeNotify(bool succ, String bcStr, String errMsg)
        {
            if (succ)
            {
                Program.RefreshStatusLabel(this, "Barcode Captured...");
                ListViewItem Row = BarCodeListAdd(bcStr);
                // Blink something to indicate?
                FlashRow(Row);
            }
            else
            {
                Program.RefreshStatusLabel(this, "Barcode Capture Error...");
                MessageBox.Show(errMsg, "Bar Code Reader Error");
            }

            bool ToCont = (ContScan) && (ScanBttnState() != ScnState.Stopping);
            if (!ToCont)
            {
                Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
                ScanBttnSetState(ScnState.Idle);
            }
            return ToCont;
        }

        private void ScanStopped()
        {
            Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
            ScanBttnSetState(ScnState.Idle);
        }

        #endregion

        private void OnCancelBttnClicked(object sender, EventArgs e)
        {

        }

        private void OnOKBttnClicked(object sender, EventArgs e)
        {

        }

        #region Form routines
     
        private void OnFormLoad(object sender, EventArgs e)
        {
            // Set up to receive HotKey event
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (ScanBttnState() == ScnState.Running)
            {
                MessageBox.Show("Please stop barcode scanning process first", "Request Denied");
                e.Cancel = true;
            }
            else
            {
                if (closeNotify != null)
                {
                    String[] BCArr;
                    int SelIdx;
                    BarCodeListToArray(out BCArr, out SelIdx);
                    closeNotify(BCArr, SelIdx);
                }
            }
        }

        #endregion

        #region Hotkey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            if (keyCode == eVKey.VK_F11)
            {
                if (down)
                {
                    // fake 'Start' key press if not already running
                    if (ScanBttnState() == ScnState.Idle)
                    {
                        OnScanBttnClicked(this, null);
                    }
                }
                else // up
                {
                    if (ScanBttnState() == ScnState.Running)
                    {
                        // Stop!
                        OnScanBttnClicked(this, null);
                    }
                }
            }
            // ignore other keys
        }
        #endregion
    }
}