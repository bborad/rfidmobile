namespace OnRamp
{
    partial class frmDispatchDoc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.lstDocs = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.DispatchNo = new System.Windows.Forms.ColumnHeader();
            this.TagID = new System.Windows.Forms.ColumnHeader();
            this.btnUpload = new System.Windows.Forms.Button();
            this.btnView = new System.Windows.Forms.Button();
            this.uploadCSV = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstDocs
            // 
            this.lstDocs.Columns.Add(this.columnHeader1);
            this.lstDocs.Columns.Add(this.DispatchNo);
            this.lstDocs.Columns.Add(this.TagID);
            this.lstDocs.Location = new System.Drawing.Point(0, 0);
            this.lstDocs.Name = "lstDocs";
            this.lstDocs.Size = new System.Drawing.Size(235, 212);
            this.lstDocs.TabIndex = 52;
            this.lstDocs.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 30;
            // 
            // DispatchNo
            // 
            this.DispatchNo.Text = "Dispatch No";
            this.DispatchNo.Width = 125;
            // 
            // TagID
            // 
            this.TagID.Text = "TagID";
            this.TagID.Width = 150;
            // 
            // btnUpload
            // 
            this.btnUpload.Location = new System.Drawing.Point(120, 244);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(114, 23);
            this.btnUpload.TabIndex = 53;
            this.btnUpload.Text = "Upload Slip";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // btnView
            // 
            this.btnView.Location = new System.Drawing.Point(2, 244);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(114, 23);
            this.btnView.TabIndex = 54;
            this.btnView.Text = "Check Selected";
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // uploadCSV
            // 
            this.uploadCSV.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(120, 218);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 23);
            this.button1.TabIndex = 55;
            this.button1.Text = "Check Any";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(2, 218);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 23);
            this.button2.TabIndex = 56;
            this.button2.Text = "Details";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmDispatchDoc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.btnUpload);
            this.Controls.Add(this.lstDocs);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDispatchDoc";
            this.Text = "Dispatch ";
            this.Load += new System.EventHandler(this.frmDispatchDoc_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lstDocs;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader DispatchNo;
        private System.Windows.Forms.ColumnHeader TagID;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.Button btnView;
        private System.Windows.Forms.OpenFileDialog uploadCSV;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}