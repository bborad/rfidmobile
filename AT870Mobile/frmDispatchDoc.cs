/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Dispatch functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmDispatchDoc : Form
    {
        public frmDispatchDoc()
        {
            InitializeComponent();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            openDetailForm();
        }

        private void openDetailForm()
        {
            try
            {
            //Open the scan form which scan the document Tag and AssetTagID, and verify the document no
            if(lstDocs.SelectedIndices !=null && lstDocs.SelectedIndices.Count >0)
            {
                frmVerifyDocs frmVerify = new frmVerifyDocs();
                TagInfo T = new TagInfo(lstDocs.Items[lstDocs.SelectedIndices[0]].SubItems[2].Text); 
                if(T.isDocumentTag())
                {
                    frmVerify.SelectedDocumentID = T.DataKey; 
                    frmVerify.Show();   
                    // disable form until this new form closed
                    this.Enabled = false;
                    frmVerify.Closed += new EventHandler(TagRdFm_Closed);
                }
                else
                {
                    Program.ShowError("Data not retrived sucessfully.");  
                }
            }
            else
            {
                Program.ShowError("Please select any document first.");  
            }
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
        }

        void TagRdFm_Closed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
            DialogResult res;
            uploadCSV.Filter = "CSV|*.csv";
            res = uploadCSV.ShowDialog();
            //String csvData = "";
            System.IO.StreamReader oRead;
            if (System.IO.File.Exists(uploadCSV.FileName))
            {
                oRead = System.IO.File.OpenText(uploadCSV.FileName);
                string strHeads = "";
                string strData = "";
                string[] docDataAr;
                bool readOnce = false;

                string TagID = "";
                List<String> lstAssetTags = new List<string>();
                List<float> lstQty = new List<float>();
                string documentNo = "";


                //1. Skip First line as Heading
                //2. Save Data into SQL CE table.
                if (!oRead.EndOfStream)
                {
                    strHeads = oRead.ReadLine();
                }
                //documentno,tagid,itemtagno,qty
                while (!oRead.EndOfStream)
                {
                    strData = oRead.ReadLine();
                    docDataAr = strData.Split(Convert.ToString(",").ToCharArray());
                    if (!readOnce)
                    {
                        TagID = docDataAr[1].ToString(); //TagID
                        documentNo = docDataAr[0].ToString(); //Document No. 
                        readOnce = true;
                    }
                    lstAssetTags.Add(docDataAr[2].ToString());
                    lstQty.Add(1);
                }
                Documents.AddDocument(TagID, lstAssetTags, lstQty, documentNo, DateTime.Now, "", 1, DateTime.Now);
                refreshGrid();
            }
            else
            {
                MessageBox.Show("Invalid File.");
            }
        }
        catch (ApplicationException ap)
        {
            Program.ShowError(ap.Message.ToString());
            Logger.LogError(ap.Message); 
        }
        catch (System.Web.Services.Protocols.SoapException ex)
        {
            if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                Program.ShowError("Request from innvalid IP address.");
            else
                Program.ShowError("Network Protocol Failure.");
            Logger.LogError(ex.Message); 
        }
        catch (System.Data.SqlServerCe.SqlCeException sqlex)
        {
            MessageBox.Show("Data File is not able to access.");
            Logger.LogError(sqlex.Message); 
        }
        catch (System.Net.WebException wex)
        {
            MessageBox.Show("Web exception occured.");
            Logger.LogError(wex.Message); 
        }
        catch (Exception ep)
        {
            Logger.LogError(ep.Message); 
            MessageBox.Show(ep.Message.ToString());
        }
        }

        private void frmDispatchDoc_Load(object sender, EventArgs e)
        {
            refreshGrid();
        }

        private void refreshGrid()
        {
            try
            {
                ListViewItem lstItem;
                ListViewItem.ListViewSubItem ls;
                DataTable dtDocs;
                dtDocs = Documents.getRows();
                lstDocs.FullRowSelect = true;
                Int32 iCount = 0;
                foreach (DataRow dr in dtDocs.Rows)
                {
                    lstItem = new ListViewItem();
                    lstItem.Text = Convert.ToString(++iCount);
                    //Convert.ToString(dr["TagID"]) + "," + Convert.ToString(dr["Name"])

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(dr["documentno"]);
                    lstItem.SubItems.Add(ls);

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(dr["TagID"]);
                    lstItem.SubItems.Add(ls);

                    lstDocs.Items.Add(lstItem);
                }
                lstDocs.Refresh();
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
        }

        //Check Any
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
            //Open the scan form which scan the document Tag and AssetTagID, and verify the document no
            if (lstDocs.Items.Count > 0)
            {
                frmVerifyDocs frmVerify = new frmVerifyDocs();
                //TagInfo T = new TagInfo(lstDocs.Items[lstDocs.SelectedIndices[0]].SubItems[2].Text);
                frmVerify.SelectedDocumentID = 0;
                
                frmVerify.Show();
                // disable form until this new form closed
                this.Enabled = false;
                frmVerify.Closed += new EventHandler(TagRdFm_Closed);
            }
            else
            {
                Program.ShowError("Data not found in list.");
            }
        }
        catch (ApplicationException ap)
        {
            Program.ShowError(ap.Message.ToString());
            Logger.LogError(ap.Message); 
        }
        catch (System.Web.Services.Protocols.SoapException ex)
        {
            if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                Program.ShowError("Request from innvalid IP address.");
            else
                Program.ShowError("Network Protocol Failure.");
            Logger.LogError(ex.Message); 
        }
        catch (System.Data.SqlServerCe.SqlCeException sqlex)
        {
            Logger.LogError(sqlex.Message); 
            MessageBox.Show("Data File is not able to access.");
        }
        catch (System.Net.WebException wex)
        {
            MessageBox.Show("Web exception occured.");
            Logger.LogError(wex.Message); 
        }
        catch (Exception ep)
        {
            Logger.LogError(ep.Message); 
            MessageBox.Show(ep.Message.ToString());
        }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
            if (lstDocs.SelectedIndices != null && lstDocs.SelectedIndices.Count > 0)
            {
                //Change the opening form
                frmDetails frmVerify = new frmDetails();
                Documents T = new Documents(lstDocs.Items[lstDocs.SelectedIndices[0]].SubItems[2].Text);
                if (T!= null && T.ServerKey!=0 )
                {
                    frmVerify.searchedData = T.LstAsset;
                    frmVerify.Text = "Document-"+ T.DocumentNo.ToString();  
                    frmVerify.Show();
                    // disable form until this new form closed
                    this.Enabled = false;
                    frmVerify.Closed += new EventHandler(TagRdFm_Closed);
                }
                else
                {
                    Program.ShowError("Data not retrived sucessfully.");
                }
            }
            else
            {
                Program.ShowError("Please select any document first.");
            }
        }
        catch (ApplicationException ap)
        {
            Program.ShowError(ap.Message.ToString());
            Logger.LogError(ap.Message); 
        }
        catch (System.Web.Services.Protocols.SoapException ex)
        {
            if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                Program.ShowError("Request from innvalid IP address.");
            else
                Program.ShowError("Network Protocol Failure.");
            Logger.LogError(ex.Message); 
        }
        catch (System.Data.SqlServerCe.SqlCeException sqlex)
        {
            MessageBox.Show("Data File is not able to access.");
            Logger.LogError(sqlex.Message); 
        }
        catch (System.Net.WebException wex)
        {
            MessageBox.Show("Web exception occured.");
            Logger.LogError(wex.Message); 
        }
        catch (Exception ep)
        {
            Logger.LogError(ep.Message); 
            MessageBox.Show(ep.Message.ToString());
        }
        }
    }
}