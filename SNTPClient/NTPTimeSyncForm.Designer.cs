namespace TimeSync
{
    partial class NTPTimeSyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NTPTimeSyncForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.InfoLbl = new System.Windows.Forms.Label();
            this.StatusLbl = new System.Windows.Forms.Label();
            this.DetailMsgLbl = new System.Windows.Forms.Label();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // InfoLbl
            // 
            resources.ApplyResources(this.InfoLbl, "InfoLbl");
            this.InfoLbl.Name = "InfoLbl";
            // 
            // StatusLbl
            // 
            resources.ApplyResources(this.StatusLbl, "StatusLbl");
            this.StatusLbl.Name = "StatusLbl";
            // 
            // DetailMsgLbl
            // 
            resources.ApplyResources(this.DetailMsgLbl, "DetailMsgLbl");
            this.DetailMsgLbl.Name = "DetailMsgLbl";
            // 
            // CancelBttn
            // 
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            this.CancelBttn.Click += new System.EventHandler(this.OnCancelBttnClicked);
            // 
            // NTPTimeSyncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.DetailMsgLbl);
            this.Controls.Add(this.StatusLbl);
            this.Controls.Add(this.InfoLbl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NTPTimeSyncForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label InfoLbl;
        private System.Windows.Forms.Label StatusLbl;
        private System.Windows.Forms.Label DetailMsgLbl;
        private System.Windows.Forms.Button CancelBttn;
    }
}