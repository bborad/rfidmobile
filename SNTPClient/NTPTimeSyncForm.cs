using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TimeSync
{
    public partial class NTPTimeSyncForm : Form
    {
        private NTPClient ntpCli = null;

        public NTPTimeSyncForm(String ntpSrvr)
        {
            InitializeComponent();

            ntpCli = new NTPClient(ntpSrvr);
        }

        #region Cancel Button States
        enum SyncOpState
        {
            Idle,
            Starting,
            Running
        }

        private void CancelBttnInitState()
        {
            SyncOpState State = SyncOpState.Idle;

            CancelBttn.Tag = State;
        }

        private SyncOpState CancelBttnState()
        {
            SyncOpState State = SyncOpState.Idle;

            if (CancelBttn.Tag is SyncOpState)
                State = (SyncOpState)CancelBttn.Tag;
            else
                MessageBox.Show("Cancel Button Tag is not of type SyncOpState");
            return State;
        }

        private void CancelBttnSetState(SyncOpState state)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NTPTimeSyncForm));
            CancelBttn.Tag = state;

            switch (state)
            {
                case SyncOpState.Idle:
                    CancelBttn.Enabled = true;
                    CancelBttn.Text = "Try Again";
                    break;
                case SyncOpState.Starting:
                    CancelBttn.Enabled = false;
                    break;
                case SyncOpState.Running:
                    CancelBttn.Enabled = true;
                    CancelBttn.Text = resources.GetString("CancelBttn.Text");
                    break;
            }
        }

        #endregion

        private void DspyLongErrMsg(String msg)
        {
            if (String.IsNullOrEmpty(msg))
            {
                DetailMsgLbl.Visible = false;
            }
            else
            {
                DetailMsgLbl.Text = msg;
                DetailMsgLbl.ForeColor = Color.Red;
                DetailMsgLbl.Visible = true;
            }
        }

        private void StartSync()
        {
            StatusLbl.Text = "Getting time information from time server...";

            CancelBttnSetState(SyncOpState.Starting);
            if (!ntpCli.SyncStart(NtpCliStateChanged, NtpCliSyncEnded))
            {
                StatusLbl.Text = "Unable to start time information from time server";
                CancelBttnSetState(SyncOpState.Idle);
            }
        }

        private void OnCancelBttnClicked(object sender, EventArgs e)
        {
            switch (CancelBttnState())
            {
                case SyncOpState.Idle:
                    DspyLongErrMsg(String.Empty);
                    StartSync();
                    break;
                case SyncOpState.Running:
                    if (ntpCli.SyncStop() == false)
                    {
                        DspyLongErrMsg("Cannot stop at this moment, please try again");
                    }
                    else
                    {
                        StatusLbl.Text = "Stopped";
                        CancelBttnSetState(SyncOpState.Idle);
                    }
                    break;
            }
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            InfoLbl.Text = "Time Server: " + ntpCli.TimeServer;
            StartSync();
        }


        private void NtpCliStateChanged(NTPClient.SyncState syncState)
        {
            switch (syncState)
            {
                case NTPClient.SyncState.DNSResolve:
                    StatusLbl.Text = "Resolving remote server address...";
                    break;
                case NTPClient.SyncState.Idle:
                    break;
                case NTPClient.SyncState.Connect:
                    StatusLbl.Text = "Connecting...";
                    CancelBttnSetState(SyncOpState.Running);
                    break;
                case NTPClient.SyncState.Request:
                    StatusLbl.Text = "Sending request...";
                    CancelBttnSetState(SyncOpState.Running);
                    break;
                case NTPClient.SyncState.WaitResponse:
                    StatusLbl.Text = "Waiting for response...";
                    CancelBttnSetState(SyncOpState.Running);
                    break;
            }
        }

        private void NtpCliSyncEnded(bool succ, String errMsg)
        {
            CancelBttnSetState(SyncOpState.Idle);
            if (succ)
            {
                StatusLbl.Text = "Synchronization succeeded";
            }
            else
            {
                StatusLbl.Text = "Failed";
                DspyLongErrMsg(errMsg);
            }
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (CancelBttnState() != SyncOpState.Idle)
            {
                // Do not close Form while Time-Sync in progress
                e.Cancel = true;
            }
        }
    }
}