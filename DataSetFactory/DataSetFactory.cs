using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DataSetFactory
{
    
    abstract public class DataSetFactory
    {
        public abstract DataSet GetDataSet();
    }

    public class InvtryDataSetFactory : DataSetFactory
    {
        public override DataSet GetDataSet()
        {
            return new InvtryDataSet();
        }
    }

}
