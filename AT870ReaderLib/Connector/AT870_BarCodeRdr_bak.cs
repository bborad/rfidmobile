﻿using System;
using System.Collections.Generic;
using System.Text;
using ReaderTypes;
using ATScan_NET;
using ClsLibBKLogs;
using OnRamp;
using System.Threading;

namespace AT870ReaderLib
{
    public class BarCode_Reader : HHDeviceInterface.BarCode.BarCodeReader
    {
        public static CScan objCScan;

        public static bool IsScanOpen;

        public static object lockScannner = new object();

        //static ClsCallBackScan objCallbackScan;

        public BarCode_Reader()
        {
            InitiateScanner();
        }

        void InitiateScanner()
        {
            if (objCScan == null)
            {
                try
                {
                    objCScan = new CScan(null);
                   
                }
                catch (Exception ex)
                {
                    Logger.LogError("InitiateScanner : " + ex.Message);
                }
            }
        }

        public override bool ChangeCurrentSetting(string tag, string subTag, string dataFld, out string retStr)
        {
            retStr = "";
            return true;
        }
        public override bool ChangeSavedSetting(string tag, string subTag, string dataFld, out string retStr)
        {
            retStr = "";
            return true;
        }
        public override void Dispose()
        {
            try
            {
                objCScan.ScanClose();
                objCScan = null;
            }
            catch (Exception ex)
            {
                Logger.LogError("Dispose : " + ex.Message);
            }
        }
        public override bool GetCurrentSetting(string tag, string subTag, out string retStr)
        {
            retStr = "";
            return true;
        }
        public override bool ScanStart()
        {
            bool result = false;
            try
            {
                InitiateScanner();
                objCScan.ActivatedForm = null;
                objCScan.ActivatedForm = new ClsCallBackScan(this);

                lock (lockScannner)
                {
                    if (!IsScanOpen)
                    {
                        if (UserPref.GetInstance().ScannerType == 1)
                        {
                            Logger.LogError("Scanner Type : 1");
                            if (!objCScan.ScanOpen(CScan.ScannerType.Type1))
                            {
                                IsScanOpen = false;
                                Logger.LogError("Scanner Open failed.");
                                return result;
                            }
                            else
                            {
                                IsScanOpen = true;
                            }
                           
                        }
                        else
                        {
                            Logger.LogError("Scanner Type : 2");
                            if (!objCScan.ScanOpen(CScan.ScannerType.Type2))
                            {
                                IsScanOpen = false;
                                Logger.LogError("Scanner Open failed.");
                                return result;
                            }
                            else
                            {
                                IsScanOpen = true;
                            }
                           
                          
                        }
                       
                        
                    }
                }

                objCScan.ScanStart();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("ScanStart : " + ex.Message);
            }
            return result;
        }

        private void OpenScanner()
        {
            if (UserPref.GetInstance().ScannerType == 1)
            {
                Logger.LogError("Scanner Type : 1");
                if (!objCScan.ScanOpen(CScan.ScannerType.Type1))
                {
                    IsScanOpen = false;
                    Logger.LogError("Scanner Open failed.");
                    //return false;
                }
                else
                {
                    IsScanOpen = true;
                }

            }
            else
            {
                Logger.LogError("Scanner Type : 2");
                if (!objCScan.ScanOpen(CScan.ScannerType.Type2))
                {
                    IsScanOpen = false;
                    Logger.LogError("Scanner Open failed.");
                    //return false;
                }
                else
                {
                    IsScanOpen = true;
                }


            }

        }
        //Initialize and open barcode scaner
        public override bool InitializeReader()
        {
            InitiateScanner();
            Thread tOpen = new Thread(new ThreadStart(OpenScanner));

            //objCScan.ActivatedForm = null;
             objCScan.ActivatedForm = new ClsCallBackScan(this);
            if (!IsScanOpen)
            {
                #region moved to new thread 
                //if (UserPref.GetInstance().ScannerType == 1)
                //{
                //    Logger.LogError("Scanner Type : 1");
                //    if (!objCScan.ScanOpen(CScan.ScannerType.Type1))
                //    {
                //        IsScanOpen = false;
                //        Logger.LogError("Scanner Open failed.");
                //        return false;
                //    }
                //    else
                //    {
                //        IsScanOpen = true;
                //    }

                //}
                //else
                //{
                //    Logger.LogError("Scanner Type : 2");
                //    if (!objCScan.ScanOpen(CScan.ScannerType.Type2))
                //    {
                //        IsScanOpen = false;
                //        Logger.LogError("Scanner Open failed.");
                //        return false;
                //    }
                //    else
                //    {
                //        IsScanOpen = true;
                //    }


                //}
                #endregion

                 tOpen.Start();
                //OpenScanner();
                if (!tOpen.Join(10000))
                {
                    tOpen.Abort();
                    
                    IsScanOpen = false;
                }
            }
            return IsScanOpen; 
        }

        private void ResetActiveForm()
        {
            //objCScan.ActivatedForm = null;
            //objCScan.ActivatedForm = new ClsCallBackScan(this);
        }
        public override bool StartReader()
        {
             
            if (objCScan == null)
                if (!InitializeReader())
                    return false;
            IsScanOpen = true;
            objCScan.ScanStart();

            return true;
        }

        public override bool StopReader()
        {
            if (objCScan != null)
            {
                Thread tclose = new Thread(new ThreadStart(StpScanner));
                tclose.IsBackground = true;
                tclose.Start();
                if(!tclose.Join(6000))
                    tclose.Abort();
                IsScanOpen = false;
                
            }

            return true;
        }
        private void StpScanner()
        {
            objCScan.ScanStop();
        }
        private void ClsScanner()
        {
            objCScan.ScanClose();
        }
        public override bool CloseReader()
        {
            if (objCScan != null)
            {
                Thread tclose = new Thread(new ThreadStart(ClsScanner));
                tclose.Start();
                tclose.Join(6000);
                tclose.Abort();
                IsScanOpen = false;
                objCScan = null;
            }

            return true;
        }

        public override bool ScanTryStop()
        {
            try
            {
                InitiateScanner(); 

                lock (lockScannner)
                {
                    IsScanOpen = false;
                    objCScan.ScanStop();                   
                    objCScan.ScanClose();
                    stopNotify();
                }
               // objCScan.ActivatedForm = null;
               
            }
            catch (Exception ex)
            {
                Logger.LogError("ScanTryStop : " + ex.Message);
            }
            return true;
        }

        public bool CodeRcvdNotifyCb(bool Succ, String codeStr, String errStr)
        {
            if (scanNotify != null)
                return scanNotify(Succ, codeStr, errStr);
            else
                return false;
        }
        public override void RegisterCodeRcvdNotificationEvent(CodeRcvdNotify handler)
        {
             this.scanNotify = handler;
            
             
        }
        public override void UnregisterCodeRcvdNotificationEvent(CodeRcvdNotify handler)
        {
            this.scanNotify -= handler;
        }

        public override void RegisterStopNotificationEvent(StopNotify handler)
        {
            this.stopNotify = handler;
        }
        public override void UnregisterStopNotificationEvent(StopNotify handler)
        {
            this.stopNotify -= handler;
        }
    }

    public class ClsCallBackScan : ATScan_NET.IScan
    {
        public BarCode_Reader objBarCodeRdr;
        public string a = "";
        public ClsCallBackScan(BarCode_Reader BarCodeRdr)
        {
            objBarCodeRdr = BarCodeRdr;
        }

        public void OnScaned(string BarcodeValue, string TypeName, string TypeID)
        {
            //listBox1.Items.Add(BarcodeValue);
            //listBox1.SelectedIndex = listBox1.Items.Count - 1;
            //Form1.scanner.PlaySuccess();
            a = BarcodeValue;
            try
            {
                lock (BarCode_Reader.lockScannner)
                {
                    if (objBarCodeRdr != null && objBarCodeRdr.scanNotify != null)
                    {
                        objBarCodeRdr.scanNotify(true, BarcodeValue, "");

                        if (BarCode_Reader.IsScanOpen)
                        {
                            BarCode_Reader.objCScan.ScanStop();
                            BarCode_Reader.objCScan.ScanStart();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("OnScaned : " + ex.Message);
            }
        }
    }
}
