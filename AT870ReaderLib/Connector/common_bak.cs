﻿using System;
using System.Collections.Generic;

using System.Text;
using ReaderTypes;
//using SP = ClslibPosSp.PosSp;
//using ClslibPosSp;

namespace AT870ReaderLib
{
    public class PosSp
    {
        public const int BUZZER_ALWAYS_ON = -1;

        private const int RadioPwrPin = 3;
        private const int ScnrLEDPin = 1;

        private static bool disposed = false;

        static PosSp()
        {
            // Initialize Native libraries

            // Turn on Radio-related GPIO
           
        }


        public static void Dispose()
        {
            if (!disposed)
            {
                // Unload native library
            
                disposed = true;
            }
        }

        /// 

        public static int f_PosSp_Initialize()
        {
            return 0;
        }


        public static int f_PosSp_Uninitialize()
        {
            return 0;
        }
        /// void f_PosSp_GetDeviceName(WCHAR* DeviceName);

        public static void f_PosSp_GetDeviceName(string DeviceName)
        {
            
        }//ref?
        /// LED
        /// GSLB101API_API BOOL LedSetOn(COLORREF Color);
        // [DllImport(B101DLL_Path, EntryPoint = "LedSetOn", CharSet = CharSet.Auto)]
        public static bool f_PosSp_LedSetOn(uint Color)
        {
            return true;
        }
        /// GSLB101API_API BOOL LedBlink(COLORREF colorRGB,WORD Period, WORD OnTime);

        public static bool f_PosSp_LedBlink(uint colorRGB, short Period, short OnTime)
        {
            return true;
        }
        /// GSLB101API_API void LedSetOff();

        public static bool f_PosSp_LedSetOff()
        {
            return true;
        }
        /// Buzzer Sound
        /// GSLB101API_API void ToneOn(WORD freq,WORD Duration,BUZZER_SOUND SoundLevel);

        public static void f_PosSp_ToneOn(short freq, short Duration, uint SoundLevel)
        {
            return;
        }
        /// GSLB101API_API void ToneOff();

        public static void f_PosSp_ToneOff()
        {
           
        }
        /// GSLB101API_API void MelodyPlay(int ToneID,WORD Duration,BUZZER_SOUND SoundLevel);

        public static void f_PosSp_MelodyPlay(int ToneID, short Duration, uint SoundLevel)
        {
           
        }
        /// GSLB101API_API void MelodyStop();

        public static void f_PosSp_MelodyStop()
        {
            
        }

        /// Vibrator GPIO
        /// void f_PosSp_VibrationOn(short OnPeriod,short OffPeriod, short Duration);
        /// Hw Not connected
        public static void f_PosSp_VibrationOn(short OnPeriod, short OffPeriod, short Duration)
        {
            
        }
        /// void f_PosSp_VibrationStop();
        /// Hw Not connected
        public static void f_PosSp_VibrationStop()
        {
            
        }
        /// WiFi
        /// int f_PosSp_GetWiFiState(); /// WIFISTATE

        public static int f_PosSp_GetWiFiState()
        {
            return 0;
        }
        /// void f_PosSp_ForceScan();

        public static void f_PosSp_ForceScan()
        {
            
        }
        //////////////////////////////////////////////////////////////////////////
        /// Open the filehandle for the device

        public static bool f_PosSp_GpioIni()
        {
            return true;
        }
        /// Close the filehandle 
        public static bool f_PosSp_GpioUnini()
        {
            return true;
        }
        /// Set the IO of Gpio 0--3    
        public static bool f_PosSp_GpioSetIo(int iGpio)
        {
            return true;
        }
        /// Write a state to Gpio 0--3    
        //[DllImport(W_PosSpDLL_Path, EntryPoint = "f_PosSp_GpioWrite", CharSet = CharSet.Auto)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        public static bool f_PosSp_GpioWrite(int iGpio, char iState)
        {
            return true;
        }

        public static bool f_PosSp_GpioRead(int iGpio, ref char piState)
        {
            return true;
        }
        //////////////////////////////////////////////////////////////////////////

        public static bool RadioPwrOn()
        {
            return true;
        }

        public static bool RadioPwrOff()
        {

            // Turn off Radio-related GPIO
            return true;
        }

        public static bool ScnrLaserOn()
        {
            return true;
        }

        public static bool ScnrLaserOff()
        {
            return true;
        }

    }


}

namespace AT870ReaderLib.UtilityTypes
{
    #region MemMgt_funcs
    public class MemMgt
    {
        //#region Memory Management
        const int LMEM_ZEROINIT = 0x40;
        const int LPTR = 0x40; /// LMEM_ZEROINIT|LMEM_FIXED

        public static IntPtr LocalAlloc(int flags, int size)
        {
            IntPtr result = new IntPtr();
            return result;
        }

        public static IntPtr LocalFree(IntPtr pMem)
        {
            IntPtr result=new IntPtr();
            return result;
        }
        //#endregion
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
    #endregion
    #region SettingMgt_funcs
    public class SettingMgt
    {
        //private const String W_SettingMgtDLL_Path = "W_SettingMgt.dll";

        public SettingMgt() { }


        public static HRESULT_RFID f_ApplicationSetup()
        {
            return HRESULT_RFID.S_OK;
        }

        /// HRESULT WINAPI f_SettingMgt_Initialize( HWND  hWnd ); //System.Runtime.InteropServices.

        public static HRESULT_RFID f_SettingMgt_Initialize(IntPtr hWnd)
        {
            return HRESULT_RFID.S_OK;
        }
        /// HRESULT WINAPI f_SettingMgt_Uninitialize(); 

        public static HRESULT_RFID f_SettingMgt_Uninitialize()
        {
            return HRESULT_RFID.S_OK;
        }
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Read(); 

        public static HRESULT_RFID f_SettingMgt_RfidSpSetting_Read()
        {
            return HRESULT_RFID.S_OK;
        }
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Write(); 

        public static HRESULT_RFID f_SettingMgt_RfidSpSetting_Write()
        {
            return HRESULT_RFID.S_OK;
        }

        public static HRESULT_RFID f_SettingMgt_StartLogTrace(String logDirPath)
        {
            return HRESULT_RFID.S_OK;
        }

        public static HRESULT_RFID f_SettingMgt_StopLogTrace()
        {
            return HRESULT_RFID.S_OK;
        }

        public static HRESULT_RFID f_CollectWifiTCPIPStatus(
                               ref bool enabled, ref bool connected, StringBuilder hwaddr, StringBuilder ipaddr, StringBuilder ipmask,
                               StringBuilder gwaddr, StringBuilder dnsaddr1, StringBuilder dnsaddr2,
                               ref bool dhcp, StringBuilder dhcpaddr, SystemTime leaseTm, SystemTime expireTm)
        {
            return HRESULT_RFID.S_OK;
        }

        public static Int32 f_SetWINSHostName(String hostName)
        {
            return 0;
        }

        public static Int32 f_GetWINSHostName(StringBuilder hostName, int bufSz)
        {
            return 0;
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    #endregion
}
