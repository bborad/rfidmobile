using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

    /// using HRESULT_RFID        = System.Int32;
using HRESULT_RFID_STATUS = ReaderTypes.HRESULT_RFID; //System.Int32;
    using RFID_RADIO_HANDLE   = System.UInt32;
using ReaderTypes;namespace ClslibRfidSp // CallApp
{
    /////////////////////////////////////////////////////////////////////////////////////////////
#region MemMgt_funcs
    public class MemMgt
    {
        //#region Memory Management
        const int LMEM_ZEROINIT = 0x40;
        const int LPTR = 0x40; /// LMEM_ZEROINIT|LMEM_FIXED
        [DllImport("coredll")]
        extern public static IntPtr LocalAlloc(int flags, int size);
        [DllImport("coredll")]
        extern public static IntPtr LocalFree(IntPtr pMem);
        //#endregion
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
#endregion
#region SettingMgt_funcs
    public class SettingMgt
    {
        private const String W_SettingMgtDLL_Path = "W_SettingMgt.dll";

        public SettingMgt() { }

        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_ApplicationSetup", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_ApplicationSetup();

        /// HRESULT WINAPI f_SettingMgt_Initialize( HWND  hWnd ); //System.Runtime.InteropServices.
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_SettingMgt_Initialize", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_SettingMgt_Initialize(
            IntPtr hWnd);
        /// HRESULT WINAPI f_SettingMgt_Uninitialize(); 
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_SettingMgt_Uninitialize", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_SettingMgt_Uninitialize();
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Read(); 
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_SettingMgt_RfidSpSetting_Read", CharSet = CharSet.Unicode)]
        public static extern HRESULT_RFID f_SettingMgt_RfidSpSetting_Read();
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Write(); 
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_SettingMgt_RfidSpSetting_Write", CharSet = CharSet.Unicode)]
        public static extern HRESULT_RFID f_SettingMgt_RfidSpSetting_Write();
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_SettingMgt_StartLogTrace", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_SettingMgt_StartLogTrace(String logDirPath);
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_SettingMgt_StopLogTrace", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_SettingMgt_StopLogTrace();
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_CollectWifiTCPIPStatus", CharSet=CharSet.Auto)]
         public static extern HRESULT_RFID f_CollectWifiTCPIPStatus(
	                            ref bool enabled, ref bool connected, StringBuilder hwaddr, StringBuilder ipaddr, StringBuilder ipmask,
	                            StringBuilder gwaddr, StringBuilder dnsaddr1, StringBuilder dnsaddr2,
                                ref bool dhcp, StringBuilder dhcpaddr, SystemTime leaseTm, SystemTime expireTm);
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_SetWINSHostName", CharSet = CharSet.Auto)]
        public static extern Int32 f_SetWINSHostName(String hostName);
        [DllImport(W_SettingMgtDLL_Path, EntryPoint = "f_GetWINSHostName", CharSet = CharSet.Auto)]
        public static extern Int32 f_GetWINSHostName(StringBuilder hostName, int bufSz);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
#endregion
#region RfidSp_funcs
    public partial class RfidSp
    {
        public RfidSp() { }
        ///////////////////////////////////////////////////////////////////////////////////////
        public static bool SUCCEEDED1( HRESULT_RFID hr  ) //uint
        {
            if ((HRESULT_RFID.S_OK              == hr) ||
                (HRESULT_RFID.S_RFID_STATUS_OK  == hr))
            {
              return true;
            }else{
              return false;
            }
        }

        #region Register/Packet value conversion routines
        public static float RssiRegToDb(UInt16 rssiReg)
        {
            // Extract WbRSSI(lower-order), NbRSSI(higher-order)
#if USE_WBRSSI
            float rssiDbm = ((rssiReg /*>> 8*/) & 0x00ff) * (0.8F); // TBD: further convert to Dbm (-ive)
#else
            float rssiDbm = ((rssiReg >> 8) & 0x00ff) * (0.8F); // TBD: further convert to Dbm (-ive)
#endif
            return rssiDbm;
        }
        #endregion
        ///////////////////////////////////////////////////////////////////////////////////////

        private const String W_RfidSpDLL_Path = "W_RfidSp.dll";
        /// int W_RFIDSP_API WINAPI f_RfidSpDll_Initialize( HWND hWnd ); //System.Runtime.InteropServices. , EntryPoint = "f_RfidSpDll_Initialize", CharSet = CharSet.Auto
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_Initialize", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidSpDll_Initialize(IntPtr hWnd);
        /// int W_RFIDSP_API WINAPI f_RfidSpDll_Uninitialize( ); 
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_Uninitialize", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidSpDll_Uninitialize();
        /// HRESULT W_RFIDSP_API WINAPI f_RfidSpDll_HeapAlloc( HLOCAL hLoc  )
        /// returns the Allocated Heap.
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_HeapAlloc", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidSpDll_HeapAlloc( ref IntPtr hLoc, uint size );
        /// HRESULT W_RFIDSP_API WINAPI f_RfidSpDll_HeapSize(	 HLOCAL hLoc );
        /// returns the size of this Heap.
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_HeapSize", CharSet = CharSet.Auto)]
        public static extern uint f_RfidSpDll_HeapSize(
            IntPtr hLoc );
        /// HRESULT W_RFIDSP_API WINAPI f_RfidSpDll_HeapFree( HLOCAL hLoc );
        /// returnS_OK & hLoc==NULL if free this Heap successfully; else return E_ & hLoc. 
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_HeapFree", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidSpDll_HeapFree( 
            IntPtr hLoc );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_StartLogTrace", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidSpDll_StartLogTrace(String logDirPath);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_StopLogTrace", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidSpDll_StopLogTrace();
        /// void W_RFIDSP_API WINAPI f_RfidSpDll_Beep ()
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_Beep", CharSet = CharSet.Auto)]
        public static extern void f_RfidSpDll_Beep(int vol);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_BeepAtFreq", CharSet = CharSet.Auto)]
        public static extern void f_RfidSpDll_BeepAtFreq(int freq, int vol); // freq value in Hz
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidSpDll_Ring", CharSet = CharSet.Auto)]
        public static extern void f_RfidSpDll_Ring(int toneID, int vol);
        //////////////////////////////////////////////////////////////////////////
        /// HRESULT W_RFIDSP_API WINAPI f_RfidMw_Initialize(   void); 
        [DllImport(W_RfidSpDLL_Path)]
        public static extern HRESULT_RFID f_RfidMw_Initialize();
        /// HRESULT W_RFIDSP_API WINAPI f_RfidMw_Uninitialize( void);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_Uninitialize", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_Uninitialize();
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_PostCmd", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_PostCmd(
            ref RfidMw_Cmd_T sRfidMw_cmd
            ); //[MarshalAs(UnmanagedType.LPStruct)]
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_SetAllTaglist", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_SetAllTaglist( 
            UInt32      siz, ref IntPtr parylist ); //ary of st_RfidMw_PecRec;
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_AddATag",       CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_AddATag(          
            ref PECRECORD_T st_PecRec );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_FindATag",      CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_FindATag(         
            ref PECRECORD_T st_PecRec);    
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_ClearAllTaglist",   CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_ClearAllTaglist();
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_UpdateAllTaglistToFile", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_UpdateAllTaglistToFile();
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_GetUpdateTaglist",  CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_GetUpdateTaglist( ); //ref UInt32 siz, ref IntPtr aryst_RfidMw_PecRec
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_BeginGetAllTaglist",     CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_GetAllTaglist(  );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_GetAllTaglist", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_GetAllTaglist(ref IntPtr tags, ref UInt32 siz);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_SetMsgMode", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_SetMsgMode(int mode); //enum 0=All, 1=Only_1st_Read
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_PecRssiMin_Set",    CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_PecRssiMin_Set   (ref UInt16 val);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_PecRssiMin_Get",    CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_PecRssiMin_Get   (ref UInt16 val);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_PecAntCtrlMin_Set", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_PecAntCtrlMin_Set(ref UInt16 val);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidMw_TagInv_PecAntCtrlMin_Get", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidMw_TagInv_PecAntCtrlMin_Get(ref UInt16 val); 
        //////////////////////////////////////////////////////////////////////////
        /// HRESULT W_RFIDSP_API WINAPI f_RfidDev_Initialize(  void); 
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_Initialize", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_Initialize();
        /// HRESULT W_RFIDSP_API WINAPI f_RfidDev_Uninitialize(void);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_Uninitialize", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_Uninitialize();
        /// public static extern int f_RfidDev_Uninitialize();
        //////////////////////////////////////////////////////////////////////////
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_Startup", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_Startup(
            ref RFID_Startup_T 			                    st_RfidSpReq_Startup );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_Shutdown", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_Shutdown(
            ref RFID_Shutdown_T 			                st_RfidSpReq_Shutdown );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RetrieveAttachedRadiosList", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RetrieveAttachedRadiosList(
            ref RFID_RetrieveAttachedRadiosList_T	        st_RfidSpReq_RetrieveAttachedRadiosList );
        /// RfidSp. Open the Radio Object /// HRESULT W_RFIDSP_API WINAPI f_RfidSpDll_RadioOpen( [in] );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioOpen", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioOpen(
            ref RFID_RadioOpen_T                            st_RfidSpReq_RadioOpen ); //[MarshalAs(UnmanagedType.LPStruct)]
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioClose", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioClose(
            ref RFID_RadioClose_T	                        st_RfidSpReq_RadioClose );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioSetConfigurationParameter", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioSetConfigurationParameter(
            ref  RFID_RadioGetSetConfigurationParameter_T	st_RfidSpReq_RadioSetConfigurationParameter );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioSetConfigurationParameterS", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioSetConfigurationParameterS(
            ref  RFID_RadioGetSetConfigurationParameter_T st_RfidSpReq_RadioSetConfigurationParameter);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetConfigurationParameterS", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetConfigurationParameterS(
            ref  RFID_RadioGetSetConfigurationParameter_T	st_RfidSpReq_RadioSetConfigurationParameter );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioSetOperationMode", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioSetOperationMode(
            ref  RFID_RadioGetSetOperationMode_T			st_RfidSpReq_RadioSetOperationMode );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetOperationMode", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetOperationMode(
            ref  RFID_RadioGetSetOperationMode_T			st_RfidSpReq_RadioGetOperationMode );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioSetPowerState", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioSetPowerState(
            ref RFID_RadioGetSetPowerState_T 				st_RfidSpReq_RadioSetPowerState );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetPowerState", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetPowerState(
            ref RFID_RadioGetSetPowerState_T 				st_RfidSpReq_RadioGetPowerState );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioSetCurrentLinkProfile", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioSetCurrentLinkProfile(
            ref RFID_RadioGetSetCurrentLinkProfile_T		st_RfidSpReq_RadioSetCurrentLinkProfile );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetCurrentLinkProfile", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetCurrentLinkProfile( 
            ref RFID_RadioGetSetCurrentLinkProfile_T		st_RfidSpReq_RadioGetCurrentLinkProfile );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetCurrentLinkProfileS", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetCurrentLinkProfileS(
            ref RFID_RadioGetSetCurrentLinkProfile_T st_RfidSpReq_RadioGetCurrentLinkProfile);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetLinkProfile", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetLinkProfile(
            ref RFID_RadioGetLinkProfile_T					st_RfidSpReq_RadioGetLinkProfile );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_AntennaPortGetStatus", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_AntennaPortGetStatus(
            ref RFID_AntennaPortGetStatus_T st_RfidSpReq_AntennaPortGetStatus);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_AntennaPortSetState", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_AntennaPortSetState(
            ref RFID_AntennaPortSetState_T st_RfidSpReq_AntennaPortSetState);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_AntennaPortSetConfiguration", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_AntennaPortSetConfiguration(
            ref RFID_AntennaPortGetSetConfiguration_T st_RfidSpReq_AntennaPortSetConfiguration);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_AntennaPortGetConfiguration", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_AntennaPortGetConfiguration(
            ref RFID_AntennaPortGetSetConfiguration_T st_RfidSpReq_AntennaPortGetConfiguration);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetSelectCriteria", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetSelectCriteria(
            ref RFID_18K6CSetSelectCriteria_T st_RfidSpReq_18K6CSetSelectCriteria);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetSelectCriteria01", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetSelectCriteria01(
            ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetSelectCriteria02", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetSelectCriteria02(
            ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetSelectCriteria03", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetSelectCriteria03(
            ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02,
            ref RFID_18K6C_SELECT_CRITERION criteria03 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetSelectCriteria04", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetSelectCriteria04(
            ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02,
            ref RFID_18K6C_SELECT_CRITERION criteria03, ref RFID_18K6C_SELECT_CRITERION criteria04 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetSelectCriteria", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetSelectCriteria(
            ref RFID_18K6CGetSelectCriteria_T st_RfidSpReq_18K6CGetSelectCriteria);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetSelectCriteria01", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetSelectCriteria01(
            ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetSelectCriteria02", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetSelectCriteria02(
            ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetSelectCriteria03", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetSelectCriteria03(
            ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02,
            ref RFID_18K6C_SELECT_CRITERION criteria03 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetSelectCriteria04", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetSelectCriteria04(
            ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02,
            ref RFID_18K6C_SELECT_CRITERION criteria03, ref RFID_18K6C_SELECT_CRITERION criteria04 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint="f_RfidDev_18K6CSetPostMatchCriteria", CharSet=CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetPostMatchCriteria (
            ref RFID_18K6CSetPostMatchCriteria_T st_RfidDev_18K6CSetPostMatchCriteria);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetPostMatchCriteria01", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetPostMatchCriteria01(
            ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetPostMatchCriteria02", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetPostMatchCriteria02(
            ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetPostMatchCriteria03", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetPostMatchCriteria03(
            ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02,
            ref RFID_18K6C_SINGULATION_CRITERION criteria03 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetPostMatchCriteria04", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetPostMatchCriteria04(
            ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02,
            ref RFID_18K6C_SINGULATION_CRITERION criteria03, ref RFID_18K6C_SINGULATION_CRITERION criteria04 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetPostMatchCriteria01", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetPostMatchCriteria01(
            ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetPostMatchCriteria02", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetPostMatchCriteria02(
            ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetPostMatchCriteria03", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetPostMatchCriteria03(
            ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02,
            ref RFID_18K6C_SINGULATION_CRITERION criteria03 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CGetPostMatchCriteria04", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetPostMatchCriteria04(
            ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria,
            ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02,
            ref RFID_18K6C_SINGULATION_CRITERION criteria03, ref RFID_18K6C_SINGULATION_CRITERION criteria04 );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CSetQueryParameters", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CSetQueryParameters(
            ref RFID_18K6CSetQueryParameters_T st_RfidSpReq_18K6CSetQueryParameters);
        [DllImport(W_RfidSpDLL_Path, 
            EntryPoint = "f_RfidDev_18K6CGetQueryParameters",
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CGetQueryParameters(
            ref RFID_18K6CGetQueryParameters_T st_RfidSpReq_18K6CGetQueryParameters);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CTagInventory", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CTagInventory(
            ref RFID_18K6CTagInventory_T st_RfidSpReq_18K6CTagInventory);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CTagRead", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CTagRead(
            ref RFID_18K6CTagRead_T st_RfidSpReq_18K6CTagRead);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CTagWrite", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CTagWrite(
            ref RFID_18K6CTagWrite_T st_RfidSpReq_18K6CTagWrite);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CTagKill", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CTagKill(
            ref RFID_18K6CTagKill_T st_RfidSpReq_18K6CTagKill);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_18K6CTagLock", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_18K6CTagLock(
            ref  RFID_18K6CTagLock_T st_RfidSpReq_18K6CTagLock);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioSetResponseDataMode", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioSetResponseDataMode(
            ref RFID_RadioGetSetResponseDataMode_T st_RfidSpReq_RadioSetResponseDataMode);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetResponseDataMode ", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetResponseDataMode(
            ref RFID_RadioGetSetResponseDataMode_T st_RfidSpReq_RadioGetResponseDataMode);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacUpdateFirmware", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacUpdateFirmware(
            ref RFID_MacUpdateFirmware_T st_RfidSpReq_MacUpdateFirmware);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacGetVersion", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacGetVersion(
            ref RFID_MacGetVersion_T st_RfidSpReq_MacGetVersion);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacReadOemData", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacReadOemData(
            ref RFID_MacReadWriteOemData_T st_RfidSpReq_MacReadOemData);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacReadOemDataS", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacReadOemDataS(
            RFID_RADIO_HANDLE handle, UInt32 addr, UInt32 count, [MarshalAs(UnmanagedType.LPArray, SizeParamIndex=2)] UInt32[] data);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacWriteOemData", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacWriteOemData(
            ref RFID_MacReadWriteOemData_T st_RfidSpReq_MacWriteOemData);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacReset", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacReset(
            ref RFID_MacReset_T st_RfidSpReq_MacReset);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacClearErrorS", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacClearErrorS(
            ref RFID_MacClearError_T st_RfidSpReq_MacClearError);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacBypassWriteRegister", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacBypassWriteRegister(
            ref RFID_MacBypassReadWriteRegister_T st_RfidSpReq_MacBypassWriteRegister);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacBypassReadRegisterS", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacBypassReadRegisterS(
            ref RFID_MacBypassReadWriteRegister_T st_RfidSpReq_MacBypassReadRegister);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacSetRegion", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacSetRegion(
            ref RFID_MacGetSetRegion_T st_RfidSpReq_MacSetRegion);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_MacGetRegion", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_MacGetRegion(
            ref RFID_MacGetSetRegion_T st_RfidSpReq_MacGetRegion);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioSetGpioPinsConfiguration", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioSetGpioPinsConfiguration(
            ref RFID_RadioSetGpioPinsConfiguration_T st_RfidSpReq_RadioSetGpioPinsConfiguration);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioGetGpioPinsConfiguration", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioGetGpioPinsConfiguration(
            ref RFID_RadioGetGpioPinsConfiguration_T st_RfidSpReq_RadioGetGpioPinsConfiguration);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioReadGpioPins", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioReadGpioPins(
            ref RFID_RadioReadWriteGpioPins_T st_RfidSpReq_RadioReadGpioPins);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioWriteGpioPins", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioWriteGpioPins(
            ref RFID_RadioReadWriteGpioPins_T st_RfidSpReq_RadioWriteGpioPins);
        //////////////////////////////////////////////////////////////////////////
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioCancelOperation", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID   f_RfidDev_RadioCancelOperation(
            ref RFID_RadioCancelOperation_T st_RfidSpReq_RadioCancelOperation );
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioAbortOperation", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID   f_RfidDev_RadioAbortOperation(
            ref RFID_RadioAbortOperation_T  st_RfidSpReq_RadioAbortOperation );
        ////////////////////////////////////////////////////////////////////////// 
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioTurnCarrierWaveOn", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioTurnCarrierWaveOn(RFID_RADIO_HANDLE handle);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_RadioTurnCarrierWaveOff", CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_RadioTurnCarrierWaveOff(RFID_RADIO_HANDLE handle);
        //////////////////////////////////////////////////////////////////////////
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetTemperature",
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetTemperature(
            RFID_RADIO_HANDLE handle);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetTemperatureS",
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetTemperatureS(
            RFID_RADIO_HANDLE handle, ref ushort amb, ref ushort xcvr, ref ushort pamp);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetThrshTemperature",
                    CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetThrshTemperature(
            RFID_RADIO_HANDLE handle);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetTagInvtryRdRate", 
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetTagInvtryRdRate(
            ref RFID_18K6CTagInventory_T st_RfidSpReq_18K6CTagInventory);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetTagInvtryRssi", 
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetTagInvtryRssi(
            ref RFID_18K6CTagInventory_T st_RfidSpReq_18K6CTagInventory, bool filterRes);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetFreqBandNum",
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetFreqBandNum(
            RFID_RADIO_HANDLE handle);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetFreqBandNumS",
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetFreqBandNumS(
            RFID_RADIO_HANDLE handle, ref uint bandNum);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomSetFreqBandNum",
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomSetFreqBandNum(
            RFID_RADIO_HANDLE handle, UInt32 num);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomGetRadioProfile", 
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomGetRadioProfile(
            RFID_RADIO_HANDLE handle);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomSetRadioChn",
                    CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomSetRadioChn(
            RFID_RADIO_HANDLE handle, CustomFreqGrp freqGrp, int chn, bool enLBT);
        [DllImport(W_RfidSpDLL_Path, EntryPoint = "f_RfidDev_CustomSetRadioProfile", 
            CharSet = CharSet.Auto)]
        public static extern HRESULT_RFID f_RfidDev_CustomSetRadioProfile(
            RFID_RADIO_HANDLE handle, int country, bool enLBT);
    }
#endregion
}
