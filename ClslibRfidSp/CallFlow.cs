using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;

    /// using HRESULT_RFID          = System.Int32;
    using HRESULT_RFID_STATUS   = System.Int32; //use System.HREsult helper class
    using RFID_RADIO_HANDLE     = System.UInt32;
using ReaderTypes;
using SP = ReaderTypes.RfidSp;

namespace ClslibRfidSp
{
    //////////////////////////////////////////////////////////////////////////////////
    public partial class CallFlow
    {
        public static HRESULT_RFID hr; // ?static; To be called 
        CallFlow(){}

        public static HRESULT_RFID f_CFlow_Initialize( IntPtr hWnd) //// +01
        {
            hr = (HRESULT_RFID) SettingMgt.f_SettingMgt_Initialize(hWnd);
            hr = (HRESULT_RFID)RfidSp.f_RfidSpDll_Initialize(hWnd);
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_Initialize();
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_Initialize();
            return hr;
        }
        public static HRESULT_RFID f_CFlow_Uninitialize( ) //// -01
        {
                                  hr = (HRESULT_RFID) RfidSp.f_RfidDev_Uninitialize();
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_Uninitialize();
            hr = (HRESULT_RFID) RfidSp.f_RfidSpDll_Uninitialize();
            hr = (HRESULT_RFID) SettingMgt.f_SettingMgt_Uninitialize();  
            return hr;
        }
        /////////////////////////////////////////////////////////////////////////////////
        /// Log File API
        public static HRESULT_RFID f_CFlow_StartLogTrace(String logDirPath)
        {
            HRESULT_RFID hr1 = (HRESULT_RFID)RfidSp.f_RfidSpDll_StartLogTrace(logDirPath);
            HRESULT_RFID  hr2 = (HRESULT_RFID)SettingMgt.f_SettingMgt_StartLogTrace(logDirPath);
            hr = SP.SUCCEEDED(hr1) ? hr2 : hr1;
            return hr;
        }

        public static HRESULT_RFID f_CFlow_StopLogTrace()
        {
            HRESULT_RFID hr1 = (HRESULT_RFID)RfidSp.f_RfidSpDll_StopLogTrace();
            HRESULT_RFID hr2 = (HRESULT_RFID)SettingMgt.f_SettingMgt_StopLogTrace();
            hr = SP.SUCCEEDED(hr1) ? hr2 : hr1;
            return hr;
        }

        public static void f_CFlow_Beep(SoundVol vol)
        {
            RfidSp.f_RfidSpDll_Beep((int)vol);
        }

        public static void f_CFlow_BeepAtFreq(int freq, SoundVol vol)
        {
            RfidSp.f_RfidSpDll_BeepAtFreq(freq, (int)vol);
        }

        public static void f_CFlow_Ring(RingTone toneID, SoundVol vol)
        {
            RfidSp.f_RfidSpDll_Ring((int)toneID, (int)vol);
        }

        /////////////////////////////////////////////////////////////////////////////////
        ///RfidSp Middleware API
        public static HRESULT_RFID f_CFlow_SettingMgt_Read() //// +02
        {
            // hr = (HRESULT_RFID) SettingMgt.f_SettingMgt_RfidSpSetting_Read();
            return hr;
        }
        public static HRESULT_RFID f_CFlow_SettingMgt_Write() //// -02
        {
            // hr = (HRESULT_RFID) SettingMgt.f_SettingMgt_RfidSpSetting_Write();
            return hr;
        }
        public static HRESULT_RFID    f_CFlow_RfidMw_TagInv_SetAllTaglist(
            UInt32      siz,
            ref PECRECORD_T[] aryst_PecRec) //
        {

            Int32 sizRec = Marshal.SizeOf(aryst_PecRec[0]); //sizeof
            IntPtr rRoot = Marshal.AllocHGlobal((int)(sizRec * siz)); //extra mem, but more safe that using unsafe code. 
            IntPtr pRoot;
            for (int ind = 0; ind < siz; ind++)  
            {
                 pRoot = (IntPtr)(rRoot.ToInt32() + sizRec*ind);
                 Marshal.StructureToPtr(aryst_PecRec[ind], pRoot , false); //fDeleteOld false may mem-leak
            }
            hr = (HRESULT_RFID)RfidSp.f_RfidMw_TagInv_SetAllTaglist( siz, ref rRoot);
            return hr;
        }
        public static HRESULT_RFID     f_CFlow_RfidMw_TagInv_AddATag(ref PECRECORD_T st_PecRec)
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_AddATag(ref st_PecRec);
            return hr;
        }
        public static HRESULT_RFID     f_CFlow_RfidMw_TagInv_FindATag(ref PECRECORD_T st_PecRec)    
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_FindATag(ref st_PecRec);
            return hr;
        }
        public static HRESULT_RFID     f_CFlow_RfidMw_TagInv_ClearAllTaglist()
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_ClearAllTaglist();
            return hr;
        }
        public static HRESULT_RFID     f_CFlow_RfidMw_TagInv_UpdateAllTaglistToFile() //string filename
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_UpdateAllTaglistToFile();
            return hr;
        }
    /// ref UInt32 siz, ref PECRECORD_T[] aryst_RfidMw_PecRec
        public static HRESULT_RFID     f_CFlow_RfidMw_TagInv_GetUpdateTaglist()
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_GetUpdateTaglist();
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidMw_TagInv_GetAllTaglist()
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_GetAllTaglist();
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidMw_TagInv_GetAllTaglist(
            out UInt16[] PCArr, out UINT96_T[] EPCArr, out float[] RSSIArr,
            out FileTime[] FTArr)
        {
            IntPtr Tags = IntPtr.Zero;
            UInt32 NumTags = 0;
            PCArr = null;
            EPCArr = null;
            RSSIArr = null;
            FTArr = null;
            hr = (HRESULT_RFID)RfidSp.f_RfidMw_TagInv_GetAllTaglist(ref Tags, ref NumTags);
            if (SP.SUCCEEDED(hr))
             {
                 PCArr = new ushort[NumTags];
                 EPCArr = new UINT96_T[NumTags];
                 RSSIArr = new float[NumTags];
                 FTArr = new FileTime[NumTags];
                 f_handlemsg_RfidMw_TagInv_GetTaglist(NumTags, ref Tags, PCArr, EPCArr, RSSIArr, FTArr);
             }
             return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidMw_TagInv_SetMsgMode(int mode)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidMw_TagInv_SetMsgMode(mode);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidMw_TagInv_PecRssiMin_Set(ref UInt16 val)
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_PecRssiMin_Set    (ref val);
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_PecRssiMin_Get    (ref val);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidMw_TagInv_PecRssiMin_Get(ref UInt16 val)
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_PecRssiMin_Get    (ref val);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidMw_TagInv_PecAntCtrlMin_Set(ref UInt16 val)
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_PecAntCtrlMin_Set (ref val);
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_PecAntCtrlMin_Get (ref val);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidMw_TagInv_PecAntCtrlMin_Get(ref UInt16 val)
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidMw_TagInv_PecAntCtrlMin_Get (ref val);;
            return hr;
        }
    /// Marshal Data handling ////////////////////////////////////////////////////////////////////////////////
    /// size of aryst_RfidMw_PecRec is changing 0--n
        public static HRESULT_RFID f_handlemsg_RfidMw_TagInv_GetTaglist(
            UInt32 siz, ref IntPtr rRoot,
            ref PECRECORD_T[] aryst_RfidMw_PecRec)
        {
            //IntPtr rRoot  = new IntPtr();
            IntPtr pRoot;
          if (siz > 0){
            //aryst_RfidMw_PecRec = new PECRECORD_T[siz];
            int sizRec = Marshal.SizeOf( aryst_RfidMw_PecRec[0] );
            for (int ind = 0; ind < siz; ind++)
            {
                //ary_PecRec[ind] = new PECRECORD_T();
                pRoot  = (IntPtr)(rRoot.ToInt32() + sizRec*ind );
                aryst_RfidMw_PecRec[ind] = (PECRECORD_T)Marshal.PtrToStructure(pRoot, typeof(PECRECORD_T)); // r2Root
            }
            Marshal.FreeHGlobal(rRoot);
          }
            return hr;
        }

        public static void f_handlemsg_RfidMw_TagInv_GetTaglist(
            UInt32 siz, ref IntPtr rRoot,
            UInt16[] PCArr, UINT96_T[] EPCArr, float[] RSSIArr,
            FileTime[] FTArr)
        {
            IntPtr pRoot;
            PECRECORD_T PecRec = new PECRECORD_T();
            if (siz > 0)
            {
                int sizRec = Marshal.SizeOf(PecRec);
                for (int ind = 0; ind < siz; ind++)
                {
                    // Mw passes array of PECRECORD_T, we only need the m_Epc field
                    pRoot = (IntPtr)(rRoot.ToInt32() + sizRec * ind);
                    PecRec = (PECRECORD_T)Marshal.PtrToStructure(pRoot, typeof(PECRECORD_T)); // 
                    PCArr[ind] = PecRec.m_Pc;
                    EPCArr[ind] = PecRec.m_Epc;
                    RSSIArr[ind] = RfidSp.RssiRegToDb(PecRec.m_Rssi);
                    FTArr[ind] = PecRec.m_LastUpdated; // shallow copy
                }
                Marshal.FreeHGlobal(rRoot);
            }
        }
        /////////////////////////////////////////////////////////////////////////////////
        ///RfidSp Device Control API
        public static HRESULT_RFID f_CFlow_RfidDev_Initialize(
            ref RFID_Startup_T 			            st_RfidSpReq_Startup,
            ref RFID_RetrieveAttachedRadiosList_T   st_RfidSpReq_RetrieveAttachedRadiosList ) //// +03
        {
            st_RfidSpReq_Startup.LibraryVersion.major = 0; //[out]
            st_RfidSpReq_Startup.LibraryVersion.minor = 0;
            st_RfidSpReq_Startup.LibraryVersion.patch = 0;
            //st_RfidSpReq_Startup.flags  Set by Caller;
            st_RfidSpReq_Startup.status = HRESULT_RFID.S_OK;
            //st_RfidSpReq_RetrieveAttachedRadiosList.radio_enum;// = Rfid.rfid_radio_enum;
            st_RfidSpReq_RetrieveAttachedRadiosList.flags = 0;
            st_RfidSpReq_RetrieveAttachedRadiosList.status = HRESULT_RFID.S_OK;
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_Startup(ref st_RfidSpReq_Startup);
            Thread.Sleep(1000);
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RetrieveAttachedRadiosList(ref st_RfidSpReq_RetrieveAttachedRadiosList);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_Uninitialize(ref RFID_Shutdown_T 	    st_RfidSpReq_Shutdown)          //// -03
        {
            Rfid.st_RfidSpReq_Shutdown.status = HRESULT_RFID.S_OK; // RFID_Shutdown_T 	    
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_Shutdown(      ref st_RfidSpReq_Shutdown );
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioOpen(ref RFID_RadioOpen_T st_RfidSpReq_RadioOpen)
        {
            /// RFID_RadioOpen_T
            st_RfidSpReq_RadioOpen.cookie = Rfid.st_RfidSpReq_RetrieveAttachedRadiosList.radio_enum._RadioInfo.cookie; //.ppRadioInfo[0]->cookie; 
            st_RfidSpReq_RadioOpen.handle = 0; //[out]          
            
            st_RfidSpReq_RadioOpen.status = HRESULT_RFID.S_OK;
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioOpen(ref st_RfidSpReq_RadioOpen);
            /// Rfid.rfid_handle is updated to MsgWnd as RFID_REQUEST_TYPE_MSGID_RadioOpen Msg; assigns: Rfid.rfid_handle = st_RfidSpReq_RadioOpen.handle;
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioClose(ref RFID_RadioClose_T st_RfidSpReq_RadioClose)
        {
            st_RfidSpReq_RadioClose.handle = Rfid.rfid_handle;
            st_RfidSpReq_RadioClose.status = HRESULT_RFID.S_OK;
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioClose(ref st_RfidSpReq_RadioClose);
            ///+ RfidSp.f_RfidDev_MacClearError(ref st_RfidSpReq_MacClearError);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioSetConfigurationParameter(ref  RFID_RadioGetSetConfigurationParameter_T st_RfidSpReq_RadioSetConfigurationParameter) ////
        {
            // synchronous version
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioSetConfigurationParameterS( ref st_RfidSpReq_RadioSetConfigurationParameter );
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioGetConfigurationParameter(ref  RFID_RadioGetSetConfigurationParameter_T st_RfidSpReq_RadioGetConfigurationParameter) ////
        {
            // synchronous version
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioGetConfigurationParameterS( ref st_RfidSpReq_RadioGetConfigurationParameter );
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioSetOperationMode(ref  RFID_RadioGetSetOperationMode_T st_RfidSpReq_RadioSetOperationMode) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioSetOperationMode(ref st_RfidSpReq_RadioSetOperationMode);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioGetOperationMode(ref  RFID_RadioGetSetOperationMode_T st_RfidSpReq_RadioGetOperationMode) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioGetOperationMode(ref st_RfidSpReq_RadioGetOperationMode);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioSetPowerState(ref RFID_RadioGetSetPowerState_T st_RfidSpReq_RadioSetPowerState) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioSetPowerState(ref st_RfidSpReq_RadioSetPowerState);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioGetPowerState(ref RFID_RadioGetSetPowerState_T st_RfidSpReq_RadioGetPowerState) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioGetPowerState(ref st_RfidSpReq_RadioGetPowerState);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioSetCurrentLinkProfile(ref RFID_RadioGetSetCurrentLinkProfile_T st_RfidSpReq_RadioSetCurrentLinkProfile)
        {   // RFID_RadioGetSetCurrentLinkProfile_T ;
            st_RfidSpReq_RadioSetCurrentLinkProfile.handle = Rfid.rfid_handle;
            st_RfidSpReq_RadioSetCurrentLinkProfile.status = HRESULT_RFID.S_OK;
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioSetCurrentLinkProfile(ref st_RfidSpReq_RadioSetCurrentLinkProfile);
            return hr;
        }
        // synchronous version
        public static HRESULT_RFID f_CFlow_RfidDev_RadioGetCurrentLinkProfile(ref RFID_RadioGetSetCurrentLinkProfile_T st_RfidSpReq_RadioGetCurrentLinkProfile)
        {   //RFID_RadioGetLinkProfile_T
            st_RfidSpReq_RadioGetCurrentLinkProfile.handle = Rfid.rfid_handle;
            st_RfidSpReq_RadioGetCurrentLinkProfile.status = HRESULT_RFID.S_OK; //[out] curr_link_profile at MsgWnd
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioGetCurrentLinkProfileS(ref st_RfidSpReq_RadioGetCurrentLinkProfile);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioGetLinkProfile(ref RFID_RadioGetLinkProfile_T st_RfidSpReq_RadioGetLinkProfile)
        {   //RFID_RadioGetLinkProfile_T
            //[in] current profile
            st_RfidSpReq_RadioGetLinkProfile.handle = Rfid.rfid_handle;
            st_RfidSpReq_RadioGetLinkProfile.status = HRESULT_RFID.S_OK; //[out] curr_link_profile at MsgWnd
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioGetLinkProfile(ref st_RfidSpReq_RadioGetLinkProfile);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_AntennaPortGetStatus( ref RFID_AntennaPortGetStatus_T st_RfidSpReq_AntennaPortGetStatus )
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_AntennaPortGetStatus( ref st_RfidSpReq_AntennaPortGetStatus );
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_AntennaPortSetState(ref RFID_AntennaPortSetState_T st_RfidSpReq_AntennaPortSetState)
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_AntennaPortSetState(ref st_RfidSpReq_AntennaPortSetState);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_AntennaPortSetConfiguration(ref RFID_AntennaPortGetSetConfiguration_T st_RfidSpReq_AntennaPortSetConfiguration) //// +04
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_AntennaPortSetConfiguration(ref st_RfidSpReq_AntennaPortSetConfiguration);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_AntennaPortGetConfiguration(ref RFID_AntennaPortGetSetConfiguration_T st_RfidSpReq_AntennaPortGetConfiguration) //// +04
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_AntennaPortGetConfiguration(ref st_RfidSpReq_AntennaPortGetConfiguration);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetSelectCriteria01(ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetSelectCriteria01(ref st_RfidSpReq_18K6CSetSelectCriteria, ref criteria01);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetSelectCriteria02(ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetSelectCriteria02(ref st_RfidSpReq_18K6CSetSelectCriteria, ref criteria01, ref criteria02);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetSelectCriteria03(ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02, ref RFID_18K6C_SELECT_CRITERION criteria03) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetSelectCriteria03(ref st_RfidSpReq_18K6CSetSelectCriteria, ref criteria01, ref criteria02, ref criteria03);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetSelectCriteria04(ref RFID_18K6CSetSelectCriteria__T st_RfidSpReq_18K6CSetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02, ref RFID_18K6C_SELECT_CRITERION criteria03, ref RFID_18K6C_SELECT_CRITERION criteria04) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetSelectCriteria04(ref st_RfidSpReq_18K6CSetSelectCriteria, ref criteria01, ref criteria02, ref criteria03, ref criteria04);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetSelectCriteria(ref RFID_18K6CGetSelectCriteria_T st_RfidSpReq_18K6CGetSelectCriteria)
        {
            // Leave the 'IntPtr' alone for now, the callee is not using memory allocated from C# anyway.
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetSelectCriteria(ref Rfid.st_RfidSpReq_18K6CGetSelectCriteria);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetSelectCriteria(ref RFID_18K6CSetSelectCriteria_T st_RfidSpReq_18K6CSetSelectCriteria, RFID_18K6C_SELECT_CRITERION[] criteria)
        {
            // Marshal criteria to Native function
            int n = criteria != null? criteria.Length : 0;
            int ElemSz = Marshal.SizeOf(typeof (RFID_18K6C_SELECT_CRITERION));
            IntPtr critPtr = Marshal.AllocCoTaskMem(n * ElemSz);
            st_RfidSpReq_18K6CSetSelectCriteria.criteria.criteria = critPtr; // point to head of array
            Byte[] FullSzMask = new Byte[ReaderTypes.RfidSp.RFID_18K6C_SELECT_MASK_BYTE_LEN];
            Byte[] SavedMask;
            for (int c = 0; c < n; c++)
            {
                if (criteria[c].mask.mask != null)
                    criteria[c].mask.mask.CopyTo(FullSzMask, 0);
                SavedMask = criteria[c].mask.mask;
                criteria[c].mask.mask = FullSzMask; // to satisfy the 'SizeConst' Attribute
                Marshal.StructureToPtr(criteria[c], critPtr, false);
                critPtr = (IntPtr)((long)critPtr + ElemSz);
                criteria[c].mask.mask = SavedMask;
            }
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetSelectCriteria(ref st_RfidSpReq_18K6CSetSelectCriteria);
            Marshal.FreeCoTaskMem(st_RfidSpReq_18K6CSetSelectCriteria.criteria.criteria);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetSelectCriteria01(ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetSelectCriteria01(ref st_RfidSpReq_18K6CGetSelectCriteria, ref criteria01);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetSelectCriteria02(ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetSelectCriteria02(ref st_RfidSpReq_18K6CGetSelectCriteria, ref criteria01, ref criteria02);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetSelectCriteria03(ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02, ref RFID_18K6C_SELECT_CRITERION criteria03) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetSelectCriteria03(ref st_RfidSpReq_18K6CGetSelectCriteria, ref criteria01, ref criteria02, ref criteria03);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetSelectCriteria04(ref RFID_18K6CGetSelectCriteria__T st_RfidSpReq_18K6CGetSelectCriteria,
            ref RFID_18K6C_SELECT_CRITERION criteria01, ref RFID_18K6C_SELECT_CRITERION criteria02, ref RFID_18K6C_SELECT_CRITERION criteria03, ref RFID_18K6C_SELECT_CRITERION criteria04) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetSelectCriteria04(ref st_RfidSpReq_18K6CGetSelectCriteria, ref criteria01, ref criteria02, ref criteria03, ref criteria04);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetPostMatchCriteria(ref RFID_18K6CSetPostMatchCriteria_T st_RfidSpReq_18K6CSetPostMatchCriteria, RFID_18K6C_SINGULATION_CRITERION[] criteria)
        {
            // Marshal criteria to Native function
            int n = criteria != null? criteria.Length : 0;
            int ElemSz = Marshal.SizeOf(typeof(RFID_18K6C_SINGULATION_CRITERION));
            IntPtr critPtr = Marshal.AllocCoTaskMem(n * ElemSz);
            st_RfidSpReq_18K6CSetPostMatchCriteria.criteria.criteria = critPtr;
            Byte[] FullSzMask = new Byte[ReaderTypes.RfidSp.RFID_18K6C_SINGULATION_MASK_BYTE_LEN];
            Byte[] SavedMask;
            for (int c = 0; c < n; c++)
            {
                if (criteria[c].mask.mask != null)
                    criteria[c].mask.mask.CopyTo(FullSzMask, 0);
                SavedMask = criteria[c].mask.mask;
                criteria[c].mask.mask = FullSzMask; // to satisfy the 'SizeConst' Attribute
                Marshal.StructureToPtr(criteria[c], critPtr, false);
                critPtr = (IntPtr)((long)critPtr + ElemSz);
                criteria[c].mask.mask = SavedMask;
            }
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetPostMatchCriteria (
                ref st_RfidSpReq_18K6CSetPostMatchCriteria);
            Marshal.FreeCoTaskMem(st_RfidSpReq_18K6CSetPostMatchCriteria.criteria.criteria);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetPostMatchCriteria01(ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria, ref RFID_18K6C_SINGULATION_CRITERION criteria01) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetPostMatchCriteria01(ref st_RfidSpReq_18K6CSetPostMatchCriteria, ref criteria01);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetPostMatchCriteria02(ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria, 
        ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetPostMatchCriteria02(ref st_RfidSpReq_18K6CSetPostMatchCriteria, ref criteria01, ref criteria02);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetPostMatchCriteria03(ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria,
        ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02, ref RFID_18K6C_SINGULATION_CRITERION criteria03) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetPostMatchCriteria03(ref st_RfidSpReq_18K6CSetPostMatchCriteria, ref criteria01, ref criteria02, ref criteria03);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetPostMatchCriteria04(ref RFID_18K6CSetPostMatchCriteria__T st_RfidSpReq_18K6CSetPostMatchCriteria,
        ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02, ref RFID_18K6C_SINGULATION_CRITERION criteria03, ref RFID_18K6C_SINGULATION_CRITERION criteria04) ////        
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetPostMatchCriteria04(ref st_RfidSpReq_18K6CSetPostMatchCriteria, ref criteria01, ref criteria02, ref criteria03, ref criteria04);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetPostMatchCriteria01(ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria, ref RFID_18K6C_SINGULATION_CRITERION criteria01) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetPostMatchCriteria01(ref st_RfidSpReq_18K6CGetPostMatchCriteria, ref criteria01);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetPostMatchCriteria02(ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria, 
        ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetPostMatchCriteria02(ref st_RfidSpReq_18K6CGetPostMatchCriteria, ref criteria01, ref criteria02);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetPostMatchCriteria03(ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria,
        ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02, ref RFID_18K6C_SINGULATION_CRITERION criteria03) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetPostMatchCriteria03(ref st_RfidSpReq_18K6CGetPostMatchCriteria, ref criteria01, ref criteria02, ref criteria03);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetPostMatchCriteria04(ref RFID_18K6CGetPostMatchCriteria__T st_RfidSpReq_18K6CGetPostMatchCriteria,
        ref RFID_18K6C_SINGULATION_CRITERION criteria01, ref RFID_18K6C_SINGULATION_CRITERION criteria02, ref RFID_18K6C_SINGULATION_CRITERION criteria03, ref RFID_18K6C_SINGULATION_CRITERION criteria04) ////        
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetPostMatchCriteria04(ref st_RfidSpReq_18K6CGetPostMatchCriteria, ref criteria01, ref criteria02, ref criteria03, ref criteria04);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CSetQueryParameters(ref RFID_18K6CSetQueryParameters_T st_RfidSpReq_18K6CSetQueryParameters) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CSetQueryParameters(ref st_RfidSpReq_18K6CSetQueryParameters);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CGetQueryParameters(
            ref RFID_18K6CGetQueryParameters_T st_RfidSpReq_18K6CGetQueryParameters)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CGetQueryParameters(
                ref Rfid.st_RfidSpReq_18K6CGetQueryParameters);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_GetInventory(ref RFID_18K6CTagInventory_T st_RfidSpReq_18K6CTagInventory) //// +04
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_18K6CTagInventory( ref st_RfidSpReq_18K6CTagInventory);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CTagRead(ref RFID_18K6CTagRead_T st_RfidSpReq_18K6CTagRead) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CTagRead(ref st_RfidSpReq_18K6CTagRead);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CTagWrite(ref RFID_18K6CTagWrite_T st_RfidSpReq_18K6CTagWrite) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CTagWrite(ref st_RfidSpReq_18K6CTagWrite);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CTagKill(ref RFID_18K6CTagKill_T st_RfidSpReq_18K6CTagKill) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CTagKill(ref st_RfidSpReq_18K6CTagKill);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_18K6CTagLock(ref  RFID_18K6CTagLock_T st_RfidSpReq_18K6CTagLock) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_18K6CTagLock(ref st_RfidSpReq_18K6CTagLock);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioSetResponseDataMode(ref RFID_RadioGetSetResponseDataMode_T st_RfidSpReq_RadioSetResponseDataMode) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioSetResponseDataMode(ref st_RfidSpReq_RadioSetResponseDataMode);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioGetResponseDataMode(ref RFID_RadioGetSetResponseDataMode_T st_RfidSpReq_RadioGetResponseDataMode) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioGetResponseDataMode(ref st_RfidSpReq_RadioGetResponseDataMode);
            return hr;
        }
        // synchronous version
        public static HRESULT_RFID f_CFlow_RfidDev_MacClearError(ref RFID_MacClearError_T st_RfidSpReq_MacClearError)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacClearErrorS(ref st_RfidSpReq_MacClearError);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_MacReset(ref RFID_RadioAbortOperation_T st_RfidSpReq_RadioAbortOperation,
                                                                ref RFID_MacReset_T st_RfidSpReq_MacReset) ////
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioAbortOperation(ref st_RfidSpReq_RadioAbortOperation);
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacReset(ref st_RfidSpReq_MacReset);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_MacGetVersion(ref RFID_MacGetVersion_T st_RfidSpReq_MacGetVersion)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacGetVersion(ref st_RfidSpReq_MacGetVersion);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_MacReadOemData(ref RFID_MacReadWriteOemData_T st_RfidSpReq_MacReadWriteOemData)
        {
            if (st_RfidSpReq_MacReadWriteOemData.pData.Length < st_RfidSpReq_MacReadWriteOemData.count)
                return HRESULT_RFID.E_RFID_ERROR_INVALID_PARAMETER;
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacReadOemDataS(st_RfidSpReq_MacReadWriteOemData.handle, st_RfidSpReq_MacReadWriteOemData.address, st_RfidSpReq_MacReadWriteOemData.count, st_RfidSpReq_MacReadWriteOemData.pData);
            st_RfidSpReq_MacReadWriteOemData.status = hr;
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_MacBypassWriteRegister(ref RFID_MacBypassReadWriteRegister_T st_RfidSpReq_MacBypassWriteRegister)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacBypassWriteRegister(ref st_RfidSpReq_MacBypassWriteRegister);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_MacBypassReadRegister(ref RFID_MacBypassReadWriteRegister_T st_RfidSpReq_MacBypassReadRegister)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacBypassReadRegisterS(ref st_RfidSpReq_MacBypassReadRegister);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_MacSetRegion(ref RFID_MacGetSetRegion_T st_RfidSpReq_MacSetRegion)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacSetRegion(ref st_RfidSpReq_MacSetRegion);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_MacGetRegion(ref RFID_MacGetSetRegion_T st_RfidSpReq_MacGetRegion)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_MacGetRegion(ref st_RfidSpReq_MacGetRegion);
            return hr;
        }
        /////////////////////////////////////////////////////////////////////////////////
        public static HRESULT_RFID f_CFlow_RfidDev_Abort(ref  RFID_RadioAbortOperation_T st_RfidSpReq_RadioAbortOperation) //// +Rfid Abort
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_RadioAbortOperation(ref st_RfidSpReq_RadioAbortOperation);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_Cancel(ref RFID_RadioCancelOperation_T st_RfidSpReq_RadioCancelOperation) //// +Rfid Cancel 
        {
            hr = (HRESULT_RFID) RfidSp.f_RfidDev_RadioCancelOperation(ref st_RfidSpReq_RadioCancelOperation);
            return hr;
        }
        /////////////////////////////////////////////////////////////////////////////////
        public static HRESULT_RFID f_CFlow_RfidDev_RadioTurnCarrierWaveOn(RFID_RADIO_HANDLE handle)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioTurnCarrierWaveOn(handle);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_RadioTurnCarrierWaveOff(RFID_RADIO_HANDLE handle)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_RadioTurnCarrierWaveOff(handle);
            return hr;
        }
        /////////////////////////////////////////////////////////////////////////////////
        public static HRESULT_RFID f_CFlow_RfidDev_CustomGetTemperature(RFID_RADIO_HANDLE handle, out ushort amb, out ushort xcvr, out ushort pamp)
        {
            amb = 0; xcvr = 0; pamp = 0;
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomGetTemperatureS(handle, ref amb,
                ref xcvr, ref pamp);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_CustomGetThrshTemperature(RFID_RADIO_HANDLE handle)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomGetThrshTemperature(handle);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_CustomGetTagInvtryRdRate(ref RFID_18K6CTagInventory_T st_RfidSpReq_18K6CTagInventory)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomGetTagInvtryRdRate(ref st_RfidSpReq_18K6CTagInventory);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_CustomGetTagInvtryRssi(ref RFID_18K6CTagInventory_T st_RfidSpReq_18K6CTagInventory, bool filterRes)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomGetTagInvtryRssi(ref st_RfidSpReq_18K6CTagInventory, filterRes);
            return hr;
        }
        // synchronous version
        public static HRESULT_RFID f_CFlow_RfidDev_CustomGetFreqBandNum(RFID_RADIO_HANDLE handle, out uint bandNum)
        {
            bandNum = 2;
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomGetFreqBandNumS(handle, ref bandNum);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_CustomSetFreqBandNum(RFID_RADIO_HANDLE handle,
            UInt32 num)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomSetFreqBandNum(handle, num);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_CustomGetRadioProfile(RFID_RADIO_HANDLE handle)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomGetRadioProfile(handle);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_CustomSetRadioChn (
            RFID_RADIO_HANDLE handle, CustomFreqGrp freqGrp, int chn, bool enLBT)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomSetRadioChn(handle, freqGrp, chn, enLBT);
            return hr;
        }
        public static HRESULT_RFID f_CFlow_RfidDev_CustomSetRadioProfile(
            RFID_RADIO_HANDLE handle, CustomFreqGrp freqGrp, bool enLBT)
        {
            hr = (HRESULT_RFID)RfidSp.f_RfidDev_CustomSetRadioProfile(handle, (int)freqGrp, enLBT);
            return hr;
        }
    }
}
