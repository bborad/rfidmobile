using System;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ClsReaderLib.Devices.Barcode
{
    public class BarCodeRdr
    {
        static private SerialPort com1;
        static private BarCodeRdr theRdr;
        static private Object rdrLck = new Object();

        private bool disposed = false;
        private bool stopPending = false;
        static public BarCodeRdr GetInstance()
        {
            if (theRdr == null)
            {
                theRdr = new BarCodeRdr();
            }
            return theRdr;
        }

        public delegate void StopNotify();
        private StopNotify stopNotify;
        private Form stopNotifee;

        #region Default Setup
        private void SetupDefaultCfg ()
        {
            String Msg;
               // Read Time-out (Trigger mode)
            if (!ChangeCurrentSetting("TRG", "STO", "0", out Msg))// inifinite read timeout (rely on 'cancel' button)
                throw new ApplicationException("Scanner Setup TRG Error: " + Msg);
            // MicroPDF
            if (!ChangeCurrentSetting("MPD", "ENA", "1", out Msg))
                throw new ApplicationException("Scanner Setup MPD Error: " + Msg);
            //Postnet
            if (!ChangeCurrentSetting("NET", "ENA", "1", out Msg))
                throw new ApplicationException("Scanner Setup NET Error: " + Msg);
            // EAN-UCC Composite
            if (!ChangeCurrentSetting("COM", "ENA", "1", out Msg))
                throw new ApplicationException("Scanner Setup COM Error: " + Msg);
            // British Post
            if (!ChangeCurrentSetting("BPO", "ENA", "1", out Msg))
                throw new ApplicationException("Scanner Setup BPO Error: " + Msg);
            // Japanese Post
            if (!ChangeCurrentSetting("JAP", "ENA", "1", out Msg))
                throw new ApplicationException("Scanner Setup JAP Error: " + Msg);
            // China Post
            if (!ChangeCurrentSetting("CPC", "ENA", "1", out Msg))
                throw new ApplicationException("Scanner Setup CPC Error: " + Msg);

        }

        #endregion
        #region ctors
        private BarCodeRdr()
        {
            disposed = false;

            PosSp.ScnrLaserOff();

            com1 = new SerialPort("COM1", 115200);

            //ShowSerialPortStatus(com1);

            // Get Around not working: ProblemGetAround();

            this.SetupDefaultCfg();
            
        }
        #endregion

        #region dtors
        ~BarCodeRdr()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing) // calling from Dispose() directly
                {
                    // free managed resources
                }
                // free unmanaged resources
                com1.Disposed -= OnSerPrtDisposed;
                com1.Dispose();

                disposed = true;
            }
        }
        #endregion

        #region Misc Routines
        private void ShowSerialPortStatus(SerialPort com1)
        {
            StringBuilder Sb = new StringBuilder();

            Sb = Sb.Append("COM1 Details:\n");
            Sb = Sb.Append("Baud Rate: " + com1.BaudRate.ToString() + "\n");
            Sb = Sb.Append("Data Bits: " + com1.DataBits + "\n");
            Sb = Sb.Append("Parity: " + com1.Parity.ToString("F") + "\n");
            Sb = Sb.Append("Handshake: " + com1.Handshake.ToString("F") + "\n");
            Sb = Sb.Append("Stop: " + com1.StopBits.ToString("F") + "\n");
            Sb = Sb.Append("CD: " + (com1.CDHolding ? "H" : "L") + "\n");
            Sb = Sb.Append("CTS: " + (com1.CtsHolding ? "H" : "L") + "\n");
            Sb = Sb.Append("DSR: " + (com1.DsrHolding ? "H" : "L") + "\n");
            Sb = Sb.Append("Dtr Enable: " + com1.DtrEnable.ToString() + "\n");
            Sb = Sb.Append("Rts Enable: " + com1.RtsEnable.ToString() + "\n");
            MessageBox.Show(Sb.ToString());
        }

        private void ProblemGetAround()
        {
            try
            {
                com1.Open();
                com1.Close();
            }
            catch (Exception e)
            {
                throw new ApplicationException("ProblemGetAround:\r\n" + e.Message);
            }
        }
        #endregion

        #region Serial Port Event Handlers
        private void OnSerPrtDisposed(object sender, EventArgs e)
        {
            throw new ApplicationException("SerialPort Disposed");
        }

        private String GetConvDataStr(SerialData serDataType)
        {
            String DataStr = null;

            int NumBytesToRd = 0;
            char[] RdBuf = null;
            switch (serDataType)
            {
                case SerialData.Chars:
                case SerialData.Eof:
                    try
                    {
                        NumBytesToRd = com1.BytesToRead;
                        if (NumBytesToRd > 0)
                        {
                            RdBuf = new char[NumBytesToRd];
                            //for (int i = 0; i < NumBytesToRd; i++)
                            //    RdBuf[i] = (Char)com1.ReadByte();
                            com1.Encoding = Encoding.ASCII;
                             com1.Read(RdBuf, 0, NumBytesToRd);

                            

                           // com1.
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        // com1 is being/already closed
                        RdBuf = null;
                    }
                    break;
            }

            if (RdBuf != null)
            {
                // Byte Array To String
                StringBuilder Sb = new StringBuilder(NumBytesToRd);
                Sb = Sb.Append(RdBuf);
                DataStr = Sb.ToString();
            }

            int indexof = -1;

            if (DataStr != null && DataStr.Length > 0)
            {
                indexof = DataStr.IndexOf("MSGGET");
            }

            if (indexof>=0)
            {
                DataStr = DataStr.Remove(0, indexof + 6);
            }

            
            

            return DataStr;
        }

        private void OnSerPrtDataRcvd(object sender, SerialDataReceivedEventArgs e)
        {
            //ClslibRfidSp.Datalog.LogStr("+++++ Serial Port Data Received\n");
            Monitor.Enter (rdrLck); // make sure that no one else touches the com1 before this finishes
                
            String DataStr = GetConvDataStr(e.EventType);     
           

            int NumBytesToRd = 0;
            if (DataStr != null)
            {
                NumBytesToRd = DataStr.Length;
                //ClslibRfidSp.Datalog.LogStr("+++++ About to Deliver Scan Notify\n");
                DeliverScanNotify(true, DataStr, null);
                //ClslibRfidSp.Datalog.LogStr("+++++ Return from Deliver Scan Notify\n");
             
            }

            if (com1.IsOpen)
            {
                com1.DiscardInBuffer();
            }

            //if (e.EventType == SerialData.Eof)
            //{
            //    com1.DiscardInBuffer();
            //   // DeliverScanNotify(false, null, "EOF Reached: " + NumBytesToRd + "bytes");
            //}

            Monitor.Exit(rdrLck);
        }

        private void OnSerPrtErrRcvd(object sender, SerialErrorReceivedEventArgs e)
        {
            // e.EventType
            DeliverScanNotify(false, null, e.EventType.ToString() + "\r\n" + e.ToString());
        }

        //
        // Serial Port Runs in different Thread (C# thing)
        // Use InvokeRequired to help
        //
        private bool DeliverScanNotify(bool succ, String bcStr, String errMsg)
        {
            bool delivered = false;
            bool contOp = false;
     
            if (scanNotify != null)
            {
                if (notifyee.InvokeRequired)
                    contOp = (bool)notifyee.Invoke(scanNotify, succ, bcStr, errMsg);
                else
                    contOp = scanNotify(succ, bcStr, errMsg);
                delivered = true;
            }
            // Note: the following might cause problem (this comes from Secondary Thread)
            if (contOp)
                ScanRestart();
            else
                ScanStop();

            if (stopPending) // this catches the ScanStop() request after 'ContOp'
            {
                stopPending = false;
                ScanStop();
                if (stopNotify != null)
                {
                    if (stopNotifee.InvokeRequired)
                        stopNotifee.Invoke(stopNotify);
                }
            }
            return delivered;
        }
        #endregion

        #region delegates
        public delegate bool CodeRcvdNotify(bool Succ, String codeStr, String errStr);
        private CodeRcvdNotify scanNotify;
        private Form notifyee;
        #endregion

        #region Public Read Routines (asychronous, multi-thread)
        public bool ScanStart(CodeRcvdNotify notify, Form caller)
        {
            scanNotify = notify;
            notifyee = caller;

            Monitor.Enter(rdrLck); // TBD: Catch Exception
            
            com1.DataReceived += new SerialDataReceivedEventHandler(OnSerPrtDataRcvd);
            com1.ErrorReceived += new SerialErrorReceivedEventHandler(OnSerPrtErrRcvd);

            // order of this?
            PosSp.ScnrLaserOn();

            if (!com1.IsOpen)
            {
                com1.Disposed += new EventHandler(OnSerPrtDisposed);
                com1.Open();
            }

            Monitor.Exit(rdrLck);
            return true;
        }

        private bool _ScanStop()
        {
            bool Succ = false;

            // order of this?
            //ClslibRfidSp.Datalog.LogStr("-------- Requesting ScanStop()\n");

            PosSp.ScnrLaserOff();

            if (com1.IsOpen)
            {


                com1.DataReceived -= OnSerPrtDataRcvd;
                com1.ErrorReceived -= OnSerPrtErrRcvd;
                // Disposed would be called by Close()
                com1.Disposed -= OnSerPrtDisposed;

                // com1.Close() would block if DataReceived Event is emitted
                // Unlock the rdrLck to allow Notification to go through
                //ClslibRfidSp.Datalog.LogStr("------ Closing COM1\n");
                /* Assuming Close() would do this
                    com1.DiscardInBuffer();
                    com1.DiscardOutBuffer();
                */
                com1.Close();

                Succ = true;

                //if (com1.BytesToRead > 0)
                //{
                //    // DataReceived Event might be pending/in-progress
                //    // Do not close because this comes from a different
                //    // thread
                //   // com1.DiscardInBuffer();
                //    Succ = false;
                //}
                //else
                //{
                //    com1.DataReceived -= OnSerPrtDataRcvd;
                //    com1.ErrorReceived -= OnSerPrtErrRcvd;
                //    // Disposed would be called by Close()
                //    com1.Disposed -= OnSerPrtDisposed;

                //    // com1.Close() would block if DataReceived Event is emitted
                //    // Unlock the rdrLck to allow Notification to go through
                //    //ClslibRfidSp.Datalog.LogStr("------ Closing COM1\n");
                //    /* Assuming Close() would do this
                //        com1.DiscardInBuffer();
                //        com1.DiscardOutBuffer();
                //    */
                //    com1.Close();
                //    // might consider put delay here to avoid immediate
                //    // re-opening of com1(MSDN doc caution against this)

                //    Succ = true;
                //}
            }
            else
                throw new ApplicationException("COM1 Already Closed?");

            return Succ;
        }

        // use StopNotify to notify caller once it's stopped after
        // this function returns
        public bool ScanTryStop(StopNotify notify, Form caller)
        {
            if (!Monitor.TryEnter(rdrLck))
            {
                stopNotify = notify;
                stopNotifee = caller;
                stopPending = true;
                return false;
            }

            bool Succ = false;

            try
            {
                Succ = _ScanStop();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
            finally
            {
                Monitor.Exit(rdrLck);
            }

            return Succ;
        }

        private bool ScanStop()
        {
            bool Succ = false;

            Monitor.Enter(rdrLck);
            try
            {
                Succ = _ScanStop();
            }
            catch
            {
            }
            finally
            {
                Monitor.Exit(rdrLck);
            }
            
            return Succ;
        }

        private bool ScanRestart()
        {
            //ClslibRfidSp.Datalog.LogStr("-------- Requesting ScanRestart()\n");    
            // Is the com port closed?
            PosSp.ScnrLaserOff();
            PosSp.ScnrLaserOn();
      
            return true;
        }
        #endregion

        #region Scanner Setting Query/Set (synchronous calls)
        private bool IssueSettingChangeCmd(String tag, String subTag, String dataFld, bool persistent, out String retStr)
        {
            int Retries = 0;
            bool Succ = false;
            retStr = null;

            TryAgain:
            if (!com1.IsOpen)
            {
                com1.Open();               
            }
            if (tag.Length < 3 || subTag.Length < 3)
            {
                retStr = "Tag/SubTag must be 3 characters long";
                return false;
            }
            Byte[] OutBuf = new Byte[3 + 3 + 3 + dataFld.Length + 1];
            // SYN M R
            OutBuf[0] = 22; OutBuf[1] = 77; OutBuf[2] = 13;
            // Tag
            for (int di = 3, si = 0; si < 3; di++, si++)
                OutBuf[di] = (Byte)tag[si];
            // SubTag
            for (int di = 6, si = 0; si < 3; di++, si++)
                OutBuf[di] = (Byte)subTag[si];
            //Data Field
            for (int di = 9, si = 0; si < dataFld.Length; di++, si++)
                OutBuf[di] = (Byte)dataFld[si];
            if (persistent)
            {
                // Save setting to Non-Volatile Memory
                OutBuf[OutBuf.Length - 1] = (Byte)'.';
            }
            else
            {
                // Save setting to Volatile Memory
                OutBuf[OutBuf.Length - 1] = (Byte)'!';
            }

            try
            {
                com1.Write(OutBuf, 0, OutBuf.Length);
                com1.ReadTimeout = 2000;
                // Read until ACK(6)/ENQ(5)/NAK(21) is received
                Char InB; // use Char type for easy conversion to String
                bool GotEndChar = false;
                StringBuilder Sb = new StringBuilder();
                do
                {
                    InB = (Char)com1.ReadByte();
                    switch ((uint)InB)
                    {
                        case 5: //ENQ (wrong tag)
                            retStr = "Invalid Tag or SubTag command (" + tag + subTag + ")";
                            GotEndChar = true;
                            break;
                        case 6:
                            Succ = true;
                            retStr = Sb.ToString();
                            GotEndChar = true;
                            break;
                        case 21:
                            Succ = false;
                            retStr = "Data Field Entry out of allowable Range for this command: (" + tag + subTag + ")";
                            GotEndChar = true;
                            break;
                        default:
                            // Add Byte to StringBuilder
                            Sb = Sb.Append(InB);
                            break;
                    }
                }
                while (!GotEndChar);
            }
            catch (TimeoutException te)
            {
                // retry 3 times(?)
                if ((++Retries) < 3)
                {
                    com1.Close();
                    goto TryAgain;
                }
                else
                {
                    retStr = te.Message;
                    Succ = false;
                }
            }
            catch (Exception e)
            {
                retStr = e.Message;
                Succ = false;
            }
            com1.Close();

            return Succ;
        }

      

        // Make change to Volatile Memory (changed setting lost after power-cycle)
        public bool ChangeCurrentSetting(String tag, String subTag, String dataFld, out String retStr)
        {
            return IssueSettingChangeCmd(tag, subTag, dataFld, false, out retStr);
        }

        public bool ChangeSavedSetting(String tag, String subTag, String dataFld, out String retStr)
        {
            return IssueSettingChangeCmd(tag, subTag, dataFld, true, out retStr);
        }

        public bool GetCurrentSetting(String tag, String subTag, out String retStr)
        {
            bool Succ = false;
            retStr = null;
            if (!com1.IsOpen)
            {
                com1.Open();
            }
            if (tag.Length < 3 || subTag.Length < 3)
            {
                retStr = "Tag/SubTag must be 3 characters long";
                return false;
            }
            Byte[] OutBuf = new Byte[3 + 3 + 3 + 2];
            OutBuf[0] = 22; OutBuf[1] = 77; OutBuf[2] = 13; // SYN M R
            for (int di = 3, si = 0; si < 3; di++, si++)
                OutBuf[di] = (Byte)tag[si];
            for (int di = 6, si = 0; si < 3; di++, si++)
                OutBuf[di] = (Byte)subTag[si];
            // Query Current Setting
            OutBuf[9] = (Byte)'?'; OutBuf[10] = (Byte)'.';

            try
            {
                com1.Write(OutBuf, 0, OutBuf.Length);
                com1.ReadTimeout = 2000;
                // Read until ACK(6)/ENQ(5)/NAK(21) is received
                Char InB; // use Char type for easy conversion to String
                bool GotEndChar = false;
                StringBuilder Sb = new StringBuilder();
                do
                {
                    InB = (Char)com1.ReadByte();
                    switch ((uint)InB)
                    {
                        case 5: //ENQ (wrong tag)
                            retStr = "Invalid Tag or SubTag command";
                            GotEndChar = true;
                            break;
                        case 6:
                            Succ = true;
                            retStr = Sb.ToString();
                            GotEndChar = true;
                            break;
                        case 21:
                            Succ = false;
                            retStr = "Data Field Entry out of allowable Range for this command";
                            GotEndChar = true;
                            break;
                        default:
                            // Add Byte to StringBuilder
                            Sb = Sb.Append(InB);
                            break;
                    }
                }
                while (!GotEndChar);
            }
            catch (Exception e)
            {
                retStr = e.Message;
                Succ = false;
            }
            com1.Close();

            return Succ;
        }
        #endregion
    }
}
