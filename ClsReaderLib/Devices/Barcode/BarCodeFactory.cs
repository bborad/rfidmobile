﻿using System;
using System.Collections.Generic;
 
using System.Text;

using ReaderTypes;
using OnRamp;
using CS101ReaderLib;
using AT870ReaderLib;

namespace ClsReaderLib
{
    public static class BarCodeFactory
    {
        public static HHDeviceInterface.BarCode.BarCodeReader GetBarCodeRdr()
        {
            HHDeviceInterface.BarCode.BarCodeReader objBarCodeRdr = null;
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                CS101ReaderLib.BarCode_Reader objBarRdr = CS101ReaderLib.BarCode_Reader.GetInstance();
                objBarCodeRdr = (HHDeviceInterface.BarCode.BarCodeReader)objBarRdr;
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                AT870ReaderLib.BarCode_Reader objBarRdr = new AT870ReaderLib.BarCode_Reader();
                objBarCodeRdr = (HHDeviceInterface.BarCode.BarCodeReader)objBarRdr;
            }
            return objBarCodeRdr;
        }
    }
}
