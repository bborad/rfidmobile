﻿using System;
using System.Collections.Generic;
 
using System.Text;
using ReaderTypes;
using OnRamp;
using AT870 = AT870ReaderLib.UtilityTypes;
using CS101 = CS101ReaderLib.UtilityTypes;

namespace ClsReaderLib.Devices
{
    #region SettingMgt_funcs
    public class SettingMgt
    {
        //private const String W_SettingMgtDLL_Path = "W_SettingMgt.dll";

        public SettingMgt() { }


        public static HRESULT_RFID f_ApplicationSetup()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_ApplicationSetup();
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_ApplicationSetup();
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }           
            
        }

        /// HRESULT WINAPI f_SettingMgt_Initialize( HWND  hWnd ); //System.Runtime.InteropServices.

        public static HRESULT_RFID f_SettingMgt_Initialize(IntPtr hWnd)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_SettingMgt_Initialize(hWnd);
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_SettingMgt_Initialize(hWnd);
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }     
        }
        /// HRESULT WINAPI f_SettingMgt_Uninitialize(); 

        public static HRESULT_RFID f_SettingMgt_Uninitialize()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_SettingMgt_Uninitialize();
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_SettingMgt_Uninitialize();
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }     
        }
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Read(); 

        public static HRESULT_RFID f_SettingMgt_RfidSpSetting_Read()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_SettingMgt_RfidSpSetting_Read();
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_SettingMgt_RfidSpSetting_Read();
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }     
        }
        /// HRESULT WINAPI f_SettingMgt_RfidSpSetting_Write(); 

        public static HRESULT_RFID f_SettingMgt_RfidSpSetting_Write()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_SettingMgt_RfidSpSetting_Write();
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_SettingMgt_RfidSpSetting_Write();
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }     
        }

        public static HRESULT_RFID f_SettingMgt_StartLogTrace(String logDirPath)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_SettingMgt_StartLogTrace(logDirPath);
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_SettingMgt_StartLogTrace(logDirPath);
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }     
        }

        public static HRESULT_RFID f_SettingMgt_StopLogTrace()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_SettingMgt_StopLogTrace();
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_SettingMgt_StopLogTrace();
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }     
        }

        public static HRESULT_RFID f_CollectWifiTCPIPStatus(
                               ref bool enabled, ref bool connected, StringBuilder hwaddr, StringBuilder ipaddr, StringBuilder ipmask,
                               StringBuilder gwaddr, StringBuilder dnsaddr1, StringBuilder dnsaddr2,
                               ref bool dhcp, StringBuilder dhcpaddr, SystemTime leaseTm, SystemTime expireTm)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_CollectWifiTCPIPStatus(
                               ref  enabled, ref   connected, hwaddr, ipaddr, ipmask,
                                 gwaddr, dnsaddr1, dnsaddr2,
                               ref   dhcp, dhcpaddr, leaseTm, expireTm);
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_CollectWifiTCPIPStatus(
                               ref  enabled, ref   connected, hwaddr, ipaddr, ipmask,
                                 gwaddr, dnsaddr1, dnsaddr2,
                               ref   dhcp, dhcpaddr, leaseTm, expireTm);
            }
            else
            {
                return HRESULT_RFID.S_OK;
            }     
        }

        public static Int32 f_SetWINSHostName(String hostName)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_SetWINSHostName(hostName);
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_SetWINSHostName(hostName);
            }
            else
            {
                return 0;
            }     
        }

        public static Int32 f_GetWINSHostName(StringBuilder hostName, int bufSz)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                return CS101.SettingMgt.f_GetWINSHostName(hostName, bufSz);
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                return AT870.SettingMgt.f_GetWINSHostName(hostName, bufSz);
            }
            else
            {
                return 0;
            }     
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    #endregion
}
