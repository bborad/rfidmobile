USE [OnRamp]
GO
/****** Object:  Trigger [dbo].[DispatchTrigger]    Script Date: 11/07/2012 15:36:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[DispatchTrigger]
ON [dbo].[Assets]
AFTER INSERT,UPDATE 
AS
Update dbo.Assets 
set Is_Active=0,AssetNo = TagID, TagID = 'xxxxxxxxxxxxxxDispatched' 
where ID_location in (select ID_Location from Locations where tagid='0000000000000000000000D') --AND is_active = 1
