USE [OnRamp]
GO
/****** Object:  View [dbo].[Vw_Asset_Movement]    Script Date: 02/28/2013 13:18:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [dbo].[Vw_Asset_Movement]
AS
--SELECT     Assets.ID_Asset, Assets.Name AS 'AssetName', Assets.Description, Assets.AssetNo, Assets.Date_Modified AS 'DateMoved', 
--                      Assets.Last_Modified_By, Employees.Name + ' ' + Employees.Surname AS 'EmployeeName', Locations.Name AS 'LocationName', 
--                      Locations.LocationNo, Assets.ID_Location, Assets.ID_AssetType, Asset_Types.Type, Assets.ID_AssetGroup, Asset_Groups.Name AS 'GroupName', 
--                      Assets.ID_AssetStatus, Asset_Status.Status, Assets.ReferenceNo, Assets.TagID 
--FROM         Assets INNER JOIN
--                      Employees ON Assets.Last_Modified_By = Employees.ID_Employee INNER JOIN
--                      Locations ON Assets.ID_Location = Locations.ID_Location INNER JOIN
--                      Asset_Types ON Assets.ID_AssetType = Asset_Types.ID_AssetType INNER JOIN
--                      Asset_Groups ON Assets.ID_AssetGroup = Asset_Groups.ID_AssetGroup INNER JOIN
--                      Asset_Status ON Assets.ID_AssetStatus = Asset_Status.ID_AssetStatus
--UNION
SELECT     dbo.Asset_Movement.ID_Asset, Assets_1.Name AS 'AssetName', Assets_1.Description, Assets_1.AssetNo, 
                      dbo.Asset_Movement.Date_Created AS 'DateMoved', dbo.Asset_Movement.Last_Modified_By, 
                      Employees_1.Name + ' ' + Employees_1.Surname AS 'EmployeeName', Locations_1.Name AS 'LocationName', Locations_1.LocationNo, 
                      dbo.Asset_Movement.ID_Location, Assets_1.ID_AssetType, Asset_Types_1.Type, Assets_1.ID_AssetGroup, 
                      Asset_Groups_1.Name AS 'GroupName',  Asset_Movement.ID_AssetStatus, Asset_Status.Status, Asset_Movement.ReferenceNo,Assets_1.TagID 
FROM         dbo.Asset_Movement INNER JOIN
                      dbo.Locations AS Locations_1 ON dbo.Asset_Movement.ID_Location = Locations_1.ID_Location INNER JOIN
                      dbo.Assets AS Assets_1 ON dbo.Asset_Movement.ID_Asset = Assets_1.ID_Asset INNER JOIN
                      dbo.Employees AS Employees_1 ON dbo.Asset_Movement.Last_Modified_By = Employees_1.ID_Employee INNER JOIN
                      dbo.Asset_Types AS Asset_Types_1 ON Assets_1.ID_AssetType = Asset_Types_1.ID_AssetType INNER JOIN
                      dbo.Asset_Groups AS Asset_Groups_1 ON Assets_1.ID_AssetGroup = Asset_Groups_1.ID_AssetGroup INNER JOIN
                      Asset_Status ON Asset_Movement.ID_AssetStatus = Asset_Status.ID_AssetStatus
