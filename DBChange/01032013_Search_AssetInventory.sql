USE [OnRamp]
GO
/****** Object:  StoredProcedure [dbo].[Search_AssetInventory]    Script Date: 03/01/2013 11:42:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		Vinay Bansal
-- Create date: 15-June-2009
-- Description:	To Search all Inventory
-- Modified By - Vijay
-- Modified On - 30/06/2011
-- =========================================================

ALTER PROCEDURE [dbo].[Search_AssetInventory] 
 @StartDate Datetime = NULL ,
 @EndDate Datetime = NULL ,
 @LocID bigint = 0
AS
Begin
	Set NoCount ON;
	
declare @str varchar(max)


set @str =	 'select a.Name as AssetName, l.Name as LocationName,ins.InventoryStatusName as [Status],ih.InventoryDate as Date, e.Name as [InventoryBy]
	from Inventory_History as ih
	join Assets as a on a.ID_Asset = ih.ID_Asset join Inventory_Status as ins on ins.ID_InventoryStatus = ih.InventoryStatus 
	join Locations as l on l.ID_Location = ih.InventoryLocation 
	join Employees as e on e.ID_Employee = ih.InventoryBy where 1=1 '

if @LocID > 0  
begin
set @str = @str + ' AND ih.InventoryLocation = ' + cast(@LocID as varchar(50))
end

if @StartDate is not NULL
begin
 set @str = @str + ' AND (dateadd(dd,0,datediff(dd,0,ih.InventoryDate)) >= dateadd(dd,0,datediff(dd,0,''' + convert(varchar(30), @StartDate)+''')) 
						and dateadd(dd,0,datediff(dd,0,ih.InventoryDate)) <= dateadd(dd,0,datediff(dd,0,'''+	convert(varchar(30) ,@EndDate) +''')))'
end

 

exec(@str)

END

