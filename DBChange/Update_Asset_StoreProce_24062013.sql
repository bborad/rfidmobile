USE [OnRamp]
GO
/****** Object:  StoredProcedure [dbo].[Update_Asset]    Script Date: 06/24/2013 11:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Modified		Deepanshu Jouhari
-- Modified By : Vinay Bansal ON 12-June- 2009	
-- Create date: 19th March 2008, 1 Jan 2009
-- Last Modified By : Vijay on 26 May 2010
-- Description:	To UPDATE a Asset
-- Modified On -- 20/06/2012 
-- =============================================

ALTER PROCEDURE [dbo].[Update_Asset] --'Tag','Deepesh' ,'123','Desc', '','12/12/2008',2,1,1,1,1,2,16
(
	@TagID varchar(50),
	@Name varchar(50),
	@AssetNo varchar(50),
	@Description varchar(1000),
	@ImageName varchar(500),
	@PurchaseDate datetime,
	@ID_TagType bigint,
	@ID_Location bigint,
	@ID_AssetType bigint,
	@ID_AssetStatus bigint,
	@ID_AssetGroup bigint,
	@ID_AssetComment bigint,
	@Last_Modified_By bigint,
	@ID_Asset bigint,
	@ID_Employee bigint,
	@ReferenceNo nvarchar(150)='',
	@Allocation nvarchar(100)='', -- Added By Vinay
	@LotNo	nvarchar(50)='',		-- Added By Vinay
	@ExpiryDate datetime = '1900-01-01',		-- Added By Vinay
	@ID_AssetMaster bigint = 0,  
	@PartCode nvarchar(10) = '0',  --Added By Deep
	@IsVernonUpdate bit = 0,
	@ID_Reason bigint =0
)
AS
	SET NOCOUNT OFF;
BEGIN

IF @ID_Asset <= 0
BEGIN
	EXEC [Add_Asset] @TagID,@Name,@AssetNo,@Description,@ImageName,@PurchaseDate,@ID_TagType,@ID_Location,@ID_AssetType,@ID_AssetStatus,@ID_AssetGroup,@ID_AssetComment,@Last_Modified_By,0,@ReferenceNo,@Allocation,@LotNo, @ExpiryDate,@ID_AssetMaster,@PartCode
 RETURN;
END

IF NOT EXISTS(Select 1 From Vw_Unique_TagID Where (TagID = @TagID and [Type]<>'Asset') or (TagID = @TagID and [Type]='Asset' and ID<>@ID_Asset)) 
	Begin 

		if(@ID_AssetMaster <=0)
		BEGIN
			Select @ID_AssetMaster = ID_AssetMaster From Assets where TagID = @TagID
		END
	
		if(@ID_AssetMaster <=0)
		BEGIN
			Select @ID_AssetMaster = ID_AssetMaster From Master_Assets where [Description] = @Name
		END

		if(@ID_AssetMaster <=0)
		BEGIN
			IF NOT Exists (Select 1 From Master_Assets where [Description] = 'Default')
			BEGIN
				INSERT INTO Master_Assets (SNo,[Description],Is_Active,IS_Deleted,CreatedBy, Date_Created)
				Values ('Default','Default',1,0,@Last_Modified_By, Getdate())
			END	
			Select @ID_AssetMaster = ID_AssetMaster From Master_Assets where [Description] = 'Default'
		END


		Declare @OldLocation bigint
		set @OldLocation = 0
		Select @OldLocation= ID_Location from  [Assets] where ID_Asset = @ID_Asset
		if (@OldLocation <> @ID_Location )
		Begin
				INSERT INTO [dbo].[Asset_Movement]
				   ([ID_Asset]
				   ,[ID_TagType]
				   ,[ID_Location]
				   ,[Date_Created]
				   ,[Date_Modified]
				   ,[Is_Deleted]
				   ,[Is_Active]
				   ,[Last_Modified_By]
				   ,[ID_Reader]
				   ,[ID_AssetStatus]
				   ,[ID_Employee]
				   ,[ReferenceNo]
				   ,[ID_LocationNew]
				   ,[IS_VernonRead]
				   ,[ID_Reason]
					)
					Select 
					[ID_Asset]
				   ,[ID_TagType]
				   ,[ID_Location]
				   ,getDate() 
				   ,[Date_Modified] 
				   ,[Is_Deleted]
				   ,[Is_Active]
				   ,@Last_Modified_By as Last_Modified_By
				   ,[ID_Reader]
				   ,[ID_AssetStatus]
				   ,@ID_Employee as ID_Employee
				   ,[ReferenceNo]	
				   ,@ID_Location as ID_LocationNew
				   ,@IsVernonUpdate as IS_VernonRead
				   ,@ID_Reason as ID_Reason	
					From [Assets] where [ID_Asset] = @ID_Asset

			Update Locations Set UsageCount = UsageCount +1 Where ID_Location = @ID_Location -- To Update Location Usage Count field ( 20/06/2012 )
		   
		End 

		IF (Len(@ImageName) != 0)
		 begin
			set dateformat dmy   
			UPDATE [Assets] SET [ID_Location] = @ID_Location , [TagID] = @TagID, [Name] = @Name, [AssetNo] = @AssetNo, [Description] = @Description, [ImageName] = @ImageName, [PurchaseDate] = @PurchaseDate, [ID_AssetType] = @ID_AssetType, [ID_AssetStatus] = @ID_AssetStatus, [ID_AssetGroup] = @ID_AssetGroup, [ID_AssetComment] = @ID_AssetComment, [Date_Modified] = getdate(), [Last_Modified_By] = @Last_Modified_By, [ID_Employee]=@ID_Employee, [ReferenceNo]=@ReferenceNo,LotNo=@LotNo,ExpiryDate=@ExpiryDate,Allocation=@Allocation,PartCode=@PartCode,ID_AssetMaster=@ID_AssetMaster WHERE (([ID_Asset] = @ID_Asset)); -- [ID_TagType]=@ID_TagType,
		 end
		ELSE
		 begin
			set dateformat dmy   
			UPDATE [Assets] SET [ID_Location] = @ID_Location, [TagID] = @TagID, [Name] = @Name, [AssetNo] = @AssetNo, [Description] = @Description, [PurchaseDate] = @PurchaseDate, [ID_AssetType] = @ID_AssetType, [ID_AssetStatus] = @ID_AssetStatus, [ID_AssetGroup] = @ID_AssetGroup, [ID_AssetComment] = @ID_AssetComment, [Date_Modified] = getdate(), [Last_Modified_By] = @Last_Modified_By, [ID_Employee]=@ID_Employee, [ReferenceNo]=@ReferenceNo ,LotNo=@LotNo,ExpiryDate=@ExpiryDate,Allocation=@Allocation,PartCode=@PartCode,ID_AssetMaster=@ID_AssetMaster WHERE (([ID_Asset] = @ID_Asset)); -- [ID_TagType]=@ID_TagType,
		 end
			
		SELECT ID_Asset, TagID, Name, AssetNo, Description, ImageName, PurchaseDate, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,ID_AssetMaster,PartCode FROM Assets WHERE (ID_Asset = @ID_Asset)
	End
else
	Begin
		RaisError(N'Duplicate TAG ID',18,1);
	end
END

