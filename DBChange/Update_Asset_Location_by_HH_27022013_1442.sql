USE [OnRamp]
GO
/****** Object:  StoredProcedure [dbo].[Update_Asset_Location_by_HH]    Script Date: 02/27/2013 16:50:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 28th May 2008, 30 July 2008, 1 Jan 2009
-- Description:	To UPDATE a Asset
-- Modified By - Vijay
-- Modified On - 01/07/2011
-- Modified On -- 20/06/2012 
-- =============================================

ALTER PROCEDURE [dbo].[Update_Asset_Location_by_HH] 
--'Tag','Deepesh' ,'123','Desc', '','12/12/2008',2,1,1,1,1,2,16
(
	@TagID varchar(MAX),
	@ID_Location bigint,
	@Last_Modified_By bigint,
	@ID_Reader bigint,
	@ReferenceNo nvarchar(150)='',
	@ID_Reason bigint=0
)
AS
	SET NOCOUNT OFF;
Declare @OldLocation bigint
set @OldLocation = 0

declare @sqlstr1 varchar(max);

--Select @OldLocation = isNULL(ID_Location,-1) from  [Assets] where TagID = @TAGID
--
--print 'Select Location';
--
----print @TAGID + cast(@OldLocation as varchar) + ' ' +  cast(@ID_Location as varchar); 
--if @OldLocation <> @ID_Location
Begin

Update [dbo].[Assets] set Date_Modified = getdate() where TagID in (@TagID)

	print 'Insert Into Asset Movement';
	SET @sqlstr1 =
			'INSERT INTO [Asset_Movement]
           ([ID_Asset]
           ,[ID_Location]
           ,[Date_Created]
           ,[Date_Modified]
           ,[Is_Deleted]
           ,[Is_Active]
           ,[Last_Modified_By]
           ,[ID_Reader]
           ,[ID_AssetStatus]
		   ,[ID_Employee]
		   ,[ID_TagType]
		   ,[ID_Alert]
			,[ReferenceNo]
			,[ID_LocationNew]
			,[IS_VernonRead]
			,[ID_Reason])
			Select 
			[ID_Asset]
			,[ID_Location]
			,getdate()
			,[Date_Modified]
			,[Is_Deleted]
			,[Is_Active]
			,' + Cast(@Last_Modified_By as varchar(5)) + ' as Last_Modified_By
			,[ID_Reader]
			,[ID_AssetStatus]
			,' + Cast(@Last_Modified_By as varchar(5)) + ' as ID_Employee
			,[ID_TagType]	
			,[ID_Alert]
			,[ReferenceNo]
			,'+ Cast(@ID_Location as varchar(5)) + ' as ID_LocationNew
			,0 as IS_VernonRead
			,'+ Cast(@ID_Reason as varchar(5)) + '  as ID_Reason	
			From [dbo].[Assets] 
			where ID_Location <> ' + Cast(@ID_Location as varchar(5)) + ' AND Is_Active = 1 AND Is_Deleted = 0 AND TAGID IN (' + @TagID + ')'
End 

--print @sqlstr1;

execute(@sqlstr1);

Declare @sqlstr varchar(max);

print 'Update Asset';

set dateformat dmy   
if(@ID_Reader <> 0)
	BEGIN		
		SET @sqlstr = '
			UPDATE [Assets]
			SET [ID_Location] = ' + Cast(@ID_Location as varchar(5)) + ',
			ID_Employee= ' + cast(@Last_Modified_By as varchar(5)) +',
			[Date_Modified] = getdate(),		
			ReferenceNo= ''' + @ReferenceNo + '''
			WHERE Is_Active = 1 AND Is_Deleted = 0 AND TagID IN (' + @TagID + ')'

	END
ELSE
	BEGIN	

		--,ReferenceNo= ''' + @ReferenceNo + '''
		
			SET @sqlstr = '
			UPDATE [Assets]
			SET [ID_Location] = ' + Cast(@ID_Location as varchar(5)) + ',
			ID_Employee= ' + cast(@Last_Modified_By as varchar(5)) +',
			[Date_Modified] = getdate()  	
			WHERE Is_Active = 1 AND Is_Deleted = 0 AND TagID IN (' + @TagID + ')'
	
	END
	
--print @sqlstr

execute(@sqlstr);

print 'Update Asset Completed';

Update Locations Set UsageCount = UsageCount +1 Where ID_Location = @ID_Location AND TagID <> '0000000000000000000000D'-- To Update Location Usage Count field ( 20/06/2012 )

