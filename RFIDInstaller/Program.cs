﻿using System;

using System.Collections.Generic;
using System.Text;
using System.IO;
using ClsLibBKLogs;
using HHDeviceInterface.Utility;
using OnRamp;

namespace RFIDInstaller
{
    class Program
    {
        //string path = "\\Program Files\\";
        static void Main(string[] args)
        {
            try
            {
                string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

                while (path.LastIndexOf("\\") > 1)
                {
                    path = path.Substring(0, path.LastIndexOf(Path.DirectorySeparatorChar));
                }
                
                path = path + "\\";

                string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\RFID\";

                // folderPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
                // folderPath = folderPath + "\\Data\\";

                if (!Directory.Exists(folderPath))
                    Directory.CreateDirectory(folderPath);
                //filePath = folderPath + @"\" + filename;

                string filePath = folderPath + UserPref.UserPrefFileName;

                File.Copy(path + UserPref.UserPrefFileName, filePath, true);

                UserPref PrefPath = UserPref.GetInstance();

                if (UserPref.IsFileFound)
                {
                    string installationPath = PrefPath.AppInstallationPath;

                    if (!(installationPath.LastIndexOf(@"\") == installationPath.Length - 1))
                    {
                        installationPath = installationPath + "\\";
                    }

                    if (!Directory.Exists(installationPath))
                    {
                        Directory.CreateDirectory(installationPath);
                    }

                    Logger.LogError("RFIDInstaller -- Copy Started.");

                    CopyFilesFromDirectory(PrefPath.BackUpDirPath + "RFID Application\\RFID", installationPath);

                    Logger.LogError("RFIDInstaller -- Copy Finished.");

                }
            }
            catch (Exception ex)
            {
                Logger.LogError("RFIDInstaller -- " + ex.Message);
            }

        }

        static void CopyFilesFromDirectory(string dirpath, string destdirpath)
        {
            try
            {
                Logger.LogError("Copy file(s) FROM (" + dirpath + ") TO (" + destdirpath + ") path.");

                DirectoryInfo dinfo = new DirectoryInfo(dirpath);

                if (dinfo == null)
                    return;

                FileSystemInfo[] arrFSinfo = dinfo.GetFileSystemInfos();

                if (arrFSinfo == null)
                    return;

                foreach (FileSystemInfo fs in arrFSinfo)
                {
                    try
                    {
                        if (fs.Attributes == FileAttributes.Directory)
                        {
                            Directory.CreateDirectory(destdirpath + "\\" + fs.Name);
                            CopyFilesFromDirectory(fs.FullName, destdirpath + "\\" + fs.Name);
                        }
                        else
                        {
                            File.Copy(fs.FullName, destdirpath + "\\" + fs.Name, true);
                            if (fs.Extension == ".lnk")
                            {
                                File.Copy(fs.FullName, Environment.GetFolderPath(Environment.SpecialFolder.Programs) + "\\" + fs.Name, true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("Error in copying -- " + ex.Message);
            }
        }
    }
}
