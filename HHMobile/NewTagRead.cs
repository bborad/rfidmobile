﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using HHDeviceInterface.RFIDSp;
using ClsReaderLib;

namespace OnRamp
{
    public partial class NewTagRead : Form
    {
        private Reader Rdr;
        public NewTagRead()
        {
            InitializeComponent();
            Rdr = ReaderFactory.GetReader();
        }
        bool isReading = false;
        private void btnstart_Click(object sender, EventArgs e)
        {
            if (isReading)
            {
                btnstart.Text = "Reading";
                Rdr.TagInventoryStop();
                isReading = false;

            }
            else
            {
                btnstart.Text = "Stop";
                timer1.Interval = 1030;
                timer1.Enabled = true;
                Rdr.TagInventoryStart(new Reader.EPCEvent(TagReadEvent));
                isReading = true;
            }
        }

        public void TagReadEvent(string epc, string rssi)
        {
            try
            {
                this.BeginInvoke(new Action<string>((string ep) =>
                {
                    if (!lsttags.Items.Contains(ep))
                    {
                        lsttags.Items.Insert(0, ep);
                        label1.Text = lsttags.Items.Count.ToString();
                    }
                }), epc);
            }
            catch { }
        }

        private void NewTagRead_Load(object sender, EventArgs e)
        {
           
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Rdr.TagInventoryStop();
            timer1.Enabled = false;
            isReading = false;
        }
    }
}