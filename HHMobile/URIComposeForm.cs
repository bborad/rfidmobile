using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class URIComposeForm : Form
    {
        public enum Protocol
        {
            HTTP,
            HTTPS,
        }

        // Attributes
        private Protocol _protocol;
        private ushort _portNum;
        private String _hostname;
        private String _absolutePath;
        private String _authenName;
        private String _authenPasswd;
        
        private Protocol defProtocol;

        // Update GUI
        private Protocol protocol
        {
            set
            {
                _protocol = value;
                switch (value)
                {
                    case Protocol.HTTP:
                        ProtoCmbBx.SelectedIndex = 0;
                        break;
                    case Protocol.HTTPS:
                        ProtoCmbBx.SelectedIndex = 1;
                        break;
                }
            }
        }

        private ushort portNum
        {
            set
            {
                _portNum = value;
                PortNumTxtBx.Text = value.ToString();
            }
        }

        private String hostname
        {
            set
            {
                _hostname = value == null? String.Empty : value;
                HostNmTxtBx.Text = _hostname;
            }
        }

        private String path
        {
            set
            {
                _absolutePath = value == null ? String.Empty : value;
                RemPathTxtBx.Text = _absolutePath;
            }
        }

        private String authenName
        {
            set
            {
                _authenName = value == null ? String.Empty : value;
                UsrNameTxtBx.Text = _authenName;
            }
        }

        private String authenPasswd
        {
            set
            {
                _authenPasswd = value == null ? String.Empty : value;
                PasswdTxtBx.Text = _authenPasswd;
            }
        }

        public URIComposeForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            //setup defaults
            {
                defProtocol = Protocol.HTTP;
            }
        }

        private bool DetermineProto(String uriScheme)
        {
            bool Supported = true;

            if (String.Equals(uriScheme, Uri.UriSchemeHttp))
            {
                protocol = Protocol.HTTP;
            }
            else if (String.Equals(uriScheme, Uri.UriSchemeHttps))
            {
                protocol = Protocol.HTTPS;
            }
            else
                Supported = false;

            return Supported;
        }

        public static bool ParseUriUserInfoStr (String uriUserInfo, out String name, out String passwd)
        {
            name = String.Empty;
            passwd = String.Empty;
            bool NameFound = true; // presume true

             // Assume that 'user' 'password' to be separated by ':'
            int CommaIdx = uriUserInfo.IndexOf(':');
            if (CommaIdx < 0) // no occurrence
            {
                if (String.IsNullOrEmpty(uriUserInfo) == false)
                {
                    name = uriUserInfo;
                }
                else
                {
                    name = String.Empty;
                    NameFound = false;
                }
                passwd = String.Empty; // ok like this?
            }
            else
            {
                name = uriUserInfo.Substring(0, CommaIdx);
                passwd = uriUserInfo.Substring(CommaIdx + 1);
            }
            return NameFound;
        }

        private void DetermineUserAuthen(String uriUserInfo)
        {
            String uname, passwd;
            URIComposeForm.ParseUriUserInfoStr(uriUserInfo, out uname, out passwd);
            authenName = uname;
            authenPasswd = passwd;
        }

        /// <summary>
        /// Readable (non-escape-encoded URI)
        /// </summary>
        public String  URIStr
        {
            set
            {
                // No matter the input string is Escape-Encoded or not (or mixed),
                // Uri constructor would escape-encode it
                // But we want to display Read-able version
                try
                {
                    Uri RemoteUri = new Uri(value);
                    // Protocol
                    DetermineProto(RemoteUri.Scheme);
                    portNum = (ushort)RemoteUri.Port;
                    hostname = RemoteUri.Host;
                    String EscapedEncodedPath = RemoteUri.PathAndQuery; // 'Escaped' by Uri constructor
                    path = Uri.UnescapeDataString(EscapedEncodedPath); // only unescape when it's escaped
                    DetermineUserAuthen(RemoteUri.UserInfo);
                }
                catch
                {
                    Program.ShowWarning("Could not breakdown into appropiate fields.\nPossible error in input URI string");
                    ResetInputFields();
                }
            }
            get
            {
                return BuildURI(); // Readable (non-escape-encoded URI)
            }
            
        }

        private void ResetInputFields()
        {
            AssignDefaultValues();   
        }

        private ushort DefaultPort(Protocol proto)
        {
            return (ushort)(proto == Protocol.HTTP? 80: 443);
        }

        private void AssignDefaultValues()
        {
            protocol = defProtocol;
            portNum = DefaultPort(_protocol);
            hostname = string.Empty;
            path = string.Empty;
            authenName = String.Empty;
            authenPasswd = String.Empty;
        }

        private void OnProtocolSelChanged(object sender, EventArgs e)
        {
            switch (ProtoCmbBx.SelectedIndex)
            {
                case 0:
                    portNum = DefaultPort(Protocol.HTTP);
                    break;
                case 1:
                    portNum = DefaultPort(Protocol.HTTPS);
                    break;
            }
        }

        // Returns a canonical (Readable, non-escape-uncoded) absolute URI
        private String BuildURI()
        {
            StringBuilder Sb = new StringBuilder();
            // protocol
            switch (ProtoCmbBx.SelectedIndex)
            {
                case 0:
                    Sb = Sb.Append(Uri.UriSchemeHttp);
                    break;
                case 1:
                    Sb = Sb.Append(Uri.UriSchemeHttps);
                    break;
            }
            // ://
            Sb = Sb.Append(Uri.SchemeDelimiter);
            // user-password
            if (UsrNameTxtBx.Text.Equals(String.Empty) == false)
            {
                Sb = Sb.Append(UsrNameTxtBx.Text + ":" + PasswdTxtBx.Text + "@");
            }
            // host
            Sb = Sb.Append(HostNmTxtBx.Text);
            // port
            if (String.IsNullOrEmpty(PortNumTxtBx.Text) == false)
            {
                Sb = Sb.Append(":" + PortNumTxtBx.Text);
            }
            // path
            if (RemPathTxtBx.Text.StartsWith("/"       ) == false)
            {
                Sb = Sb.Append("/");
            }
            Sb = Sb.Append(RemPathTxtBx.Text); // this part could be escaped, unescaped or both
            // borrow URI constructor to clean it up
            String ReadableURI = String.Empty;
            try
            {
                Uri ComposedURI = new Uri(Sb.ToString());
                ReadableURI = ComposedURI.ToString();
            }
            catch
            {
                ReadableURI = String.Empty;
            }

            return ReadableURI;
        }

        private void OnPortTxtBxKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }
    
    }
}