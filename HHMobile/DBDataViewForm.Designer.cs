namespace OnRamp
{
    partial class DBDataViewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBDataViewForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.DtGrd = new System.Windows.Forms.DataGrid();
            this.ChngsOnlyBttn = new System.Windows.Forms.Button();
            this.VwAllBttn = new System.Windows.Forms.Button();
            this.AccptBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DtGrd
            // 
            this.DtGrd.BackColor = System.Drawing.SystemColors.Info;
            this.DtGrd.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            resources.ApplyResources(this.DtGrd, "DtGrd");
            this.DtGrd.Name = "DtGrd";
            // 
            // ChngsOnlyBttn
            // 
            resources.ApplyResources(this.ChngsOnlyBttn, "ChngsOnlyBttn");
            this.ChngsOnlyBttn.Name = "ChngsOnlyBttn";
            this.ChngsOnlyBttn.Click += new System.EventHandler(this.OnChngsOnlyBttnClicked);
            // 
            // VwAllBttn
            // 
            resources.ApplyResources(this.VwAllBttn, "VwAllBttn");
            this.VwAllBttn.Name = "VwAllBttn";
            this.VwAllBttn.Click += new System.EventHandler(this.OnVwAllBttnClicked);
            // 
            // AccptBttn
            // 
            resources.ApplyResources(this.AccptBttn, "AccptBttn");
            this.AccptBttn.Name = "AccptBttn";
            this.AccptBttn.Click += new System.EventHandler(this.OnAccptBttnClicked);
            // 
            // DBDataViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.AccptBttn);
            this.Controls.Add(this.VwAllBttn);
            this.Controls.Add(this.ChngsOnlyBttn);
            this.Controls.Add(this.DtGrd);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBDataViewForm";
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.DBDataViewForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.DtGrd = new System.Windows.Forms.DataGrid();
            this.ChngsOnlyBttn = new System.Windows.Forms.Button();
            this.VwAllBttn = new System.Windows.Forms.Button();
            this.AccptBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DtGrd
            // 
            this.DtGrd.BackColor = System.Drawing.SystemColors.Info;
            this.DtGrd.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            resources.ApplyResources(this.DtGrd, "DtGrd");
            this.DtGrd.Name = "DtGrd";
            // 
            // ChngsOnlyBttn
            // 
            resources.ApplyResources(this.ChngsOnlyBttn, "ChngsOnlyBttn");
            this.ChngsOnlyBttn.Name = "ChngsOnlyBttn";
            this.ChngsOnlyBttn.Click += new System.EventHandler(this.OnChngsOnlyBttnClicked);
            // 
            // VwAllBttn
            // 
            resources.ApplyResources(this.VwAllBttn, "VwAllBttn");
            this.VwAllBttn.Name = "VwAllBttn";
            this.VwAllBttn.Click += new System.EventHandler(this.OnVwAllBttnClicked);
            // 
            // AccptBttn
            // 
            resources.ApplyResources(this.AccptBttn, "AccptBttn");
            this.AccptBttn.Name = "AccptBttn";
            this.AccptBttn.Click += new System.EventHandler(this.OnAccptBttnClicked);
            // 
            // DBDataViewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.AccptBttn);
            this.Controls.Add(this.VwAllBttn);
            this.Controls.Add(this.ChngsOnlyBttn);
            this.Controls.Add(this.DtGrd);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBDataViewForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGrid DtGrd;
        private System.Windows.Forms.Button ChngsOnlyBttn;
        private System.Windows.Forms.Button VwAllBttn;
        private System.Windows.Forms.Button AccptBttn;
    }
}