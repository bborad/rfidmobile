/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Asset Search functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

using System.Runtime.InteropServices;

namespace OnRamp
{
    public partial class frmSearchAssetNew : Form
    {
         

        private int searchOption;

        private DataTable dtAssetColumns;

        List<string> lstAssetColumns;

        TransientMsgDlg msgbox = null;
        public int SelectedLocationID, SelectedGroupID, SelectedSubGroupID;

        public frmSearchAssetNew()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void frmSearchAsset_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;


            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;

            }
           
            pnlSearchOptions.Visible = false;

            pnlSearch.Visible = true;
            pnlSearch.BringToFront();
           

            Cursor.Current = Cursors.WaitCursor;

            searchOption = UserPref.GetInstance().SearchOption;

            SetRadioButtons(searchOption);

            lnkGroups.Tag = "All Groups";
            lnkSubGroup.Tag = "All Sub Groups";
            lnkLocation.Tag = "All Locations";
           
           
#region"Set Location Combo"
        
            cboLoc.KeyUp += cboLoc_KeyUp;
            cboLoc.DropDownStyle = ComboBoxStyle.DropDown;
        

            //DataTable dtList = new DataTable();         

            //dtList.Columns.Add("ID_Location");
            //dtList.Columns.Add("Name");

            //DataRow dr; 

            //dr = dtList.NewRow();
            //dr["ID_Location"] = -2;
            //dr["Name"] = "--SELECT--";
            //dtList.Rows.Add(dr);
            //dtList.AcceptChanges(); 

            //cboLoc.ValueMember = "ID_Location";
            //cboLoc.DisplayMember = "Name";
            //cboLoc.DataSource = dtList;

          //  cboLoc.SelectedValue = -2;

            LocationItem item = new LocationItem("--SELECT--", "-2");
            cboLoc.Items.Add(item);
            cboLoc.SelectedItem = item;
          
#endregion

            #region"Set Parent Group"
            //Set Location Combo
            DataTable dtGList = new DataTable();
            //dtGList = AssetGroups.getGroups(0);

            dtGList.Columns.Add("ID_AssetGroup");
            dtGList.Columns.Add("Name");

            DataRow drG = dtGList.NewRow();
            drG["ID_AssetGroup"] = -2;
            drG["Name"] = "--SELECT--";
            dtGList.Rows.Add(drG);
            dtGList.AcceptChanges();

            //drG = dtGList.NewRow();
            //drG["ID_AssetGroup"] = 0;
            //drG["Name"] = "--ALL--";
            //dtGList.Rows.Add(drG);
            //dtGList.AcceptChanges();

            cboGroup.ValueMember = "ID_AssetGroup";
            cboGroup.DisplayMember = "Name";
            cboGroup.DataSource = dtGList;

            //cboGroup.SelectedValue = -2;
            cboGroup.SelectedValue = -2;

            cboGroup.KeyUp += cboGroup_KeyUp;
            cboGroup.DropDownStyle = ComboBoxStyle.DropDown;

            #endregion


            #region"Set SUB Group"
            //Set Location Combo
            DataTable dtSGList = new DataTable();
            dtSGList = AssetGroups.getGroups(-3);

            DataRow drSG = dtSGList.NewRow();
            drSG["ID_AssetGroup"] = -2;
            drSG["Name"] = "--SELECT--";
            dtSGList.Rows.Add(drSG);
            dtSGList.AcceptChanges();

            drSG = dtSGList.NewRow();
            drSG["ID_AssetGroup"] = 0;
            drSG["Name"] = "--ALL--";
            dtSGList.Rows.Add(drSG);
            dtSGList.AcceptChanges();

            cboSubGroup.ValueMember = "ID_AssetGroup";
            cboSubGroup.DisplayMember = "Name";
            cboSubGroup.DataSource = dtSGList;

            //cboSubGroup.SelectedValue = -2;
            cboSubGroup.SelectedValue = 0;
            #endregion

            CreateDataTable();

            cmbColumn1.DataSource = dtAssetColumns;
            cmbColumn1.DisplayMember = "ColumnDispName";
            cmbColumn1.ValueMember = "ColumnName";

            cmbColumn2.DataSource = dtAssetColumns.Copy();
            cmbColumn2.DisplayMember = "ColumnDispName";
            cmbColumn2.ValueMember = "ColumnName";

            cmbColumn3.DataSource = dtAssetColumns.Copy();
            cmbColumn3.DisplayMember = "ColumnDispName";
            cmbColumn3.ValueMember = "ColumnName";

            Cursor.Current = Cursors.Default; 
        }

        private void cboLoc_KeyUp(object sender, KeyEventArgs e)
        {
           
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                    case Keys.Right:
                    case Keys.Tab:
                    case Keys.Up:
                    case Keys.Delete:
                    case Keys.Down:
                    case Keys.ShiftKey:
                    case Keys.Shift:
                        return;
                }

                string typedSoFar;

                if (e.KeyCode == Keys.Back)
                {
                    typedSoFar = Convert.ToString(cboLoc.Tag);

                    if (cboLoc.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = cboLoc.Text.Trim();
                    }
                    else
                    {
                        return;
                    }

                    //if (typedSoFar.Length > 0 && typedSoFar.Length == cboLoc.Text.Length)
                    //{
                    //    typedSoFar = typedSoFar.Remove(typedSoFar.Length - 1, 1);
                    //}
                }
                else
                {
                    typedSoFar = Convert.ToString(cboLoc.Tag);
                    if (cboLoc.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = cboLoc.Text.Trim();
                    }
                   // typedSoFar = typedSoFar + Convert.ToChar(e.KeyValue);
                }

                cboLoc.Tag = typedSoFar;

                DataTable dtList = new DataTable();

                if (typedSoFar.Trim().Length >= 1)
                { 

                    bool setSelection = false;
                    int selValue = 0;

                    try
                    {
                        cboLoc.DataSource = null;

                        if (typedSoFar.ToLower() == "all")
                        {
                            dtList = Locations.getLocationList(cboLoc);
                        }
                        else
                        {
                            dtList = Locations.getLocationList(typedSoFar.Trim(),cboLoc);
                        }

                        //dtList = Locations.getLocationList(typedSoFar.Trim());

                        //if (dtList != null)
                        //{
                        //    DataRow dr;

                        //    //dr = dtList.NewRow();
                        //    //dr["ID_Location"] = 0;
                        //    //dr["Name"] = "--ALL--";
                        //    //dtList.Rows.Add(dr);
                        //    //dtList.AcceptChanges();

                        //    dr = dtList.NewRow();
                        //    dr["ID_Location"] = -2;
                        //    dr["Name"] = "--SELECT--";
                        //    dtList.Rows.Add(dr);
                        //    dtList.AcceptChanges();
                        //    //dtList.
                        //    if (dtList.Rows.Count == 1)
                        //    {
                        //        dr = dtList.NewRow();
                        //        if (typedSoFar.ToLower() == "all")
                        //        {
                        //            selValue = 0;

                        //        }
                        //        else
                        //        {

                        //            selValue = -3;

                        //        }
                        //        dr["ID_Location"] = selValue;

                        //        dr["Name"] = typedSoFar;
                        //        dtList.Rows.InsertAt(dr, 0);
                        //        dtList.AcceptChanges();

                        //        setSelection = true;

                        //    }
                        //    else if (typedSoFar.ToLower() == "all")
                        //    {
                        //        selValue = 0;
                        //        dr = dtList.NewRow();
                        //        dr["ID_Location"] = selValue;

                        //        dr["Name"] = typedSoFar;
                        //        dtList.Rows.InsertAt(dr, 0);
                        //        dtList.AcceptChanges();
                        //        setSelection = true;
                        //    }

                        //    cboLoc.ValueMember = "ID_Location";
                        //    cboLoc.DisplayMember = "Name";
                        //    cboLoc.DataSource = dtList;

                        //}

                        LocationItem item = new LocationItem("--SELECT--", "-2");
                        cboLoc.Items.Add(item);
                        if (cboLoc.Items.Count == 1)
                        {
                           
                            if (typedSoFar.ToLower() == "all")
                            {
                                selValue = 0;

                            }
                            else
                            {

                                selValue = -3;

                            }
                            item = new LocationItem(typedSoFar, selValue.ToString());

                            cboLoc.Items.Insert(0, item);

                            setSelection = true;

                        }
                        else if (typedSoFar.ToLower() == "all")
                        {
                            selValue = 0; 
                            item = new LocationItem(typedSoFar, selValue.ToString());

                            cboLoc.Items.Insert(0, item);
                            setSelection = true;
                        } 

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    //'Select the Appended Text

                    Utility.setDropDownHeight(cboLoc.Handle, 20);

                    Utility.setShowDropDown(cboLoc.Handle);

                    if (setSelection)
                    {
                        cboLoc.SelectedValue = selValue;
                    }

                    Utility.setSelectionStart((short)typedSoFar.Length, cboLoc.Handle); 

                }
                else
                {
                    //dtList.Columns.Add("ID_Location");
                    //dtList.Columns.Add("Name");

                    //DataRow dr = dtList.NewRow();
                    //dr["ID_Location"] = -2;
                    //dr["Name"] = "--SELECT--";
                    //dtList.Rows.Add(dr);
                    //dtList.AcceptChanges();

                    //cboLoc.ValueMember = "ID_Location";
                    //cboLoc.DisplayMember = "Name";
                    //cboLoc.DataSource = dtList;

                    //cboLoc.SelectedValue = -2;

                    LocationItem item = new LocationItem("--SELECT--", "-2");
                    cboLoc.Items.Add(item);
                    cboLoc.SelectedItem = item;

                }
            }
            catch
            {
            }

          
        }

        private void cboGroup_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                    case Keys.Right:
                    case Keys.Tab:
                    case Keys.Up:
                    case Keys.Delete:
                    case Keys.Down:
                    case Keys.ShiftKey:
                    case Keys.Shift:
                        return;
                }

                string typedSoFar;

                if (e.KeyCode == Keys.Back)
                {
                    typedSoFar = Convert.ToString(cboGroup.Tag);

                    if (cboGroup.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = cboGroup.Text.Trim();
                    }
                    else
                    {
                        return;
                    }

                    //if (typedSoFar.Length > 0 && typedSoFar.Length == cboGroup.Text.Length)
                    //{
                    //    typedSoFar = typedSoFar.Remove(typedSoFar.Length - 1, 1);
                    //}
                }
                else
                {
                    typedSoFar = Convert.ToString(cboGroup.Tag);
                    if (cboGroup.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = cboGroup.Text.Trim();
                    }
                    // typedSoFar = typedSoFar + Convert.ToChar(e.KeyValue);
                }

                cboGroup.Tag = typedSoFar;

                DataTable dtList = new DataTable();

                if (typedSoFar.Trim().Length >= 1)
                {
                    bool setSelection = false;
                    int selValue = 0;

                    try
                    {

                        if (typedSoFar.ToLower() == "all")
                        {
                            dtList = AssetGroups.getGroups(0);
                        }
                        else
                        {
                             
                            dtList = AssetGroups.getGroups(0,typedSoFar);
                        }

                        //dtList = Locations.getLocationList(typedSoFar.Trim());

                        if (dtList != null)
                        {
                            DataRow dr;

                            //dr = dtList.NewRow();
                            //dr["ID_Location"] = 0;
                            //dr["Name"] = "--ALL--";
                            //dtList.Rows.Add(dr);
                            //dtList.AcceptChanges();

                            dr = dtList.NewRow();
                            dr["ID_AssetGroup"] = -2;
                            dr["Name"] = "--SELECT--";
                            dtList.Rows.Add(dr);
                            dtList.AcceptChanges();

                            if (dtList.Rows.Count == 1)
                            {
                                dr = dtList.NewRow();
                                if (typedSoFar.ToLower() == "all")
                                {
                                    selValue = 0;

                                }
                                else
                                {

                                    selValue = -4;

                                }
                                dr["ID_AssetGroup"] = selValue;

                                dr["Name"] = typedSoFar;
                                dtList.Rows.InsertAt(dr, 0);
                                dtList.AcceptChanges();

                                setSelection = true;

                            }
                            else if (typedSoFar.ToLower() == "all")
                            {
                                selValue = 0;
                                dr = dtList.NewRow();
                                dr["ID_AssetGroup"] = selValue;

                                dr["Name"] = typedSoFar;
                                dtList.Rows.InsertAt(dr, 0);
                                dtList.AcceptChanges();
                                setSelection = true;
                            }

                            cboGroup.ValueMember = "ID_AssetGroup";
                            cboGroup.DisplayMember = "Name";
                            cboGroup.DataSource = dtList;

                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    //'Select the Appended Text

                    Utility.setDropDownHeight(cboGroup.Handle, 20);

                    Utility.setShowDropDown(cboGroup.Handle);  

                    if (setSelection)
                    {
                        cboGroup.SelectedValue = selValue;
                    }

                    Utility.setSelectionStart((short)typedSoFar.Length, cboGroup.Handle);

                }
                else
                {
                    dtList.Columns.Add("ID_AssetGroup");
                    dtList.Columns.Add("Name");

                    DataRow dr = dtList.NewRow();
                    dr["ID_AssetGroup"] = -2;
                    dr["Name"] = "--SELECT--";
                    dtList.Rows.Add(dr);
                    dtList.AcceptChanges();

                    cboGroup.ValueMember = "ID_AssetGroup";
                    cboGroup.DisplayMember = "Name";
                    cboGroup.DataSource = dtList;

                    cboGroup.SelectedValue = -2;

                }
            }
            catch
            {
            }
        }

    
        private void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboGroup.SelectedValue) != 0 && Convert.ToInt32(cboGroup.SelectedValue) != -2)
                {
                    #region"Set Child Group"
                    DataTable dtGList = new DataTable();

                    dtGList = AssetGroups.getGroups(Convert.ToInt32(cboGroup.SelectedValue));
                    DataRow drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = -2;
                    drG["Name"] = "--SELECT--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = 0;
                    drG["Name"] = "--ALL--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    cboSubGroup.ValueMember = "ID_AssetGroup";
                    cboSubGroup.DisplayMember = "Name";
                    cboSubGroup.DataSource = dtGList;

                    cboSubGroup.SelectedValue = 0;
                    #endregion
                }
                else
                {
                    DataTable dtGList = new DataTable();
                    dtGList = AssetGroups.getGroups(-2);
                    DataRow drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = -2;
                    drG["Name"] = "--SELECT--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = 0;
                    drG["Name"] = "--ALL--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    cboSubGroup.ValueMember = "ID_AssetGroup";
                    cboSubGroup.DisplayMember = "Name";
                    cboSubGroup.DataSource = dtGList;

                    cboSubGroup.SelectedValue = 0;
                }
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //Int32 LocationID;

                //if (cboLoc.SelectedItem != null)
                //{
                //    LocationItem li = (LocationItem)cboLoc.SelectedItem;
                //    LocationID = Convert.ToInt32(li.ID_Location);
                //    if (LocationID == -2)
                //    {
                //        MessageBox.Show("Please Select any Location.");
                //        return;
                //    }

                //}
                //else 
                //{
                //    LocationID = Convert.ToInt32(cboLoc.SelectedValue);
                //    if (LocationID == -2 || LocationID == 0)
                //    {
                //        MessageBox.Show("Please Select any Location.");
                //        return;
                //    } 
                //}

                //if (Convert.ToInt32(cboGroup.SelectedValue) == -2)
                //{
                //    MessageBox.Show("Please Select any Group.");
                //    return;
                //}

                //if (Convert.ToInt32(cboSubGroup.SelectedValue) == -2)
                //{
                //    MessageBox.Show("Please Select any Sub Group.");
                //    return;
                //}

                Dictionary<string, string> colValPair = new Dictionary<string, string>();

                if (txtValue1.Text.Trim().Length > 0)
                {
                    if (cmbColumn1.SelectedValue.ToString() != "0")
                    {
                        colValPair.Add(cmbColumn1.SelectedValue.ToString(), txtValue1.Text.Trim());
                    }

                }
                if (txtValue2.Text.Trim().Length > 0)
                {
                    if (cmbColumn2.SelectedValue.ToString() != "0")
                    {
                        if (colValPair.ContainsKey(cmbColumn2.SelectedValue.ToString()))
                        {
                            MessageBox.Show("Please select distinct fields.");
                            cmbColumn2.SelectedValue = "0";
                            cmbColumn2.Focus();
                            return;
                        }
                        else
                        {
                            colValPair.Add(cmbColumn2.SelectedValue.ToString(), txtValue2.Text.Trim());
                        }
                    }

                }
                if (txtValue3.Text.Trim().Length > 0)
                {
                    if (cmbColumn3.SelectedValue.ToString() != "0")
                    {
                        if (colValPair.ContainsKey(cmbColumn3.SelectedValue.ToString()))
                        {
                            MessageBox.Show("Please select distinct fields.");
                            cmbColumn3.SelectedValue = "0";
                            cmbColumn3.Focus();
                            return;
                        }
                        else
                        {
                            colValPair.Add(cmbColumn3.SelectedValue.ToString(), txtValue3.Text.Trim());
                        }
                    }

                }

            #region"Search Asset"
                DataTable dtGList = new DataTable();
                try
                {
                    Cursor.Current = Cursors.WaitCursor;                   
                    //dtGList = Assets.getSearchReasult(txtAssetNo.Text, txtAssetName.Text,Convert.ToInt32(cboLoc.SelectedValue), Convert.ToInt32(cboGroup.SelectedValue), Convert.ToInt32(cboSubGroup.SelectedValue),txtLotNo.Text);

                   // dtGList = Assets.getSearchReasult(txtAssetNo.Text, txtAssetName.Text, Convert.ToInt32(cboLoc.SelectedValue), Convert.ToInt32(cboGroup.SelectedValue), Convert.ToInt32(cboSubGroup.SelectedValue), txtLotNo.Text, searchOption);

                    dtGList = Assets.getSearchReasultNew(colValPair, SelectedLocationID, SelectedGroupID, SelectedSubGroupID, searchOption);
                                     
                   
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message); 
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            #endregion
            if (dtGList.Rows.Count != 0)
            {
                // Open new Tag Invtry window
                frmSearchlist TagList = new frmSearchlist();
                TagList.searchedData = dtGList;
                TagList.Show();  
                // disable form until this new form closed
                this.Enabled = false;
                TagList.Closed += new EventHandler(this.OnOperFrmClosed);
                //    // Open new Tag Invtry window
                //    TagRangingForm TagRangingFm = new TagRangingForm();
                //    TagRangingFm.SearchTagList = csvData.ToString();   
                //    TagRangingFm.Show();
                //    // disable form until this new form closed
                //    this.Enabled = false;
                //    TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else
            {
                MessageBox.Show("Search data not found.");  
            }
        }
        catch (ApplicationException ap)
        {
            Program.ShowError(ap.Message.ToString());
            Logger.LogError(ap.Message); 
        }
        catch (System.Web.Services.Protocols.SoapException ex)
        {
            if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                Program.ShowError("Request from innvalid IP address.");
            else
                Program.ShowError("Network Protocol Failure.");
            Logger.LogError(ex.Message); 
        }
        catch (System.Data.SqlServerCe.SqlCeException)
        {
            MessageBox.Show("Data File is not able to access.");
        }
        catch (System.Net.WebException)
        {
            MessageBox.Show("Web exception occured.");
        }
        catch (Exception ep)
        {

            MessageBox.Show(ep.Message.ToString());
        }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            DialogResult res;
            uploadCSV.Filter = "CSV|*.csv"; 
            res = uploadCSV.ShowDialog();
            String csvData = "";
            System.IO.StreamReader oRead;
            if (System.IO.File.Exists(uploadCSV.FileName))
            {
                oRead = System.IO.File.OpenText(uploadCSV.FileName);
                csvData = oRead.ReadToEnd();
                if (csvData.Trim().Length != 0)
                {
                    Cursor.Current = Cursors.WaitCursor; 

                    DataTable dtGList = new DataTable();
                    dtGList.Columns.Add("TagID");
                    dtGList.Columns.Add("Name");
                    dtGList.AcceptChanges();  
                    DataRow dr;
                    foreach(String tag in csvData.Split(",".ToCharArray()))
                    {
                        if (tag.Trim().Length != 0)
                        {
                            dr = dtGList.NewRow();
                            dr["TagID"] = tag.Trim();
                            dr["Name"] = tag.Trim();
                            dtGList.Rows.Add(dr);
                        }
                    }
                    dtGList.AcceptChanges();

                    Cursor.Current = Cursors.Default; 
//                    dtGList.LoadDataRow(, true);  

                    frmSearchlist TagList = new frmSearchlist();
                    TagList.searchedData = dtGList;
                    TagList.IsUploadFromFile = true;   
                    TagList.Show();
                    // disable form until this new form closed
                    this.Enabled = false;
                    TagList.Closed += new EventHandler(this.OnOperFrmClosed);


                    //// Open new Tag Invtry window
                    //TagRangingForm TagRangingFm = new TagRangingForm();
                    //TagRangingFm.SearchTagList = csvData;
                    //TagRangingFm.Show();
                    //// disable form until this new form closed
                    //this.Enabled = false;
                    //TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
                }
                else
                {
                    MessageBox.Show("Invalid File.");
                }
            }
            //System.IO.File.OpenText(uploadCSV.FileName);
//System.IO.File.te

            //uploadCSV.FileName   
        }

        private void rdbtnAnyWordDigit_Click(object sender, EventArgs e)
        {
            searchOption = 1;
            SetRadioButtons(searchOption);          
        }

        private void SetRadioButtons(int option)
        {
            switch (option)
            {
                case 1:
                    rdbtnAnyWordDigit.Checked = true;                    
                    rdbtnWholeString.Checked = false;
                    rdbtnExactString.Checked = false;
                    break;
                case 2:
                    rdbtnAnyWordDigit.Checked = false;                   
                    rdbtnWholeString.Checked = true;
                    rdbtnExactString.Checked = false;
                    break;
                case 3:
                    rdbtnAnyWordDigit.Checked = false;                   
                    rdbtnWholeString.Checked = false;
                    rdbtnExactString.Checked = true;
                    break;
                default :
                    rdbtnAnyWordDigit.Checked = true;
                    rdbtnWholeString.Checked = false;
                    rdbtnExactString.Checked = false;
                    break;
            }

           
        }

        private void rdbtnWholeString_Click(object sender, EventArgs e)
        {
            searchOption = 2;
            SetRadioButtons(searchOption);       
        }

        private void rdbtnExactString_Click(object sender, EventArgs e)
        {
            searchOption = 3;
            SetRadioButtons(searchOption);       
        }

        private void btnSearchOptions_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;

            pnlSearchOptions.Visible = true;            
            pnlSearchOptions.BringToFront();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            UserPref.GetInstance().SearchOption = searchOption;
            pnlSearchOptions.Visible = false;

            pnlSearch.Visible = true;
            pnlSearch.BringToFront();
        }

        void CreateDataTable()
        {
            dtAssetColumns = new DataTable();

            DataColumn[] dcc = new DataColumn[] {new DataColumn("ColumnDispName"),
                                                 new DataColumn("ColumnName")
            };

            dtAssetColumns.Columns.AddRange(dcc);

            dtAssetColumns.AcceptChanges();

            DataRow dr;

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "0";
            dr["ColumnDispName"] = "--Select--";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "TagID";
            dr["ColumnDispName"] = "Tag ID";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "Name";
            dr["ColumnDispName"] = "Item Name";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "AssetNo";
            dr["ColumnDispName"] = "Item No.";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "Description";
            dr["ColumnDispName"] = "Item Description";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "LotNo";
            dr["ColumnDispName"] = "Lot No";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "ReferenceNo";
            dr["ColumnDispName"] = "Reference No";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "Allocation";
            dr["ColumnDispName"] = "Allocation";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "PartCode";
            dr["ColumnDispName"] = "Part Code";
            dtAssetColumns.Rows.Add(dr);


        }

        private void frmSearchAssetNew_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:

                    Program.On_KeyUp(sender, e);

                    break;
                default:
                    break;
            }
        }

   

        private void btnSelectLoc_Click(object sender, EventArgs e)
        {
            string locName;
            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.Locations, "All Locations", 0, true);
            frmSel.ShowDialog();

            if (frmSel.objectID != -99999)
            { 
                locName = frmSel.objectName;
                SelectedLocationID = frmSel.objectID;
            }
            else if (SelectedLocationID == frmSel.defaultID)
            {
                locName = frmSel.defaultName;
            }
            else
            {
                return;
            }


            if (locName.Length > 15)
            {
                lnkLocation.Text = locName.Substring(0, 13);
                lnkLocation.Text = lnkLocation.Text + "..";
            }
            else
            {
                lnkLocation.Text = locName;
            }
            lnkLocation.Tag = locName;


        }

        private void btnGroups_Click(object sender, EventArgs e)
        {
            string selectedName;
            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.Groups, "All Groups", 0, true);
            frmSel.ShowDialog();

            if (frmSel.objectID != -99999)
            {
                selectedName = frmSel.objectName;
                SelectedGroupID = frmSel.objectID;
            }
            else if (SelectedGroupID == frmSel.defaultID)
            {
                selectedName = frmSel.defaultName;
            }
            else
            {
                return;
            } 

            // ttLocation.SetTooltipText(lblLocation, lblLocation.Text);

            if (selectedName.Length > 15)
            {
                lnkGroups.Text = selectedName.Substring(0, 13);
                lnkGroups.Text = lnkGroups.Text + "..";
            }
            else
            {
                lnkGroups.Text = selectedName;
            }
            lnkGroups.Tag = selectedName;
        }

        private void btnSubGroup_Click(object sender, EventArgs e)
        {
            string selectedName;
            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.SubGroups, "All Sub Groups", 0, true);
            if (SelectedGroupID > 0)
                frmSel.parentID = SelectedGroupID;
            else
                frmSel.parentID = -2;

            frmSel.ShowDialog();

           if (frmSel.objectID != -99999)
            {
                selectedName = frmSel.objectName;
                SelectedSubGroupID = frmSel.objectID;
            }
            else if (SelectedSubGroupID == frmSel.defaultID)
            {
                selectedName = frmSel.defaultName;
            }
            else
            {
                return;
            }  

            // ttLocation.SetTooltipText(lblLocation, lblLocation.Text);

            if (selectedName.Length > 15)
            {
                lnkSubGroup.Text = selectedName.Substring(0, 13);
                lnkSubGroup.Text = lnkSubGroup.Text + "..";
            }
            else
            {
                lnkSubGroup.Text = selectedName;
            }
            lnkSubGroup.Tag = selectedName;
        }


        private void lnkLocation_Click(object sender, EventArgs e)
        {
            //MessageBox.Show(lnkLocation.Tag.ToString());
            ShowMessage(lnkLocation.Tag.ToString());
        }



        private void lnkGroups_Click(object sender, EventArgs e)
        {
            ShowMessage(lnkGroups.Tag.ToString());
        }



        private void lnkSubGroup_Click(object sender, EventArgs e)
        {
            ShowMessage(lnkSubGroup.Tag.ToString());
        }

        // TransientMsgDlg msgbox = null;

        void ShowMessage(string message)
        {
            try
            {
                if (msgbox != null)
                {
                    msgbox.Close();
                    //msgbox = null;
                }
                else
                {

                }

                msgbox = new TransientMsgDlg(0);

                msgbox.Closed += new EventHandler(msgbox_Closed);

                int dispLength;

                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    dispLength = 23;
                    msgbox.Size = new Size(210, 110);
                }
                else
                {
                    dispLength = 30;
                    msgbox.Height = 110;
                }

                if (message.Trim().Length > dispLength)
                {
                    char[] splitParams = new char[1];
                    splitParams[0] = ' ';
                    string[] msgSplit = message.Split(splitParams);

                    int end = 0;

                    if (msgSplit != null && msgSplit.Length > 0)
                    {
                        StringBuilder strValue = new StringBuilder();

                        foreach (string value in msgSplit)
                        {
                            end = strValue.Length;
                            strValue.Append(value);

                            if (value.Length > dispLength)
                            {
                                try
                                {
                                    strValue.Insert(end + dispLength, " ");
                                    end = dispLength + 1;
                                    //strValue.Append(value.Insert(dispLength, " "));
                                    if (value.Substring(dispLength).Length > dispLength)
                                    {
                                        //strValue.Append(value.Insert(dispLength * 2, " "));
                                        strValue.Insert(end + dispLength, " ");
                                    }
                                }
                                catch
                                {
                                }
                            }
                            //else
                            //{
                            //    strValue.Append(value);
                            //}

                            strValue.Append(" ");
                        }

                        message = strValue.ToString().Trim();
                    }

                }

                msgbox.MsgHAlign = ContentAlignment.TopLeft;
                msgbox.SetTimeout(3);
                msgbox.TopMost = true;
                msgbox.AllowUserClose = true;
                msgbox.SetDpyMsg(message, "Full Text");

                msgbox.AutoScroll = false;

                msgbox.Show();
            }
            catch
            {
            }
        }

        void msgbox_Closed(object sender, EventArgs e)
        {
            try
            {
                msgbox = null;

            }
            catch
            {
            }
        }

        private void btnentertagid_Click(object sender, EventArgs e)
        {
            TagSearchForm frmsearch = new TagSearchForm();
            frmsearch.ShowDialog();
           
            frmsearch.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        
    }
}