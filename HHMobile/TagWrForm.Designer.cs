namespace OnRamp
{
    partial class TagWrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrForm));
            System.Windows.Forms.ColumnHeader EPCColHdr;
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem();
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem();
            this.ScanWrButton = new System.Windows.Forms.Button();
            this.DataDispPanel = new System.Windows.Forms.Panel();
            this.B3Label = new System.Windows.Forms.Label();
            this.B1Label = new System.Windows.Forms.Label();
            this.B0Label = new System.Windows.Forms.Label();
            this.btndelete = new System.Windows.Forms.Button();
            //this.EPCListV = new System.Windows.Forms.ListView();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.LngLstButton = new System.Windows.Forms.Button();
            this.ScanRdButton = new System.Windows.Forms.Button();
            this.Read1Button = new System.Windows.Forms.Button();
            this.Write1Button = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.ClrLstBttn = new System.Windows.Forms.Button();
            this.btnAddAsset = new System.Windows.Forms.Button();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.btnAddLocation = new System.Windows.Forms.Button();
            this.btnScnBarcode = new System.Windows.Forms.Button();
            this.epclistbox = new System.Windows.Forms.ListBox();
            this.tagWrBnkInput = new OnRamp.TagWrBnkInput();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.DataDispPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            resources.ApplyResources(RowNumHdr, "RowNumHdr");
            // 
            // EPCColHdr
            // 
            resources.ApplyResources(EPCColHdr, "EPCColHdr");
            // 
            // ScanWrButton
            // 
            resources.ApplyResources(this.ScanWrButton, "ScanWrButton");
            this.ScanWrButton.Name = "ScanWrButton";
            this.ScanWrButton.Click += new System.EventHandler(this.OnScanWrButtonClicked);
            // 
            // DataDispPanel
            // 
            this.DataDispPanel.BackColor = System.Drawing.Color.Transparent;
            this.DataDispPanel.Controls.Add(this.tagWrBnkInput);
            this.DataDispPanel.Controls.Add(this.B3Label);
            this.DataDispPanel.Controls.Add(this.B1Label);
            this.DataDispPanel.Controls.Add(this.B0Label);
            resources.ApplyResources(this.DataDispPanel, "DataDispPanel");
            this.DataDispPanel.Name = "DataDispPanel";
            // 
            // B3Label
            // 
            resources.ApplyResources(this.B3Label, "B3Label");
            this.B3Label.Name = "B3Label";
            // 
            // B1Label
            // 
            resources.ApplyResources(this.B1Label, "B1Label");
            this.B1Label.Name = "B1Label";
            // 
            // B0Label
            // 
            resources.ApplyResources(this.B0Label, "B0Label");
            this.B0Label.Name = "B0Label";
            // 
            // btndelete
            // 
            resources.ApplyResources(this.btndelete, "btndelete");
            this.btndelete.Name = "btndelete";
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // EPCListV
            // 
            //this.EPCListV.Columns.Add(RowNumHdr);
            //this.EPCListV.Columns.Add(EPCColHdr);
            //this.EPCListV.FullRowSelect = true;
            //this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            listViewItem1.Text = resources.GetString("EPCListV.Items");
            listViewItem2.Text = resources.GetString("EPCListV.Items1");
            listViewItem3.Text = resources.GetString("EPCListV.Items2");
            listViewItem4.Text = resources.GetString("EPCListV.Items3");
            listViewItem5.Text = resources.GetString("EPCListV.Items4");
            listViewItem6.Text = resources.GetString("EPCListV.Items5");
            listViewItem7.Text = resources.GetString("EPCListV.Items6");
            listViewItem8.Text = resources.GetString("EPCListV.Items7");
            //this.EPCListV.Items.Add(listViewItem1);
            //this.EPCListV.Items.Add(listViewItem2);
            //this.EPCListV.Items.Add(listViewItem3);
            //this.EPCListV.Items.Add(listViewItem4);
            //this.EPCListV.Items.Add(listViewItem5);
            //this.EPCListV.Items.Add(listViewItem6);
            //this.EPCListV.Items.Add(listViewItem7);
            //this.EPCListV.Items.Add(listViewItem8);
            //resources.ApplyResources(this.EPCListV, "EPCListV");
            //this.EPCListV.Name = "EPCListV";
            //this.EPCListV.View = System.Windows.Forms.View.Details;
            //this.EPCListV.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            this.TagCntLabel.ParentChanged += new System.EventHandler(this.TagCntLabel_ParentChanged);
            // 
            // LngLstButton
            // 
            resources.ApplyResources(this.LngLstButton, "LngLstButton");
            this.LngLstButton.Name = "LngLstButton";
            this.LngLstButton.Click += new System.EventHandler(this.OnLngLstButtonClicked);
            // 
            // ScanRdButton
            // 
            resources.ApplyResources(this.ScanRdButton, "ScanRdButton");
            this.ScanRdButton.Name = "ScanRdButton";
            this.ScanRdButton.Click += new System.EventHandler(this.OnScanRdButtonClicked);
            // 
            // Read1Button
            // 
            resources.ApplyResources(this.Read1Button, "Read1Button");
            this.Read1Button.Name = "Read1Button";
            //this.Read1Button.Click += new System.EventHandler(this.OnRead1ButtonClicked);
            // 
            // Write1Button
            // 
            resources.ApplyResources(this.Write1Button, "Write1Button");
            this.Write1Button.Name = "Write1Button";
            this.Write1Button.Enabled = false;
            //this.Write1Button.Click += new System.EventHandler(this.OnWrite1ButtonClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            // 
            // ClrLstBttn
            // 
            resources.ApplyResources(this.ClrLstBttn, "ClrLstBttn");
            this.ClrLstBttn.Name = "ClrLstBttn";
            this.ClrLstBttn.Click += new System.EventHandler(this.OnClrLstBttnClicked);
            // 
            // btnAddAsset
            // 
            resources.ApplyResources(this.btnAddAsset, "btnAddAsset");
            this.btnAddAsset.Name = "btnAddAsset";
            this.btnAddAsset.Click += new System.EventHandler(this.btnAddAsset_Click);
            // 
            // btnAddEmployee
            // 
            resources.ApplyResources(this.btnAddEmployee, "btnAddEmployee");
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // btnAddLocation
            // 
            resources.ApplyResources(this.btnAddLocation, "btnAddLocation");
            this.btnAddLocation.Name = "btnAddLocation";
            this.btnAddLocation.Click += new System.EventHandler(this.btnAddLocation_Click);
            // 
            // btnScnBarcode
            // 
            resources.ApplyResources(this.btnScnBarcode, "btnScnBarcode");
            this.btnScnBarcode.Name = "btnScnBarcode";
            this.btnScnBarcode.Click += new System.EventHandler(this.btnScnBarcode_Click);
            // 
            // epclistbox
            // 
            resources.ApplyResources(this.epclistbox, "epclistbox");
            this.epclistbox.Location = new System.Drawing.Point(1, 2);
            this.epclistbox.Name = "epclistbox";
            this.epclistbox.Size = new System.Drawing.Size(263, 114);
            this.epclistbox.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            // 
            // tagWrBnkInput
            // 
            resources.ApplyResources(this.tagWrBnkInput, "tagWrBnkInput");
            this.tagWrBnkInput.Name = "tagWrBnkInput";
            // 
            // TagWrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.epclistbox);
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnScnBarcode);
            this.Controls.Add(this.ClrLstBttn);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.Write1Button);
            this.Controls.Add(this.ScanRdButton);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.Read1Button);
            this.Controls.Add(this.LngLstButton);
            //this.Controls.Add(this.EPCListV);
            this.Controls.Add(this.DataDispPanel);
            this.Controls.Add(this.ScanWrButton);
            this.Controls.Add(this.btnAddLocation);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.btnAddAsset);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagWrForm";
            this.Load += new System.EventHandler(this.OnFrmLoad);
            this.Closed += new System.EventHandler(this.OnFrmClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFmClosing);
            this.DataDispPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagWrForm));
            System.Windows.Forms.ColumnHeader EPCColHdr;
            
            this.ScanWrButton = new System.Windows.Forms.Button();
            this.DataDispPanel = new System.Windows.Forms.Panel();
            this.tagWrBnkInput = new OnRamp.TagWrBnkInput();
            this.B3Label = new System.Windows.Forms.Label();
            this.B1Label = new System.Windows.Forms.Label();
            this.B0Label = new System.Windows.Forms.Label();
            this.epclistbox = new System.Windows.Forms.ListBox();
            //this.EPCListV = new System.Windows.Forms.ListView();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.LngLstButton = new System.Windows.Forms.Button();
            this.ScanRdButton = new System.Windows.Forms.Button();
            this.Read1Button = new System.Windows.Forms.Button();
            this.Write1Button = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.ClrLstBttn = new System.Windows.Forms.Button();
            this.btnAddAsset = new System.Windows.Forms.Button();
            this.btnAddEmployee = new System.Windows.Forms.Button();
            this.btnAddLocation = new System.Windows.Forms.Button();
            this.btnScnBarcode = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.DataDispPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            resources.ApplyResources(RowNumHdr, "RowNumHdr");
            // 
            // EPCColHdr
            // 
            resources.ApplyResources(EPCColHdr, "EPCColHdr");
            // 
            // ScanWrButton
            // 
            resources.ApplyResources(this.ScanWrButton, "ScanWrButton");
            this.ScanWrButton.Name = "ScanWrButton";
            this.ScanWrButton.Click += new System.EventHandler(this.OnScanWrButtonClicked);
            // 
            // DataDispPanel
            // 
            this.DataDispPanel.BackColor = System.Drawing.Color.Transparent;
            this.DataDispPanel.Controls.Add(this.tagWrBnkInput);
            this.DataDispPanel.Controls.Add(this.B3Label);
            this.DataDispPanel.Controls.Add(this.B1Label);
            this.DataDispPanel.Controls.Add(this.B0Label);
            resources.ApplyResources(this.DataDispPanel, "DataDispPanel");
            this.DataDispPanel.Name = "DataDispPanel";
            // 
            // tagWrBnkInput
            // 
            resources.ApplyResources(this.tagWrBnkInput, "tagWrBnkInput");
            this.tagWrBnkInput.Name = "tagWrBnkInput";
            // 
            // B3Label
            // 
            resources.ApplyResources(this.B3Label, "B3Label");
            this.B3Label.Name = "B3Label";
            this.B3Label.Font = new System.Drawing.Font(this.B3Label.Font.Name, 8.5f, System.Drawing.FontStyle.Regular);
            // 
            // B1Label
            // 
            resources.ApplyResources(this.B1Label, "B1Label");
            this.B1Label.Name = "B1Label";
            // 
            // B0Label
            // 
            resources.ApplyResources(this.B0Label, "B0Label");
            this.B0Label.Name = "B0Label";
            // 
            // EPCListV
            // 
            //this.EPCListV.Columns.Add(RowNumHdr);
            //this.EPCListV.Columns.Add(EPCColHdr);
            //this.EPCListV.FullRowSelect = true;
            //this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            //resources.ApplyResources(this.EPCListV, "EPCListV");
            //this.EPCListV.Name = "EPCListV";
            //this.EPCListV.View = System.Windows.Forms.View.Details;
            //this.EPCListV.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            //
            resources.ApplyResources(this.epclistbox, "epclistbox");
            this.epclistbox.Location = new System.Drawing.Point(1, 29);
            this.epclistbox.Name = "epclistbox";
            this.epclistbox.Size = new System.Drawing.Size(234, 125);
            this.epclistbox.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            this.TagCntLabel.ParentChanged += new System.EventHandler(this.TagCntLabel_ParentChanged);
            // 
            // LngLstButton
            // 
            resources.ApplyResources(this.LngLstButton, "LngLstButton");
            this.LngLstButton.Name = "LngLstButton";
            this.LngLstButton.Click += new System.EventHandler(this.OnLngLstButtonClicked);
            // 
            // ScanRdButton
            // 
            resources.ApplyResources(this.ScanRdButton, "ScanRdButton");
            this.ScanRdButton.Name = "ScanRdButton";
            this.ScanRdButton.Click += new System.EventHandler(this.OnScanRdButtonClicked);
            // 
            // Read1Button
            // 
            resources.ApplyResources(this.Read1Button, "Read1Button");
            this.Read1Button.Name = "Read1Button";
            //this.Read1Button.Click += new System.EventHandler(this.OnRead1ButtonClicked);
            // 
            // Write1Button
            // 
            resources.ApplyResources(this.Write1Button, "Write1Button");
            this.Write1Button.Name = "Write1Button";
            this.Write1Button.Enabled = false;
            //this.Write1Button.Click += new System.EventHandler(this.OnWrite1ButtonClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            // 
            // ClrLstBttn
            // 
            resources.ApplyResources(this.ClrLstBttn, "ClrLstBttn");
            this.ClrLstBttn.Name = "ClrLstBttn";
            this.ClrLstBttn.Click += new System.EventHandler(this.OnClrLstBttnClicked);
            // 
            // btnAddAsset
            // 
            resources.ApplyResources(this.btnAddAsset, "btnAddAsset");
            this.btnAddAsset.Name = "btnAddAsset";
            this.btnAddAsset.Click += new System.EventHandler(this.btnAddAsset_Click);
            // 
            // btnAddEmployee
            // 
            resources.ApplyResources(this.btnAddEmployee, "btnAddEmployee");
            this.btnAddEmployee.Name = "btnAddEmployee";
            this.btnAddEmployee.Click += new System.EventHandler(this.btnAddEmployee_Click);
            // 
            // btnAddLocation
            // 
            resources.ApplyResources(this.btnAddLocation, "btnAddLocation");
            this.btnAddLocation.Name = "btnAddLocation";
            this.btnAddLocation.Click += new System.EventHandler(this.btnAddLocation_Click);
            // 
            // btnScnBarcode
            // 
            resources.ApplyResources(this.btnScnBarcode, "btnScnBarcode");
            this.btnScnBarcode.Name = "btnScnBarcode";
            this.btnScnBarcode.Click += new System.EventHandler(this.btnScnBarcode_Click);
            // 
            // btndelete
            // 
            resources.ApplyResources(this.btndelete, "btndelete");
            this.btndelete.Name = "btndelete";
            this.btndelete.Location = new System.Drawing.Point(126, 211);
            this.btndelete.Size = new System.Drawing.Size(109, 27);
            this.btndelete.Text = "Delete";
            this.btndelete.Click += new System.EventHandler(this.btndelete_Click);
            // 
            // TagWrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btndelete);
            this.Controls.Add(this.btnScnBarcode);
            this.Controls.Add(this.ClrLstBttn);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.Write1Button);
            this.Controls.Add(this.ScanRdButton);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.Read1Button);
            this.Controls.Add(this.LngLstButton);
            //this.Controls.Add(this.EPCListV);
            this.Controls.Add(this.epclistbox);
            this.Controls.Add(this.DataDispPanel);
            this.Controls.Add(this.ScanWrButton);
            this.Controls.Add(this.btnAddLocation);
            this.Controls.Add(this.btnAddEmployee);
            this.Controls.Add(this.btnAddAsset);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagWrForm";
            this.Load += new System.EventHandler(this.OnFrmLoad);
            this.Closed += new System.EventHandler(this.OnFrmClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFmClosing);
            this.DataDispPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ScanWrButton;
        private System.Windows.Forms.Panel DataDispPanel;
        //private System.Windows.Forms.ListView EPCListV;
        private System.Windows.Forms.Label TagCntLabel;
        private System.Windows.Forms.Button LngLstButton;
        private System.Windows.Forms.Button ScanRdButton;
        private System.Windows.Forms.Button Read1Button;
        private System.Windows.Forms.Button Write1Button;
        private System.Windows.Forms.Label B3Label;
        private System.Windows.Forms.Label B1Label;
        private System.Windows.Forms.Label B0Label;
        private System.Windows.Forms.CheckBox PwdReqChkBx;
        private System.Windows.Forms.Button ClrLstBttn;
        private TagWrBnkInput tagWrBnkInput;
        private System.Windows.Forms.Button btnAddAsset;
        private System.Windows.Forms.Button btnAddEmployee;
        private System.Windows.Forms.Button btnAddLocation;
        private System.Windows.Forms.Button btnScnBarcode;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.ListBox epclistbox;
    }
}