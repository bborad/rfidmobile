﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4959
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnRamp.ResourceFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class TagScanSelectDlg {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal TagScanSelectDlg() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OnRamp.ResourceFiles.TagScanSelectDlg", typeof(TagScanSelectDlg).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static System.Drawing.Point CancelBttn_Location {
            get {
                object obj = ResourceManager.GetObject("CancelBttn.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size CancelBttn_Size {
            get {
                object obj = ResourceManager.GetObject("CancelBttn.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int CancelBttn_TabIndex {
            get {
                object obj = ResourceManager.GetObject("CancelBttn.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string CancelBttn_Text {
            get {
                return ResourceManager.GetString("CancelBttn.Text", resourceCulture);
            }
        }
        
        internal static bool columnHeader1_GenerateMember {
            get {
                object obj = ResourceManager.GetObject("columnHeader1.GenerateMember", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string columnHeader1_Text {
            get {
                return ResourceManager.GetString("columnHeader1.Text", resourceCulture);
            }
        }
        
        internal static int columnHeader1_Width {
            get {
                object obj = ResourceManager.GetObject("columnHeader1.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static bool columnHeader2_GenerateMember {
            get {
                object obj = ResourceManager.GetObject("columnHeader2.GenerateMember", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to PC.
        /// </summary>
        internal static string columnHeader2_Text {
            get {
                return ResourceManager.GetString("columnHeader2.Text", resourceCulture);
            }
        }
        
        internal static int columnHeader2_Width {
            get {
                object obj = ResourceManager.GetObject("columnHeader2.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static bool columnHeader3_GenerateMember {
            get {
                object obj = ResourceManager.GetObject("columnHeader3.GenerateMember", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to EPC.
        /// </summary>
        internal static string columnHeader3_Text {
            get {
                return ResourceManager.GetString("columnHeader3.Text", resourceCulture);
            }
        }
        
        internal static int columnHeader3_Width {
            get {
                object obj = ResourceManager.GetObject("columnHeader3.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point mainMenu1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("mainMenu1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Point OKBttn_Location {
            get {
                object obj = ResourceManager.GetObject("OKBttn.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size OKBttn_Size {
            get {
                object obj = ResourceManager.GetObject("OKBttn.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int OKBttn_TabIndex {
            get {
                object obj = ResourceManager.GetObject("OKBttn.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        internal static string OKBttn_Text {
            get {
                return ResourceManager.GetString("OKBttn.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point ScanBttn_Location {
            get {
                object obj = ResourceManager.GetObject("ScanBttn.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size ScanBttn_Size {
            get {
                object obj = ResourceManager.GetObject("ScanBttn.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int ScanBttn_TabIndex {
            get {
                object obj = ResourceManager.GetObject("ScanBttn.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scan.
        /// </summary>
        internal static string ScanBttn_Text {
            get {
                return ResourceManager.GetString("ScanBttn.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Font TagCntLbl_Font {
            get {
                object obj = ResourceManager.GetObject("TagCntLbl.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point TagCntLbl_Location {
            get {
                object obj = ResourceManager.GetObject("TagCntLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size TagCntLbl_Size {
            get {
                object obj = ResourceManager.GetObject("TagCntLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string TagCntLbl_Text {
            get {
                return ResourceManager.GetString("TagCntLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.ContentAlignment TagCntLbl_TextAlign {
            get {
                object obj = ResourceManager.GetObject("TagCntLbl.TextAlign", resourceCulture);
                return ((System.Drawing.ContentAlignment)(obj));
            }
        }
        
        internal static System.Drawing.Point TagLstV_Location {
            get {
                object obj = ResourceManager.GetObject("TagLstV.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size TagLstV_Size {
            get {
                object obj = ResourceManager.GetObject("TagLstV.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int TagLstV_TabIndex {
            get {
                object obj = ResourceManager.GetObject("TagLstV.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
    }
}
