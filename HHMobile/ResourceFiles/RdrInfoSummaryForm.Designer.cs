﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4959
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnRamp.ResourceFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class RdrInfoSummaryForm {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal RdrInfoSummaryForm() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OnRamp.ResourceFiles.RdrInfoSummaryForm", typeof(RdrInfoSummaryForm).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static System.Drawing.Font CountryLbl_Font {
            get {
                object obj = ResourceManager.GetObject("CountryLbl.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point CountryLbl_Location {
            get {
                object obj = ResourceManager.GetObject("CountryLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size CountryLbl_Size {
            get {
                object obj = ResourceManager.GetObject("CountryLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -.
        /// </summary>
        internal static string CountryLbl_Text {
            get {
                return ResourceManager.GetString("CountryLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Font DhcpLnkLbl_Font {
            get {
                object obj = ResourceManager.GetObject("DhcpLnkLbl.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point DhcpLnkLbl_Location {
            get {
                object obj = ResourceManager.GetObject("DhcpLnkLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size DhcpLnkLbl_Size {
            get {
                object obj = ResourceManager.GetObject("DhcpLnkLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int DhcpLnkLbl_TabIndex {
            get {
                object obj = ResourceManager.GetObject("DhcpLnkLbl.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (dhcp).
        /// </summary>
        internal static string DhcpLnkLbl_Text {
            get {
                return ResourceManager.GetString("DhcpLnkLbl.Text", resourceCulture);
            }
        }
        
        internal static bool DhcpLnkLbl_Visible {
            get {
                object obj = ResourceManager.GetObject("DhcpLnkLbl.Visible", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Point DNSLbl_Location {
            get {
                object obj = ResourceManager.GetObject("DNSLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size DNSLbl_Size {
            get {
                object obj = ResourceManager.GetObject("DNSLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.0.0.0.
        /// </summary>
        internal static string DNSLbl_Text {
            get {
                return ResourceManager.GetString("DNSLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point FrqBndLbl_Location {
            get {
                object obj = ResourceManager.GetObject("FrqBndLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size FrqBndLbl_Size {
            get {
                object obj = ResourceManager.GetObject("FrqBndLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -.
        /// </summary>
        internal static string FrqBndLbl_Text {
            get {
                return ResourceManager.GetString("FrqBndLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point GWLbl_Location {
            get {
                object obj = ResourceManager.GetObject("GWLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size GWLbl_Size {
            get {
                object obj = ResourceManager.GetObject("GWLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.0.0.0.
        /// </summary>
        internal static string GWLbl_Text {
            get {
                return ResourceManager.GetString("GWLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Font IPAddrLbl_Font {
            get {
                object obj = ResourceManager.GetObject("IPAddrLbl.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point IPAddrLbl_Location {
            get {
                object obj = ResourceManager.GetObject("IPAddrLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size IPAddrLbl_Size {
            get {
                object obj = ResourceManager.GetObject("IPAddrLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.0.0.0.
        /// </summary>
        internal static string IPAddrLbl_Text {
            get {
                return ResourceManager.GetString("IPAddrLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label1_Location {
            get {
                object obj = ResourceManager.GetObject("label1.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label1_Size {
            get {
                object obj = ResourceManager.GetObject("label1.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reader Name:.
        /// </summary>
        internal static string label1_Text {
            get {
                return ResourceManager.GetString("label1.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label2_Location {
            get {
                object obj = ResourceManager.GetObject("label2.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label2_Size {
            get {
                object obj = ResourceManager.GetObject("label2.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Frequency Band:.
        /// </summary>
        internal static string label2_Text {
            get {
                return ResourceManager.GetString("label2.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label3_Location {
            get {
                object obj = ResourceManager.GetObject("label3.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label3_Size {
            get {
                object obj = ResourceManager.GetObject("label3.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country:.
        /// </summary>
        internal static string label3_Text {
            get {
                return ResourceManager.GetString("label3.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label4_Location {
            get {
                object obj = ResourceManager.GetObject("label4.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label4_Size {
            get {
                object obj = ResourceManager.GetObject("label4.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Power(dBm):.
        /// </summary>
        internal static string label4_Text {
            get {
                return ResourceManager.GetString("label4.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label5_Location {
            get {
                object obj = ResourceManager.GetObject("label5.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label5_Size {
            get {
                object obj = ResourceManager.GetObject("label5.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Link Profile:.
        /// </summary>
        internal static string label5_Text {
            get {
                return ResourceManager.GetString("label5.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label6_Location {
            get {
                object obj = ResourceManager.GetObject("label6.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label6_Size {
            get {
                object obj = ResourceManager.GetObject("label6.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IP,HW Addr:.
        /// </summary>
        internal static string label6_Text {
            get {
                return ResourceManager.GetString("label6.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label7_Location {
            get {
                object obj = ResourceManager.GetObject("label7.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label7_Size {
            get {
                object obj = ResourceManager.GetObject("label7.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gateway:.
        /// </summary>
        internal static string label7_Text {
            get {
                return ResourceManager.GetString("label7.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Font label8_Font {
            get {
                object obj = ResourceManager.GetObject("label8.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point label8_Location {
            get {
                object obj = ResourceManager.GetObject("label8.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label8_Size {
            get {
                object obj = ResourceManager.GetObject("label8.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Network.
        /// </summary>
        internal static string label8_Text {
            get {
                return ResourceManager.GetString("label8.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label9_Location {
            get {
                object obj = ResourceManager.GetObject("label9.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label9_Size {
            get {
                object obj = ResourceManager.GetObject("label9.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DNS:.
        /// </summary>
        internal static string label9_Text {
            get {
                return ResourceManager.GetString("label9.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point LnkPrfLbl_Location {
            get {
                object obj = ResourceManager.GetObject("LnkPrfLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size LnkPrfLbl_Size {
            get {
                object obj = ResourceManager.GetObject("LnkPrfLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -.
        /// </summary>
        internal static string LnkPrfLbl_Text {
            get {
                return ResourceManager.GetString("LnkPrfLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point mainMenu1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("mainMenu1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Point PwrLbl_Location {
            get {
                object obj = ResourceManager.GetObject("PwrLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size PwrLbl_Size {
            get {
                object obj = ResourceManager.GetObject("PwrLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -.
        /// </summary>
        internal static string PwrLbl_Text {
            get {
                return ResourceManager.GetString("PwrLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point RdrNameLbl_Location {
            get {
                object obj = ResourceManager.GetObject("RdrNameLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size RdrNameLbl_Size {
            get {
                object obj = ResourceManager.GetObject("RdrNameLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to -.
        /// </summary>
        internal static string RdrNameLbl_Text {
            get {
                return ResourceManager.GetString("RdrNameLbl.Text", resourceCulture);
            }
        }
    }
}
