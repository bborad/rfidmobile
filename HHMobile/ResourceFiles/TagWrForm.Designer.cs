﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4959
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnRamp.ResourceFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class TagWrForm {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal TagWrForm() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OnRamp.ResourceFiles.TagWrForm", typeof(TagWrForm).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static System.Drawing.Point B0Label_Location {
            get {
                object obj = ResourceManager.GetObject("B0Label.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size B0Label_Size {
            get {
                object obj = ResourceManager.GetObject("B0Label.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bnk0.
        /// </summary>
        internal static string B0Label_Text {
            get {
                return ResourceManager.GetString("B0Label.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point B1Label_Location {
            get {
                object obj = ResourceManager.GetObject("B1Label.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size B1Label_Size {
            get {
                object obj = ResourceManager.GetObject("B1Label.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tag ID.
        /// </summary>
        internal static string B1Label_Text {
            get {
                return ResourceManager.GetString("B1Label.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point B3Label_Location {
            get {
                object obj = ResourceManager.GetObject("B3Label.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size B3Label_Size {
            get {
                object obj = ResourceManager.GetObject("B3Label.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _.
        /// </summary>
        internal static string B3Label_Text {
            get {
                return ResourceManager.GetString("B3Label.Text", resourceCulture);
            }
        }
        
        internal static bool btnAddAsset_Enabled {
            get {
                object obj = ResourceManager.GetObject("btnAddAsset.Enabled", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Point btnAddAsset_Location {
            get {
                object obj = ResourceManager.GetObject("btnAddAsset.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size btnAddAsset_Size {
            get {
                object obj = ResourceManager.GetObject("btnAddAsset.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int btnAddAsset_TabIndex {
            get {
                object obj = ResourceManager.GetObject("btnAddAsset.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Item.
        /// </summary>
        internal static string btnAddAsset_Text {
            get {
                return ResourceManager.GetString("btnAddAsset.Text", resourceCulture);
            }
        }
        
        internal static bool btnAddAsset_Visible {
            get {
                object obj = ResourceManager.GetObject("btnAddAsset.Visible", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static bool btnAddEmployee_Enabled {
            get {
                object obj = ResourceManager.GetObject("btnAddEmployee.Enabled", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Point btnAddEmployee_Location {
            get {
                object obj = ResourceManager.GetObject("btnAddEmployee.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size btnAddEmployee_Size {
            get {
                object obj = ResourceManager.GetObject("btnAddEmployee.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int btnAddEmployee_TabIndex {
            get {
                object obj = ResourceManager.GetObject("btnAddEmployee.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Employee.
        /// </summary>
        internal static string btnAddEmployee_Text {
            get {
                return ResourceManager.GetString("btnAddEmployee.Text", resourceCulture);
            }
        }
        
        internal static bool btnAddEmployee_Visible {
            get {
                object obj = ResourceManager.GetObject("btnAddEmployee.Visible", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static bool btnAddLocation_Enabled {
            get {
                object obj = ResourceManager.GetObject("btnAddLocation.Enabled", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Point btnAddLocation_Location {
            get {
                object obj = ResourceManager.GetObject("btnAddLocation.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size btnAddLocation_Size {
            get {
                object obj = ResourceManager.GetObject("btnAddLocation.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int btnAddLocation_TabIndex {
            get {
                object obj = ResourceManager.GetObject("btnAddLocation.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Location.
        /// </summary>
        internal static string btnAddLocation_Text {
            get {
                return ResourceManager.GetString("btnAddLocation.Text", resourceCulture);
            }
        }
        
        internal static bool btnAddLocation_Visible {
            get {
                object obj = ResourceManager.GetObject("btnAddLocation.Visible", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Point btnScnBarcode_Location {
            get {
                object obj = ResourceManager.GetObject("btnScnBarcode.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size btnScnBarcode_Size {
            get {
                object obj = ResourceManager.GetObject("btnScnBarcode.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int btnScnBarcode_TabIndex {
            get {
                object obj = ResourceManager.GetObject("btnScnBarcode.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scan BarCode.
        /// </summary>
        internal static string btnScnBarcode_Text {
            get {
                return ResourceManager.GetString("btnScnBarcode.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point ClrLstBttn_Location {
            get {
                object obj = ResourceManager.GetObject("ClrLstBttn.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size ClrLstBttn_Size {
            get {
                object obj = ResourceManager.GetObject("ClrLstBttn.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int ClrLstBttn_TabIndex {
            get {
                object obj = ResourceManager.GetObject("ClrLstBttn.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear.
        /// </summary>
        internal static string ClrLstBttn_Text {
            get {
                return ResourceManager.GetString("ClrLstBttn.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point DataDispPanel_Location {
            get {
                object obj = ResourceManager.GetObject("DataDispPanel.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size DataDispPanel_Size {
            get {
                object obj = ResourceManager.GetObject("DataDispPanel.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static bool EPCColHdr_GenerateMember {
            get {
                object obj = ResourceManager.GetObject("EPCColHdr.GenerateMember", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tag ID.
        /// </summary>
        internal static string EPCColHdr_Text {
            get {
                return ResourceManager.GetString("EPCColHdr.Text", resourceCulture);
            }
        }
        
        internal static int EPCColHdr_Width {
            get {
                object obj = ResourceManager.GetObject("EPCColHdr.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point EPCListV_Location {
            get {
                object obj = ResourceManager.GetObject("EPCListV.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size EPCListV_Size {
            get {
                object obj = ResourceManager.GetObject("EPCListV.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int EPCListV_TabIndex {
            get {
                object obj = ResourceManager.GetObject("EPCListV.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point LngLstButton_Location {
            get {
                object obj = ResourceManager.GetObject("LngLstButton.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size LngLstButton_Size {
            get {
                object obj = ResourceManager.GetObject("LngLstButton.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int LngLstButton_TabIndex {
            get {
                object obj = ResourceManager.GetObject("LngLstButton.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Long.
        /// </summary>
        internal static string LngLstButton_Text {
            get {
                return ResourceManager.GetString("LngLstButton.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point mainMenu1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("mainMenu1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Point PwdReqChkBx_Location {
            get {
                object obj = ResourceManager.GetObject("PwdReqChkBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size PwdReqChkBx_Size {
            get {
                object obj = ResourceManager.GetObject("PwdReqChkBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int PwdReqChkBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("PwdReqChkBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        internal static string PwdReqChkBx_Text {
            get {
                return ResourceManager.GetString("PwdReqChkBx.Text", resourceCulture);
            }
        }
        
        internal static bool PwdReqChkBx_Visible {
            get {
                object obj = ResourceManager.GetObject("PwdReqChkBx.Visible", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static bool Read1Button_Enabled {
            get {
                object obj = ResourceManager.GetObject("Read1Button.Enabled", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Point Read1Button_Location {
            get {
                object obj = ResourceManager.GetObject("Read1Button.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size Read1Button_Size {
            get {
                object obj = ResourceManager.GetObject("Read1Button.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int Read1Button_TabIndex {
            get {
                object obj = ResourceManager.GetObject("Read1Button.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Read Selected.
        /// </summary>
        internal static string Read1Button_Text {
            get {
                return ResourceManager.GetString("Read1Button.Text", resourceCulture);
            }
        }
        
        internal static bool Read1Button_Visible {
            get {
                object obj = ResourceManager.GetObject("Read1Button.Visible", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static bool RowNumHdr_GenerateMember {
            get {
                object obj = ResourceManager.GetObject("RowNumHdr.GenerateMember", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to .
        /// </summary>
        internal static string RowNumHdr_Text {
            get {
                return ResourceManager.GetString("RowNumHdr.Text", resourceCulture);
            }
        }
        
        internal static int RowNumHdr_Width {
            get {
                object obj = ResourceManager.GetObject("RowNumHdr.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point ScanRdButton_Location {
            get {
                object obj = ResourceManager.GetObject("ScanRdButton.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size ScanRdButton_Size {
            get {
                object obj = ResourceManager.GetObject("ScanRdButton.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int ScanRdButton_TabIndex {
            get {
                object obj = ResourceManager.GetObject("ScanRdButton.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scan First.
        /// </summary>
        internal static string ScanRdButton_Text {
            get {
                return ResourceManager.GetString("ScanRdButton.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point ScanWrButton_Location {
            get {
                object obj = ResourceManager.GetObject("ScanWrButton.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size ScanWrButton_Size {
            get {
                object obj = ResourceManager.GetObject("ScanWrButton.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int ScanWrButton_TabIndex {
            get {
                object obj = ResourceManager.GetObject("ScanWrButton.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Write Any.
        /// </summary>
        internal static string ScanWrButton_Text {
            get {
                return ResourceManager.GetString("ScanWrButton.Text", resourceCulture);
            }
        }
        
        internal static bool ScanWrButton_Visible {
            get {
                object obj = ResourceManager.GetObject("ScanWrButton.Visible", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Font TagCntLabel_Font {
            get {
                object obj = ResourceManager.GetObject("TagCntLabel.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point TagCntLabel_Location {
            get {
                object obj = ResourceManager.GetObject("TagCntLabel.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size TagCntLabel_Size {
            get {
                object obj = ResourceManager.GetObject("TagCntLabel.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0.
        /// </summary>
        internal static string TagCntLabel_Text {
            get {
                return ResourceManager.GetString("TagCntLabel.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.ContentAlignment TagCntLabel_TextAlign {
            get {
                object obj = ResourceManager.GetObject("TagCntLabel.TextAlign", resourceCulture);
                return ((System.Drawing.ContentAlignment)(obj));
            }
        }
        
        internal static System.Drawing.Point tagWrBnkInput_Location {
            get {
                object obj = ResourceManager.GetObject("tagWrBnkInput.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size tagWrBnkInput_Size {
            get {
                object obj = ResourceManager.GetObject("tagWrBnkInput.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int tagWrBnkInput_TabIndex {
            get {
                object obj = ResourceManager.GetObject("tagWrBnkInput.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static bool Write1Button_Enabled {
            get {
                object obj = ResourceManager.GetObject("Write1Button.Enabled", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Point Write1Button_Location {
            get {
                object obj = ResourceManager.GetObject("Write1Button.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size Write1Button_Size {
            get {
                object obj = ResourceManager.GetObject("Write1Button.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int Write1Button_TabIndex {
            get {
                object obj = ResourceManager.GetObject("Write1Button.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Write Selected.
        /// </summary>
        internal static string Write1Button_Text {
            get {
                return ResourceManager.GetString("Write1Button.Text", resourceCulture);
            }
        }
    }
}
