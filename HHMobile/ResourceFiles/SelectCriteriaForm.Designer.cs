﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4959
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnRamp.ResourceFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class SelectCriteriaForm {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal SelectCriteriaForm() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OnRamp.ResourceFiles.SelectCriteriaForm", typeof(SelectCriteriaForm).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static System.Drawing.Font CritNumLbl_Font {
            get {
                object obj = ResourceManager.GetObject("CritNumLbl.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point CritNumLbl_Location {
            get {
                object obj = ResourceManager.GetObject("CritNumLbl.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size CritNumLbl_Size {
            get {
                object obj = ResourceManager.GetObject("CritNumLbl.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Criteria .
        /// </summary>
        internal static string CritNumLbl_Text {
            get {
                return ResourceManager.GetString("CritNumLbl.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Font label19_Font {
            get {
                object obj = ResourceManager.GetObject("label19.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point label19_Location {
            get {
                object obj = ResourceManager.GetObject("label19.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label19_Size {
            get {
                object obj = ResourceManager.GetObject("label19.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to For Tag
        ///s with Bank.
        /// </summary>
        internal static string label19_Text {
            get {
                return ResourceManager.GetString("label19.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label21_Location {
            get {
                object obj = ResourceManager.GetObject("label21.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label21_Size {
            get {
                object obj = ResourceManager.GetObject("label21.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to content.
        /// </summary>
        internal static string label21_Text {
            get {
                return ResourceManager.GetString("label21.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Font label22_Font {
            get {
                object obj = ResourceManager.GetObject("label22.Font", resourceCulture);
                return ((System.Drawing.Font)(obj));
            }
        }
        
        internal static System.Drawing.Point label22_Location {
            get {
                object obj = ResourceManager.GetObject("label22.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label22_Size {
            get {
                object obj = ResourceManager.GetObject("label22.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to And for the rest:.
        /// </summary>
        internal static string label22_Text {
            get {
                return ResourceManager.GetString("label22.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point mainMenu1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("mainMenu1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set.
        /// </summary>
        internal static string MatchedActTypeCmbBx_Items {
            get {
                return ResourceManager.GetString("MatchedActTypeCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Toggle.
        /// </summary>
        internal static string MatchedActTypeCmbBx_Items1 {
            get {
                return ResourceManager.GetString("MatchedActTypeCmbBx.Items1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do Nothing.
        /// </summary>
        internal static string MatchedActTypeCmbBx_Items2 {
            get {
                return ResourceManager.GetString("MatchedActTypeCmbBx.Items2", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point MatchedActTypeCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("MatchedActTypeCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size MatchedActTypeCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("MatchedActTypeCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int MatchedActTypeCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("MatchedActTypeCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SL.
        /// </summary>
        internal static string MatchedFlagCmbBx_Items {
            get {
                return ResourceManager.GetString("MatchedFlagCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session0.
        /// </summary>
        internal static string MatchedFlagCmbBx_Items1 {
            get {
                return ResourceManager.GetString("MatchedFlagCmbBx.Items1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session1.
        /// </summary>
        internal static string MatchedFlagCmbBx_Items2 {
            get {
                return ResourceManager.GetString("MatchedFlagCmbBx.Items2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session2.
        /// </summary>
        internal static string MatchedFlagCmbBx_Items3 {
            get {
                return ResourceManager.GetString("MatchedFlagCmbBx.Items3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session3.
        /// </summary>
        internal static string MatchedFlagCmbBx_Items4 {
            get {
                return ResourceManager.GetString("MatchedFlagCmbBx.Items4", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point MatchedFlagCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("MatchedFlagCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size MatchedFlagCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("MatchedFlagCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int MatchedFlagCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("MatchedFlagCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to On.
        /// </summary>
        internal static string MatchedSLValCmbBx_Items {
            get {
                return ResourceManager.GetString("MatchedSLValCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Off.
        /// </summary>
        internal static string MatchedSLValCmbBx_Items1 {
            get {
                return ResourceManager.GetString("MatchedSLValCmbBx.Items1", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point MatchedSLValCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("MatchedSLValCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size MatchedSLValCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("MatchedSLValCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int MatchedSLValCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("MatchedSLValCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A.
        /// </summary>
        internal static string MatchedTgtCmbBx_Items {
            get {
                return ResourceManager.GetString("MatchedTgtCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to B.
        /// </summary>
        internal static string MatchedTgtCmbBx_Items1 {
            get {
                return ResourceManager.GetString("MatchedTgtCmbBx.Items1", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point MatchedTgtCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("MatchedTgtCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size MatchedTgtCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("MatchedTgtCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int MatchedTgtCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("MatchedTgtCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set.
        /// </summary>
        internal static string NonMatchedActTypeCmbBx_Items {
            get {
                return ResourceManager.GetString("NonMatchedActTypeCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Toggle.
        /// </summary>
        internal static string NonMatchedActTypeCmbBx_Items1 {
            get {
                return ResourceManager.GetString("NonMatchedActTypeCmbBx.Items1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do Nothing.
        /// </summary>
        internal static string NonMatchedActTypeCmbBx_Items2 {
            get {
                return ResourceManager.GetString("NonMatchedActTypeCmbBx.Items2", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point NonMatchedActTypeCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("NonMatchedActTypeCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size NonMatchedActTypeCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("NonMatchedActTypeCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int NonMatchedActTypeCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("NonMatchedActTypeCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SL.
        /// </summary>
        internal static string NonMatchedFlagCmbBx_Items {
            get {
                return ResourceManager.GetString("NonMatchedFlagCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session0.
        /// </summary>
        internal static string NonMatchedFlagCmbBx_Items1 {
            get {
                return ResourceManager.GetString("NonMatchedFlagCmbBx.Items1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session1.
        /// </summary>
        internal static string NonMatchedFlagCmbBx_Items2 {
            get {
                return ResourceManager.GetString("NonMatchedFlagCmbBx.Items2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session2.
        /// </summary>
        internal static string NonMatchedFlagCmbBx_Items3 {
            get {
                return ResourceManager.GetString("NonMatchedFlagCmbBx.Items3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session3.
        /// </summary>
        internal static string NonMatchedFlagCmbBx_Items4 {
            get {
                return ResourceManager.GetString("NonMatchedFlagCmbBx.Items4", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point NonMatchedFlagCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("NonMatchedFlagCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size NonMatchedFlagCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("NonMatchedFlagCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int NonMatchedFlagCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("NonMatchedFlagCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to On.
        /// </summary>
        internal static string NonMatchedSLValCmbBx_Items {
            get {
                return ResourceManager.GetString("NonMatchedSLValCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Off.
        /// </summary>
        internal static string NonMatchedSLValCmbBx_Items1 {
            get {
                return ResourceManager.GetString("NonMatchedSLValCmbBx.Items1", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point NonMatchedSLValCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("NonMatchedSLValCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size NonMatchedSLValCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("NonMatchedSLValCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int NonMatchedSLValCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("NonMatchedSLValCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A.
        /// </summary>
        internal static string NonMatchedTgtCmbBx_Items {
            get {
                return ResourceManager.GetString("NonMatchedTgtCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to B.
        /// </summary>
        internal static string NonMatchedTgtCmbBx_Items1 {
            get {
                return ResourceManager.GetString("NonMatchedTgtCmbBx.Items1", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point NonMatchedTgtCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("NonMatchedTgtCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size NonMatchedTgtCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("NonMatchedTgtCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int NonMatchedTgtCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("NonMatchedTgtCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point RstLnk_Location {
            get {
                object obj = ResourceManager.GetObject("RstLnk.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size RstLnk_Size {
            get {
                object obj = ResourceManager.GetObject("RstLnk.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int RstLnk_TabIndex {
            get {
                object obj = ResourceManager.GetObject("RstLnk.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to (Reset).
        /// </summary>
        internal static string RstLnk_Text {
            get {
                return ResourceManager.GetString("RstLnk.Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 1.EPC.
        /// </summary>
        internal static string SelCritBankCmbBx_Items {
            get {
                return ResourceManager.GetString("SelCritBankCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2.TID.
        /// </summary>
        internal static string SelCritBankCmbBx_Items1 {
            get {
                return ResourceManager.GetString("SelCritBankCmbBx.Items1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 3.User.
        /// </summary>
        internal static string SelCritBankCmbBx_Items2 {
            get {
                return ResourceManager.GetString("SelCritBankCmbBx.Items2", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point SelCritBankCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("SelCritBankCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size SelCritBankCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("SelCritBankCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int SelCritBankCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("SelCritBankCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Begins with.
        /// </summary>
        internal static string SelCritMaskPosCmbBx_Items {
            get {
                return ResourceManager.GetString("SelCritMaskPosCmbBx.Items", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ends with.
        /// </summary>
        internal static string SelCritMaskPosCmbBx_Items1 {
            get {
                return ResourceManager.GetString("SelCritMaskPosCmbBx.Items1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to of Any.
        /// </summary>
        internal static string SelCritMaskPosCmbBx_Items2 {
            get {
                return ResourceManager.GetString("SelCritMaskPosCmbBx.Items2", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point SelCritMaskPosCmbBx_Location {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskPosCmbBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size SelCritMaskPosCmbBx_Size {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskPosCmbBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int SelCritMaskPosCmbBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskPosCmbBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point SelCritMaskTxtBx_Location {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskTxtBx.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static int SelCritMaskTxtBx_MaxLength {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskTxtBx.MaxLength", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Size SelCritMaskTxtBx_Size {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskTxtBx.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int SelCritMaskTxtBx_TabIndex {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskTxtBx.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static bool SelCritMaskTxtBx_WordWrap {
            get {
                object obj = ResourceManager.GetObject("SelCritMaskTxtBx.WordWrap", resourceCulture);
                return ((bool)(obj));
            }
        }
    }
}
