namespace OnRamp
{
    partial class TagAuthenForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagAuthenForm));
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.OpnFileBttn = new System.Windows.Forms.Button();
            this.AuthFileLbl = new System.Windows.Forms.Label();
            this.BCChkBx = new System.Windows.Forms.CheckBox();
            this.RfidChkBx = new System.Windows.Forms.CheckBox();
            this.StartBttn = new System.Windows.Forms.Button();
            this.ItemsCntLbl = new System.Windows.Forms.Label();
            this.AuthRfidCntLbl = new System.Windows.Forms.Label();
            this.TotRfidCntLbl = new System.Windows.Forms.Label();
            this.AuthBCCntLbl = new System.Windows.Forms.Label();
            this.TotBCCntLbl = new System.Windows.Forms.Label();
            this.VwLstBttn = new System.Windows.Forms.Button();
            this.ClrBttn = new System.Windows.Forms.Button();
            this.VwResBttn = new System.Windows.Forms.Button();
            this.ExtraRfidCntLbl = new System.Windows.Forms.Label();
            this.ExtraBCCntLbl = new System.Windows.Forms.Label();
            this.tagAuthenFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // OpnFileBttn
            // 
            resources.ApplyResources(this.OpnFileBttn, "OpnFileBttn");
            this.OpnFileBttn.Name = "OpnFileBttn";
            this.OpnFileBttn.Click += new System.EventHandler(this.OnFileOpnClicked);
            // 
            // AuthFileLbl
            // 
            resources.ApplyResources(this.AuthFileLbl, "AuthFileLbl");
            this.AuthFileLbl.Name = "AuthFileLbl";
            // 
            // BCChkBx
            // 
            this.BCChkBx.Checked = true;
            this.BCChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.BCChkBx, "BCChkBx");
            this.BCChkBx.Name = "BCChkBx";
            this.BCChkBx.CheckStateChanged += new System.EventHandler(this.OnBCChkChanged);
            // 
            // RfidChkBx
            // 
            this.RfidChkBx.Checked = true;
            this.RfidChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.RfidChkBx, "RfidChkBx");
            this.RfidChkBx.Name = "RfidChkBx";
            this.RfidChkBx.CheckStateChanged += new System.EventHandler(this.OnRFIDChkChanged);
            // 
            // StartBttn
            // 
            resources.ApplyResources(this.StartBttn, "StartBttn");
            this.StartBttn.Name = "StartBttn";
            this.StartBttn.Click += new System.EventHandler(this.OnStartBttnClicked);
            // 
            // ItemsCntLbl
            // 
            resources.ApplyResources(this.ItemsCntLbl, "ItemsCntLbl");
            this.ItemsCntLbl.Name = "ItemsCntLbl";
            // 
            // AuthRfidCntLbl
            // 
            resources.ApplyResources(this.AuthRfidCntLbl, "AuthRfidCntLbl");
            this.AuthRfidCntLbl.Name = "AuthRfidCntLbl";
            // 
            // TotRfidCntLbl
            // 
            resources.ApplyResources(this.TotRfidCntLbl, "TotRfidCntLbl");
            this.TotRfidCntLbl.Name = "TotRfidCntLbl";
            // 
            // AuthBCCntLbl
            // 
            resources.ApplyResources(this.AuthBCCntLbl, "AuthBCCntLbl");
            this.AuthBCCntLbl.Name = "AuthBCCntLbl";
            // 
            // TotBCCntLbl
            // 
            resources.ApplyResources(this.TotBCCntLbl, "TotBCCntLbl");
            this.TotBCCntLbl.Name = "TotBCCntLbl";
            // 
            // VwLstBttn
            // 
            resources.ApplyResources(this.VwLstBttn, "VwLstBttn");
            this.VwLstBttn.Name = "VwLstBttn";
            this.VwLstBttn.Click += new System.EventHandler(this.OnVwBttnClicked);
            // 
            // ClrBttn
            // 
            resources.ApplyResources(this.ClrBttn, "ClrBttn");
            this.ClrBttn.Name = "ClrBttn";
            this.ClrBttn.Click += new System.EventHandler(this.OnClrBttnClicked);
            // 
            // VwResBttn
            // 
            resources.ApplyResources(this.VwResBttn, "VwResBttn");
            this.VwResBttn.Name = "VwResBttn";
            this.VwResBttn.Click += new System.EventHandler(this.OnVwBttnClicked);
            // 
            // label2
            // 
            resources.ApplyResources(label2, "label2");
            label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(label3, "label3");
            label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(label4, "label4");
            label4.Name = "label4";
            // 
            // ExtraRfidCntLbl
            // 
            resources.ApplyResources(this.ExtraRfidCntLbl, "ExtraRfidCntLbl");
            this.ExtraRfidCntLbl.Name = "ExtraRfidCntLbl";
            // 
            // ExtraBCCntLbl
            // 
            resources.ApplyResources(this.ExtraBCCntLbl, "ExtraBCCntLbl");
            this.ExtraBCCntLbl.Name = "ExtraBCCntLbl";
            // 
            // tagAuthenFormBindingSource
            // 
            this.tagAuthenFormBindingSource.DataSource = typeof(OnRamp.TagAuthenForm);
            // 
            // TagAuthenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ExtraBCCntLbl);
            this.Controls.Add(this.ExtraRfidCntLbl);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(this.VwResBttn);
            this.Controls.Add(this.ClrBttn);
            this.Controls.Add(this.VwLstBttn);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TotBCCntLbl);
            this.Controls.Add(this.AuthBCCntLbl);
            this.Controls.Add(this.TotRfidCntLbl);
            this.Controls.Add(this.AuthRfidCntLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ItemsCntLbl);
            this.Controls.Add(this.StartBttn);
            this.Controls.Add(this.RfidChkBx);
            this.Controls.Add(this.BCChkBx);
            this.Controls.Add(this.AuthFileLbl);
            this.Controls.Add(this.OpnFileBttn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagAuthenForm";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagAuthenForm));
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.OpnFileBttn = new System.Windows.Forms.Button();
            this.AuthFileLbl = new System.Windows.Forms.Label();
            this.BCChkBx = new System.Windows.Forms.CheckBox();
            this.RfidChkBx = new System.Windows.Forms.CheckBox();
            this.StartBttn = new System.Windows.Forms.Button();
            this.ItemsCntLbl = new System.Windows.Forms.Label();
            this.AuthRfidCntLbl = new System.Windows.Forms.Label();
            this.TotRfidCntLbl = new System.Windows.Forms.Label();
            this.AuthBCCntLbl = new System.Windows.Forms.Label();
            this.TotBCCntLbl = new System.Windows.Forms.Label();
            this.VwLstBttn = new System.Windows.Forms.Button();
            this.ClrBttn = new System.Windows.Forms.Button();
            this.VwResBttn = new System.Windows.Forms.Button();
            this.ExtraRfidCntLbl = new System.Windows.Forms.Label();
            this.ExtraBCCntLbl = new System.Windows.Forms.Label();
            this.tagAuthenFormBindingSource = new System.Windows.Forms.BindingSource(this.components);
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // OpnFileBttn
            // 
            resources.ApplyResources(this.OpnFileBttn, "OpnFileBttn");
            this.OpnFileBttn.Name = "OpnFileBttn";
            this.OpnFileBttn.Click += new System.EventHandler(this.OnFileOpnClicked);
            // 
            // AuthFileLbl
            // 
            resources.ApplyResources(this.AuthFileLbl, "AuthFileLbl");
            this.AuthFileLbl.Name = "AuthFileLbl";
            // 
            // BCChkBx
            // 
            this.BCChkBx.Checked = true;
            this.BCChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.BCChkBx, "BCChkBx");
            this.BCChkBx.Name = "BCChkBx";
            this.BCChkBx.CheckStateChanged += new System.EventHandler(this.OnBCChkChanged);
            // 
            // RfidChkBx
            // 
            this.RfidChkBx.Checked = true;
            this.RfidChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.RfidChkBx, "RfidChkBx");
            this.RfidChkBx.Name = "RfidChkBx";
            this.RfidChkBx.CheckStateChanged += new System.EventHandler(this.OnRFIDChkChanged);
            // 
            // StartBttn
            // 
            resources.ApplyResources(this.StartBttn, "StartBttn");
            this.StartBttn.Name = "StartBttn";
            this.StartBttn.Click += new System.EventHandler(this.OnStartBttnClicked);
            // 
            // ItemsCntLbl
            // 
            resources.ApplyResources(this.ItemsCntLbl, "ItemsCntLbl");
            this.ItemsCntLbl.Name = "ItemsCntLbl";
            // 
            // AuthRfidCntLbl
            // 
            resources.ApplyResources(this.AuthRfidCntLbl, "AuthRfidCntLbl");
            this.AuthRfidCntLbl.Name = "AuthRfidCntLbl";
            // 
            // TotRfidCntLbl
            // 
            resources.ApplyResources(this.TotRfidCntLbl, "TotRfidCntLbl");
            this.TotRfidCntLbl.Name = "TotRfidCntLbl";
            // 
            // AuthBCCntLbl
            // 
            resources.ApplyResources(this.AuthBCCntLbl, "AuthBCCntLbl");
            this.AuthBCCntLbl.Name = "AuthBCCntLbl";
            // 
            // TotBCCntLbl
            // 
            resources.ApplyResources(this.TotBCCntLbl, "TotBCCntLbl");
            this.TotBCCntLbl.Name = "TotBCCntLbl";
            // 
            // VwLstBttn
            // 
            resources.ApplyResources(this.VwLstBttn, "VwLstBttn");
            this.VwLstBttn.Name = "VwLstBttn";
            this.VwLstBttn.Click += new System.EventHandler(this.OnVwBttnClicked);
            // 
            // ClrBttn
            // 
            resources.ApplyResources(this.ClrBttn, "ClrBttn");
            this.ClrBttn.Name = "ClrBttn";
            this.ClrBttn.Click += new System.EventHandler(this.OnClrBttnClicked);
            // 
            // VwResBttn
            // 
            resources.ApplyResources(this.VwResBttn, "VwResBttn");
            this.VwResBttn.Name = "VwResBttn";
            this.VwResBttn.Click += new System.EventHandler(this.OnVwBttnClicked);
            // 
            // label2
            // 
            resources.ApplyResources(label2, "label2");
            label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(label3, "label3");
            label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(label4, "label4");
            label4.Name = "label4";
            // 
            // ExtraRfidCntLbl
            // 
            resources.ApplyResources(this.ExtraRfidCntLbl, "ExtraRfidCntLbl");
            this.ExtraRfidCntLbl.Name = "ExtraRfidCntLbl";
            // 
            // ExtraBCCntLbl
            // 
            resources.ApplyResources(this.ExtraBCCntLbl, "ExtraBCCntLbl");
            this.ExtraBCCntLbl.Name = "ExtraBCCntLbl";
            // 
            // tagAuthenFormBindingSource
            // 
            this.tagAuthenFormBindingSource.DataSource = typeof(OnRamp.TagAuthenForm);
            // 
            // TagAuthenForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ExtraBCCntLbl);
            this.Controls.Add(this.ExtraRfidCntLbl);
            this.Controls.Add(label4);
            this.Controls.Add(label3);
            this.Controls.Add(label2);
            this.Controls.Add(this.VwResBttn);
            this.Controls.Add(this.ClrBttn);
            this.Controls.Add(this.VwLstBttn);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TotBCCntLbl);
            this.Controls.Add(this.AuthBCCntLbl);
            this.Controls.Add(this.TotRfidCntLbl);
            this.Controls.Add(this.AuthRfidCntLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ItemsCntLbl);
            this.Controls.Add(this.StartBttn);
            this.Controls.Add(this.RfidChkBx);
            this.Controls.Add(this.BCChkBx);
            this.Controls.Add(this.AuthFileLbl);
            this.Controls.Add(this.OpnFileBttn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagAuthenForm";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OpnFileBttn;
        private System.Windows.Forms.Label AuthFileLbl;
        private System.Windows.Forms.BindingSource tagAuthenFormBindingSource;
        private System.Windows.Forms.CheckBox BCChkBx;
        private System.Windows.Forms.CheckBox RfidChkBx;
        private System.Windows.Forms.Button StartBttn;
        private System.Windows.Forms.Label ItemsCntLbl;
        private System.Windows.Forms.Label AuthRfidCntLbl;
        private System.Windows.Forms.Label TotRfidCntLbl;
        private System.Windows.Forms.Label AuthBCCntLbl;
        private System.Windows.Forms.Label TotBCCntLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button VwLstBttn;
        private System.Windows.Forms.Button ClrBttn;
        private System.Windows.Forms.Button VwResBttn;
        private System.Windows.Forms.Label ExtraRfidCntLbl;
        private System.Windows.Forms.Label ExtraBCCntLbl;
    }
}