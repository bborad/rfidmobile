namespace OnRamp
{
    partial class TagRdForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));
            this.LngLstButton = new System.Windows.Forms.Button();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.ScanButton = new System.Windows.Forms.Button();
            this.Read1Button = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtLoc = new System.Windows.Forms.TextBox();
            this.txtAsset = new System.Windows.Forms.TextBox();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnSavePage = new System.Windows.Forms.Button();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.colLocation = new System.Windows.Forms.ColumnHeader();
            this.colLotNo = new System.Windows.Forms.ColumnHeader();
            this.colTagID = new System.Windows.Forms.ColumnHeader();
            this.btnScnBarcd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSaveAll = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.lnkLocName = new System.Windows.Forms.LinkLabel();
            this.btnSelLoc = new System.Windows.Forms.Button();
            this.epclistbox = new System.Windows.Forms.ListBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LngLstButton
            // 
            resources.ApplyResources(this.LngLstButton, "LngLstButton");
            this.LngLstButton.Name = "LngLstButton";
            this.LngLstButton.Click += new System.EventHandler(this.OnLngLstButtonClicked);
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            // 
            // ScanButton
            // 
            resources.ApplyResources(this.ScanButton, "ScanButton");
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Click += new System.EventHandler(this.OnScanButtonClicked);
            // 
            // Read1Button
            // 
            resources.ApplyResources(this.Read1Button, "Read1Button");
            this.Read1Button.Name = "Read1Button";
            //this.Read1Button.Click += new System.EventHandler(this.OnRead1ButtonClicked);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.txtLoc);
            this.panel1.Controls.Add(this.txtAsset);
            this.panel1.Controls.Add(this.txtTag);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtLoc
            // 
            resources.ApplyResources(this.txtLoc, "txtLoc");
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.ReadOnly = true;
            this.txtLoc.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtAsset
            // 
            resources.ApplyResources(this.txtAsset, "txtAsset");
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.ReadOnly = true;
            this.txtAsset.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtTag
            // 
            resources.ApplyResources(this.txtTag, "txtTag");
            this.txtTag.Name = "txtTag";
            this.txtTag.ReadOnly = true;
            this.txtTag.TextChanged += new System.EventHandler(this.txtLoc_TextChanged);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSavePage
            // 
            resources.ApplyResources(this.btnSavePage, "btnSavePage");
            this.btnSavePage.Name = "btnSavePage";
            this.btnSavePage.Click += new System.EventHandler(this.btnSavePage_Click);
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.colLocation);
            this.lstAsset.Columns.Add(this.colLotNo);
            this.lstAsset.Columns.Add(this.colTagID);
            resources.ApplyResources(this.lstAsset, "lstAsset");
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // AssetName
            // 
            resources.ApplyResources(this.AssetName, "AssetName");
            // 
            // colLocation
            // 
            resources.ApplyResources(this.colLocation, "colLocation");
            // 
            // colLotNo
            // 
            resources.ApplyResources(this.colLotNo, "colLotNo");
            // 
            // colTagID
            // 
            resources.ApplyResources(this.colTagID, "colTagID");
            // 
            // btnScnBarcd
            // 
            resources.ApplyResources(this.btnScnBarcd, "btnScnBarcd");
            this.btnScnBarcd.Name = "btnScnBarcd";
            this.btnScnBarcd.Click += new System.EventHandler(this.btnScnBarcd_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSaveAll
            // 
            resources.ApplyResources(this.btnSaveAll, "btnSaveAll");
            this.btnSaveAll.Name = "btnSaveAll";
            this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.lblPage, "lblPage");
            this.lblPage.Name = "lblPage";
            // 
            // cmbPage
            // 
            resources.ApplyResources(this.cmbPage, "cmbPage");
            this.cmbPage.Name = "cmbPage";
            // 
            // lnkLocName
            // 
            resources.ApplyResources(this.lnkLocName, "lnkLocName");
            this.lnkLocName.ForeColor = System.Drawing.Color.Black;
            this.lnkLocName.Name = "lnkLocName";
            this.lnkLocName.Click += new System.EventHandler(this.lnkLocName_Click);
            // 
            // btnSelLoc
            // 
            resources.ApplyResources(this.btnSelLoc, "btnSelLoc");
            this.btnSelLoc.Name = "btnSelLoc";
            this.btnSelLoc.Click += new System.EventHandler(this.btnSelLoc_Click);
            // 
            // epclistbox
            // 
            resources.ApplyResources(this.epclistbox, "epclistbox");
            this.epclistbox.Name = "epclistbox";
            // 
            // TagRdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.epclistbox);
            this.Controls.Add(this.btnSelLoc);
            this.Controls.Add(this.lnkLocName);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSavePage);
            this.Controls.Add(this.btnSaveAll);
            this.Controls.Add(this.cboLoc);
            this.Controls.Add(this.ScanButton);
            this.Controls.Add(this.btnScnBarcd);
            this.Controls.Add(this.Read1Button);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.LngLstButton);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagRdForm";
            this.Load += new System.EventHandler(this.OnTagRdFormLoad);
            this.Closed += new System.EventHandler(this.OnTagRdFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnTagRdFormClosing);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagRdForm));
             
            this.LngLstButton = new System.Windows.Forms.Button();
            this.TagCntLabel = new System.Windows.Forms.Label();
            
            this.epclistbox = new System.Windows.Forms.ListBox();
            //this.EPCColHdr = new System.Windows.Forms.ColumnHeader();
            //this.B0ChkBx = new System.Windows.Forms.CheckBox();
            //this.B1ChkBx = new System.Windows.Forms.CheckBox();
            //this.B2ChkBx = new System.Windows.Forms.CheckBox();
            //this.B3ChkBx = new System.Windows.Forms.CheckBox();
            this.ScanButton = new System.Windows.Forms.Button();
            //this.B0_1TxtBx = new System.Windows.Forms.TextBox();
            //this.B1_1TxtBx = new System.Windows.Forms.TextBox();
            //this.B2_1TxtBx = new System.Windows.Forms.TextBox();
            //this.B3TxtBx = new System.Windows.Forms.TextBox();
            //this.DataDispPanel = new System.Windows.Forms.Panel();
            //this.B0_2TxtBx = new System.Windows.Forms.TextBox();
            //this.B2_3TxtBx = new System.Windows.Forms.TextBox();
            //this.B2_2TxtBx = new System.Windows.Forms.TextBox();
            //this.B1_2TxtBx = new System.Windows.Forms.TextBox();
            this.Read1Button = new System.Windows.Forms.Button();
            //this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtLoc = new System.Windows.Forms.TextBox();
            this.txtAsset = new System.Windows.Forms.TextBox();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnSavePage = new System.Windows.Forms.Button();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.colLocation = new System.Windows.Forms.ColumnHeader();
            this.colLotNo = new System.Windows.Forms.ColumnHeader();
            this.colTagID = new System.Windows.Forms.ColumnHeader();
            this.btnScnBarcd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSaveAll = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.lnkLocName = new System.Windows.Forms.LinkLabel();
            this.btnSelLoc = new System.Windows.Forms.Button();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            //this.DataDispPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            resources.ApplyResources(RowNumHdr, "RowNumHdr");
            // 
            // LngLstButton
            // 
            resources.ApplyResources(this.LngLstButton, "LngLstButton");
            this.LngLstButton.Name = "LngLstButton";
            this.LngLstButton.Click += new System.EventHandler(this.OnLngLstButtonClicked);
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
           
            // 
            // EPCListV
            // 
            //this.EPCListV.Columns.Add(RowNumHdr);
            //this.EPCListV.Columns.Add("EPC",50,System.Windows.Forms.HorizontalAlignment.Left);
            //this.EPCListV.FullRowSelect = true;
            //this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            //resources.ApplyResources(this.EPCListV, "EPCListV");
            //this.EPCListV.Name = "EPCListV";
            //this.EPCListV.View = System.Windows.Forms.View.Details;
            //this.EPCListV.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            // 
            // EPCColHdr
            // 
            //resources.ApplyResources(this.EPCColHdr, "EPCColHdr");
            // 
            // B0ChkBx
            // 
            //resources.ApplyResources(this.B0ChkBx, "B0ChkBx");
            //this.B0ChkBx.Name = "B0ChkBx";
            //this.B0ChkBx.CheckStateChanged += new System.EventHandler(this.OnB0ChkBxStateChanged);
            // 
            // B1ChkBx
            // 
            //this.B1ChkBx.Checked = true;
            //this.B1ChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            //resources.ApplyResources(this.B1ChkBx, "B1ChkBx");
            //this.B1ChkBx.Name = "B1ChkBx";
            // 
            // B2ChkBx
            // 
            //resources.ApplyResources(this.B2ChkBx, "B2ChkBx");
            //this.B2ChkBx.Name = "B2ChkBx";
            //this.B2ChkBx.CheckStateChanged += new System.EventHandler(this.OnB2ChkBxStateChanged);
            // 
            // B3ChkBx
            // 
            //resources.ApplyResources(this.B3ChkBx, "B3ChkBx");
            //this.B3ChkBx.Name = "B3ChkBx";
            //this.B3ChkBx.CheckStateChanged += new System.EventHandler(this.OnB3ChkBxStateChanged);
            // 
            // ScanButton
            // 
            resources.ApplyResources(this.ScanButton, "ScanButton");
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Click += new System.EventHandler(this.OnScanButtonClicked);
            // 
            // B0_1TxtBx
            // 
            //resources.ApplyResources(this.B0_1TxtBx, "B0_1TxtBx");
            //this.B0_1TxtBx.Name = "B0_1TxtBx";
            //this.B0_1TxtBx.ReadOnly = true;
            // 
            // B1_1TxtBx
            // 
            //resources.ApplyResources(this.B1_1TxtBx, "B1_1TxtBx");
            //this.B1_1TxtBx.Name = "B1_1TxtBx";
            //this.B1_1TxtBx.ReadOnly = true;
            // 
            // B2_1TxtBx
            // 
            //resources.ApplyResources(this.B2_1TxtBx, "B2_1TxtBx");
            //this.B2_1TxtBx.Name = "B2_1TxtBx";
            //this.B2_1TxtBx.ReadOnly = true;
            // 
            // B3TxtBx
            // 
            //resources.ApplyResources(this.B3TxtBx, "B3TxtBx");
            //this.B3TxtBx.Name = "B3TxtBx";
            //this.B3TxtBx.ReadOnly = true;
            // 
            // DataDispPanel
            // 
            //this.DataDispPanel.BackColor = System.Drawing.Color.Transparent;
            //this.DataDispPanel.Controls.Add(this.B0_2TxtBx);
            //this.DataDispPanel.Controls.Add(this.B2_3TxtBx);
            //this.DataDispPanel.Controls.Add(this.B2_2TxtBx);
            //this.DataDispPanel.Controls.Add(this.B1_2TxtBx);
            //this.DataDispPanel.Controls.Add(this.B0ChkBx);
            //this.DataDispPanel.Controls.Add(this.B3TxtBx);
            //this.DataDispPanel.Controls.Add(this.B1ChkBx);
            //this.DataDispPanel.Controls.Add(this.B2_1TxtBx);
            //this.DataDispPanel.Controls.Add(this.B1_1TxtBx);
            //this.DataDispPanel.Controls.Add(this.B0_1TxtBx);
            //this.DataDispPanel.Controls.Add(this.B2ChkBx);
            //this.DataDispPanel.Controls.Add(this.B3ChkBx);
            //resources.ApplyResources(this.DataDispPanel, "DataDispPanel");
            //this.DataDispPanel.Name = "DataDispPanel";
            // 
            // B0_2TxtBx
            // 
            //resources.ApplyResources(this.B0_2TxtBx, "B0_2TxtBx");
            //this.B0_2TxtBx.Name = "B0_2TxtBx";
            //this.B0_2TxtBx.ReadOnly = true;
            // 
            // B2_3TxtBx
            // 
            //resources.ApplyResources(this.B2_3TxtBx, "B2_3TxtBx");
            //this.B2_3TxtBx.Name = "B2_3TxtBx";
            //this.B2_3TxtBx.ReadOnly = true;
            // 
            // B2_2TxtBx
            // 
            //resources.ApplyResources(this.B2_2TxtBx, "B2_2TxtBx");
            //this.B2_2TxtBx.Name = "B2_2TxtBx";
            //this.B2_2TxtBx.ReadOnly = true;
            // 
            // B1_2TxtBx
            // 
            //resources.ApplyResources(this.B1_2TxtBx, "B1_2TxtBx");
            //this.B1_2TxtBx.Name = "B1_2TxtBx";
            //this.B1_2TxtBx.ReadOnly = true;
            // 
            // Read1Button
            // 
            resources.ApplyResources(this.Read1Button, "Read1Button");
            this.Read1Button.Name = "Read1Button";
            //this.Read1Button.Click += new System.EventHandler(this.OnRead1ButtonClicked);
            // 
            // PwdReqChkBx
            // 
            //resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            //this.PwdReqChkBx.Name = "PwdReqChkBx";
            //this.PwdReqChkBx.CheckStateChanged += new System.EventHandler(this.PwdReqChkBx_CheckStateChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.txtLoc);
            this.panel1.Controls.Add(this.txtAsset);
            this.panel1.Controls.Add(this.txtTag);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtLoc
            // 
            resources.ApplyResources(this.txtLoc, "txtLoc");
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.ReadOnly = true;
            this.txtLoc.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtAsset
            // 
            resources.ApplyResources(this.txtAsset, "txtAsset");
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.ReadOnly = true;
            this.txtAsset.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtTag
            // 
            resources.ApplyResources(this.txtTag, "txtTag");
            this.txtTag.Name = "txtTag";
            this.txtTag.ReadOnly = true;
            this.txtTag.TextChanged += new System.EventHandler(this.txtLoc_TextChanged);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSavePage
            // 
            resources.ApplyResources(this.btnSavePage, "btnSavePage");
            this.btnSavePage.Name = "btnSavePage";
            this.btnSavePage.Click += new System.EventHandler(this.btnSavePage_Click);
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            this.cboLoc.Visible = false;
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.colLocation);
            this.lstAsset.Columns.Add(this.colLotNo);
            this.lstAsset.Columns.Add(this.colTagID);
            resources.ApplyResources(this.lstAsset, "lstAsset");
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.View = System.Windows.Forms.View.Details;
            // 
            // epclistbox
            // 
            resources.ApplyResources(this.epclistbox, "epclistbox");
            this.epclistbox.Name = "epclistbox";
            this.epclistbox.Location = this.lstAsset.Location;
            this.epclistbox.Size = this.lstAsset.Size;
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // AssetName
            // 
            resources.ApplyResources(this.AssetName, "AssetName");
            // 
            // colLocation
            // 
            resources.ApplyResources(this.colLocation, "colLocation");
            // 
            // colLotNo
            // 
            resources.ApplyResources(this.colLotNo, "colLotNo");
            // 
            // colTagID
            // 
            resources.ApplyResources(this.colTagID, "colTagID");
            // 
            // btnScnBarcd
            // 
            resources.ApplyResources(this.btnScnBarcd, "btnScnBarcd");
            this.btnScnBarcd.Name = "btnScnBarcd";
            this.btnScnBarcd.Click += new System.EventHandler(this.btnScnBarcd_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSaveAll
            // 
            resources.ApplyResources(this.btnSaveAll, "btnSaveAll");
            this.btnSaveAll.Name = "btnSaveAll";
            this.btnSaveAll.Click += new System.EventHandler(this.btnSaveAll_Click);
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.lblPage, "lblPage");
            this.lblPage.Name = "lblPage";
            // 
            // cmbPage
            // 
            resources.ApplyResources(this.cmbPage, "cmbPage");
            this.cmbPage.Name = "cmbPage";

            // lnkLocName
            // 
            this.lnkLocName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Underline);
            this.lnkLocName.ForeColor = System.Drawing.Color.Black;
            this.lnkLocName.Location = new System.Drawing.Point(4, 1);
            this.lnkLocName.Name = "lnkLocName";
            this.lnkLocName.Size = new System.Drawing.Size(181, 14);
            this.lnkLocName.TabIndex = 69;
            this.lnkLocName.Text = "Select Location";
            this.lnkLocName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lnkLocName.Click += new System.EventHandler(this.lnkLocName_Click);
            // 
            // btnSelLoc
            // 
            this.btnSelLoc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnSelLoc.Location = new System.Drawing.Point(193, 1);
            this.btnSelLoc.Name = "btnSelLoc";
            this.btnSelLoc.Size = new System.Drawing.Size(40, 20);
            this.btnSelLoc.TabIndex = 70;
            this.btnSelLoc.Text = "Select";
            this.btnSelLoc.Click += new System.EventHandler(this.btnSelLoc_Click);


            // 
            // TagRdForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.btnSelLoc);
            this.Controls.Add(this.lnkLocName);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSavePage);
            this.Controls.Add(this.btnSaveAll);
            this.Controls.Add(this.cboLoc);
            //this.Controls.Add(this.DataDispPanel);
            this.Controls.Add(this.ScanButton);
            this.Controls.Add(this.btnScnBarcd);
            this.Controls.Add(this.Read1Button);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.LngLstButton);
            //this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.panel1);
            //this.Controls.Add(this.EPCListV);
            this.Controls.Add(this.epclistbox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagRdForm";
            this.Load += new System.EventHandler(this.OnTagRdFormLoad);
            this.Closed += new System.EventHandler(this.OnTagRdFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnTagRdFormClosing);
            //this.DataDispPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button LngLstButton;
        private System.Windows.Forms.Label TagCntLabel;
        private System.Windows.Forms.Button ScanButton;
        private System.Windows.Forms.Button Read1Button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtAsset;
        private System.Windows.Forms.TextBox txtLoc;
        private System.Windows.Forms.TextBox txtTag;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnSavePage;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.ListView lstAsset;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader AssetName;
        private System.Windows.Forms.ColumnHeader colLocation;
        private System.Windows.Forms.ColumnHeader colTagID;
        private System.Windows.Forms.Button btnScnBarcd;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ColumnHeader colLotNo;
        private System.Windows.Forms.Button btnSaveAll;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.ComboBox cmbPage;
        private System.Windows.Forms.LinkLabel lnkLocName;
        private System.Windows.Forms.Button btnSelLoc;
        private System.Windows.Forms.ListBox epclistbox;
    }
}