namespace OnRamp
{
    partial class frmSearchlist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnSearch = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnUnSelectAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(3, 169);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(67, 23);
            this.btnSearch.TabIndex = 49;
            this.btnSearch.Text = "Search All";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(217, 169);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 50;
            this.button1.Text = "Search Selected";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.columnHeader2);
            this.lstAsset.Location = new System.Drawing.Point(2, 25);
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.Size = new System.Drawing.Size(313, 142);
            this.lstAsset.TabIndex = 51;
            this.lstAsset.View = System.Windows.Forms.View.Details;
            this.lstAsset.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstAsset_ItemCheck);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = global::OnRamp.ResourceFiles.TagWrForm.RowNumHdr_Text;
            this.columnHeader1.Width = 30;
            // 
            // AssetName
            // 
            this.AssetName.Text = "Item";
            this.AssetName.Width = 125;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TagID";
            this.columnHeader2.Width = 150;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(72, 169);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(62, 23);
            this.btnSelectAll.TabIndex = 52;
            this.btnSelectAll.Text = "Select All";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // lblPage
            // 
            this.lblPage.Location = new System.Drawing.Point(-1, 4);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(47, 16);
            this.lblPage.Text = "Pages";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbPage
            // 
            this.cmbPage.Location = new System.Drawing.Point(52, 1);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(103, 23);
            this.cmbPage.TabIndex = 61;
            // 
            // btnUnSelectAll
            // 
            this.btnUnSelectAll.Location = new System.Drawing.Point(136, 169);
            this.btnUnSelectAll.Name = "btnUnSelectAll";
            this.btnUnSelectAll.Size = new System.Drawing.Size(79, 23);
            this.btnUnSelectAll.TabIndex = 62;
            this.btnUnSelectAll.Text = "UnSelect All";
            this.btnUnSelectAll.Click += new System.EventHandler(this.btnUnSelectAll_Click);
            // 
            // frmSearchlist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(317, 195);
            this.Controls.Add(this.btnUnSelectAll);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSearch);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearchlist";
            this.Text = "Item List";
            this.Load += new System.EventHandler(this.frmSearchlist_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmSearchlist_KeyUp);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnSearch = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnUnSelectAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(16, 241);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(97, 23);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Search All";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(121, 241);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(103, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Search Selected";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.columnHeader2);
            this.lstAsset.Location = new System.Drawing.Point(2, 25);
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.Size = new System.Drawing.Size(233, 181);
            this.lstAsset.TabIndex = 1;
            this.lstAsset.View = System.Windows.Forms.View.Details;
            this.lstAsset.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.lstAsset_ItemCheck);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 30;
            // 
            // AssetName
            // 
            this.AssetName.Text = "Item";
            this.AssetName.Width = 125;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TagID";
            this.columnHeader2.Width = 150;
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(16, 212);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(97, 23);
            this.btnSelectAll.TabIndex = 2;
            this.btnSelectAll.Text = "Select All";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // lblPage
            // 
            this.lblPage.Location = new System.Drawing.Point(-1, 4);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(47, 16);
            this.lblPage.Text = "Pages";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbPage
            // 
            this.cmbPage.Location = new System.Drawing.Point(52, 1);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(103, 23);
            this.cmbPage.TabIndex = 0;
            // 
            // btnUnSelectAll
            // 
            this.btnUnSelectAll.Location = new System.Drawing.Point(121, 212);
            this.btnUnSelectAll.Name = "btnUnSelectAll";
            this.btnUnSelectAll.Size = new System.Drawing.Size(101, 23);
            this.btnUnSelectAll.TabIndex = 3;
            this.btnUnSelectAll.Text = "UnSelect All";
            this.btnUnSelectAll.Click += new System.EventHandler(this.btnUnSelectAll_Click);
            // 
            // frmSearchlist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.btnUnSelectAll);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnSearch);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearchlist";
            this.Text = "Item List";
            this.Load += new System.EventHandler(this.frmSearchlist_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView lstAsset;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader AssetName;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.ComboBox cmbPage;
        private System.Windows.Forms.Button btnUnSelectAll;
    }
}