﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;

using System.Windows.Forms;
using System.IO;
using ClsReaderLib;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using HHDeviceInterface.BarCode;

namespace OnRamp
{
    public partial class ExportTag : Form
    {

        Reader Rdr = null;
        
        List<string> tags = new List<string>();
        private bool isReading = false;
        private bool inventory = false;
        private bool changeOnDisable = false;
        private string fileName = "";
        public ExportTag(bool inve,string fname)
        {
            InitializeComponent();
            inventory = inve;
            fileName = fname;
        }
 
        private void btnremove_Click(object sender, EventArgs e)
        {
            lstvtaglist.SuspendLayout();
 
            for (int i = lstvtaglist.Items.Count - 1; i >= 0; i--)
                if (lstvtaglist.Items[i].Checked)
                {
                    
                    tags.Remove(lstvtaglist.Items[i].Text);
                    lstvtaglist.Items.RemoveAt(i);
                }

            lblcounter.Text = lstvtaglist.Items.Count.ToString();
            lstvtaglist.ResumeLayout();
            SetFocus(null, new EventArgs());
        }

        private void btnsave_Click(object sender, EventArgs e)
        {
            if (lstvtaglist.Items.Count < 1)
            {
                MessageBox.Show("No items to save.");
                return;
            }
            StringBuilder sb = new StringBuilder();

           foreach(ListViewItem i in lstvtaglist.Items)//  for (int i = 0; i < lstvtaglist.Items.Count; i++)
            {
                string item = "";
                foreach (ListViewItem.ListViewSubItem it in i.SubItems)
                    item += it.Text + ",";
                sb.Append(item.Trim(',') + "\n");
            }
            string upath = UserPref.GetInstance().CSVPath; //@"Flash Disk\RFIDMobile";
            string path = upath;
            try
            {
                if (!Directory.Exists(upath))
                {
                    if (Directory.Exists("Flash Disk"))
                    {
                        if (!Directory.Exists(@"\Flash Disk\RFIDMobile"))
                            Directory.CreateDirectory(@"\Flash Disk\RFIDMobile");
                        path = @"\Flash Disk\RFIDMobile";
                    }
                    else
                    {
                        if (!Directory.Exists("RFIDMobile"))
                            Directory.CreateDirectory("RFIDMobile");
                        path = "RFIDMobile";
                    }

                    MessageBox.Show(upath + " does not exist.\r\nFile will be saved in " + path);
                }
                

                try
                {
                    foreach (string f in Directory.GetFiles(path))
                    {
                        
                        if (new FileInfo(f).CreationTime < DateTime.Now.Subtract(new TimeSpan(UserPref.GetInstance().BackupDays, 0, 0, 0)))
                            File.Delete( f);
                    }
                }
                catch
                { }

                StreamWriter sw = new StreamWriter(path + @"\" + fileName +"_"+ DateTime.Now.ToString("ddMMyyyy_HHmmss") + DateTime.Now.Millisecond.ToString() + ".csv", true);
                sw.Write(sb.ToString());
                sw.Close();
                ClearTagList();
                txtlocation.Text = "";
                MessageBox.Show("File is saved in " + path + " folder.", "Save file");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
               
            }

            SetFocus(null,new EventArgs());
            
            
        }

        private void ClearTagList()
        {
            lstvtaglist.SuspendLayout();
            lstvtaglist.Items.Clear();
            tags.Clear();
            lstvtaglist.ResumeLayout();
            lblcounter.Text = "0";
        }

        private void btnclear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you want to clear the tag list?", "Clear Taglist", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                ClearTagList();
                
            }
            SetFocus(null, new EventArgs());
        }

        //Barcode
        private void btnbarcode_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
           
            //Starting
            if (!isReading)
            {

                StartBarcodeReading();
            }
            else
            {
                StopBarcodeReading();
                
            }
            Cursor.Current = Cursors.Default;
        }
        private void StartBarcodeReading()
        {

            //BRdr.StartReader();
            if (!Program.barInitialized)
            {

                Program.InitBarcodeReader();

                if (!Program.barInitialized)
                {
                    MessageBox.Show("Barcode scanner initialization error.");
                    return;
                }
                Program.BRdr.notifyee = this;
                Program.BRdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
            }
            Program.BRdr.StartReader();
            isReading = true;
            btnremove.Enabled = false;
            btnclear.Enabled = false;
            btnsave.Enabled = false;
        }
        private void StopBarcodeReading()
        {
            //Stopping
            //BRdr.RegisterStopNotificationEvent(ScanStopped);

            //BRdr.StopReader();

            if (Program.barInitialized)
                Program.BRdr.StopReader();

            isReading = false;
            btnsave.Enabled = true;
            btnclear.Enabled = true;
            btnremove.Enabled = true;
        }
        private bool BarCodeNotify(bool succ, String bcStr, String errMsg)
        {
            if (succ)
            {
                if (!chcontinuous.Checked)
                    StopBarcodeReading();
                
                AddToList(bcStr);
               
            }
            

            return true;
        }
        
        //RFID read
        private void btnread_Click(object sender, EventArgs e)
        {

             
            if (!isReading)
            {
                StartRFIDReading();
            }
            else
            {
                StopRFIDReading();
            }
            //btnread.Enabled = true;
             
        }
        private void StopRFIDReading()
        {
            //Stop reading tags
            Rdr.TagInventoryStop();
            Rdr.UnregisterDscvrTagEvent(AddTag);

            isReading = false;
            btnsave.Enabled = true;
            btnclear.Enabled = true;
            btnremove.Enabled = true;
        }
        private void StartRFIDReading()
        {
            //Start reading tags
            if (Rdr == null)
                Rdr = ReaderFactory.GetReader();

            if (Rdr == null)
            {
                MessageBox.Show("Cannot access RFID reader module.", "Reader Error");
                return;
            }

            Rdr.RegisterDscvrTagEvent(AddTag);

            if (!Rdr.SetRepeatedTagObsrvMode(true))
                MessageBox.Show("Disable Repeated Tag Observation mode failed", "Warning");

            if (!Rdr.TagInventoryStart(5))
            {
                MessageBox.Show("Cannot start reader.", "Reader Error");
                Rdr.UnregisterDscvrTagEvent(AddTag);
                return;
            }

            isReading = true;
            btnremove.Enabled = false;
            btnclear.Enabled = false;
            btnsave.Enabled = false;
        }

        private void AddTag(object sender, DscvrTagEventArgs e)
        {
            // Add new TagID to list (or update occurrence)
            if (e.Cnt != 0)
            {
                if (!chcontinuous.Checked)
                    StopRFIDReading();
                AddToList(e.EPC.ToString());
                
            }
        }

        private void AddToList(string epc)
        {
            if (txtlocation.Focused)
            {
                txtlocation.Text = epc;
                if (txtrfidtag.Visible)
                    txtrfidtag.Focus();
                else
                    lstvtaglist.Focus();
            }
            else if (txtrfidtag.Focused)
            {
                txtrfidtag.Text = epc;
                txtassetid.Focus();
            }
            else if (txtassetid.Focused)
            {
                if (txtrfidtag.Text != epc)
                {
                    txtassetid.Text = epc;
                    AddToGrid(epc);
                }
            }
            else if(inventory)
            {
                AddToGrid(epc);
            }
            

        }

        private void AddToGrid(string epc)
        {
            if (inventory)
            {
                if (!tags.Contains(epc))
                {

                    lstvtaglist.Items.Add(new ListViewItem(new string[] { epc, txtlocation.Text }));
                    tags.Add(epc);
                }
            }
            else
            {
                if (!tags.Contains(txtrfidtag.Text.Trim() + ":" + txtassetid.Text.Trim()))
                {
                    lstvtaglist.Items.Add(new ListViewItem(new string[] {txtrfidtag.Text,txtassetid.Text,txtlocation.Text }));
                    tags.Add(txtrfidtag.Text.Trim() + ":" + txtassetid.Text.Trim());
                   
                } 
                txtrfidtag.Text = txtassetid.Text ="";
                    txtrfidtag.Focus();
            }
            lblcounter.Text = lstvtaglist.Items.Count.ToString();
        }

        private void ExportTag_Load(object sender, EventArgs e)
        {
            this.Text = fileName; 
            Cursor.Current = Cursors.WaitCursor;
            lblmessage.Text = "";
             Rdr = ReaderFactory.GetReader();
             //BRdr = BarCodeFactory.GetBarCodeRdr();
             //BRdr.InitializeReader();
             if (Program.BRdr != null)
             {
                 Program.BRdr.notifyee = this;
                 Program.BRdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
             }
             lblcounter.Text = "0";
             this.KeyPreview = true;
             if (inventory)
             {
                 lblmessage.Visible = txtrfidtag.Visible = txtassetid.Visible = false;
                 lstvtaglist.Location = new Point(7, 85);
                 lstvtaglist.Size = new Size(227,154);
                 lstvtaglist.Columns.Remove(lstvtaglist.Columns[1]);
                 chcontinuous.Visible =  true;
                 if (!fileName.ToLower().StartsWith("move"))
                     chcontinuous.Checked = true;

             }
             txtlocation.Focus();
             Cursor.Current = Cursors.Default;

        }

       

        private void ExportTag_KeyDown(object sender, KeyEventArgs e)
        {
            panel1.Enabled = false;
            //Stop reading on trigger
            switch (e.KeyCode)
            {
                case Keys.F19:
                    {
                        if (rdbrfid.Checked)
                        {
                            //RFID reader
                            StartRFIDReading();

                        }
                        else
                        {
                            //Barcode
                            StartBarcodeReading();

                        }
                    }
                    break;
                
            }

            panel1.Enabled = true;
        }

        private void ExportTag_KeyUp(object sender, KeyEventArgs e)
        {
            panel1.Enabled = false;
            //Stop reading on trigger
            switch(e.KeyCode)
            {
                case Keys.F19:
                    {
                        if (rdbrfid.Checked)
                        {
                            //RFID reader
                            StopRFIDReading();
                            
                        }
                        else
                        {
                            //Barcode
                            StopBarcodeReading();
                             
                        }
                    }
                    break;
                case Keys.F8:
                case Keys.F7:
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
            }
            
            panel1.Enabled = true;
        }

        private void rdbrfid_CheckedChanged(object sender, EventArgs e)
        {
           
        }

        private void ExportTag_Closing(object sender, CancelEventArgs e)
        {

            if (isReading)
            {
                MessageBox.Show("Please stop the current operation.");
                e.Cancel = true;
            }
            Cursor.Current = Cursors.WaitCursor;
            if (Program.BRdr != null)
            {
                Program.BRdr.StopReader();
                Program.BRdr.UnregisterCodeRcvdNotificationEvent(BarCodeNotify);
                
            }
            Cursor.Current = Cursors.Default;
        }

        private void txtrfidtag_GotFocus(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            
            if (t != null)
            {
                t.SelectAll();

                lblmessage.Text = (t.Tag != null)?t.Tag.ToString():"";
                if (t.Name == "txtassetid")
                {
                    if (rdbrfid.Checked)
                    {
                        changeOnDisable = true;
                        rdbbarcode.Checked = true;
                    }
                    rdbrfid.Enabled = false;
                }
            }
            if (t.Name == "txtlocation" || t.Name == "txtassetid")
                inputPanel1.Enabled = true;
        }

        private void txtrfidtag_LostFocus(object sender, EventArgs e)
        {
            if (((TextBox)sender).Name == "txtassetid")
            {
                rdbrfid.Enabled = true;
            }
            if (changeOnDisable)
                rdbrfid.Checked = true;

            changeOnDisable = false;
            lblmessage.Text ="";
            inputPanel1.Enabled = false;
        }

        private void SetFocus(object sender, EventArgs e)
        {
            if (inventory)
                lstvtaglist.Focus();
            else
                txtrfidtag.Focus();
        }

        private void txtlocation_LostFocus(object sender, EventArgs e)
        {
            inputPanel1.Enabled = false;
        }

        private void txtlocation_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (inventory)
                    lstvtaglist.Focus();
                else
                    txtrfidtag.Focus();
            }
        }

        private void txtassetid_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                if (txtrfidtag.Text != txtassetid.Text)
                {

                    AddToGrid(txtassetid.Text);
                }
            }
        }

        private void txtrfidtag_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                txtassetid.Focus();
            }
        }

       

        

        

    }
}