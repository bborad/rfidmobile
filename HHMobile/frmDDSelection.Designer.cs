﻿namespace OnRamp
{
    partial class frmDDSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.txtEnteredData = new System.Windows.Forms.TextBox();
            this.btnScan = new System.Windows.Forms.Button();
            this.pnlObjectList = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.lvSelectOption = new System.Windows.Forms.ListView();
            this.ID = new System.Windows.Forms.ColumnHeader();
            this.chName = new System.Windows.Forms.ColumnHeader();
            this.chID = new System.Windows.Forms.ColumnHeader();
            this.pnlTagReads = new System.Windows.Forms.Panel();
            this.btnTagSelect = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lvTagReads = new System.Windows.Forms.ListView();
            this.chTagId = new System.Windows.Forms.ColumnHeader();
            this.chRSSI = new System.Windows.Forms.ColumnHeader();
            this.cbPage = new System.Windows.Forms.ComboBox();
            this.pnlObjectList.SuspendLayout();
            this.pnlTagReads.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtEnteredData
            // 
            this.txtEnteredData.Location = new System.Drawing.Point(4, 4);
            this.txtEnteredData.Name = "txtEnteredData";
            this.txtEnteredData.Size = new System.Drawing.Size(253, 23);
            this.txtEnteredData.TabIndex = 0;
            this.txtEnteredData.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEnteredData_KeyUp);
            this.txtEnteredData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEnteredData_KeyPress);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(263, 4);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(48, 21);
            this.btnScan.TabIndex = 1;
            this.btnScan.Text = "Scan";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // pnlObjectList
            // 
            this.pnlObjectList.Controls.Add(this.cbPage);
            this.pnlObjectList.Controls.Add(this.btnReset);
            this.pnlObjectList.Controls.Add(this.btnCancel);
            this.pnlObjectList.Controls.Add(this.btnSelect);
            this.pnlObjectList.Controls.Add(this.lvSelectOption);
            this.pnlObjectList.Location = new System.Drawing.Point(3, 30);
            this.pnlObjectList.Name = "pnlObjectList";
            this.pnlObjectList.Size = new System.Drawing.Size(308, 164);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(70, 140);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(60, 21);
            this.btnReset.TabIndex = 8;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(180, 140);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(47, 21);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(130, 140);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(47, 21);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "Select";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // lvSelectOption
            // 
            this.lvSelectOption.Columns.Add(this.ID);
            this.lvSelectOption.Columns.Add(this.chName);
            this.lvSelectOption.Columns.Add(this.chID);
            this.lvSelectOption.FullRowSelect = true;
            this.lvSelectOption.Location = new System.Drawing.Point(1, 2);
            this.lvSelectOption.Name = "lvSelectOption";
            this.lvSelectOption.Size = new System.Drawing.Size(308, 138);
            this.lvSelectOption.TabIndex = 3;
            this.lvSelectOption.View = System.Windows.Forms.View.Details;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 1;
            // 
            // chName
            // 
            this.chName.Text = "Name";
            this.chName.Width = 200;
            // 
            // chID
            // 
            this.chID.Text = "Location ID";
            this.chID.Width = 200;
            // 
            // pnlTagReads
            // 
            this.pnlTagReads.Controls.Add(this.btnTagSelect);
            this.pnlTagReads.Controls.Add(this.btnClose);
            this.pnlTagReads.Controls.Add(this.lvTagReads);
            this.pnlTagReads.Location = new System.Drawing.Point(3, 29);
            this.pnlTagReads.Name = "pnlTagReads";
            this.pnlTagReads.Size = new System.Drawing.Size(311, 164);
            // 
            // btnTagSelect
            // 
            this.btnTagSelect.Location = new System.Drawing.Point(5, 140);
            this.btnTagSelect.Name = "btnTagSelect";
            this.btnTagSelect.Size = new System.Drawing.Size(64, 22);
            this.btnTagSelect.TabIndex = 8;
            this.btnTagSelect.Text = "Select";
            this.btnTagSelect.Click += new System.EventHandler(this.btnTagSelect_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(73, 140);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 22);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lvTagReads
            // 
            this.lvTagReads.Columns.Add(this.chTagId);
            this.lvTagReads.Columns.Add(this.chRSSI);
            this.lvTagReads.FullRowSelect = true;
            this.lvTagReads.Location = new System.Drawing.Point(0, 0);
            this.lvTagReads.Name = "lvTagReads";
            this.lvTagReads.Size = new System.Drawing.Size(308, 138);
            this.lvTagReads.TabIndex = 4;
            this.lvTagReads.View = System.Windows.Forms.View.Details;
            // 
            // chTagId
            // 
            this.chTagId.Text = "Tag ID";
            this.chTagId.Width = 250;
            // 
            // chRSSI
            // 
            this.chRSSI.Text = "RSSI";
            this.chRSSI.Width = 50;
            // 
            // cbPage
            // 
            this.cbPage.Location = new System.Drawing.Point(4, 139);
            this.cbPage.Name = "cbPage";
            this.cbPage.Size = new System.Drawing.Size(64, 23);
            this.cbPage.TabIndex = 9;
            // 
            // frmDDSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(317, 195);
            this.Controls.Add(this.pnlObjectList);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.txtEnteredData);
            this.Controls.Add(this.pnlTagReads);
            this.Name = "frmDDSelection";
            this.Text = "Selection Screen";
            this.Load += new System.EventHandler(this.frmDDSelection_Load);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmDDSelection_Closing);
            this.pnlObjectList.ResumeLayout(false);
            this.pnlTagReads.ResumeLayout(false);
            this.ResumeLayout(false);

        }


        private void InitializeComponent_AT870()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.txtEnteredData = new System.Windows.Forms.TextBox();
            this.btnScan = new System.Windows.Forms.Button();
            this.pnlObjectList = new System.Windows.Forms.Panel();
            this.btnReset = new System.Windows.Forms.Button();
            this.cbPage = new System.Windows.Forms.ComboBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.lvSelectOption = new System.Windows.Forms.ListView();
            this.ID = new System.Windows.Forms.ColumnHeader();
            this.chName = new System.Windows.Forms.ColumnHeader();
            this.chID = new System.Windows.Forms.ColumnHeader();
            this.pnlTagReads = new System.Windows.Forms.Panel();
            this.btnTagSelect = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lvTagReads = new System.Windows.Forms.ListView();
            this.chTagId = new System.Windows.Forms.ColumnHeader();
            this.chRSSI = new System.Windows.Forms.ColumnHeader();
            this.pnlObjectList.SuspendLayout();
            this.pnlTagReads.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtEnteredData
            // 
            this.txtEnteredData.Location = new System.Drawing.Point(4, 4);
            this.txtEnteredData.Name = "txtEnteredData";
            this.txtEnteredData.Size = new System.Drawing.Size(177, 23);
            this.txtEnteredData.TabIndex = 0;
            this.txtEnteredData.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtEnteredData_KeyUp);
            this.txtEnteredData.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEnteredData_KeyPress);
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(185, 5);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(48, 21);
            this.btnScan.TabIndex = 1;
            this.btnScan.Text = "Scan";
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // pnlObjectList
            // 
            this.pnlObjectList.Controls.Add(this.cbPage);
            this.pnlObjectList.Controls.Add(this.btnReset);
            this.pnlObjectList.Controls.Add(this.btnCancel);
            this.pnlObjectList.Controls.Add(this.btnSelect);
            this.pnlObjectList.Controls.Add(this.lvSelectOption);
            this.pnlObjectList.Location = new System.Drawing.Point(3, 30);
            this.pnlObjectList.Name = "pnlObjectList";
            this.pnlObjectList.Size = new System.Drawing.Size(233, 238);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(70, 213);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(60, 21);
            this.btnReset.TabIndex = 8;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(180, 213);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(47, 21);
            this.btnCancel.TabIndex = 7;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(130, 213);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(47, 21);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "Select";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);

            // 
            // cbPage
            // 
            this.cbPage.Location = new System.Drawing.Point(4, 213);
            this.cbPage.Name = "cbPage";
            this.cbPage.Size = new System.Drawing.Size(64, 23);
            this.cbPage.TabIndex = 9;

            // 
            // lvSelectOption
            // 
            this.lvSelectOption.Columns.Add(this.ID);
            this.lvSelectOption.Columns.Add(this.chName);
            this.lvSelectOption.Columns.Add(this.chID);
            this.lvSelectOption.FullRowSelect = true;
            this.lvSelectOption.Location = new System.Drawing.Point(1, 2);
            this.lvSelectOption.Name = "lvSelectOption";
            this.lvSelectOption.Size = new System.Drawing.Size(230, 207);
            this.lvSelectOption.TabIndex = 3;
            this.lvSelectOption.View = System.Windows.Forms.View.Details;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            this.ID.Width = 1;
            // 
            // chName
            // 
            this.chName.Text = "Name";
            this.chName.Width = 200;
            // 
            // chID
            // 
            this.chID.Text = "Location ID";
            this.chID.Width = 200;
            // 
            // pnlTagReads
            // 
            this.pnlTagReads.Controls.Add(this.btnTagSelect);
            this.pnlTagReads.Controls.Add(this.btnClose);
            this.pnlTagReads.Controls.Add(this.lvTagReads);
            this.pnlTagReads.Location = new System.Drawing.Point(3, 30);
            this.pnlTagReads.Name = "pnlTagReads";
            this.pnlTagReads.Size = new System.Drawing.Size(233, 238);
            // 
            // btnTagSelect
            // 
            this.btnTagSelect.Location = new System.Drawing.Point(5, 214);
            this.btnTagSelect.Name = "btnTagSelect";
            this.btnTagSelect.Size = new System.Drawing.Size(64, 22);
            this.btnTagSelect.TabIndex = 8;
            this.btnTagSelect.Text = "Select";
            this.btnTagSelect.Click += new System.EventHandler(this.btnTagSelect_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(73, 214);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 22);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lvTagReads
            // 
            this.lvTagReads.Columns.Add(this.chTagId);
            this.lvTagReads.Columns.Add(this.chRSSI);
            this.lvTagReads.FullRowSelect = true;
            this.lvTagReads.Location = new System.Drawing.Point(0, 0);
            this.lvTagReads.Name = "lvTagReads";
            this.lvTagReads.Size = new System.Drawing.Size(233, 211);
            this.lvTagReads.TabIndex = 4;
            this.lvTagReads.View = System.Windows.Forms.View.Details;
            // 
            // chTagId
            // 
            this.chTagId.Text = "Tag ID";
            this.chTagId.Width = 200;
            // 
            // chRSSI
            // 
            this.chRSSI.Text = "RSSI";
            this.chRSSI.Width = 50;
            // 
            // frmDDSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.pnlTagReads);
            this.Controls.Add(this.pnlObjectList);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.txtEnteredData);
            this.Name = "frmDDSelection";
            this.Text = "Selection Screen";
            this.Load += new System.EventHandler(this.frmDDSelection_Load);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmDDSelection_Closing);
            this.pnlObjectList.ResumeLayout(false);
            this.pnlTagReads.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtEnteredData;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Panel pnlObjectList;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.ListView lvSelectOption;
        private System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chID;
        private System.Windows.Forms.Panel pnlTagReads;
        private System.Windows.Forms.ListView lvTagReads;
        private System.Windows.Forms.ColumnHeader chTagId;
        private System.Windows.Forms.ColumnHeader chRSSI;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnTagSelect;
        private System.Windows.Forms.ComboBox cbPage;
    }
}