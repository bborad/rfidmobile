/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Inventory functionality
 **************************************************************************************/
using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;

namespace OnRamp
{
    public partial class frmDocDesdripency : Form
    {
        private DataTable _searchedData;

        public DataTable searchedData
        {
            set
            {
                _searchedData = value;
            }
        }

        public frmDocDesdripency()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void frmDocDesdripency_Load(object sender, EventArgs e)
        {
            BindListData();
        }

        private void BindListData()
        {
            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            //DataRow dr=new DataRow();
            lstAsset.CheckBoxes = true;
            lstAsset.FullRowSelect = true;
            Boolean bFound = false; 
            Documents D;
            Int32 ServerKey;
            if (_searchedData.Rows.Count <= 0)
            {
                Program.ShowError("No scanned record found.");
                return; 
            }
            ServerKey = Convert.ToInt32(_searchedData.Rows[0]["ServerKey"]);
            D = new Documents(ServerKey); 
            foreach (Assets ap in D.LstAsset)
            {
                lstItem = new ListViewItem();
                //Convert.ToString(dr["TagID"]) + "," + Convert.ToString(dr["Name"])

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(D.DocumentNo);
                lstItem.SubItems.Add(ls);


                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(ap.Name.Trim());
                lstItem.SubItems.Add(ls);

                bFound = false; 
                foreach (DataRow dr in _searchedData.Rows)
                {
                    if (dr["AssetTagID"].ToString().Trim() == ap.TagID.Trim())
                    {
                        bFound = true;
                        break;
                    }
                }
                if (!bFound)
                {
                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = "Missing";
                    lstItem.SubItems.Add(ls);
                    lstItem.BackColor = Color.Red;    
                }
                else
                {
                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = "Verify";
                    lstItem.SubItems.Add(ls);
                    lstItem.BackColor = Color.White;    
                }
                lstAsset.Items.Add(lstItem);
            }

            //For Excess
            foreach (DataRow dr in _searchedData.Rows)
            {
                lstItem = new ListViewItem();
                //Convert.ToString(dr["TagID"]) + "," + Convert.ToString(dr["Name"])

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = "NA";
                lstItem.SubItems.Add(ls);


                Assets and = new Assets(dr["AssetTagID"].ToString().Trim());
 
                ls = new ListViewItem.ListViewSubItem();
                if (and!=null)
                {
                    ls.Text = and.Name; 
                }
                else
                {
                    ls.Text = "";
                }
                lstItem.SubItems.Add(ls);

                bFound = false; 
                foreach (Assets ap in D.LstAsset)
                {
                    if (dr["AssetTagID"].ToString().Trim() == ap.TagID.Trim())
                    {
                        bFound = true;
                        break;
                    }
                }
                if (!bFound)
                {
                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = "Excess";
                    lstItem.SubItems.Add(ls);
                    lstItem.BackColor = Color.Green;
                    lstAsset.Items.Add(lstItem);
                }
                
                
            }
            lstAsset.Refresh();

        }

        //private void btnIgnorAll_Click(object sender, EventArgs e)
        //{
        //    lstAsset.Clear();
        //}

        //private void btnChangeLoc_Click(object sender, EventArgs e)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("");
        //    int iOther = 0, iCount = 0;
        //    foreach (ListViewItem lv in lstAsset.Items)
        //    {
        //        if (lv.Checked)
        //        {
        //            iCount++;
        //            if (Convert.ToInt64(lv.SubItems[7].Text.ToString().Trim()) < 0)
        //            {
        //                MessageBox.Show("Please do Synchronisation first.");
        //                return;
        //            }
        //            else
        //            {
        //                if (lv.SubItems[4].Text.ToString().Trim().ToLower() == "location_mismatch")
        //                {
        //                    Assets.UpdateAssetLocation(lv.SubItems[5].Text.ToString().Trim(), Convert.ToInt64(lv.SubItems[7].Text.ToString().Trim()));

        //                    if (sb.ToString() == "")
        //                        sb.Append(lv.SubItems[5].Text.ToString().Trim());
        //                    else
        //                        sb.Append("," + lv.SubItems[5].Text.ToString().Trim());
        //                }
        //                else
        //                    iOther++;
        //            }
        //        }
        //    }

        //    if (iCount == 0)
        //    {
        //        MessageBox.Show("Please select first.");
        //    }
        //    string[] stTag = sb.ToString().Split(',');
        //    int iListcount = 0;
        //    for (int i = 0; i < lstAsset.Items.Count; i++)
        //    {
        //        foreach (string s in stTag)
        //        {
        //            if (lstAsset.Items[i].SubItems[5].Text.ToString().Trim() == s)
        //            {
        //                lstAsset.Items.RemoveAt(i);
        //                iListcount++;
        //            }
        //        }

        //    }
        //    lstAsset.Refresh();

        //    if (iOther > 0)
        //        MessageBox.Show("Descripencies removed only for Location_Mismatch error code.");
        //    else
        //        MessageBox.Show("Descripencies removed successfully.");

        //    //searchedData.Rows[
        //}

        //private void btnSelectAll_Click(object sender, EventArgs e)
        //{
        //    foreach (ListViewItem lv in lstAsset.Items)
        //    {
        //        if (lv.SubItems[4].Text.ToString().Trim().ToLower() == "location_mismatch")
        //            lv.Checked = true;
        //    }
        //}
    }
}