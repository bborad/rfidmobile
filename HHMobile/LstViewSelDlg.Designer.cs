namespace OnRamp
{
    partial class LstViewSelDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LstViewSelDlg));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.ItemListV = new System.Windows.Forms.ListView();
            this.HeaderLbl = new System.Windows.Forms.Label();
            this.OKBttn = new System.Windows.Forms.Button();
            this.RstBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ItemListV
            // 
            this.ItemListV.FullRowSelect = true;
            this.ItemListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            resources.ApplyResources(this.ItemListV, "ItemListV");
            this.ItemListV.Name = "ItemListV";
            this.ItemListV.View = System.Windows.Forms.View.Details;
            // 
            // HeaderLbl
            // 
            resources.ApplyResources(this.HeaderLbl, "HeaderLbl");
            this.HeaderLbl.Name = "HeaderLbl";
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            // 
            // RstBttn
            // 
            resources.ApplyResources(this.RstBttn, "RstBttn");
            this.RstBttn.Name = "RstBttn";
            this.RstBttn.Click += new System.EventHandler(this.OnRstBttnClicked);
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // LstViewSelDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.RstBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.HeaderLbl);
            this.Controls.Add(this.ItemListV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LstViewSelDlg";
            this.Load += new System.EventHandler(this.OnFormLoaded);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.LstViewSelDlg));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.ItemListV = new System.Windows.Forms.ListView();
            this.HeaderLbl = new System.Windows.Forms.Label();
            this.OKBttn = new System.Windows.Forms.Button();
            this.RstBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ItemListV
            // 
            this.ItemListV.FullRowSelect = true;
            this.ItemListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            resources.ApplyResources(this.ItemListV, "ItemListV");
            this.ItemListV.Name = "ItemListV";
            this.ItemListV.View = System.Windows.Forms.View.Details;
            // 
            // HeaderLbl
            // 
            resources.ApplyResources(this.HeaderLbl, "HeaderLbl");
            this.HeaderLbl.Name = "HeaderLbl";
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            // 
            // RstBttn
            // 
            resources.ApplyResources(this.RstBttn, "RstBttn");
            this.RstBttn.Name = "RstBttn";
            this.RstBttn.Click += new System.EventHandler(this.OnRstBttnClicked);
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // LstViewSelDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.RstBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.HeaderLbl);
            this.Controls.Add(this.ItemListV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LstViewSelDlg";
            this.Load += new System.EventHandler(this.OnFormLoaded);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView ItemListV;
        private System.Windows.Forms.Label HeaderLbl;
        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Button RstBttn;
        private System.Windows.Forms.Button CancelBttn;
    }
}