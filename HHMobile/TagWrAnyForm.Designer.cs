namespace OnRamp
{
    partial class TagWrAnyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrAnyForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.DataDispPanel = new System.Windows.Forms.Panel();
            this.AutoIncBttn = new System.Windows.Forms.CheckBox();
            this.StopAtOneTagBttn = new System.Windows.Forms.CheckBox();
            this.tagWrBnkInput = new OnRamp.TagWrBnkInput();
            this.Bnk3Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk1Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk0Bttn = new System.Windows.Forms.RadioButton();
            this.ResBttn = new System.Windows.Forms.Button();
            this.StartBttn = new System.Windows.Forms.Button();
            this.EPCLstV = new System.Windows.Forms.ListView();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.ClrBttn = new System.Windows.Forms.Button();
            this.DataDispPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataDispPanel
            // 
            this.DataDispPanel.BackColor = System.Drawing.Color.Transparent;
            this.DataDispPanel.Controls.Add(this.AutoIncBttn);
            this.DataDispPanel.Controls.Add(this.StopAtOneTagBttn);
            this.DataDispPanel.Controls.Add(this.tagWrBnkInput);
            this.DataDispPanel.Controls.Add(this.Bnk3Bttn);
            this.DataDispPanel.Controls.Add(this.Bnk1Bttn);
            this.DataDispPanel.Controls.Add(this.Bnk0Bttn);
            resources.ApplyResources(this.DataDispPanel, "DataDispPanel");
            this.DataDispPanel.Name = "DataDispPanel";
            // 
            // AutoIncBttn
            // 
            resources.ApplyResources(this.AutoIncBttn, "AutoIncBttn");
            this.AutoIncBttn.Name = "AutoIncBttn";
            // 
            // StopAtOneTagBttn
            // 
            resources.ApplyResources(this.StopAtOneTagBttn, "StopAtOneTagBttn");
            this.StopAtOneTagBttn.Name = "StopAtOneTagBttn";
            // 
            // tagWrBnkInput
            // 
            resources.ApplyResources(this.tagWrBnkInput, "tagWrBnkInput");
            this.tagWrBnkInput.Name = "tagWrBnkInput";
            // 
            // Bnk3Bttn
            // 
            resources.ApplyResources(this.Bnk3Bttn, "Bnk3Bttn");
            this.Bnk3Bttn.Name = "Bnk3Bttn";
            this.Bnk3Bttn.TabStop = false;
            this.Bnk3Bttn.Click += new System.EventHandler(this.OnBnk3Clicked);
            // 
            // Bnk1Bttn
            // 
            this.Bnk1Bttn.Checked = true;
            resources.ApplyResources(this.Bnk1Bttn, "Bnk1Bttn");
            this.Bnk1Bttn.Name = "Bnk1Bttn";
            this.Bnk1Bttn.Click += new System.EventHandler(this.OnBnk1Clicked);
            // 
            // Bnk0Bttn
            // 
            resources.ApplyResources(this.Bnk0Bttn, "Bnk0Bttn");
            this.Bnk0Bttn.Name = "Bnk0Bttn";
            this.Bnk0Bttn.TabStop = false;
            this.Bnk0Bttn.Click += new System.EventHandler(this.OnBnk0Clicked);
            // 
            // ResBttn
            // 
            resources.ApplyResources(this.ResBttn, "ResBttn");
            this.ResBttn.Name = "ResBttn";
            this.ResBttn.Click += new System.EventHandler(this.OnResBttnClicked);
            // 
            // StartBttn
            // 
            resources.ApplyResources(this.StartBttn, "StartBttn");
            this.StartBttn.Name = "StartBttn";
            this.StartBttn.Click += new System.EventHandler(this.OnStartBttnClicked);
            // 
            // EPCLstV
            // 
            resources.ApplyResources(this.EPCLstV, "EPCLstV");
            this.EPCLstV.Name = "EPCLstV";
            this.EPCLstV.View = System.Windows.Forms.View.Details;
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            // 
            // ClrBttn
            // 
            resources.ApplyResources(this.ClrBttn, "ClrBttn");
            this.ClrBttn.Name = "ClrBttn";
            this.ClrBttn.Click += new System.EventHandler(this.OnClrBttnClicked);
            // 
            // TagWrAnyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ClrBttn);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.EPCLstV);
            this.Controls.Add(this.StartBttn);
            this.Controls.Add(this.ResBttn);
            this.Controls.Add(this.DataDispPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagWrAnyForm";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnFormKeyPressed);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.DataDispPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagWrAnyForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.DataDispPanel = new System.Windows.Forms.Panel();
            this.AutoIncBttn = new System.Windows.Forms.CheckBox();
            this.StopAtOneTagBttn = new System.Windows.Forms.CheckBox();
            this.tagWrBnkInput = new OnRamp.TagWrBnkInput();
            this.Bnk3Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk1Bttn = new System.Windows.Forms.RadioButton();
            this.Bnk0Bttn = new System.Windows.Forms.RadioButton();
            this.ResBttn = new System.Windows.Forms.Button();
            this.StartBttn = new System.Windows.Forms.Button();
            this.EPCLstV = new System.Windows.Forms.ListView();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.ClrBttn = new System.Windows.Forms.Button();
            this.DataDispPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataDispPanel
            // 
            this.DataDispPanel.BackColor = System.Drawing.Color.Transparent;
            this.DataDispPanel.Controls.Add(this.AutoIncBttn);
            this.DataDispPanel.Controls.Add(this.StopAtOneTagBttn);
            this.DataDispPanel.Controls.Add(this.tagWrBnkInput);
            this.DataDispPanel.Controls.Add(this.Bnk3Bttn);
            this.DataDispPanel.Controls.Add(this.Bnk1Bttn);
            this.DataDispPanel.Controls.Add(this.Bnk0Bttn);
            resources.ApplyResources(this.DataDispPanel, "DataDispPanel");
            this.DataDispPanel.Name = "DataDispPanel";
            // 
            // AutoIncBttn
            // 
            resources.ApplyResources(this.AutoIncBttn, "AutoIncBttn");
            this.AutoIncBttn.Name = "AutoIncBttn";
            // 
            // StopAtOneTagBttn
            // 
            resources.ApplyResources(this.StopAtOneTagBttn, "StopAtOneTagBttn");
            this.StopAtOneTagBttn.Name = "StopAtOneTagBttn";
            // 
            // tagWrBnkInput
            // 
            resources.ApplyResources(this.tagWrBnkInput, "tagWrBnkInput");
            this.tagWrBnkInput.Name = "tagWrBnkInput";
            // 
            // Bnk3Bttn
            // 
            resources.ApplyResources(this.Bnk3Bttn, "Bnk3Bttn");
            this.Bnk3Bttn.Name = "Bnk3Bttn";
            this.Bnk3Bttn.TabStop = false;
            this.Bnk3Bttn.Click += new System.EventHandler(this.OnBnk3Clicked);
            // 
            // Bnk1Bttn
            // 
            this.Bnk1Bttn.Checked = true;
            resources.ApplyResources(this.Bnk1Bttn, "Bnk1Bttn");
            this.Bnk1Bttn.Name = "Bnk1Bttn";
            this.Bnk1Bttn.Click += new System.EventHandler(this.OnBnk1Clicked);
            // 
            // Bnk0Bttn
            // 
            resources.ApplyResources(this.Bnk0Bttn, "Bnk0Bttn");
            this.Bnk0Bttn.Name = "Bnk0Bttn";
            this.Bnk0Bttn.TabStop = false;
            this.Bnk0Bttn.Click += new System.EventHandler(this.OnBnk0Clicked);
            // 
            // ResBttn
            // 
            resources.ApplyResources(this.ResBttn, "ResBttn");
            this.ResBttn.Name = "ResBttn";
            this.ResBttn.Click += new System.EventHandler(this.OnResBttnClicked);
            // 
            // StartBttn
            // 
            resources.ApplyResources(this.StartBttn, "StartBttn");
            this.StartBttn.Name = "StartBttn";
            this.StartBttn.Click += new System.EventHandler(this.OnStartBttnClicked);
            // 
            // EPCLstV
            // 
            resources.ApplyResources(this.EPCLstV, "EPCLstV");
            this.EPCLstV.Name = "EPCLstV";
            this.EPCLstV.View = System.Windows.Forms.View.Details;
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            // 
            // ClrBttn
            // 
            resources.ApplyResources(this.ClrBttn, "ClrBttn");
            this.ClrBttn.Name = "ClrBttn";
            this.ClrBttn.Click += new System.EventHandler(this.OnClrBttnClicked);
            // 
            // TagWrAnyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ClrBttn);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.EPCLstV);
            this.Controls.Add(this.StartBttn);
            this.Controls.Add(this.ResBttn);
            this.Controls.Add(this.DataDispPanel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagWrAnyForm";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnFormKeyPressed);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.DataDispPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel DataDispPanel;
        private System.Windows.Forms.RadioButton Bnk3Bttn;
        private System.Windows.Forms.RadioButton Bnk1Bttn;
        private System.Windows.Forms.RadioButton Bnk0Bttn;
        private TagWrBnkInput tagWrBnkInput;
        private System.Windows.Forms.CheckBox AutoIncBttn;
        private System.Windows.Forms.CheckBox StopAtOneTagBttn;
        private System.Windows.Forms.Button ResBttn;
        private System.Windows.Forms.Button StartBttn;
        private System.Windows.Forms.ListView EPCLstV;
        private System.Windows.Forms.CheckBox PwdReqChkBx;
        private System.Windows.Forms.Button ClrBttn;
    }
}