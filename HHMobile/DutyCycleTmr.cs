using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Windows.Forms;

namespace OnRamp
{
    class DutyCycleTmr
    {
        private Timer tmr;
        private int onDuration_ms, offDuration_ms;
        public delegate void TmrExpired(Timer tmr);
        
        private TmrExpired onNotify;
        private TmrExpired offNotify;
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="winFormTmr">rely on caller's Windows.Forms.Form to manage the timer</param>
        /// <param name="onDuration">in minutes</param>
        /// <param name="offDuration">in minutes</param>
        public DutyCycleTmr(Timer winFormTmr, int onDuration, int offDuration)
        {
            if (winFormTmr == null)
                throw new ApplicationException("Null Argument");
            this.tmr = winFormTmr;
            this.onDuration_ms = onDuration * 60 * 1000;
            this.offDuration_ms = offDuration * 60 * 1000;

        }

        public void OnCycStart(TmrExpired notify)
        {
            //Datalog.LogStr("DutyCycleTmr: OnCycStart\n");
            onNotify = notify;
            tmr.Interval = onDuration_ms;
            //tmr.Interval = 30 * 1000; // faster debug time
            tmr.Tick += OnOnCycTickEvent;
            tmr.Enabled = true;
        }

        public void OffCycStart(TmrExpired notify)
        {
           // Datalog.LogStr("DutyCycleTmr: OffCycStart\n");
            offNotify = notify;
            tmr.Interval = offDuration_ms;
            //tmr.Interval = 15 * 1000; // faster debug time
            tmr.Tick += OnOffCycTickEvent;
            tmr.Enabled = true;
        }

        
        public void OffCycCancel()
        {
           // Datalog.LogStr("DutyCycleTmr: OffCycCancel\n");
            tmr.Enabled = false;
            tmr.Tick -= OnOffCycTickEvent;
        }

        public void OnCycCancel()
        {
           // Datalog.LogStr("DutyCycleTmr: OnCycCancel\n");
            tmr.Enabled = false;
            tmr.Tick -= OnOnCycTickEvent;
        }


        private void OnOnCycTickEvent(object sender, EventArgs e)
        {
            //Datalog.LogStr("DutyCycleTmr: OnCycTickEvent\n");
            OnCycCancel();

            if (onNotify != null)
                onNotify(tmr);
        }

        private void OnOffCycTickEvent(object sender, EventArgs e)
        {
           // Datalog.LogStr("DutyCycleTmr: OffCycTickEvent\n");
            OffCycCancel();

            if (offNotify != null)
                offNotify(tmr);
        }

    }
}
