using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Data;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsLibBKLogs;
using ClsRampdb;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class TagFSForm : Form
    {

        private String CheckedTag = "";
        private String InTagList = "";
        private Assets SelectedAsset;
        public DataTable dtAsset { get; set; }

        StringBuilder tagids = null;
        Reader Rdr;
        float LocationRSSI;

        #region "sound and meolody variables"
        const int NumThresholdLvls = 8;
        //#if USE_WBRSSI
        //        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
        //            {
        //                0.0F, // within read range
        //                100.0F, // low
        //                100.0F, // 
        //                100.0F,
        //                110.0F, // medium
        //                110.0F,
        //                110.0F, // 
        //                120.0F, // high
        //            };
        //#else
        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
            {
                0.0F, // within read range
                50.0F, // low
                60.0F, // 
                65.0F,
                70.0F, // medium
                75.0F,
                80.0F, // 
                90.0F, // high
            };
        //#endif

        static uint[] PulsePeriods = new uint[NumThresholdLvls]
            {
                1000,
                1000,
                1000,
                600,
                400,
                200,
                100,
                100,
            };

        static uint[] LedRadius = new uint[NumThresholdLvls]
            {
                20,
                20,
                20,
                30,
                40,
                50,
                60,
                60,
            };

        static uint[] BuzzerPitch = new uint[NumThresholdLvls]
            {
                200,
                200,
                200,
                400,
                800,
                1600,
                3200,
                3200,
            };
        static RingTone[] RingMelody = new RingTone[NumThresholdLvls]
            {
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T3,
                RingTone.T4,
                RingTone.T5,
                RingTone.T5,
            };
        #endregion

        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        private TagAccListVHelper ListVHelper;
        private TagAccErrSummary errSummary;

        private void TaskPerformedHandler(String ID_Asset)
        {
            // Remove this item from list.
            DataRow[] drAr;
            foreach (ListViewItem lv in lstAsset.Items)
            {
                if (lv.SubItems[4].Text == ID_Asset)
                {
                    lstAsset.Items.Remove(lv);
                    drAr = dtAsset.Select("ID_Asset='" + ID_Asset + "'");
                    if (drAr.Length > 0)
                    {
                        drAr[0].Delete();
                        dtAsset.AcceptChanges();
                    }
                    lstAsset.Refresh();
                    break;
                }
            }
        }

        public TagFSForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            InitLngLstButtonState();

            InitScanButtonState();

            InitRead1ButtonState();

            InitChkBxState();

            //ListVHelper = new TagAccListVHelper(this.EPCListV);

            errSummary = new TagAccErrSummary();

            Rdr = ReaderFactory.GetReader();

        }

        #region Paging Code

        // 0 -- All, 1 -- Scanned Mode, 2 -- Missed mode
        int mode;
        public int recPerPage;
        int curPage, curPage_Scanned, curPage_Missed;
        DataTable dtPages;

        DataView dvScanned, dvMissed;

        private int SetupPaging(int totalRec)
        {
            //int totalRec = EPCListV.Items.Count;
            int totalPages = 0;

            dtPages = new DataTable();
            DataColumn dc = new DataColumn("Page");
            dtPages.Columns.Add(dc);

            DataColumn dcc = new DataColumn("Index");
            dtPages.Columns.Add(dcc);

            DataRow dr;

            //  cmbPage.Items.Clear();

            if (totalRec > 0)
            {
                totalPages = totalRec / recPerPage;
                if (totalRec % recPerPage != 0)
                    totalPages = totalPages + 1;

                for (int i = 1; i <= totalPages; i++)
                {
                    dr = dtPages.NewRow();
                    dr["Page"] = i.ToString();
                    dr["Index"] = i;
                    dtPages.Rows.Add(dr);
                }
            }
            else
            {
                dr = dtPages.NewRow();
                dr["Page"] = "0";
                dr["Index"] = 0;
                dtPages.Rows.Add(dr);
            }
          
            dtPages.AcceptChanges();

            // cmbPage.DataSource = dtPages;
            // cmbPage.DisplayMember = "Page";
            //  cmbPage.ValueMember = "Index";

            //if (totalPages > 0)
            //cmbPage.SelectedValue = 1;

            // cmbPage.Visible = true;
            //  lblPage.Visible = true;
            //  cmbPage.SelectedValueChanged += new EventHandler(cmbPage_SelectedValueChanged);

            return totalPages;
        }

        private void FillListView()
        {
            lstAsset.Items.Clear();

           
            if (mode == 0)
            {
                FillAll();
            }
            else if (mode == 1)
            {
                FillScanned();
            }
            else if(mode == 2)
            {
                FillMissed();
            }
        }

        private void FillAll()
        {
            int fromRec, toRec;
            fromRec = ((curPage - 1) * recPerPage) + 1;
            toRec = curPage * recPerPage;

            if (dtAsset.Rows.Count <= toRec)
                toRec = dtAsset.Rows.Count;

            DataRow dr;
            btnMissingItems.Tag = ScanStatus.All;
            btnMissingItems.Visible = false;
            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            Int32 iCount = 0; 

            for (int i = fromRec - 1; i < toRec; i++)
            {
                dr = dtAsset.Rows[i];
                iCount++;
                lstItem = new ListViewItem(dr["Name"].ToString());

                //Sub items are retrive by index no. do not change the order.

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["Date_Modified"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                if (dr["Employee"] == DBNull.Value)
                    ls.Text = "";
                else
                    ls.Text = Convert.ToString(dr["Employee"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["TagID"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["AssetNo"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["ID_Asset"].ToString());
                lstItem.SubItems.Add(ls);

                lstAsset.Items.Add(lstItem);

               // dr["ScanStatus"] = ScanStatus.Missing;
              //  dr.AcceptChanges();
            }
            
            lstAsset.Visible = true;
            lstAsset.BringToFront();
            lstAsset.CheckBoxes = false;
            lstAsset.Refresh();

        }

        private void FillScanned()
        {
            int fromRec, toRec;
            fromRec = ((curPage_Scanned - 1) * recPerPage) + 1;
            toRec = curPage_Scanned * recPerPage;

            if (dvScanned.Count <= toRec)
                toRec = dvScanned.Count;

            for (int i = fromRec - 1; i < toRec; i++)
            {
            }
        }

        private void FillMissed()
        {
            int fromRec, toRec;
            fromRec = ((curPage_Missed - 1) * recPerPage) + 1;
            toRec = curPage_Missed * recPerPage;

            if (dvMissed.Count <= toRec)
                toRec = dvMissed.Count;

            for (int i = fromRec - 1; i < toRec; i++)
            {
            }
        }

        #endregion

        #region AddBarCOdeTag
        public void AddListViewItems(ListView.ListViewItemCollection lAr)
        {
            try
            {
                //RFID_18K6C_MEMORY_BANK en = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_EPC;
                //ushort crc = 0;
                //ushort pc = 0;
                //UINT96_T p = new UINT96_T();
                //string s = li.Text.PadRight(24).ToString().Replace("Z", "A");
                //p.ParseString(li.Text.PadLeft(24, '0').ToString().Replace("Z", "A"));
                //string data = "";
                //UInt16 iRSSI = 20;
                //MemBnkRdEventArgs mem = new MemBnkRdEventArgs(ref crc, ref pc, ref p, en, data, iRSSI);
                //MemBnkRdEvtHandler(this, mem);


                //lstAsset.Items.Clear();
                ListViewItem lstItem;
                ListViewItem.ListViewSubItem ls;
                Int16 iCount = Convert.ToInt16(lstAsset.Items.Count);

                Int32 iTag = 0;
                String TgNo;
                Assets Ast;


                foreach (ListViewItem li in lAr)
                {

                    // for (iTag = 0; iTag <= (EPCListV.Items.Count - 1); iTag++)
                    // {
                    iCount++;
                    iTag++;
                    lstItem = new ListViewItem(iCount.ToString());


                    //  TgNo = EPCListV.Items[iTag].SubItems[1].Text;
                    TgNo = li.SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');



                    Ast = new Assets(TgNo);
                    //Doc = new Documents(TgNo);

                    if (Ast.ServerKey != 0)
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString(Ast.Name.ToString());
                        lstItem.SubItems.Add(ls);

                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString(Ast.getLocationName());
                        lstItem.SubItems.Add(ls);
                    }
                    else
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls);

                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls);
                    }

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(TgNo);
                    lstItem.SubItems.Add(ls);

                    lstAsset.Items.Add(lstItem);
                    ////}

                }
                lstAsset.Refresh();

                lstAsset.Visible = true;
                epclistbox.Visible = false;
                ScanButton.Enabled = true;
                ////btnScnBarcd.Enabled = true;
                btnClear.Enabled = true;
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 

            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message); 
            }
        }
        #endregion

        #region CheckBox Routines
        private void InitChkBxState()
        {
            // Set default 'Checked' status according to Availability
            if ((EPCTag.AvailBanks & MemoryBanks4Op.Zero) != 0)
                B0ChkBx.Checked = true;
            else // not available, useless to try
                B0ChkBx.Enabled = false;
            if ((EPCTag.AvailBanks & MemoryBanks4Op.One) != 0)
                B1ChkBx.Checked = true;
            else // not available, useless to try
                B1ChkBx.Enabled = false;
            if ((EPCTag.AvailBanks & MemoryBanks4Op.Two) != 0)
                B2ChkBx.Checked = true;
            else // not available, useless to try
                B2ChkBx.Enabled = false;
            if ((EPCTag.AvailBanks & MemoryBanks4Op.Three) != 0)
                B3ChkBx.Checked = true;
            else // not available, useless to try
                B3ChkBx.Enabled = false;
        }

        private void OnB0ChkBxStateChanged(object sender, EventArgs e)
        {
            switch (B0ChkBx.CheckState)
            {
                case CheckState.Checked:
                    // enable corresponding TextBox (reset content?)
                    B0_1TxtBx.Enabled = true;
                    B0_2TxtBx.Enabled = true;
                    PwdReqChkBx.Visible = true;
                    break;
                case CheckState.Unchecked:
                    // disable corresponding TextBox
                    B0_1TxtBx.Enabled = false;
                    B0_2TxtBx.Enabled = false;
                    PwdReqChkBx.Visible = false;
                    break;
                case CheckState.Indeterminate:
                    throw new ApplicationException("Not supposed to be here");

            }
        }

        private void OnB2ChkBxStateChanged(object sender, EventArgs e)
        {
            switch (B2ChkBx.CheckState)
            {
                case CheckState.Checked:
                    // enable corresponding TextBox (reset content?)
                    B2_1TxtBx.Enabled = true;
                    break;
                case CheckState.Unchecked:
                    // disable corresponding TextBox
                    B2_1TxtBx.Enabled = false;
                    break;
                case CheckState.Indeterminate:
                    throw new ApplicationException("Not supposed to be here");

            }
        }

        private void OnB3ChkBxStateChanged(object sender, EventArgs e)
        {
            switch (B3ChkBx.CheckState)
            {
                case CheckState.Checked:
                    // enable corresponding TextBox (reset content?)
                    B3TxtBx.Enabled = true;
                    break;
                case CheckState.Unchecked:
                    // disable corresponding TextBox
                    B3TxtBx.Enabled = false;
                    break;
                case CheckState.Indeterminate:
                    throw new ApplicationException("Not supposed to be here");
            }
        }

        private MemoryBanks4Op SelectedBanksToEnum()
        {
            MemoryBanks4Op bnks = MemoryBanks4Op.None;

            if (B0ChkBx.Checked)
                bnks |= MemoryBanks4Op.Zero;
            if (B1ChkBx.Checked)
                bnks |= MemoryBanks4Op.One;
            if (B2ChkBx.Checked)
                bnks |= MemoryBanks4Op.Two;
            if (B3ChkBx.Checked)
                bnks |= MemoryBanks4Op.Three;

            return bnks;
        }
        #endregion

        #region LngLstButton Routines
        private void OnLngLstButtonClicked(object sender, EventArgs e)
        {
            bool IsLong;
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagFSForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));
            }

            // State stored in Control's 'Tag' field
            if ((IsLong = ListIsLong()) == true)
            {
                // Restore EPCListV Size
                epclistbox.Size = (System.Drawing.Size)resources.GetObject("epclistbox.Size");
                // Show Panel
                //panel1.Show();
                //DataDispPanel.Show();
            }
            else
            {
                // Hide Panel
                //DataDispPanel.Hide();
                panel1.Hide();
                // Change EPCListV Size
                // EPCListV.Height = panel1.Location.Y + panel1.Size.Height - EPCListV.Location.Y;
                //EPCListV.Height = DataDispPanel.Location.Y + DataDispPanel.Size.Height - EPCListV.Location.Y;
            }
            // update state
            SetLngLstButtonState(!IsLong);
        }

        private void InitLngLstButtonState()
        {
            bool ListIsLong = false;
            this.LngLstButton.Tag = ListIsLong; // boxing : creates an object of type bool in heap
        }

        private bool ListIsLong()
        {
            Boolean IsLong = false;

            if (this.LngLstButton.Tag is Boolean)
            {
                IsLong = (Boolean)this.LngLstButton.Tag;
            }
            else
            {
                throw new ApplicationException("LngLstButton Tag is not Boolean");
            }

            return IsLong;
        }

        private void SetLngLstButtonState(bool Long)
        {
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagFSForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));
            }
            Boolean IsLong = Long;

            // Update Label
            if (Long)
            {
                // Display 'Short'
                this.LngLstButton.Text = "Short";
            }
            else
            {
                // Display original 
                this.LngLstButton.Text = resources.GetString("LngLstButton.Text");
            }
            this.LngLstButton.Tag = IsLong;
        }
        #endregion

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion

        #region Action State variables
        private int fldRdCnt = 0; // TBD: find a better method to store/manage this.
        #endregion

        #region Scan Button Routines
        private void InitScanButtonState()
        {
            OpState State = OpState.Stopped;
            this.ScanButton.Tag = State; // boxing
        }

        private OpState ScanState()
        {
            OpState state = OpState.Stopped;
            if (ScanButton.Tag is OpState)
            {
                state = (OpState)ScanButton.Tag;
            }
            else
            {
                throw new ApplicationException("ScanButton Tag is not OpState");
            }

            return state;
        }

        private void SetScanState(OpState newState)
        {
            OpState CurState = ScanState();
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagFSForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));
            }

            switch (newState)
            {
                case OpState.Starting:
                    // disable scan button
                    ScanButton.Enabled = false;
                    // //btnScnBarcd.Enabled = false;
                    // disable read1 button
                    Read1Button.Enabled = false;

                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    ScanButton.Text = "Stop";
                    ScanButton.Enabled = true;
                    // //btnScnBarcd.Enabled = true;
                    Read1Button.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    ScanButton.Enabled = false;
                    // //btnScnBarcd.Enabled = false;
                    Read1Button.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    ScanButton.Text = resources.GetString("ScanButton.Text");
                    // //btnScnBarcd.Enabled = true;
                    ScanButton.Enabled = true;
                    Read1Button.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }

            ScanButton.Tag = newState;
        }
        public bool isReading { get; set; }
        private void OnScanButtonClicked(object sender, EventArgs e)
        {
            //deepanshu
            
            if (isReading)
            {
                //stop reading
                Rdr.TagInventoryStop();
                isReading = false;
                //btnClear.Enabled = true;
                ScanButton.Enabled = false;

                ////btnScnBarcd.Enabled = false;

                //lstAsset.Items.Clear();


                DataRow[] drAr;
                Int32 iTag;
                String TgNo;

                for (iTag = 0; iTag <= (epclistbox.Items.Count - 1); iTag++)
                {
                    TgNo = epclistbox.Items[iTag].ToString();

                    drAr = dtAsset.Select("TagID='" + TgNo + "'");
                    if (drAr.Length > 0)
                    {
                        drAr[0]["ScanStatus"] = ScanStatus.Scanned;
                        drAr[0].AcceptChanges();
                    }
                }

                filterItemView(ScanStatus.Scanned);

                 

                 lstAsset.CheckBoxes = true;
               // btnMissingItems.Visible = true;

                 lstAsset.Visible = true;
                 lstAsset.Refresh();
                epclistbox.Visible = false;
                ScanButton.Enabled = true;
                // //btnScnBarcd.Enabled = true;
                btnClear.Enabled = true;
                ScanButton.Text = "Scan Tags";
            }
            else
            {
                //start reading
                  lstAsset.Visible = false;
                epclistbox.Visible = true;
                  
                if (Rdr.TagInventoryStart(new Reader.EPCEvent(TagReadEvent)))
                {
                     
                    isReading = true;
                    //btnClear.Enabled = false;
                    ScanButton.Text = "Stop";
                }
                else
                {
                    btnClear.Enabled = true;
                }
            }

            //try
            //{
            //    InTagList = "";
            //    CheckedTag = "";
            //    LocationRSSI = 10000;
            //    // Scan or Stop
            //    //RFIDRdr Rdr = RFIDRdr.GetInstance();
                
            //    //switch (ScanState())
            //    //{
            //    //    case OpState.Stopped:
            //    //        lstAsset.Visible = false;
            //    //        epclistbox.Visible = true;
            //    //       // epclistbox.Items.Clear();
            //    //        //Rdr.InventoryOpStEvent += InvtryOpEvtHandler;
            //    //        //Rdr.DscvrTagEvent += DscvrTagEvtHandler;
                        
            //    //        //Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
            //    //        Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
            //    //        SetScanState(OpState.Starting);
            //    //        Program.RefreshStatusLabel(this, "Starting...");
                        
            //    //        if(tagids == null)
            //    //            tagids = new StringBuilder("'-1'");

            //    //        //Count Status Message
            //    //        if (!Rdr.SetRepeatedTagObsrvMode(true))
            //    //            // Continue despite error
            //    //            Program.ShowError("Disable Repeated Tag Observation mode failed");
            //    //        //UserPref Pref = UserPref.GetInstance();
            //    //        //Byte[] Mask; uint MaskOffset;
            //    //        //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
            //    //        bool Succ = Rdr.TagInventoryStart(5);
            //    //        if (!Succ)
            //    //        {
            //    //            SetScanState(OpState.Stopped);
            //    //            Program.RefreshStatusLabel(this, "Error Stopped");
            //    //            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
            //    //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
            //    //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
            //    //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
            //    //            Program.ShowError("Error: TagInventory Failed to Start\n");
            //    //            Application.DoEvents();
            //    //            btnClear.Enabled = true;

            //    //        }
            //    //        btnClear.Enabled = false;
            //    //        //btnScnBarcd.Enabled = false;
            //    //        break;
            //    //    case OpState.Started:
            //    //        // Stop Tag Read
            //    //        SetScanState(OpState.Stopping);
            //    //        Program.RefreshStatusLabel(this, "Stopping...");
            //    //        if (!Rdr.TagInventoryStop())
            //    //        {
            //    //            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
            //    //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
            //    //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
            //    //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
            //    //            SetScanState(OpState.Stopped); // give up
            //    //            Program.RefreshStatusLabel(this, "Error Stopped...");
            //    //            Program.ShowError("Error: TagRead Failed to Stop\n");
            //    //            btnClear.Enabled = false;
            //    //        }
            //    //        else
            //    //        {
            //    //            ScanButton.Enabled = false;
            //    //            ////btnScnBarcd.Enabled = false;

            //    //            //lstAsset.Items.Clear();


            //    //            DataRow[] drAr;
            //    //            Int32 iTag;
            //    //            String TgNo;

            //    //            for (iTag = 0; iTag <= (epclistbox.Items.Count - 1); iTag++)
            //    //            {
            //    //                TgNo = epclistbox.Items[iTag].ToString();

            //    //                drAr = dtAsset.Select("TagID='" + TgNo + "'");
            //    //                if (drAr.Length > 0)
            //    //                {
            //    //                    drAr[0]["ScanStatus"] = ScanStatus.Scanned;
            //    //                    drAr[0].AcceptChanges();
            //    //                }
            //    //            }

            //    //            filterItemView(ScanStatus.Scanned);

            //    //            //------------ Paging Code ------------

            //    //            //dvScanned = new DataView(dtAsset);
            //    //            //dvScanned.RowFilter = "Where TagID IN (" + tagids.ToString() + ")";

            //    //            //dvMissed = new DataView(dtAsset);
            //    //            //dvMissed.RowFilter = "Where TagID Not IN (" + tagids.ToString() + ")";

            //    //            ////show scanned list view.
            //    //            //lstAsset.CheckBoxes = true;
            //    //            //btnMissingItems.Visible = true;

            //    //            //mode = 1;
            //    //            //SetupPaging(dvScanned.Count);
            //    //            //FillListView();

            //    //            //------------ Paging Code ------------

            //    //            lstAsset.CheckBoxes = true;
            //    //            btnMissingItems.Visible = true;

            //    //            lstAsset.Visible = true;
            //    //            lstAsset.Refresh();
            //    //            epclistbox.Visible = false;
            //    //            ScanButton.Enabled = true;
            //    //            // //btnScnBarcd.Enabled = true;
            //    //            btnClear.Enabled = true;
            //    //        }
            //    //        //btnScnBarcd.Enabled = true;
            //    //        break;
            //    //    case OpState.Starting:
            //    //        // Stop Tag Read
            //    //        SetScanState(OpState.Stopping);
            //    //        Program.RefreshStatusLabel(this, "Stopping...");
            //    //        if (!Rdr.TagInventoryStop())
            //    //        {
            //    //            // Restore
            //    //            SetScanState(OpState.Starting);
            //    //            Program.RefreshStatusLabel(this, "Starting...");
            //    //            Program.ShowWarning("Tag Inventory Stop failed during Starting phase, please try again");
            //    //        }
            //    //        //btnScnBarcd.Enabled = true;
            //    //        break;
            //    //    default:
            //    //        // ignore
            //    //        break;
            //    //}
            //}
            //catch (System.Web.Services.Protocols.SoapException ex)
            //{
            //    if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
            //        Program.ShowError("Request from innvalid IP address.");
            //    else
            //        Program.ShowError("Network Protocol Failure.");
            //    Logger.LogError(ex.Message); 
            //}
            //catch (System.Data.SqlServerCe.SqlCeException sqlex)
            //{
            //    MessageBox.Show("Data File is not able to access.");
            //    Logger.LogError(sqlex.Message); 
            //}
            //catch (System.Net.WebException wex)
            //{
            //    MessageBox.Show("Web exception occured.");
            //    Logger.LogError(wex.Message); 
            //}
            //catch (Exception ep)
            //{
            //    Logger.LogError(ep.Message); 
            //    MessageBox.Show(ep.Message.ToString());
            //}
        }
        #endregion

        #region Read1 Button Routines
        private void InitRead1ButtonState()
        {
            OpState State = OpState.Stopped;
            this.Read1Button.Tag = State; // boxing
        }

        private OpState Read1State()
        {
            OpState state = OpState.Stopped;
            if (Read1Button.Tag is OpState)
            {
                state = (OpState)Read1Button.Tag;
            }
            else
            {
                throw new ApplicationException("Read1Button Tag is not OpState");
            }

            return state;
        }

        private void SetRead1State(OpState newState)
        {
            OpState CurState = Read1State();
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));


            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagFSForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));
            }

            switch (newState)
            {
                case OpState.Starting:
                    // disable read1 button
                    Read1Button.Enabled = false;
                    // disable scan button
                    ScanButton.Enabled = false;
                    ////btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    Read1Button.Text = "Stop";
                    Read1Button.Enabled = true;
                    ScanButton.Enabled = false;
                    ////btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    Read1Button.Enabled = false;
                    ScanButton.Enabled = false;
                    ////btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    Read1Button.Text = resources.GetString("Read1Button.Text");
                    Read1Button.Enabled = true;
                    ScanButton.Enabled = true;
                    // //btnScnBarcd.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }

            Read1Button.Tag = newState;
        }

        private bool GetCurrRowUnreadBnk(out MemoryBanks4Op bnk2Rd, out UINT96_T epc)
        {
            bool Succ = false;
            epc = new UINT96_T();
            bnk2Rd = MemoryBanks4Op.None;
            MemoryBanks4Op UnreadBnks = MemoryBanks4Op.None;
            if ((epclistbox.Items.Count > 0) && epclistbox.SelectedIndex > -1)
            {
                Succ = true;
                ListViewItem SelectedRow = null;// epclistbox.Items[epclistbox.SelectedIndex];
                ListVHelper.GetEPC(SelectedRow, out epc);
                UnreadBnks = RowGetEmptyBanks(SelectedRow)
                    & EPCTag.AvailBanks & SelectedBanksToEnum();
                bnk2Rd = EPCTag.CurBankToAcc(UnreadBnks);
            }
            else
                Succ = false;

            return Succ;
        }

        private void OnRead1ButtonClicked(object sender, EventArgs e)
        {
            // Read1 or Stop
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
           // Reader Rdr = ReaderFactory.GetReader();
            switch (Read1State())
            {
                case OpState.Stopped:
                    // Target EPC to Read (currently selected) 
                    // TBD: strongest RSSI (sorted list?)
                    UINT96_T TgtEPC = new UINT96_T();
                    MemoryBanks4Op NxtBnk2Rd = MemoryBanks4Op.None;
                    if (GetCurrRowUnreadBnk(out NxtBnk2Rd, out TgtEPC))
                    {
                        if (NxtBnk2Rd == MemoryBanks4Op.None)
                        {
                            MessageBox.Show("No Empty Banks to Read");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select a tag from the list first");
                        return;
                    }
                    // Prompt for password if required
                    UInt32 CurAccPasswd = 0;
                    if (PwdReqChkBx.Visible == true && PwdReqChkBx.Checked == true)
                    {
                        if (!PasswdInputDlg.GetPassword(out CurAccPasswd))
                        {
                            // user cancel
                            return;
                        }
                    }
                    //Rdr.RdOpStEvent += new EventHandler<RdOpEventArgs>(TgtRdOpStEventHandler);
                    //Rdr.MemBnkRdEvent += MemBnkRdEvtHandler;
                    Rdr.RegisterRdOpStEvent(TgtRdOpStEventHandler);
                    Rdr.RegisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    SetRead1State(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting..."); // Clear Count Status Message
                    fldRdCnt = 0;
                    BnkRdOffset = 0;
                    errSummary.Clear();
                    UserPref Pref = UserPref.GetInstance();
                    Rdr.TagtEPC = TgtEPC;
                   // Rdr.TagReadBankData += TgtBnkReqCb;
                    Rdr.RegisterTagReadBankDataEvent(TgtBnkReqCb);
                    Rdr.CurAccPasswd = CurAccPasswd;
                    if (!Rdr.TagReadBanksStart())
                    {
                        SetRead1State(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        MessageBox.Show("Error: TagRead Failed to Start\n");
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    }
                    break;
                case OpState.Started:
                    // Stop Tag Read
                    SetRead1State(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (!Rdr.TagReadStop())
                    {
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                        MessageBox.Show("Error: TagRead Failed to Stop\n");
                        SetRead1State(OpState.Stopped); // give up
                        Program.RefreshStatusLabel(this, "Error Stopped...");
                    }
                    break;
                default:
                    // ignore
                    break;
            }
        }


        #endregion

        #region EPCListV Routines

        //
        // Helper function for RowGetEmptyBanks
        //
        private void CheckAndMarkEmptyBank(String[] memBnks, int bIdx,
            MemoryBanks4Op bnkToChk, ref MemoryBanks4Op markBnks)
        {
            if (memBnks[bIdx] == null || memBnks[bIdx].Length == 0)
                markBnks |= bnkToChk;
        }

        private MemoryBanks4Op RowGetEmptyBanks(ListViewItem item)
        {
            MemoryBanks4Op EmptyBanks = MemoryBanks4Op.None;
            String[] MemBanks = ListVHelper.GetBnkData(item);
            CheckAndMarkEmptyBank(MemBanks, 0, MemoryBanks4Op.Zero,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 1, MemoryBanks4Op.One,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 2, MemoryBanks4Op.Two,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 3, MemoryBanks4Op.Three,
                ref EmptyBanks);
            return EmptyBanks;
        }

        private void OnEPCListVSelChanged(object sender, EventArgs e)
        {
            if (AnyOperationRunning() || epclistbox.SelectedIndex  <= -1)
            {
                DataDispPanel.Enabled = false;
                Read1Button.Enabled = false;
                return;
            }

            if (epclistbox.SelectedIndex > -1)
            {
                //LoadTxtBxes(epclistbox.Items[epclistbox.SelectedIndex]);
            }

            Read1Button.Enabled = true;
            DataDispPanel.Enabled = true;
        }

        private void RefreshTagCntLabel()
        {
            //TagCntLabel.Text = EPCListV.Items.Count.ToString();
            TagCntLabel.Text = epclistbox.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            //TagCntLabel.Refresh();
        }
        #endregion

        #region TextBoxes Routines
        private void LoadTxtBxes(ListViewItem row)
        {
            String[] MemBnks = ListVHelper.GetBnkData(row);
            // Display Tag Data at the TextFields
            TxtBxesDpyData(MemBnks);
        }

        private void TxtBxesDpyData(String[] MemBnks)
        {
            // Bank 0 (Reserved) // Kill and Access Passwords
            B0_1TxtBx.Text = (MemBnks[0] != null) ? MemBnks[0].Substring(0, 8) : "";
            B0_2TxtBx.Text = (MemBnks[0] != null) ? MemBnks[0].Substring(8, 8) : "";

            // Bank 1 (EPC + PC)
            // CRC(2x2) + PC(2x2) + EPC(...)
            B1_1TxtBx.Text = (MemBnks[1] != null) ? MemBnks[1].Substring(4, 4) : ""; // PC
            B1_2TxtBx.Text = (MemBnks[1] != null) ? MemBnks[1].Substring(8) : ""; //EPC

            // Bank2 (TID)
            B2_1TxtBx.Text = (MemBnks[2] != null) ? MemBnks[2].Substring(0, 2) : ""; // Allocation Class Identifier
            B2_2TxtBx.Text = (MemBnks[2] != null) ? MemBnks[2].Substring(2, 6) : ""; // Class-specific attributes
            B2_3TxtBx.Text = (MemBnks[2] != null) ? MemBnks[2].Substring(8) : ""; // Tag/Vendor-specific

            // Bank3 (User)
            B3TxtBx.Text = (MemBnks[3] != null) ? MemBnks[3] : "";

            //Retrive Tag Info from database.
            setTagInfo(B1_2TxtBx.Text);

        }

        private void setTagInfo(String TagID)
        {
            try
            {
                //Bkey
                SelectedAsset = new Assets(TagID);
                txtAsset.Text = SelectedAsset.Name;
                txtTag.Text = TagID;
                txtLoc.Text = SelectedAsset.getLocationName();
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {                 
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message); 
            }
        }
        #endregion

        #region ScanRead Event Handlers
        //private void ScanRdOpStEventHandler(object sender, RdOpEventArgs e)
        //{
        //    //RFIDRdr Rdr = RFIDRdr.GetInstance();
        //    Reader Rdr = ReaderFactory.GetReader();
        //    switch (e.Status)
        //    {
        //        case RdOpStatus.started:
        //            SetScanState(OpState.Started);
        //            // Make the List Long (purpose of scanning)
        //            if (!ListIsLong())
        //                OnLngLstButtonClicked(this, null);
        //            Program.RefreshStatusLabel(this, "Scanning...");
        //            break;
        //        case RdOpStatus.error:
        //            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
        //                MessageBox.Show("Error: " + e.ErrMsg);
        //            break;
        //        case RdOpStatus.tagAccError:
        //            errSummary.Rec(e.TagAccErr);
        //            break;
        //        case RdOpStatus.errorStopped:
        //        case RdOpStatus.stopped:
        //            //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
        //            //Rdr.RdOpStEvent -= ScanRdOpStEventHandler;
                   
        //            Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
        //            Rdr.UnregisterRdOpStEvent(ScanRdOpStEventHandler);
        //            SetScanState(OpState.Stopped);
        //            if (fldRdCnt > 0)
        //            {
        //                //if (EPCListV.SelectedIndices == null || EPCListV.SelectedIndices.Count == 0)
        //                //{
        //                //    EPCListV.Items[0].Selected = true; // select and focus on the first one
        //                //    EPCListV.Focus();
        //                //}
        //                Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " new Tags");
        //            }
        //            else
        //                Program.RefreshStatusLabel(this, "Stopped: 0 new Tags");
        //            if (ListIsLong())
        //                OnLngLstButtonClicked(this, null);
        //            if (e.Status == RdOpStatus.errorStopped)
        //            {
        //                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
        //                    RestartRfidDevice();
        //                else
        //                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
        //                        MessageBox.Show("Error: " + e.ErrMsg);
        //            }
        //            break;
        //    }
        //}

        private void MemBnkRdEvtHandler(object sender, MemBnkRdEventArgs e)
        {
            bool FldAdded;
            // Debug: Add/Update Tag to EPCListV 
            // Save/Update Assciated Data with Row
            ListViewItem Item = ListVHelper.AddEPC(e.CRC, e.PC, e.EPC);
            RefreshTagCntLabel();
            // associate bank data with row
            if (e.BankNum == MemoryBanks4Op.One)
                FldAdded = false; // ignore this bank
            else
            {
                if (BnkRdBuf == null)
                    BnkRdBuf = new StringBuilder();
                if (BnkRdOffset == 0 && BnkRdBuf.Length > 0)
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                BnkRdBuf = BnkRdBuf.Append(e.Data);
                BnkRdOffset += e.Data.Length / 4; // 4 hex chars per Word
                // if full-bank is read, store to list and reset the buffer
                if ((BnkRdBuf.Length / 4) == EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(e.BankNum)))
                {
                    FldAdded = ListVHelper.StoreData(Item, e.BankNum, BnkRdBuf.ToString());
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                    BnkRdOffset = 0;
                }
                else
                {
                    FldAdded = false;
                }
            }
            if (FldAdded)
            {
                fldRdCnt++;
                Program.RefreshStatusLabel(this, "Fields Added : " + fldRdCnt);
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            }

            // Update Text Fields if applicable
            if (epclistbox.SelectedIndex > -1)
            {
                if (Item.Index == epclistbox.SelectedIndex)
                {
                    LoadTxtBxes(Item);
                }
            }
        }

        private bool Bnk2ReqCb(out MemoryBanks4Op bnks2Rd)
        {
            bnks2Rd = MemoryBanks4Op.Two;
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (ScanState() == OpState.Stopping || ScanState() == OpState.Stopped)
                return false;
            // Should we continue if invalid address error / too many errors occurs?
            return true; // until user stops
        }
        #endregion

        #region Tag Inventory Event Handler

        //private TagRangeList tagList = new TagRangeList();

        private void UpdateTagRead(string[] e)
        {
            try
            {
                if (!epclistbox.Items.Contains(e[0]))
                {
                    epclistbox.Items.Insert(0, e[0]);
                    RefreshTagCntLabel();
                }

                //if (InTagList.IndexOf(e.EPC.ToString()) <= 0)
                //{

                //    InTagList += "," + e.EPC.ToString();
                //    TagInfo T = new TagInfo(e.EPC.ToString());
                //    if (T.isAssetTag())
                //    {
                        
                //        if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
                //        {
                //            // New Tag (Note: missing CRC data)
                //            // CheckedTag += "," + e.EPC.ToString();
                //            //ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC);

                //            AddEPCToListV(e.PC, e.EPC);
                //            //tagids.Append("," + "'" + e.EPC + "'");
                //            //ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC,nm,loc);
                            

                //            //TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                //        }

                //    }
                //    else
                //    {
                //        //TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                //    }
                //}
            }
            catch { }
        }
        
        private void TagReadEvent(string epc, string rssi)
        {
            //TimeSpan etime = DateTime.Now.TimeOfDay;
            //double ms = (etime - stime).TotalMilliseconds;
            //long ttick = (etime - stime).Ticks;
            //this.BeginInvoke(new Action<string[]>(UpdateTagRead), new string[]{epc,rssi});
            try
            {
                this.BeginInvoke(new Action<string>((string e) =>
                {
                    if (!epclistbox.Items.Contains(epc))
                        epclistbox.Items.Insert(0, epc);
                    TagCntLabel.Text = epclistbox.Items.Count.ToString();
                }), epc);
                
            }
            catch { }
             
        }
        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            //Deepanshu
            // Add new TagID to list (or update occurrence)
            if (e.Cnt != 0)
            {
                this.BeginInvoke( new Action<string[]>(UpdateTagRead),new string[]{e.EPC.ToString(),e.RSSI.ToString()});
               
            }
        }

        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState State = ScanState();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
           // Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    SetScanState(OpState.Started);
                    // Make the List Long (purpose of scanning)
                    //if (!ListIsLong())
                    //    OnLngLstButtonClicked(this, null);
                    //Program.RefreshStatusLabel(this, "Scanning...");
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    {
                        //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                       
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        if (e.status == InvtryOpStatus.errorStopped)
                        {
                            // Display error or Restart Radio (if necessary)
                            if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                                RestartRfidDevice();
                            else
                                Program.ShowError("Tag Inventory Stopped with Error: " + e.msg);
                        }
                        else if (e.status == InvtryOpStatus.macErrorStopped)
                        {
                            //ushort MacErrCode;

                            if (Rdr.GetMacError() && Reader.macerr != 0)
                            {
                                // if fatal mac error, display message
                                if (Rdr.MacErrorIsFatal() || (ScanState() != OpState.Stopping))
                                    Program.ShowError(e.msg);
                            }
                            else
                                Program.ShowError("Unknown HW error. Abort");
                        }
                        // restore StartButton
                        SetScanState(OpState.Stopped);
                        //Program.RefreshStatusLabel(this, "Finished");
                        //if (ListIsLong())
                        //    OnLngLstButtonClicked(this, null);
                    }
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton

                    switch (State)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                             
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            {
                                MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                                //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                                //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                                 
                                Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                                Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            }
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    SetScanState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
            }
            // Re-enable Field Input Textboxes (disabled during inventory operation)
            if (ScanState() == OpState.Stopped)
            {
                //if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
                //{
                //   // ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                //   // LoadTxtBxes(row);
                //    // Enable (Avail) Text Fields
                //   // DataDispPanel.Enabled = true;
                //}
            }
        }

        //private void AddEPCToListV(UInt16 PC, UINT96_T EPC)
        //{
        //    // Assuming that the Tag of this EPC is not already in list
        //    int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
        //    ListViewItem item = new ListViewItem(new String[] {
        //         (CurNumRows+1).ToString(), EPC.ToString() });
        //    // Insert new item to the top of the list for easy viewing
        //    EPCListV.Items.Insert(0, item);
        //    item.Tag = PC;
        //    // EPCListV.Refresh();
        //    // RefreshTagCntLabel();
        //}

        #endregion

        #region TgtRead Event Handlers
        private void TgtRdOpStEventHandler(object sender, RdOpEventArgs e)
        {

           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case RdOpStatus.started:
                    SetRead1State(OpState.Started);
                    UINT96_T EPC;
                    MemoryBanks4Op CurEmptyBnk;
                    GetCurrRowUnreadBnk(out CurEmptyBnk, out EPC);
                    Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(CurEmptyBnk) + "." + BnkRdOffset);
                    break;
                case RdOpStatus.completed:
                    break;
                case RdOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case RdOpStatus.tagAccError:
                    // Add error to summary
                    errSummary.Rec(e.TagAccErr);
                    // display error immediately upon Unrecoverable Errors
                    switch (e.TagAccErr)
                    {
                        case AccErrorTypes.Unauthorized:
                            MessageBox.Show("Password Required", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.AccessPasswordError:
                            MessageBox.Show("Incorrect Bank Access Password", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.InvalidAddr:
                            MessageBox.Show("Invalid Addr: Requested bank does not exist or Bank Size exceeds available memory",
                                "Tag Bank Read Failed");
                            break;
                    }
                    break;
                case RdOpStatus.stopped:
                case RdOpStatus.errorStopped:
                    //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                    //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                    Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                    SetRead1State(OpState.Stopped);
                    if (fldRdCnt > 0)
                        Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " Banks");
                    else
                        Program.RefreshStatusLabel(this, "Stopped:  " + fldRdCnt + " Banks");
                    if (e.Status == RdOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
            }
        }

        StringBuilder BnkRdBuf = null;
        int BnkRdOffset = 0;

        private ushort OptNumWdsToRd(MemoryBanks4Op bnk2Rd, int wdOffset)
        {
            ushort MaxNumWds = EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(bnk2Rd));
            return (ushort)Math.Min(4, MaxNumWds - wdOffset);
        }

        private bool TgtBnkReqCb(out MemoryBanks4Op bnk2Rd, out ushort wdOffset, out ushort wdCnt)
        {
            bnk2Rd = MemoryBanks4Op.None;
            wdOffset = 0;
            wdCnt = 0;

            // if unrecoverable error from last read (such as password), stop
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (Read1State() == OpState.Stopping || Read1State() == OpState.Stopped)
                return false;
            UINT96_T TgtEPC;
            if (GetCurrRowUnreadBnk(out bnk2Rd, out TgtEPC) == false)
                return false;
            bool MoreBnks2Rd = (bnk2Rd != MemoryBanks4Op.None);
            if (MoreBnks2Rd)
            {
                wdOffset = (ushort)BnkRdOffset;
                wdCnt = OptNumWdsToRd(bnk2Rd, wdOffset);
                Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(bnk2Rd)
                    + "." + wdOffset);
            }
            return MoreBnks2Rd;
        }
        #endregion

        private bool AnyOperationRunning()
        {
            return isReading;
        }

        private void OnTagFSFormClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = false;
            if (AnyOperationRunning())
            {
                MessageBox.Show("Please stop the scan operation first");
                e.Cancel = true;
            }
            else
            {
                Task.taskPerformed -= new Task.TaskPerformedNotify(TaskPerformedHandler);
            }

            // TBD: Ask for confirmation here
        }

        private void OnTagFSFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }

        private void OnTagFSFormLoad(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            Task.taskPerformed += new Task.TaskPerformedNotify(TaskPerformedHandler);

            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }

            InitVariables();

            //------------ Paging Code ------------
            //curPage = 1;
            //curPage_Scanned = 1;
            //curPage_Missed = 1;

            //mode = 0;
            //SetupPaging(dtAsset.Rows.Count);
            //FillListView();

            //------------ Paging Code ------------

            Cursor.Current = Cursors.Default;
            // Scan Rd
            //this.OnScanButtonClicked(this, null);
        }

        #region F11/F4/F5 Hotkey
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11: // no need to support auto-fire because ScanRd is
                 case eVKey.VK_F19:
                    //HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    //if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    //{
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if (!isReading)
                            {
                                OnScanButtonClicked(this, null);
                            }
                             
                        }
                        else // up
                        {
                            // Stop running Scan-Rd Op
                            if (isReading)
                            {
                                OnScanButtonClicked(this, null);
                            }
                            
                        }
                    //}
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (!AnyOperationRunning())
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && !AnyOperationRunning())
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
               // case Keys.F7:
                //case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (AnyOperationRunning())
                    {
                        MessageBox.Show("Please stop the scan operation first");                        
                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }


        #endregion

        private void OnClrLstBttnClicked(object sender, EventArgs e)
        {
            //btnSave.Enabled = false;

            //EPCListV.BeginUpdate();

            //if (EPCListV.Items != null)
            //    EPCListV.Items.Clear();

            //EPCListV.EndUpdate();

            //tagList.Clear();

            RefreshTagCntLabel();

            // lstAsset.Items.Clear();
            // No need to do any thing in TagAccListVHelper
        }

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        private void InitVariables()
        {
            btnMissingItems.Tag = ScanStatus.All;
            TagCntLabel.Visible=true;
            btnMissingItems.Visible = false;
            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            Int32 iCount = 0;
            //dtAsset = Assets.getAssetsByLocation(Convert.ToInt32(cboLoc.SelectedValue));
            //dtAsset.Columns.Add("ScanStatus", Type.GetType("System.Int16"));
            //dtAsset.AcceptChanges();  

            foreach (DataRow dr in dtAsset.Rows)
            {
                iCount++;
                lstItem = new ListViewItem(dr["Name"].ToString());

                //Sub items are retrive by index no. do not change the order.

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["Date_Modified"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                if (dr["Employee"] == DBNull.Value)
                    ls.Text = "";
                else
                    ls.Text = Convert.ToString(dr["Employee"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["TagID"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["AssetNo"].ToString());
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Convert.ToString(dr["ID_Asset"].ToString());
                lstItem.SubItems.Add(ls);

                lstAsset.Items.Add(lstItem);

                dr["ScanStatus"] = ScanStatus.Missing;
                dr.AcceptChanges();
            }
            //lstAsset.Visible = true;
            //lstAsset.BringToFront();
            //lstAsset.CheckBoxes = false;
            //lstAsset.Refresh();

        }

        private void filterItemView(ScanStatus st)
        {
            Cursor.Current = Cursors.WaitCursor;  
            btnMissingItems.Tag = st;
            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            lstAsset.Items.Clear();
            foreach (DataRow dr in dtAsset.Rows)
            {
                if (st == ScanStatus.All || (Convert.ToInt32(st) == Convert.ToInt32(dr["ScanStatus"])))
                {
                    lstItem = new ListViewItem(dr["Name"].ToString());

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(dr["Date_Modified"].ToString());
                    lstItem.SubItems.Add(ls);

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(dr["Employee"].ToString());
                    lstItem.SubItems.Add(ls);

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(dr["TagID"].ToString());
                    lstItem.SubItems.Add(ls);

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(dr["AssetNo"].ToString());
                    lstItem.SubItems.Add(ls);

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(dr["ID_Asset"].ToString());
                    lstItem.SubItems.Add(ls);

                    if (st == ScanStatus.Scanned)
                        lstItem.Checked = true;
                    else
                        lstItem.Checked = false;

                    lstAsset.Items.Add(lstItem);
                }
            }
            lstAsset.Refresh();
            Cursor.Current = Cursors.Default; 
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtLoc_TextChanged(object sender, EventArgs e)
        {

        }

        private void PwdReqChkBx_CheckStateChanged(object sender, EventArgs e)
        {

        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //tagList.Clear();
            //EPCListV.Items.Clear();
            lstAsset.Items.Clear();
            //EPCListV.Visible = true;
            epclistbox.Items.Clear();
            RefreshTagCntLabel();
            //epclistbox.Visible = false;
        }

        private void btnMissingItems_Click(object sender, EventArgs e)
        {
            if (btnMissingItems.Tag.ToString() == ScanStatus.Missing.ToString())
            {
                btnMissingItems.Text = "Missing Items";
                filterItemView(ScanStatus.Scanned);
        
                //-------------- Paging Code -----------------
               // mode = 1;
               // SetupPaging(dvScanned.Count);
               // FillListView();
                //-------------- Paging Code -----------------

            }
            else
            {
                btnMissingItems.Text = "Scanned Items";
               filterItemView(ScanStatus.Missing);

                //-------------- Paging Code -----------------
               // mode = 2;
              //  SetupPaging(dvMissed.Count);
              //  FillListView();
                //-------------- Paging Code -----------------
            }
        }

        public void ItemSelected()
        {
            Assets Ast;
            Ast = null;
            if (lstAsset.SelectedIndices.Count == 1)
            {
                Ast = new Assets(lstAsset.Items[lstAsset.SelectedIndices[0]].SubItems[3].Text);
            }
            else
            {
                MessageBox.Show("Please select a single item.");
                return;
            }

            if (btnMissingItems.Tag.ToString() == ScanStatus.Missing.ToString())
            {
                //Go to search item
                // Open new Tag Search Window
                TagSearchForm TagSrchFm = new TagSearchForm();
                TagSrchFm.SearchTagNo = Ast.TagID;
                TagSrchFm.AssetName = Ast.Name;
                TagSrchFm.AssetNo = Ast.AssetNo;
                TagSrchFm.Show();
                this.Enabled = false;
                TagSrchFm.Closed += new EventHandler(this.OnTagFSFormClosed);
            }
            else if (btnMissingItems.Tag.ToString() == ScanStatus.Scanned.ToString())
            {
                //Go To task performed page.
                frmPerformTask frm = new frmPerformTask();
                frm.ItemID = Ast.ServerKey;
                frm.AssetNo = Ast.AssetNo;
                frm.AssetName = Ast.Name;
                frm.Show();
                // disable form until this new form closed
                this.Enabled = false;
                frm.Closed += new EventHandler(this.OnTagFSFormClosed);
            }
            else
            {
                //Nothing
            }
        }

        private void lstAsset_ItemActivate(object sender, EventArgs e)
        {
            ItemSelected();
        }

        private void lstAsset_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //Added to generate Sound and Melody
        #region Search-by-RSSI Routines
        // Relies on Tag Inventory
        //private void TagRssiInvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        //{
        //    OpState CurState = SrchBttnState();
        //    RFIDRdr Rdr = RFIDRdr.GetInstance();

        //    switch (e.status)
        //    {
        //        case InvtryOpStatus.started:
        //            Program.RefreshStatusLabel(this, "Searching...");
        //            SrchBttnSetState(OpState.Running);
        //            break;
        //        case InvtryOpStatus.stopped:
        //        case InvtryOpStatus.errorStopped:
        //        case InvtryOpStatus.macErrorStopped:
        //            StopOORDetectTmr();
        //            RssiLbl.Text = String.Empty;
        //            Rdr.TagRssiEvent -= TagRssiEvtHandler;
        //            Rdr.InvtryOpStEvent -= TagRssiInvtryOpEvtHandler;
        //            if (e.status == InvtryOpStatus.errorStopped)
        //            {
        //                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
        //                    RestartRfidDevice();
        //                else
        //                    MessageBox.Show(e.msg, "Tag Search Stopped with Error");
        //            }
        //            else if (e.status == InvtryOpStatus.macErrorStopped)
        //            {
        //                ushort MacErrCode;
        //                if (Rdr.GetMacError(out MacErrCode) && MacErrCode != 0)
        //                {
        //                    // Show error if fatal
        //                    if (RFIDRdr.MacErrorIsFatal(MacErrCode))
        //                    {
        //                        Program.ShowError(e.msg);
        //                    }
        //                    // Show message if non-fatal and non-user-stop
        //                    else if (SrchBttnState() != OpState.Stopping)
        //                    {
        //                        Program.ShowError(e.msg);
        //                    }
        //                }
        //                else
        //                    Program.ShowError("Unknown hardware (mac) error");
        //            }
        //            // restore Search Button
        //            SrchBttnSetState(OpState.Idle);
        //            Program.RefreshStatusLabel(this, "Ready");
        //            break;
        //        case InvtryOpStatus.error:
        //            switch (CurState)
        //            {
        //                case OpState.Starting:
        //                    MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
        //                    Rdr.TagRssiEvent -= TagRssiEvtHandler;
        //                    Rdr.InvtryOpStEvent -= TagRssiInvtryOpEvtHandler;
        //                    break;
        //                case OpState.Running:
        //                    // TBD
        //                    MessageBox.Show(e.msg, "Tag Search Error");
        //                    break;
        //                case OpState.Stopping:
        //                    MessageBox.Show(e.msg, "Tag Search Stop Error");
        //                    Rdr.TagRssiEvent -= TagRssiEvtHandler;
        //                    Rdr.InvtryOpStEvent -= TagRssiInvtryOpEvtHandler;
        //                    break;
        //                case OpState.Idle:
        //                    throw new ApplicationException("Unexpected error return during Idle State");
        //            }
        //            SrchBttnSetState(OpState.Idle);
        //            Program.RefreshStatusLabel(this, "Error Stopped");
        //            break;
        //    }
        //}

        // RSSI information updates
        private void RssiMelodyGenerate(Double RSSIval)
        {
            try
            {

                for (int i = (NumThresholdLvls - 1); i >= 1; i--)
                {
                    if (RSSIval >= RssiLvlThresholds[i])
                    {

                        //FlashBorder();
                        //#if DEBUG
                        //                      RFIDRdr.GetInstance().MelodyRing(RingMelody[i], UserPref.GetInstance().SndVol);
                        //#else
                        Reader rdr = ReaderFactory.GetReader();
                        rdr.RingingID = RingMelody[i];
                        rdr.Svol = SoundVol.High;
                        rdr.MelodyRing();
                        //RFIDRdr.GetInstance().MelodyRing(RingMelody[i], SoundVol.High);
                        //#endif
                        break;
                    }
                }

                //if (RSSIval < RssiLvlThresholds[0])
                //    RssiLbl.Text = String.Empty;

                //else
                //    RestartOORDetectTmr();


                //#if false // Testing Only
                //                if (firstTag)
                //                {
                //                    RFIDRdr.GetInstance().TagInvtryRssiStop();
                //                    firstTag = false;
                //                }
                //#endif
            }
            catch (ArithmeticException)
            {
                //RssiLbl.Text = "Arithmetic Exception caught";
                //RssiLbl.Refresh(); // must show to the user immediately
            }
            catch
            {
                //RssiLbl.Text = "Exception caught";
                //RssiLbl.Refresh(); // must show to the user immediately
            }
        }

        #endregion

        
    }

    //public class TagAccErrSummary
    //{
    //    private int[] tagTAErrs;

    //    public TagAccErrSummary()
    //    {
    //        tagTAErrs = new int[(int)AccErrorTypes.totalNumDefined];
    //    }

    //    public void Clear()
    //    {
    //        Array.Clear(tagTAErrs, 0, tagTAErrs.Length);
    //    }

    //    public void DspyErrSummary()
    //    {
    //        StringBuilder Sb = new StringBuilder();
    //        for (int i = 0; i < tagTAErrs.Length; i++)
    //        {
    //            if (tagTAErrs[i] > 0)
    //            {
    //                Sb.Append(((AccErrorTypes)i).ToString("F") + ": " + tagTAErrs[i].ToString() + "\n");
    //            }
    //        }
    //        if (Sb.Length > 0)
    //            MessageBox.Show(Sb.ToString(), "Tag Access Error Summary");
    //        else
    //            Program.ShowWarning("No Tag Access Error recorded");
    //    }

    //    public void Rec(AccErrorTypes err)
    //    {
    //        (tagTAErrs[(int)err])++;
    //    }

    //    public bool UnrecoverableErrOccurred
    //    {
    //        get
    //        {
    //            return (tagTAErrs[(int)AccErrorTypes.InvalidAddr] > 0)
    //                    || (tagTAErrs[(int)AccErrorTypes.Unauthorized] > 0)
    //                    || (tagTAErrs[(int)AccErrorTypes.AccessPasswordError] > 0);
    //        }
    //    }

    //    public bool IsUnrecoverable(AccErrorTypes err)
    //    {
    //        return (err == AccErrorTypes.InvalidAddr)
    //            || (err == AccErrorTypes.Unauthorized)
    //            || (err == AccErrorTypes.AccessPasswordError);
    //    }

    //    public bool IsWriteVerifyErrors(AccErrorTypes err)
    //    {
    //        return (err == AccErrorTypes.WriteResponseCrcError)
    //            || (err == AccErrorTypes.WriteVerifyCrcError)
    //            || (err == AccErrorTypes.WriteRetryCountExceeded);
    //    }

    //    public bool IsAnyWriteError(AccErrorTypes err)
    //    {
    //        return (err == AccErrorTypes.WriteResponseCrcError)
    //          || (err == AccErrorTypes.WriteVerifyCrcError)
    //          || (err == AccErrorTypes.WriteRetryCountExceeded)
    //        || (err == AccErrorTypes.WriteError)
    //        || (err == AccErrorTypes.WriteCmdError);
    //    }

    //    public bool WriteVerifyWarningsOccurred
    //    {
    //        get
    //        {
    //            return (tagTAErrs[(int)AccErrorTypes.WriteResponseCrcError] > 0)
    //                || (tagTAErrs[(int)AccErrorTypes.WriteVerifyCrcError] > 0)
    //                || (tagTAErrs[(int)AccErrorTypes.WriteRetryCountExceeded] > 0);
    //        }
    //    }


    //}
}