/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Fieldservice functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmSelectItems : Form
    {
        DataTable dtAsset, dtGroups,dtListAssets;

        TransientMsgDlg msgbox = null;
        public int SelectedLocationID = 0, SelectedGroupID = 0, SelectedSubGroupID = 0;

        public frmSelectItems()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void frmSelectItems_Load(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            CreateTablesSchemas();

            lnkGroups.Text = "All Groups";
            lnkSubGroups.Text = "All Sub Groups";
            lnkLocName.Text = "All Locations";

            lnkGroups.Tag = "All Groups";
            lnkSubGroups.Tag = "All Sub Groups";
            lnkLocName.Tag = "All Locations";

            #region"Set Location Combo"
           // cboLoc.Enabled = false;
           // DataTable dtList = new DataTable();
           // dtList = FS_AssetStatus.getLocationList();

           //// dtList.Merge(dtLocations, true, MissingSchemaAction.Add); 

           // DataRow dr = dtList.NewRow();
           // dr["ID_Location"] = -2;
           // dr["Name"] = "--SELECT--";
           // dtList.Rows.Add(dr);
           // dtList.AcceptChanges();

           // dr = dtList.NewRow();
           // dr["ID_Location"] = 0;
           // dr["Name"] = "--ALL--";
           // dtList.Rows.Add(dr);

           // dtList.AcceptChanges();

           // cboLoc.ValueMember = "ID_Location";
           // cboLoc.DisplayMember = "Name";
           // cboLoc.DataSource = dtList;

           // cboLoc.SelectedValue = -2;
           // cboLoc.Enabled = true;
           #endregion

          #region"Set Parent Group"
           // cboGroup.Enabled = false;
           // DataTable dtGList = new DataTable();
           // dtGList = FS_AssetStatus.getGroups(0);        

           // DataRow drG = dtGList.NewRow();
           // drG["ID_AssetGroup"] = -2;
           // drG["Name"] = "--SELECT--";
           // dtGList.Rows.Add(drG);
           // dtGList.AcceptChanges();

           // drG = dtGList.NewRow();
           // drG["ID_AssetGroup"] = 0;
           // drG["Name"] = "--ALL--";
           // dtGList.Rows.Add(drG);
           // dtGList.AcceptChanges();

           // cboGroup.ValueMember = "ID_AssetGroup";
           // cboGroup.DisplayMember = "Name";
           // cboGroup.DataSource = dtGList;

           // cboGroup.SelectedValue = -2;
           // cboGroup.Enabled = true;

          #endregion

        #region "set child Groups"
           // cboSubGroup.Enabled = false;
           // //DataTable dtGSList = new DataTable();
           // //dtGSList = FS_AssetStatus.getGroups(-3);

           //// dtGSList.Merge(dtGroups, true, MissingSchemaAction.Add);
           //// dtGSList.AcceptChanges();

           // //DataRow drGS = dtGSList.NewRow();
           // //drGS["ID_AssetGroup"] = -2;
           // //drGS["Name"] = "--SELECT--";
           // //dtGSList.Rows.Add(drGS);
           // //dtGSList.AcceptChanges();

           // //drGS = dtGSList.NewRow();
           // //drGS["ID_AssetGroup"] = 0;
           // //drGS["Name"] = "--ALL--";
           // //dtGSList.Rows.Add(drGS);
           // //dtGSList.AcceptChanges();

           // cboSubGroup.ValueMember = "ID_AssetGroup";
           // cboSubGroup.DisplayMember = "Name";
           // cboSubGroup.DataSource = dtGroups;

           // cboSubGroup.SelectedValue = 0;
           // cboSubGroup.Enabled = true;
           #endregion

            #region"Set Item List"
            //cboItem.Enabled = false;
            //DataTable dtItemList = new DataTable();
            //dtItemList = FS_AssetStatus.getAssets(0, 0, 0); // No data come

            ////dtItemList.Merge(dtAsset, true, MissingSchemaAction.Add);
            ////dtItemList.AcceptChanges();

            ////dtAsset = dtItemList.Copy();
            ////dtAsset.Columns.Add("ScanStatus", Type.GetType("System.Int16"));
            ////dtAsset.AcceptChanges();

            ////DataRow drItem = dtItemList.NewRow();
            ////drItem["ID_Asset"] = 0;
            ////drItem["Name"] = "--ALL--";
            ////dtItemList.Rows.Add(drItem);
            ////dtItemList.AcceptChanges();

            ////drItem = dtItemList.NewRow();
            ////drItem["ID_Asset"] = -2;
            ////drItem["Name"] = "--SELECT--";
            ////dtItemList.Rows.Add(drItem);
            ////dtItemList.AcceptChanges();

            //cboItem.ValueMember = "ID_Asset";
            //cboItem.DisplayMember = "Name";
            //cboItem.DataSource = dtAsset;

            //cboItem.SelectedValue = 0;
            //cboItem.Enabled = true;
            #endregion

            Cursor.Current = Cursors.WaitCursor;
                setItemList();
            Cursor.Current = Cursors.Default;

            //cboLoc.SelectedValueChanged += new EventHandler(cboLoc_SelectedValueChanged);
            //cboGroup.SelectedValueChanged += new EventHandler(cboGroup_SelectedValueChanged);
            //cboSubGroup.SelectedValueChanged += new EventHandler(cboSubGroup_SelectedValueChanged);

            Cursor.Current = Cursors.Default;
        }

        private void cboGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
               // if (cboGroup.Enabled)
               // {
                    cboSubGroup.Enabled = false;
                    cboItem.Enabled = false;
                    if (Convert.ToInt32(cboGroup.SelectedValue) != 0 && Convert.ToInt32(cboGroup.SelectedValue) != -2)
                    {
                        #region"Set Child Group"
                        DataTable dtGList = new DataTable();

                        dtGList = FS_AssetStatus.getGroups(Convert.ToInt32(cboGroup.SelectedValue));
                        //DataRow drG = dtGList.NewRow();
                        //drG["ID_AssetGroup"] = 0;
                        //drG["Name"] = "--ALL--";
                        //dtGList.Rows.Add(drG);
                        //dtGList.AcceptChanges();

                        //drG = dtGList.NewRow();
                        //drG["ID_AssetGroup"] = -2;
                        //drG["Name"] = "--SELECT--"; 
                        //dtGList.Rows.Add(drG);
                        //dtGList.AcceptChanges();

                        dtGList.Merge(dtGroups, true, MissingSchemaAction.Add);

                        cboSubGroup.ValueMember = "ID_AssetGroup";
                        cboSubGroup.DisplayMember = "Name";
                        cboSubGroup.DataSource = dtGList;

                        cboSubGroup.SelectedValue = 0;
                        #endregion
                    }
                    else
                    {
                        //DataTable dtGList = new DataTable();
                        //dtGList = FS_AssetStatus.getGroups(-3); //No value Retrived
                        //DataRow drG = dtGList.NewRow();
                        //drG["ID_AssetGroup"] = 0;
                        //drG["Name"] = "--ALL--";
                        //dtGList.Rows.Add(drG);
                        //dtGList.AcceptChanges();

                        //drG = dtGList.NewRow();
                        //drG["ID_AssetGroup"] = -2;
                        //drG["Name"] = "--SELECT--";
                        //dtGList.Rows.Add(drG);
                        //dtGList.AcceptChanges();

                        cboSubGroup.ValueMember = "ID_AssetGroup";
                        cboSubGroup.DisplayMember = "Name";
                        cboSubGroup.DataSource = dtGroups;

                        cboSubGroup.SelectedValue = 0;
                    }
                    //GetAllItems();
               // }
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
            finally
            {
                cboItem.Enabled = true;
                cboSubGroup.Enabled = true;
            }
        }

        private void setItemList()
        {
            #region "Set Items"
            DataTable dtItemList = new DataTable();
            //if (Convert.ToInt64(cboSubGroup.SelectedValue) == 0 || Convert.ToInt64(cboSubGroup.SelectedValue) == -2)
            if (SelectedSubGroupID == 0)
            {
                dtItemList = FS_AssetStatus.getAssets(SelectedLocationID, SelectedGroupID, 0);  
            }
            else
            {
                dtItemList = FS_AssetStatus.getAssets(SelectedLocationID, SelectedSubGroupID, 0);  
            }

            dtListAssets = dtItemList.Copy();

            DataColumn dc = new DataColumn("ScanStatus", Type.GetType("System.Int16"));
            dc.DefaultValue = ScanStatus.Missing;
            dtListAssets.Columns.Add(dc);
            dtListAssets.AcceptChanges();

          //  DataRow drItem = dtItemList.NewRow();
          //  drItem["ID_Asset"] = 0;
          //  drItem["Name"] = "--ALL--";
          //  dtItemList.Rows.Add(drItem);
          ////  dtItemList.AcceptChanges();

          //  drItem = dtItemList.NewRow();
          //  drItem["ID_Asset"] = -2;
          //  drItem["Name"] = "--SELECT--";
          //  dtItemList.Rows.Add(drItem);
          //  dtItemList.AcceptChanges();

            dtItemList.Merge(dtAsset, true, MissingSchemaAction.Add);
            dtItemList.AcceptChanges();

            cboItem.ValueMember = "ID_Asset";
            cboItem.DisplayMember = "Name";
            cboItem.DataSource = dtItemList;

            cboItem.SelectedValue = 0;
            #endregion "Set Items"
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

            try
            {
                #region"Open Scan form"
                TagFSForm FSForm = new TagFSForm();
                FSForm.recPerPage = UserPref.GetInstance().PageSize;
                if (SelectedLocationID < 0)
                {
                    MessageBox.Show("Please select any Location.");
                    return;
                }
                else if (SelectedGroupID < 0)
                {
                    MessageBox.Show("Please select any Group.");
                    return;
                }
                else if (SelectedSubGroupID < 0)
                {
                    MessageBox.Show("Please select any Sub Group.");
                    return;
                }
                else if (Convert.ToInt32(cboItem.SelectedValue) == -2)
                {
                    MessageBox.Show("Please select any Item.");
                    return;
                }
                else if (Convert.ToInt32(cboItem.SelectedValue) == 0)
                {
                    if (dtListAssets != null && dtListAssets.Rows.Count >= 1)
                    {
                        FSForm.dtAsset = dtListAssets;
                    }
                    else
                    {
                        MessageBox.Show("Items not found for Field Services.");
                        return;
                    }
                }
                else
                {
                    Cursor.Current = Cursors.WaitCursor;
                    dtListAssets = FS_AssetStatus.getAssets(0, 0, Convert.ToInt64(cboItem.SelectedValue));
                    DataColumn dc = new DataColumn("ScanStatus", Type.GetType("System.Int16"));
                    dc.DefaultValue = ScanStatus.Missing;
                    dtListAssets.Columns.Add(dc);
                    dtListAssets.AcceptChanges();
                    FSForm.dtAsset = dtListAssets;
                    Cursor.Current = Cursors.Default;
                }

                this.Enabled = false;
                FSForm.Closed += new EventHandler(this.OnOperFrmClosed);               
                FSForm.Show();
                #endregion
            }
            catch (ApplicationException ap)
            {
                Logger.LogError(ap.Message); 
                Program.ShowError(ap.Message.ToString());
                this.Enabled = true;
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Logger.LogError(ex.Message); 
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                this.Enabled = true;
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {              
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
                this.Enabled = true;
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
                this.Enabled = true;
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message); 
                this.Enabled = true;
            }
            Cursor.Current = Cursors.Default;
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void cboLoc_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboLoc.SelectedValue) == -2 || Convert.ToInt32(cboGroup.SelectedValue) == -2)
            {
                cboItem.ValueMember = "ID_Asset";
                cboItem.DisplayMember = "Name";
                cboItem.DataSource = dtAsset;

                cboItem.SelectedValue = 0;
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            if (cboLoc.Enabled)
            {
                cboItem.Enabled = false;
                setItemList();
                cboItem.Enabled = true;
            }
            Cursor.Current = Cursors.Default;
        }

        private void cboSubGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            GetAllItems();
        }

        private void GetAllItems()
        {
            if (SelectedLocationID == 0 && SelectedGroupID == 0)
            {
                cboItem.ValueMember = "ID_Asset";
                cboItem.DisplayMember = "Name";
                cboItem.DataSource = dtAsset;
                cboItem.SelectedValue = 0;
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            //if (cboSubGroup.Enabled)
           // {
                cboItem.Enabled = false;
                setItemList();
                cboItem.Enabled = true;
            //}
            Cursor.Current = Cursors.Default;
        }

        private void CreateTablesSchemas()
        {
            #region Create Datatable Assets
            dtAsset = new DataTable();

            DataColumn dc = new DataColumn("ID_Asset", Type.GetType("System.Int64"));
            dtAsset.Columns.Add(dc);

            dc = new DataColumn("Name", Type.GetType("System.String"));
            dtAsset.Columns.Add(dc);

            dc = new DataColumn("ScanStatus", Type.GetType("System.Int16"));
            dc.DefaultValue = ScanStatus.Missing;
            dtAsset.Columns.Add(dc);

            DataRow drItem = dtAsset.NewRow();
            drItem["ID_Asset"] = 0;
            drItem["Name"] = "--ALL--";
            dtAsset.Rows.Add(drItem);

            drItem = dtAsset.NewRow();
            drItem["ID_Asset"] = -2;
            drItem["Name"] = "--SELECT--";
            dtAsset.Rows.Add(drItem);

            dtAsset.AcceptChanges();
            #endregion Create Datatable Assets           

            #region Create Datatable Asset Groups
            dtGroups = new DataTable();

            DataColumn dcGrp = new DataColumn("ID_AssetGroup", Type.GetType("System.Int64"));
            dtGroups.Columns.Add(dcGrp);

            dcGrp = new DataColumn("Name", Type.GetType("System.String"));
            dtGroups.Columns.Add(dcGrp);

            DataRow drGrp = dtGroups.NewRow();
            drGrp["ID_AssetGroup"] = 0;
            drGrp["Name"] = "--ALL--";
            dtGroups.Rows.Add(drGrp);          

            dtGroups.AcceptChanges();
            #endregion Create Datatable Asset Groups
         

        }

        private void btnSelLoc_Click(object sender, EventArgs e)
        {
            string locName;
            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.Locations, "All Locations", 0, true);
            frmSel.ShowDialog();

            int lastSelected = SelectedLocationID;

            if (frmSel.objectID != -99999)
            {
                locName = frmSel.objectName;
                SelectedLocationID = frmSel.objectID;
            }
            else if (SelectedLocationID == frmSel.defaultID)
            {
                locName = frmSel.defaultName;
            }
            else
            {
                return;
            }


            if (locName.Length > 15)
            {
                lnkLocName.Text = locName.Substring(0, 13);
                lnkLocName.Text = lnkLocName.Text + "..";
            }
            else
            {
                lnkLocName.Text = locName;
            }
            lnkLocName.Tag = locName;

            if (SelectedLocationID != lastSelected)
            {
                //if (Convert.ToInt32(cboLoc.SelectedValue) == -2 || Convert.ToInt32(cboGroup.SelectedValue) == -2)
                //{
                //    cboItem.ValueMember = "ID_Asset";
                //    cboItem.DisplayMember = "Name";
                //    cboItem.DataSource = dtAsset;

                //    cboItem.SelectedValue = 0;
                //    return;
                //}

                Cursor.Current = Cursors.WaitCursor;
                 
                    cboItem.Enabled = false;
                    setItemList();
                    cboItem.Enabled = true;
               
                Cursor.Current = Cursors.Default;
            } 

        }

        private void lnkLocName_Click(object sender, EventArgs e)
        {
            try
            {
                ShowMessage(lnkLocName.Tag.ToString());
            }
            catch
            {
            }
        }

        private void btnGroups_Click(object sender, EventArgs e)
        {
            string selectedName;
            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.Groups, "All Groups", 0, true);
            frmSel.ShowDialog();

            if (frmSel.objectID != -99999)
            {
                selectedName = frmSel.objectName;
                SelectedGroupID = frmSel.objectID;
            }
            else if (SelectedGroupID == frmSel.defaultID)
            {
                selectedName = frmSel.defaultName;
            }
            else
            {
                return;
            }

            // ttLocation.SetTooltipText(lblLocation, lblLocation.Text);

            if (selectedName.Length > 15)
            {
                lnkGroups.Text = selectedName.Substring(0, 13);
                lnkGroups.Text = lnkGroups.Text + "..";
            }
            else
            {
                lnkGroups.Text = selectedName;
            }
            lnkGroups.Tag = selectedName;
        }

        private void lnkGroups_Click(object sender, EventArgs e)
        {
            ShowMessage(lnkGroups.Tag.ToString());
        }

        private void btnSubGroups_Click(object sender, EventArgs e)
        {
            string selectedName;

            int lastSelected = SelectedSubGroupID;

            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.SubGroups, "All Sub Groups", 0, true);
            if (SelectedGroupID > 0)
                frmSel.parentID = SelectedGroupID;
            else
                frmSel.parentID = -2;

            frmSel.ShowDialog();

            if (frmSel.objectID != -99999)
            {
                selectedName = frmSel.objectName;
                SelectedSubGroupID = frmSel.objectID;
            }
            else if (SelectedSubGroupID == frmSel.defaultID)
            {
                selectedName = frmSel.defaultName;
            }
            else
            {
                return;
            }

            // ttLocation.SetTooltipText(lblLocation, lblLocation.Text);

            if (selectedName.Length > 15)
            {
                lnkSubGroups.Text = selectedName.Substring(0, 13);
                lnkSubGroups.Text = lnkSubGroups.Text + "..";
            }
            else
            {
                lnkSubGroups.Text = selectedName;
            }
            lnkSubGroups.Tag = selectedName;

            if(lastSelected != SelectedSubGroupID)
                GetAllItems();

        }

        private void lnkSubGroups_Click(object sender, EventArgs e)
        {
            ShowMessage(lnkSubGroups.Tag.ToString());
        }        

        void ShowMessage(string message)
        {
            try
            {
                if (msgbox != null)
                {
                    msgbox.Close();
                    //msgbox = null;
                }
                else
                {

                }

                msgbox = new TransientMsgDlg(0);

                msgbox.Closed += new EventHandler(msgbox_Closed);

                int dispLength;

                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    dispLength = 23;
                    msgbox.Size = new Size(210, 110);
                }
                else
                {
                    dispLength = 30;
                    msgbox.Height = 110;
                }

                if (message.Trim().Length > dispLength)
                {
                    char[] splitParams = new char[1];
                    splitParams[0] = ' ';
                    string[] msgSplit = message.Split(splitParams);

                    int end = 0;

                    if (msgSplit != null && msgSplit.Length > 0)
                    {
                        StringBuilder strValue = new StringBuilder();

                        foreach (string value in msgSplit)
                        {
                            end = strValue.Length;
                            strValue.Append(value);

                            if (value.Length > dispLength)
                            {
                                try
                                {
                                    strValue.Insert(end + dispLength, " ");
                                    end = dispLength + 1;
                                    //strValue.Append(value.Insert(dispLength, " "));
                                    if (value.Substring(dispLength).Length > dispLength)
                                    {
                                        //strValue.Append(value.Insert(dispLength * 2, " "));
                                        strValue.Insert(end + dispLength, " ");
                                    }
                                }
                                catch
                                {
                                }
                            }
                            //else
                            //{
                            //    strValue.Append(value);
                            //}

                            strValue.Append(" ");
                        }

                        message = strValue.ToString().Trim();
                    }

                }

                msgbox.MsgHAlign = ContentAlignment.TopLeft;
                msgbox.SetTimeout(3);
                msgbox.TopMost = true;
                msgbox.AllowUserClose = true;
                msgbox.SetDpyMsg(message, "Full Text");

                msgbox.AutoScroll = false;

                msgbox.Show();
            }
            catch
            {
            }
        }

        void msgbox_Closed(object sender, EventArgs e)
        {
            try
            {
                msgbox = null;

            }
            catch
            {
            }
        }

    }
}