/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Asset Search functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmSearchAssetImmuno : Form
    {
        public frmSearchAssetImmuno()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void frmSearchAssetImmuno_Load(object sender, EventArgs e)
        {

            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
               
            }
          

            Cursor.Current = Cursors.WaitCursor; 
#region"Set Location Combo"
            DataTable dtList = new DataTable();
            dtList = Locations.getLocationList();

            DataRow dr = dtList.NewRow();
            dr["ID_Location"] = 0;
            dr["Name"] = "--ALL--";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            dr = dtList.NewRow();
            dr["ID_Location"] = -2;
            dr["Name"] = "--SELECT--";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboLoc.ValueMember = "ID_Location";
            cboLoc.DisplayMember = "Name";
            cboLoc.DataSource = dtList;

            cboLoc.SelectedValue = -2;
#endregion

            #region"Set Parent Group"
            //Set Location Combo
            DataTable dtGList = new DataTable();
            dtGList = AssetGroups.getGroups(0);

            DataRow drG = dtGList.NewRow();
            drG["ID_AssetGroup"] = -2;
            drG["Name"] = "--SELECT--";
            dtGList.Rows.Add(drG);
            dtGList.AcceptChanges();

            drG = dtGList.NewRow();
            drG["ID_AssetGroup"] = 0;
            drG["Name"] = "--ALL--";
            dtGList.Rows.Add(drG);
            dtGList.AcceptChanges();

            cboGroup.ValueMember = "ID_AssetGroup";
            cboGroup.DisplayMember = "Name";
            cboGroup.DataSource = dtGList;

            cboGroup.SelectedValue = -2;
            #endregion


            #region"Set SUB Group"
            //Set Location Combo
            DataTable dtSGList = new DataTable();
            dtSGList = AssetGroups.getGroups(-3);

            DataRow drSG = dtSGList.NewRow();
            drSG["ID_AssetGroup"] = -2;
            drSG["Name"] = "--SELECT--";
            dtSGList.Rows.Add(drSG);
            dtSGList.AcceptChanges();

            drSG = dtSGList.NewRow();
            drSG["ID_AssetGroup"] = 0;
            drSG["Name"] = "--ALL--";
            dtSGList.Rows.Add(drSG);
            dtSGList.AcceptChanges();

            cboSubGroup.ValueMember = "ID_AssetGroup";
            cboSubGroup.DisplayMember = "Name";
            cboSubGroup.DataSource = dtSGList;

            cboSubGroup.SelectedValue = -2;
            #endregion

            Cursor.Current = Cursors.Default; 
        }

        private void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboGroup.SelectedValue) != 0 && Convert.ToInt32(cboGroup.SelectedValue) != -2)
                {
                    #region"Set Child Group"
                    DataTable dtGList = new DataTable();

                    dtGList = AssetGroups.getGroups(Convert.ToInt32(cboGroup.SelectedValue));
                    DataRow drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = -2;
                    drG["Name"] = "--SELECT--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = 0;
                    drG["Name"] = "--ALL--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    cboSubGroup.ValueMember = "ID_AssetGroup";
                    cboSubGroup.DisplayMember = "Name";
                    cboSubGroup.DataSource = dtGList;

                    cboSubGroup.SelectedValue = 0;
                    #endregion
                }
                else
                {
                    DataTable dtGList = new DataTable();
                    dtGList = AssetGroups.getGroups(-2);
                    DataRow drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = -2;
                    drG["Name"] = "--SELECT--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = 0;
                    drG["Name"] = "--ALL--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    cboSubGroup.ValueMember = "ID_AssetGroup";
                    cboSubGroup.DisplayMember = "Name";
                    cboSubGroup.DataSource = dtGList;

                    cboSubGroup.SelectedValue = 0;
                }
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 LocationID;

                if (cboLoc.SelectedItem != null)
                {
                    LocationItem li = (LocationItem)cboLoc.SelectedItem;
                    LocationID = Convert.ToInt32(li.ID_Location);
                    if (LocationID == -2)
                    {
                        MessageBox.Show("Please Select any Location.");
                        return;
                    }

                }
                else
                {
                    MessageBox.Show("Please Select any Location.");
                    return;
                }
                if (Convert.ToInt32(cboGroup.SelectedValue) == -2)
                {
                    MessageBox.Show("Please Select any Group.");
                    return; 
                }

                if (Convert.ToInt32(cboSubGroup.SelectedValue) == -2)
                {
                    MessageBox.Show("Please Select any Sub Group.");
                    return; 
                }


            #region"Search Asset"
                Cursor.Current = Cursors.WaitCursor; 
            DataTable dtGList = new DataTable();
            dtGList = Assets.getSearchReasult(txtAssetNo.Text, txtAssetName.Text, LocationID, Convert.ToInt32(cboGroup.SelectedValue), Convert.ToInt32(cboSubGroup.SelectedValue), txtLotNo.Text, txtTagID.Text.Trim());
            //DataRow drG;
            System.Text.StringBuilder csvData=new System.Text.StringBuilder("");
            //foreach (DataRow drG in dtGList.Rows) 
            //{
            //    csvData.Append(", "+ drG["TagID"]);  
            //}
            Cursor.Current = Cursors.Default; 
            #endregion
            if (dtGList.Rows.Count != 0)
            {
                // Open new Tag Invtry window
                frmSearchlist TagList = new frmSearchlist();
                TagList.searchedData = dtGList;
                TagList.Show();  
                // disable form until this new form closed
                this.Enabled = false;
                TagList.Closed += new EventHandler(this.OnOperFrmClosed);
                //    // Open new Tag Invtry window
                //    TagRangingForm TagRangingFm = new TagRangingForm();
                //    TagRangingFm.SearchTagList = csvData.ToString();   
                //    TagRangingFm.Show();
                //    // disable form until this new form closed
                //    this.Enabled = false;
                //    TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else
            {
                MessageBox.Show("Search data not found.");  
            }
        }
        catch (ApplicationException ap)
        {
            Program.ShowError(ap.Message.ToString());
            Logger.LogError(ap.Message); 
        }
        catch (System.Web.Services.Protocols.SoapException ex)
        {
            if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                Program.ShowError("Request from innvalid IP address.");
            else
                Program.ShowError("Network Protocol Failure.");
            Logger.LogError(ex.Message); 
        }
        catch (System.Data.SqlServerCe.SqlCeException sqlex)
        {
            MessageBox.Show("Data File is not able to access.");
            Logger.LogError(sqlex.Message); 
        }
        catch (System.Net.WebException wex)
        {
            MessageBox.Show("Web exception occured.");
            Logger.LogError(wex.Message); 
        }
        catch (Exception ep)
        {
            Logger.LogError(ep.Message); 
            MessageBox.Show(ep.Message.ToString());
        }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            DialogResult res;
            uploadCSV.Filter = "CSV|*.csv"; 
            res = uploadCSV.ShowDialog();
            String csvData = "";
            System.IO.StreamReader oRead;
            if (System.IO.File.Exists(uploadCSV.FileName))
            {
                oRead = System.IO.File.OpenText(uploadCSV.FileName);
                csvData = oRead.ReadToEnd();
                if (csvData.Trim().Length != 0)
                {
                    DataTable dtGList = new DataTable();
                    dtGList.Columns.Add("TagID");
                    dtGList.Columns.Add("Name");
                    dtGList.AcceptChanges();  
                    DataRow dr;
                    foreach(String tag in csvData.Split(",".ToCharArray()))
                    {
                        if (tag.Trim().Length != 0)
                        {
                            dr = dtGList.NewRow();
                            dr["TagID"] = tag.Trim();
                            dr["Name"] = tag.Trim();
                            dtGList.Rows.Add(dr);
                        }
                    }
                    dtGList.AcceptChanges();  
                    

//                    dtGList.LoadDataRow(, true);  

                    frmSearchlist TagList = new frmSearchlist();
                    TagList.searchedData = dtGList;
                    TagList.IsUploadFromFile = true;   
                    TagList.Show();
                    // disable form until this new form closed
                    this.Enabled = false;
                    TagList.Closed += new EventHandler(this.OnOperFrmClosed);


                    //// Open new Tag Invtry window
                    //TagRangingForm TagRangingFm = new TagRangingForm();
                    //TagRangingFm.SearchTagList = csvData;
                    //TagRangingFm.Show();
                    //// disable form until this new form closed
                    //this.Enabled = false;
                    //TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
                }
                else
                {
                    MessageBox.Show("Invalid File.");
                }
            }
            //System.IO.File.OpenText(uploadCSV.FileName);
//System.IO.File.te

            //uploadCSV.FileName   
        }

        private void label6_ParentChanged(object sender, EventArgs e)
        {

        }

        
        private void txtTagID_TextChanged(object sender, EventArgs e)
        {
            if (txtTagID.Text.Length != 0)
            {
                if (txtTagID.Text.Length == txtTagID.MaxLength)
                {
                    txtTagID.BackColor = Color.Yellow;
                }
                else
                {
                    txtTagID.BackColor = Color.Cyan;
                }
            }
            else
            {
                txtTagID.BackColor = Color.White;
            }
        }

        private void txtTagID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.HexKeyPressChk(sender, e);
        }

        private void frmSearchAssetImmuno_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:

                    Program.On_KeyUp(sender, e);

                    break;
                default:
                    break;
            }
        }
    }
}