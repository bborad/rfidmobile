﻿using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using CS101UILib;
using System.Windows.Forms;
using ClsLibBKLogs;
using ClsRampdb;
using ClsReaderLib;
using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class frmReceiveDispatch : Form
    {
        private String ItemTags = "-1";
        private StringBuilder InTagList = new StringBuilder("");
        private Assets SelectedAsset;
        List<Assets> lstItems;
        Int32 SelectedLocationID = 0;
        ArrayList InTagArray;

        TransientMsgDlg msgbox = null;

       // private TagAccListVHelper ListVHelper;
        Reader Rdr ;
        public frmReceiveDispatch()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            InitScanButtonState();

            //ListVHelper = new TagAccListVHelper(this.EPCListV);

            lstItems = new List<Assets>();
            Rdr = ReaderFactory.GetReader();
           // EPCListV.BringToFront();
        }

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion

        private void InitScanButtonState()
        {
            OpState State = OpState.Stopped;
            this.btnScan.Tag = State; // boxing
        }

        private OpState ScanState()
        {
            OpState state = OpState.Stopped;
            if (btnScan.Tag is OpState)
            {
                state = (OpState)btnScan.Tag;
            }
            else
            {
                throw new ApplicationException("Scan Button Tag is not OpState");
            }
            return state;
        }

        private void SetScanState(OpState newState)
        {
            OpState CurState = ScanState();
            // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrForm));

            switch (newState)
            {
                case OpState.Starting:
                    // disable scan button
                    btnScan.Enabled = false;
                    // disable read1 button
                    // Read1Button.Enabled = false;
                    // disable write1 button
                    //  Write1Button.Enabled = false;
                    //  btnAddAsset.Enabled = false;
                    //  btnAddEmployee.Enabled = false;
                    //  btnAddLocation.Enabled = false;
                    // disable write-any button
                    //   ScanWrButton.Enabled = false;
                    //   ClrLstBttn.Enabled = false;
                    //TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    btnScan.Text = "Stop";
                    btnScan.Enabled = true;
                    // ClrLstBttn.Enabled = false;
                    //TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    btnScan.Enabled = false;
                    // ClrLstBttn.Enabled = false;
                   // TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    btnScan.Text = "Scan Tags";
                    btnScan.Enabled = true;
                    // Read1Button.Enabled = true;
                    //  Write1Button.Enabled = true;
                    //  btnAddAsset.Enabled = true;
                    //  btnAddEmployee.Enabled = true;
                    //  btnAddLocation.Enabled = true;
                    //  ScanWrButton.Enabled = true;
                    //  ClrLstBttn.Enabled = true;
                   // TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }
            btnScan.Tag = newState;
        }
        private void setState(bool readingState)
        {
            if (readingState)
            {
                //in reading mode
                btnScan.Text = "Stop";
                btnScan.Enabled = true;
                pnlTagDetails.Visible = false;
                pnlTagScan.Visible = true;

                btnConfirm.Visible = false;
                btnSelectAll.Visible = false;
                btnConfirmAll.Visible = false;

                btnScanBarcode.Enabled = false;
              
                 
                TagCntLabel.Visible = true;
                epclistbox.Visible = true;
               
            }
            else
            {
                //in ideal mode
                btnScan.Text = "Scan Tags";
                btnScan.Enabled = true;
               
            }
        }

        private void setScanTags()
        {
            int fromRec, toRec;

            fromRec = ((curPage - 1) * recPerPage) + 1;
            toRec = curPage * recPerPage;

            InTagList = new StringBuilder("'-1'");
            Int32 iTag;

            if (InTagArray == null)
                InTagArray = new ArrayList(recPerPage);
            else
                InTagArray.Clear();

            if (barCodeScan == false)
            {
                if (epclistbox.Items.Count <= toRec)
                    toRec = epclistbox.Items.Count;

                try
                {
                    for (iTag = fromRec - 1; iTag < toRec; iTag++)
                    {
                        InTagList.Append("," + "'" + epclistbox.Items[iTag].ToString() + "'");
                        InTagArray.Add(epclistbox.Items[iTag].ToString());

                    }
                }
                catch
                {
                }
            }
            else
            {
                if (BCList.Items.Count <= toRec)
                    toRec = BCList.Items.Count;

                string tagid;

                int CurNumRows = epclistbox.Items.Count ;
                try
                {
                    for (iTag = fromRec - 1; iTag < toRec; iTag++)
                    {

                        tagid = BCList.Items[iTag].SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');
                        InTagList.Append("," + "'" + tagid + "'");
                        InTagArray.Add(tagid);

                        ListViewItem item = new ListViewItem(new String[] { (CurNumRows + 1).ToString(), tagid });
                                                                            
                        // Insert new item to the top of the list for easy viewing
                         //EPCListV.Items.Insert(0, item);
                        epclistbox.Items.Insert(0, tagid);


                    }
                }
                catch
                { }
            }

            lstItems.Clear();
        }


        #region "sound and meolody variables"
        const int NumThresholdLvls = 8;
        //#if USE_WBRSSI
        //        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
        //            {
        //                0.0F, // within read range
        //                100.0F, // low
        //                100.0F, // 
        //                100.0F,
        //                110.0F, // medium
        //                110.0F,
        //                110.0F, // 
        //                120.0F, // high
        //            };
        //#else
        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
            {
                0.0F, // within read range
                50.0F, // low
                60.0F, // 
                65.0F,
                70.0F, // medium
                75.0F,
                80.0F, // 
                90.0F, // high
            };
        //#endif

        static uint[] PulsePeriods = new uint[NumThresholdLvls]
            {
                1000,
                1000,
                1000,
                600,
                400,
                200,
                100,
                100,
            };

        static uint[] LedRadius = new uint[NumThresholdLvls]
            {
                20,
                20,
                20,
                30,
                40,
                50,
                60,
                60,
            };

        static uint[] BuzzerPitch = new uint[NumThresholdLvls]
            {
                200,
                200,
                200,
                400,
                800,
                1600,
                3200,
                3200,
            };
        static RingTone[] RingMelody = new RingTone[NumThresholdLvls]
            {
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T3,
                RingTone.T4,
                RingTone.T5,
                RingTone.T5,
            };
        #endregion

        #region F11/F4/F5 Hotkey
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            if (!this.Enabled)
            {
                return;
            }
            switch (keyCode)
            {
                case eVKey.VK_F11: // no need to support auto-fire because ScanRd is
                case eVKey.VK_F19:
                    HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    {
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if ((!F11Depressed) && (!AnyOperationRunning()))
                            {
                                ScanButton_Click(this, null);
                            }
                            F11Depressed = true;
                        }
                        else // up
                        {
                            // Stop running Scan-Rd Op
                            if (isReading)
                            {
                                ScanButton_Click(this, null);
                            }
                            F11Depressed = false;
                        }
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (!AnyOperationRunning())
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && !AnyOperationRunning())
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                // case Keys.F7:
                // case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (AnyOperationRunning())
                    {
                        MessageBox.Show("Please stop any Tag Operation first", "Denied");

                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }


        private bool AnyOperationRunning()
        {
            return ((ScanState() != OpState.Stopped));
            // || (Read1State() != OpState.Stopped));
        }

        #endregion


        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }

        private void RefreshTagCntLabel()
        {
            TagCntLabel.Text =epclistbox.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLabel.Refresh();
            //Application.DoEvents(); 
        }

        

        #region Tag Inventory Event Handler

        private TagRangeList tagList = new TagRangeList();

        int count = 0;
        ListView tempList = null;

        private void TagReadEvent(string epc, string rssi)
        {
            try
            {
                this.BeginInvoke(new Action<string>((string e) =>
                {
                    if (!epclistbox.Items.Contains(epc))
                    {
                        epclistbox.Items.Insert(0, epc);
                        TagCntLabel.Text = epclistbox.Items.Count.ToString();
                    }

                }), epc);

            }
            catch { }

        }
        //private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        //{
        //    // Add new TagID to list (or update occurrence)

        //    if (e.Cnt != 0)
        //    {
        //        count++;

        //        //ListViewItem Item = new ListViewItem(new string[] { e.EPC.ToString() });
        //        //if (!tempList.Items.Contains(Item))
        //        //{
        //        //    tempList.Items.Insert(0, Item); 
        //        //    AddEPCToListV(e.PC, e.EPC);
        //        //    RefreshTagCntLabel();
        //        //    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        //        //    if (count >= 10)
        //        //    {
        //        //        count = 0;
        //        //        EPCListV.Refresh();
        //        //        Application.DoEvents();
        //        //    }
        //        //}

        //        if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
        //        {
        //            AddEPCToListV(e.PC, e.EPC);
        //            RefreshTagCntLabel();
        //            //TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        //            if (count >= 10)
        //            {
        //                count = 0;
        //                EPCListV.Refresh();
        //                Application.DoEvents();
        //            }
        //        }

        //    }
        //}

        //private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        //{
        //    OpState State = ScanState();
        //    //RFIDRdr Rdr = RFIDRdr.GetInstance();
        //    Reader Rdr = ReaderFactory.GetReader();
        //    switch (e.status)
        //    {
        //        case InvtryOpStatus.started:
        //            SetScanState(OpState.Started);
        //            // Make the List Long (purpose of scanning)
        //            // if (!ListIsLong())
        //            //  OnLngLstButtonClicked(this, null);
        //            Program.RefreshStatusLabel(this, "Scanning...");
        //            break;
        //        case InvtryOpStatus.stopped:
        //        case InvtryOpStatus.errorStopped:
        //        case InvtryOpStatus.macErrorStopped:
        //            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
        //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
        //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
        //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
        //            if (e.status == InvtryOpStatus.errorStopped)
        //            {
        //                // Display error or Restart Radio (if necessary)
        //                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
        //                    RestartRfidDevice();
        //                else
        //                    Program.ShowError("Tag Inventory Stopped with Error: " + e.msg);
        //            }
        //            else if (e.status == InvtryOpStatus.macErrorStopped)
        //            {
        //                // ushort MacErrCode;
        //                if (Rdr.GetMacError() && Reader.macerr != 0)
        //                {
        //                    // if fatal mac error, display message
        //                    if (Rdr.MacErrorIsFatal() || (ScanState() != OpState.Stopping))
        //                        Program.ShowError(e.msg);
        //                }
        //                else
        //                    Program.ShowError("Unknown HW error. Abort");
        //            }
        //            // restore StartButton

        //            SetScanState(OpState.Stopped);
        //            Program.RefreshStatusLabel(this, "Finished");
        //            //if (ListIsLong())
        //            // OnLngLstButtonClicked(this, null);
        //            break;
        //        case InvtryOpStatus.error:
        //            // restore StartButton
        //            switch (State)
        //            {
        //                case OpState.Starting:
        //                    MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
        //                    //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
        //                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
        //                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
        //                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
        //                    break;
        //                case OpState.Started:
        //                    // TBD
        //                    MessageBox.Show(e.msg, "Tag Inventry Error");
        //                    break;
        //                case OpState.Stopping:
        //                    MessageBox.Show(e.msg, "Tag Inventory Stop Error");
        //                    //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
        //                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
        //                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
        //                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
        //                    break;
        //                case OpState.Stopped:
        //                    throw new ApplicationException("Unexpected error return during Stopped State");
        //            }
        //            SetScanState(OpState.Stopped);
        //            Program.RefreshStatusLabel(this, "Error Stopped");
        //            break;
        //    }

        //    // Re-enable Field Input Textboxes (disabled during inventory operation)
        //    if (ScanState() == OpState.Stopped)
        //    {
        //        btnScan.Enabled = false;
        //        Cursor.Current = Cursors.WaitCursor;
        //        try
        //        {
        //            //EPCListV.Visible = false;                   
        //            // button1.Visible = true;
        //            // TagCntLabel.Visible = false;
        //            // lstAsset.Visible = true;

        //            if (EPCListV.Items.Count > 0)
        //            {
        //                Program.RefreshStatusLabel(this, "Data Processing..");
        //                pnlTagDetails.Visible = true;
        //                pnlTagScan.Visible = false;

        //                //cboLocations.Size = new Size(220, 23);
        //                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.CS101Reader)
        //                {
        //                    lnkLocName.Size = new Size(176, 15);
        //                    btnSelLoc.Location = new Point(182, 4);
        //                }

        //                cmbPage.Visible = true;
        //                lblPage.Visible = true;
        //                cmbPage.BringToFront();

        //                pnlTagDetails.BringToFront();

        //                // recPerPage = 10;
        //                totalPages = SetupPaging(EPCListV.Items.Count);

        //                Application.DoEvents();

        //                curPage = 1;

        //                setScanTags();

        //                FillDetailView();

        //            }
        //            else
        //            {
        //                this.Text = "Move Items : Total Read - " + TagCntLabel.Text;
        //            }


        //            //if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
        //            //{
        //            //    ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
        //            //    LoadTxtBxes(row);
        //            //    // Enable (Avail) Text Fields
        //            //    DataDispPanel.Enabled = true;
        //            //}
        //        }
        //        catch (Exception epx)
        //        {
        //            throw epx;
        //        }
        //        finally
        //        {
        //            Cursor.Current = Cursors.Default;
        //            btnScan.Enabled = true;
        //            btnScanBarcode.Enabled = true;
        //        }
        //    }
        //}

        private void FillDetailView()
        {

            try
            {
                lstAsset.Items.Clear();

                if (lstItems.Count <= 0 && InTagList.Length > 0)
                {
                    lstItems = Assets.GetAssets_ByTagID(InTagList.ToString(), recPerPage);
                }

                //if (SelectedLocationID != 0)
                //{
                //    cboLocations.SelectedValue = SelectedLocationID;
                //}

                Int32 iCount = 0;
                ListViewItem lstItem;
                if (lstAsset.Items.Count <= 0)
                {
                    //lstAsset.CheckBoxes = true;

                    ListViewItem.ListViewSubItem ls;
                    ListViewItem.ListViewSubItem ls1;
                    // ListViewItem.ListViewSubItem ls2;
                    ListViewItem.ListViewSubItem ls3;
                    ItemTags = "-1";

                    foreach (Assets Ast in lstItems)
                    {
                        iCount++;

                        //lstItem = new ListViewItem(iCount.ToString());

                        lstItem = new ListViewItem();

                        ls3 = new ListViewItem.ListViewSubItem();
                        ls3.Text = Convert.ToString(Ast.TagID.Trim());
                        lstItem.SubItems.Add(ls3);

                        //lstItem = new ListViewItem();
                        if (Ast.ServerKey != 0)
                        {
                            // ItemTags = ItemTags + "," + Ast.TagID.Trim();

                            InTagArray.Remove(Ast.TagID.Trim());

                            ls = new ListViewItem.ListViewSubItem();
                            ls.Text = Convert.ToString(Ast.Name.ToString());
                            lstItem.SubItems.Add(ls);

                            ls1 = new ListViewItem.ListViewSubItem();
                            ls1.Text = Convert.ToString(Ast.getLocationName());
                            lstItem.SubItems.Add(ls1);

                            //ls2 = new ListViewItem.ListViewSubItem();
                            //ls2.Text = Convert.ToString(Ast.LotNo.ToString());
                            //lstItem.SubItems.Add(ls2);
                        }

                        lstAsset.Items.Add(lstItem);
                    }

                    //Tags Those are not assigned to database.                              

                    //  String[] TagAr = Convert.ToString("-1" + InTagList.ToString()).Split(new char[] { ',' });
                    foreach (String uTag in InTagArray)
                    {
                        //if (uTag.Trim().IndexOf("-1") < 0)
                        //{
                        if (ItemTags.IndexOf(uTag.Trim()) < 0)
                        {
                            iCount++;
                            //lstItem = new ListViewItem(iCount.ToString());
                            lstItem = new ListViewItem();

                            ls3 = new ListViewItem.ListViewSubItem();
                            ls3.Text = Convert.ToString(uTag.Trim());
                            lstItem.SubItems.Add(ls3);

                            ls = new ListViewItem.ListViewSubItem();
                            ls.Text = Convert.ToString("NA");
                            lstItem.SubItems.Add(ls);

                            ls1 = new ListViewItem.ListViewSubItem();
                            ls1.Text = Convert.ToString("");
                            lstItem.SubItems.Add(ls1);

                            //ls2 = new ListViewItem.ListViewSubItem();
                            //ls2.Text = Convert.ToString("");
                            //lstItem.SubItems.Add(ls2);                          

                            lstAsset.Items.Add(lstItem);
                        }
                        //}
                    }
                    lstAsset.Refresh();
                }

             
                // btnBack.Visible = true;
            }
            catch(Exception ex)
            {
                Logger.LogError("Move Item(s) -- FillDetailsView -- " + ex.Message);
            }
            finally
            {
                btnScan.Visible = false;
                btnScanBarcode.Visible = false;

                btnClear.Visible = true;
                btnSelectAll.Visible = true;
                btnConfirm.Visible = true;
                btnConfirmAll.Visible = true;
                this.Text = "Move Items : Total Read - " + TagCntLabel.Text;
            }


        }

        //private void AddEPCToListV(UInt16 PC, UINT96_T EPC)
        //{
        //    // Assuming that the Tag of this EPC is not already in list
        //    int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
        //    ListViewItem item = new ListViewItem(new String[] {
        //         (CurNumRows+1).ToString(), EPC.ToString() });
        //    // Insert new item to the top of the list for easy viewing
        //    EPCListV.Items.Insert(0, item);
        //    item.Tag = PC;
        //    EPCListV.Refresh();
        //    RefreshTagCntLabel();
        //}

        #endregion

        #region AddBarCOdeTag

        public ListView BCList;

        public bool barCodeScan = false;

        public void AddListViewItems()
        {

            try
            {
                //RFID_18K6C_MEMORY_BANK en = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_EPC;
                //ushort crc = 0;
                //ushort pc = 0;
                //UINT96_T p = new UINT96_T();
                //string s = li.Text.PadRight(24).ToString().Replace("Z", "A");
                //p.ParseString(li.Text.PadLeft(24, '0').ToString().Replace("Z", "A"));
                //string data = "";
                //UInt16 iRSSI = 20;
                //MemBnkRdEventArgs mem = new MemBnkRdEventArgs(ref crc, ref pc, ref p, en, data, iRSSI);
                //MemBnkRdEvtHandler(this, mem);


                //lstAsset.Items.Clear();
                //ListViewItem lstItem;
                // ListViewItem.ListViewSubItem ls;
                // Int16 iCount = Convert.ToInt16(lstAsset.Items.Count);

                if (BCList == null)
                    return;

                TagCntLabel.Text = BCList.Items.Count.ToString();

                // Int32 iTag = 0;
                String TgNo;
                //Assets Ast;

                if (BCList.Items.Count > 0)
                {
                    barCodeScan = true;
                    Program.RefreshStatusLabel(this, "Data Processing..");
                    pnlTagDetails.Visible = true;
                    pnlTagScan.Visible = false;

                    //cboLocations.Size = new Size(220, 23);

                    if (UserPref.GetInstance().SelectedHardware == HardwareSelection.CS101Reader)
                    {
                        lnkLocName.Size = new Size(176, 15);
                        btnSelLoc.Location = new Point(182, 4);
                    }

                    cmbPage.Visible = true;
                    lblPage.Visible = true;

                    pnlTagDetails.BringToFront();

                    // recPerPage = 10;
                    totalPages = SetupPaging(BCList.Items.Count);

                    Application.DoEvents();

                    curPage = 1;

                    setScanTags();

                    FillDetailView();

                }
                else
                {
                    this.Text = "Move Items : Total Read - " + TagCntLabel.Text;
                }


                //foreach (ListViewItem li in lAr)
                //{
                //   TgNo = li.SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');
                //   InTagList.Append("," + TgNo);
                //    // for (iTag = 0; iTag <= (EPCListV.Items.Count - 1); iTag++)
                //    // {
                //    iCount++;
                //    iTag++;
                //    lstItem = new ListViewItem(iCount.ToString());

                //    //  TgNo = EPCListV.Items[iTag].SubItems[1].Text;
                //    TgNo = li.SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');



                //    Ast = new Assets(TgNo);
                //    //Doc = new Documents(TgNo);

                //    if (Ast.ServerKey != 0)
                //    {
                //        ls = new ListViewItem.ListViewSubItem();
                //        ls.Text = Convert.ToString(Ast.Name.ToString());
                //        lstItem.SubItems.Add(ls);

                //        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                //        ls1.Text = Convert.ToString(Ast.getLocationName());
                //        lstItem.SubItems.Add(ls1);

                //        ListViewItem.ListViewSubItem ls2 = new ListViewItem.ListViewSubItem();
                //        ls2.Text = Convert.ToString(Ast.LotNo.Trim());
                //        lstItem.SubItems.Add(ls2);
                //    }
                //    else
                //    {
                //        ls = new ListViewItem.ListViewSubItem();
                //        ls.Text = Convert.ToString("NA");
                //        lstItem.SubItems.Add(ls);

                //        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                //        ls1.Text = Convert.ToString("NA");
                //        lstItem.SubItems.Add(ls1);

                //        ListViewItem.ListViewSubItem ls2 = new ListViewItem.ListViewSubItem();
                //        ls2.Text = Convert.ToString("NA");
                //        lstItem.SubItems.Add(ls2);
                //    }

                //    ListViewItem.ListViewSubItem ls3 = new ListViewItem.ListViewSubItem();
                //    ls3.Text = Convert.ToString(TgNo);
                //    lstItem.SubItems.Add(ls3);

                //    lstAsset.Items.Add(lstItem);
                //    ////}

                //}
                // lstAsset.Refresh();

                // lstAsset.Visible = true;
                //  EPCListV.Visible = false;
                //  btnScan.Enabled = true;
                //btnScnBarcd.Enabled = true;
                //button1.Enabled = true;
                //  btnClear.Enabled = true;
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message);
                MessageBox.Show(ep.Message.ToString());
            }
            finally
            {
                btnScan.Enabled = true;
                btnScanBarcode.Enabled = true;
            }
        }

        #endregion

       
 
        public bool isReading { get; set; }
        private void ScanButton_Click(object sender, EventArgs e)
        {

            btnScan.Enabled = false;

            Cursor.Current = Cursors.WaitCursor;
            try
            {

                if (isReading)
                {
                    //stop reading

                    Rdr.TagInventoryStop();
                    isReading = false;
                    setState(isReading);
                    if (epclistbox.Items.Count > 0)
                    {
                        Program.RefreshStatusLabel(this, "Data Processing..");
                        pnlTagDetails.Visible = true;
                        pnlTagScan.Visible = false;

                        //cboLocations.Size = new Size(220, 23);
                        if (UserPref.GetInstance().SelectedHardware == HardwareSelection.CS101Reader)
                        {
                            lnkLocName.Size = new Size(176, 15);
                            btnSelLoc.Location = new Point(182, 4);
                        }

                        cmbPage.Visible = true;
                        lblPage.Visible = true;
                        cmbPage.BringToFront();

                        pnlTagDetails.BringToFront();

                        // recPerPage = 10;
                        totalPages = SetupPaging(epclistbox.Items.Count);

                        Application.DoEvents();

                        curPage = 1;

                        setScanTags();

                        FillDetailView();

                    }
                }
                else
                {
                    
                    //Start reading
                    lstAsset.Items.Clear();
                    barCodeScan = false;
                    if (Rdr.TagInventoryStart(new Reader.EPCEvent(TagReadEvent)))
                    {
                        isReading = true;
                        setState(isReading);
                    }
                }
                
                //--------------------------------------------------


                // Scan or Stop
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
              
                //switch (ScanState())
                //{
                //    case OpState.Stopped:
                //        if (EPCListV.Items.Count <= 0)
                //        {
                //            ItemTags = "-1";
                //            InTagList = new StringBuilder("");
                //        }
                //        barCodeScan = false;

                //        lstAsset.Items.Clear();

                //        pnlTagDetails.Visible = false;
                //        pnlTagScan.Visible = true;

                //        btnConfirm.Visible = false;
                //        btnSelectAll.Visible = false;
                //        btnConfirmAll.Visible = false;

                //        btnScanBarcode.Enabled = false;

                //        // TagCntLabel.Visible = true;
                //        // EPCListV.Visible = true;
                //        // btnSave.Enabled = false;

                //        //Rdr.InvtryOpStEvent += InvtryOpEvtHandler;
                //        //Rdr.DscvrTagEvent += DscvrTagEvtHandler;
                //        Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                //        Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                //        SetScanState(OpState.Starting);
                //        Program.RefreshStatusLabel(this, "Starting...");
                //        //Count Status Message
                //        if (!Rdr.SetRepeatedTagObsrvMode(true))
                //            // Continue despite error
                //            Program.ShowError("Disable Repeated Tag Observation mode failed");
                //        //UserPref Pref = UserPref.GetInstance();
                //        //Byte[] Mask; uint MaskOffset;
                //        //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                //        bool Succ = Rdr.TagInventoryStart(5);
                //        if (!Succ)
                //        {
                //            SetScanState(OpState.Stopped);
                //            Program.RefreshStatusLabel(this, "Error Stopped");
                //            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                //            Program.ShowError("Error: TagInventory Failed to Start\n");
                //            Application.DoEvents();

                //        }

                //        break;
                //    case OpState.Started:
                //        // Stop Tag Read
                //        SetScanState(OpState.Stopping);
                //        Program.RefreshStatusLabel(this, "Stopping...");
                //        if (!Rdr.TagInventoryStop())
                //        {
                //            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                //            SetScanState(OpState.Stopped); // give up
                //            Program.RefreshStatusLabel(this, "Error Stopped...");
                //            Program.ShowError("Error: TagRead Failed to Stop\n");
                //            // button1.Enabled = false;
                //            // btnClear.Enabled = false;

                //        }
                //        else
                //        {
                //            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                //            //  Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                //            //Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                //        }

                //        // btnScnBarcd.Enabled = true;
                //        break;
                //    case OpState.Starting:
                //        // Stop Tag Read

                //        //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                //        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                //        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                //        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);

                //        SetScanState(OpState.Stopping);
                //        Program.RefreshStatusLabel(this, "Stopping...");
                //        if (!Rdr.TagInventoryStop())
                //        {
                //            // Restore
                //            SetScanState(OpState.Starting);
                //            Program.RefreshStatusLabel(this, "Starting...");
                //            Program.ShowWarning("Tag Inventory Stop failed during Starting phase, please try again");
                //        }
                //        //  btnScnBarcd.Enabled = true;
                //        break;
                //    default:
                //        // ignore
                //        break;
                //}
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message);
                MessageBox.Show(ep.Message.ToString());
            }
            Cursor.Current = Cursors.Default;

        }

        private void frmReceiveDispatch_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;

            this.MinimizeBox = false;
            this.MaximizeBox = false;

            Cursor.Current = Cursors.WaitCursor;

            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }

            pnlTagDetails.Visible = false;
            pnlTagScan.Visible = true;
            pnlTagScan.BringToFront();
            epclistbox.Items.Clear();

            btnScan.Text = "Scan Tags";
            btnClear.Visible = false;
            btnConfirm.Visible = false;
            btnSelectAll.Visible = false;
            btnConfirmAll.Visible = false;

            btnScanBarcode.Visible = true;

            //cboLocations.BringToFront();


            //cboLocations.KeyUp += cboLoc_KeyUp;
            // cboLocations.DropDownStyle = ComboBoxStyle.DropDown;
            //  cboLoc.Tag = "";

            //DataTable dtList = new DataTable();
            //// dtList = Locations.getLocationList();

            //dtList.Columns.Add("ID_Location");
            //dtList.Columns.Add("Name");

            //DataRow dr = dtList.NewRow();
            //dr["ID_Location"] = 0;
            //dr["Name"] = "Select Location";
            //dtList.Rows.Add(dr);
            //dtList.AcceptChanges();

            //cboLocations.ValueMember = "ID_Location";
            //cboLocations.DisplayMember = "Name";
            //cboLocations.DataSource = dtList;

            //cboLocations.SelectedValue = 0;

            // LocationItem item = new LocationItem("Select Location", "0");
            //  cboLocations.Items.Add(item);
            //  cboLocations.SelectedItem = item;

            //cboLocations.DataSource = Locations.getLocationList();
            //cboLocations.DisplayMember = "Name";
            //cboLocations.ValueMember = "ID_Location";

            //if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            //{
            //    cboLocations.Size = new Size(220, 23);
            //}
            //else
            //{
            //    cboLocations.Size = new Size(310, 23);
            //}


            cmbPage.BringToFront();
            lblPage.BringToFront();
            lblPage.Visible = false;
            cmbPage.Visible = false;

            lnkLocName.BringToFront();
            btnSelLoc.BringToFront();

            lnkLocName.Tag = lnkLocName.Text;

            this.btnSelLoc.Click += new EventHandler(btnSelLoc_Click);
            this.lnkLocName.Click += new EventHandler(lnkLocName_Click);

            Cursor.Current = Cursors.Default;

            // for Load Testing Only
            //if (MessageBox.Show("Load Testing?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            //{ 
            //    AddToList();
            //}           

        }

        void lnkLocName_Click(object sender, EventArgs e)
        {
            try
            {
                ShowMessage(lnkLocName.Tag.ToString());
            }
            catch
            {
            }
        }

        void btnSelLoc_Click(object sender, EventArgs e)
        {
            string locName;

            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);

            try
            {
                frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.Locations, lnkLocName.Tag.ToString(), SelectedLocationID, false);
                frmSel.ShowDialog();

                if (frmSel.objectID != -99999)
                {
                    locName = frmSel.objectName;

                    SelectedLocationID = frmSel.objectID;
                }
                else
                {
                    ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
                    return;

                } 

                if (locName.Length > 22)
                {
                    lnkLocName.Text = locName.Substring(0, 20);
                    lnkLocName.Text = lnkLocName.Text + "..";
                }
                else
                {
                    lnkLocName.Text = locName;
                }
                lnkLocName.Tag = locName;
            }
            catch
            {
            }       

            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        void ShowMessage(string message)
        {
            try
            {
                if (msgbox != null)
                {
                    msgbox.Close();
                    //msgbox = null;
                }
                else
                {

                }

                msgbox = new TransientMsgDlg(0);

                msgbox.Closed += new EventHandler(msgbox_Closed);

                int dispLength;

                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    dispLength = 23;
                    msgbox.Size = new Size(210, 110);
                }
                else
                {
                    dispLength = 30;
                    msgbox.Height = 110;
                }

                if (message.Trim().Length > dispLength)
                {
                    char[] splitParams = new char[1];
                    splitParams[0] = ' ';
                    string[] msgSplit = message.Split(splitParams);

                    int end = 0;

                    if (msgSplit != null && msgSplit.Length > 0)
                    {
                        StringBuilder strValue = new StringBuilder();

                        foreach (string value in msgSplit)
                        {
                            end = strValue.Length;
                            strValue.Append(value);

                            if (value.Length > dispLength)
                            {
                                try
                                {
                                    strValue.Insert(end + dispLength, " ");
                                    end = dispLength + 1;
                                    //strValue.Append(value.Insert(dispLength, " "));
                                    if (value.Substring(dispLength).Length > dispLength)
                                    {
                                        //strValue.Append(value.Insert(dispLength * 2, " "));
                                        strValue.Insert(end + dispLength, " ");
                                    }
                                }
                                catch
                                {
                                }
                            }
                            //else
                            //{
                            //    strValue.Append(value);
                            //}

                            strValue.Append(" ");
                        }

                        message = strValue.ToString().Trim();
                    }

                }

                msgbox.MsgHAlign = ContentAlignment.TopLeft;
                msgbox.SetTimeout(3);
                msgbox.TopMost = true;
                msgbox.AllowUserClose = true;
                msgbox.SetDpyMsg(message, "Full Text");

                msgbox.AutoScroll = false;

                msgbox.Show();
            }
            catch
            {
            }
        }

        void msgbox_Closed(object sender, EventArgs e)
        {
            try
            {
                msgbox = null;

            }
            catch
            {
            }
        }

        //private void cboLoc_KeyUp(object sender, KeyEventArgs e)
        //{
        //    try
        //    {
        //        switch (e.KeyCode)
        //        {
        //            case Keys.Left:
        //            case Keys.Right:
        //            case Keys.Tab:
        //            case Keys.Up:
        //            case Keys.Delete:
        //            case Keys.Down:
        //            case Keys.ShiftKey:
        //            case Keys.Shift:
        //                return;
        //        }

        //        string typedSoFar;

        //        if (e.KeyCode == Keys.Back)
        //        {
        //            typedSoFar = Convert.ToString(cboLocations.Tag);

        //            if (cboLocations.Text.Trim() != typedSoFar)
        //            {
        //                typedSoFar = cboLocations.Text.Trim();
        //            }
        //            else
        //            {
        //                return;
        //            }

        //            //if (typedSoFar.Length > 0 && typedSoFar.Length == cboLoc.Text.Length)
        //            //{
        //            //    typedSoFar = typedSoFar.Remove(typedSoFar.Length - 1, 1);
        //            //}
        //        }
        //        else
        //        {
        //            typedSoFar = Convert.ToString(cboLocations.Tag);
        //            if (cboLocations.Text.Trim() != typedSoFar)
        //            {
        //                typedSoFar = cboLocations.Text.Trim();
        //            }
        //            // typedSoFar = typedSoFar + Convert.ToChar(e.KeyValue);
        //        }

        //        cboLocations.Tag = typedSoFar;

        //        DataTable dtList = new DataTable();

        //        if (typedSoFar.Trim().Length >= 1)
        //        {
        //            cboLocations.DataSource = null;
        //            bool setSelection = false;
        //            int selValue = 0;

        //            try
        //            {
        //                if (typedSoFar.ToLower() == "all")
        //                {
        //                    dtList = Locations.getLocationList(cboLocations);
        //                }
        //                else
        //                {
        //                    dtList = Locations.getLocationList(typedSoFar.Trim(), cboLocations);
        //                }

        //                //dtList = Locations.getLocationList(typedSoFar.Trim());

        //                if (dtList != null)
        //                {
        //                    DataRow dr;

        //                    //dr = dtList.NewRow();
        //                    //dr["ID_Location"] = 0;
        //                    //dr["Name"] = "--ALL--";
        //                    //dtList.Rows.Add(dr);
        //                    //dtList.AcceptChanges();

        //                    dr = dtList.NewRow();
        //                    dr["ID_Location"] = 0;
        //                    dr["Name"] = "Select Location";
        //                    dtList.Rows.Add(dr);
        //                    dtList.AcceptChanges();

        //                    if (dtList.Rows.Count == 1)
        //                    {
        //                        dr = dtList.NewRow();

        //                        selValue = -2;

        //                        dr["ID_Location"] = selValue;

        //                        dr["Name"] = typedSoFar;
        //                        dtList.Rows.Add(dr);
        //                        dtList.AcceptChanges();

        //                        setSelection = true;

        //                    }

        //                    cboLocations.ValueMember = "ID_Location";
        //                    cboLocations.DisplayMember = "Name";
        //                    cboLocations.DataSource = dtList;

        //                }
        //                else
        //                {

        //                    LocationItem item = new LocationItem("--SELECT--", "0");
        //                    cboLocations.Items.Add(item);
        //                    if (cboLocations.Items.Count == 1)
        //                    {
        //                        selValue = -2;

        //                        item = new LocationItem(typedSoFar, selValue.ToString());

        //                        cboLocations.Items.Insert(0, item);

        //                        setSelection = true;

        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //            }

        //            //'Select the Appended Text

        //            Utility.setDropDownHeight(cboLocations.Handle, 20);

        //            Utility.setShowDropDown(cboLocations.Handle);

        //            if (setSelection)
        //            {
        //                cboLocations.SelectedValue = selValue;
        //            }

        //            Utility.setSelectionStart((short)typedSoFar.Length, cboLocations.Handle);

        //        }
        //        else
        //        {
        //            //dtList.Columns.Add("ID_Location");
        //            //dtList.Columns.Add("Name");

        //            //DataRow dr = dtList.NewRow();
        //            //dr["ID_Location"] = 0;
        //            //dr["Name"] = "Select Location";
        //            //dtList.Rows.Add(dr);
        //            //dtList.AcceptChanges();

        //            //cboLocations.ValueMember = "ID_Location";
        //            //cboLocations.DisplayMember = "Name";
        //            //cboLocations.DataSource = dtList;

        //            //cboLocations.SelectedValue = 0;

        //            LocationItem item = new LocationItem("Select Location", "0");
        //            cboLocations.Items.Add(item);
        //            cboLocations.SelectedItem = item;

        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

        // Select Page
        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lstAsset.Items)
            {
                if (item.SubItems[2].Text != "NA")
                    item.Checked = true;
            }
        }

        // Clear Button Handler
        private void btnBack_Click(object sender, EventArgs e)
        {
            tagList.Clear();
            epclistbox.Items.Clear();
            lstAsset.Items.Clear();

            barCodeScan = false;

            BCList = null;

            btnConfirm.Visible = false;
            btnSelectAll.Visible = false;
            btnConfirmAll.Visible = false;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.CS101Reader)
            {
                this.lnkLocName.Size = new System.Drawing.Size(266, 15);
                this.btnSelLoc.Location = new System.Drawing.Point(275, 4);
            }            

            cmbPage.Visible = false;
            lblPage.Visible = false;

            cmbPage.SelectedValueChanged -= new EventHandler(cmbPage_SelectedValueChanged);

            btnClear.Visible = false;

            pnlTagDetails.Visible = false;

            pnlTagScan.Visible = true;
            pnlTagScan.BringToFront();
            TagCntLabel.Text = "0";
            btnScan.Visible = true;
            btnScanBarcode.Visible = true;
            btnScanBarcode.Enabled = true;

            btnScan.Enabled = true;

            InTagList = null;
            //EPCListV.Items.Clear();
            epclistbox.Items.Clear();
            this.Text = "Move Items - " + UserPref.CurVersionNo;
            InitScanButtonState();

        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            // change location of selected items to specified location

            int success = 0;
            int itemChecked = 0;
            bool invalidItem = false;
            //Int64 locationID = Convert.ToInt64(cboLocations.SelectedValue);

            Int32 locationID = 0;

            locationID = SelectedLocationID;

            //if (cboLocations.SelectedItem != null)
            //{
            //    LocationItem li = (LocationItem)cboLocations.SelectedItem;
            //    locationID = Convert.ToInt32(li.ID_Location); 

            //}

            if (locationID == 0 || locationID == -99999)
            {
                MessageBox.Show("Please Select any Location.");
                return;
            }

            ArrayList tmpList = new ArrayList();
            StringBuilder tags = new StringBuilder("'-1'");
            ArrayList csvArray = new ArrayList();

            Cursor.Current = Cursors.WaitCursor;

            foreach (ListViewItem item in lstAsset.Items)
            {
                try
                {
                    if (item.Checked == true)
                    {
                        if (item.SubItems[2].Text != "NA")
                        {
                            itemChecked = 1;
                            if (tags.Length >= 6000)
                            {
                                csvArray.Add(tags.ToString());
                                tags = new StringBuilder("'-1'");
                            }
                            tags.Append("," + "'" + item.SubItems[1].Text + "'");
                            success += 1;
                            tmpList.Add(item.Index);
                        }
                        else
                        {
                            invalidItem = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                }
            }


            if (itemChecked == 1)
            {
                csvArray.Add(tags.ToString());
                foreach (string strCsvTag in csvArray)
                {
                    Assets.Update_AssetsLocation(strCsvTag, locationID, 0);
                }

                if (success > 0)
                {
                    //tmpList.Sort();
                    //int i;

                    //for (i = tmpList.Count - 1; i >= 0; i--)
                    //{
                    //    lstAsset.Items.RemoveAt(Convert.ToInt32(tmpList[i]));
                    //}

                    MessageBox.Show(success.ToString() + " item(s) moved to new location.");

                    setScanTags();
                    FillDetailView();
                    lstAsset.Refresh();
                }
                else
                    MessageBox.Show("No records updated.");
            }
            else if (invalidItem == false)
            {
                MessageBox.Show("Please select the item(s) to change Location.");
            }
            else
            {
                MessageBox.Show("Please select valid item(s)");
            }

            Cursor.Current = Cursors.Default;

        }

        private void btnScanBarcode_Click(object sender, EventArgs e)
        {
            InTagList = null;
            epclistbox.Items.Clear();
            this.Text = "Move Items";
            lstAsset.Items.Clear();
            tagList.Clear();
            lstItems.Clear();

            frmBCScanForm fBCSFm = new frmBCScanForm();
            fBCSFm.Owner = this;
            fBCSFm.OpenMode = frmBCScanForm.FormType.Inventoryform;
            fBCSFm.formID = "ReceiveDispatch";
            fBCSFm.disableButtons();
            fBCSFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            fBCSFm.Closed += new EventHandler(this.OnBarCodeClosed);
        }

        private void OnBarCodeClosed(object sender, EventArgs e)
        {
            try
            {
                this.Enabled = true;
                AddListViewItems();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.Message);
            }
        }

        private void frmReceiveDispatch_Closed(object sender, EventArgs e)
        {

            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        #region Paging Code

        int curPage, totalPages;
        public int recPerPage;

        DataTable dtPages;
        private int SetupPaging(int totalRec)
        {
            //int totalRec = EPCListV.Items.Count;
            int totalPages = 0;

            dtPages = new DataTable();
            DataColumn dc = new DataColumn("Page");
            dtPages.Columns.Add(dc);

            DataColumn dcc = new DataColumn("Index");
            dtPages.Columns.Add(dcc);

            if (totalRec > 0)
            {
                totalPages = totalRec / recPerPage;
                if (totalRec % recPerPage != 0)
                    totalPages = totalPages + 1;
            }

            //  cmbPage.Items.Clear();

            DataRow dr;

            //dr = dtPages.NewRow();
            //dr["Page"] = "Select";
            //dr["Index"] = 0;
            //dtPages.Rows.Add(dr);

            for (int i = 1; i <= totalPages; i++)
            {
                dr = dtPages.NewRow();
                dr["Page"] = i.ToString();
                dr["Index"] = i;
                dtPages.Rows.Add(dr);
            }

            dtPages.AcceptChanges();

            cmbPage.DataSource = dtPages;
            cmbPage.DisplayMember = "Page";
            cmbPage.ValueMember = "Index";

            if (totalPages > 0)
                cmbPage.SelectedValue = 1;

            cmbPage.Visible = true;
            lblPage.Visible = true;
            cmbPage.SelectedValueChanged += new EventHandler(cmbPage_SelectedValueChanged);

            return totalPages;
        }

        private void cmbPage_SelectedValueChanged(object sender, EventArgs e)
        {
            if (curPage == Convert.ToInt32(cmbPage.SelectedValue))
                return;
            curPage = Convert.ToInt32(cmbPage.SelectedValue);

            Cursor.Current = Cursors.WaitCursor;

            setScanTags();
            FillDetailView();

            Cursor.Current = Cursors.Default;
        }

        #endregion

        private void btnConfirmAll_Click(object sender, EventArgs e)
        {
            int success = 0;
            // bool invalidItem = false;
            // Int64 locationID = Convert.ToInt64(cboLocations.SelectedValue);
            StringBuilder tags = new StringBuilder("'-1'");
            Int32 locationID = 0;

            locationID = SelectedLocationID;

            //if (cboLocations.SelectedItem != null)
            //{
            //    LocationItem li = (LocationItem)cboLocations.SelectedItem;
            //    locationID = Convert.ToInt32(li.ID_Location); 
            //}

            if (locationID == 0 || locationID == -99999)
            {
                MessageBox.Show("Please Select any Location.");
                return;
            }

            //ListView tempList = null;

            Cursor.Current = Cursors.WaitCursor;

            if (barCodeScan == false)
            {
                //tempList = EPCListV;

                foreach (string item in epclistbox.Items)
                {
                    try
                    {
                        if (tags.Length >= 6000)
                        {
                            success = success + Assets.Update_AssetsLocation(tags.ToString(), locationID, 0);

                            tags = new StringBuilder("'-1'");
                        }

                        tags.Append("," + "'" + item + "'");

                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.Message);
                    }
                }

                if (tags.Length >= 4)
                    success = success + Assets.Update_AssetsLocation(tags.ToString(), locationID, 0);

            }
            else
            {
                tempList = BCList;

                foreach (ListViewItem item in tempList.Items)
                {
                    try
                    {
                        if (tags.Length >= 6000)
                        {
                            success = success + Assets.Update_AssetsLocation(tags.ToString(), locationID, 0);

                            tags = new StringBuilder("'-1'");
                        }

                        tags.Append("," + "'" + item.SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A') + "'");

                    }
                    catch (Exception ex)
                    {
                        Logger.LogError(ex.Message);
                    }
                }

                if (tags.Length >= 4)
                    success = success + Assets.Update_AssetsLocation(tags.ToString(), locationID, 0);

            }

            if (success > 0)
            {
                MessageBox.Show("All item(s) moved to new location.");
                setScanTags();
                FillDetailView();
            }
            else
                MessageBox.Show("No item(s) moved to new location.");

            Cursor.Current = Cursors.Default;

        }

        #region Load Testing Code

        //string itemCSV = "";
        //ArrayList tempArray;

        //private Environment.SpecialFolder folderType = Environment.SpecialFolder.ApplicationData;
        //string folderpath = null;
        //string fileName = "itemCSV.txt";

        //private void AddToList()
        //{
        //    folderpath = Environment.GetFolderPath(folderType);
        //    string filepath = folderpath + @"\" + fileName;

        //    StreamReader sr = new StreamReader(filepath);
        //    itemCSV = sr.ReadToEnd();

        //    sr.Close();

        //    if (itemCSV != null || itemCSV != "")
        //    {
        //        string[] items = itemCSV.Split(',');
        //        tempArray = new ArrayList();
        //        foreach (string item in items)
        //        {
        //            if (!tempArray.Contains(item.ToString()))
        //            {
        //                int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
        //                ListViewItem lvitem = new ListViewItem(new String[] {
        //         (CurNumRows+1).ToString(), item});
        //                // Insert new item to the top of the list for easy viewing
        //                EPCListV.Items.Insert(0, lvitem);
        //                EPCListV.Refresh();
        //                RefreshTagCntLabel();
        //            }
        //        }

        //        ViewDetails();
        //    }

        //}

        //private void ViewDetails()
        //{
        //   // btnScan.Enabled = false;
        //    Cursor.Current = Cursors.WaitCursor;
        //    try
        //    {
        //        //EPCListV.Visible = false;                   
        //        // button1.Visible = true;
        //        // TagCntLabel.Visible = false;
        //        // lstAsset.Visible = true;

        //        if (EPCListV.Items.Count > 0)
        //        {
        //            Program.RefreshStatusLabel(this, "Data Processing..");
        //            pnlTagDetails.Visible = true;
        //            pnlTagScan.Visible = false;

        //            cboLocations.Size = new Size(220, 23);
        //            cmbPage.Visible = true;
        //            lblPage.Visible = true;
        //            cmbPage.BringToFront();

        //            pnlTagDetails.BringToFront();

        //            //recPerPage = 10;
        //            totalPages = SetupPaging(EPCListV.Items.Count);

        //            Application.DoEvents();

        //            curPage = 1;

        //            setScanTags();

        //            FillDetailView();

        //        }
        //        else
        //        {
        //            this.Text = "Move Items : Total Read - " + TagCntLabel.Text;
        //        }


        //        //if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
        //        //{
        //        //    ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
        //        //    LoadTxtBxes(row);
        //        //    // Enable (Avail) Text Fields
        //        //    DataDispPanel.Enabled = true;
        //        //}
        //    }
        //    catch (Exception epx)
        //    {
        //        throw epx;
        //    }
        //    finally
        //    {
        //        Cursor.Current = Cursors.Default;
        //       // btnScan.Enabled = true;
        //       // btnScanBarcode.Enabled = true;
        //    }
        //}

        #endregion

    }
}