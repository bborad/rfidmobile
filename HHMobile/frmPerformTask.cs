/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For FieldService functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmPerformTask : Form
    {

        DataTable dtTask;  
        public frmPerformTask()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        public String TaskID
        {
            set
            {
                cboTemplate.SelectedValue = value;
            }
        }

        private string _AssetName;
        public String AssetName
        {
            set
            {
                _AssetName = value;
            }
        }

        private string _AssetNo;
        public String AssetNo
        {
            set
            {
                _AssetNo = value;
            }
        }

        private string _TagNo;
        public String TagNo
        {
            set
            {
                _TagNo = value;  
            }
            get
            {
                return _TagNo; 
            }
        }

        private Int32 _ItemId;
        public Int32 ItemID
        {
            set
            {
                _ItemId = value;
            }
            get
            {
                return _ItemId; 
            }
        }

        private void txtLoc_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboTemplate.SelectedValue) == 0)
            {
                MessageBox.Show("Please Select Template");
                return;
            }
            try
            {
                //throw new ApplicationException("Function not completed"); 
                DataRow[] drAr;
                FSStatusType fst = FSStatusType.InProcess;

                if (chkComplete.Checked)
                {
                    fst = FSStatusType.Completed;
                }
                foreach (ListViewItem lsi in lstTask.Items)
                {
                    drAr = dtTask.Select("ID_FSAssetStatus="+ lsi.SubItems[2].Text);
                    if (drAr.Length > 0)
                    {
                        if (fst == FSStatusType.Completed)
                        {
                            //completed
                            if(!lsi.Checked)
                                FS_AssetStatus.Update(Convert.ToInt32(drAr[0]["ID_FSAssetStatus"]), fst, FSStatusType.Ignored, txtComments.Text.Trim());
                            else
                                FS_AssetStatus.Update(Convert.ToInt32(drAr[0]["ID_FSAssetStatus"]), fst, FSStatusType.Completed, txtComments.Text.Trim());
                        }
                        else
                        {
                            //in process
                            FS_AssetStatus.Update(Convert.ToInt32(drAr[0]["ID_FSAssetStatus"]), fst, FSStatusType.InProcess, txtComments.Text.Trim());

                        }
                        //if (!lsi.Checked) //Task Box not checked
                        //{
                        //    if (fst == FSStatusType.Completed)
                        //    {
                        //        FS_AssetStatus.Update(Convert.ToInt32(drAr[0]["ID_FSAssetStatus"]), fst, FSStatusType.Ignored, txtComments.Text.Trim());
                        //    }
                        //    else
                        //    {
                        //        FS_AssetStatus.Update(Convert.ToInt32(drAr[0]["ID_FSAssetStatus"]), fst, FSStatusType.InProcess, txtComments.Text.Trim());
                        //    }
                        //}
                        //else
                        //{
                        //    FS_AssetStatus.Update(Convert.ToInt32(drAr[0]["ID_FSAssetStatus"]), fst,FSStatusType.Completed,txtComments.Text.Trim());
                        //}
                    }
                }

                //Task.AddTaskPerformed(Convert.ToInt32(cboTask.SelectedValue), Login.ID, ItemID);
                MessageBox.Show("Tasks recorded for the item.");
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");

                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message); 
            }
        }

        private void cboTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Add Task list for particular Template
            if (cboTemplate.Enabled)
            {

                dtTask = FS_AssetStatus.getTaskList(_ItemId, Convert.ToInt64(cboTemplate.SelectedValue));
                lstTask.Items.Clear();
                lstTask.CheckBoxes = true;
                if (dtTask != null && dtTask.Rows.Count > 0)
                {
                    if (dtTask.Rows[0]["Comments"] != DBNull.Value)
                    {
                        txtComments.Text = Convert.ToString(dtTask.Rows[0]["Comments"]);
                    }
                    ListViewItem lstItem;
                    ListViewItem.ListViewSubItem ls;

                    foreach (DataRow dr in dtTask.Rows)
                    {
                        lstItem = new ListViewItem(dr["Title"].ToString());

                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToDateTime(dr["DueDate"].ToString()).ToString("dd/MM/yy");
                        lstItem.SubItems.Add(ls);

                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString(dr["ID_FSAssetStatus"].ToString());
                        lstItem.SubItems.Add(ls);

                        if (Convert.ToInt32(dr["TaskStatus"]) == Convert.ToInt32(FSStatusType.Completed))
                            lstItem.Checked = true;
                        else
                            lstItem.Checked = false;

                        lstTask.Items.Add(lstItem);

                    }
                    lstTask.Refresh();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmPerformTask_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;
            DataTable dtList = new DataTable();
            dtList = FS_AssetStatus.getTemplateList(_ItemId);   
            //dtList =  Task.getTasksList();  

            DataRow dr = dtList.NewRow();
            dr["ID_Template"] = 0;
            dr["Title"] = "Select Service";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboTemplate.Enabled = false;  
            cboTemplate.ValueMember = "ID_Template";
            cboTemplate.DisplayMember = "Title";
            cboTemplate.DataSource = dtList;

            cboTemplate.SelectedValue = 0;
            cboTemplate.Enabled = true;  

            txtName.Text = _AssetName;  

            

        }

        private void lstAsset_ItemActivate(object sender, EventArgs e)
        {

        }

        private void lstAsset_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void chkComplete_CheckStateChanged(object sender, EventArgs e)
        {

        }


    }
}