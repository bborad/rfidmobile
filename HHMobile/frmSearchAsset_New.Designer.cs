namespace OnRamp
{
    partial class frmSearchAsset_New
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearchAsset_New));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.lnkSubGroups = new System.Windows.Forms.LinkLabel();
            this.lnkGroups = new System.Windows.Forms.LinkLabel();
            this.lnkLocation = new System.Windows.Forms.LinkLabel();
            this.btnSubGroup = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnGroups = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSelectLoc = new System.Windows.Forms.Button();
            this.lblLocation = new System.Windows.Forms.Label();
            this.cmbColumn3 = new System.Windows.Forms.ComboBox();
            this.cmbColumn2 = new System.Windows.Forms.ComboBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.cmbColumn1 = new System.Windows.Forms.ComboBox();
            this.btnSearchOptions = new System.Windows.Forms.Button();
            this.txtValue2 = new System.Windows.Forms.TextBox();
            this.cboSubGroup = new System.Windows.Forms.ComboBox();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtValue3 = new System.Windows.Forms.TextBox();
            this.txtValue1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.uploadCSV = new System.Windows.Forms.OpenFileDialog();
            this.pnlSearchOptions = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.rdbtnExactString = new System.Windows.Forms.RadioButton();
            this.rdbtnWholeString = new System.Windows.Forms.RadioButton();
            this.rdbtnAnyWordDigit = new System.Windows.Forms.RadioButton();
            this.ttLocation = new Microsoft.Samples.CF.ToolTipCF.TooltipCF();
            this.pnlSearch.SuspendLayout();
            this.pnlSearchOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.White;
            this.pnlSearch.Controls.Add(this.lnkSubGroups);
            this.pnlSearch.Controls.Add(this.lnkGroups);
            this.pnlSearch.Controls.Add(this.lnkLocation);
            this.pnlSearch.Controls.Add(this.btnSubGroup);
            this.pnlSearch.Controls.Add(this.label4);
            this.pnlSearch.Controls.Add(this.btnGroups);
            this.pnlSearch.Controls.Add(this.label3);
            this.pnlSearch.Controls.Add(this.btnSelectLoc);
            this.pnlSearch.Controls.Add(this.lblLocation);
            this.pnlSearch.Controls.Add(this.cmbColumn3);
            this.pnlSearch.Controls.Add(this.cmbColumn2);
            this.pnlSearch.Controls.Add(this.btnUpload);
            this.pnlSearch.Controls.Add(this.cmbColumn1);
            this.pnlSearch.Controls.Add(this.btnSearchOptions);
            this.pnlSearch.Controls.Add(this.txtValue2);
            this.pnlSearch.Controls.Add(this.cboSubGroup);
            this.pnlSearch.Controls.Add(this.cboGroup);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.txtValue3);
            this.pnlSearch.Controls.Add(this.txtValue1);
            this.pnlSearch.Controls.Add(this.label2);
            this.pnlSearch.Controls.Add(this.cboLoc);
            resources.ApplyResources(this.pnlSearch, "pnlSearch");
            this.pnlSearch.Name = "pnlSearch";
            // 
            // lnkSubGroups
            // 
            this.lnkSubGroups.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkSubGroups, "lnkSubGroups");
            this.lnkSubGroups.Name = "lnkSubGroups";
            this.lnkSubGroups.Click += new System.EventHandler(this.lnkSubGroups_Click);
            // 
            // lnkGroups
            // 
            this.lnkGroups.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkGroups, "lnkGroups");
            this.lnkGroups.Name = "lnkGroups";
            this.lnkGroups.Click += new System.EventHandler(this.lnkGroups_Click);
            // 
            // lnkLocation
            // 
            this.lnkLocation.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkLocation, "lnkLocation");
            this.lnkLocation.Name = "lnkLocation";
            this.lnkLocation.Click += new System.EventHandler(this.lnkLocation_Click);
            // 
            // btnSubGroup
            // 
            resources.ApplyResources(this.btnSubGroup, "btnSubGroup");
            this.btnSubGroup.Name = "btnSubGroup";
            this.btnSubGroup.Click += new System.EventHandler(this.btnSubGroup_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // btnGroups
            // 
            resources.ApplyResources(this.btnGroups, "btnGroups");
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Click += new System.EventHandler(this.btnGroups_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // btnSelectLoc
            // 
            resources.ApplyResources(this.btnSelectLoc, "btnSelectLoc");
            this.btnSelectLoc.Name = "btnSelectLoc";
            this.btnSelectLoc.Click += new System.EventHandler(this.btnSelectLoc_Click);
            // 
            // lblLocation
            // 
            resources.ApplyResources(this.lblLocation, "lblLocation");
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Tag = "-1";
            // 
            // cmbColumn3
            // 
            resources.ApplyResources(this.cmbColumn3, "cmbColumn3");
            this.cmbColumn3.Name = "cmbColumn3";
            // 
            // cmbColumn2
            // 
            resources.ApplyResources(this.cmbColumn2, "cmbColumn2");
            this.cmbColumn2.Name = "cmbColumn2";
            // 
            // btnUpload
            // 
            resources.ApplyResources(this.btnUpload, "btnUpload");
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // cmbColumn1
            // 
            resources.ApplyResources(this.cmbColumn1, "cmbColumn1");
            this.cmbColumn1.Name = "cmbColumn1";
            // 
            // btnSearchOptions
            // 
            resources.ApplyResources(this.btnSearchOptions, "btnSearchOptions");
            this.btnSearchOptions.Name = "btnSearchOptions";
            this.btnSearchOptions.Click += new System.EventHandler(this.btnSearchOptions_Click);
            // 
            // txtValue2
            // 
            resources.ApplyResources(this.txtValue2, "txtValue2");
            this.txtValue2.Name = "txtValue2";
            // 
            // cboSubGroup
            // 
            resources.ApplyResources(this.cboSubGroup, "cboSubGroup");
            this.cboSubGroup.Name = "cboSubGroup";
            // 
            // cboGroup
            // 
            resources.ApplyResources(this.cboGroup, "cboGroup");
            this.cboGroup.Name = "cboGroup";
            this.cboGroup.SelectedIndexChanged += new System.EventHandler(this.cboGroup_SelectedIndexChanged);
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtValue3
            // 
            resources.ApplyResources(this.txtValue3, "txtValue3");
            this.txtValue3.Name = "txtValue3";
            // 
            // txtValue1
            // 
            resources.ApplyResources(this.txtValue1, "txtValue1");
            this.txtValue1.Name = "txtValue1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            // 
            // uploadCSV
            // 
            this.uploadCSV.FileName = "openFileDialog1";
            // 
            // pnlSearchOptions
            // 
            this.pnlSearchOptions.BackColor = System.Drawing.SystemColors.Window;
            this.pnlSearchOptions.Controls.Add(this.btnOk);
            this.pnlSearchOptions.Controls.Add(this.rdbtnExactString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnWholeString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnAnyWordDigit);
            resources.ApplyResources(this.pnlSearchOptions, "pnlSearchOptions");
            this.pnlSearchOptions.Name = "pnlSearchOptions";
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rdbtnExactString
            // 
            resources.ApplyResources(this.rdbtnExactString, "rdbtnExactString");
            this.rdbtnExactString.Name = "rdbtnExactString";
            this.rdbtnExactString.Click += new System.EventHandler(this.rdbtnExactString_Click);
            // 
            // rdbtnWholeString
            // 
            resources.ApplyResources(this.rdbtnWholeString, "rdbtnWholeString");
            this.rdbtnWholeString.Name = "rdbtnWholeString";
            this.rdbtnWholeString.Click += new System.EventHandler(this.rdbtnWholeString_Click);
            // 
            // rdbtnAnyWordDigit
            // 
            resources.ApplyResources(this.rdbtnAnyWordDigit, "rdbtnAnyWordDigit");
            this.rdbtnAnyWordDigit.Name = "rdbtnAnyWordDigit";
            this.rdbtnAnyWordDigit.Click += new System.EventHandler(this.rdbtnAnyWordDigit_Click);
            // 
            // ttLocation
            // 
            this.ttLocation.AutoPopDelay = 2000;
            this.ttLocation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ttLocation.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular);
            this.ttLocation.ForeColor = System.Drawing.Color.Black;
            this.ttLocation.InitialDelay = 500;
            // 
            // frmSearchAsset_New
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.pnlSearchOptions);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearchAsset_New";
            this.Load += new System.EventHandler(this.frmSearchAsset_Load);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearchOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.TextBox txtValue3;
        private System.Windows.Forms.TextBox txtValue1;
        private System.Windows.Forms.ComboBox cboGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboSubGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog uploadCSV;
        private System.Windows.Forms.TextBox txtValue2;
        private System.Windows.Forms.Button btnSearchOptions;
        private System.Windows.Forms.Panel pnlSearchOptions;
        private System.Windows.Forms.RadioButton rdbtnAnyWordDigit;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.RadioButton rdbtnExactString;
        private System.Windows.Forms.RadioButton rdbtnWholeString;
        private System.Windows.Forms.ComboBox cmbColumn3;
        private System.Windows.Forms.ComboBox cmbColumn2;
        private System.Windows.Forms.ComboBox cmbColumn1;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectLoc;
        private System.Windows.Forms.Button btnSubGroup;
        private System.Windows.Forms.Button btnGroups;
        private Microsoft.Samples.CF.ToolTipCF.TooltipCF ttLocation;
        private System.Windows.Forms.LinkLabel lnkLocation;
        private System.Windows.Forms.LinkLabel lnkGroups;
        private System.Windows.Forms.LinkLabel lnkSubGroups;
    }
}