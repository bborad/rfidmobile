using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;
//using ClsReaderLib.Devices.Barcode;
using HHDeviceInterface.BarCode;

namespace OnRamp
{
    public partial class TagAuthenForm : Form
    {
        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        enum BCRdOpState
        {
            Idle,
            Scanning,
            Stopping,
        }

        enum RfidRdOpState
        {
            Idle,
            Starting,
            Running,
            Stopping,
        }

        enum AuthenOpState
        {
            Idle,
            Starting,
            Running,
            Stopping,
        }

        private static bool ContScan = true; // Continue scanning after capture
        
        public TagAuthenForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            StartBttnInitState();
        }
   
        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (StartBttnState() != AuthenOpState.Idle)
            {
                MessageBox.Show ("Please stop authentication first");
                e.Cancel = true;
            }
            else if (BCAuth != null || RfidAuth != null)
            {
                e.Cancel = (Program.AskUserConfirm("This would delete all unsaved results.\nProceed?") != DialogResult.OK);
            }
        }

        private void TextLabelFlash(Label lbl, Color color)
        {
            Color OrigBackColor = lbl.BackColor;
            lbl.BackColor = color;
            lbl.Refresh();
            // instead of sleep, do counting to avoid (serial port)thread switch
            //System.Threading.Thread.Sleep(150);
            for (int i = 0; i < 1000; i++) ; // choice of number is important
            lbl.BackColor = OrigBackColor;
        }

        private void TextLabelFlashGreen(Label lbl)
        {
            TextLabelFlash(lbl, Color.Green);
        }

        private void TextLabelFlashBlue(Label lbl)
        {
            TextLabelFlash(lbl, Color.Blue);
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }
        }

        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11:
                case eVKey.VK_F19:
                    HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    {
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if (StartBttnState() == AuthenOpState.Idle)
                            {
                                OnStartBttnClicked(this, null);
                            }
                        }
                        else // up
                        {
                            if (StartBttnState() == AuthenOpState.Running)
                            {
                                // Stop!
                                OnStartBttnClicked(this, null);
                            }
                        }
                    }
                break;
            case eVKey.VK_F4:
            case eVKey.VK_F5:
                if (down)
                {
                    if (StartBttnState() == AuthenOpState.Idle)
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                }
                break;
                case eVKey.VK_F1:
                    if (down && StartBttnState() == AuthenOpState.Idle)
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
               // case Keys.F7:
               // case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (StartBttnState() != AuthenOpState.Idle)
                    {
                        MessageBox.Show("Please stop authentication first");
                      
                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }


        private String lastOpenDir = null;
        private void OnFileOpnClicked(object sender, EventArgs e)
        {
            OpenFileDialog OpnFileDlg = new OpenFileDialog();
            if (lastOpenDir != null)
                OpnFileDlg.InitialDirectory = lastOpenDir;
            OpnFileDlg.Filter = "CSV Files (*.csv)|*.csv|All Files (*.*)|*.*";
            OpnFileDlg.FilterIndex = 1;
            DialogResult Res = OpnFileDlg.ShowDialog();
            if (Res == DialogResult.OK)
            {
                String AuthFullFileName = "";
                try
                {
                    lastOpenDir = Path.GetDirectoryName(OpnFileDlg.FileName);
                    AuthFullFileName = OpnFileDlg.FileName;
                    if (RfidAuth != null || BCAuth != null)
                    {
                        if (Program.AskUserConfirm("This would clear all authentication results. Proceed?") != DialogResult.OK)
                            return;
                        else
                            ClrAuthResults();
                    }
                    LoadAuthFile(AuthFullFileName,  out loadedIDs);
                    RefreshItemsCntLbl(loadedIDs);
                    UpdateTotIDCntLbls();
                }
                catch
                {
                    AuthFullFileName = "";
                }
                AuthFileLbl.Text = Path.GetFileName(AuthFullFileName);
            }
            OpnFileDlg.Dispose(); // required for OpenFileDialog?
            StartBttnChkEnable();
        }

        /// <summary>
        /// Load CSV file into a String Array
        /// </summary>
        /// <param name="AuthFileName"></param>
        private bool LoadAuthFile(String AuthFileName, out String[] Ids)
        {
            List<String> IdLst = new List<String>();
            bool Succ = false;

            Ids = new String[0];

            try
            {
                using (StreamReader sr = new StreamReader(AuthFileName))
                {
                    String line;
                    char comma = ',';
                    while ((line = sr.ReadLine()) != null)
                    {
                        String[] Splits = line.Split(comma);
                        foreach (String Id in Splits)
                        {
                            String TrimId = Id.Trim();
                            if (String.Compare(TrimId, String.Empty) != 0)
                            {
                                IdLst.Add(TrimId);
                            }
                        }
                    }
                    Succ = true;
                    // convert List to Array
                    Ids = IdLst.ToArray();
                }
            }
            catch (Exception e)
            {
                Program.ShowError("Error occurred during load: " + e.Message);
                Succ = false;
            }

            return Succ;
        }

        #region User supplied ID List
        private String[] loadedIDs;

        private void RefreshItemsCntLbl(String[] items)
        {
            if (items != null && items.Length > 0)
                ItemsCntLbl.Text = items.Length.ToString();
            else
                ItemsCntLbl.Text = "0";
        }

        // Assuming that the IDs are in alternative rows
        private bool SplitIDArr(String[] allIDs, out String[] id1s, out String[] id2s)
        {
            id1s = null;
            id2s = null;
            if (allIDs == null || allIDs.Length < 2)
                return false;
            int FullLen = allIDs.Length;
            if ((allIDs.Length % 0x1) == 1)
                FullLen -= 1;
            id1s = new String[FullLen / 2];
            id2s = new String[FullLen / 2];
            for (int i = 0, j = 1, k=0; i < FullLen && j < FullLen; i += 2, j += 2, k++)
            {
                id1s[k] = allIDs[i];
                id2s[k] = allIDs[j];
            }
            return true;
        }

        private void ShowIDLst(String[] items)
        {
            LstViewSavForm LstVwFm = new LstViewSavForm(false);
            LstVwFm.Title = "ID List (loaded from file)";
            int NumCols = (this.RfidChkBx.Checked ? 1 : 0) + (this.BCChkBx.Checked ? 1 : 0);
            
            if (NumCols == 0)
            {
                Program.ShowError("Could not fulfill request.\n"
                    + "Please select either/both from RfidTag and BarCode checkboxes");
            }
            else if (NumCols == 2)
            {
                LstVwFm.SetColumns(new String[2] { "RFID", "BarCode" }, new int[2] { -2, -2 });
            }
            else if (RfidChkBx.Checked)
            {
                LstVwFm.SetColumns(new String[1] { "RFID"}, new int[1]{-2});
            }
            else // Barcode
            {
                LstVwFm.SetColumns(new String[1] { "BarCode" }, new int[1]{-2});
            }

            if (items != null && items.Length > 0)
            {
                if (NumCols == 2)
                {
                    String[] Rfids;
                    String[] BCs;
                    if (SplitIDArr(items, out Rfids, out BCs))
                    {
                        for (int i = 0; i < Rfids.Length; i++)
                            LstVwFm.AddRow(new String[2] { Rfids[i], BCs[i] });
                    }
                }
                else if (NumCols == 1)
                {
                    foreach (String Id in items)
                    {
                        LstVwFm.AddRow(new String[1] { Id });
                    }
                }
            }
            if (NumCols > 0)
            {
                LstVwFm.ShowDialog();
                LstVwFm.Dispose();
            }
        }

        private void ShowDetailAuthResult(String itemName, Authentication auth)
        {
            LstViewSavForm LstVwSavFm = new LstViewSavForm(true);
            LstVwSavFm.Title = itemName + " Authentication Results";
            LstVwSavFm.SetColumns(new String[1] { itemName }, new int[1] { -2 });
            int NumIDs = auth.IDCnt;
            bool ValidID = false;
            String IDStr = String.Empty;
            for (int i = 0; i < NumIDs; i++)
            {
                if (auth.GetID(i, out IDStr, out ValidID))
                {
                    if (ValidID)
                        LstVwSavFm.AddRow(new String[1] { IDStr }, Color.Green);
                    else
                        LstVwSavFm.AddRow(new String[1] { IDStr });
                }
            }
            NumIDs = auth.OutOfLstIDCnt;
            for (int i = 0; i < NumIDs; i++)
            {
                if (auth.GetOutOfLstID(i, out IDStr))
                    LstVwSavFm.AddRow(new String[1] { IDStr }, Color.Blue);
            }
        
            LstVwSavFm.ShowDialog();
            LstVwSavFm.Dispose();
        }

        private void ShowDetailAuthResult(String item1Name, Authentication auth1,
            String item2Name, Authentication auth2)
        {
            LstViewSavForm LstVwSavFm = new LstViewSavForm(true);
            LstVwSavFm.Title = item1Name + " & " + item2Name + " Authentication Results";
            LstVwSavFm.SetColumns(new String[2] { item1Name, item2Name }, new int[2] { -2, -2 });

            int NumID1s = auth1.IDCnt;
            int NumID2s = auth2.IDCnt;
            bool ID1Valid = false, ID2Valid = false;
            String ID1Str = String.Empty;
            String ID2Str = String.Empty;
            for (int i = 0, j = 0; i < NumID1s && j < NumID2s; i++, j++)
            {
                // if either one is invalid, mark the row invalid
                if (auth1.GetID(i, out ID1Str, out ID1Valid)
                    && auth2.GetID(j, out ID2Str, out ID2Valid))
                {
                    if (ID1Valid && ID2Valid)
                        LstVwSavFm.AddRow(new String[2] { ID1Str, ID2Str }, Color.Green);
                    else
                        LstVwSavFm.AddRow(new String[2] { ID1Str, ID2Str });
                }
            }
            
            int NumIDs = auth1.OutOfLstIDCnt;
            for (int i = 0; i < NumIDs; i++)
            {
                if (auth1.GetOutOfLstID(i, out ID1Str))
                    LstVwSavFm.AddRow(new String[2] { ID1Str, String.Empty }, Color.Blue);
            }
            NumIDs = auth2.OutOfLstIDCnt;
            for (int i = 0; i < NumIDs; i++)
            {
                if (auth2.GetOutOfLstID(i, out ID2Str))
                    LstVwSavFm.AddRow(new String[2] { String.Empty, ID2Str }, Color.Blue);
            }
            LstVwSavFm.ShowDialog();
            LstVwSavFm.Dispose();
        }

        #endregion
        private void OnVwBttnClicked(object sender, EventArgs e)
        {
            if (BCAuth == null && RfidAuth == null)
                // Show Loaded List
                ShowIDLst(loadedIDs);
            else if (! RfidChkBx.Checked && BCChkBx.Checked && BCAuth != null)
            {
                // Show Loaded List + Detail Authentication results
                ShowDetailAuthResult("BarCode", BCAuth);
            }
            else if (!BCChkBx.Checked && RfidChkBx.Checked && RfidAuth != null)
            {
                ShowDetailAuthResult("RFID", RfidAuth);
            }
            else if (BCChkBx.Checked && BCAuth != null
           && RfidChkBx.Checked && RfidAuth != null)
            {
                ShowDetailAuthResult("RFID", RfidAuth, "BarCode", BCAuth);
            }
        }

        /// <summary>
        /// Update TotRfidCntLbl and TotBCCntLbl based on CheckBox states
        /// </summary>
        private void UpdateTotIDCntLbls()
        {
            int NumItems = (RfidChkBx.Checked ? 1 : 0) + (BCChkBx.Checked ? 1 : 0);
            switch (NumItems)
            {
                case 2:
                    if (loadedIDs != null)
                    {
                        TotRfidCntLbl.Text = (loadedIDs.Length/ 2).ToString();
                        TotBCCntLbl.Text = TotRfidCntLbl.Text;
                    }
                    break;
                case 1:
                    if (loadedIDs != null)
                    {
                        if (RfidChkBx.Checked)
                        {
                            TotRfidCntLbl.Text = loadedIDs.Length.ToString();
                            TotBCCntLbl.Text = "0";
                        }
                        else
                        {
                            TotRfidCntLbl.Text = "0";
                            TotBCCntLbl.Text = loadedIDs.Length.ToString();
                        }
                    }
                    break;
                case 0:
                    TotRfidCntLbl.Text = "0";
                    TotBCCntLbl.Text = TotRfidCntLbl.Text;
                    break;
            }
        }

        private void OnRFIDChkChanged(object sender, EventArgs e)
        {
            UpdateTotIDCntLbls();
            StartBttnChkEnable();
        }

        private void OnBCChkChanged(object sender, EventArgs e)
        {
            UpdateTotIDCntLbls();
            StartBttnChkEnable();
        }

        #region Start Button

        // return false if neither is started
        // Assumes that the RfidChkBx/BCChkBx checked value did not change
        // if RfidAuth and/or BCAuth are not 'null'
        private bool StartAuthentication(out bool RfidScanStarting, out bool BCScanStarted)
        {
            RfidScanStarting = false;
            BCScanStarted = false;

            if (loadedIDs == null || loadedIDs.Length <= 0)
            {
                MessageBox.Show("List of IDs not loaded or empty,", "Fail");
                return false;
            }
            if (RfidChkBx.Checked && BCChkBx.Checked)
            {
                // create separate Arrays for BC and Rfid IDs
                String[] Rfids;
                String[] BCs;
                if (SplitIDArr(loadedIDs, out Rfids, out BCs))
                {
                    if (RfidAuth == null)
                        RfidAuth = new Authentication(Rfids);
                    if (BCAuth == null)
                        BCAuth = new Authentication(BCs);
                }
                else
                {
                    if (Program.AskUserConfirm("List of IDs not loaded or empty, continue?") != DialogResult.OK)
                    {
                        return false;
                    }
                }
            }
            else if (BCChkBx.Checked)
            {
                if (BCAuth == null)
                    BCAuth = new Authentication(loadedIDs);
            }
            else // RFID checked only
            {
                if (RfidAuth == null)
                    RfidAuth = new Authentication(loadedIDs);
            }

            if (BCChkBx.Checked)
            {
                BCScanStarted = BCScanStart();
                if (!BCScanStarted)
                {
                    Program.ShowError("Barcode Scan Failed to Start");
                }
            }
            if (RfidChkBx.Checked)
            {
                RfidScanStarting = RfidScanStart();
                if (!RfidScanStarting)
                {
                    Program.ShowError("RFID Scan Failed to Start");
                }
            }
            return (BCScanStarted || RfidScanStarting);
        }

        private bool StopAuthentication(out bool RfidScanStopping, out bool BCScanStopped,
            out bool BCScanStopping)
        {
            RfidScanStopping = false;
            BCScanStopped = false;
            BCScanStopping = false;

            if (BCChkBx.Checked)
            {
                if (BCScanStop())
                    BCScanStopped = true;
                else
                    BCScanStopping = true;
            }

            if (RfidChkBx.Checked)
            {
                if (RfidScanStop())
                    RfidScanStopping = true;
            }

            return true;
        }
        
        // Assuming that StartBttn is only enabled when at least one
        // of the checkboxes are in Checked state
        private void OnStartBttnClicked(object sender, EventArgs e)
        {
            switch (StartBttnState())
            {
                case AuthenOpState.Idle:
                    Program.RefreshStatusLabel(this, "Starting...");
                    StartBttnSetState(AuthenOpState.Starting);
                    // Start Operation
                    bool RfidStarted, BCStarted;
                    if (StartAuthentication(out RfidStarted, out BCStarted))
                    {
                        if (!RfidStarted && BCStarted)
                        {
                            // go to Running state (BC does not have Starting...)
                            Program.RefreshStatusLabel(this, "Running...");
                            StartBttnSetState(AuthenOpState.Running);
                        }
                    }
                    else
                    {
                        Program.RefreshStatusLabel(this, "Canceled");
                        StartBttnSetState(AuthenOpState.Idle);
                    }
                    break;
                case AuthenOpState.Running:
                    Program.RefreshStatusLabel(this, "Stopping..");
                    StartBttnSetState(AuthenOpState.Stopping);
                    // Stop Operation
                    bool RfidStopping, BCStopping, BCStopped;
                    if (StopAuthentication(out RfidStopping, out BCStopped, out BCStopping))
                    {
                        if (!RfidStopping && BCStopped)
                        {
                            // Go directly to Idle state
                            Program.RefreshStatusLabel(this, "Finished");
                            StartBttnSetState(AuthenOpState.Idle);
                        }
                    }
                    else
                    {
                        Program.RefreshStatusLabel(this, "Canceled");
                        StartBttnSetState(AuthenOpState.Idle);
                    }
                    break;
                default:
                    // Do nothing
                    break;
            }


        }
        #endregion

        #region Start Button State Routines
        private void StartBttnInitState()
        {
            AuthenOpState State = AuthenOpState.Idle;

            StartBttn.Tag = State;
        }

        private AuthenOpState StartBttnState()
        {
            AuthenOpState State = AuthenOpState.Idle;

            if (StartBttn.Tag is AuthenOpState)
            {
                State = (AuthenOpState)StartBttn.Tag;
            }
            else
            {
                throw new ApplicationException("StartBttn Tag is not AuthenOpState");
            }
            return State;
        }

        private void StartBttnSetState(AuthenOpState newState)
        {
            StartBttn.Tag = newState; // boxing
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagAuthenForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagCommForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagCommForm));
            }

            switch (newState)
            {
                case AuthenOpState.Idle:
                    StartBttn.Text = resources.GetString("StartBttn.Text");
                    // re-enable button
                    StartBttn.Enabled = true;
                    ClrBttn.Enabled = true;
                    OpnFileBttn.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
                case AuthenOpState.Starting:
                    StartBttn.Enabled = false;
                    ClrBttn.Enabled = false;
                    OpnFileBttn.Enabled = false;
                    UserInputsFreeze();
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case AuthenOpState.Running:
                    StartBttn.Text = "Stop";
                    StartBttn.Enabled = true;
                    ClrBttn.Enabled = false;
                    OpnFileBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case AuthenOpState.Stopping:
                    StartBttn.Enabled = false;
                    ClrBttn.Enabled = false;
                    OpnFileBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
            }
        }

        private void UserInputsFreeze()
        {
            RfidChkBx.Enabled = false;
            BCChkBx.Enabled = false;
        }

        private void UserInputsThaw()
        {
            RfidChkBx.Enabled = true;
            BCChkBx.Enabled = true;
        }
        #endregion

        private void StartBttnChkEnable()
        {
            StartBttn.Enabled = (RfidChkBx.Checked || BCChkBx.Checked) 
                        && ((loadedIDs != null && loadedIDs.Length > 0));
        }

        #region Labels
        private void RefreshAuthCntLbl(Label lbl, Authentication auth)
        {
            if (auth == null)
                lbl.Text = "0";
            else
                lbl.Text = auth.AuthCnt.ToString();
        }

        private void RefreshExtrCntLbl(Label lbl, Authentication auth)
        {
            if (auth == null)
                lbl.Text = "0";
            else
                lbl.Text = auth.OutOfLstIDCnt.ToString();
        }
        #endregion
        
        #region RFID Rdr(new)
        private RfidRdOpState _RfidRdrState = RfidRdOpState.Idle;

        private RfidRdOpState RfidRdrState
        {
            set
            {
                _RfidRdrState = value;
                if (RfidRdrState == RfidRdOpState.Idle)
                {
                    // StartBttn to 'Idle' state?
                    if (BCRdrState == BCRdOpState.Idle)
                    {
                        StartBttnSetState(AuthenOpState.Idle);
                        Program.RefreshStatusLabel(this, "Finished");
                    }
                    else if (BCRdrState == BCRdOpState.Scanning)
                    {
                        StartBttnSetState(AuthenOpState.Running);
                    }
                }
                else if ((RfidRdrState == RfidRdOpState.Running)
                    && (StartBttnState() == AuthenOpState.Starting))
                {
                    Program.RefreshStatusLabel(this, "Running...");
                    StartBttnSetState(AuthenOpState.Running);
                }
            }
            get
            {
                return _RfidRdrState;
            }
        }

        private Authentication RfidAuth = null;

        private bool RfidScanStart()
        {
            bool Succ = false;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();

            switch (RfidRdrState)
            {
                case RfidRdOpState.Idle:
                    RfidRdrState = RfidRdOpState.Starting;
                    //Rdr.InvtryOpStEvent += new EventHandler<InvtryOpEventArgs>(InvtryOpEvtHandler);
                    //Rdr.DscvrTagEvent += new EventHandler<DscvrTagEventArgs>(DscvrTagEvtHandler);
                    Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                    // Perform continous inventory until stop requested
                    //UserPref Pref = UserPref.GetInstance();
                    //Byte[] BnkMask; uint WdOffset;
                    //Pref.GetEPCBnkSelMask(out BnkMask, out WdOffset);
                    // No Duplicate Filtering
                    if (Rdr.SetRepeatedTagObsrvMode(true) == false)
                        Program.ShowError("Failed to disable Duplicate-Tag-Filtering");
                    if ((Succ = Rdr.TagInventoryStart(5)) == false) 
                    {
                        // rewind previous setup
                        RfidRdrState = RfidRdOpState.Idle;
                        //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    }
                    break;
                case RfidRdOpState.Running:
                    Succ = true; // Already running
                    break;
            }
            return Succ;
        }

        private bool RfidScanStop()
        {
            bool Succ = false;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (RfidRdrState)
            {
                case RfidRdOpState.Running:
                    RfidRdrState = RfidRdOpState.Stopping;
                    if ((Succ = Rdr.TagInventoryStop()) == false)
                    {
                        RfidRdrState = RfidRdOpState.Idle;
                        //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    }
                    break;
                case RfidRdOpState.Idle:
                    Succ = true; // Already stopped
                    break;
            }
            return Succ;
        }

        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            if (RfidAuth.AuthID(e.EPC.ToString()))
            {
                RefreshAuthCntLbl(AuthRfidCntLbl, RfidAuth);
                TextLabelFlashGreen(AuthRfidCntLbl);
            }
            else
            {
                RefreshExtrCntLbl(ExtraRfidCntLbl, RfidAuth);
                TextLabelFlashBlue(ExtraRfidCntLbl);
            }
            // without duplicate-filtering, moving this statement to the end makes
            // a big difference!
            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        }

        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            RfidRdOpState state = RfidRdrState;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    RfidRdrState = RfidRdOpState.Running;
                    break;
                case InvtryOpStatus.cycBegin:
                    //Program.RefreshStatusLabel(this, "InvCyc  Begins");
                    break;
                case InvtryOpStatus.cycEnd:
                    //Program.RefreshStatusLabel(this, "InvCyc  Ends");
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            Program.PromptUserRestartRadio();
                        else
                            MessageBox.Show(e.msg, "Tag Inventory Stopped with Error");
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped && RfidRdrState != RfidRdOpState.Stopping)
                    {
                        // stop because of hardware error
                        Program.ShowError(e.msg);
                    }
                    RfidRdrState = RfidRdOpState.Idle;
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (state)
                    {
                        case RfidRdOpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case RfidRdOpState.Running:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case RfidRdOpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case RfidRdOpState.Idle:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    RfidRdrState = RfidRdOpState.Idle;
                    break;
                default:
                    break;
            }
        }
        #endregion

        #region Barcode Rdr (new)
        private BCRdOpState _BCRdrState = BCRdOpState.Idle;
        private BCRdOpState BCRdrState
        {
            set
            {
                _BCRdrState = value;
                if (BCRdrState == BCRdOpState.Idle)
                {
                    // StartBttn to 'Idle' state?
                    if (RfidRdrState == RfidRdOpState.Idle)
                    {
                        StartBttnSetState(AuthenOpState.Idle);
                        Program.RefreshStatusLabel(this, "Finished");
                    }
                }
            }
            get
            {
                return _BCRdrState;
            }
        }

        private Authentication BCAuth = null;

        private bool BarCodeNotify(bool succ, String bcStr, String errMsg)
        {
            if (succ)
            {
                // TBD: Update Sound trigger here
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                if (BCAuth.AuthID(bcStr))
                {
                    RefreshAuthCntLbl(AuthBCCntLbl, BCAuth);
                    TextLabelFlashGreen(AuthBCCntLbl);
                }
                else
                {
                    RefreshExtrCntLbl(ExtraBCCntLbl, BCAuth);
                    TextLabelFlashBlue(ExtraBCCntLbl);
                }
            }
            else
            {
                Program.RefreshStatusLabel(this, "Barcode Capture Error...");
                MessageBox.Show(errMsg, "Bar Code Reader Error");
            }

            bool ToCont = (ContScan) && (BCRdrState != BCRdOpState.Stopping);
            if (!ToCont)
            {
                Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
                BCRdrState = BCRdOpState.Idle;
            }
            return ToCont;
        }

        private void BCScanStopped()
        {
            Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
            BCRdrState = BCRdOpState.Idle;
        }

        private bool BCScanStart()
        {
            //BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
            bool Succ = false;
            switch (BCRdrState)
            {
                case BCRdOpState.Idle:
                    BCRdrState = BCRdOpState.Scanning;
                    if (this.InvokeRequired)
                    {
                        Rdr.notifyee = this;
                    }
                    Rdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
                    //Succ = Rdr.ScanStart(BarCodeNotify, this);
                    Succ = Rdr.ScanStart();
                    if (!Succ)
                        BCRdrState = BCRdOpState.Idle;
                    break;
            }
            return Succ;
        }

        private bool BCScanStop()
        {
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
            //BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            bool Succ = false;
            if (BCRdrState == BCRdOpState.Scanning)
            {
                Program.RefreshStatusLabel(this, "Stopping Barcode Scan...");
                BCRdrState = BCRdOpState.Stopping;
                if (this.InvokeRequired)
                {
                    Rdr.stopNotifee = this;
                }
                Rdr.RegisterStopNotificationEvent(BCScanStopped);
                //if ((Succ = Rdr.ScanTryStop(BCScanStopped, this)))
                if ((Succ = Rdr.ScanTryStop()))
                {
                    Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
                    BCRdrState = BCRdOpState.Idle;
                }
                else
                {
                    // Scanner busy at the moment
                    // Leave the 'Stopping' State as to return 'not-to-continue'
                    //  on the next bar-code read notification
                    // MessageBox.Show("Scanner Busy", "Stop Denied");
                }
            }
            else if (BCRdrState == BCRdOpState.Idle)
                Succ = true; // it's already stopped
            return Succ;
        }
        #endregion

        class Authentication
        {
            private int authCnt = 0; // number of 'true's in idAuthState
            private bool[] idAuthState; // shadow array of LoadedIDs
            // TBD: consider changing to other data-structures to improve search speed
            private String[] loadedIDs;
            private List<String> unmatchedIDs = new List<string>();

            public int AuthCnt
            {
                get 
                {
                    return authCnt;
                }
            }
            
            public int IDCnt
            {
                get
                {
                    return loadedIDs.Length;
                }
            }

            public int OutOfLstIDCnt
            {
                get
                {
                    return unmatchedIDs.Count;
                }
            }

            public bool GetID(int idx, out String idStr, out bool valid)
            {
                idStr = String.Empty; valid = false;
                if (loadedIDs == null || loadedIDs.Length <= 0)
                    return false;
                if (idx < 0 || idx >= loadedIDs.Length)
                    return false;
                idStr = loadedIDs[idx];
                valid = idAuthState[idx];
                return true;
            }

            public bool GetOutOfLstID(int idx, out String idStr)
            {
                idStr = String.Empty;

                if (unmatchedIDs.Count == 0)
                    return false;
                if (idx < 0 || idx >= unmatchedIDs.Count)
                    return false;
                idStr = unmatchedIDs[idx];
                return true;
            }

            public Authentication(String[] ids)
            {
                if (ids == null)
                {
                    loadedIDs = new string[0];
                    idAuthState = new bool[0];
                }
                else
                {
                    loadedIDs = ids;
                    idAuthState = new bool[ids.Length]; // default is 'false'
                }
            }

            /// <summary>
            /// Check ID against the loaded list
            /// </summary>
            /// <param name="scannedBC"></param>
            /// <returns>false if not in list</returns>
            public bool AuthID(String scannedID)
            {
                int Idx;
                bool InLst = false;
               if ((InLst = Lookup(scannedID, out Idx)))
               {
                   if (idAuthState[Idx] == false)
                       authCnt++;
                   idAuthState[Idx] = true;
               }
               else
               {
                   if (! unmatchedIDs.Contains(scannedID))
                    unmatchedIDs.Add(scannedID);
               }
                return InLst;
            }

            private bool Lookup(String id, out int idx)
            {
                idx = -1;
                bool Found = false;

                for (int i = 0; i < loadedIDs.Length && !Found; i++)
                {
                    if (id.Equals(loadedIDs[i]))
                    {
                        Found = true;
                        idx = i;
                        break;
                    }
                }
                return Found;
            }
        }

        // Clear current Authentication Result
        private void OnClrBttnClicked(object sender, EventArgs e)
        {
            // Prompt to avoid accidental press
            if (Program.AskUserConfirm("This would destroy the authentication result. Proceed?") == DialogResult.OK)
            {
                ClrAuthResults();
            }
        }

        // Refresh Form controls to reflect results
        private void ClrAuthResults()
        {
            RfidAuth = null;
            BCAuth = null;
            RefreshAuthCntLbl(AuthRfidCntLbl, RfidAuth);
            RefreshExtrCntLbl(ExtraRfidCntLbl, RfidAuth);
            RefreshAuthCntLbl(AuthBCCntLbl, BCAuth);
            RefreshExtrCntLbl(ExtraBCCntLbl, BCAuth);
            UserInputsThaw();
        }
     
    }
}