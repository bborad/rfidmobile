namespace OnRamp
{
    partial class TagSearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagSearchForm));
            this.label1 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.SrchByRateBttn = new System.Windows.Forms.RadioButton();
            this.SrchByRSSIBttn = new System.Windows.Forms.RadioButton();
            this.SrchBttn = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.FilterResChkBx = new System.Windows.Forms.CheckBox();
            this.RssiLbl = new System.Windows.Forms.Label();
            this.FlshBrdrTmr = new System.Windows.Forms.Timer();
            this.OORDetectTmr = new System.Windows.Forms.Timer();
            this.lblAssetNo = new System.Windows.Forms.Label();
            this.lblAssetName = new System.Windows.Forms.Label();
            this.btnTask = new System.Windows.Forms.Button();
            this.btnChngLoc = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtprefix = new System.Windows.Forms.TextBox();
            this.MaskInput = new OnRamp.EPCMaskInput();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // SrchByRateBttn
            // 
            resources.ApplyResources(this.SrchByRateBttn, "SrchByRateBttn");
            this.SrchByRateBttn.Name = "SrchByRateBttn";
            this.SrchByRateBttn.TabStop = false;
            // 
            // SrchByRSSIBttn
            // 
            this.SrchByRSSIBttn.Checked = true;
            resources.ApplyResources(this.SrchByRSSIBttn, "SrchByRSSIBttn");
            this.SrchByRSSIBttn.Name = "SrchByRSSIBttn";
            // 
            // SrchBttn
            // 
            resources.ApplyResources(this.SrchBttn, "SrchBttn");
            this.SrchBttn.Name = "SrchBttn";
            this.SrchBttn.Click += new System.EventHandler(this.OnSrchBttnClicked);
            // 
            // progressBar1
            // 
            resources.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Maximum = 90;
            this.progressBar1.Minimum = 60;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Value = 60;
            // 
            // FilterResChkBx
            // 
            this.FilterResChkBx.Checked = true;
            this.FilterResChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.FilterResChkBx, "FilterResChkBx");
            this.FilterResChkBx.Name = "FilterResChkBx";
            // 
            // RssiLbl
            // 
            this.RssiLbl.BackColor = System.Drawing.Color.Gray;
            resources.ApplyResources(this.RssiLbl, "RssiLbl");
            this.RssiLbl.ForeColor = System.Drawing.Color.ForestGreen;
            this.RssiLbl.Name = "RssiLbl";
            // 
            // FlshBrdrTmr
            // 
            this.FlshBrdrTmr.Tick += new System.EventHandler(this.OnFlshBrdrTmrTick);
            // 
            // OORDetectTmr
            // 
            this.OORDetectTmr.Interval = 2000;
            this.OORDetectTmr.Tick += new System.EventHandler(this.OnOORDetectTmrTick);
            // 
            // lblAssetNo
            // 
            resources.ApplyResources(this.lblAssetNo, "lblAssetNo");
            this.lblAssetNo.Name = "lblAssetNo";
            // 
            // lblAssetName
            // 
            resources.ApplyResources(this.lblAssetName, "lblAssetName");
            this.lblAssetName.Name = "lblAssetName";
            // 
            // btnTask
            // 
            resources.ApplyResources(this.btnTask, "btnTask");
            this.btnTask.Name = "btnTask";
            this.btnTask.Click += new System.EventHandler(this.btnTask_Click);
            // 
            // btnChngLoc
            // 
            resources.ApplyResources(this.btnChngLoc, "btnChngLoc");
            this.btnChngLoc.Name = "btnChngLoc";
            this.btnChngLoc.Click += new System.EventHandler(this.btnChngLoc_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // txtprefix
            // 
            this.txtprefix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            resources.ApplyResources(this.txtprefix, "txtprefix");
            this.txtprefix.Name = "txtprefix";
            this.txtprefix.TextChanged += new System.EventHandler(this.txtTagID_TextChanged);
            // 
            // MaskInput
            // 
            this.MaskInput.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.MaskInput, "MaskInput");
            this.MaskInput.Name = "MaskInput";
            this.MaskInput.Click += new System.EventHandler(this.MaskInput_Click);
            // 
            // TagSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.txtprefix);
            this.Controls.Add(this.btnChngLoc);
            this.Controls.Add(this.btnTask);
            this.Controls.Add(this.lblAssetName);
            this.Controls.Add(this.lblAssetNo);
            this.Controls.Add(this.RssiLbl);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.SrchBttn);
            this.Controls.Add(this.SrchByRSSIBttn);
            this.Controls.Add(this.SrchByRateBttn);
            this.Controls.Add(this.MaskInput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FilterResChkBx);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagSearchForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagSearchForm));
            this.label1 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.SrchByRateBttn = new System.Windows.Forms.RadioButton();
            this.SrchByRSSIBttn = new System.Windows.Forms.RadioButton();
            this.SrchBttn = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.FilterResChkBx = new System.Windows.Forms.CheckBox();
            this.RssiLbl = new System.Windows.Forms.Label();
            this.FlshBrdrTmr = new System.Windows.Forms.Timer();
            this.OORDetectTmr = new System.Windows.Forms.Timer();
            this.lblAssetNo = new System.Windows.Forms.Label();
            this.lblAssetName = new System.Windows.Forms.Label();
            this.btnTask = new System.Windows.Forms.Button();
            this.btnChngLoc = new System.Windows.Forms.Button();
            this.MaskInput = new OnRamp.EPCMaskInput();
            //this.txtTagID = new System.Windows.Forms.TextBox();
            this.txtprefix = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            //this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            this.label1.Left = 2;
            this.label1.Text = "Search:";
            // 
            // SrchByRateBttn
            // 
            resources.ApplyResources(this.SrchByRateBttn, "SrchByRateBttn");
            this.SrchByRateBttn.Name = "SrchByRateBttn";
            this.SrchByRateBttn.TabStop = false;
            // 
            // SrchByRSSIBttn
            // 
            this.SrchByRSSIBttn.Checked = true;
            resources.ApplyResources(this.SrchByRSSIBttn, "SrchByRSSIBttn");
            this.SrchByRSSIBttn.Name = "SrchByRSSIBttn";

            // 
            // txtprefix
            // 
            resources.ApplyResources(this.txtprefix, "txtprefix");
            this.txtprefix.Name = "txtprefix";
            this.txtprefix.Location = new System.Drawing.Point(48, 40);
            this.txtprefix.Width = 180;
            this.txtprefix.TextChanged += new System.EventHandler(this.txtTagID_TextChanged);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            this.label2.Location = new System.Drawing.Point(7, 40);
            // 
            // label3
            // 
          //  resources.ApplyResources(this.label3, "label3");
           // this.label3.Name = "label3";
           // this.label3.Location = new System.Drawing.Point(7, 65);
            // 
            // SrchBttn
            // 
            resources.ApplyResources(this.SrchBttn, "SrchBttn");
            this.SrchBttn.Name = "SrchBttn";
            this.SrchBttn.Click += new System.EventHandler(this.OnSrchBttnClicked);
            // 
            // progressBar1
            // 
            resources.ApplyResources(this.progressBar1, "progressBar1");
            this.progressBar1.Maximum = 90;
            this.progressBar1.Minimum = 60;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Value = 60;
            // 
            // FilterResChkBx
            // 
            this.FilterResChkBx.Checked = true;
            this.FilterResChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.FilterResChkBx, "FilterResChkBx");
            this.FilterResChkBx.Name = "FilterResChkBx";
            // 
            // RssiLbl
            // 
            resources.ApplyResources(this.RssiLbl, "RssiLbl");
            this.RssiLbl.ForeColor = System.Drawing.Color.ForestGreen;
            this.RssiLbl.Top += 20;
            this.RssiLbl.Name = "RssiLbl";
            // 
            // FlshBrdrTmr
            // 
            this.FlshBrdrTmr.Tick += new System.EventHandler(this.OnFlshBrdrTmrTick);
            // 
            // OORDetectTmr
            // 
            this.OORDetectTmr.Interval = 2000;
            this.OORDetectTmr.Tick += new System.EventHandler(this.OnOORDetectTmrTick);
            // 
            // lblAssetNo
            // 
            resources.ApplyResources(this.lblAssetNo, "lblAssetNo");
            this.lblAssetNo.Name = "lblAssetNo";
            // 
            // lblAssetName
            // 
            resources.ApplyResources(this.lblAssetName, "lblAssetName");
            this.lblAssetName.Name = "lblAssetName";
            this.lblAssetName.Left = 48;
            this.lblAssetName.Width = 180;
            // 
            // btnTask
            // 
            resources.ApplyResources(this.btnTask, "btnTask");
            this.btnTask.Name = "btnTask";
            this.btnTask.Click += new System.EventHandler(this.btnTask_Click);
            // 
            // btnChngLoc
            // 
            resources.ApplyResources(this.btnChngLoc, "btnChngLoc");
            this.btnChngLoc.Name = "btnChngLoc";
            this.btnChngLoc.Click += new System.EventHandler(this.btnChngLoc_Click);

            //
            //TagID
            //
            //resources.ApplyResources(this.txtTagID, "txtTagID");
            //this.txtTagID.Name = "txtTagID";
            //this.txtTagID.Location = new System.Drawing.Point(48, 40);
            //this.txtTagID.Width = 180;
            //this.txtTagID.TextChanged += new System.EventHandler(this.txtTagID_TextChanged);
            // 
            // MaskInput
            // 
            this.MaskInput.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.MaskInput, "MaskInput");
            this.MaskInput.Name = "MaskInput";
            this.MaskInput.Click += new System.EventHandler(this.MaskInput_Click);
            // 
            // TagSearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnChngLoc);
            this.Controls.Add(this.btnTask);
            this.Controls.Add(this.lblAssetName);
            this.Controls.Add(this.lblAssetNo);
            this.Controls.Add(this.RssiLbl);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.SrchBttn);
            this.Controls.Add(this.SrchByRSSIBttn);
            this.Controls.Add(this.SrchByRateBttn);
            this.Controls.Add(this.MaskInput);
           // this.Controls.Add(this.txtTagID);
            this.Controls.Add(this.txtprefix);
            this.Controls.Add(this.label2);
           // this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FilterResChkBx);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagSearchForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton SrchByRateBttn;
        private System.Windows.Forms.RadioButton SrchByRSSIBttn;
        private System.Windows.Forms.Button SrchBttn;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox FilterResChkBx;
        private System.Windows.Forms.Label RssiLbl;
        private System.Windows.Forms.Timer FlshBrdrTmr;
        private System.Windows.Forms.Timer OORDetectTmr;
        private System.Windows.Forms.Label lblAssetNo;
        private System.Windows.Forms.Label lblAssetName;
        private System.Windows.Forms.Button btnTask;
        private System.Windows.Forms.Button btnChngLoc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtprefix;
        private EPCMaskInput MaskInput;
    }
}