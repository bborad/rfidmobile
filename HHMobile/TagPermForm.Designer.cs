namespace OnRamp
{
    partial class TagPermForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagPermForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.KillPwdBttn = new System.Windows.Forms.Button();
            this.AccPwdBttn = new System.Windows.Forms.Button();
            this.EPCBnkBttn = new System.Windows.Forms.Button();
            this.TIDBnkBttn = new System.Windows.Forms.Button();
            this.UsrBnkBttn = new System.Windows.Forms.Button();
            this.PCLbl = new System.Windows.Forms.Label();
            this.EPCLbl = new System.Windows.Forms.Label();
            this.TagBttn = new System.Windows.Forms.Button();
            this.ApplyBttn = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // KillPwdBttn
            // 
            resources.ApplyResources(this.KillPwdBttn, "KillPwdBttn");
            this.KillPwdBttn.Name = "KillPwdBttn";
            this.KillPwdBttn.Click += new System.EventHandler(this.OnKillPWClicked);
            // 
            // AccPwdBttn
            // 
            resources.ApplyResources(this.AccPwdBttn, "AccPwdBttn");
            this.AccPwdBttn.Name = "AccPwdBttn";
            this.AccPwdBttn.Click += new System.EventHandler(this.OnAccPWClicked);
            // 
            // EPCBnkBttn
            // 
            resources.ApplyResources(this.EPCBnkBttn, "EPCBnkBttn");
            this.EPCBnkBttn.Name = "EPCBnkBttn";
            this.EPCBnkBttn.Click += new System.EventHandler(this.OnEPCBnkClicked);
            // 
            // TIDBnkBttn
            // 
            resources.ApplyResources(this.TIDBnkBttn, "TIDBnkBttn");
            this.TIDBnkBttn.Name = "TIDBnkBttn";
            this.TIDBnkBttn.Click += new System.EventHandler(this.OnTIDBnkClicked);
            // 
            // UsrBnkBttn
            // 
            resources.ApplyResources(this.UsrBnkBttn, "UsrBnkBttn");
            this.UsrBnkBttn.Name = "UsrBnkBttn";
            this.UsrBnkBttn.Click += new System.EventHandler(this.OnUserBnkClicked);
            // 
            // PCLbl
            // 
            resources.ApplyResources(this.PCLbl, "PCLbl");
            this.PCLbl.Name = "PCLbl";
            // 
            // EPCLbl
            // 
            resources.ApplyResources(this.EPCLbl, "EPCLbl");
            this.EPCLbl.Name = "EPCLbl";
            // 
            // TagBttn
            // 
            resources.ApplyResources(this.TagBttn, "TagBttn");
            this.TagBttn.Name = "TagBttn";
            this.TagBttn.Click += new System.EventHandler(this.OnTagBttnClicked);
            // 
            // ApplyBttn
            // 
            resources.ApplyResources(this.ApplyBttn, "ApplyBttn");
            this.ApplyBttn.Name = "ApplyBttn";
            this.ApplyBttn.Click += new System.EventHandler(this.OnApplyClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            // 
            // TagPermForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.ApplyBttn);
            this.Controls.Add(this.TagBttn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.EPCLbl);
            this.Controls.Add(this.PCLbl);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.UsrBnkBttn);
            this.Controls.Add(this.TIDBnkBttn);
            this.Controls.Add(this.EPCBnkBttn);
            this.Controls.Add(this.AccPwdBttn);
            this.Controls.Add(this.KillPwdBttn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagPermForm";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagPermForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.KillPwdBttn = new System.Windows.Forms.Button();
            this.AccPwdBttn = new System.Windows.Forms.Button();
            this.EPCBnkBttn = new System.Windows.Forms.Button();
            this.TIDBnkBttn = new System.Windows.Forms.Button();
            this.UsrBnkBttn = new System.Windows.Forms.Button();
            this.PCLbl = new System.Windows.Forms.Label();
            this.EPCLbl = new System.Windows.Forms.Label();
            this.TagBttn = new System.Windows.Forms.Button();
            this.ApplyBttn = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // KillPwdBttn
            // 
            resources.ApplyResources(this.KillPwdBttn, "KillPwdBttn");
            this.KillPwdBttn.Name = "KillPwdBttn";
            this.KillPwdBttn.Click += new System.EventHandler(this.OnKillPWClicked);
            // 
            // AccPwdBttn
            // 
            resources.ApplyResources(this.AccPwdBttn, "AccPwdBttn");
            this.AccPwdBttn.Name = "AccPwdBttn";
            this.AccPwdBttn.Click += new System.EventHandler(this.OnAccPWClicked);
            // 
            // EPCBnkBttn
            // 
            resources.ApplyResources(this.EPCBnkBttn, "EPCBnkBttn");
            this.EPCBnkBttn.Name = "EPCBnkBttn";
            this.EPCBnkBttn.Click += new System.EventHandler(this.OnEPCBnkClicked);
            // 
            // TIDBnkBttn
            // 
            resources.ApplyResources(this.TIDBnkBttn, "TIDBnkBttn");
            this.TIDBnkBttn.Name = "TIDBnkBttn";
            this.TIDBnkBttn.Click += new System.EventHandler(this.OnTIDBnkClicked);
            // 
            // UsrBnkBttn
            // 
            resources.ApplyResources(this.UsrBnkBttn, "UsrBnkBttn");
            this.UsrBnkBttn.Name = "UsrBnkBttn";
            this.UsrBnkBttn.Click += new System.EventHandler(this.OnUserBnkClicked);
            // 
            // PCLbl
            // 
            resources.ApplyResources(this.PCLbl, "PCLbl");
            this.PCLbl.Name = "PCLbl";
            // 
            // EPCLbl
            // 
            resources.ApplyResources(this.EPCLbl, "EPCLbl");
            this.EPCLbl.Name = "EPCLbl";
            // 
            // TagBttn
            // 
            resources.ApplyResources(this.TagBttn, "TagBttn");
            this.TagBttn.Name = "TagBttn";
            this.TagBttn.Click += new System.EventHandler(this.OnTagBttnClicked);
            // 
            // ApplyBttn
            // 
            resources.ApplyResources(this.ApplyBttn, "ApplyBttn");
            this.ApplyBttn.Name = "ApplyBttn";
            this.ApplyBttn.Click += new System.EventHandler(this.OnApplyClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            // 
            // TagPermForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.ApplyBttn);
            this.Controls.Add(this.TagBttn);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.EPCLbl);
            this.Controls.Add(this.PCLbl);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.UsrBnkBttn);
            this.Controls.Add(this.TIDBnkBttn);
            this.Controls.Add(this.EPCBnkBttn);
            this.Controls.Add(this.AccPwdBttn);
            this.Controls.Add(this.KillPwdBttn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagPermForm";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button KillPwdBttn;
        private System.Windows.Forms.Button AccPwdBttn;
        private System.Windows.Forms.Button EPCBnkBttn;
        private System.Windows.Forms.Button TIDBnkBttn;
        private System.Windows.Forms.Button UsrBnkBttn;
        private System.Windows.Forms.Label PCLbl;
        private System.Windows.Forms.Label EPCLbl;
        private System.Windows.Forms.Button TagBttn;
        private System.Windows.Forms.Button ApplyBttn;
        private System.Windows.Forms.CheckBox PwdReqChkBx;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}