using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class SelectCriteriaForm : Form
    {
        private uint critIdx;
        private RFID_18K6C_SELECT_CRITERION loadedCrit;

        enum MaskPos // corresponds to position in DropDownList of ComboBox
        {
            None = -1,
            Begins = 0,
            Ends = 1,
            Any = 2,
        }

        // Assuming that the length of hexStr is 'even' number
        private Byte[] HexStrToByteMask(String hexStr, int zeroLPad)
        {
            Byte[] Mask= new Byte[zeroLPad + hexStr.Length / 2];

            for (int i = 0; i < zeroLPad; i++)
                Mask[i] = 0;
            for (int m = zeroLPad, s = 0; s < hexStr.Length; s+=2, m++)
            {
                Mask[m] = Byte.Parse(hexStr.Substring(s, 2), System.Globalization.NumberStyles.HexNumber);
            }

            return Mask;
        }

        private String ByteMaskToHexStr(Byte[] mask, int offset, int count)
        {
            // padding zeros as 'offset'
            StringBuilder SBldr = new StringBuilder();

            for (int i = 0; i < count; i++)
                SBldr = SBldr.AppendFormat(null, "{0:X2}", mask[i]);

            String HexStr = SBldr.ToString();

            return  HexStr.PadLeft(HexStr.Length + offset * 2, '0');
        }

        private void ActTypeCmbBxSetByName(ComboBox cmbBx, String dpyName)
        {
            for (int i = 0; i < cmbBx.Items.Count; i++)
            {
                if (String.Compare (cmbBx.Items[i].ToString(), dpyName, true) == 0)
                {
                    cmbBx.SelectedIndex = i;
                }
            }
        }

        private void ResetFormUI(ref RFID_18K6C_SELECT_CRITERION origCrit)
        {
            this.SelCritBankCmbBx.SelectedIndex = (int)(origCrit.mask.bank - listedBnks[0]);
            // bit -> byte
            int ByteOffset = (int)origCrit.mask.offset / 8 + ((origCrit.mask.offset % 8 != 0) ? 1 : 0);
            int ByteCount = (int)origCrit.mask.count / 8 + ((origCrit.mask.count % 8 != 0)? 1 : 0);
            if (origCrit.mask.count == 0)
            {
                this.SelCritMaskTxtBx.Text = "";
                this.SelCritMaskPosCmbBx.SelectedIndex = (int)MaskPos.Any;
            }
            else // Take of Ends-with by considering offset
            {
                // minus the ByteOffset by the application-defined offsets
                ByteOffset -= (int)listedBnksMemStart[SelCritBankCmbBx.SelectedIndex];
                this.SelCritMaskTxtBx.Text = ByteMaskToHexStr(origCrit.mask.mask, ByteOffset, ByteCount);
                this.SelCritMaskPosCmbBx.SelectedIndex = (int)MaskPos.Begins;
            }

            switch (origCrit.action.action)
            {
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Set]);
                    MatchedTgtCmbBx.SelectedIndex = (int)TgtType.A;
                    MatchedSLValCmbBx.SelectedIndex = (int)SLValType.On;
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Set]);
                    NonMatchedTgtCmbBx.SelectedIndex = (int)TgtType.B;
                    NonMatchedSLValCmbBx.SelectedIndex = (int)SLValType.Off;
                    break;
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_NOTHING:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx,  ActTypeDpyStrings[(int)ActType.Set]);
                    MatchedTgtCmbBx.SelectedIndex = (int)TgtType.A;
                    MatchedSLValCmbBx.SelectedIndex = (int)SLValType.On;
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.DoNothing]);
                    break;
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_DSLINVB:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.DoNothing]);
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Set]);
                    NonMatchedTgtCmbBx.SelectedIndex = (int)TgtType.B;
                    NonMatchedSLValCmbBx.SelectedIndex = (int)SLValType.Off;
                    break;
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_NSLINVS_NOTHING:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Toggle]);
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.DoNothing]);
                    break;
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Set]);
                    MatchedTgtCmbBx.SelectedIndex = (int)TgtType.B;
                    MatchedSLValCmbBx.SelectedIndex = (int)SLValType.Off;
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Set]);
                    NonMatchedTgtCmbBx.SelectedIndex = (int)TgtType.A;
                    NonMatchedSLValCmbBx.SelectedIndex = (int)SLValType.On;
                    break;
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_NOTHING:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Set]);
                    MatchedTgtCmbBx.SelectedIndex = (int)TgtType.B;
                    MatchedSLValCmbBx.SelectedIndex = (int)SLValType.Off;
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.DoNothing]);
                    break;
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_ASLINVA:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.DoNothing]);
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Set]);
                    NonMatchedTgtCmbBx.SelectedIndex = (int)TgtType.A;
                    NonMatchedSLValCmbBx.SelectedIndex = (int)SLValType.On;
                    break;
                case RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_NSLINVS:
                    ActTypeCmbBxSetByName(MatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.DoNothing]);
                    ActTypeCmbBxSetByName(NonMatchedActTypeCmbBx, ActTypeDpyStrings[(int)ActType.Toggle]);
                    break;
            }
            // Note: no need to set NonMatchedFlagCmbBx because it's a follower
            //             of MatchedFlagCmbBx
            switch (origCrit.action.target)
            {
                case RFID_18K6C_TARGET.RFID_18K6C_TARGET_INVENTORY_S0:
                    MatchedFlagCmbBx.SelectedIndex = (int)FlagType.S0;
                    break;
                case RFID_18K6C_TARGET.RFID_18K6C_TARGET_INVENTORY_S1:
                    MatchedFlagCmbBx.SelectedIndex = (int)FlagType.S1;
                    break;
                case RFID_18K6C_TARGET.RFID_18K6C_TARGET_INVENTORY_S2:
                    MatchedFlagCmbBx.SelectedIndex = (int)FlagType.S2;
                    break;
                case RFID_18K6C_TARGET.RFID_18K6C_TARGET_INVENTORY_S3:
                    MatchedFlagCmbBx.SelectedIndex = (int)FlagType.S3;
                    break;
                case RFID_18K6C_TARGET.RFID_18K6C_TARGET_SELECTED:
                    MatchedFlagCmbBx.SelectedIndex = (int)FlagType.SL;
                    break;
            }
        }

        private void SelectCriteriaFormBasic(uint critNum)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectCriteriaForm));
            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.SelectCriteriaForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectCriteriaForm));
            }
            // Display 1-4 (internal storage 0-3)
            critIdx = critNum;
            this.CritNumLbl.Text = resources.GetString("CritNumLbl.Text") + (critNum + 1).ToString();

        }

        public SelectCriteriaForm (uint critNum, ref RFID_18K6C_SELECT_CRITERION origCrit,
            ref RFID_18K6C_SELECT_CRITERION lastModifiedCrit)
        {
            SelectCriteriaFormBasic(critNum);

            this.loadedCrit = new RFID_18K6C_SELECT_CRITERION();
            origCrit.CopyTo(ref loadedCrit);            
            // After initial display, revert to 'loaded' Version not last Modified version
            // when Reset Link is clicked
            ResetFormUI(ref lastModifiedCrit);
            this.RstLnk.Visible = true;
        }

        public SelectCriteriaForm(uint critNum, ref RFID_18K6C_SELECT_CRITERION origCrit) 
        {
            SelectCriteriaFormBasic(critNum);

            this.loadedCrit = new RFID_18K6C_SELECT_CRITERION();
            origCrit.CopyTo(ref loadedCrit);
            ResetFormUI(ref loadedCrit);
            this.RstLnk.Visible = true;
        }

        public SelectCriteriaForm(uint critNum)
        {
            SelectCriteriaFormBasic(critNum);
            
            this.RstLnk.Visible = false; //Nothing to RESET
        }

        public void InitializeFields (ref RFID_18K6C_SELECT_CRITERION lastModifiedCrit)
        {
            ResetFormUI(ref lastModifiedCrit);
        }

       
        private void HexKeyPressChk(object sender, KeyPressEventArgs e)
        {
            Program.HexKeyPressChk(sender, e);
        }

        enum ActType // Note: not the same as position in ComboBox DropDownList
        {
            None = -1,
            Set,
            Toggle,
            DoNothing,
        };

        String[] ActTypeDpyStrings = new String[3]
        {
            "Set", "Toggle", "Do Nothing"
        };

        // Compare Strings because items in 'Action' Combo Box changes
        // dynamically
        private ActType SelectedActTypeToEnum(object cmbBxItem)
        {
            ActType act = ActType.None;

            if (cmbBxItem != null)
            {
                String SelItemStr = cmbBxItem.ToString();
                if (String.Compare (SelItemStr, ActTypeDpyStrings[(int)ActType.Set], true) == 0)
                    act = ActType.Set;
                else if (String.Compare(SelItemStr, ActTypeDpyStrings[(int)ActType.Toggle], true) == 0)
                    act = ActType.Toggle;
                else if (String.Compare(SelItemStr, ActTypeDpyStrings[(int)ActType.DoNothing], true) == 0)
                    act = ActType.DoNothing;
            }

            return act;
        }

        private void ActCmbBxSetItems (ComboBox cmbBx, ActType[] items)
        {
            ActType curType = SelectedActTypeToEnum(cmbBx.SelectedItem);

           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectCriteriaForm));
            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.SelectCriteriaForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectCriteriaForm));
            }
            cmbBx.Items.Clear(); // this affects the SelectedIndex (reset to -1), trigger ValueChanged Event
  
            bool found = false;
            int newPos = -1;
            for (int i = 0; i < items.Length; i++)
            {
                switch (items[i])
                {
                    case ActType.Set:
                        cmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items"));
                        break;
                    case ActType.Toggle:
                        cmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items1"));
                        break;
                    case ActType.DoNothing:
                        cmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items2"));
                        break;
                }
                if (items[i] == curType)
                {
                    found = true;
                    newPos = i;
                }
            }
            // Update Selected Index if out of the new set
            if (!found)
                cmbBx.SelectedIndex = 0; // The first one
            else
                cmbBx.SelectedIndex = newPos; // same Action, (possibly)different position in list
            
        }

        private void OnMatchedActTypeChanged(object sender, EventArgs e)
        {
            ActType Act = SelectedActTypeToEnum(MatchedActTypeCmbBx.SelectedItem);

            switch (Act)
            {
                case ActType.Set:
                    MatchedFlagCmbBx.Visible = true;
                    switch ((FlagType)MatchedFlagCmbBx.SelectedIndex)
                    {
                        case FlagType.SL:
                            MatchedTgtCmbBx.Visible = false;
                            MatchedSLValCmbBx.Visible = true;
                            break;
                        case FlagType.S0:
                        case FlagType.S1:
                        case FlagType.S2:
                        case FlagType.S3:
                            MatchedTgtCmbBx.Visible = true;
                            MatchedSLValCmbBx.Visible = false;
                            break;
                    }
                    break;
                case ActType.Toggle:
                    MatchedFlagCmbBx.Visible = true;
                    MatchedTgtCmbBx.Visible = false;
                    MatchedSLValCmbBx.Visible = false;
                    break;
                case ActType.DoNothing:
                    MatchedFlagCmbBx.Visible = false;
                    MatchedTgtCmbBx.Visible = false;
                    MatchedSLValCmbBx.Visible = false;
                    break;
            }
            // corresponding adjustments/restrictions in NonMatched action
            // as a result of this change of Action choice.

            // Temp disabled to avoid indirect infinite recursion
            MatchedActTypeCmbBx.SelectedIndexChanged -= OnMatchedActTypeChanged;
            switch (Act)
            {
                case ActType.Set:
                    // Allow only Opposite FlagValue, DoNothing
                    ActCmbBxSetItems(NonMatchedActTypeCmbBx, new ActType[] { ActType.Set, ActType.DoNothing });
                    break;
                case ActType.Toggle:
                    // Allow only Nothing
                    ActCmbBxSetItems(NonMatchedActTypeCmbBx, new ActType[] {ActType.DoNothing });
                    break;
                case ActType.DoNothing:
                    // Allow all Set and Toggle only
                    ActCmbBxSetItems(NonMatchedActTypeCmbBx, new ActType[] { ActType.Set, ActType.Toggle });
                    break;
                
            }
            // re-enabled
            MatchedActTypeCmbBx.SelectedIndexChanged += OnMatchedActTypeChanged;
            // Initialize Flag ComboBox if not already did
            if (MatchedFlagCmbBx.SelectedIndex == (int)FlagType.None)
                MatchedFlagCmbBx.SelectedIndex = (int)FlagType.S0;

        }

        enum FlagType
        {
            None = -1,
            SL,
            S0,
            S1,
            S2,
            S3
        };
        private void OnMatchedFlagChanged(object sender, EventArgs e)
        {
            switch (SelectedActTypeToEnum(MatchedActTypeCmbBx.SelectedItem))
            {
                case ActType.Set:
                    switch ((FlagType)MatchedFlagCmbBx.SelectedIndex)
                    {
                        case FlagType.SL:
                            MatchedTgtCmbBx.Visible = false;
                            MatchedSLValCmbBx.Visible = true;
                            break;
                        case FlagType.S0:
                        case FlagType.S1:
                        case FlagType.S2:
                        case FlagType.S3:
                            MatchedTgtCmbBx.Visible = true;
                            MatchedSLValCmbBx.Visible = false;
                            break;
                    }
                     // Initialize Tgt/SLVal ComboBox if not already did
                    switch ((FlagType)MatchedFlagCmbBx.SelectedIndex)
                    {
                        case FlagType.SL:
                            if (MatchedSLValCmbBx.SelectedIndex == (int)SLValType.None)
                                MatchedSLValCmbBx.SelectedIndex = (int)SLValType.On; // TBD: might collide with NonMatched
                            break;
                        case FlagType.S0:
                        case FlagType.S1:
                        case FlagType.S2:
                        case FlagType.S3:
                            if (MatchedTgtCmbBx.SelectedIndex == (int)TgtType.None)
                                MatchedTgtCmbBx.SelectedIndex = (int)TgtType.A; // TBD: might collide with NonMatched
                            break;
                    }
                    break;
                case ActType.Toggle:
                MatchedSLValCmbBx.Visible = false;
                MatchedTgtCmbBx.Visible  = false;
                break;
            }

            // Non Matched side must follow if not already did
            NonMatchedFlagCmbBx.SelectedIndex = MatchedFlagCmbBx.SelectedIndex;

        }

        enum TgtType // Equivalent to the Position in ComboBox DropDownList
        {
            None = -1,
            A = 0,
            B = 1
        }
        enum SLValType // Equivalent to the Position in ComboBox DropDownList
        {
            None = -1,
            On = 0,
            Off = 1
        };

        private void OnNonMatchedActTypeChanged(object sender, EventArgs e)
        {
            ActType Act = SelectedActTypeToEnum(NonMatchedActTypeCmbBx.SelectedItem);
            switch (Act)
            {
                case ActType.Set:
                    NonMatchedFlagCmbBx.Visible = true;
                    switch ((FlagType)NonMatchedFlagCmbBx.SelectedIndex)
                    {
                        case FlagType.SL:
                            NonMatchedTgtCmbBx.Visible = false;
                            NonMatchedSLValCmbBx.Visible = true;
                            break;
                        case FlagType.S0:
                        case FlagType.S1:
                        case FlagType.S2:
                        case FlagType.S3:
                            NonMatchedTgtCmbBx.Visible = true;
                            NonMatchedSLValCmbBx.Visible = false;
                            break;
                    }
                    break;
                case ActType.Toggle:
                    NonMatchedFlagCmbBx.Visible = true;
                    NonMatchedTgtCmbBx.Visible = false;
                    NonMatchedSLValCmbBx.Visible = false;
                    break;
                case ActType.DoNothing:
                    NonMatchedFlagCmbBx.Visible = false;
                    NonMatchedTgtCmbBx.Visible = false;
                    NonMatchedSLValCmbBx.Visible = false;
                    break;
            }
            // corresponding adjustments/restrictions in Matched action
            // as a result of this change of Action choice.
            // Temp disabled to avoid indirect infinite recursion
            NonMatchedActTypeCmbBx.SelectedIndexChanged -= OnNonMatchedActTypeChanged;
            switch (Act)
            {
                case ActType.Set:
                    // Allow only Opposite FlagValue, DoNothing
                    ActCmbBxSetItems(MatchedActTypeCmbBx, new ActType[] { ActType.Set, ActType.DoNothing });
                    break;
                case ActType.Toggle:
                    // Allow only Nothing
                    ActCmbBxSetItems(MatchedActTypeCmbBx, new ActType[] { ActType.DoNothing });
                    break;
                case ActType.DoNothing:
                    // Allow all Set and Toggle only
                    ActCmbBxSetItems(MatchedActTypeCmbBx, new ActType[] { ActType.Set, ActType.Toggle });
                    break;

            }
            // re-enabled
            NonMatchedActTypeCmbBx.SelectedIndexChanged += OnNonMatchedActTypeChanged;
            // Initialize Flag ComboBox if not already did
            if (NonMatchedFlagCmbBx.SelectedIndex == (int)FlagType.None)
                NonMatchedFlagCmbBx.SelectedIndex = (int)FlagType.S0;
        }

        private void OnNonMatchedFlagChanged(object sender, EventArgs e)
        {
            switch (SelectedActTypeToEnum(NonMatchedActTypeCmbBx.SelectedItem))
            {
                case ActType.Set:
                    switch ((FlagType)NonMatchedFlagCmbBx.SelectedIndex)
                    {
                        case FlagType.SL:
                            NonMatchedTgtCmbBx.Visible = false;
                            NonMatchedSLValCmbBx.Visible = true;
                            break;
                        case FlagType.S0:
                        case FlagType.S1:
                        case FlagType.S2:
                        case FlagType.S3:
                            NonMatchedTgtCmbBx.Visible = true;
                            NonMatchedSLValCmbBx.Visible = false;
                            break;
                    }

                    break;
                case ActType.Toggle:
                    NonMatchedSLValCmbBx.Visible = false;
                    NonMatchedTgtCmbBx.Visible = false;
                    break;
            }

            // Matched side must follow if not already did
            MatchedFlagCmbBx.SelectedIndex = NonMatchedFlagCmbBx.SelectedIndex;
        }

        private void OnNonMatchedTgtChanged(object sender, EventArgs e)
        {
            //Make sure that the Matched-side has a different value
            switch ((TgtType)NonMatchedTgtCmbBx.SelectedIndex)
            {
                case TgtType.A:
                    MatchedTgtCmbBx.SelectedIndex = (int)TgtType.B;
                    break;
                case TgtType.B:
                    MatchedTgtCmbBx.SelectedIndex = (int)TgtType.A;
                    break;
            }
        }

        private void OnNonMatchedSLValChanged(object sender, EventArgs e)
        {
            //Make sure that the Matched-side has a different value
            switch ((SLValType)NonMatchedSLValCmbBx.SelectedIndex)
            {
                case SLValType.On:
                    MatchedSLValCmbBx.SelectedIndex = (int)SLValType.Off;
                    break;
                case SLValType.Off:
                    MatchedSLValCmbBx.SelectedIndex = (int)SLValType.On;
                    break;
            }
        }

        private void OnMatchedTgtChanged(object sender, EventArgs e)
        {
            //Make sure that the Non-Matched-side has a different value
            switch ((TgtType)MatchedTgtCmbBx.SelectedIndex)
            {
                case TgtType.A:
                    NonMatchedTgtCmbBx.SelectedIndex = (int)TgtType.B;
                    break;
                case TgtType.B:
                    NonMatchedTgtCmbBx.SelectedIndex = (int)TgtType.A;
                    break;
            }
        }

        private void OnMatchedSLValChanged(object sender, EventArgs e)
        {
            //Make sure that the Non-Matched-side has a different value
            switch ((SLValType)MatchedSLValCmbBx.SelectedIndex)
            {
                case SLValType.On:
                    NonMatchedSLValCmbBx.SelectedIndex = (int)SLValType.Off;
                    break;
                case SLValType.Off:
                    NonMatchedSLValCmbBx.SelectedIndex = (int)SLValType.On;
                    break;
            }
        }

        private void OnRstLnkClicked(object sender, EventArgs e)
        {
            ResetFormUI(ref loadedCrit);
        }


        public delegate void SelectCriteriaEditNotify (uint critNum, ref RFID_18K6C_SELECT_CRITERION crit);
        public SelectCriteriaEditNotify CritEditedNotify;

        RFID_18K6C_MEMORY_BANK[] listedBnks = new RFID_18K6C_MEMORY_BANK[3] {
            // Reserved not allowed (According to EPCGlobal Class1Gen2 Tag Spec)
            RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_EPC,
            RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_TID,
            RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_USER
        };
        uint[] listedBnksMemStart = new uint[3] {
            4, // Byte [0:1]: CRC16; Byte[2:3]: PC
            0, // Could consider starting from 4th byte also
            0,
        };
        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            RFID_18K6C_SELECT_CRITERION crit = new RFID_18K6C_SELECT_CRITERION();

            crit.mask.bank = listedBnks[SelCritBankCmbBx.SelectedIndex];
            if (SelCritMaskTxtBx.Text.Length > 0 && (SelCritMaskPosCmbBx.SelectedIndex != (int)MaskPos.Any))
            {
                crit.mask.offset = listedBnksMemStart[SelCritBankCmbBx.SelectedIndex]*8; // 8 bit/byte
                // 4 bit per hex char.
                int PadBytes = 0;
                if (SelCritMaskPosCmbBx.SelectedIndex == (int)MaskPos.Begins)
                {
                    PadBytes = 0;
                    if ((SelCritMaskTxtBx.Text.Length & 1) != 0) // odd, pad ending zero
                        SelCritMaskTxtBx.Text = SelCritMaskTxtBx.Text.PadRight(SelCritMaskTxtBx.Text.Length + 1, '0');
                }
                else if (SelCritMaskPosCmbBx.SelectedIndex == (int)MaskPos.Ends)
                {
                    if ((SelCritMaskTxtBx.Text.Length & 1) != 0)// odd, pad beginning zero
                        SelCritMaskTxtBx.Text = SelCritMaskTxtBx.Text.PadLeft(SelCritMaskTxtBx.Text.Length + 1, '0');
                    PadBytes = RfidSp.RFID_18K6C_SELECT_MASK_BYTE_LEN - (SelCritMaskTxtBx.Text.Length / 2);
                }
                Byte[] Mask = HexStrToByteMask(SelCritMaskTxtBx.Text, PadBytes);
                crit.mask.mask = (Byte[])Mask.Clone();
                crit.mask.count = (uint)Mask.Length * 8; // bits
            }
            else
            {
                crit.mask.offset = 0;
                crit.mask.count = 0;
            }

            ActType MatchedAct = SelectedActTypeToEnum(MatchedActTypeCmbBx.SelectedItem);
            ActType NonMatchedAct = SelectedActTypeToEnum(NonMatchedActTypeCmbBx.SelectedItem);

            RFID_18K6C_ACTION Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB;

            switch (MatchedAct)
            {
                case ActType.Set:
                    switch (NonMatchedAct)
                    {
                        case ActType.Set: // Set, Set
                            if (IsSessFlag((FlagType)MatchedFlagCmbBx.SelectedIndex))
                            {
                                if (MatchedTgtCmbBx.SelectedIndex == (int)TgtType.A)
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB;
                                else
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA;
                            }
                            else
                            {
                                if (MatchedSLValCmbBx.SelectedIndex == (int)SLValType.On)
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_DSLINVB;
                                else
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_ASLINVA;
                            }
                            break;
                        case ActType.Toggle: // Set, Toggle
                            throw new ApplicationException("Programming Error");
                            
                        case ActType.DoNothing: // Set, Nothing
                            if (IsSessFlag((FlagType)MatchedFlagCmbBx.SelectedIndex))
                            {
                                if (MatchedTgtCmbBx.SelectedIndex == (int)TgtType.A)
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_NOTHING;
                                else
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_NOTHING;
                            }
                            else
                            {
                                if (MatchedSLValCmbBx.SelectedIndex == (int)SLValType.On)
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_ASLINVA_NOTHING;
                                else
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_DSLINVB_NOTHING;
                            }
                            break;
                    }
                    break;
                case ActType.Toggle:
                    switch (NonMatchedAct)
                    {
                        case ActType.Set: // Toggle, Set
                            throw new ApplicationException("Programming Error");
                            
                        case ActType.Toggle: // Toggle, Toggle
                            throw new ApplicationException("Programming Error");
                            
                        case ActType.DoNothing: // Toggle, Nothing
                                Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_NSLINVS_NOTHING;
                            break;
                    }
                    break;
                case ActType.DoNothing:
                    switch (NonMatchedAct)
                    {
                        case ActType.Set: // Nothing, Set
                            if (IsSessFlag((FlagType)NonMatchedFlagCmbBx.SelectedIndex))
                            {
                                if (NonMatchedTgtCmbBx.SelectedIndex == (int)TgtType.A)
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_ASLINVA;
                                else
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_DSLINVB;
                            }
                            else
                            {
                                if (NonMatchedSLValCmbBx.SelectedIndex == (int)SLValType.On)
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_ASLINVA;
                                else
                                    Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_DSLINVB;
                            }
                            break;
                        case ActType.Toggle: // Nothing, Toggle
                            Action = RFID_18K6C_ACTION.RFID_18K6C_ACTION_NOTHING_NSLINVS;
                            break;
                        case ActType.DoNothing: // Nothing, Nothing
                            throw new ApplicationException("Programming Error");
                            
                    }
                    break;
            }
            crit.action.action = Action;
            // Assuming both Matched and NonMatched Flag are the same
            if (IsSessFlag((FlagType)MatchedFlagCmbBx.SelectedIndex))
                crit.action.target = (RFID_18K6C_TARGET)((int)RFID_18K6C_TARGET.RFID_18K6C_TARGET_INVENTORY_S0 + ((MatchedFlagCmbBx.SelectedIndex - (int)FlagType.S0)));
            else
                crit.action.target = RFID_18K6C_TARGET.RFID_18K6C_TARGET_SELECTED;
            crit.action.enableTruncate = 0; // save the trouble
            
            if (CritEditedNotify != null)
                CritEditedNotify(critIdx, ref crit);
        }
        
        bool IsSessFlag(FlagType flag)
        {
            return (flag != FlagType.SL); // Assume that it's not FlagType.None

        }

        private void OnSelCritMaskPosChanged(object sender, EventArgs e)
        {
            // Make SelCritMaskTxtBx invisible when 'of Any' is chosen
            switch ((MaskPos)SelCritMaskPosCmbBx.SelectedIndex)
            {
                case MaskPos.Any:
                case MaskPos.None:
                    SelCritMaskTxtBx.Visible = false;
                    break;
                case MaskPos.Begins:
                case MaskPos.Ends:
                    SelCritMaskTxtBx.Visible = true;
                    break;
            }
        }

    }
}