using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class TagPermForm : Form
    {
        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }

        public TagPermForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            
            tagScanSelectDlg = new TagScanSelectDlg(TagScanSelStatusUpdated);
            
            // initialize members
            killPWPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_NO_CHANGE;
            accPWPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_NO_CHANGE;
            epcWrPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_NO_CHANGE;
            tidWrPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_NO_CHANGE;
            userWrPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_NO_CHANGE;

            ApplyBttnInitState();
        }

        ~TagPermForm()
        {
            if (tagScanSelectDlg != null)
                tagScanSelectDlg.Dispose();
        }

        #region TagID         
        private void TagScanSelStatusUpdated(TagOperEvtType state)
        {
            TagOperEvt(this, new TagOperEvtArgs(state));
        }

        private TagScanSelectDlg tagScanSelectDlg = null;
        

        private void OnTagBttnClicked(object sender, EventArgs e)
        {
            // Remove Hotkey Handler (avoid duplicate event)
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            DialogResult Res = tagScanSelectDlg.ShowDialog();
            if (Res == DialogResult.OK)
            {
                // Update currently chosen PC-EPC as confirmed selection
                if (tagScanSelectDlg.ChosenTag.PC != 0)
                {
                    PCLbl.Text = tagScanSelectDlg.ChosenTag.PC.ToString("X4");
                    EPCLbl.Text = tagScanSelectDlg.ChosenTag.EPC.ToString();
                }
            }
            // Restore Hotkey Handler (avoid duplicate event)
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }
        #endregion

        #region Permission
        
        private RFID_18K6C_TAG_PERM _TagPerm;

        #region Private Accessors/Modifiers
        private RFID_18K6C_TAG_PWD_PERM killPWPerm
        {
            get
            {
                return _TagPerm.killPasswordPermissions;
            }
            set
            {
                _TagPerm.killPasswordPermissions = value;
                PasswdBttnSetText(KillPwdBttn, value);
            }
        }

        private RFID_18K6C_TAG_PWD_PERM accPWPerm
        {
            get
            {
                return _TagPerm.accessPasswordPermissions;
            }
            set
            {
                _TagPerm.accessPasswordPermissions = value;
                PasswdBttnSetText(AccPwdBttn, value);
            }
        }

        private RFID_18K6C_TAG_MEM_PERM epcWrPerm
        {
            get
            {
                return _TagPerm.epcMemoryBankPermissions;
            }
            set
            {
                _TagPerm.epcMemoryBankPermissions = value;
                BnkBttnSetText(EPCBnkBttn, value);
            }
        }

        private RFID_18K6C_TAG_MEM_PERM tidWrPerm
        {
            get
            {
                return _TagPerm.tidMemoryBankPermissions;
            }
            set
            {
                _TagPerm.tidMemoryBankPermissions = value;
                BnkBttnSetText(TIDBnkBttn, value);
            }
        }

        private RFID_18K6C_TAG_MEM_PERM userWrPerm
        {
            get
            {
                return _TagPerm.userMemoryBankPermissions;
            }
            set
            {
                _TagPerm.userMemoryBankPermissions = value;
                BnkBttnSetText(UsrBnkBttn, value);
            }
        }
#endregion

        #region Button Label Routines
        private void PasswdBttnSetText(Button bttn, RFID_18K6C_TAG_PWD_PERM perm)
        {
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BankPermDlg));           

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.BankPermDlg));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(BankPermDlg));
            }
            switch (perm)
            {
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_ACCESSIBLE:
                    bttn.Text = resources.GetString("Pwd0Bttn.Text");
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_ACCESSIBLE:
                    bttn.Text = resources.GetString("Pwd1Bttn.Text");
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_SECURED_ACCESSIBLE:
                    bttn.Text = resources.GetString("Pwd2Bttn.Text");
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_NOT_ACCESSIBLE:
                    bttn.Text = resources.GetString("Pwd3Bttn.Text");
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_NO_CHANGE:
                    bttn.Text = resources.GetString("Pwd4Bttn.Text");
                    break;
            }
        }

        private void BnkBttnSetText(Button bttn, RFID_18K6C_TAG_MEM_PERM perm)
        {
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BankPermDlg));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.BankPermDlg));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(BankPermDlg));
            }

            switch (perm)
            {
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_PERM_WRITEABLE:
                    bttn.Text = resources.GetString("Bnk0Bttn.Text");
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_WRITEABLE:
                    bttn.Text = resources.GetString("Bnk1Bttn.Text");
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_SECURED_WRITEABLE:
                    bttn.Text = resources.GetString("Bnk2Bttn.Text");
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_NOT_WRITEABLE:
                    bttn.Text = resources.GetString("Bnk3Bttn.Text");
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_NO_CHANGE:
                    bttn.Text = resources.GetString("Bnk4Bttn.Text");
                    break;
            }
        }
        #endregion

        #region Permission Button Event Handlers
        private void OnKillPWClicked(object sender, EventArgs e)
        {
            BankPermDlg dlg = new BankPermDlg(killPWPerm);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                killPWPerm = dlg.PwdPerm;
            }
            dlg.Dispose();
        }

        private void OnAccPWClicked(object sender, EventArgs e)
        {
            BankPermDlg dlg = new BankPermDlg(accPWPerm);
            DialogResult Res = dlg.ShowDialog();
            if (Res == DialogResult.OK)
            {
                accPWPerm = dlg.PwdPerm;
                if (IsPasswordChange(accPWPerm))
                    PwdReqChkBx.Checked = true;
            }    
            dlg.Dispose();
        }

        private void OnEPCBnkClicked(object sender, EventArgs e)
        {
            BankPermDlg dlg = new BankPermDlg(epcWrPerm);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                epcWrPerm = dlg.MemPerm;
            }
            dlg.Dispose();
        }

        private void OnTIDBnkClicked(object sender, EventArgs e)
        {
            BankPermDlg dlg = new BankPermDlg(tidWrPerm);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                tidWrPerm = dlg.MemPerm;
            }
            dlg.Dispose();
        }

        private void OnUserBnkClicked(object sender, EventArgs e)
        {
            BankPermDlg dlg = new BankPermDlg(userWrPerm);
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                userWrPerm = dlg.MemPerm;
            }
            dlg.Dispose();
        }
        #endregion

        #region Permission Checking Routines
        private bool IsPermanentChange(RFID_18K6C_TAG_PWD_PERM perm)
        {
            return (perm == RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_ACCESSIBLE
               || perm == RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_NOT_ACCESSIBLE);     
        }

        private bool IsPermanentChange(RFID_18K6C_TAG_MEM_PERM perm)
        {
            return (perm == RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_WRITEABLE
            || perm == RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_NOT_WRITEABLE);
        }

        private bool PermanentChangesRequested(ref RFID_18K6C_TAG_PERM perm)
        {
            return (IsPermanentChange(perm.accessPasswordPermissions)
                || IsPermanentChange(perm.killPasswordPermissions)
                || IsPermanentChange(perm.epcMemoryBankPermissions)
                || IsPermanentChange(perm.tidMemoryBankPermissions)
                || IsPermanentChange(perm.userMemoryBankPermissions));
        }

        private bool IsPasswordChange(RFID_18K6C_TAG_PWD_PERM perm)
        {
            return (perm == RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_SECURED_ACCESSIBLE);
        }

        private bool IsPasswordChange(RFID_18K6C_TAG_MEM_PERM perm)
        {
            return (perm == RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_SECURED_WRITEABLE);
        }
        #endregion

        #region Password Dialog Routine
        
        private bool TagIsChosen(out UInt16 chosenPC, out UINT96_T chosenEPC)
        {
            chosenPC = 0;
            chosenEPC = new UINT96_T();
            bool Chosen = false;
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagPermForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagPermForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagPermForm));
            }
            // Check Label.Text
            if (String.Equals(PCLbl.Text, resources.GetString("PCLbl.Text"))
                 && String.Equals(EPCLbl.Text, resources.GetString("EPCLbl.Text")))
                return false;
            // Parse Label Text
            try
            {
                chosenPC = UInt16.Parse(PCLbl.Text, System.Globalization.NumberStyles.HexNumber);
                chosenEPC.ParseString(EPCLbl.Text);
                Chosen = true;
            }
            catch
            {
                Chosen = false;
            }
            return Chosen;
        }
        #endregion

        #region SetPermission Button State
        private void ApplyBttnInitState()
        {
            OpState State = OpState.Stopped;

            ApplyBttn.Tag = State;
        }

        private OpState ApplyBttnState()
        {
            OpState State = OpState.Stopped;

            if (ApplyBttn.Tag is OpState)
            {
                State = (OpState)ApplyBttn.Tag;
            }
            else
                throw new ApplicationException("ApplyBttn Tag is not of type OpState");
            return State;
        }

        private void DisableAllBnkBttns()
        {
            KillPwdBttn.Enabled = false;
            AccPwdBttn.Enabled = false;
            EPCBnkBttn.Enabled = false;
            TIDBnkBttn.Enabled = false;
            UsrBnkBttn.Enabled = false;
        }

        private void EnableAllBnkBttns()
        {
            KillPwdBttn.Enabled = true;
            AccPwdBttn.Enabled = true;
            EPCBnkBttn.Enabled = true;
            TIDBnkBttn.Enabled = true;
            UsrBnkBttn.Enabled = true;
        }

        private void ApplyBttnSetState(OpState newState)
        {
            ApplyBttn.Tag = newState;

           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagPermForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagPermForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagPermForm));
            }

            switch (newState)
            {
                case OpState.Starting:
                    ApplyBttn.Enabled = false;
                    DisableAllBnkBttns(); // disable to prevent user from starting Inventory Op
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    ApplyBttn.Text = "Cancel";
                    ApplyBttn.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    ApplyBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    ApplyBttn.Enabled = true;
                    ApplyBttn.Text = resources.GetString("ApplyBttn.Text");
                    EnableAllBnkBttns();
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }
        }
        #endregion
        private bool PermParamIsValid(out RFID_18K6C_TAG_PERM chosenPerm, out UInt32 accPasswd)
        {
            chosenPerm = new RFID_18K6C_TAG_PERM();
            accPasswd = 0;
            // Check whether Tag has been chosen
           
            chosenPerm = new RFID_18K6C_TAG_PERM();
            chosenPerm.killPasswordPermissions = killPWPerm;
            chosenPerm.accessPasswordPermissions = accPWPerm;
            chosenPerm.epcMemoryBankPermissions = epcWrPerm;
            chosenPerm.tidMemoryBankPermissions = tidWrPerm;
            chosenPerm.userMemoryBankPermissions = userWrPerm;

            if (PermanentChangesRequested(ref chosenPerm))
            {
                if (Program.AskUserConfirm("Some chosen permissions are permanent changes.\r\nAre you sure?") != DialogResult.OK)
                    return false; // user-cancel
            }

            PasswdInputDlg.PasswdType PasswdInputs = PasswdInputDlg.PasswdType.None;

            // Ask for password if checked
            if (PwdReqChkBx.Checked)
                PasswdInputs |= PasswdInputDlg.PasswdType.CurrAcc;

            
            if (PasswdInputs != PasswdInputDlg.PasswdType.None)
            {
                if (!PasswdInputDlg.GetPassword(out accPasswd))
                    return false;
            }
            
             //MessageBox.Show("Password entered: " + Ctxt.CurAccPasswd.ToString("X8"));
             return true;
        }

        private void OnApplyClicked(object sender, EventArgs e)
        {
            RFID_18K6C_TAG_PERM TagPerm;
            UInt32 AccPasswd = 0;
           // Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            UInt16 ChosenPC;
            UINT96_T ChosenEPC;
            OpState CurState = ApplyBttnState();

            switch (CurState)
            {
                case OpState.Stopped:
                    if (!TagIsChosen(out ChosenPC, out ChosenEPC))
                    {
                        MessageBox.Show("Please choose a Tag first");
                    }
                    // Check Param
                    else if (PermParamIsValid(out TagPerm, out AccPasswd))
                    {
                        Program.RefreshStatusLabel(this, "Starting...");
                        ApplyBttnSetState(OpState.Starting);
                        tagAccCnt = 0;
                        //Rdr.LckOpStEvent += LckOpEvtHandler;
                        //Rdr.TagPermSetEvent += TagPermSetEvtHandler;
                        Rdr.RegisterLckOpStEvent(LckOpEvtHandler);
                        Rdr.RegisterTagPermSetEvent(TagPermSetEvtHandler);
                        UserPref Pref = UserPref.GetInstance();
                        if (!Rdr.TagSetPermStart(ref TagPerm, ChosenPC, ref ChosenEPC, AccPasswd))
                        {
                            //Rdr.LckOpStEvent -= LckOpEvtHandler;
                            //Rdr.TagPermSetEvent -= TagPermSetEvtHandler;
                            Rdr.UnregisterLckOpStEvent(LckOpEvtHandler);
                            Rdr.UnregisterTagPermSetEvent(TagPermSetEvtHandler);
                            Program.RefreshStatusLabel(this, "Fail to Start...");
                            ApplyBttnSetState(OpState.Stopped);
                            MessageBox.Show("Tag Permission Set failed to start", "Tag Permission Set Error");
                        }
                    }
                    break;
                case OpState.Started:
                    Program.RefreshStatusLabel(this, "Stopping...");
                    ApplyBttnSetState(OpState.Stopping);
                    if (!Rdr.TagSetPermStop())
                    {
                        //Rdr.LckOpStEvent -= LckOpEvtHandler;
                        //Rdr.TagPermSetEvent -= TagPermSetEvtHandler;
                        Rdr.UnregisterLckOpStEvent(LckOpEvtHandler);
                        Rdr.UnregisterTagPermSetEvent(TagPermSetEvtHandler);
                        Program.RefreshStatusLabel(this, "Fail to Stop...");
                        ApplyBttnSetState(OpState.Stopped);
                        MessageBox.Show("Tag Permission Set failed to stop", "Tag Permission Set Error");
                    }
                    break;
                case OpState.Starting:
                    Program.RefreshStatusLabel(this, "Stopping...");
                    ApplyBttnSetState(OpState.Stopping);
                    if (!Rdr.TagSetPermStop())
                    {
                        // Restore
                        Program.RefreshStatusLabel(this, "Starting...");
                        ApplyBttnSetState(OpState.Starting);
                        Program.ShowWarning("Tag Lock Stop failed during Starting phase, please try again");
                    }
                    break;
            }
        }

        private void LckOpEvtHandler(object sender, LckOpEventArgs e)
        {
            OpState CurState = ApplyBttnState();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case LckOpStatus.started:
                    Program.RefreshStatusLabel(this, "Setting...");
                    ApplyBttnSetState(OpState.Started);
                    break;
                case LckOpStatus.error:
                    switch (CurState)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Set-Permission Start Error");
                            //Rdr.LckOpStEvent -= LckOpEvtHandler;
                            //Rdr.TagPermSetEvent -= TagPermSetEvtHandler;
                            Rdr.UnregisterLckOpStEvent(LckOpEvtHandler);
                            Rdr.UnregisterTagPermSetEvent(TagPermSetEvtHandler);
                            break;
                        case OpState.Stopped:
                            MessageBox.Show(e.ErrMsg, "Tag Set-Permission Stopped Error");
                            break;
                        case OpState.Started:
                            MessageBox.Show(e.ErrMsg, "Tag Set-Permission Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.ErrMsg, "Tag Set-Permission Stopping Error");
                            break;
                    }
                    break;
                case LckOpStatus.tagAccError:
                    // Use 'Transient Dialog' to display informational error
                    // Use MessageBox to display error that require subsequent user action
                    switch (e.TagAccErr)
                    {
                        case AccErrorTypes.Unauthorized:
                            MessageBox.Show("Password Required", "Tag Permission Set Failed");
                            break;
                        case AccErrorTypes.AccessPasswordError:
                            MessageBox.Show("Incorrect Bank Access Password", "Tag Permission Set Failed");
                            break;
                        default:
                            Program.ShowWarning("Tag Access Error:" + "\n"
                                + "Tag: " + ((e.ErrMsg != null) ? e.ErrMsg : "Missing") + "\r\n"
                                + "TagAccError: " + e.TagAccErr.ToString("F"));
                            break;
                    }
                    break;
                case LckOpStatus.stopped:
                case LckOpStatus.errorStopped:
                    //Rdr.LckOpStEvent -= LckOpEvtHandler;
                    //Rdr.TagPermSetEvent -= TagPermSetEvtHandler;
                    Rdr.UnregisterLckOpStEvent(LckOpEvtHandler);
                    Rdr.UnregisterTagPermSetEvent(TagPermSetEvtHandler);
                    ApplyBttnSetState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Finished (" +  tagAccCnt + " accesses)");
                    if (e.Status == LckOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            Program.PromptUserRestartRadio();
                        else
                            MessageBox.Show(e.ErrMsg, "Tag Lock Stopped with Error");
                    }
                    break;
            }
        }

        private int tagAccCnt = 0;
        private void TagPermSetEvtHandler(object sender, DscvrTagEventArgs e)
        {
            // Variable PC/EPC To be handled
            // Just keep count of access for now
            tagAccCnt++;
            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        }
        #endregion

     
        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (ApplyBttnState() != OpState.Stopped)
            {
                MessageBox.Show("Please stop operation first", "Request Denied");
                e.Cancel = true;
            }
        }


        private void OnFormLoad(object sender, EventArgs e)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        #region F11/F4/F5 Hotkey  Handler
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11: // disable auto-fire
                 case eVKey.VK_F19:
                    HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    {
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if ((!F11Depressed) && (ApplyBttnState() == OpState.Stopped))
                            {
                                OnApplyClicked(this, null);
                            }
                            F11Depressed = true;
                        }
                        else // up
                        {
                            if (ApplyBttnState() == OpState.Started
                                || ApplyBttnState() == OpState.Starting)
                            {
                                // Stop!
                                OnApplyClicked(this, null);
                            }
                            F11Depressed = false;
                        }
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (down)
                    {
                        if (ApplyBttnState() == OpState.Stopped)
                        {
                            AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                        }
                    }
                    break;
                case eVKey.VK_F1:
                   if ((down) && (ApplyBttnState() == OpState.Stopped))
                       Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
               // case Keys.F7:
                //case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (ApplyBttnState() != OpState.Stopped)
                    {
                        MessageBox.Show("Please stop operation first", "Request Denied");                      
                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }


        #endregion
    }
}