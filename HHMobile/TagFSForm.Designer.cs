namespace OnRamp
{
    partial class TagFSForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagFSForm));
            this.LngLstButton = new System.Windows.Forms.Button();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.B0ChkBx = new System.Windows.Forms.CheckBox();
            this.B1ChkBx = new System.Windows.Forms.CheckBox();
            this.B2ChkBx = new System.Windows.Forms.CheckBox();
            this.B3ChkBx = new System.Windows.Forms.CheckBox();
            this.ScanButton = new System.Windows.Forms.Button();
            this.B0_1TxtBx = new System.Windows.Forms.TextBox();
            this.B1_1TxtBx = new System.Windows.Forms.TextBox();
            this.B2_1TxtBx = new System.Windows.Forms.TextBox();
            this.B3TxtBx = new System.Windows.Forms.TextBox();
            this.DataDispPanel = new System.Windows.Forms.Panel();
            this.B0_2TxtBx = new System.Windows.Forms.TextBox();
            this.B2_3TxtBx = new System.Windows.Forms.TextBox();
            this.B2_2TxtBx = new System.Windows.Forms.TextBox();
            this.B1_2TxtBx = new System.Windows.Forms.TextBox();
            this.Read1Button = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtLoc = new System.Windows.Forms.TextBox();
            this.txtAsset = new System.Windows.Forms.TextBox();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.Item = new System.Windows.Forms.ColumnHeader();
            this.LastRead = new System.Windows.Forms.ColumnHeader();
            this.ByWhom = new System.Windows.Forms.ColumnHeader();
            this.TagID = new System.Windows.Forms.ColumnHeader();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnMissingItems = new System.Windows.Forms.Button();
            this.epclistbox = new System.Windows.Forms.ListBox();
            this.DataDispPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LngLstButton
            // 
            resources.ApplyResources(this.LngLstButton, "LngLstButton");
            this.LngLstButton.Name = "LngLstButton";
            this.LngLstButton.Click += new System.EventHandler(this.OnLngLstButtonClicked);
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            // 
            // B0ChkBx
            // 
            resources.ApplyResources(this.B0ChkBx, "B0ChkBx");
            this.B0ChkBx.Name = "B0ChkBx";
            this.B0ChkBx.CheckStateChanged += new System.EventHandler(this.OnB0ChkBxStateChanged);
            // 
            // B1ChkBx
            // 
            this.B1ChkBx.Checked = true;
            this.B1ChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.B1ChkBx, "B1ChkBx");
            this.B1ChkBx.Name = "B1ChkBx";
            // 
            // B2ChkBx
            // 
            resources.ApplyResources(this.B2ChkBx, "B2ChkBx");
            this.B2ChkBx.Name = "B2ChkBx";
            this.B2ChkBx.CheckStateChanged += new System.EventHandler(this.OnB2ChkBxStateChanged);
            // 
            // B3ChkBx
            // 
            resources.ApplyResources(this.B3ChkBx, "B3ChkBx");
            this.B3ChkBx.Name = "B3ChkBx";
            this.B3ChkBx.CheckStateChanged += new System.EventHandler(this.OnB3ChkBxStateChanged);
            // 
            // ScanButton
            // 
            resources.ApplyResources(this.ScanButton, "ScanButton");
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Click += new System.EventHandler(this.OnScanButtonClicked);
            // 
            // B0_1TxtBx
            // 
            resources.ApplyResources(this.B0_1TxtBx, "B0_1TxtBx");
            this.B0_1TxtBx.Name = "B0_1TxtBx";
            this.B0_1TxtBx.ReadOnly = true;
            // 
            // B1_1TxtBx
            // 
            resources.ApplyResources(this.B1_1TxtBx, "B1_1TxtBx");
            this.B1_1TxtBx.Name = "B1_1TxtBx";
            this.B1_1TxtBx.ReadOnly = true;
            // 
            // B2_1TxtBx
            // 
            resources.ApplyResources(this.B2_1TxtBx, "B2_1TxtBx");
            this.B2_1TxtBx.Name = "B2_1TxtBx";
            this.B2_1TxtBx.ReadOnly = true;
            // 
            // B3TxtBx
            // 
            resources.ApplyResources(this.B3TxtBx, "B3TxtBx");
            this.B3TxtBx.Name = "B3TxtBx";
            this.B3TxtBx.ReadOnly = true;
            // 
            // DataDispPanel
            // 
            this.DataDispPanel.BackColor = System.Drawing.Color.Transparent;
            this.DataDispPanel.Controls.Add(this.B0_2TxtBx);
            this.DataDispPanel.Controls.Add(this.B2_3TxtBx);
            this.DataDispPanel.Controls.Add(this.B2_2TxtBx);
            this.DataDispPanel.Controls.Add(this.B1_2TxtBx);
            this.DataDispPanel.Controls.Add(this.B0ChkBx);
            this.DataDispPanel.Controls.Add(this.B3TxtBx);
            this.DataDispPanel.Controls.Add(this.B1ChkBx);
            this.DataDispPanel.Controls.Add(this.B2_1TxtBx);
            this.DataDispPanel.Controls.Add(this.B1_1TxtBx);
            this.DataDispPanel.Controls.Add(this.B0_1TxtBx);
            this.DataDispPanel.Controls.Add(this.B2ChkBx);
            this.DataDispPanel.Controls.Add(this.B3ChkBx);
            resources.ApplyResources(this.DataDispPanel, "DataDispPanel");
            this.DataDispPanel.Name = "DataDispPanel";
            // 
            // B0_2TxtBx
            // 
            resources.ApplyResources(this.B0_2TxtBx, "B0_2TxtBx");
            this.B0_2TxtBx.Name = "B0_2TxtBx";
            this.B0_2TxtBx.ReadOnly = true;
            // 
            // B2_3TxtBx
            // 
            resources.ApplyResources(this.B2_3TxtBx, "B2_3TxtBx");
            this.B2_3TxtBx.Name = "B2_3TxtBx";
            this.B2_3TxtBx.ReadOnly = true;
            // 
            // B2_2TxtBx
            // 
            resources.ApplyResources(this.B2_2TxtBx, "B2_2TxtBx");
            this.B2_2TxtBx.Name = "B2_2TxtBx";
            this.B2_2TxtBx.ReadOnly = true;
            // 
            // B1_2TxtBx
            // 
            resources.ApplyResources(this.B1_2TxtBx, "B1_2TxtBx");
            this.B1_2TxtBx.Name = "B1_2TxtBx";
            this.B1_2TxtBx.ReadOnly = true;
            // 
            // Read1Button
            // 
            resources.ApplyResources(this.Read1Button, "Read1Button");
            this.Read1Button.Name = "Read1Button";
            this.Read1Button.Click += new System.EventHandler(this.OnRead1ButtonClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            this.PwdReqChkBx.CheckStateChanged += new System.EventHandler(this.PwdReqChkBx_CheckStateChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.txtLoc);
            this.panel1.Controls.Add(this.txtAsset);
            this.panel1.Controls.Add(this.txtTag);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtLoc
            // 
            resources.ApplyResources(this.txtLoc, "txtLoc");
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.ReadOnly = true;
            this.txtLoc.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtAsset
            // 
            resources.ApplyResources(this.txtAsset, "txtAsset");
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.ReadOnly = true;
            this.txtAsset.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtTag
            // 
            resources.ApplyResources(this.txtTag, "txtTag");
            this.txtTag.Name = "txtTag";
            this.txtTag.ReadOnly = true;
            this.txtTag.TextChanged += new System.EventHandler(this.txtLoc_TextChanged);
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.Item);
            this.lstAsset.Columns.Add(this.LastRead);
            this.lstAsset.Columns.Add(this.ByWhom);
            this.lstAsset.Columns.Add(this.TagID);
            resources.ApplyResources(this.lstAsset, "lstAsset");
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.View = System.Windows.Forms.View.Details;
            this.lstAsset.ItemActivate += new System.EventHandler(this.lstAsset_ItemActivate);
            this.lstAsset.SelectedIndexChanged += new System.EventHandler(this.lstAsset_SelectedIndexChanged);
            // 
            // Item
            // 
            resources.ApplyResources(this.Item, "Item");
            // 
            // LastRead
            // 
            resources.ApplyResources(this.LastRead, "LastRead");
            // 
            // ByWhom
            // 
            resources.ApplyResources(this.ByWhom, "ByWhom");
            // 
            // TagID
            // 
            resources.ApplyResources(this.TagID, "TagID");
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnMissingItems
            // 
            resources.ApplyResources(this.btnMissingItems, "btnMissingItems");
            this.btnMissingItems.Name = "btnMissingItems";
            this.btnMissingItems.Click += new System.EventHandler(this.btnMissingItems_Click);
            // 
            // epclistbox
            // 
            resources.ApplyResources(this.epclistbox, "epclistbox");
            this.epclistbox.Name = "epclistbox";
            // 
            // TagFSForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.btnMissingItems);
            this.Controls.Add(this.DataDispPanel);
            this.Controls.Add(this.ScanButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Read1Button);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.LngLstButton);
            this.Controls.Add(this.epclistbox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagFSForm";
            this.Load += new System.EventHandler(this.OnTagFSFormLoad);
            this.Closed += new System.EventHandler(this.OnTagFSFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnTagFSFormClosing);
            this.DataDispPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagFSForm));
            
            this.LngLstButton = new System.Windows.Forms.Button();
            this.TagCntLabel = new System.Windows.Forms.Label();
            //this.EPCListV = new System.Windows.Forms.ListView();
            this.epclistbox = new System.Windows.Forms.ListBox();
            //this.EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.B0ChkBx = new System.Windows.Forms.CheckBox();
            this.B1ChkBx = new System.Windows.Forms.CheckBox();
            this.B2ChkBx = new System.Windows.Forms.CheckBox();
            this.B3ChkBx = new System.Windows.Forms.CheckBox();
            this.ScanButton = new System.Windows.Forms.Button();
            this.B0_1TxtBx = new System.Windows.Forms.TextBox();
            this.B1_1TxtBx = new System.Windows.Forms.TextBox();
            this.B2_1TxtBx = new System.Windows.Forms.TextBox();
            this.B3TxtBx = new System.Windows.Forms.TextBox();
            this.DataDispPanel = new System.Windows.Forms.Panel();
            this.B0_2TxtBx = new System.Windows.Forms.TextBox();
            this.B2_3TxtBx = new System.Windows.Forms.TextBox();
            this.B2_2TxtBx = new System.Windows.Forms.TextBox();
            this.B1_2TxtBx = new System.Windows.Forms.TextBox();
            this.Read1Button = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtLoc = new System.Windows.Forms.TextBox();
            this.txtAsset = new System.Windows.Forms.TextBox();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.Item = new System.Windows.Forms.ColumnHeader();
            this.LastRead = new System.Windows.Forms.ColumnHeader();
            this.ByWhom = new System.Windows.Forms.ColumnHeader();
            this.TagID = new System.Windows.Forms.ColumnHeader();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnMissingItems = new System.Windows.Forms.Button();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            this.DataDispPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            resources.ApplyResources(RowNumHdr, "RowNumHdr");
            // 
            // LngLstButton
            // 
            resources.ApplyResources(this.LngLstButton, "LngLstButton");
            this.LngLstButton.Name = "LngLstButton";
            this.LngLstButton.Click += new System.EventHandler(this.OnLngLstButtonClicked);
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            // 
            // EPCListV
            // 
            
            //this.EPCListV.Columns.Add(RowNumHdr);
            //this.EPCListV.Columns.Add(this.EPCColHdr);
            //this.EPCListV.FullRowSelect = true;
            //this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.epclistbox, "epclistbox");
            this.epclistbox.Name = "epclistbox";
            this.epclistbox.Location = new System.Drawing.Point(3, 39);
            this.epclistbox.Size = new System.Drawing.Size(232, 165);
            //this.EPCListV.View = System.Windows.Forms.View.Details;
            //this.EPCListV.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            // 
            // EPCColHdr
            // 
            //resources.ApplyResources(this.EPCColHdr, "EPCColHdr");
            // 
            // B0ChkBx
            // 
            resources.ApplyResources(this.B0ChkBx, "B0ChkBx");
            this.B0ChkBx.Name = "B0ChkBx";
            this.B0ChkBx.CheckStateChanged += new System.EventHandler(this.OnB0ChkBxStateChanged);
            // 
            // B1ChkBx
            // 
            this.B1ChkBx.Checked = true;
            this.B1ChkBx.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.B1ChkBx, "B1ChkBx");
            this.B1ChkBx.Name = "B1ChkBx";
            // 
            // B2ChkBx
            // 
            resources.ApplyResources(this.B2ChkBx, "B2ChkBx");
            this.B2ChkBx.Name = "B2ChkBx";
            this.B2ChkBx.CheckStateChanged += new System.EventHandler(this.OnB2ChkBxStateChanged);
            // 
            // B3ChkBx
            // 
            resources.ApplyResources(this.B3ChkBx, "B3ChkBx");
            this.B3ChkBx.Name = "B3ChkBx";
            this.B3ChkBx.CheckStateChanged += new System.EventHandler(this.OnB3ChkBxStateChanged);
            // 
            // ScanButton
            // 
            resources.ApplyResources(this.ScanButton, "ScanButton");
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Click += new System.EventHandler(this.OnScanButtonClicked);
            // 
            // B0_1TxtBx
            // 
            resources.ApplyResources(this.B0_1TxtBx, "B0_1TxtBx");
            this.B0_1TxtBx.Name = "B0_1TxtBx";
            this.B0_1TxtBx.ReadOnly = true;
            // 
            // B1_1TxtBx
            // 
            resources.ApplyResources(this.B1_1TxtBx, "B1_1TxtBx");
            this.B1_1TxtBx.Name = "B1_1TxtBx";
            this.B1_1TxtBx.ReadOnly = true;
            // 
            // B2_1TxtBx
            // 
            resources.ApplyResources(this.B2_1TxtBx, "B2_1TxtBx");
            this.B2_1TxtBx.Name = "B2_1TxtBx";
            this.B2_1TxtBx.ReadOnly = true;
            // 
            // B3TxtBx
            // 
            resources.ApplyResources(this.B3TxtBx, "B3TxtBx");
            this.B3TxtBx.Name = "B3TxtBx";
            this.B3TxtBx.ReadOnly = true;
            // 
            // DataDispPanel
            // 
            this.DataDispPanel.BackColor = System.Drawing.Color.Transparent;
            this.DataDispPanel.Controls.Add(this.B0_2TxtBx);
            this.DataDispPanel.Controls.Add(this.B2_3TxtBx);
            this.DataDispPanel.Controls.Add(this.B2_2TxtBx);
            this.DataDispPanel.Controls.Add(this.B1_2TxtBx);
            this.DataDispPanel.Controls.Add(this.B0ChkBx);
            this.DataDispPanel.Controls.Add(this.B3TxtBx);
            this.DataDispPanel.Controls.Add(this.B1ChkBx);
            this.DataDispPanel.Controls.Add(this.B2_1TxtBx);
            this.DataDispPanel.Controls.Add(this.B1_1TxtBx);
            this.DataDispPanel.Controls.Add(this.B0_1TxtBx);
            this.DataDispPanel.Controls.Add(this.B2ChkBx);
            this.DataDispPanel.Controls.Add(this.B3ChkBx);
            resources.ApplyResources(this.DataDispPanel, "DataDispPanel");
            this.DataDispPanel.Name = "DataDispPanel";
            // 
            // B0_2TxtBx
            // 
            resources.ApplyResources(this.B0_2TxtBx, "B0_2TxtBx");
            this.B0_2TxtBx.Name = "B0_2TxtBx";
            this.B0_2TxtBx.ReadOnly = true;
            // 
            // B2_3TxtBx
            // 
            resources.ApplyResources(this.B2_3TxtBx, "B2_3TxtBx");
            this.B2_3TxtBx.Name = "B2_3TxtBx";
            this.B2_3TxtBx.ReadOnly = true;
            // 
            // B2_2TxtBx
            // 
            resources.ApplyResources(this.B2_2TxtBx, "B2_2TxtBx");
            this.B2_2TxtBx.Name = "B2_2TxtBx";
            this.B2_2TxtBx.ReadOnly = true;
            // 
            // B1_2TxtBx
            // 
            resources.ApplyResources(this.B1_2TxtBx, "B1_2TxtBx");
            this.B1_2TxtBx.Name = "B1_2TxtBx";
            this.B1_2TxtBx.ReadOnly = true;
            // 
            // Read1Button
            // 
            resources.ApplyResources(this.Read1Button, "Read1Button");
            this.Read1Button.Name = "Read1Button";
            this.Read1Button.Click += new System.EventHandler(this.OnRead1ButtonClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            this.PwdReqChkBx.CheckStateChanged += new System.EventHandler(this.PwdReqChkBx_CheckStateChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.txtLoc);
            this.panel1.Controls.Add(this.txtAsset);
            this.panel1.Controls.Add(this.txtTag);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtLoc
            // 
            resources.ApplyResources(this.txtLoc, "txtLoc");
            this.txtLoc.Name = "txtLoc";
            this.txtLoc.ReadOnly = true;
            this.txtLoc.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtAsset
            // 
            resources.ApplyResources(this.txtAsset, "txtAsset");
            this.txtAsset.Name = "txtAsset";
            this.txtAsset.ReadOnly = true;
            this.txtAsset.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtTag
            // 
            resources.ApplyResources(this.txtTag, "txtTag");
            this.txtTag.Name = "txtTag";
            this.txtTag.ReadOnly = true;
            this.txtTag.TextChanged += new System.EventHandler(this.txtLoc_TextChanged);
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.Item);
            this.lstAsset.Columns.Add(this.LastRead);
            this.lstAsset.Columns.Add(this.ByWhom);
            this.lstAsset.Columns.Add(this.TagID);
            resources.ApplyResources(this.lstAsset, "lstAsset");
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.View = System.Windows.Forms.View.Details;
            this.lstAsset.ItemActivate += new System.EventHandler(this.lstAsset_ItemActivate);
            this.lstAsset.SelectedIndexChanged += new System.EventHandler(this.lstAsset_SelectedIndexChanged);
            // 
            // Item
            // 
            resources.ApplyResources(this.Item, "Item");
            // 
            // LastRead
            // 
            resources.ApplyResources(this.LastRead, "LastRead");
            // 
            // ByWhom
            // 
            resources.ApplyResources(this.ByWhom, "ByWhom");
            // 
            // TagID
            // 
            resources.ApplyResources(this.TagID, "TagID");
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnMissingItems
            // 
            resources.ApplyResources(this.btnMissingItems, "btnMissingItems");
            this.btnMissingItems.Name = "btnMissingItems";
            this.btnMissingItems.Click += new System.EventHandler(this.btnMissingItems_Click);
            // 
            // TagFSForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.btnMissingItems);
            this.Controls.Add(this.DataDispPanel);
            this.Controls.Add(this.ScanButton);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Read1Button);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.epclistbox);
            this.Controls.Add(this.LngLstButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagFSForm";
            this.Load += new System.EventHandler(this.OnTagFSFormLoad);
            this.Closed += new System.EventHandler(this.OnTagFSFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnTagFSFormClosing);
            this.DataDispPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button LngLstButton;
        private System.Windows.Forms.Label TagCntLabel;
        private System.Windows.Forms.CheckBox B0ChkBx;
        private System.Windows.Forms.CheckBox B1ChkBx;
        private System.Windows.Forms.CheckBox B2ChkBx;
        private System.Windows.Forms.CheckBox B3ChkBx;
        private System.Windows.Forms.Button ScanButton;
        private System.Windows.Forms.TextBox B0_1TxtBx;
        private System.Windows.Forms.TextBox B1_1TxtBx;
        private System.Windows.Forms.TextBox B2_1TxtBx;
        private System.Windows.Forms.TextBox B3TxtBx;
        private System.Windows.Forms.Panel DataDispPanel;
        private System.Windows.Forms.TextBox B1_2TxtBx;
        private System.Windows.Forms.Button Read1Button;
        private System.Windows.Forms.TextBox B2_2TxtBx;
        private System.Windows.Forms.TextBox B2_3TxtBx;
        private System.Windows.Forms.TextBox B0_2TxtBx;
        private System.Windows.Forms.CheckBox PwdReqChkBx;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtAsset;
        private System.Windows.Forms.TextBox txtLoc;
        private System.Windows.Forms.TextBox txtTag;
        private System.Windows.Forms.ListView lstAsset;
        private System.Windows.Forms.ColumnHeader Item;
        private System.Windows.Forms.ColumnHeader LastRead;
        private System.Windows.Forms.ColumnHeader TagID;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ColumnHeader ByWhom;
        private System.Windows.Forms.Button btnMissingItems;
        private System.Windows.Forms.ListBox epclistbox;
    }
}