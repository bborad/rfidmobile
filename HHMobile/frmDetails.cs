using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;

namespace OnRamp
{
    public partial class frmDetails : Form
    {
        
        private List<Assets> _searchedData;

        public List<Assets> searchedData
        {
            set
            {
                _searchedData = value;
            }
        }

        public frmDetails()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void frmDetails_Load(object sender, EventArgs e)
        {
            BindListData();
        }

        private void BindListData()
        {
            ListViewItem lstItem;
            ListViewItem.ListViewSubItem ls;
            //DataRow dr=new DataRow();
            lstAsset.CheckBoxes = false;
            lstAsset.FullRowSelect = true;
            if (_searchedData.Count <= 0)
            {
                Program.ShowError("No record found.");
                return; 
            }
            
            foreach (Assets Ap in _searchedData)
            {
                lstItem = new ListViewItem();
                //Convert.ToString(dr["TagID"]) + "," + Convert.ToString(dr["Name"])


                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Ap.AssetNo; 
                lstItem.SubItems.Add(ls);

                ls = new ListViewItem.ListViewSubItem();
                ls.Text = Ap.Name; 
                lstItem.SubItems.Add(ls);

                
                ls = new ListViewItem.ListViewSubItem();
                ls.Text = "1";
                lstItem.SubItems.Add(ls);
                lstAsset.Items.Add(lstItem);
            }
            lstAsset.Refresh();

        }

        //private void btnIgnorAll_Click(object sender, EventArgs e)
        //{
        //    lstAsset.Clear();
        //}

        //private void btnChangeLoc_Click(object sender, EventArgs e)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("");
        //    int iOther = 0, iCount = 0;
        //    foreach (ListViewItem lv in lstAsset.Items)
        //    {
        //        if (lv.Checked)
        //        {
        //            iCount++;
        //            if (Convert.ToInt64(lv.SubItems[7].Text.ToString().Trim()) < 0)
        //            {
        //                MessageBox.Show("Please do Synchronisation first.");
        //                return;
        //            }
        //            else
        //            {
        //                if (lv.SubItems[4].Text.ToString().Trim().ToLower() == "location_mismatch")
        //                {
        //                    Assets.UpdateAssetLocation(lv.SubItems[5].Text.ToString().Trim(), Convert.ToInt64(lv.SubItems[7].Text.ToString().Trim()));

        //                    if (sb.ToString() == "")
        //                        sb.Append(lv.SubItems[5].Text.ToString().Trim());
        //                    else
        //                        sb.Append("," + lv.SubItems[5].Text.ToString().Trim());
        //                }
        //                else
        //                    iOther++;
        //            }
        //        }
        //    }

        //    if (iCount == 0)
        //    {
        //        MessageBox.Show("Please select first.");
        //    }
        //    string[] stTag = sb.ToString().Split(',');
        //    int iListcount = 0;
        //    for (int i = 0; i < lstAsset.Items.Count; i++)
        //    {
        //        foreach (string s in stTag)
        //        {
        //            if (lstAsset.Items[i].SubItems[5].Text.ToString().Trim() == s)
        //            {
        //                lstAsset.Items.RemoveAt(i);
        //                iListcount++;
        //            }
        //        }

        //    }
        //    lstAsset.Refresh();

        //    if (iOther > 0)
        //        MessageBox.Show("Descripencies removed only for Location_Mismatch error code.");
        //    else
        //        MessageBox.Show("Descripencies removed successfully.");

        //    //searchedData.Rows[
        //}

        //private void btnSelectAll_Click(object sender, EventArgs e)
        //{
        //    foreach (ListViewItem lv in lstAsset.Items)
        //    {
        //        if (lv.SubItems[4].Text.ToString().Trim().ToLower() == "location_mismatch")
        //            lv.Checked = true;
        //    }
        //}
    }
}