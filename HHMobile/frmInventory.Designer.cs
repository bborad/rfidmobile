namespace OnRamp
{
    partial class frmInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.LocationCol = new System.Windows.Forms.ColumnHeader();
            this.Desc = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.btnChangeLoc = new System.Windows.Forms.Button();
            this.btnIgnorAll = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.cboReason = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnChangeAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.columnHeader3);
            this.lstAsset.Columns.Add(this.columnHeader4);
            this.lstAsset.Columns.Add(this.LocationCol);
            this.lstAsset.Columns.Add(this.Desc);
            this.lstAsset.Columns.Add(this.columnHeader2);
            this.lstAsset.Location = new System.Drawing.Point(3, 29);
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.Size = new System.Drawing.Size(311, 140);
            this.lstAsset.TabIndex = 2;
            this.lstAsset.View = System.Windows.Forms.View.Details;
            this.lstAsset.ItemActivate += new System.EventHandler(this.lstAsset_ItemActivate);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = global::OnRamp.ResourceFiles.TagWrForm.RowNumHdr_Text;
            this.columnHeader1.Width = 30;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "SearchLocation";
            this.columnHeader3.Width = 75;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Item/Inventory";
            this.columnHeader4.Width = 120;
            // 
            // LocationCol
            // 
            this.LocationCol.Text = "Loc";
            this.LocationCol.Width = 80;
            // 
            // Desc
            // 
            this.Desc.Text = "Discrepancies";
            this.Desc.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TagID";
            this.columnHeader2.Width = 120;
            // 
            // btnChangeLoc
            // 
            this.btnChangeLoc.Location = new System.Drawing.Point(137, 172);
            this.btnChangeLoc.Name = "btnChangeLoc";
            this.btnChangeLoc.Size = new System.Drawing.Size(62, 20);
            this.btnChangeLoc.TabIndex = 3;
            this.btnChangeLoc.Text = "Save";
            this.btnChangeLoc.Click += new System.EventHandler(this.btnChangeLoc_Click);
            // 
            // btnIgnorAll
            // 
            this.btnIgnorAll.Location = new System.Drawing.Point(243, 172);
            this.btnIgnorAll.Name = "btnIgnorAll";
            this.btnIgnorAll.Size = new System.Drawing.Size(72, 20);
            this.btnIgnorAll.TabIndex = 4;
            this.btnIgnorAll.Text = "Ignore All";
            this.btnIgnorAll.Visible = false;
            this.btnIgnorAll.Click += new System.EventHandler(this.btnIgnorAll_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(2, 172);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(121, 20);
            this.btnSelectAll.TabIndex = 5;
            this.btnSelectAll.Text = "Select Mismatch";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // cboReason
            // 
            this.cboReason.Location = new System.Drawing.Point(53, 3);
            this.cboReason.Name = "cboReason";
            this.cboReason.Size = new System.Drawing.Size(170, 23);
            this.cboReason.TabIndex = 55;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 19);
            this.label1.Text = "Reason";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.White;
            this.lblPage.Location = new System.Drawing.Point(223, 5);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(35, 20);
            this.lblPage.Text = "Page";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbPage
            // 
            this.cmbPage.Location = new System.Drawing.Point(261, 3);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(53, 23);
            this.cmbPage.TabIndex = 72;
            this.cmbPage.Visible = false;
            // 
            // btnChangeAll
            // 
            this.btnChangeAll.Location = new System.Drawing.Point(213, 172);
            this.btnChangeAll.Name = "btnChangeAll";
            this.btnChangeAll.Size = new System.Drawing.Size(99, 20);
            this.btnChangeAll.TabIndex = 73;
            this.btnChangeAll.Text = "Save All";
            this.btnChangeAll.Click += new System.EventHandler(this.btnChangeAll_Click);
            // 
            // frmInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(317, 195);
            this.Controls.Add(this.btnChangeAll);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboReason);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnChangeLoc);
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.btnIgnorAll);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInventory";
            this.Text = "Descripencies";
            this.Load += new System.EventHandler(this.frmInventory_Load);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.LocationCol = new System.Windows.Forms.ColumnHeader();
            this.Desc = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.btnChangeLoc = new System.Windows.Forms.Button();
            this.btnIgnorAll = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.cboReason = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnChangeAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.columnHeader3);
            this.lstAsset.Columns.Add(this.columnHeader4);
            this.lstAsset.Columns.Add(this.LocationCol);
            this.lstAsset.Columns.Add(this.Desc);
            this.lstAsset.Columns.Add(this.columnHeader2);
            this.lstAsset.Location = new System.Drawing.Point(3, 63);
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.Size = new System.Drawing.Size(232, 149);
            this.lstAsset.TabIndex = 2;
            this.lstAsset.View = System.Windows.Forms.View.Details;
            this.lstAsset.ItemActivate += new System.EventHandler(this.lstAsset_ItemActivate);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 30;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "SearchLocation";
            this.columnHeader3.Width = 75;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Item/Inventory";
            this.columnHeader4.Width = 120;
            // 
            // LocationCol
            // 
            this.LocationCol.Text = "Loc";
            this.LocationCol.Width = 80;
            // 
            // Desc
            // 
            this.Desc.Text = "Discrepancies";
            this.Desc.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TagID";
            this.columnHeader2.Width = 120;
            // 
            // btnChangeLoc
            // 
            this.btnChangeLoc.Location = new System.Drawing.Point(158, 218);
            this.btnChangeLoc.Name = "btnChangeLoc";
            this.btnChangeLoc.Size = new System.Drawing.Size(62, 20);
            this.btnChangeLoc.TabIndex = 4;
            this.btnChangeLoc.Text = "Save ";
            this.btnChangeLoc.Click += new System.EventHandler(this.btnChangeLoc_Click);
            // 
            // btnIgnorAll
            // 
            this.btnIgnorAll.Location = new System.Drawing.Point(3, 218);
            this.btnIgnorAll.Name = "btnIgnorAll";
            this.btnIgnorAll.Size = new System.Drawing.Size(72, 20);
            this.btnIgnorAll.TabIndex = 4;
            this.btnIgnorAll.Text = "Ignore All";
            this.btnIgnorAll.Visible = false;
            this.btnIgnorAll.Click += new System.EventHandler(this.btnIgnorAll_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(112, 244);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(108, 20);
            this.btnSelectAll.TabIndex = 5;
            this.btnSelectAll.Text = "Select Mismatch";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // cboReason
            // 
            this.cboReason.Location = new System.Drawing.Point(53, 3);
            this.cboReason.Name = "cboReason";
            this.cboReason.Size = new System.Drawing.Size(182, 23);
            this.cboReason.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 19);
            this.label1.Text = "Reason";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.White;
            this.lblPage.Location = new System.Drawing.Point(15, 34);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(35, 20);
            this.lblPage.Text = "Page";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbPage
            // 
            this.cmbPage.Location = new System.Drawing.Point(53, 32);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(53, 23);
            this.cmbPage.TabIndex = 1;
            this.cmbPage.Visible = false;
            // 
            // btnChangeAll
            // 
            this.btnChangeAll.Location = new System.Drawing.Point(3, 218);
            this.btnChangeAll.Name = "btnChangeAll";
            this.btnChangeAll.Size = new System.Drawing.Size(138, 20);
            this.btnChangeAll.TabIndex = 3;
            this.btnChangeAll.Text = "Save All";
            this.btnChangeAll.Click += new System.EventHandler(this.btnChangeAll_Click);
            // 
            // frmInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.btnChangeAll);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboReason);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnChangeLoc);
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.btnIgnorAll);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInventory";
            this.Text = "Descripencies";
            this.Load += new System.EventHandler(this.frmInventory_Load);
            this.ResumeLayout(false);

        }

        #endregion


        private System.Windows.Forms.ListView lstAsset;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader LocationCol;
        private System.Windows.Forms.ColumnHeader Desc;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Button btnChangeLoc;
        private System.Windows.Forms.Button btnIgnorAll;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.ComboBox cboReason;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.ComboBox cmbPage;
        private System.Windows.Forms.Button btnChangeAll;
    }
}