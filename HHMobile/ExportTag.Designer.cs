﻿namespace OnRamp
{
    partial class ExportTag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnremove = new System.Windows.Forms.Button();
            this.btnsave = new System.Windows.Forms.Button();
            this.txtlocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblcounter = new System.Windows.Forms.Label();
            this.lstvtaglist = new System.Windows.Forms.ListView();
            this.Tag = new System.Windows.Forms.ColumnHeader();
            this.Asset = new System.Windows.Forms.ColumnHeader();
            this.Note = new System.Windows.Forms.ColumnHeader();
            this.btnclear = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdbbarcode = new System.Windows.Forms.RadioButton();
            this.rdbrfid = new System.Windows.Forms.RadioButton();
            this.txtrfidtag = new System.Windows.Forms.TextBox();
            this.txtassetid = new System.Windows.Forms.TextBox();
            this.lblmessage = new System.Windows.Forms.Label();
            this.chcontinuous = new System.Windows.Forms.CheckBox();
            this.inputPanel1 = new Microsoft.WindowsCE.Forms.InputPanel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnremove
            // 
            this.btnremove.Location = new System.Drawing.Point(7, 243);
            this.btnremove.Name = "btnremove";
            this.btnremove.Size = new System.Drawing.Size(70, 25);
            this.btnremove.TabIndex = 5;
            this.btnremove.Text = "Remove";
            this.btnremove.Click += new System.EventHandler(this.btnremove_Click);
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(163, 243);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(70, 25);
            this.btnsave.TabIndex = 7;
            this.btnsave.Text = "Save";
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // txtlocation
            // 
            this.txtlocation.Location = new System.Drawing.Point(67, 38);
            this.txtlocation.Name = "txtlocation";
            this.txtlocation.Size = new System.Drawing.Size(167, 23);
            this.txtlocation.TabIndex = 0;
            this.txtlocation.TabStop = false;
            this.txtlocation.GotFocus += new System.EventHandler(this.txtrfidtag_GotFocus);
            this.txtlocation.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtlocation_KeyUp);
            this.txtlocation.LostFocus += new System.EventHandler(this.txtlocation_LostFocus);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(7, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 34);
            this.label1.Text = "Location or Status ";
            // 
            // lblcounter
            // 
            this.lblcounter.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.lblcounter.Location = new System.Drawing.Point(163, 5);
            this.lblcounter.Name = "lblcounter";
            this.lblcounter.Size = new System.Drawing.Size(65, 20);
            this.lblcounter.Text = "9999";
            this.lblcounter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lstvtaglist
            // 
            this.lstvtaglist.CheckBoxes = true;
            this.lstvtaglist.Columns.Add(this.Tag);
            this.lstvtaglist.Columns.Add(this.Asset);
            this.lstvtaglist.Columns.Add(this.Note);
            this.lstvtaglist.FullRowSelect = true;
            this.lstvtaglist.Location = new System.Drawing.Point(7, 119);
            this.lstvtaglist.Name = "lstvtaglist";
            this.lstvtaglist.Size = new System.Drawing.Size(227, 120);
            this.lstvtaglist.TabIndex = 4;
            this.lstvtaglist.View = System.Windows.Forms.View.Details;
            // 
            // Tag
            // 
            this.Tag.Text = "TagID";
            this.Tag.Width = 100;
            // 
            // Asset
            // 
            this.Asset.Text = "AssetID";
            this.Asset.Width = 100;
            // 
            // Note
            // 
            this.Note.Text = "Location or Status";
            this.Note.Width = 100;
            // 
            // btnclear
            // 
            this.btnclear.Location = new System.Drawing.Point(85, 243);
            this.btnclear.Name = "btnclear";
            this.btnclear.Size = new System.Drawing.Size(70, 25);
            this.btnclear.TabIndex = 6;
            this.btnclear.Text = "Clear";
            this.btnclear.Click += new System.EventHandler(this.btnclear_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdbbarcode);
            this.panel1.Controls.Add(this.rdbrfid);
            this.panel1.Location = new System.Drawing.Point(7, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 28);
            this.panel1.GotFocus += new System.EventHandler(this.SetFocus);
            // 
            // rdbbarcode
            // 
            this.rdbbarcode.Checked = true;
            this.rdbbarcode.Location = new System.Drawing.Point(3, 4);
            this.rdbbarcode.Name = "rdbbarcode";
            this.rdbbarcode.Size = new System.Drawing.Size(80, 20);
            this.rdbbarcode.TabIndex = 0;
            this.rdbbarcode.TabStop = false;
            this.rdbbarcode.Text = "Barcode";
            this.rdbbarcode.GotFocus += new System.EventHandler(this.SetFocus);
            // 
            // rdbrfid
            // 
            this.rdbrfid.Location = new System.Drawing.Point(86, 4);
            this.rdbrfid.Name = "rdbrfid";
            this.rdbrfid.Size = new System.Drawing.Size(59, 20);
            this.rdbrfid.TabIndex = 1;
            this.rdbrfid.TabStop = false;
            this.rdbrfid.Text = "RFID";
            this.rdbrfid.GotFocus += new System.EventHandler(this.SetFocus);
            this.rdbrfid.CheckedChanged += new System.EventHandler(this.rdbrfid_CheckedChanged);
            // 
            // txtrfidtag
            // 
            this.txtrfidtag.Location = new System.Drawing.Point(7, 73);
            this.txtrfidtag.Name = "txtrfidtag";
            this.txtrfidtag.Size = new System.Drawing.Size(110, 23);
            this.txtrfidtag.TabIndex = 2;
            this.txtrfidtag.Tag = "Scan or Input RIFD Tag ID";
            this.txtrfidtag.GotFocus += new System.EventHandler(this.txtrfidtag_GotFocus);
            this.txtrfidtag.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtrfidtag_KeyUp);
            this.txtrfidtag.LostFocus += new System.EventHandler(this.txtrfidtag_LostFocus);
            // 
            // txtassetid
            // 
            this.txtassetid.Location = new System.Drawing.Point(124, 73);
            this.txtassetid.Name = "txtassetid";
            this.txtassetid.Size = new System.Drawing.Size(110, 23);
            this.txtassetid.TabIndex = 3;
            this.txtassetid.Tag = "Scan or Input Asset ID";
            this.txtassetid.GotFocus += new System.EventHandler(this.txtrfidtag_GotFocus);
            this.txtassetid.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtassetid_KeyUp);
            this.txtassetid.LostFocus += new System.EventHandler(this.txtrfidtag_LostFocus);
            // 
            // lblmessage
            // 
            this.lblmessage.Location = new System.Drawing.Point(7, 98);
            this.lblmessage.Name = "lblmessage";
            this.lblmessage.Size = new System.Drawing.Size(227, 18);
            this.lblmessage.Text = "label2";
            // 
            // chcontinuous
            // 
            this.chcontinuous.Location = new System.Drawing.Point(67, 64);
            this.chcontinuous.Name = "chcontinuous";
            this.chcontinuous.Size = new System.Drawing.Size(130, 20);
            this.chcontinuous.TabIndex = 1;
            this.chcontinuous.Text = "Continuous Read";
            this.chcontinuous.Visible = false;
            // 
            // ExportTag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(638, 455);
            this.Controls.Add(this.chcontinuous);
            this.Controls.Add(this.lblmessage);
            this.Controls.Add(this.txtassetid);
            this.Controls.Add(this.txtrfidtag);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnclear);
            this.Controls.Add(this.lstvtaglist);
            this.Controls.Add(this.lblcounter);
            this.Controls.Add(this.txtlocation);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.btnremove);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportTag";
            this.Text = "Export TagList";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ExportTag_Load);
            this.GotFocus += new System.EventHandler(this.SetFocus);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ExportTag_Closing);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ExportTag_KeyUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ExportTag_KeyDown);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnremove;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.TextBox txtlocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblcounter;
        private System.Windows.Forms.ListView lstvtaglist;
        private System.Windows.Forms.Button btnclear;
        private System.Windows.Forms.ColumnHeader Tag;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdbbarcode;
        private System.Windows.Forms.RadioButton rdbrfid;
        private System.Windows.Forms.TextBox txtrfidtag;
        private System.Windows.Forms.TextBox txtassetid;
        private System.Windows.Forms.Label lblmessage;
        private System.Windows.Forms.ColumnHeader Asset;
        private System.Windows.Forms.ColumnHeader Note;
        private System.Windows.Forms.CheckBox chcontinuous;
        private Microsoft.WindowsCE.Forms.InputPanel inputPanel1;
    }
}