/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Add Employee functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmAddEmployee : Form
    {
        public frmAddEmployee()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        public String TagNo
        {
            set
            {
                txtTag.Text = value;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            if (!(txtTag.Text.Trim().Length <= 24 && txtTag.Text.Trim().Length >= 0))
            {
                MessageBox.Show("Invalid Tag No.");
                return;
            }
            if (txtEmployeeNo.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please Add Employee No.");
                return;
            }
            if (Convert.ToInt32(cboRole.SelectedValue) == 0)
            {
                MessageBox.Show("Please Select Role");
                return;
            }
            if (txtName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please Add User Name");
                return;
            }
            /************* Changes 19/09/2012 ******************
             * 
             * Prevent special Case when a space is in the User
             * Name txt box.
             * 
             * Changes made by Damien Stevenson
             */
            if (txtName.Text.IndexOf(' ') != -1)
            {
                MessageBox.Show("No white spaces are allowed in User Name");
                return;
            }
            /*************** End of Changes *******************/
            if (txtPassword.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please Add Password.");
                return;
            }
            else if (txtPassword.Text.Trim().Length < 6)
            {
                MessageBox.Show("Password should required 6 characters or more.");
                return;
            }

            pnlPassword.BringToFront();
            pnlPassword.Visible = true;
            txtCoPass.Focus(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
                if (txtPassword.Text != txtCoPass.Text)
                {
                    MessageBox.Show("Password Mismatch.");  
                    return;
                }

                pnlPassword.Visible = false;
                //Write Employee save Code.

                try
                {
                    //ClsR
                    Employee.AddEmployee(txtTag.Text.Trim(), txtEmployeeNo.Text.Trim(), txtName.Text.Trim(), txtPassword.Text.Trim(), Convert.ToInt32(cboRole.SelectedValue));
                    MessageBox.Show("Employee Sucessfully Saved.");
                    this.Close();
                }
                catch (System.Web.Services.Protocols.SoapException ex)
                {
                    if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                        Program.ShowError("Request from innvalid IP address.");
                    else
                        Program.ShowError("Network Protocol Failure.");
                    Logger.LogError(ex.Message); 
                }
                catch (System.Data.SqlServerCe.SqlCeException sqlex)
                {
                    MessageBox.Show("Data File is not able to access.");
                    Logger.LogError(sqlex.Message); 
                }
                catch (System.Net.WebException wex)
                {
                    MessageBox.Show("Web exception occured.");
                    Logger.LogError(wex.Message); 
                }
                catch (Exception ep)
                {
                    Logger.LogError(ep.Message); 
                    MessageBox.Show(ep.Message.ToString());
                }
        }

        private void frmAddEmployee_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;

            this.btn_Cancle.Click += new EventHandler(btn_Cancle_Click);
            //Set Location Combo
            DataTable dtList = new DataTable();
            dtList = SecurityGroup.getSecurityGroups();

            DataRow dr = dtList.NewRow();
            dr["ID_SecurityGroup"] = 0;
            dr["Name"] = "Select Role";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboRole.ValueMember = "ID_SecurityGroup";
            cboRole.DisplayMember = "Name";
            cboRole.DataSource = dtList;

            cboRole.SelectedValue = 0;

            panel1.BringToFront();
            panel1.Visible = true;
        }

        void btn_Cancle_Click(object sender, EventArgs e)
        {
            pnlPassword.SendToBack();
            pnlPassword.Visible = false;
            panel1.BringToFront();
            panel1.Visible = true;
            txtPassword.Focus();
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close(); 
        }

        /**************** CHANGE 19/09/2012 *********************
         * 
         * Functions txtName_KeyPress, txtPassword_KeyPress and
         * txtCoPass_KeyPress to prevent user from inserting
         * whitespace and illegal characters into the username
         * and password text-boxes.
         * 
         * Changes made by Damien Stevenson.
         */

        private void txtName_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        private void txtCoPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        /***************** END OF CHANGES **********************/
    }
}