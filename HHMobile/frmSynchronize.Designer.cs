namespace OnRamp
{
    partial class frmSynchronize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.panel1 = new System.Windows.Forms.Panel();
            this.VerLbl = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.SyncProgress = new System.Windows.Forms.ProgressBar();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.VerLbl);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.SyncProgress);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(318, 195);
            // 
            // VerLbl
            // 
            this.VerLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.VerLbl.Location = new System.Drawing.Point(22, 23);
            this.VerLbl.Name = "VerLbl";
            this.VerLbl.Size = new System.Drawing.Size(276, 14);
            this.VerLbl.Text = "Status";
            this.VerLbl.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(85, 117);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(147, 26);
            this.btnStart.TabIndex = 49;
            this.btnStart.Text = "Start";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // SyncProgress
            // 
            this.SyncProgress.Location = new System.Drawing.Point(22, 69);
            this.SyncProgress.Name = "SyncProgress";
            this.SyncProgress.Size = new System.Drawing.Size(276, 27);
            // 
            // frmSynchronize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(318, 195);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSynchronize";
            this.Text = "Synchronize";
            this.Load += new System.EventHandler(this.frmSynchronize_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmSynchronize_Closing);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.panel1 = new System.Windows.Forms.Panel();
            this.VerLbl = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.SyncProgress = new System.Windows.Forms.ProgressBar();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.VerLbl);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.SyncProgress);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 270);
            // 
            // VerLbl
            // 
            this.VerLbl.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.VerLbl.Location = new System.Drawing.Point(86, 70);
            this.VerLbl.Name = "VerLbl";
            this.VerLbl.Size = new System.Drawing.Size(81, 21);
            this.VerLbl.Text = "Status";
            this.VerLbl.Visible = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(46, 127);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(147, 26);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // SyncProgress
            // 
            this.SyncProgress.Location = new System.Drawing.Point(3, 94);
            this.SyncProgress.Name = "SyncProgress";
            this.SyncProgress.Size = new System.Drawing.Size(232, 27);
            // 
            // frmSynchronize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSynchronize";
            this.Text = "Synchronize";
            this.Load += new System.EventHandler(this.frmSynchronize_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmSynchronize_Closing);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ProgressBar SyncProgress;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label VerLbl;
    }
}