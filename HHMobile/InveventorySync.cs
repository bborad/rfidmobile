/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Sync Inventory functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmInveventorySync : Form
    {
        public delegate void stateHandler(Int64 status);

        public frmInveventorySync()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                Sync ss = new Sync();
                DataTable dtResult = new DataTable("dtResult");
                dtResult = ss.SyncInventory();

                // Open new Tag Write window
                frmInventory InvFm = new frmInventory();
                InvFm.Closed += new EventHandler(this.OnOperFrmClosed);
                InvFm.searchedData = dtResult.Copy();
                InvFm.Show();
                syncProgress.Value = syncProgress.Maximum;
                this.Enabled = false;
                // disable form until this new form closed 
                ss = null;
                GC.Collect();

            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message);
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message);
                MessageBox.Show(ep.Message.ToString());
            }
            finally
            {
                this.Close();
            }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            try
            {
               // syncProgress.Value = syncProgress.Minimum;this.Enabled = true;
            }
            catch
            {
            }
            
        }

        private void frmInveventorySync_Load(object sender, EventArgs e)
        {
            this.Text = "Inventory Sync - " + UserPref.CurVersionNo;
             


            ProgressStatus.progressState += new ProgressStatus.progressStateHandler(ProgressStatus_progressState);
        }

        void ProgressStatus_progressState(long status)
        {
            if (this.InvokeRequired)
            {
                //ProgressStatus_progressState(status);
                this.Invoke(new stateHandler(this.ProgressStatus_progressState), new Object[] { status });
                return;
            }
            syncProgress.Value = (int)status;
            return;
        }

        private void frmInveventorySync_Closing(object sender, CancelEventArgs e)
        {
            ProgressStatus.progressState -= new ProgressStatus.progressStateHandler(ProgressStatus_progressState);
        }

    }
}