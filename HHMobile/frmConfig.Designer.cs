﻿namespace OnRamp
{
    partial class frmConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RstButton = new System.Windows.Forms.Button();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.TabCtrl = new System.Windows.Forms.TabControl();
            this.tpControlSettings = new System.Windows.Forms.TabPage();
            this.lblFirmWareVer = new System.Windows.Forms.Label();
            this.txtHoppingMode = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtProtocolVer = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLBTTime = new System.Windows.Forms.TextBox();
            this.txtSession = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtScantime = new System.Windows.Forms.TextBox();
            this.txtQValue = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.AntTab = new System.Windows.Forms.TabPage();
            this.label53 = new System.Windows.Forms.Label();
            this.txtHighValue = new System.Windows.Forms.TextBox();
            this.txtMedValue = new System.Windows.Forms.TextBox();
            this.txtLowValue = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.PwrLvlValLbl = new System.Windows.Forms.Label();
            this.PwrLvlTrkBr = new System.Windows.Forms.TrackBar();
            this.label11 = new System.Windows.Forms.Label();
            this.ServiceSettings = new System.Windows.Forms.TabPage();
            this.label50 = new System.Windows.Forms.Label();
            this.txtMsgDisplayTimeOut = new System.Windows.Forms.TextBox();
            this.txtPageSize = new System.Windows.Forms.TextBox();
            this.txtServiceUrl = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.rbErrLogDisable = new System.Windows.Forms.RadioButton();
            this.rbErrLogEnable = new System.Windows.Forms.RadioButton();
            this.label48 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.tabBarcodeSettings = new System.Windows.Forms.TabPage();
            this.rb2DScanner = new System.Windows.Forms.RadioButton();
            this.rb1DScanner = new System.Windows.Forms.RadioButton();
            this.tpDDSelectionSettings = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.txtObjectListPageSize = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.rbRfid = new System.Windows.Forms.RadioButton();
            this.rbBarcode = new System.Windows.Forms.RadioButton();
            this.rbManual = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbDropDown = new System.Windows.Forms.RadioButton();
            this.rbInstantSearch = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.tpaddmode = new System.Windows.Forms.TabPage();
            this.rdbstandalonemode = new System.Windows.Forms.RadioButton();
            this.pnlstandalonemode = new System.Windows.Forms.Panel();
            this.txtdays = new System.Windows.Forms.TextBox();
            this.txtcsvpath = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txttagprefix = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.rdbintegratedmode = new System.Windows.Forms.RadioButton();
            this.TabCtrl.SuspendLayout();
            this.tpControlSettings.SuspendLayout();
            this.AntTab.SuspendLayout();
            this.ServiceSettings.SuspendLayout();
            this.tabBarcodeSettings.SuspendLayout();
            this.tpDDSelectionSettings.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tpaddmode.SuspendLayout();
            this.pnlstandalonemode.SuspendLayout();
            this.SuspendLayout();
            // 
            // RstButton
            // 
            this.RstButton.Location = new System.Drawing.Point(102, 243);
            this.RstButton.Name = "RstButton";
            this.RstButton.Size = new System.Drawing.Size(102, 23);
            this.RstButton.TabIndex = 8;
            this.RstButton.Text = "Undo Changes";
            this.RstButton.Click += new System.EventHandler(this.RstButton_Click);
            // 
            // ApplyButton
            // 
            this.ApplyButton.Location = new System.Drawing.Point(9, 243);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(91, 23);
            this.ApplyButton.TabIndex = 7;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.Click += new System.EventHandler(this.OnApplyButtonClicked);
            // 
            // TabCtrl
            // 
            this.TabCtrl.Controls.Add(this.tpControlSettings);
            this.TabCtrl.Controls.Add(this.AntTab);
            this.TabCtrl.Controls.Add(this.ServiceSettings);
            this.TabCtrl.Controls.Add(this.tabBarcodeSettings);
            this.TabCtrl.Controls.Add(this.tpDDSelectionSettings);
            this.TabCtrl.Controls.Add(this.tpaddmode);
            this.TabCtrl.Dock = System.Windows.Forms.DockStyle.Top;
            this.TabCtrl.Location = new System.Drawing.Point(0, 0);
            this.TabCtrl.Name = "TabCtrl";
            this.TabCtrl.SelectedIndex = 0;
            this.TabCtrl.Size = new System.Drawing.Size(238, 237);
            this.TabCtrl.TabIndex = 6;
            // 
            // tpControlSettings
            // 
            this.tpControlSettings.BackColor = System.Drawing.SystemColors.Window;
            this.tpControlSettings.Controls.Add(this.lblFirmWareVer);
            this.tpControlSettings.Controls.Add(this.txtHoppingMode);
            this.tpControlSettings.Controls.Add(this.label14);
            this.tpControlSettings.Controls.Add(this.txtProtocolVer);
            this.tpControlSettings.Controls.Add(this.label13);
            this.tpControlSettings.Controls.Add(this.label1);
            this.tpControlSettings.Controls.Add(this.label9);
            this.tpControlSettings.Controls.Add(this.label8);
            this.tpControlSettings.Controls.Add(this.txtLBTTime);
            this.tpControlSettings.Controls.Add(this.txtSession);
            this.tpControlSettings.Controls.Add(this.label7);
            this.tpControlSettings.Controls.Add(this.label5);
            this.tpControlSettings.Controls.Add(this.label2);
            this.tpControlSettings.Controls.Add(this.txtScantime);
            this.tpControlSettings.Controls.Add(this.txtQValue);
            this.tpControlSettings.Controls.Add(this.label6);
            this.tpControlSettings.Controls.Add(this.label12);
            this.tpControlSettings.Location = new System.Drawing.Point(4, 25);
            this.tpControlSettings.Name = "tpControlSettings";
            this.tpControlSettings.Size = new System.Drawing.Size(230, 208);
            this.tpControlSettings.Text = "Control Settings";
            // 
            // lblFirmWareVer
            // 
            this.lblFirmWareVer.Location = new System.Drawing.Point(5, 183);
            this.lblFirmWareVer.Name = "lblFirmWareVer";
            this.lblFirmWareVer.Size = new System.Drawing.Size(222, 20);
            this.lblFirmWareVer.Text = "Firmware Ver : ";
            // 
            // txtHoppingMode
            // 
            this.txtHoppingMode.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtHoppingMode.Location = new System.Drawing.Point(89, 31);
            this.txtHoppingMode.Name = "txtHoppingMode";
            this.txtHoppingMode.ReadOnly = true;
            this.txtHoppingMode.Size = new System.Drawing.Size(118, 19);
            this.txtHoppingMode.TabIndex = 81;
            this.txtHoppingMode.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(0, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(92, 16);
            this.label14.Text = "Hopping Mode";
            // 
            // txtProtocolVer
            // 
            this.txtProtocolVer.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtProtocolVer.Location = new System.Drawing.Point(89, 5);
            this.txtProtocolVer.Name = "txtProtocolVer";
            this.txtProtocolVer.ReadOnly = true;
            this.txtProtocolVer.Size = new System.Drawing.Size(118, 19);
            this.txtProtocolVer.TabIndex = 80;
            this.txtProtocolVer.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(5, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(83, 16);
            this.label13.Text = "Protocol Ver.";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(139, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.Text = "1 ~ 3";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(139, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 16);
            this.label9.Text = "0 ~ 60";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(139, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.Text = "0 ~ 15";
            // 
            // txtLBTTime
            // 
            this.txtLBTTime.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtLBTTime.Location = new System.Drawing.Point(91, 133);
            this.txtLBTTime.Name = "txtLBTTime";
            this.txtLBTTime.Size = new System.Drawing.Size(36, 19);
            this.txtLBTTime.TabIndex = 78;
            this.txtLBTTime.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtSession
            // 
            this.txtSession.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtSession.Location = new System.Drawing.Point(91, 108);
            this.txtSession.Name = "txtSession";
            this.txtSession.Size = new System.Drawing.Size(36, 19);
            this.txtSession.TabIndex = 77;
            this.txtSession.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(21, 133);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 16);
            this.label7.Text = "LBT Time";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(28, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 16);
            this.label5.Text = "Session";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(21, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.Text = "ScanTime";
            // 
            // txtScantime
            // 
            this.txtScantime.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtScantime.Location = new System.Drawing.Point(91, 83);
            this.txtScantime.Name = "txtScantime";
            this.txtScantime.Size = new System.Drawing.Size(36, 19);
            this.txtScantime.TabIndex = 75;
            this.txtScantime.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtQValue
            // 
            this.txtQValue.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtQValue.Location = new System.Drawing.Point(91, 57);
            this.txtQValue.Name = "txtQValue";
            this.txtQValue.Size = new System.Drawing.Size(36, 19);
            this.txtQValue.TabIndex = 74;
            this.txtQValue.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(31, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 16);
            this.label6.Text = "QValue";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(138, 133);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 16);
            this.label12.Text = "0 ~ 4000";
            // 
            // AntTab
            // 
            this.AntTab.BackColor = System.Drawing.Color.White;
            this.AntTab.Controls.Add(this.label53);
            this.AntTab.Controls.Add(this.txtHighValue);
            this.AntTab.Controls.Add(this.txtMedValue);
            this.AntTab.Controls.Add(this.txtLowValue);
            this.AntTab.Controls.Add(this.label52);
            this.AntTab.Controls.Add(this.label51);
            this.AntTab.Controls.Add(this.PwrLvlValLbl);
            this.AntTab.Controls.Add(this.PwrLvlTrkBr);
            this.AntTab.Controls.Add(this.label11);
            this.AntTab.Location = new System.Drawing.Point(4, 25);
            this.AntTab.Name = "AntTab";
            this.AntTab.Size = new System.Drawing.Size(230, 208);
            this.AntTab.Text = "Antenna";
            // 
            // label53
            // 
            this.label53.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label53.Location = new System.Drawing.Point(16, 120);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(80, 18);
            this.label53.Text = "High Value";
            this.label53.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtHighValue
            // 
            this.txtHighValue.Location = new System.Drawing.Point(105, 115);
            this.txtHighValue.Name = "txtHighValue";
            this.txtHighValue.Size = new System.Drawing.Size(61, 23);
            this.txtHighValue.TabIndex = 32;
            this.txtHighValue.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtMedValue
            // 
            this.txtMedValue.Location = new System.Drawing.Point(105, 89);
            this.txtMedValue.Name = "txtMedValue";
            this.txtMedValue.Size = new System.Drawing.Size(61, 23);
            this.txtMedValue.TabIndex = 29;
            this.txtMedValue.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtLowValue
            // 
            this.txtLowValue.Location = new System.Drawing.Point(105, 63);
            this.txtLowValue.Name = "txtLowValue";
            this.txtLowValue.Size = new System.Drawing.Size(61, 23);
            this.txtLowValue.TabIndex = 25;
            this.txtLowValue.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label52
            // 
            this.label52.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label52.Location = new System.Drawing.Point(16, 94);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(80, 18);
            this.label52.Text = "Medium Value";
            this.label52.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label51
            // 
            this.label51.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label51.Location = new System.Drawing.Point(16, 68);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(80, 18);
            this.label51.Text = "Low Value";
            this.label51.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // PwrLvlValLbl
            // 
            this.PwrLvlValLbl.ForeColor = System.Drawing.Color.Indigo;
            this.PwrLvlValLbl.Location = new System.Drawing.Point(160, 28);
            this.PwrLvlValLbl.Name = "PwrLvlValLbl";
            this.PwrLvlValLbl.Size = new System.Drawing.Size(70, 25);
            this.PwrLvlValLbl.Text = "MEDIUM";
            this.PwrLvlValLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.PwrLvlValLbl.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // PwrLvlTrkBr
            // 
            this.PwrLvlTrkBr.LargeChange = 1;
            this.PwrLvlTrkBr.Location = new System.Drawing.Point(2, 25);
            this.PwrLvlTrkBr.Maximum = 3;
            this.PwrLvlTrkBr.Minimum = 1;
            this.PwrLvlTrkBr.Name = "PwrLvlTrkBr";
            this.PwrLvlTrkBr.Size = new System.Drawing.Size(162, 28);
            this.PwrLvlTrkBr.TabIndex = 11;
            this.PwrLvlTrkBr.Value = 2;
            this.PwrLvlTrkBr.ValueChanged += new System.EventHandler(this.PwrLvlTrkBr_ValueChanged);
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(209, 22);
            this.label11.Text = "Antenna Power (0 - 31 dBm):";
            // 
            // ServiceSettings
            // 
            this.ServiceSettings.BackColor = System.Drawing.Color.White;
            this.ServiceSettings.Controls.Add(this.label50);
            this.ServiceSettings.Controls.Add(this.txtMsgDisplayTimeOut);
            this.ServiceSettings.Controls.Add(this.txtPageSize);
            this.ServiceSettings.Controls.Add(this.txtServiceUrl);
            this.ServiceSettings.Controls.Add(this.label49);
            this.ServiceSettings.Controls.Add(this.rbErrLogDisable);
            this.ServiceSettings.Controls.Add(this.rbErrLogEnable);
            this.ServiceSettings.Controls.Add(this.label48);
            this.ServiceSettings.Controls.Add(this.label47);
            this.ServiceSettings.Controls.Add(this.label46);
            this.ServiceSettings.Location = new System.Drawing.Point(4, 25);
            this.ServiceSettings.Name = "ServiceSettings";
            this.ServiceSettings.Size = new System.Drawing.Size(230, 208);
            this.ServiceSettings.Text = "Service Settings";
            // 
            // label50
            // 
            this.label50.Location = new System.Drawing.Point(163, 72);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(39, 20);
            this.label50.Text = "sec";
            // 
            // txtMsgDisplayTimeOut
            // 
            this.txtMsgDisplayTimeOut.Location = new System.Drawing.Point(82, 69);
            this.txtMsgDisplayTimeOut.MaxLength = 4;
            this.txtMsgDisplayTimeOut.Name = "txtMsgDisplayTimeOut";
            this.txtMsgDisplayTimeOut.Size = new System.Drawing.Size(81, 23);
            this.txtMsgDisplayTimeOut.TabIndex = 16;
            this.txtMsgDisplayTimeOut.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtPageSize
            // 
            this.txtPageSize.Location = new System.Drawing.Point(3, 68);
            this.txtPageSize.MaxLength = 4;
            this.txtPageSize.Name = "txtPageSize";
            this.txtPageSize.Size = new System.Drawing.Size(64, 23);
            this.txtPageSize.TabIndex = 5;
            this.txtPageSize.TextChanged += new System.EventHandler(this.txt_TextChanged);
            this.txtPageSize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPageSize_KeyPress);
            // 
            // txtServiceUrl
            // 
            this.txtServiceUrl.Location = new System.Drawing.Point(3, 22);
            this.txtServiceUrl.Name = "txtServiceUrl";
            this.txtServiceUrl.Size = new System.Drawing.Size(224, 23);
            this.txtServiceUrl.TabIndex = 1;
            this.txtServiceUrl.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label49
            // 
            this.label49.Location = new System.Drawing.Point(82, 48);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(134, 20);
            this.label49.Text = "Msg Display TimeOut";
            // 
            // rbErrLogDisable
            // 
            this.rbErrLogDisable.Location = new System.Drawing.Point(98, 115);
            this.rbErrLogDisable.Name = "rbErrLogDisable";
            this.rbErrLogDisable.Size = new System.Drawing.Size(98, 20);
            this.rbErrLogDisable.TabIndex = 11;
            this.rbErrLogDisable.Text = "Disable";
            // 
            // rbErrLogEnable
            // 
            this.rbErrLogEnable.Location = new System.Drawing.Point(12, 115);
            this.rbErrLogEnable.Name = "rbErrLogEnable";
            this.rbErrLogEnable.Size = new System.Drawing.Size(84, 20);
            this.rbErrLogEnable.TabIndex = 10;
            this.rbErrLogEnable.Text = "Enable";
            this.rbErrLogEnable.CheckedChanged += new System.EventHandler(this.rbErrLogEnable_CheckedChanged);
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(3, 94);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(110, 20);
            this.label48.Text = "Error Logging ";
            // 
            // label47
            // 
            this.label47.Location = new System.Drawing.Point(3, 47);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(64, 20);
            this.label47.Text = "Page Size";
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(3, 4);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(110, 20);
            this.label46.Text = "Service URL";
            // 
            // tabBarcodeSettings
            // 
            this.tabBarcodeSettings.BackColor = System.Drawing.Color.White;
            this.tabBarcodeSettings.Controls.Add(this.rb2DScanner);
            this.tabBarcodeSettings.Controls.Add(this.rb1DScanner);
            this.tabBarcodeSettings.Location = new System.Drawing.Point(4, 25);
            this.tabBarcodeSettings.Name = "tabBarcodeSettings";
            this.tabBarcodeSettings.Size = new System.Drawing.Size(230, 208);
            this.tabBarcodeSettings.Text = "Barcode Settings";
            // 
            // rb2DScanner
            // 
            this.rb2DScanner.Location = new System.Drawing.Point(26, 62);
            this.rb2DScanner.Name = "rb2DScanner";
            this.rb2DScanner.Size = new System.Drawing.Size(146, 20);
            this.rb2DScanner.TabIndex = 13;
            this.rb2DScanner.Text = "2D Scanner";
            // 
            // rb1DScanner
            // 
            this.rb1DScanner.Location = new System.Drawing.Point(26, 25);
            this.rb1DScanner.Name = "rb1DScanner";
            this.rb1DScanner.Size = new System.Drawing.Size(146, 20);
            this.rb1DScanner.TabIndex = 12;
            this.rb1DScanner.Text = "1D Scanner";
            this.rb1DScanner.CheckedChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // tpDDSelectionSettings
            // 
            this.tpDDSelectionSettings.BackColor = System.Drawing.Color.White;
            this.tpDDSelectionSettings.Controls.Add(this.panel3);
            this.tpDDSelectionSettings.Controls.Add(this.panel2);
            this.tpDDSelectionSettings.Controls.Add(this.panel1);
            this.tpDDSelectionSettings.Location = new System.Drawing.Point(4, 25);
            this.tpDDSelectionSettings.Name = "tpDDSelectionSettings";
            this.tpDDSelectionSettings.Size = new System.Drawing.Size(230, 208);
            this.tpDDSelectionSettings.Text = "Input Settings";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Window;
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.txtObjectListPageSize);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Location = new System.Drawing.Point(0, 154);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(228, 48);
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.label15.Location = new System.Drawing.Point(11, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(211, 17);
            this.label15.Text = "Set Page size to 0 to turn off the paging";
            // 
            // txtObjectListPageSize
            // 
            this.txtObjectListPageSize.Location = new System.Drawing.Point(129, 2);
            this.txtObjectListPageSize.MaxLength = 4;
            this.txtObjectListPageSize.Name = "txtObjectListPageSize";
            this.txtObjectListPageSize.Size = new System.Drawing.Size(87, 23);
            this.txtObjectListPageSize.TabIndex = 7;
            this.txtObjectListPageSize.TextChanged += new System.EventHandler(this.txtObjectListPageSize_TextChanged);
            this.txtObjectListPageSize.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtObjectListPageSize_KeyPress);
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(7, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 20);
            this.label10.Text = "Object list page size";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Window;
            this.panel2.Controls.Add(this.rbRfid);
            this.panel2.Controls.Add(this.rbBarcode);
            this.panel2.Controls.Add(this.rbManual);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(1, 64);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(227, 89);
            // 
            // rbRfid
            // 
            this.rbRfid.Location = new System.Drawing.Point(13, 63);
            this.rbRfid.Name = "rbRfid";
            this.rbRfid.Size = new System.Drawing.Size(133, 15);
            this.rbRfid.TabIndex = 25;
            this.rbRfid.Text = "RFID Input";
            // 
            // rbBarcode
            // 
            this.rbBarcode.Location = new System.Drawing.Point(13, 41);
            this.rbBarcode.Name = "rbBarcode";
            this.rbBarcode.Size = new System.Drawing.Size(133, 15);
            this.rbBarcode.TabIndex = 24;
            this.rbBarcode.Text = "Barcode Input";
            // 
            // rbManual
            // 
            this.rbManual.Location = new System.Drawing.Point(12, 21);
            this.rbManual.Name = "rbManual";
            this.rbManual.Size = new System.Drawing.Size(133, 15);
            this.rbManual.TabIndex = 22;
            this.rbManual.Text = "Manual Input Only";
            this.rbManual.CheckedChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(4, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 16);
            this.label4.Text = "Input Method";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Window;
            this.panel1.Controls.Add(this.rbDropDown);
            this.panel1.Controls.Add(this.rbInstantSearch);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(227, 64);
            // 
            // rbDropDown
            // 
            this.rbDropDown.Location = new System.Drawing.Point(14, 23);
            this.rbDropDown.Name = "rbDropDown";
            this.rbDropDown.Size = new System.Drawing.Size(133, 15);
            this.rbDropDown.TabIndex = 17;
            this.rbDropDown.Text = "Dropdown Select";
            this.rbDropDown.CheckedChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // rbInstantSearch
            // 
            this.rbInstantSearch.Location = new System.Drawing.Point(13, 40);
            this.rbInstantSearch.Name = "rbInstantSearch";
            this.rbInstantSearch.Size = new System.Drawing.Size(133, 17);
            this.rbInstantSearch.TabIndex = 16;
            this.rbInstantSearch.Text = "Instant Search";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(4, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 16);
            this.label3.Text = "Listing Method";
            // 
            // tpaddmode
            // 
            this.tpaddmode.Controls.Add(this.rdbstandalonemode);
            this.tpaddmode.Controls.Add(this.pnlstandalonemode);
            this.tpaddmode.Controls.Add(this.txttagprefix);
            this.tpaddmode.Controls.Add(this.label16);
            this.tpaddmode.Controls.Add(this.rdbintegratedmode);
            this.tpaddmode.Location = new System.Drawing.Point(4, 25);
            this.tpaddmode.Name = "tpaddmode";
            this.tpaddmode.Size = new System.Drawing.Size(230, 208);
            this.tpaddmode.Text = "App Mode";
            this.tpaddmode.Click += new System.EventHandler(this.tpaddmode_Click);
            // 
            // rdbstandalonemode
            // 
            this.rdbstandalonemode.Location = new System.Drawing.Point(12, 46);
            this.rdbstandalonemode.Name = "rdbstandalonemode";
            this.rdbstandalonemode.Size = new System.Drawing.Size(138, 20);
            this.rdbstandalonemode.TabIndex = 1;
            this.rdbstandalonemode.Text = "Stand-alone Mode";
            this.rdbstandalonemode.CheckedChanged += new System.EventHandler(this.rdbstandalonemode_CheckedChanged);
            // 
            // pnlstandalonemode
            // 
            this.pnlstandalonemode.Controls.Add(this.txtdays);
            this.pnlstandalonemode.Controls.Add(this.txtcsvpath);
            this.pnlstandalonemode.Controls.Add(this.label17);
            this.pnlstandalonemode.Controls.Add(this.label19);
            this.pnlstandalonemode.Controls.Add(this.label18);
            this.pnlstandalonemode.Enabled = false;
            this.pnlstandalonemode.Location = new System.Drawing.Point(4, 57);
            this.pnlstandalonemode.Name = "pnlstandalonemode";
            this.pnlstandalonemode.Size = new System.Drawing.Size(222, 96);
            // 
            // txtdays
            // 
            this.txtdays.Location = new System.Drawing.Point(104, 63);
            this.txtdays.Name = "txtdays";
            this.txtdays.Size = new System.Drawing.Size(63, 23);
            this.txtdays.TabIndex = 2;
            this.txtdays.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtcsvpath
            // 
            this.txtcsvpath.Location = new System.Drawing.Point(8, 34);
            this.txtcsvpath.Name = "txtcsvpath";
            this.txtcsvpath.Size = new System.Drawing.Size(208, 23);
            this.txtcsvpath.TabIndex = 1;
            this.txtcsvpath.Text = "\\Flash Disk\\RFIDMobile";
            this.txtcsvpath.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(8, 14);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 20);
            this.label17.Text = "CSV Path";
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(171, 66);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(48, 20);
            this.label19.Text = "day(s).";
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(8, 66);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(98, 20);
            this.label18.Text = "Delete file after";
            // 
            // txttagprefix
            // 
            this.txttagprefix.Location = new System.Drawing.Point(12, 180);
            this.txttagprefix.Name = "txttagprefix";
            this.txttagprefix.Size = new System.Drawing.Size(206, 23);
            this.txttagprefix.TabIndex = 0;
            this.txttagprefix.Text = "000000000000000000000000";
            this.txttagprefix.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(12, 160);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 20);
            this.label16.Text = "Tag Prefix";
            // 
            // rdbintegratedmode
            // 
            this.rdbintegratedmode.Checked = true;
            this.rdbintegratedmode.Location = new System.Drawing.Point(12, 20);
            this.rdbintegratedmode.Name = "rdbintegratedmode";
            this.rdbintegratedmode.Size = new System.Drawing.Size(138, 20);
            this.rdbintegratedmode.TabIndex = 0;
            this.rdbintegratedmode.Text = "Integrated Mode";
            this.rdbintegratedmode.CheckedChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // frmConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.RstButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.TabCtrl);
            this.Name = "frmConfig";
            this.Text = "RFID Config";
            this.Load += new System.EventHandler(this.frmConfig_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnForm_KeyUp);
            this.TabCtrl.ResumeLayout(false);
            this.tpControlSettings.ResumeLayout(false);
            this.AntTab.ResumeLayout(false);
            this.ServiceSettings.ResumeLayout(false);
            this.tabBarcodeSettings.ResumeLayout(false);
            this.tpDDSelectionSettings.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tpaddmode.ResumeLayout(false);
            this.pnlstandalonemode.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RstButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.TabControl TabCtrl;
        private System.Windows.Forms.TabPage tpControlSettings;
        private System.Windows.Forms.TextBox txtHoppingMode;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtProtocolVer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLBTTime;
        private System.Windows.Forms.TextBox txtSession;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtScantime;
        private System.Windows.Forms.TextBox txtQValue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TabPage AntTab;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtHighValue;
        private System.Windows.Forms.TextBox txtMedValue;
        private System.Windows.Forms.TextBox txtLowValue;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label PwrLvlValLbl;
        private System.Windows.Forms.TrackBar PwrLvlTrkBr;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TabPage ServiceSettings;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox txtMsgDisplayTimeOut;
        private System.Windows.Forms.TextBox txtPageSize;
        private System.Windows.Forms.TextBox txtServiceUrl;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.RadioButton rbErrLogDisable;
        private System.Windows.Forms.RadioButton rbErrLogEnable;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.TabPage tabBarcodeSettings;
        private System.Windows.Forms.RadioButton rb2DScanner;
        private System.Windows.Forms.RadioButton rb1DScanner;
        private System.Windows.Forms.Label lblFirmWareVer;
        private System.Windows.Forms.TabPage tpDDSelectionSettings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbInstantSearch;
        private System.Windows.Forms.RadioButton rbDropDown;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbManual;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rbBarcode;
        private System.Windows.Forms.RadioButton rbRfid;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox txtObjectListPageSize;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tpaddmode;
        private System.Windows.Forms.RadioButton rdbstandalonemode;
        private System.Windows.Forms.RadioButton rdbintegratedmode;
        private System.Windows.Forms.Panel pnlstandalonemode;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtdays;
        private System.Windows.Forms.TextBox txtcsvpath;
        private System.Windows.Forms.TextBox txttagprefix;

    }
}