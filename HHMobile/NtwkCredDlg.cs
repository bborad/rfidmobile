using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class NtwkCredDlg : Form
    {
        public NtwkCredDlg()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        public String Title
        {
            set
            {
                this.Text = value;
            }
        }

        public String Prompt
        {
            set 
            {
                InfoLbl.Text = value;
            }
        }

        public String UsrName
        {
            set
            {
                UnameTxtBx.Text = (value == null) ? String.Empty : value;
            }
            get
            {
                return UnameTxtBx.Text;
            }
        }

        public String Password
        {
            set
            {
                PasswdTxtBx.Text = (value == null) ? String.Empty : value;
            }
            get
            {
                return PasswdTxtBx.Text;
            }
        }

        

    }
}