using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class LstViewSelDlg : Form
    {

        private int beginSelIdx = -1;
        
        public int SelectedRow
        {
            get
            {
                return (ItemListV.SelectedIndices != null && ItemListV.SelectedIndices.Count > 0) ?
                    ItemListV.SelectedIndices[0] : -1;
            }
        }

        public int InitialRow
        {
            set
            {
                 beginSelIdx = (value < 0)? -1 : value;
                // Actual Selection won't be made until Form is Loaded
            }
        }

        public LstViewSelDlg(String ItemName)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            // Replace <Item> in Title bar with 'ItemName'
            SetTitleBarString(ItemName);

        }

        private void SetTitleBarString(String ItemName)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LstViewSelDlg));

            // Replace <Item> in Title bar with 'ItemName'
            String DefTitle = resources.GetString(@"$this.Text");
            StringBuilder TitleBldr = new StringBuilder(DefTitle);
            TitleBldr = TitleBldr.Replace("<Item>", ItemName);
            this.Text = TitleBldr.ToString();
        }

        // Assuming that the array size of colHdrs and widths are the same
        // Setting either Argument to NULL would hide the 'Column Header'
        public void SetColHeaders(int[] widths)
        {
            ItemListV.HeaderStyle = ColumnHeaderStyle.None;
            uint NumCols = (uint)widths.Length;
            for (int c = 0; c < NumCols; c++)
            {
                ItemListV.Columns.Add("", widths[c], HorizontalAlignment.Left);
            }
        }

        public void SetColHeaders(String[] colHdrs, int[] widths)
        {
            if (colHdrs == null || colHdrs.Length == 0)
                SetColHeaders( widths );    
            else
            {
                ItemListV.HeaderStyle = ColumnHeaderStyle.Nonclickable;
                uint NumCols = (uint)((colHdrs.Length < widths.Length) ? colHdrs.Length : widths.Length);
                for (int c = 0; c < NumCols; c++)
                    ItemListV.Columns.Add(colHdrs[c], widths[c], HorizontalAlignment.Left);
            }
        }

        public void AddRow(String[] rowItems)
        {
            ListViewItem row = new ListViewItem(rowItems);
            ItemListV.Items.Add(row);
        }

        private void OnFormLoaded(object sender, EventArgs e)
        {
            // Set current selection
            if (beginSelIdx >= ItemListV.Items.Count)
                beginSelIdx = ItemListV.Items.Count - 1; //auto-adjust
            ResetItemListV();
        }

        private void ResetItemListV()
        {
            if (beginSelIdx >= 0)
            {
                ListViewItem row = ItemListV.Items[beginSelIdx];
                row.Selected = true;
                // Make Ensure Visible More than beginSelIdx () to skip over the scrollbar (if there is)
                int rowsFromBottom = (ItemListV.Items.Count - beginSelIdx);
                if (rowsFromBottom > 2)
                    ItemListV.EnsureVisible(beginSelIdx+2);
                else if (rowsFromBottom > 1)
                    ItemListV.EnsureVisible(beginSelIdx+1);
                else
                    ItemListV.EnsureVisible(beginSelIdx);
            }
            else
            {
                foreach (ListViewItem row in ItemListV.Items)
                    row.Selected = false;
            }
        }

        private void OnRstBttnClicked(object sender, EventArgs e)
        {
            ResetItemListV();
            ItemListV.Focus();
        }

    }
}