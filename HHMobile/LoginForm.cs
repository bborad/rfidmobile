/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Login functionality
 **************************************************************************************/

using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using CS101UILib;
using System.Windows.Forms;
using System.Threading;
using ClsRampdb;
using ClslibSerialNo;
using ClsLibBKLogs;
using System.IO;
using System.Threading;
using HHDeviceInterface.Utility;

namespace OnRamp
{
    public delegate void AsynMethodCallBack();
    public partial class LoginForm : Form
    {
        RegistrationData objRegData;
        public LoginForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }


            /* Select the prompt message */
            this.UsrNameTxtBx.SelectAll();

        }


        private void SkipButtonClicked(object sender, EventArgs e)
        {
            /* 
            // Close LoginForm and Open MainMenuForm
            MainMenuForm MainMenu = new MainMenuForm();
            MainMenu.Show();
            this.Close();
            */
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            Program.ReqEnd = true;
        }

        private void OnOKButtonClicked(object sender, EventArgs e)
        {
            /* 
              if (VerifyPassword() == true)
                 SkipButtonClicked(sender, e);
             */

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                UserPref Pref = UserPref.GetInstance();

                Login.urlStr = Pref.ServiceURL;
                Login.OnLineMode = chkOnline.Checked;

                /**************** Changes 19/09/2012 ******************
                 * Changes: Ensure that In-Memory Data Set Mode is always
                 * disabled. This is done to ensure that data is saved 
                 * to sqlce database so that data is not lost upon soft/hard
                 * restart.
                 * 
                 * Changes made by Damien Stevenson
                 */

                //Pref.UsingNewDBLib = chkUseInMemoryMode.Checked;
                Pref.UsingNewDBLib = false;

                /***************** End of Changes ***********************/

                bool IsRegistered = true;
                if (Login.verifyPassword(UsrNameTxtBx.Text, PwdTxtBx.Text))
                {
                    Pref.UserName = UsrNameTxtBx.Text.Trim();
                    Pref.Passwd = PwdTxtBx.Text.Trim();
                    String appCode;
                    DateTime ExpiryDate;
                    string NoOfItemCode;
                    if (chkRegistration.Checked || !objRegData.IsKeyFound || Program.newInstallation)
                    {
                        IsRegistered = false;
                    }
                    else
                    {

                        if (Encrypt.IsValidKey(objRegData.RegKey, SerialNo.GetDeviceID(), out appCode, out ExpiryDate, out NoOfItemCode))
                        {
                            if (objRegData.LastUpdateDate.Date < DateTime.Now.Date)
                            {
                                objRegData.LastUpdateDate = DateTime.Now;
                            }
                            else if (objRegData.LastUpdateDate.Date > DateTime.Now.Date)
                            {
                                MessageBox.Show("Invalid date found. Cannot proceed.");
                                IsRegistered = false;
                            }

                            if (appCode.Length != 4 || appCode.Substring(1, 1) != "1")
                            {
                                MessageBox.Show("Invalid Registration key, Please contact your vendor.");
                                IsRegistered = false;
                            }
                            else if (ExpiryDate.Date <= objRegData.LastUpdateDate.Date)
                            {
                                MessageBox.Show("Your subscription has expired. Please contact your vendor.");
                                IsRegistered = false;
                            }
                            //else if (Pref.ItemLimit.Trim() != NoOfItemCode.Trim())
                            //{
                            //    MessageBox.Show("Corrupted Item Limit.");
                            //    IsRegistered = false;
                            //}
                            if (IsRegistered)
                            { 
                                if (UserPref.AppModuleImplementation)
                                {
                                    int iClient = 0;

                                    string regKey = objRegData.RegKey;

                                    string appModuleCode = Encrypt.GetAppModuleCodes(ref regKey, ref iClient);
                                    appModuleCode = Convert.ToString(Convert.ToInt32(appModuleCode, 16), 2);

                                    appModuleCode = appModuleCode.PadLeft(32, '0');

                                    if (UserPref.forClient != UserPref.Client.Access)
                                    {
                                        if (iClient == 1)
                                            UserPref.forClient = UserPref.Client.OnRamp;
                                        else
                                            UserPref.forClient = UserPref.Client.SmartTrack;
                                    }

                                    Pref.allApplicationModules = appModuleCode;
                                    Pref.SetAppModules();                                 

                                }

                                Int32 NoOfItems = Encrypt.GetMaxNoofItems(NoOfItemCode);
                                if (NoOfItems != 0)
                                {
                                    if (!Login.OnLineMode)
                                    {
                                        Login.ItemLimit = NoOfItems - Assets.getNoOfItems();
                                        if (Login.ItemLimit < 0)
                                        {
                                            MessageBox.Show("You are crossing registered item limit(" + NoOfItems + "). Please upgrade your registration.");
                                            IsRegistered = false;
                                        }
                                    }
                                    else
                                    {
                                        Login.ItemLimit = NoOfItems;
                                        if (Assets.getNoOfItems() > NoOfItems)
                                        {
                                            MessageBox.Show("You are crossing registered item limit(" + NoOfItems + "). Please upgrade your registration.");
                                            IsRegistered = false;
                                        }
                                    }
                                }
                                else
                                {
                                    Login.ItemLimit = 100000;
                                }
                            }

                            objRegData.SaveKeyData(UserPref.RegKeyfolderPath, UserPref.pattern);

                        }
                        else
                        {
                            MessageBox.Show("Invalid Registration key, Please contact your vendor.");
                            IsRegistered = false;
                        }


                    }
                    Cursor.Current = Cursors.Default;
                    if (IsRegistered)
                    {
                        string dbFilePath = CEConn.dbFilePath;
                        Pref.UsingNewDBLib = chkUseInMemoryMode.Checked;

                        CEConn.dbFilePath = dbFilePath;

                        //Doesn't work if is called on new thread
                        //Program.barThread = new System.Threading.Thread(new System.Threading.ThreadStart(Program.InitBarcodeReader));
                        //Program.barThread.Start();


                        Program.InitBarcodeReader();
                        //MainMenuForm MainMenu = new MainMenuForm();
                        //MainMenu.Show();
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    }
                    else
                    {
                        Program.InitBarcodeReader();
                        frmRegister fReg = new frmRegister();
                        if (fReg.ShowDialog() == DialogResult.OK)
                            this.DialogResult = DialogResult.OK;
                       
                        this.Close();
                    }

                   
                    /*
                     * MainMenuForm MainMenu = new MainMenuForm();
                    MainMenu.Show();
                    this.Close(); 
                     * */
                    //this.Enabled = false;
                    //MainMenu.Closed += new EventHandler(MainMenu_Closed);
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    if (Login.err.Length != 0)
                        MessageBox.Show(Login.err);
                    else
                        MessageBox.Show("Invalid User Name or Password.");

                    Logger.LogError("Invalid User Name or Password.");

                }
                //Login.OnLineMode = false;// chkOnline.Checked;  
            }
            catch (ApplicationException AP)
            {
                Cursor.Current = Cursors.Default;
                Logger.LogError(AP.Message);
                Program.ShowError(AP.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sEx)
            {
                Cursor.Current = Cursors.Default;
                Logger.LogError(sEx.Message);
                Program.ShowError("Data File is not able to access.");
            }
            catch (System.Net.WebException wEx)
            {
                Cursor.Current = Cursors.Default;
                Logger.LogError(wEx.Message);
                Program.ShowError("Web exception occured.");
            }
            catch (Exception ep)
            {
                Cursor.Current = Cursors.Default;
                Logger.LogError(ep.Message);
                Program.ShowError(ep.Message.ToString());
            }

            Cursor.Current = Cursors.Default;
        }

        void MainMenu_Closed(object sender, EventArgs e)
        {
            this.Enabled = true;
            //throw new NotImplementedException();
        }

        private bool VerifyPassword()
        {
            UserPref Pref = UserPref.GetInstance();

            if (Pref.UserName == null || Pref.UserName.Length == 0)
                return true;
            if (String.Compare(Pref.UserName, UsrNameTxtBx.Text, true) != 0)
            {
                MessageBox.Show("Unrecognized Login Name", "Failed");
                return false;
            }
            if (Pref.Passwd == null || Pref.Passwd.Length == 0)
                return true;
            if (String.Compare(Pref.Passwd, PwdTxtBx.Text, false) != 0)
            {
                MessageBox.Show("Incorrect Password", "Failed");
                return false;
            }

            return true;
        }

        private void OnUsrNameTxtBxKeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        private void OnPwdTxtBxKeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        private void PwdTxtBx_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoginForm_Closed(object sender, EventArgs e)
        {

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            chkUseInMemoryMode.Visible = false;

            if (UserPref.GetInstance().ApplicationMode == UserPref.AppMode.StandAloneMode)
                chkOnline.Visible = false;

            lblVersion.Text = UserPref.CurVersionNo;

            UserPref Pref = UserPref.GetInstance();

            UsrNameTxtBx.Text = Pref.UserName;
            PwdTxtBx.Text = Pref.Passwd;

            Logger.enableErrorLogging = Pref.EnableErrorLogging;

            objRegData = new RegistrationData();
            objRegData.FileName = UserPref.KeyFileName;

            try
            {
                if (File.Exists(UserPref.RegKeyfolderPath + UserPref.KeyFileName))
                {
                    objRegData.GetKeyData(UserPref.RegKeyfolderPath, UserPref.pattern);

                }
                else
                {
                    objRegData.LastUpdateDate = DateTime.Now;

                    if (!Directory.Exists(UserPref.RegKeyfolderPath))
                        Directory.CreateDirectory(UserPref.RegKeyfolderPath);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //this.Close();
            }

        }
    }
}