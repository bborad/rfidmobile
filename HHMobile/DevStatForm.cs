using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ClsLibBKLogs;
using ClsReaderLib;

using ClsReaderLib.Devices;

using InvtrySession = OnRamp.UserPref.OperSession;
using OperQPopRange = OnRamp.UserPref.OperQPopRange;

namespace OnRamp
{
    public partial class DevStatForm : Form
    {

        private void RemoveTab(TabPage page)
        {
            int TabIdx = this.TabCtrl.TabPages.IndexOf(page);
            if (TabIdx >= 0)
                this.TabCtrl.TabPages.RemoveAt(TabIdx);
        }

        public DevStatForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            // Move DQPanel to 0,37 (same x,y as FQPanel)
            DQPanel.Location = FQPanel.Location;

            // remove SELECT, QUERY parameters Tab
            RemoveTab(TagSelTab);
            RemoveTab(QryPrmTab);

            TabCtrl.SelectedIndex = 0; // somehow tab removal affects selected index

            // Disable user choosing of FreqBand
            FreqBndNumLbl.Location = new Point(FreqBndCmbBx.Location.X, BandFrqRangeLbl.Location.Y);


            InitFreqBndRanges();
            InitFreqBndCountryRanges();

            ResetChosenChnIdx();
        }

        static DevStatForm()
        {
            DevStatForm.InitFreqLists();
            DevStatForm.InitFreqBndCountriesArr();
            DevStatForm.InitFreqBndCountryRanges();
        }

        #region Shared Button Routines

        private void OnRstButtonClicked(object sender, EventArgs e)
        {
            TabPage SelPage = TabCtrl.TabPages[TabCtrl.SelectedIndex];
            UserPref upref = UserPref.GetInstance();

            if (SelPage == InvtrySetupTab)
            {
                InvtrySetupTabResetVal();
            }
            else if (SelPage == TagAttrTab)
            {
                TagAttrTabResetVal();
            }
            else if (SelPage == OvrHtProtTab)
            {
                OvrHtProtTabResetVal();
            }
            else if (SelPage == FreqTab)
            {
                FreqTabResetVal(loadedFreqBnd, loadedFreqGrp, loadedChn, loadedLBTEn);
                FreqTabUpdateApplyBttnEnabled();
            }
            else if (SelPage == LnkPrfTab)
            {
                LnkProfTabResetVal(loadedLnkProfNum);
                LnkProfTabUpdateApplyBttnEnabled();
            }
            else if (SelPage == AntTab)
            {
                AntTabResetVal(ref loadedPrt0Cfg);
                AntTabUpdateApplyBttnEnabled();
            }
            else if (SelPage == TagSelTab)
            {
                TagSelTabResetVal(ref loadedSelCrit);
                // Apply Button supposed to stay Disabled
            }
            else if (SelPage == QryPrmTab)
            {
                QryPrmTabResetVal(ref loadedQryParms);
            }
            else if (SelPage == tpDDSelectionSettings)
            {


                if (upref.IDInputMethod == 0)
                {
                    rbManual.Checked = true;

                }
                else if (upref.IDInputMethod == 1)
                {
                    rbBarcode.Checked = true;
                }
                else if (upref.IDInputMethod == 2)
                {
                    rbRfid.Checked = true;
                }

                if (upref.DDSelectionOption == 0)
                {
                    rbDropDown.Checked = true;

                }
                else if (upref.DDSelectionOption == 1)
                {
                    rbInstantSearch.Checked = true;
                }
                txtObjectListPageSize.Text = upref.SelectScreen_PageSize.ToString();

            }
        }

        private void OnApplyButtonClicked(object sender, EventArgs e)
        {
            TabPage SelPage = TabCtrl.TabPages[TabCtrl.SelectedIndex];
            if (SelPage == InvtrySetupTab)
            {
                InvtrySetupTabApplyChanges();
                Program.RefreshStatusLabel(this, "Changes Saved");
            }
            else if (SelPage == TagAttrTab)
            {
                TagAttrTabApplyChanges();
                Program.RefreshStatusLabel(this, "Changes Saved");
            }
            else if (SelPage == OvrHtProtTab)
            {
                OvrHtProtTabApplyChanges();
            }
            else if (SelPage == FreqTab)
            {
                this.Enabled = false;
                if (!FreqTabApplyChanges())
                    this.Enabled = true;
            }
            else if (SelPage == LnkPrfTab)
            {
                this.Enabled = false;
                if (!LnkProfTabApplyChanges())
                    this.Enabled = true;
            }
            else if (SelPage == AntTab)
            {
                this.Enabled = false;

                if (ValidatePowerLevelValues(true))
                {
                    AntTabApplyChanges();
                       
                } 
               this.Enabled = true;
            }
            else if (SelPage == tabBarcodeSettings)
            {
                this.Enabled = false;

                UserPref Pref = UserPref.GetInstance();

                if(rb1DScanner.Checked)
                    Pref.ScannerType = 1;
                else if (rb2DScanner.Checked)
                    Pref.ScannerType = 2;

                this.Enabled = true;
            }
            else if (SelPage == TagSelTab || SelPage == QryPrmTab || SelPage == TempTab)
            {
                MessageBox.Show("This setting is for viewing only", "Request Denied");
                ApplyButton.Enabled = false;
            }
            else if (SelPage == ServiceSettings)
            {
                //MessageBox.Show("Functionality is under construction.");

                UserPref Pref = UserPref.GetInstance();
                Pref.ServiceURL = textBox1.Text;
                if (String.IsNullOrEmpty(textBox1.Text))
                {
                    Program.ShowWarning("Please input Service URL");
                    return;
                }

                try
                {
                    if (String.IsNullOrEmpty(txtPageSize.Text))
                    {
                        Program.ShowWarning("Please input Page Size");
                        txtPageSize.Focus();
                        return;
                    }

                    Int32 pagesize = Convert.ToInt32(txtPageSize.Text);
                    if (pagesize <= 0)
                    {
                        Program.ShowWarning("Page size must be greater than zero.");
                        txtPageSize.Focus();
                        return;
                    }
                    Pref.PageSize = pagesize;
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    Program.ShowWarning("Page Size must be a numeric value.");
                    txtPageSize.Focus();
                    return;
                }

                try
                {
                    if (String.IsNullOrEmpty(txtMsgDisplayTimeOut.Text))
                    {
                        Program.ShowWarning("Please input Msg Display TimeOut value");
                        txtMsgDisplayTimeOut.Focus();
                        return;
                    }

                    Int32 MsgDisplayTimeOut = Convert.ToInt32(txtMsgDisplayTimeOut.Text);
                    if (MsgDisplayTimeOut <= 0)
                    {
                        Program.ShowWarning("Msg Display TimeOut must be greater than zero.");
                        txtPageSize.Focus();
                        return;
                    }
                    Pref.MsgWndTimeOut = MsgDisplayTimeOut;
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    Program.ShowWarning("Page Size must be a numeric value.");
                    txtPageSize.Focus();
                    return;
                }

                if (rbErrLogEnable.Checked)
                {
                    Pref.EnableErrorLogging = true;
                }
                else
                {
                    Pref.EnableErrorLogging = false;
                }

                Logger.enableErrorLogging = Pref.EnableErrorLogging;

                //InvtrySetupUpdateApplyBttnEnabled();
                ApplyButton.Enabled = true;
                Program.RefreshStatusLabel(this, "Changes Saved");

            }
            else if (SelPage == tpDDSelectionSettings)
            {
                this.Enabled = false;

                UserPref Pref = UserPref.GetInstance();
                if (rbDropDown.Checked)
                {
                    Pref.DDSelectionOption = 0; // pre load

                }
                else
                {
                    Pref.DDSelectionOption = 1; // non pre Load
                }

                if (rbManual.Checked)
                {
                    Pref.IDInputMethod = 0; // Manual Input only ( no Scan Button )
                }
                else if (rbBarcode.Checked)
                {
                    Pref.IDInputMethod = 1; // Barcode Input
                }
                else if (rbRfid.Checked)
                {
                    Pref.IDInputMethod = 2; // RFID Input
                }
                if (txtObjectListPageSize.Text.Trim() != "")
                {
                    Pref.SelectScreen_PageSize = Convert.ToInt32(txtObjectListPageSize.Text);
                }
                else
                {
                    Pref.SelectScreen_PageSize = 0;
                }

                this.Enabled = true;
            }

        }

        #endregion

        #region TabCtrl Routines
        private void OnSelectedPageChanged(object sender, EventArgs e)
        {
            TabPage SelPage = TabCtrl.TabPages[TabCtrl.SelectedIndex];
            if (SelPage == InvtrySetupTab)
            {
                InvtrySetupUpdateApplyBttnEnabled();
            }
            else if (SelPage == TagAttrTab)
            {
                TagAttrUpdateApplyBttnEnabled();
            }
            else if (SelPage == OvrHtProtTab)
            {
                OvrHtProtUpdateApplyBttnEnabled();
            }
            else if (SelPage == FreqTab)
            {
                // if(fr
                FreqTabUpdateApplyBttnEnabled();
            }
            else if (SelPage == LnkPrfTab)
            {
                LnkProfTabUpdateApplyBttnEnabled();
            }
            else if (SelPage == AntTab)
            {
                AntTabUpdateApplyBttnEnabled();
            }
            else if (SelPage == TagSelTab || SelPage == QryPrmTab || SelPage == TempTab)
            {
                ApplyButton.Enabled = false; // This is a Read-only Tab
            }

            Program.RefreshStatusLabel(this, String.Empty); // clear status from previous page
        }
        #endregion

        #region Inventory Setup Page
        private void InvtrySetupTabResetVal()
        {
            UserPref Pref = UserPref.GetInstance();
            textBox1.Text = Pref.ServiceURL.ToString();

            txtPageSize.Text = Pref.PageSize.ToString();

            txtMsgDisplayTimeOut.Text = Pref.MsgWndTimeOut.ToString();

            if (Pref.EnableErrorLogging == true)
            {
                rbErrLogDisable.Checked = false;
                rbErrLogEnable.Checked = true;
            }
            else
            {
                rbErrLogDisable.Checked = true;
                rbErrLogEnable.Checked = false;
            }

            InvtrySessCmbBx.SelectedIndex = (int)Pref.Session;
            Pref.PopRange = OperQPopRange.SpecificNum; // hard-code
            PopSzTxtBx.Text = Pref.PopSize.ToString();
            QLbl.Text = Pref.QSize.ToString();
            if (Pref.QAlgo == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ)
                DynQBttn.Checked = true;
            else
                FixedQBttn.Checked = true;

            Byte[] DataFld = Pref.PCMask;
            if (DataFld == null || DataFld.Length == 0)
                MaskInput.SetPC(false, null);
            else
                MaskInput.SetPC(true, DataFld);
            DataFld = Pref.EPCMask;
            if (DataFld == null || DataFld.Length == 0)
                MaskInput.SetEPC(false, null);
            else
                MaskInput.SetEPC(true, DataFld);
        }

        private void InvtrySetupTabApplyChanges()
        {
            UserPref Pref = UserPref.GetInstance();
            Pref.Session = (InvtrySession)InvtrySessCmbBx.SelectedIndex;
            if (String.IsNullOrEmpty(PopSzTxtBx.Text))
            {
                Program.ShowWarning("Please input population size");
                return;
            }
            Pref.PopSize = Int32.Parse(PopSzTxtBx.Text);
            Pref.QAlgo = DynQBttn.Checked ? RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ
                : RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ;
            Byte[] Data;
            bool Checked = MaskInput.GetPC(out Data);
            Pref.PCMask = (Checked) ? Data : null;
            Checked = MaskInput.GetEPC(out Data);
            Pref.EPCMask = (Checked) ? Data : null;
            InvtrySetupUpdateApplyBttnEnabled();
            Program.RefreshStatusLabel(this, "Changes Saved");
        }

        private bool InvtrySetupIsModified()
        {
            UserPref Pref = UserPref.GetInstance();
            InvtrySession ChosenSess = (InvtrySession)InvtrySessCmbBx.SelectedIndex;
            // Guard against empty PopSzTxtBx even though Validating event is already
            // doing the same thing. Reason: this function gets called when the TextBox is
            // not yet initialized
            int ChosenPopulation = String.IsNullOrEmpty(PopSzTxtBx.Text) ?
                Pref.PopSize : Int32.Parse(PopSzTxtBx.Text);
            RFID_18K6C_SINGULATION_ALGORITHM ChosenQAlgo = DynQBttn.Checked ?
                RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ
                : RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ;
            Byte[] Data;
            bool FldEnabled = MaskInput.GetPC(out Data);
            bool PCModified = CheckMaskModified(FldEnabled, Pref.PCMask, Data);
            FldEnabled = MaskInput.GetEPC(out Data);
            bool EPCModified = CheckMaskModified(FldEnabled, Pref.EPCMask, Data);

            return (ChosenSess != Pref.Session) || (ChosenPopulation != Pref.PopSize)
                || (ChosenQAlgo != Pref.QAlgo) || PCModified || EPCModified;
        }

        private void InvtrySetupUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = InvtrySetupIsModified();
        }


        private void OnInvtrySessChanged(object sender, EventArgs e)
        {
            InvtrySetupUpdateApplyBttnEnabled();
        }


        private void OnPopSzChanged(object sender, EventArgs e)
        {
            // update label
            UserPref Pref = UserPref.GetInstance();
            try
            {
                QLbl.Text = UserPref.Population2QSize(Int32.Parse(PopSzTxtBx.Text)).ToString();
                InvtrySetupUpdateApplyBttnEnabled();
            }
            catch (OverflowException)
            {
                Program.ShowWarning("Input population size too large.");
                PopSzTxtBx.Text = Pref.PopSize.ToString();
            }
            catch (FormatException)
            {
                if (String.IsNullOrEmpty(PopSzTxtBx.Text) == false)
                {
                    Program.ShowWarning("Invalid population size input");
                    PopSzTxtBx.Text = String.Empty; // clear it
                }
            }
        }

        private void OnPopSzValidating(object sender, CancelEventArgs e)
        {
            if (String.IsNullOrEmpty(PopSzTxtBx.Text))
            {
                Program.ShowWarning("Please input population size");
                e.Cancel = true;
            }
        }

        private void OnDynQBttnChecked(object sender, EventArgs e)
        {
            InvtrySetupUpdateApplyBttnEnabled();
        }

        private void OnFixedQBttnChecked(object sender, EventArgs e)
        {
            InvtrySetupUpdateApplyBttnEnabled();
        }

        private bool CheckMaskModified(bool enabled, Byte[] savedData, Byte[] curData)
        {
            bool modified = false;

            if (enabled == false)
            {
                if (savedData != null && savedData.Length > 0)
                    modified = true;
            }
            else // currently enabled
            {
                if (curData == null || curData.Length == 0)
                {
                    // Current: Empty; Saved: Non-Empty
                    if (savedData != null && savedData.Length > 0)
                        modified = true;
                }
                else if (savedData == null || savedData.Length == 0)
                {
                    // Current: Non-Empty;  Saved: Empty
                    modified = true;
                }
                else
                {
                    // Current: Non-Empty; Saved: Non-Empty
                    if (curData.Length != savedData.Length)
                        modified = true;
                    else
                    {
                        for (int i = 0; i < curData.Length; i++)
                        {
                            if (curData[i] != savedData[i])
                            {
                                modified = true;
                                break;
                            }
                        }
                    }
                }
            }
            return modified;
        }

        private void OnMaskInputFieldChanged(object sender, EPCMaskInput.FieldChangedEventArgs e)
        {
            UserPref Pref = UserPref.GetInstance();

            InvtrySetupUpdateApplyBttnEnabled();
        }

        private void OnPopSzKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }

        #endregion

        #region Tag Memory Page
        private void TagAttrTabResetVal()
        {
            // TID (Vendor/Tag-Specifc area) Memory
            UserPref Pref = UserPref.GetInstance();
            TagTIDChkBx.Checked = (Pref.TIDOptMemSz > 0);
            if (TagTIDChkBx.Checked)
                TIDMemSzUD.Value = TIDMemSzUD.Minimum + Pref.TIDOptMemSz;

            // User Memory
            TagUserMemChkBx.Checked = (Pref.UsrOptMemSz > 0);
            if (TagUserMemChkBx.Checked)
                UserMemSzUD.Value = Pref.UsrOptMemSz;

            ClrTagVndrCmbBx();
        }

        private bool TagAttrIsModified()
        {
            bool Modified = false;
            UserPref Pref = UserPref.GetInstance();

            Modified = (TagTIDChkBx.Checked != (Pref.TIDOptMemSz > 0))
                    || (TagUserMemChkBx.Checked != (Pref.UsrOptMemSz > 0));

            if (!Modified) // further check
            {
                Modified = ((TagTIDChkBx.Checked == true)
                                            && (TIDMemSzUD.Value != (Pref.TIDOptMemSz + TIDMemSzUD.Minimum)))
                                    || ((TagUserMemChkBx.Checked == true)
                                            && (UserMemSzUD.Value != Pref.UsrOptMemSz));
            }

            return (Modified == true);
        }

        private void TagAttrUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = TagAttrIsModified();
        }

        private void TagAttrTabApplyChanges()
        {
            UserPref Pref = UserPref.GetInstance();

            if (TagTIDChkBx.Checked == true)
            {
                uint TIDExtrMemSz = (uint)(TIDMemSzUD.Value - TIDMemSzUD.Minimum);
                Pref.TIDOptMemSz = TIDExtrMemSz;
                EPCTag.AvailBanksAdd(MemoryBanks4Op.Two, (ushort)TIDExtrMemSz);
            }
            else
            {
                Pref.TIDOptMemSz = 0;
                EPCTag.AvailBanksSub(MemoryBanks4Op.Two);
            }
            if (TagUserMemChkBx.Checked == true)
            {
                Pref.UsrOptMemSz = (uint)UserMemSzUD.Value;
                EPCTag.AvailBanksAdd(MemoryBanks4Op.Three, (ushort)UserMemSzUD.Value);
            }
            else
            {
                Pref.UsrOptMemSz = 0;
                EPCTag.AvailBanksSub(MemoryBanks4Op.Three);
            }

            TagAttrUpdateApplyBttnEnabled();

            Program.RefreshStatusLabel(this, "Changes Saved");
        }

        private void OnTagTIDChkBxChanged(object sender, EventArgs e)
        {
            TIDMemSzUD.Enabled = TagTIDChkBx.Checked;
            TagAttrUpdateApplyBttnEnabled();
            ClrTagVndrCmbBx();
        }

        private void OnTagUserMemChkBxChanged(object sender, EventArgs e)
        {
            UserMemSzUD.Enabled = TagUserMemChkBx.Checked;
            TagAttrUpdateApplyBttnEnabled();
            ClrTagVndrCmbBx();
        }

        private void OnTIDExtraSzChanged(object sender, EventArgs e)
        {
            TagAttrUpdateApplyBttnEnabled();
            ClrTagVndrCmbBx();
            TIDBnkSzLbl.Text = (TIDMemSzUD.Value * 16).ToString();
        }

        private void OnUserMemSzChanged(object sender, EventArgs e)
        {
            TagAttrUpdateApplyBttnEnabled();
            ClrTagVndrCmbBx();
            UsrBnkSzLbl.Text = (UserMemSzUD.Value * 16).ToString();
        }

        private void OnTIDExtrSzEnabled(object sender, EventArgs e)
        {
            if (TIDMemSzUD.Enabled == false)
                TIDBnkSzLbl.Text = (TIDMemSzUD.Minimum * 16).ToString();
            else
                TIDBnkSzLbl.Text = (TIDMemSzUD.Value * 16).ToString();
        }

        private void OnUserMemSzEnabled(object sender, EventArgs e)
        {
            if (UserMemSzUD.Enabled == false)
                UsrBnkSzLbl.Text = (UserMemSzUD.Minimum * 16).ToString();
            else
                UsrBnkSzLbl.Text = (UserMemSzUD.Value * 16).ToString();
        }

        private void CheckResetTagVndrSelection()
        {
            if (TagVndrCmbBx.SelectedIndex >= 0)
            {
                TagVendor Vendor = (TagVendor)TagVndrCmbBx.SelectedIndex;
                MemBnkSz BnkSz;
                if (EPCTag.TagVendorMemBnkSz(Vendor, out BnkSz))
                {
                    if (BnkSz.Bnk2 != (uint)TIDMemSzUD.Value)
                        ResetTagVndrCmbBx(); // de-select
                    else if (BnkSz.Bnk3 != (uint)UserMemSzUD.Value)
                        ResetTagVndrCmbBx(); // de-select
                }
            }
        }

        private void ResetTagVndrCmbBx()
        {
            TagVndrCmbBx.SelectedIndex = (int)TagVendor.Any;
        }

        private void ClrTagVndrCmbBx()
        {
            TagVndrCmbBx.SelectedIndex = -1;
            TagVndrCmbBx.Text = String.Empty;
        }

        private void TagAttrEvtHandlerDisableAll()
        {
            TIDMemSzUD.ValueChanged -= OnTIDExtraSzChanged;
            TagTIDChkBx.CheckStateChanged -= OnTagTIDChkBxChanged;
            UserMemSzUD.ValueChanged -= OnUserMemSzChanged;
            TagUserMemChkBx.CheckStateChanged -= OnTagUserMemChkBxChanged;
        }

        private void TagAttrEvtHandlerEnableAll()
        {
            TIDMemSzUD.ValueChanged += OnTIDExtraSzChanged;
            TagTIDChkBx.CheckStateChanged += OnTagTIDChkBxChanged;
            UserMemSzUD.ValueChanged += OnUserMemSzChanged;
            TagUserMemChkBx.CheckStateChanged += OnTagUserMemChkBxChanged;
        }

        private void OnTagVndrChanged(object sender, EventArgs e)
        {
            if (TagVndrCmbBx.SelectedIndex >= 0)
            {
                TagVendor Vendor = (TagVendor)TagVndrCmbBx.SelectedIndex;
                MemBnkSz BnkSz;
                if (EPCTag.TagVendorMemBnkSz(Vendor, out BnkSz))
                {
                    TagAttrEvtHandlerDisableAll();
                    TagTIDChkBx.Checked = (BnkSz.Bnk2 > TIDMemSzUD.Minimum);
                    TIDMemSzUD.Enabled = TagTIDChkBx.Checked;
                    TIDMemSzUD.Value = BnkSz.Bnk2;
                    TagUserMemChkBx.Checked = (BnkSz.Bnk3 > 0);
                    UserMemSzUD.Enabled = TagUserMemChkBx.Checked;
                    UserMemSzUD.Value = BnkSz.Bnk3;
                    TagAttrEvtHandlerEnableAll();
                    TagAttrUpdateApplyBttnEnabled();
                }
            }
        }


        #endregion

        #region New Frequency Page
        private static int[] FreqBandNums = { 1, 2, 3, 4, 6 };
        public static CustomFreqGrp[][] FreqBndCountries;
        double[,] FreqBndRanges;
        public static double[,] FreqBndCountryRanges;
        public static double[,] FreqBndCountryDisplayRanges;
        private static void InitFreqBndCountriesArr()
        {
            FreqBndCountries = new CustomFreqGrp[FreqBandNums.Length][];
            FreqBndCountries[0] = new CustomFreqGrp[] { CustomFreqGrp.ETSI  /*, India */ };
            FreqBndCountries[1] = new CustomFreqGrp[] { CustomFreqGrp.FCC };
            FreqBndCountries[2] = new CustomFreqGrp[] { CustomFreqGrp.JPN };
            FreqBndCountries[3] = new CustomFreqGrp[] { CustomFreqGrp.CN, CustomFreqGrp.HK, /* Malaysia, Singapore */CustomFreqGrp.TW };
            FreqBndCountries[4] = new CustomFreqGrp[] { CustomFreqGrp.KR };
        }

        private void InitFreqBndRanges()
        {
            FreqBndRanges = new double[5, 2] { { 865, 868 }, { 902, 928 }, { 952, 954 }, { 919, 928 }, { 910, 914 } };
        }

        private static void InitFreqBndCountryRanges()
        {
            FreqBndCountryRanges = new double[7, 2] { { 902.75D, 927.50D }, { 920.625D, 924.375D }, 
                { 922.25D, 927.75D }, { 865.7D, 867.5D }, { 910.2D, 913.8D }, { 920.250D, 924.750D },
                { 952.2D, 953.8D } };
            FreqBndCountryDisplayRanges = new double[7, 2] { { 902, 928 }, { 920.5D, 924.5D },
                { 922, 928 }, { 865, 868 }, { 910, 914 }, { 920, 925 }, { 952, 954 } };
        }


        String[] CountryStr = { "USA(FCC)", "China", "Taiwan", "Europe(ETSI)", "Korea", "Hong Kong", "Japan", "PlaceHolder" };

        public static int FreqBandNumGetIndex(int bandNum)
        {
            int Idx = 0;
            bool Found = false;
            for (int i = 0; i < FreqBandNums.Length && !Found; i++)
            {
                if (FreqBandNums[i] == bandNum)
                {
                    Idx = i;
                    Found = true;
                }
            }
            return Found ? Idx : 0; // throw exception instead?
        }

        private int FreqCountryGetIndex(int bandNumIdx, CustomFreqGrp cntry)
        {
            int Idx = 0;
            bool Found = false;

            for (int i = 0; i < FreqBndCountries[bandNumIdx].Length && !Found; i++)
            {
                if (FreqBndCountries[bandNumIdx][i] == cntry)
                {
                    Idx = i;
                    Found = true;
                }
            }
            return Found ? Idx : 0; // throw exception instead?
        }


        #endregion

        #region FrequencyPage
        private CustomFreqGrp loadedFreqGrp;
        private int loadedFreqBnd;
        private int loadedChn;
        private bool loadedLBTEn;
        private void FreqTabResetVal(int bandNum, CustomFreqGrp freqGrp, int chn, bool enLBT)
        {
            FreqBndNumLbl.Text = bandNum.ToString();
            FreqBndCmbBx.SelectedIndex = FreqBandNumGetIndex(bandNum);
            FreqCntryCmbBx.SelectedIndex = FreqCountryGetIndex(FreqBndCmbBx.SelectedIndex, freqGrp);
            if (enLBT)
                LBTYesBttn.Checked = true;
            else
                LBTNoBttn.Checked = true;
            SingleChnChkBx.Checked = (chn >= 0);

            if (chn >= 0)
            {
                SingleChnLnkLbl.Text = "(" + chn + ") " + FreqLists[(int)freqGrp][chn].ToString("F3") + " Mhz";
                SetChosenChnIdx(chn);
            }
            loadedChn = chn;
        }
        #region Freq Tables Initialization Routines
        public static float[][] FreqLists;
        private static void InitFreqLists()
        {

            InitFCCFreqList();
            InitCNFreqList();
            InitTWFreqList();
            InitETSIFreqList();
            InitKRFreqList();
            InitHKFreqList();
            InitJPNFreqList();

            FreqLists = new float[(int)CustomFreqGrp.NumCountries][];
            FreqLists[(int)CustomFreqGrp.CN] = CNFreqList;
            FreqLists[(int)CustomFreqGrp.ETSI] = ETSIFreqList;
            FreqLists[(int)CustomFreqGrp.FCC] = FCCFreqList;
            FreqLists[(int)CustomFreqGrp.HK] = HKFreqList;
            FreqLists[(int)CustomFreqGrp.JPN] = JPNFreqList;
            FreqLists[(int)CustomFreqGrp.KR] = KRFreqList;
            FreqLists[(int)CustomFreqGrp.TW] = TWFreqList;
        }

        // Diff from hopping table, this one in ascending order
        private static float[] FCCFreqList;
        private const int FCCChnCnt = 50;
        private static void InitFCCFreqList()
        {
            FCCFreqList = new float[FCCChnCnt];
            int i = 0;
            float f = 902.75F;
            for (; i < FCCChnCnt; i++, f += 0.50F)
            {
                FCCFreqList[i] = f;
            }
        }

        private static float[] CNFreqList;
        private const int CNChnCnt = 16;
        private static void InitCNFreqList()
        {
            CNFreqList = new float[CNChnCnt];
            float f = 920.625F;
            for (int i = 0; i < CNChnCnt; i++, f += 0.250F)
                CNFreqList[i] = f;
        }

        private static float[] TWFreqList;
        private const int TWChnCnt = 12;
        private static void InitTWFreqList()
        {
            TWFreqList = new float[TWChnCnt];
            float f = 922.250F;
            for (int i = 0; i < TWChnCnt; i++, f += 0.500F)
                TWFreqList[i] = f;
        }

        private static float[] ETSIFreqList;
        private const int ETSIChnCnt = 10;
        private static void InitETSIFreqList()
        {
            ETSIFreqList = new float[ETSIChnCnt];
            // needs 'double' precision to handle addition.
            double d = 865.700D;
            for (int i = 0; i < ETSIChnCnt; i++, d += 0.200D)
                ETSIFreqList[i] = (float)d;
        }

        private static float[] KRFreqList;
        private const int KRChnCnt = 19;
        private static void InitKRFreqList()
        {
            KRFreqList = new float[KRChnCnt];
            float f = 910.20F;
            for (int i = 0; i < KRChnCnt; i++, f += 0.20F)
                KRFreqList[i] = f;
        }

        private static float[] HKFreqList;
        private const int HKChnCnt = 10;
        private static void InitHKFreqList()
        {
            HKFreqList = new float[HKChnCnt];
            float f = 920.250F;
            for (int i = 0; i < HKChnCnt; i++, f += 0.500F)
                HKFreqList[i] = f;
        }

        private static float[] JPNFreqList;
        private const int JPNChnCnt = 17;
        private static void InitJPNFreqList()
        {
            JPNFreqList = new float[JPNChnCnt];
            double d = 952.200D;
            for (int i = 0; i < JPNChnCnt; i++, d += 0.200D)
                JPNFreqList[i] = (float)d;
        }
        #endregion

        #region Channel Selection Dialog Routines
        private String[] ChnLstVHdrs = new string[2] { "Chn", "Frequency(Mhz)" };
        private int[] ChnLstVColWths = new int[2] { 25, -2 };
        /// <summary>
        /// 
        /// </summary>
        /// <param name="freqList"></param>
        /// <param name="selectedRow">IN/OUT parameter</param>
        /// <returns></returns>
        private DialogResult ShowChnSelDialog(float[] freqList, ref int selectedRow)
        {
            LstViewSelDlg Dlg = new LstViewSelDlg("Channel");
            Dlg.SetColHeaders(ChnLstVHdrs, ChnLstVColWths);
            for (int i = 0; i < freqList.Length; i++)
                Dlg.AddRow(new String[2] { i.ToString(), freqList[i].ToString("F3") });
            Dlg.InitialRow = selectedRow;
            DialogResult Res = Dlg.ShowDialog();
            if (Res == DialogResult.OK)
                selectedRow = Dlg.SelectedRow;
            Dlg.Dispose();
            return Res;
        }
        #endregion

        #region Control Event Handling Routines
        private void OnFqBndChanged(object sender, EventArgs e)
        {
            // Display Freq Range in current Band
            // Reload Countries inside ComboBox
            // Show/Hide LBT Panel
            int BndIdx = FreqBndCmbBx.SelectedIndex;
            if (FreqBndCmbBx.SelectedIndex >= 0)
            {
                BandFrqRangeLbl.Text = FreqBndRanges[BndIdx, 0].ToString("F0")
                    + " - " + FreqBndRanges[BndIdx, 1].ToString("F0") + " Mhz"; // 3 decimal places
                LoadCntryCmbBx(FreqCntryCmbBx, FreqBndCountries[BndIdx]);
                if (BndIdx == 0 || BndIdx == 2)
                {
                    LBTPanel.Enabled = true;
                    //SingleChnChkBx.Location.Y = LBTPanel.Location.Y + LBTPanel.Height;
                    //SingleChnLnkLbl.Location.Y = SingleChnChkBx.Location.Y;
                }
                else
                {
                    LBTNoBttn.Checked = true;
                    LBTPanel.Enabled = false;
                    //SingleChnChkBx.Location.Y = LBTPanel.Location.Y;
                    //SingleChnLnkLbl.Location.Y = LBTPanel.Location.Y;
                }
            }
            else
            {
                // how to handle?
            }

            FreqTabUpdateApplyBttnEnabled();
        }

        private void DeselectSingleChn()
        {
            SingleChnChkBx.Checked = false;
            ResetChosenChnIdx();
        }

        private void LoadCntryCmbBx(ComboBox cntryCmbBx, CustomFreqGrp[] countries)
        {
            cntryCmbBx.BeginUpdate();
            cntryCmbBx.Items.Clear();
            for (int i = 0; i < countries.Length; i++)
                cntryCmbBx.Items.Add(CountryStr[(int)countries[i]]);
            cntryCmbBx.SelectedIndex = 0;  // select the first one?

            cntryCmbBx.EndUpdate();
        }

        private void OnCntryCmbBxChanged(object sender, EventArgs e)
        {
            // Display Freq Range in current Band
            int BndIdx = FreqBndCmbBx.SelectedIndex;
            if (FreqCntryCmbBx.SelectedIndex >= 0)
            {
                CustomFreqGrp Cntry = FreqBndCountries[BndIdx][FreqCntryCmbBx.SelectedIndex];
                if (FreqBndCountries[BndIdx].Length > 1)
                    CntryFreqRangeLbl.Text = FreqBndCountryDisplayRanges[(int)Cntry, 0].ToString("F1")
                                + " - " + FreqBndCountryRanges[(int)Cntry, 1].ToString("F1") + " Mhz";
                else
                    CntryFreqRangeLbl.Text = String.Empty; // display only whenever necessary
            }
            else
            {
                // Clear Label
                CntryFreqRangeLbl.Text = String.Empty;
            }
            ResetChosenChnIdx();
            FreqTabUpdateApplyBttnEnabled();
        }

        private void OnSingleChnChecked(object sender, EventArgs e)
        {
            SingleChnLnkLbl.Visible = SingleChnChkBx.Checked;

            FreqTabUpdateApplyBttnEnabled();
        }


        private void OnSingleChnLnkClicked(object sender, EventArgs e)
        {
            CustomFreqGrp cntry = FreqBndCountries[FreqBndCmbBx.SelectedIndex][FreqCntryCmbBx.SelectedIndex];
            int SelectedRow = LastChosenChnIdx();
            if (ShowChnSelDialog(FreqLists[(int)cntry], ref SelectedRow) == DialogResult.OK)
            {
                SingleChnLnkLbl.Text = "(" + SelectedRow + ") "
                    + FreqLists[(int)cntry][SelectedRow].ToString("F3") + " Mhz";
                SetChosenChnIdx(SelectedRow);
            }

            FreqTabUpdateApplyBttnEnabled();
        }

        private void OnLBTYesBttnClicked(object sender, EventArgs e)
        {
            FreqTabUpdateApplyBttnEnabled();
        }

        private void OnLBTNoBttnClicked(object sender, EventArgs e)
        {
            FreqTabUpdateApplyBttnEnabled();
        }

        #endregion

        #region Chosen Parameters Routines

        private bool LastChosenLBT()
        {
            return (/* LBTPanel.Enabled && */ LBTYesBttn.Checked);
        }

        private int LastChosenChnIdx()
        {
            int chnIdx = (int)SingleChnLnkLbl.Tag;
            return chnIdx;
        }

        private void SetChosenChnIdx(int chnIdx)
        {
            SingleChnLnkLbl.Tag = chnIdx;
        }

        private void ResetChosenChnIdx()
        {
            int chnIdx = -1;
            SingleChnLnkLbl.Tag = chnIdx;
            //   System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevStatForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.DevStatForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(DevStatForm));
            }

            SingleChnLnkLbl.Text = resources.GetString("SingleChnLnkLbl.Text");
        }

        #endregion

        private bool FreqTabIsModified()
        {
            int chosenChn = SingleChnChkBx.Checked ? LastChosenChnIdx() : -1;
            bool Modified = false;

            FreqBndCmbBx.SelectedIndex = FreqBandNumGetIndex(loadedFreqBnd);

            if (loadedFreqBnd != FreqBandNums[FreqBndCmbBx.SelectedIndex])
                Modified = true;
            else if (loadedFreqGrp != FreqBndCountries[FreqBndCmbBx.SelectedIndex][FreqCntryCmbBx.SelectedIndex])
                Modified = true;
            else if (LastChosenLBT() != loadedLBTEn)
                Modified = true;
            else if (SingleChnChkBx.Checked && (LastChosenChnIdx() >= 0))
            {
                if (loadedChn != LastChosenChnIdx()) //  from all-channel/single-channel to single-channel/different channel
                    Modified = true;
            }
            else
            {
                if (loadedChn > 0)  // from single-channel to all-channels
                    Modified = true;
            }

            return Modified;
        }

        private void FreqTabUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = FreqTabIsModified();
        }

        private void FrqGroupGetNotify(bool Succ, CustomFreqGrp freqGrp, int chn, bool enLBT)
        {
            loadedFreqGrp = freqGrp;
            loadedChn = chn;
            loadedLBTEn = enLBT;
            if (loadedFreqBnd < 0)
            {
                // deduce from freqGrp
                bool Found = false;
                for (int i = 0; i < FreqBandNums.Length && !Found; i++)
                {
                    for (int j = 0; j < FreqBndCountries[i].Length && !Found; j++)
                    {
                        if (FreqBndCountries[i][j] == freqGrp)
                        {
                            Found = true;
                            loadedFreqBnd = FreqBandNums[i];
                        }
                    }
                }
            }

            FreqTabResetVal(loadedFreqBnd, loadedFreqGrp, chn, enLBT);
        }

        // validate and save
        private bool FreqTabSaveBandNum(Reader Rdr)
        {
            if (FreqBndCmbBx.SelectedIndex < 0 || FreqBndCmbBx.SelectedIndex >= FreqBandNums.Length)
            {
                MessageBox.Show("Invalid Frequency Band Number chosen", "Error Notice");
                return false;
            }
            Rdr.bandNum = (uint)FreqBandNums[FreqBndCmbBx.SelectedIndex];
            return Rdr.CustomFreqBandNumSet();
        }

        private bool FreqTabApplyChanges()
        {
            bool Succ = false;
            Reader Rdr = ReaderFactory.GetReader();
            Program.RefreshStatusLabel(this, "Applying Frequency Changes...");
            Succ = FreqTabSaveBandNum(Rdr);
            if (Succ)
            {
                CustomFreqGrp Cntry = FreqBndCountries[FreqBndCmbBx.SelectedIndex][FreqCntryCmbBx.SelectedIndex];
                int SingleChn = (SingleChnChkBx.Checked && (LastChosenChnIdx() >= 0)) ? LastChosenChnIdx() : -1;
                bool LBTEn = LastChosenLBT();
                Succ = SingleChn >= 0 ? Rdr.CustomFreqSet(Cntry, SingleChn, LBTEn, FrqChangesApplied)
                    : Rdr.CustomFreqSet(Cntry, LBTEn, FrqChangesApplied);
            }
            if (!Succ)
            {
                MessageBox.Show("Apply Frequency Changes Failed", "Error");
            }
            return Succ;
        }

        private void FrqChangesApplied(bool Succ, String errMsg)
        {
            if (Succ)
            {
                Program.RefreshStatusLabel(this, "Applied");
                loadedFreqBnd = FreqBandNums[FreqBndCmbBx.SelectedIndex];
                loadedFreqGrp = FreqBndCountries[FreqBndCmbBx.SelectedIndex][FreqCntryCmbBx.SelectedIndex];
                loadedChn = SingleChnChkBx.Checked ? LastChosenChnIdx() : -1;
                loadedLBTEn = LastChosenLBT();
            }
            else
            {
                Program.RefreshStatusLabel(this, "Failed");
                // Revert to previous setting
                FreqTabResetVal(loadedFreqBnd, loadedFreqGrp, loadedChn, loadedLBTEn);
                MessageBox.Show(errMsg, "Error in Applying Frequency Changes");
            }
            this.Enabled = true;
            FreqTabUpdateApplyBttnEnabled();

            // Save as user Pref
            if (Succ)
            {
                UserPref Pref = UserPref.GetInstance();
                Pref.FreqProf = loadedFreqGrp;
                Pref.SingleFreqChn = loadedChn;
                Pref.EnFreqChnLBT = loadedLBTEn;
            }
        }

        #endregion

        #region Link Profile Page
        private UInt32 loadedLnkProfNum = 0, chosenLnkProfNum;

        private void LnkProfTabResetVal(UInt32 lnkProfNum)
        {
            switch (lnkProfNum)
            {
                case 0: LnkPrf0Button.Checked = true; break;
                case 1: LnkPrf1Button.Checked = true; break;
                case 2: LnkPrf2Button.Checked = true; break;
                case 3: LnkPrf3Button.Checked = true; break;
                case 4: LnkPrf4Button.Checked = true; break;
                case 5: LnkPrf5Button.Checked = true; break;
                default:
                    MessageBox.Show("Unexpected Link Profile #" + lnkProfNum);
                    break;
            }
        }

        private bool LnkProfTabIsModified()
        {
            return (chosenLnkProfNum != loadedLnkProfNum);
        }

        private void LnkProfTabUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = LnkProfTabIsModified();
        }

        private bool LnkProfTabApplyChanges()
        {
            bool Succ = false;
            // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            Program.RefreshStatusLabel(this, "Selecting a different Link Profile....");
            Rdr.ProfNum = chosenLnkProfNum;
            //Rdr.LnkProfNumoSetNotification+=LnkPrfNumSetNotify;
            Rdr.RegisterLnkProfNumoSetNotificationEvent(LnkPrfNumSetNotify);
            Succ = Rdr.LinkProfNumSet();
            if (!Succ)
            {
                MessageBox.Show("Select a different Link Profile Failed", "Error");
            }
            return Succ;
        }

        private void LnkPrfNumSetNotify(bool Succ, String errMsg)
        {
            if (!Succ)
            {
                Program.RefreshStatusLabel(this, "Failed to Set Link Profile");
                MessageBox.Show(errMsg, "Select Link Profile failed");
            }
            else
            {
                Program.RefreshStatusLabel(this, "Link Profile Selected");
                loadedLnkProfNum = chosenLnkProfNum;
                LnkProfTabResetVal(loadedLnkProfNum);
                LnkProfTabUpdateApplyBttnEnabled();
            }
            this.Enabled = true;

            if (Succ) // save as User Pref
            {
                UserPref.GetInstance().LinkProf = loadedLnkProfNum;
            }
        }

        private void LnkPrfDetailsLnkMove(RadioButton button)
        {
            // Move LnkPrfDetailsLnk to currently selected Profile Num
            Point newLoc = new Point();

            newLoc.X = button.Location.X + button.Width;
            newLoc.Y = button.Location.Y;
            LnkPrfDetailsLnk.Location = newLoc;
            LnkPrfDetailsLnk.Visible = true;
        }

        private void LnkPrfDetailAreaClear()
        {
            LnkPrfMdfyDetailsLnk.Visible = false;
            LnkPrfDetailHdrLbl.Visible = false;
            LnkPrfDetailLbl.Visible = false;
            LnkPrfDetailLbl.Text = "";
        }

        private void PrfButtonToNormalFnt(RadioButton button)
        {
            button.Font = new Font(button.Font.Name, button.Font.Size, FontStyle.Regular);
        }

        private void LnkPrfChkChangeHandler(RadioButton bttn, uint prfNum)
        {
            if (bttn.Checked)
            {
                chosenLnkProfNum = prfNum;
                LnkPrfDetailsLnkMove(bttn);
                LnkProfTabUpdateApplyBttnEnabled();
            }
            else
            {
                // clear area
                LnkPrfDetailAreaClear();
                PrfButtonToNormalFnt(bttn);
            }
        }

        private void OnLnkPrf5ChkChanged(object sender, EventArgs e)
        {
            LnkPrfChkChangeHandler(LnkPrf5Button, 5);
        }

        private void OnLnkPrf4ChkChanged(object sender, EventArgs e)
        {
            LnkPrfChkChangeHandler(LnkPrf4Button, 4);
        }

        private void OnLnkPrf3ChkChanged(object sender, EventArgs e)
        {
            LnkPrfChkChangeHandler(LnkPrf3Button, 3);
        }

        private void OnLnkPrf2ChkChanged(object sender, EventArgs e)
        {
            LnkPrfChkChangeHandler(LnkPrf2Button, 2);
        }

        private void OnLnkPrf1ChkChanged(object sender, EventArgs e)
        {
            LnkPrfChkChangeHandler(LnkPrf1Button, 1);
        }

        private void OnLnkPrf0ChkChanged(object sender, EventArgs e)
        {
            LnkPrfChkChangeHandler(LnkPrf0Button, 0);
        }


        private void OnLnkPrfDetailsClicked(object sender, EventArgs e)
        {
            Program.RefreshStatusLabel(this, "Requesting Link Profile details....");
            this.Enabled = false;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.ProfNum = chosenLnkProfNum;
            //Rdr.LnkProfInfoGetNotification+=LnkPrfDetailsGetNotify;
            Rdr.RegisterLnkProfInfoGetNotificationEvent(LnkPrfDetailsGetNotify);
            if (!Rdr.LinkProfInfoGet())
            {
                Program.RefreshStatusLabel(this, "Request Link Profile detail failed");
                MessageBox.Show("Fail to request Link Profile details", "Error");
                this.Enabled = true;
            }
        }

        private void LnkPrfDetailsGetNotify(bool succ, ref RFID_RADIO_LINK_PROFILE lnkPrf, String errMsg)
        {
            if (!succ)
            {
                Program.RefreshStatusLabel(this, "Failed to receive Link Profile Details");
                MessageBox.Show(errMsg, "Error getting Link Profile");
            }
            else
            {
                Program.RefreshStatusLabel(this, "Link Profile Received");
                // display lnkPrf
                LnkPrfDetailDpy(ref lnkPrf, false);
                PrfButtonToBoldFnt(chosenLnkProfNum);
            }
            this.Enabled = true;
        }

        private void LnkPrfDetailDpy(ref RFID_RADIO_LINK_PROFILE lnkPrf, bool verbose)
        {
            //  Header
            Point NewLoc = new Point(LnkPrfDetailsLnk.Location.X, LnkPrfDetailsLnk.Location.Y);
            LnkPrfDetailHdrLbl.Location = NewLoc;
            LnkPrfDetailHdrLbl.Visible = true;
            NewLoc.X = LnkPrfDetailHdrLbl.Location.X + LnkPrfDetailHdrLbl.Width;
            NewLoc.Y = LnkPrfDetailHdrLbl.Location.Y;
            LnkPrfMdfyDetailsLnk.Location = NewLoc;
            LnkPrfMdfyDetailsLnk.Visible = true;
            // Fields
            String LnkPrfStr;
            if (verbose)
            {
                LnkPrfStr = "Enabled: " + (lnkPrf.enabled != 0 ? "Y" : "N") + "\r\n"
                + "DenseReaderMode: " + (lnkPrf.denseReaderMode != 0 ? "Y" : "N") + "\r\n"
                + "Avging WbRSSI samples: " + lnkPrf.widebandRssiSamples + "\r\n"
                + "Avging NbRSSI samples: " + lnkPrf.narrowbandRssiSamples + "\r\n"
                + lnkPrf.iso18K6C.modulationType + "\r\n"
                + "Tari Duration: " + lnkPrf.iso18K6C.tari + "ns\r\n"
                + lnkPrf.iso18K6C.data01Difference + "\r\n"
                + "Pulse Width: " + lnkPrf.iso18K6C.pulseWidth + "ns\r\n"
                + "R-T Calibration: " + lnkPrf.iso18K6C.rtCalibration + "ns\r\n"
                + "T-R Calibration: " + lnkPrf.iso18K6C.trCalibration + "ns\r\n"
                + lnkPrf.iso18K6C.divideRatio + "\r\n"
                + lnkPrf.iso18K6C.millerNumber + "\r\n"
                + "T-R LinkFreq: " + lnkPrf.iso18K6C.trLinkFrequency + "Hz\r\n"
                + "Var T2 Delay: " + lnkPrf.iso18K6C.varT2Delay + "us\r\n"
                + "Rx Delay: " + lnkPrf.iso18K6C.rxDelay * 20.833 + "ns\r\n" // 48Mhz cycle = 20.833333... ns T
                + "Min T2 Delay: " + lnkPrf.iso18K6C.minT2Delay + "us\r\n"
                + "Tx Prop. Delay: " + lnkPrf.iso18K6C.txPropagationDelay + "us\r\n";
            }
            else
            {
                String ModStr = null;
                switch (lnkPrf.iso18K6C.modulationType)
                {
                    case RFID_18K6C_MODULATION_TYPE.RFID_18K6C_MODULATION_TYPE_DSB_ASK:
                        ModStr = "DSB-ASK"; break;
                    case RFID_18K6C_MODULATION_TYPE.RFID_18K6C_MODULATION_TYPE_PR_ASK:
                        ModStr = "PR-ASK"; break;
                    case RFID_18K6C_MODULATION_TYPE.RFID_18K6C_MODULATION_TYPE_SSB_ASK:
                        ModStr = "SSB-ASK"; break;
                }
                String EncStr = null;
                switch (lnkPrf.iso18K6C.millerNumber)
                {
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_FM0:
                        EncStr = "FM0"; break;
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_2:
                        EncStr = "Miller-2"; break;
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_4:
                        EncStr = "Miller-4"; break;
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_8:
                        EncStr = "Miller-8"; break;
                }
                LnkPrfStr = "In Use: " + (lnkPrf.enabled != 0 ? "Y" : "N") + "\r\n"
                    + "Tari Duration: " + lnkPrf.iso18K6C.tari / 1000D + "us\r\n"
                    + "R-T Modulation: " + ModStr + "\r\n"
                    + "T-R LinkFreq: " + lnkPrf.iso18K6C.trLinkFrequency / 1000D + "kHz\r\n"
                    + "Tag Encoding: " + EncStr + "\r\n";

            }
            LnkPrfDetailLbl.Text = LnkPrfStr;
            NewLoc.X = LnkPrfDetailHdrLbl.Location.X;
            NewLoc.Y = LnkPrfDetailHdrLbl.Location.Y + LnkPrfDetailHdrLbl.Height;
            LnkPrfDetailLbl.Location = NewLoc;
            LnkPrfDetailLbl.Visible = true;
        }

        private void PrfButtonToBoldFnt(RadioButton button)
        {
            button.Font = new Font(button.Font.Name, button.Font.Size, FontStyle.Bold);
        }

        private void PrfButtonToBoldFnt(uint profNum)
        {
            switch (profNum)
            {
                case 0: PrfButtonToBoldFnt(LnkPrf0Button); break;
                case 1: PrfButtonToBoldFnt(LnkPrf1Button); break;
                case 2: PrfButtonToBoldFnt(LnkPrf2Button); break;
                case 3: PrfButtonToBoldFnt(LnkPrf3Button); break;
                case 4: PrfButtonToBoldFnt(LnkPrf4Button); break;
                case 5: PrfButtonToBoldFnt(LnkPrf5Button); break;
                default:
                    break;
            }
        }

        #endregion

        #region Antenna Page
        private RFID_ANTENNA_PORT_CONFIG loadedPrt0Cfg;
        private RFID_ANTENNA_PORT_CONFIG chosenPrt0Cfg;
        private float TrkBarStepSz = 0.5F;

        private void AntTabResetVal(ref RFID_ANTENNA_PORT_CONFIG cfg)
        {
            //Settings View


            UserPref pref = UserPref.GetInstance();

            if (pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                PwrLvlTrkBr.Value = (int)(Convert.ToInt32(pref.AntennaPowerLevel));
            }
            else
            {
                PwrLvlTrkBr.Value = (int)(cfg.powerLevel * 0.1F / TrkBarStepSz);
            }

            DwellTimeUD.Value = (int)cfg.dwellTime;
            InvtryCycUD.Value = cfg.numberInventoryCycles;

            // Reset Chosen
            chosenPrt0Cfg = loadedPrt0Cfg;
        }

        private bool AntTabIsModified()
        {
            return !loadedPrt0Cfg.SameAs(ref chosenPrt0Cfg);
        }

        private void AntTabUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = AntTabIsModified();
        }

        private bool AntTabApplyChanges()
        {
            bool Succ = false;


            // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            Program.RefreshStatusLabel(this, "Applying antenna changes....");

            RFID_ANTENNA_PWRLEVEL lastValue = UserPref.GetInstance().AntennaPowerLevel;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                UserPref.GetInstance().AntennaPowerLevel = (RFID_ANTENNA_PWRLEVEL)PwrLvlTrkBr.Value;
            }

            Succ = Rdr.AntPortCfgSetOne(0, ref chosenPrt0Cfg, AntennaCfgSetOneNotify);
            if (!Succ)
            {
                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    UserPref.GetInstance().AntennaPowerLevel = lastValue;
                    PwrLvlTrkBr.Value = (int)lastValue;
                }
                MessageBox.Show("Set Antenna Port Configuration Failed", "Error");
            }
            else
            {
             
            }
            return Succ;
        }

        private void AntennaCfgSetOneNotify(bool succ, uint antNum, String errMsg)
        {
            if (!succ)
            {
                MessageBox.Show(errMsg, "Error in Setting Antenna Port Configuration");
            }
            else
            {
                Program.RefreshStatusLabel(this, "Antenna Port changes applied");
                loadedPrt0Cfg = chosenPrt0Cfg;
                AntTabResetVal(ref loadedPrt0Cfg);
                AntTabUpdateApplyBttnEnabled();
            }

            this.Enabled = true;

            if (succ)
            {
                // save last set power
                UserPref.GetInstance().AntennaPwr = (float)(loadedPrt0Cfg.powerLevel / 10.0F);
            }
        }

        private void AntennaCfgGetOneNotify(bool succ, uint antNum,
            ref RFID_ANTENNA_PORT_CONFIG cfg, String errMsg)
        {
            if (!succ)
            {
                MessageBox.Show(errMsg, "Error loading Antenna Status");
            }
            else
            {
                loadedPrt0Cfg = cfg;
                AntTabResetVal(ref loadedPrt0Cfg);
            }
        }

        private void OnPwrLvlValChanged(object sender, EventArgs e)
        {
            UserPref pref = UserPref.GetInstance();

            if (pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                RFID_ANTENNA_PWRLEVEL level = ((RFID_ANTENNA_PWRLEVEL)PwrLvlTrkBr.Value);
                if (level == RFID_ANTENNA_PWRLEVEL.LOW)
                {
                    chosenPrt0Cfg.powerLevel = Convert.ToUInt32(pref.LowPowerValue);
                }
                else if (level == RFID_ANTENNA_PWRLEVEL.MEDIUM)
                {
                    chosenPrt0Cfg.powerLevel = Convert.ToUInt32(pref.MediumPowerValue);
                }
                else if (level == RFID_ANTENNA_PWRLEVEL.HIGH)
                {
                    chosenPrt0Cfg.powerLevel = Convert.ToUInt32(pref.HighPowerValue);
                }
               
                // update display label
                PwrLvlValLbl.Text = level.ToString();

            }
            else
            {
                Decimal dec = (Decimal)(PwrLvlTrkBr.Value / 0.2F);
                // intermediate storage 'Decimal'  type gives better precision.
                chosenPrt0Cfg.powerLevel = (uint)dec;
                // update display label
                float FloatVal = ((float)PwrLvlTrkBr.Value) * TrkBarStepSz;
                PwrLvlValLbl.Text = FloatVal.ToString("F1");

            }

            AntTabUpdateApplyBttnEnabled();
        }

        private void OnInvtryCycUDChanged(object sender, EventArgs e)
        {
            if (InvtryCycUD.Value == 0 && DwellTimeUD.Value == 0)
            {
                MessageBox.Show("Dwell Time and Inventory Cycles could not both be 0", "Invalid Input Value");
                InvtryCycUD.Value = chosenPrt0Cfg.numberInventoryCycles; // restore old value, infinite loop?
            }
            else
                chosenPrt0Cfg.numberInventoryCycles = (uint)InvtryCycUD.Value;
            if (InvtryCycUD.Value == 0)
            {
                Point NewLoc = InvtryCycUD.Location;
                NewLoc.X += InvtryCycUD.Width;
                NoLmtLbl.Location = NewLoc;
                NoLmtLbl.Visible = true;
            }
            else if (DwellTimeUD.Value != 0)
                NoLmtLbl.Visible = false;

            AntTabUpdateApplyBttnEnabled();
        }

        private void OnDwellTimeUDChanged(object sender, EventArgs e)
        {
            if (DwellTimeUD.Value == 0 && InvtryCycUD.Value == 0)
            {
                MessageBox.Show("Dwell Time and Inventory Cycles could not both be 0", "Invalid Input Value");
                DwellTimeUD.Value = chosenPrt0Cfg.dwellTime; // restore old value, infinite loop?
            }
            else
                chosenPrt0Cfg.dwellTime = (uint)DwellTimeUD.Value;
            if (DwellTimeUD.Value == 0)
            {
                Point NewLoc = DwellTimeUD.Location;
                NewLoc.X += DwellTimeUD.Width;
                NoLmtLbl.Location = NewLoc;
                NoLmtLbl.Visible = true;
            }
            else if (InvtryCycUD.Value != 0)
                NoLmtLbl.Visible = false;

            AntTabUpdateApplyBttnEnabled();
        }

        #endregion

        #region SELECT Criteria Page (Read-Only, No Modification)
        private RFID_18K6C_SELECT_CRITERION defaultSelCrit = new RFID_18K6C_SELECT_CRITERION();
        // Array size fixed to number of loaded
        // criteria until next save/load
        private RFID_18K6C_SELECT_CRITERION[] loadedSelCrit = new RFID_18K6C_SELECT_CRITERION[0];
        // Array size fixed to 'max of 4', but 
        //data relevant is only according
        // to the current SelCritCntUD.Value
        private RFID_18K6C_SELECT_CRITERION[] chosenSelCrit = new RFID_18K6C_SELECT_CRITERION[0];

        private void TagSelTabResetVal(ref RFID_18K6C_SELECT_CRITERION[] criteria)
        {
            SelCritCntUD.Value = criteria.Length;

            if (chosenSelCrit.Length == 0)
                chosenSelCrit = new RFID_18K6C_SELECT_CRITERION[4];
            // Reset Each Element with critieria
            for (int i = 0; i < chosenSelCrit.Length; i++)
            {
                if (i < loadedSelCrit.Length)
                    loadedSelCrit[i].CopyTo(ref chosenSelCrit[i]);
                else
                    defaultSelCrit.CopyTo(ref chosenSelCrit[i]);
            }
            // TBD: Clear modification marks
        }

        private void TagSelCritGetNotify(bool Succ, uint numCrit,
          RFID_18K6C_SELECT_CRITERION[] criteria, String errMsg)
        {
            if (!Succ)
            {
                SelCritCntUD.Enabled = false;
                MessageBox.Show(errMsg, "Error loading SELECT criteria");
            }
            else // assuming numCrit == criteria.Length
            {
                SelCritCntUD.Value = numCrit;
                loadedSelCrit = new RFID_18K6C_SELECT_CRITERION[numCrit];
                loadedSelCrit.Initialize();
                for (int i = 0; i < numCrit; i++)
                    criteria[i].CopyTo(ref loadedSelCrit[i]); // deep copy
                TagSelTabResetVal(ref loadedSelCrit);
            }
        }



        private void MakeSelCritLnkVisible(uint n)
        {
            SelCrit0Lnk.Visible = (n > 0);
            SelCrit1Lnk.Visible = (n > 1);
            SelCrit2Lnk.Visible = (n > 2);
            SelCrit3Lnk.Visible = (n > 3);
        }

        private void OnSelCritCntValChanged(object sender, EventArgs e)
        {
            MakeSelCritLnkVisible((uint)SelCritCntUD.Value);
        }

        private void OnSelCrit3LnkClicked(object sender, EventArgs e)
        {
            OnSelCritXLnkClicked((uint)3);
        }

        private void OnSelCrit2LnkClicked(object sender, EventArgs e)
        {
            OnSelCritXLnkClicked((uint)2);
        }

        private void OnSelCrit1LnkClicked(object sender, EventArgs e)
        {
            OnSelCritXLnkClicked((uint)1);
        }

        private void OnSelCrit0LnkClicked(object sender, EventArgs e)
        {
            OnSelCritXLnkClicked((uint)0);
        }

        private void OnSelCritXLnkClicked(uint critNum)
        {
            // View and allow user to Edit the selected Criteria detail
            this.Enabled = false;
            SelectCriteriaForm SCForm;
            if (loadedSelCrit.Length > critNum) // Editing a loaded criteria
                SCForm = new SelectCriteriaForm(critNum, ref loadedSelCrit[critNum], ref chosenSelCrit[critNum]);
            else // Editing a new criteria
            {
                SCForm = new SelectCriteriaForm(critNum);
                SCForm.InitializeFields(ref chosenSelCrit[critNum]);
            }
            SCForm.Closed += new EventHandler(OnSCFormClosed);
            SCForm.CritEditedNotify = OnSCFormValueUpdated;
            SCForm.Show();
        }

        private void OnSCFormClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void OnSCFormValueUpdated(uint critNum, ref RFID_18K6C_SELECT_CRITERION criteria)
        {
            criteria.CopyTo(ref chosenSelCrit[critNum]);

            // read-only page: ApplyButton.Enabled = true;
        }

        #endregion

        #region Query Parameters Page (Read-Only. No Modification)
        // Constants
        readonly RFID_18K6C_SINGULATION_FIXEDQ_PARMS defaultFQParms
            = new RFID_18K6C_SINGULATION_FIXEDQ_PARMS(0, 0, 0, 0); // To be confirmed
        readonly RFID_18K6C_SINGULATION_DYNAMICQ_PARMS defaultDQParms
            = new RFID_18K6C_SINGULATION_DYNAMICQ_PARMS(7, 0, 15, 0, 10, 0); // To be confirmed

        private RFID_18K6C_QUERY_PARMS loadedQryParms;
        private RFID_18K6C_SINGULATION_ALGORITHM chosenQType; // Fixed or Dynamic
        private RFID_18K6C_SINGULATION_FIXEDQ_PARMS chosenFQParms;
        private RFID_18K6C_SINGULATION_DYNAMICQ_PARMS chosenDQParms;
        private RFID_18K6C_TAG_GROUP chosenTagGrp;

        private void QryPrmTabResetVal(ref RFID_18K6C_QUERY_PARMS qryParms)
        {
            switch (loadedQryParms.tagGroup.selected)
            {
                case RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ALL:
                    SLAllButton.Checked = true;
                    break;
                case RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ON:
                    SLOnButton.Checked = true;
                    break;
                case RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_OFF:
                    SLOffButton.Checked = true;
                    break;
            }
            SessCmbBx.SelectedIndex = (int)(loadedQryParms.tagGroup.session - RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0);
            SessValCmbBx.SelectedIndex = (int)(loadedQryParms.tagGroup.target - RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A);
            switch (loadedQryParms.singulationParms.singulationAlgorithm)
            {
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ:
                    FQButton.Checked = true;
                    FQSzTrkBr.Value = (int)qryParms.singulationParms.fixedQ.qValue;
                    FQRetryCntUD.Value = qryParms.singulationParms.fixedQ.retryCount;
                    RptChkBx.Checked = (qryParms.singulationParms.fixedQ.repeatUntilNoTags != 0);
                    ToggleTgtChkBx.Checked = qryParms.singulationParms.fixedQ.toggleTarget != 0;
                    // default values for DQ
                    StartQTrkBr.Value = (int)defaultDQParms.startQValue;
                    MinQTrkBr.Value = (int)defaultDQParms.minQValue;
                    MaxQTrkBr.Value = (int)defaultDQParms.maxQValue;
                    DQRetryCntUD.Value = defaultDQParms.retryCount;
                    DQMxQryCntUD.Value = defaultDQParms.maxQueryRepCount;
                    break;
                case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ:
                    DQButton.Checked = true;
                    StartQTrkBr.Value = (int)qryParms.singulationParms.dynamicQ.startQValue;
                    MinQTrkBr.Value = (int)qryParms.singulationParms.dynamicQ.minQValue;
                    MaxQTrkBr.Value = (int)qryParms.singulationParms.dynamicQ.maxQValue;
                    DQRetryCntUD.Value = qryParms.singulationParms.dynamicQ.retryCount;
                    DQMxQryCntUD.Value = qryParms.singulationParms.dynamicQ.maxQueryRepCount;
                    ToggleTgtChkBx.Checked = qryParms.singulationParms.dynamicQ.toggleTarget != 0;
                    // default values for FQ
                    FQSzTrkBr.Value = (int)defaultFQParms.qValue;
                    FQRetryCntUD.Value = defaultFQParms.retryCount;
                    RptChkBx.Checked = (defaultFQParms.repeatUntilNoTags != 0);
                    break;
            }
        }

        private void QueryParmGetNotify(bool succ, ref RFID_18K6C_QUERY_PARMS parms,
            String errMsg)
        {
            // Singulation Page
            if (!succ)
            {
                MessageBox.Show(errMsg, "Load Query Parameter Settings Error");
            }
            else
            {
                // Save Loaded Params (for Save)
                parms.Clone(out loadedQryParms);
                // Initialize chosen TagGroup
                chosenTagGrp = loadedQryParms.tagGroup; // shallow copy
                // Initialize chosenF/DQParms 
                chosenQType = loadedQryParms.singulationParms.singulationAlgorithm;
                switch (chosenQType)
                {
                    case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ:
                        chosenFQParms = loadedQryParms.singulationParms.fixedQ;
                        chosenDQParms = defaultDQParms;
                        break;
                    case RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ:
                        chosenDQParms = loadedQryParms.singulationParms.dynamicQ;
                        chosenFQParms = defaultFQParms;
                        break;
                }
                // Initialize UI Control
                QryPrmTabResetVal(ref loadedQryParms);
            }
            this.Enabled = true;
        }

        #region FQ Panel
        private void OnFQRetryCntValChanged(object sender, EventArgs e)
        {
            chosenFQParms.retryCount = (uint)FQRetryCntUD.Value;

            if (loadedQryParms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ)
                ControlMarkModified(FQRetryCntUD, chosenFQParms.retryCount != loadedQryParms.singulationParms.fixedQ.retryCount);

        }

        private void OnRptChkBxChkChanged(object sender, EventArgs e)
        {
            chosenFQParms.repeatUntilNoTags = (uint)(RptChkBx.Checked ? 1 : 0);
            if (loadedQryParms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ)
            {
                // make bool comparison instead of int val comparison (meaningless)
                ControlMarkModified(RptChkBx, ((chosenFQParms.repeatUntilNoTags != 0)
                    != (loadedQryParms.singulationParms.fixedQ.repeatUntilNoTags != 0)));
            }
        }

        private void OnFxQValChanged(object sender, EventArgs e)
        {
            chosenFQParms.qValue = (uint)FQSzTrkBr.Value;

            QSzLbl.Text = ((int)Math.Pow(2D, (double)FQSzTrkBr.Value)).ToString();

            if (loadedQryParms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ)
                ControlMarkModified(FQSzTrkBr, (chosenFQParms.qValue != loadedQryParms.singulationParms.fixedQ.qValue));
        }

        #endregion

        #region DQ Panel
        private void OnDQMxQryCntValChanged(object sender, EventArgs e)
        {
            chosenDQParms.maxQueryRepCount = (uint)DQMxQryCntUD.Value;

            if (loadedQryParms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ)
                ControlMarkModified(DQMxQryCntUD, chosenDQParms.maxQueryRepCount != loadedQryParms.singulationParms.dynamicQ.maxQueryRepCount);
        }

        private void OnDQRetryCntValChanged(object sender, EventArgs e)
        {
            chosenDQParms.retryCount = (uint)DQRetryCntUD.Value;

            if (loadedQryParms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ)
                ControlMarkModified(DQRetryCntUD, chosenDQParms.retryCount != loadedQryParms.singulationParms.dynamicQ.retryCount);
        }

        private void OnMxQValChanged(object sender, EventArgs e)
        {
            chosenDQParms.maxQValue = (uint)MaxQTrkBr.Value;

            QMxLbl.Text = ((int)Math.Pow(2D, (double)MaxQTrkBr.Value)).ToString();
            // must be >= StartQVal
            if (MaxQTrkBr.Value < StartQTrkBr.Value)
                StartQTrkBr.Value = MaxQTrkBr.Value;

            if (loadedQryParms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ)
                ControlMarkModified(MaxQTrkBr, (chosenDQParms.maxQValue != loadedQryParms.singulationParms.dynamicQ.maxQValue));
        }

        private void OnMnQValChanged(object sender, EventArgs e)
        {
            chosenDQParms.minQValue = (uint)MinQTrkBr.Value;

            QMnLbl.Text = ((int)Math.Pow(2D, (double)MinQTrkBr.Value)).ToString();
            // must be <= StartQVal
            if (MinQTrkBr.Value > StartQTrkBr.Value)
                StartQTrkBr.Value = MinQTrkBr.Value;

            if (loadedQryParms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ)
                ControlMarkModified(MinQTrkBr, (chosenDQParms.minQValue != loadedQryParms.singulationParms.dynamicQ.minQValue));
        }



        private void OnStQValChanged(object sender, EventArgs e)
        {
            chosenDQParms.startQValue = (uint)StartQTrkBr.Value;

            QStLbl.Text = ((int)Math.Pow(2D, (double)StartQTrkBr.Value)).ToString();

            // must be >= MinQVal
            if (MinQTrkBr.Value > StartQTrkBr.Value)
                MinQTrkBr.Value = StartQTrkBr.Value;
            // must be <= MaxQVal
            if (MaxQTrkBr.Value < StartQTrkBr.Value)
                MaxQTrkBr.Value = StartQTrkBr.Value;

            if (loadedQryParms.singulationParms.singulationAlgorithm
                == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ)
                ControlMarkModified(StartQTrkBr, (chosenDQParms.startQValue != loadedQryParms.singulationParms.dynamicQ.startQValue));
        }
        #endregion

        #region FQ/DQ Check Box
        private void OnDQChkChanged(object sender, EventArgs e)
        {
            if (this.DQButton.Checked)
            {
                chosenQType = RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ;
                ControlMarkModified(DQPanel, (chosenQType != loadedQryParms.singulationParms.singulationAlgorithm));
                this.DQPanel.Visible = true;
            }
            else
                this.DQPanel.Visible = false;
        }

        private void OnFQChkChanged(object sender, EventArgs e)
        {
            if (this.FQButton.Checked)
            {
                chosenQType = RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ;
                ControlMarkModified(FQPanel, chosenQType != loadedQryParms.singulationParms.singulationAlgorithm);
                this.FQPanel.Visible = true;
            }
            else
                this.FQPanel.Visible = false;
        }

        #endregion

        #region Session Flag Target
        private void OnSessValIdxChanged(object sender, EventArgs e)
        {
            chosenTagGrp.target = (RFID_18K6C_INVENTORY_SESSION_TARGET)((int)RFID_18K6C_INVENTORY_SESSION_TARGET.RFID_18K6C_INVENTORY_SESSION_TARGET_A + SessValCmbBx.SelectedIndex);
            ControlMarkModified(SessValCmbBx, chosenTagGrp.target != loadedQryParms.tagGroup.target);
        }

        private void OnSessIdxChanged(object sender, EventArgs e)
        {
            chosenTagGrp.session = (RFID_18K6C_INVENTORY_SESSION)((int)RFID_18K6C_INVENTORY_SESSION.RFID_18K6C_INVENTORY_SESSION_S0 + SessCmbBx.SelectedIndex);
            ControlMarkModified(SessCmbBx, chosenTagGrp.session != loadedQryParms.tagGroup.session);
        }

        private void OnToggleTgtChkBxChanged(object sender, EventArgs e)
        {
            chosenDQParms.toggleTarget = ToggleTgtChkBx.Checked ? 1U : 0U;
            chosenFQParms.toggleTarget = ToggleTgtChkBx.Checked ? 1U : 0U;

            if (loadedQryParms.singulationParms.singulationAlgorithm
                == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_DYNAMICQ)
                ControlMarkModified(ToggleTgtChkBx, chosenDQParms.toggleTarget != loadedQryParms.singulationParms.dynamicQ.toggleTarget);
            else
                ControlMarkModified(ToggleTgtChkBx, chosenFQParms.toggleTarget != loadedQryParms.singulationParms.fixedQ.toggleTarget);

        }

        #endregion
        #region SL Flag Target
        private void OnSLChkChanged(RadioButton button, RFID_18K6C_SELECTED selection)
        {
            if (button.Checked)
            {
                chosenTagGrp.selected = selection;
                ControlMarkModified(button, loadedQryParms.tagGroup.selected != chosenTagGrp.selected);
            }
            else
                ControlMarkModified(button, false);
        }
        private void OnALAllChkChanged(object sender, EventArgs e)
        {
            OnSLChkChanged(SLOnButton, RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ALL);
        }

        private void OnSLOffChkChanged(object sender, EventArgs e)
        {
            OnSLChkChanged(SLOffButton, RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_OFF);
        }

        private void OnSLOnChkChanged(object sender, EventArgs e)
        {
            OnSLChkChanged(SLOnButton, RFID_18K6C_SELECTED.RFID_18K6C_SELECTED_ON);
        }
        #endregion
        #endregion

        #region Overheat Protection Page
        static public EventHandler OvrHtProtTabChanged;

        private void OvrHtProtTabResetVal()
        {
            UserPref Pref = UserPref.GetInstance();

            DCOnUD.Value = Pref.DCOnDuration;
            DCOffUD.Value = Pref.DCOffDuration;

            XcvrTempUD.Value = Pref.XcvrTempLmt;
        }

        private void OvrHtProtTabApplyChanges()
        {
            Reader Rdr = ReaderFactory.GetReader();
            UserPref Pref = UserPref.GetInstance();
            Pref.DCOnDuration = (uint)DCOnUD.Value;
            Pref.DCOffDuration = (uint)DCOffUD.Value;
            if (Pref.XcvrTempLmt != (Int32)XcvrTempUD.Value)
            {
                if (Rdr.SetTempThreshold((ushort)XcvrTempUD.Value))
                    Pref.XcvrTempLmt = (Int32)XcvrTempUD.Value;
                else
                    MessageBox.Show("Setting Temp Threshold Failed.\r\nIs radio device ready?");
            }
            OvrHtProtUpdateApplyBttnEnabled();
            if (OvrHtProtTabChanged != null)
                OvrHtProtTabChanged(this, new EventArgs());
            Program.RefreshStatusLabel(this, "Changes Saved");
        }

        private bool OvrHtProtIsModified()
        {
            UserPref Pref = UserPref.GetInstance();
            return (DCOnUD.Value != Pref.DCOnDuration) || (DCOffUD.Value != Pref.DCOffDuration)
            || (XcvrTempUD.Value != Pref.XcvrTempLmt);
        }

        private void OvrHtProtUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = OvrHtProtIsModified();
        }

        private void OnDCOnChanged(object sender, EventArgs e)
        {
            if (DCOnUD.Value == 0)
            {
                if (DCOffUD.Value != 0)
                    DCOffUD.Value = 0;
                OnfrvrLbl.Visible = true;
            }
            else
            {
                if (DCOffUD.Value == 0)
                    DCOffUD.Value = DCOnUD.Value;
                OnfrvrLbl.Visible = false;
            }

            OvrHtProtUpdateApplyBttnEnabled();
        }

        private void OnDCOffChanged(object sender, EventArgs e)
        {
            if (DCOffUD.Value == 0)
            {
                if (DCOnUD.Value != 0)
                    DCOnUD.Value = 0; // means forever On
                OnfrvrLbl.Visible = true;
            }
            else
            {
                if (DCOnUD.Value == 0)
                    DCOnUD.Value = DCOffUD.Value;
                OnfrvrLbl.Visible = false;
            }

            OvrHtProtUpdateApplyBttnEnabled();
        }


        private void OnXcvrTempChanged(object sender, EventArgs e)
        {
            OvrHtProtUpdateApplyBttnEnabled();
        }

        // This handle user direct input to textarea (before validation) when
        // focus is out of the control
        private void OnXverTempUDKeyUp(object sender, KeyEventArgs e)
        {
            OvrHtProtUpdateApplyBttnEnabled();
        }

        #endregion

        #region Temperature Page
        private void RFThrshTempGetNotify(UInt16 amb, UInt16 xcvr, UInt16 pwrAmp, UInt16 paDelta)
        {
            AmbTempLbl.Text = amb.ToString();
            XcvrTempLbl.Text = xcvr.ToString();
            PATempLbl.Text = pwrAmp.ToString();
            DeltaTempLbl.Text = paDelta.ToString();

            AllSettingsLoaded(true);
        }
#if false
        private void TempTabApplyChanges()
        {

           // RFIDRdr Rdr = RFIDRdr.GetInstance();
        Reader Rdr=ReaderFactory.GetReader();

            ushort newTemp = (ushort)TempLmtUD.Value;
            if (!Rdr.SetTempThreshold(newTemp))
                throw new ApplicationException("SetTempThreshold return error!");
            TempTabCheckModified();
        }


        private void OnTempLmtChanged(object sender, EventArgs e)
        {
            TempTabCheckModified();
        }

        private void TempTabCheckModified()
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
        Reader Rdr=ReaderFactory.GetReader();
            ApplyButton.Enabled = (TempLmtUD.Value != (Decimal)Rdr.GetTempThreshold());
        }
#endif

        #endregion

        #region Common GUI Utilities
        private void ControlMarkModified(Control ctrl, bool modified)
        {
            ctrl.BackColor = modified ? Color.Yellow : ctrl.Parent.BackColor;
        }
        #endregion

        #region Form Routines
        private void AllSettingsLoaded(bool succ)
        {
            this.Enabled = true; // last loaded parameter one for now
            Program.RefreshStatusLabel(this, succ ? "Ready" : "Error occurred");

            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance(); 
            this.Text = this.Text + " - " + UserPref.CurVersionNo;

            UserPref upref = UserPref.GetInstance();

            if (upref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                PwrLvlTrkBr.Maximum = 3;
                PwrLvlTrkBr.Minimum = 1;
                TrkBarStepSz = 1f;
                this.KeyPreview = true;
                PwrLvlValLbl.Size = new Size(70, 25);

                PwrLvlValLbl.Text = upref.AntennaPowerLevel.ToString();

                txtLowValue.KeyPress += NumKeyOnly;
                txtMedValue.KeyPress += NumKeyOnly;
                txtHighValue.KeyPress += NumKeyOnly;

                txtLowValue.TextChanged += txt_TextChanged;
                txtMedValue.TextChanged += txt_TextChanged;
                txtHighValue.TextChanged += txt_TextChanged;

                txtLowValue.Text = upref.LowPowerValue.ToString();
                txtMedValue.Text = upref.MediumPowerValue.ToString();
                txtHighValue.Text = upref.HighPowerValue.ToString();

                PwrLvlTrkBr.Value = (int)upref.AntennaPowerLevel;

                if (upref.ScannerType == 1)
                {
                    rb1DScanner.Checked = true;
                    rb2DScanner.Checked = false;
                }
                else if (upref.ScannerType == 2)
                {
                    rb1DScanner.Checked = false;
                    rb2DScanner.Checked = true;
                }

                rb2DScanner.CheckedChanged += new EventHandler(rb2DScanner_CheckedChanged);
                rb1DScanner.CheckedChanged += new EventHandler(rb1DScanner_CheckedChanged);

            
            }

            if (upref.IDInputMethod == 0)
            {
                rbManual.Checked = true;

            }
            else if (upref.IDInputMethod == 1)
            {
                rbBarcode.Checked = true;
            }
            else if (upref.IDInputMethod == 2)
            {
                rbRfid.Checked = true;
            }

            if (upref.DDSelectionOption == 0)
            {
                rbDropDown.Checked = true;

            }
            else if (upref.DDSelectionOption == 1)
            {
                rbInstantSearch.Checked = true;
            }

            txtObjectListPageSize.Text = upref.SelectScreen_PageSize.ToString();
          

            Reader Rdr = ReaderFactory.GetReader();
            // Items that does not rely on Radio
            InvtrySetupTabResetVal();
            TagAttrTabResetVal();
            OvrHtProtTabResetVal();
            //Rdr.ThrshTempGetNotification += RFThrshTempGetNotify;
            Rdr.RegisterThrshTempGetNotificationEvent(RFThrshTempGetNotify);
            if (Rdr.RadioReady())
            {
                // Load Values from Rdr to Tabs
                this.Enabled = false;
                Program.RefreshStatusLabel(this, "Loading...");

                //UInt32 BandNum;
                if (Rdr.CustomFreqBandNumGet())
                {
                    loadedFreqBnd = (int)Rdr.bandNum;
                    // Some Link Profile(s) not available in certain bands

                    FreqBndNumLbl.Text = loadedFreqBnd.ToString();

                    LnkPrf1Button.Enabled = RfidSp.LinkProf1xBnds[FreqBandNumGetIndex((int)Rdr.bandNum)];
                    LnkPrf4Button.Enabled = RfidSp.LinkProf4xBnds[FreqBandNumGetIndex((int)Rdr.bandNum)];

                    // Limit the Max. Antenna Power to 29.5dBm in certain bands
                    if (Rdr.bandNum == RfidSp.AntPwrLimitingBndNum && UserPref.GetInstance().SelectedHardware != HardwareSelection.AT870Reader)
                    {
                        PwrLvlTrkBr.Maximum = (int)(RfidSp.MAX_BAND4_ANTENNA_PWR / TrkBarStepSz);
                    }
                }
                else
                {
                    loadedFreqBnd = -1;
                    MessageBox.Show("Get Frequency band number failed: " + Rdr.LastErrCode.ToString("F"), "Error");
                }
                UInt32 CurLnkPrfNum = 0;
                Rdr.ProfNum = CurLnkPrfNum;
                // Rdr.HealthCheckStatusNotification += RFThrshTempGetNotify;
                //Rdr.CustomerFreqGetNotification += AntennaCfgGetOneNotify;

                Rdr.RegisterCustomFreqGetNotificationEvent(FrqGroupGetNotify);
                Rdr.RegisterAntPotCfgGetOneNotificationEvent(AntennaCfgGetOneNotify);

                if (Rdr.LinkProfNumGet() == true)
                {
                    loadedLnkProfNum = CurLnkPrfNum;
                    LnkProfTabResetVal(loadedLnkProfNum);
                }
                else
                {
                    MessageBox.Show("Error getting current Link Profile number: " + Rdr.LastErrCode.ToString("F"));
                }
                if (!Rdr.CustomFreqGet())
                {
                    MessageBox.Show("Get Frequency setting Failed", "Error");
                    AllSettingsLoaded(false);
                }
                else if (!Rdr.AntPortCfgGetOne())
                {
                    MessageBox.Show("Get Antenna Configuration Failed", "Error");
                    AllSettingsLoaded(false);
                }
#if false
                else if (!Rdr.TagSelectCritGet(TagSelCritGetNotify))
                {
                    MessageBox.Show("Get SELECT Criteria Failed", "Error");
                    AllSettingsLoaded(false);
                }
                else if (!Rdr.QueryParmsGet(QueryParmGetNotify))
                {
                    MessageBox.Show("Get Query Parms Failed", "Error");
                    AllSettingsLoaded(false);
                }
#endif
                else if (!Rdr.GetTempThreshold())
                {
                    MessageBox.Show("Get Threshold Temp. Failed", "Error");
                    AllSettingsLoaded(false);
                }
            }
            else
            {
                MessageBox.Show("Radio is not ready yet", "Unable to load current settings");
                Program.RefreshStatusLabel(this, "Failed to load current settings");
                this.Close();
            }
        }

        void rb1DScanner_CheckedChanged(object sender, EventArgs e)
        {
            if (UserPref.GetInstance().ScannerType != 1)
                ApplyButton.Enabled = true;
            else
                ApplyButton.Enabled = false;
        }

        void rb2DScanner_CheckedChanged(object sender, EventArgs e)
        {
            if (UserPref.GetInstance().ScannerType != 2)
                ApplyButton.Enabled = true;
            else
                ApplyButton.Enabled = false;
        }

       

        private void OnFormClosed(object sender, EventArgs e)
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        #endregion

        #region F1/F4/F5 Hotkey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F1:
                    if (down && this.Enabled) // Form Enabled implies no RFID operation is running
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    break;
            }
        }
        #endregion

        private TabPage ActivateModifiedTab()
        {
            TabPage FirstModifiedTab = null;

            if (InvtrySetupIsModified())
            {
                FirstModifiedTab = InvtrySetupTab;
            }
            else if (TagAttrIsModified())
            {
                FirstModifiedTab = TagAttrTab;
            }
            else if (FreqTabIsModified())
            {
                FirstModifiedTab = FreqTab;
            }
            else if (LnkProfTabIsModified())
            {
                FirstModifiedTab = LnkPrfTab;
            }
            else if (AntTabIsModified())
            {
                FirstModifiedTab = AntTab;
            }
            else if (OvrHtProtIsModified())
            {
                FirstModifiedTab = OvrHtProtTab;
            }

            if (FirstModifiedTab != null)
                TabCtrl.SelectedIndex = TabCtrl.TabPages.IndexOf(FirstModifiedTab);

            return FirstModifiedTab;
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            // Prompt user if there are unsaved settings
            if (ActivateModifiedTab() != null)
            {
                if (Program.AskUserConfirm("There are unsaved changes.\n" + "Ignore and leave?")
                    != DialogResult.OK)
                {
                    e.Cancel = true;
                }
            }
        }

        private void label46_ParentChanged(object sender, EventArgs e)
        {

        }

        private void RampSettings_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ApplyButton.Enabled = true;
        }

        private void txtPageSize_TextChanged(object sender, EventArgs e)
        {
            ApplyButton.Enabled = true;
        }

        private void txtPageSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }

        public static void NumKeyOnly(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (e.KeyChar == (char)Keys.Back))
            {
                e.Handled = false; // accept
            }
            else
            {
                e.Handled = true;

                TextBox txt = (TextBox)sender;
                string value = txt.Text.Trim();
                if (value != "")
                {
                    try
                    {
                        value = value + e.KeyChar.ToString();
                        if (Convert.ToInt32(value) > 30)
                        {
                            e.Handled = false;
                        }
                    }
                    catch
                    {
                        e.Handled = false;
                    }
                }
                
            }
        }

        public bool ValidatePowerLevelValues(bool apply)
        {
            bool result = true;
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                try
                {
                    string value1, value2, value3;
                    int low, medium, high;

                    value1 = txtLowValue.Text;
                    value2 = txtMedValue.Text;
                    value3 = txtHighValue.Text;

                    if (value1 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtLowValue.Focus();
                        result = false;
                    }
                    else if (value2 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtMedValue.Focus();
                        result = false;
                    }
                    else if (value3 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtHighValue.Focus();
                        result = false;
                    }

                    low = Convert.ToInt32(value1);
                    medium = Convert.ToInt32(value2);
                    high = Convert.ToInt32(value3);

                    if (low > 30 || medium > 30 || high > 30)
                    {
                        MessageBox.Show("Value cannot be greater than 30.");
                        result = false;
                    }
                    else if (low > medium || low > high)
                    {
                        MessageBox.Show("Low value cannot be greater than Medium OR Hign value.");
                        txtLowValue.Focus();
                        result = false;
                    }
                    else if (medium > high)
                    {
                        MessageBox.Show("Medium value cannot be greater than Hign value.");
                        txtMedValue.Focus();
                        result = false;
                    }

                    if (apply == true && result == true)
                    {
                        UserPref pref = UserPref.GetInstance();

                        pref.LowPowerValue = low;
                        pref.MediumPowerValue = medium;
                        pref.HighPowerValue = high;

                        RFID_ANTENNA_PWRLEVEL level = ((RFID_ANTENNA_PWRLEVEL)PwrLvlTrkBr.Value);
                        if (level == RFID_ANTENNA_PWRLEVEL.LOW)
                        {
                            chosenPrt0Cfg.powerLevel = Convert.ToUInt32(pref.LowPowerValue);
                        }
                        else if (level == RFID_ANTENNA_PWRLEVEL.MEDIUM)
                        {
                            chosenPrt0Cfg.powerLevel = Convert.ToUInt32(pref.MediumPowerValue);
                        }
                        else if (level == RFID_ANTENNA_PWRLEVEL.HIGH)
                        {
                            chosenPrt0Cfg.powerLevel = Convert.ToUInt32(pref.HighPowerValue);
                        }
               
                    }

                }
                catch
                {
                }
            }
            return result;
        }

        private void txt_TextChanged(object sender, EventArgs e)
        { 

           // ApplyButton.Enabled = ValidatePowerLevelValues(false); 
            ApplyButton.Enabled = true;
        }

        private void txtMsgDisplayTimeOut_TextChanged(object sender, EventArgs e)
        {
            ApplyButton.Enabled = true;
        }

        private void txtMsgDisplayTimeOut_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }

        private void DevStatForm_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:

                    Program.On_KeyUp(sender, e);

                    break;
                default:
                    break;
            }
        }

        private void rbDropDown_CheckedChanged(object sender, EventArgs e)
        {
            ApplyButton.Enabled = true; 
        }

        private void rbManual_CheckedChanged(object sender, EventArgs e)
        {
            ApplyButton.Enabled = true; 
        }

        private void txtObjectListPageSize_TextChanged(object sender, EventArgs e)
        {
            ApplyButton.Enabled = true; 
        }

        private void txtObjectListPageSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        } 
       

    }
}