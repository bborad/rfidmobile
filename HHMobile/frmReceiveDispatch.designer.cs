﻿namespace OnRamp
{
    partial class frmReceiveDispatch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
       // private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.Windows.Forms.ColumnHeader EPCColHdr;
             
            this.btnScan = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.pnlTagScan = new System.Windows.Forms.Panel();
            //this.EPCListV = new System.Windows.Forms.ListView();
            this.epclistbox = new System.Windows.Forms.ListBox();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.pnlTagDetails = new System.Windows.Forms.Panel();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.Location = new System.Windows.Forms.ColumnHeader();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnScanBarcode = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnConfirmAll = new System.Windows.Forms.Button();
            this.lnkLocName = new System.Windows.Forms.LinkLabel();
            this.btnSelLoc = new System.Windows.Forms.Button();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.pnlTagScan.SuspendLayout();
            this.pnlTagDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            RowNumHdr.Text = global::OnRamp.ResourceFiles.TagWrForm.RowNumHdr_Text;
            RowNumHdr.Width = 35;
            // 
            // EPCColHdr
            // 
            EPCColHdr.Text = "Tag ID";
            EPCColHdr.Width = 190;
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(3, 168);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(69, 21);
            this.btnScan.TabIndex = 27;
            this.btnScan.Text = "Scan Tags";
            this.btnScan.Click += new System.EventHandler(this.ScanButton_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(3, 168);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(77, 21);
            this.btnSelectAll.TabIndex = 28;
            this.btnSelectAll.Text = "Select Page";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(82, 168);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(56, 21);
            this.btnConfirm.TabIndex = 29;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // pnlTagScan
            // 
            this.pnlTagScan.BackColor = System.Drawing.Color.White;
            this.pnlTagScan.Controls.Add(this.epclistbox);
            this.pnlTagScan.Location = new System.Drawing.Point(1, 27);
            this.pnlTagScan.Name = "pnlTagScan";
            this.pnlTagScan.Size = new System.Drawing.Size(314, 138);
            // 
            // EPCListV
            // 
            //this.EPCListV.BackColor = System.Drawing.Color.White;
            //this.EPCListV.Columns.Add(RowNumHdr);
            //this.EPCListV.Columns.Add(EPCColHdr);
            //this.EPCListV.FullRowSelect = true;
            //this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            //this.EPCListV.Location = new System.Drawing.Point(1, 1);
            //this.EPCListV.Name = "EPCListV";
            //this.EPCListV.Size = new System.Drawing.Size(313, 135);
            //this.EPCListV.TabIndex = 17;
            //this.EPCListV.View = System.Windows.Forms.View.Details;
            //
            //epclistbox
            //
            this.epclistbox.Location = new System.Drawing.Point(1, 1);
            this.epclistbox.Size = this.pnlTagScan.Size;
            // 
            // TagCntLabel
            // 
            this.TagCntLabel.BackColor = System.Drawing.Color.White;
            this.TagCntLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TagCntLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.TagCntLabel.Location = new System.Drawing.Point(269, 170);
            this.TagCntLabel.Name = "TagCntLabel";
            this.TagCntLabel.Size = new System.Drawing.Size(45, 20);
            this.TagCntLabel.Text = "0";
            this.TagCntLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlTagDetails
            // 
            this.pnlTagDetails.BackColor = System.Drawing.Color.White;
            this.pnlTagDetails.Controls.Add(this.lstAsset);
            this.pnlTagDetails.Location = new System.Drawing.Point(1, 27);
            this.pnlTagDetails.Name = "pnlTagDetails";
            this.pnlTagDetails.Size = new System.Drawing.Size(312, 138);
            // 
            // lstAsset
            // 
            this.lstAsset.BackColor = System.Drawing.Color.White;
            this.lstAsset.CheckBoxes = true;
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.columnHeader2);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.Location);
            this.lstAsset.Location = new System.Drawing.Point(1, 1);
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.Size = new System.Drawing.Size(313, 133);
            this.lstAsset.TabIndex = 52;
            this.lstAsset.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = global::OnRamp.ResourceFiles.TagWrForm.RowNumHdr_Text;
            this.columnHeader1.Width = 20;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TagID";
            this.columnHeader2.Width = 100;
             
             //AssetName
             
            this.AssetName.Text = "Item/Inventory";
            this.AssetName.Width = 100;
            // 
            // Location
            // 
            this.Location.Text = "Location";
            this.Location.Width = 100;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(213, 168);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(55, 21);
            this.btnClear.TabIndex = 30;
            this.btnClear.Text = "Clear";
            this.btnClear.Visible = false;
            this.btnClear.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnScanBarcode
            // 
            this.btnScanBarcode.Location = new System.Drawing.Point(75, 168);
            this.btnScanBarcode.Name = "btnScanBarcode";
            this.btnScanBarcode.Size = new System.Drawing.Size(91, 21);
            this.btnScanBarcode.TabIndex = 57;
            this.btnScanBarcode.Text = "Scan Barcode";
            this.btnScanBarcode.Click += new System.EventHandler(this.btnScanBarcode_Click);
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.White;
            this.lblPage.Location = new System.Drawing.Point(222, 3);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(35, 20);
            this.lblPage.Text = "Page";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbPage
            // 
            this.cmbPage.Location = new System.Drawing.Point(260, 1);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(53, 23);
            this.cmbPage.TabIndex = 62;
            this.cmbPage.Visible = false;
            // 
            // btnConfirmAll
            // 
            this.btnConfirmAll.Location = new System.Drawing.Point(139, 168);
            this.btnConfirmAll.Name = "btnConfirmAll";
            this.btnConfirmAll.Size = new System.Drawing.Size(73, 21);
            this.btnConfirmAll.TabIndex = 64;
            this.btnConfirmAll.Text = "Confirm All";
            this.btnConfirmAll.Click += new System.EventHandler(this.btnConfirmAll_Click);
            // 
            // lnkLocName
            // 
            this.lnkLocName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Underline);
            this.lnkLocName.ForeColor = System.Drawing.Color.Black;
            this.lnkLocName.Location = new System.Drawing.Point(4, 5);
            this.lnkLocName.Name = "lnkLocName";
            this.lnkLocName.Size = new System.Drawing.Size(266, 15);
            this.lnkLocName.TabIndex = 69;
            this.lnkLocName.Text = "Select Location";
            this.lnkLocName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lnkLocName.BackColor = System.Drawing.SystemColors.Control;
            // 
            // btnSelLoc
            // 
            this.btnSelLoc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnSelLoc.Location = new System.Drawing.Point(275, 4);
            this.btnSelLoc.Name = "btnSelLoc";
            this.btnSelLoc.Size = new System.Drawing.Size(40, 18);
            this.btnSelLoc.TabIndex = 70;
            this.btnSelLoc.Text = "Select";
            // 
            // frmReceiveDispatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(317, 195);
            this.Controls.Add(this.btnSelLoc);
            this.Controls.Add(this.lnkLocName);
            this.Controls.Add(this.btnConfirmAll);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.pnlTagDetails);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.pnlTagScan);
      
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnScanBarcode);
            //this.Menu = this.mainMenu1;
            this.Name = "frmReceiveDispatch";
            this.Text = "Move Items";
            this.Load += new System.EventHandler(this.frmReceiveDispatch_Load);
            this.Closed += new System.EventHandler(this.frmReceiveDispatch_Closed);
            this.pnlTagScan.ResumeLayout(false);
            this.pnlTagDetails.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.Windows.Forms.ColumnHeader EPCColHdr;
             //this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.btnScan = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.pnlTagScan = new System.Windows.Forms.Panel();
            //this.EPCListV = new System.Windows.Forms.ListView();
            this.epclistbox = new System.Windows.Forms.ListBox();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.pnlTagDetails = new System.Windows.Forms.Panel();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.Location = new System.Windows.Forms.ColumnHeader();
            this.lnkLocName = new System.Windows.Forms.LinkLabel();
            this.btnSelLoc = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnScanBarcode = new System.Windows.Forms.Button();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.btnConfirmAll = new System.Windows.Forms.Button();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.pnlTagScan.SuspendLayout();
            this.pnlTagDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            RowNumHdr.Text = "";
            RowNumHdr.Width = 35;
            // 
            // EPCColHdr
            // 
            EPCColHdr.Text = "Tag ID";
            EPCColHdr.Width = 190;
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(5, 195);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(91, 21);
            this.btnScan.TabIndex = 27;
            this.btnScan.Text = "Scan Tags";
            this.btnScan.Click += new System.EventHandler(this.ScanButton_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Location = new System.Drawing.Point(5, 195);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(91, 21);
            this.btnSelectAll.TabIndex = 28;
            this.btnSelectAll.Text = "Select Page";
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(5, 222);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(91, 21);
            this.btnConfirm.TabIndex = 29;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // pnlTagScan
            // 
            this.pnlTagScan.BackColor = System.Drawing.Color.White;
            this.pnlTagScan.Controls.Add(this.epclistbox);
            this.pnlTagScan.Location = new System.Drawing.Point(0, 52);
            this.pnlTagScan.Name = "pnlTagScan";
            this.pnlTagScan.Size = new System.Drawing.Size(233, 138);
            // 
            // EPCListV
            // 
            //this.EPCListV.BackColor = System.Drawing.Color.White;
            //this.EPCListV.Columns.Add(RowNumHdr);
            //this.EPCListV.Columns.Add(EPCColHdr);
            //this.EPCListV.FullRowSelect = true;
            //this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            //this.EPCListV.Location = new System.Drawing.Point(1, 1);
            //this.EPCListV.Name = "EPCListV";
            //this.EPCListV.Size = new System.Drawing.Size(229, 135);
            //this.EPCListV.TabIndex = 17;
            //this.EPCListV.View = System.Windows.Forms.View.Details;
            //
            //epclistbox
            //
            this.epclistbox.Location = new System.Drawing.Point(1, 1);
            this.epclistbox.Size = this.pnlTagScan.Size;
            this.Name = "epclistbox";

            // 
            // TagCntLabel
            // 
            this.TagCntLabel.BackColor = System.Drawing.Color.White;
            this.TagCntLabel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular);
            this.TagCntLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.TagCntLabel.Location = new System.Drawing.Point(99, 194);
            this.TagCntLabel.Name = "TagCntLabel";
            this.TagCntLabel.Size = new System.Drawing.Size(37, 20);
            this.TagCntLabel.Text = "0";
            this.TagCntLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlTagDetails
            // 
            this.pnlTagDetails.BackColor = System.Drawing.Color.White;
            this.pnlTagDetails.Controls.Add(this.lstAsset);
            this.pnlTagDetails.Location = new System.Drawing.Point(3, 52);
            this.pnlTagDetails.Name = "pnlTagDetails";
            this.pnlTagDetails.Size = new System.Drawing.Size(230, 138);
            // 
            // lstAsset
            // 
            this.lstAsset.BackColor = System.Drawing.Color.White;
            this.lstAsset.CheckBoxes = true;
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.columnHeader2);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.Location);
            this.lstAsset.Location = new System.Drawing.Point(1, 1);
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.Size = new System.Drawing.Size(226, 133);
            this.lstAsset.TabIndex = 52;
            this.lstAsset.View = System.Windows.Forms.View.Details;
           
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "";
            this.columnHeader1.Width = 20;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "TagID";
            this.columnHeader2.Width = 100;
            // 
            // AssetName
            // 
            this.AssetName.Text = "Item/Inventory";
            this.AssetName.Width = 100;
            // 
            // Location
            // 
            this.Location.Text = "Location";
            this.Location.Width = 100;
            // 

            // lnkLocName
            // 
            this.lnkLocName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Underline);
            this.lnkLocName.ForeColor = System.Drawing.Color.Black;
            this.lnkLocName.Location = new System.Drawing.Point(4, 1);
            this.lnkLocName.Name = "lnkLocName";
            this.lnkLocName.Size = new System.Drawing.Size(181, 14);
            this.lnkLocName.TabIndex = 69;
            this.lnkLocName.Text = "Select Location";
            this.lnkLocName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lnkLocName.BackColor = System.Drawing.SystemColors.Control;
            // 
            // btnSelLoc
            // 
            this.btnSelLoc.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnSelLoc.Location = new System.Drawing.Point(193, 1);
            this.btnSelLoc.Name = "btnSelLoc";
            this.btnSelLoc.Size = new System.Drawing.Size(40, 20);
            this.btnSelLoc.TabIndex = 70;
            this.btnSelLoc.Text = "Select";

            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(142, 195);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(91, 21);
            this.btnClear.TabIndex = 30;
            this.btnClear.Text = "Clear";
            this.btnClear.Visible = false;
            this.btnClear.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnScanBarcode
            // 
            this.btnScanBarcode.Location = new System.Drawing.Point(5, 222);
            this.btnScanBarcode.Name = "btnScanBarcode";
            this.btnScanBarcode.Size = new System.Drawing.Size(91, 21);
            this.btnScanBarcode.TabIndex = 57;
            this.btnScanBarcode.Text = "Scan Barcode";
            this.btnScanBarcode.Click += new System.EventHandler(this.btnScanBarcode_Click);
            // 
            // lblPage
            // 
            this.lblPage.BackColor = System.Drawing.Color.White;
            this.lblPage.Location = new System.Drawing.Point(3, 26);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(35, 20);
            this.lblPage.Text = "Page";
            this.lblPage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbPage
            // 
            this.cmbPage.Location = new System.Drawing.Point(41, 24);
            this.cmbPage.Name = "cmbPage";
            this.cmbPage.Size = new System.Drawing.Size(53, 23);
            this.cmbPage.TabIndex = 62;
            this.cmbPage.Visible = false;
            // 
            // btnConfirmAll
            // 
            this.btnConfirmAll.Location = new System.Drawing.Point(142, 222);
            this.btnConfirmAll.Name = "btnConfirmAll";
            this.btnConfirmAll.Size = new System.Drawing.Size(91, 21);
            this.btnConfirmAll.TabIndex = 64;
            this.btnConfirmAll.Text = "Confirm All";
            this.btnConfirmAll.Click += new System.EventHandler(this.btnConfirmAll_Click);
            // 
            // frmReceiveDispatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.btnConfirmAll);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.cmbPage);
            this.Controls.Add(this.btnSelLoc);
            this.Controls.Add(this.lnkLocName);
            this.Controls.Add(this.pnlTagDetails);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnSelectAll);
            this.Controls.Add(this.btnScan);
            this.Controls.Add(this.pnlTagScan);
            this.Controls.Add(this.TagCntLabel);
            //this.Controls.Add(this.EPCListV);
            //this.Controls.Add(this.epclistbox);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnScanBarcode);
             //this.Menu = this.mainMenu1;
            this.Name = "frmReceiveDispatch";
            this.Text = "Move Items";
            this.Load += new System.EventHandler(this.frmReceiveDispatch_Load);
            this.Closed += new System.EventHandler(this.frmReceiveDispatch_Closed);
            this.pnlTagScan.ResumeLayout(false);
            this.pnlTagDetails.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Panel pnlTagScan;
        //private System.Windows.Forms.ListView EPCListV;
        private System.Windows.Forms.Label TagCntLabel;
        private System.Windows.Forms.Panel pnlTagDetails;
        private System.Windows.Forms.ListView lstAsset;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader AssetName;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ColumnHeader Location;
        private System.Windows.Forms.Button btnScanBarcode;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.ComboBox cmbPage;
        private System.Windows.Forms.Button btnConfirmAll;
        private System.Windows.Forms.LinkLabel lnkLocName;
        private System.Windows.Forms.Button btnSelLoc;
        private System.Windows.Forms.ListBox epclistbox;
    }
}