using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace OnRamp
{
    public partial class HttpFileTransferForm : Form
    {
        const String AppName = "OnRamp Reader";
        const String AppVer = "Beta";
        readonly String Platform;
        readonly String DotNetVer;
        readonly string UsrAgentName;
        private Exception lastExceptionInfo;

        public HttpFileTransferForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            Platform = Environment.OSVersion.ToString();
            DotNetVer = ".NET CLR " + Environment.Version.ToString();
            UsrAgentName = AppName + "/" + AppVer + " (" + Platform + "; " + DotNetVer + ")";

        }

        private void ShowInformation(String msg)
        {
            ShowInformation(msg, Color.Black);
        }

        private void ShowInformation(XfrType type, Uri remoteUri, String localDir, String localFile)
        {
            StringBuilder Sb = new StringBuilder();
            Sb = Sb.Append(type.ToString("F") + ": \n");
            DirectoryInfo DInfo = new DirectoryInfo(localDir);
            switch (type)
            {
                case XfrType.Download:
                    Sb = Sb.Append (remoteUri.Host);
                    Sb = Sb.Append (" -> ");
                    Sb = Sb.Append (DInfo.Name + Path.DirectorySeparatorChar);
                    Sb = Sb.Append(localFile);
                    break;
                case XfrType.Upload:
                    Sb = Sb.Append (DInfo.Name + Path.DirectorySeparatorChar);
                    Sb = Sb.Append(localFile);
                    Sb = Sb.Append (" -> ");
                    Sb = Sb.Append (remoteUri.Host);
                    break;
            }

            ShowInformation(Sb.ToString());
            
        }

        private void ShowInformation(String msg, Color textColor)
        {
            InfoLbl.Text = msg == null ? String.Empty : msg;
            InfoLbl.ForeColor = textColor;
        }

        public enum XfrType
        {
            Download,
            Upload
        }

        private enum XferState
        {
            Undefined,
            Initialized, // From, To parameters initialized
            Connecting,
            Requesting,
            Responding,
            Completed,
            Unauthorized, // received 401
            HeadAuthorized, 
        }
        XferState state = XferState.Undefined;


        XfrType xfrType;
        Uri remoteUri;
        String localDir;
        String localFile;
        CredentialCache credCache;

        // Setting uname to Empty string removes the stored credential
        const String httpAuthType = "Basic";
        private void SetBasicCredential(Uri reqUri, String uname, String passwd)
        {
            if (credCache == null)
                credCache = new CredentialCache();

            // multiple-add gives exception, but multiple-remove is ok
            credCache.Remove(reqUri, httpAuthType);
            if (String.IsNullOrEmpty(uname) == false)
            {
                credCache.Add(reqUri, httpAuthType, new NetworkCredential(uname, passwd));
            }
        }

        // Setting uriUserName to Empty string removes the stored credential
        private void SetBasicCredential(Uri reqUri, String uriUserInfo)
        {
            String uname, passwd;
            URIComposeForm.ParseUriUserInfoStr(uriUserInfo, out uname, out passwd);
            SetBasicCredential(reqUri, uname, passwd);
        }

        public bool SetXFrParams (XfrType type, Object from, Object to)
        {
            bool Succ = false;
            xfrType = type;
            String LocalPath;

            switch (type)
            {
                case XfrType.Download:
                    if ((from is Uri) && (to is String))
                    {
                        remoteUri = (Uri)from;
                        SetBasicCredential(remoteUri, remoteUri.UserInfo);
                        LocalPath = (String)to;
                        localDir = Path.GetDirectoryName(LocalPath);
                        localFile = Path.GetFileName(LocalPath);
                        ShowInformation(type, remoteUri, localDir, localFile);
                        Succ = true;
                        state = XferState.Initialized;
                    }
                    else
                    {
                        ShowInformation( "Invalid Uri/File Parameters", Color.Red);
                        Succ = false;
                    }
                    break;
                case XfrType.Upload:
                    if ((from is String) && (to is Uri))
                    {
                        LocalPath = (String)from;
                        localDir = Path.GetDirectoryName(LocalPath);
                        localFile = Path.GetFileName(LocalPath);
                        remoteUri = (Uri)to;
                        SetBasicCredential(remoteUri, remoteUri.UserInfo);
                        ShowInformation(type, remoteUri, localDir, localFile);
                        Succ = true;
                        state = XferState.Initialized;
                    }
                    else
                    {
                        ShowInformation( "Invalid Uri/File Parameters", Color.Red );
                        Succ = false;
                    }
                    break;
            }
            return Succ;
        }

        private void SetupCommonReqHeaders(HttpWebRequest req)
        {
            req.Accept = @"*/*" /*"text/*"*/;
            req.AllowAutoRedirect = true;
            // req.MaximumAutomaticRedirections; use default (50)
            req.KeepAlive = false;
            req.Pipelined = false; // no need for this fancy stuff
            req.UserAgent = UsrAgentName;
            // Do not make If-Modified-Since (no caching)
            req.Credentials = credCache;
            req.PreAuthenticate = true; // whenever possible (side-effects?)
        }

        private void SetupHeadReqHeaders(HttpWebRequest req)
        {
            SetupCommonReqHeaders(req);
            req.Method = "HEAD";
        }

        private void SetupDownloadReqHeaders(HttpWebRequest req)
        {
            SetupCommonReqHeaders(req);
            req.Method = "GET";
            // req.ContentLength = -1; // works with 'GET'
            
        }

        private void SetupUploadReqHeaders(HttpWebRequest req, int contentLen)
        {
            SetupCommonReqHeaders(req);
            req.Method = "PUT";
            req.ContentType = "application/octet-stream"; // binary stream
            req.ContentLength = contentLen;
            req.SendChunked = true; // if not set, ProtocolViolationException
        }

        #region File Routines
        private FileStream CreateDownloadDest(String localFilePath)
        {
            FileStream FStrm = null;

            try
            {
                FStrm = new FileStream(localFilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
            }
            catch (Exception e)
            {
                lastExceptionInfo = new Exception(e.Message, e.GetBaseException());
            }

            return FStrm;
        }

        private int GetFileSize(String filePath)
        {
            int FileSz = -1;

            try
            {
                FileSz = (int)new FileInfo(filePath).Length;
            }
            catch (Exception ex)
            {
                ShowFatalError("Unable to get input file size information",
                    ex.GetBaseException().GetType().Name,
                    ex.Message);
                FileSz = -1; // mark as failure
            }
            return FileSz;
        }

        private FileStream OpenUpLoadFile(String localFilePath)
        {
            FileStream FStrm = null;

            try
            {
                FStrm = new FileStream(localFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
            }
            catch (Exception e)
            {
                lastExceptionInfo = new Exception(e.Message, e.GetBaseException());
            }

            return FStrm;
        }
        #endregion

        #region Shared variables between Form thread and network Async threads
        private String ntwkStatusMsg = String.Empty;
        private Color ntwrkStatusForeColor = Color.Black;
        private String ntwkDetailMsg = String.Empty;
        private Color ntwkDetailForeColor = Color.Black;

        private int totXfrSz;
        private int curXfrSz;

        private XferState stateChgReq = XferState.Undefined;
        #endregion

        // These calls are designed to be invoked through Control.Invoke
        private void FormDisplayProgress(object sender, EventArgs e)
        {
            if (totXfrSz > 0)
            {
                ShowStatus("Transferring: " + curXfrSz + "/" + totXfrSz + "bytes");
                XfrPrgBr.Value = (curXfrSz * 100) / totXfrSz;
            }
            else
                XfrPrgBr.Value = curXfrSz;
        }

        private void FormDisplayStatus(object sender, EventArgs e)
        {
            // Check before setting to reduce screen flickers
            if (ntwkStatusMsg.Equals(StatusLbl.Text) == false)
            {
                StatusLbl.Text = ntwkStatusMsg;
                StatusLbl.ForeColor = ntwrkStatusForeColor;
            }
            if (String.IsNullOrEmpty(ntwkDetailMsg))
            {
                DetailsLbl.Visible = false;
                DetailsLbl.Text = String.Empty; // so that it would become visible again
            }
            else if (ntwkDetailMsg.Equals(DetailsLbl.Text) == false)
            {
                DetailsLbl.Text = ntwkDetailMsg;
                DetailsLbl.ForeColor = ntwkDetailForeColor;
                DetailsLbl.Visible = true;
            }
        }

        private void FormSetXferStatus(object sender, EventArgs e)
        {
            if (stateChgReq != XferState.Undefined)
                state = stateChgReq;

            //System.ComponentModel.ComponentResourceManager resources = 
            
            //    new System.ComponentModel.ComponentResourceManager(typeof(HttpFileTransferForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.HttpFileTransferForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(HttpFileTransferForm));
            }

            switch (state)
            {
                case XferState.Initialized:
                case XferState.Completed:
                    StartBttn.Enabled = true;
                    resources.ApplyResources(StartBttn, "StartBttn");
                    break;
                case XferState.Connecting:
                    StartBttn.Enabled = false;
                    break;
                case XferState.Requesting:
                    StartBttn.Enabled = true;
                    StartBttn.Text = "Cancel";
                    break;
                case XferState.Responding:
                    StartBttn.Enabled = true;
                    StartBttn.Text = "Stop";
                    break;
                case XferState.Unauthorized:
                    StartBttn.Enabled = false;
                    // start timer (this call might be Invoke from another thread)
                    CredPromptTmr.Enabled = true; 
                    break;
                case XferState.HeadAuthorized:
                    StartBttn.Enabled = false;
                    // start timer (this call might be Invoke from another thread)
                    UploadXferStartTmr.Enabled = true;
                    break;
            }
        }

        // Unrecoverable Error that causes the operation to stop
        private void ShowFatalError(String briefMsg, params String[] details)
        {
            ntwkStatusMsg = briefMsg;
            ntwrkStatusForeColor = Color.Red;
            StringBuilder Sb = new StringBuilder();
            if (details != null && details.Length > 0)
            {
                for (int i = 0; i < details.Length; i++)
                {
                    if (i > 0)
                        Sb = Sb.Append("\n");
                    Sb = Sb.Append(details[i]);
                    ntwkDetailMsg = Sb.ToString();
                    ntwkDetailForeColor = Color.Red;
                }
            }
            else
                ntwkDetailMsg = String.Empty;

            if (this.InvokeRequired)
                this.Invoke(new EventHandler(FormDisplayStatus));
            else
            {
                //MessageBox.Show("PInvoke not required as expected", "Warning");
                FormDisplayStatus(this, EventArgs.Empty);
            }
        }
        
        private void ShowStatus(String statusMsg, params String[] details)
        {
            ntwkStatusMsg = statusMsg;
            ntwrkStatusForeColor = Color.Black;
            StringBuilder Sb = new StringBuilder();
            if (details != null && details.Length > 0)
            {
                for (int i = 0; i < details.Length; i++)
                {
                    if (i > 0)
                        Sb = Sb.Append("\n");
                    Sb = Sb.Append(details[i]);
                }
                ntwkDetailMsg = Sb.ToString();
                ntwkDetailForeColor = Color.Black;
            }
            else
                ntwkDetailMsg = String.Empty;

            if (this.InvokeRequired)
                this.Invoke(new EventHandler(FormDisplayStatus));
            else
            {
                //MessageBox.Show("PInvoke not required as expected", "Warning");
                FormDisplayStatus(this, EventArgs.Empty);
            }
        }

        private void ShowProgress(int cur, int tot)
        {
            curXfrSz = cur;
            totXfrSz = tot;
            if (this.InvokeRequired)
                this.Invoke(new EventHandler(FormDisplayProgress));
            else
                FormDisplayProgress(this, EventArgs.Empty);
        }

        private void ChangeXferState(XferState newState)
        {
            stateChgReq = newState;
            if (this.InvokeRequired)
                this.Invoke(new EventHandler(FormSetXferStatus));
            else
                FormSetXferStatus(this, EventArgs.Empty);
        }

        private void HeadRespAsyncCallback(IAsyncResult result)
        {
            HttpWebRequest req = (HttpWebRequest)result.AsyncState;
            HttpWebResponse Resp = null;
            Stream InStrm = null;
            try
            {
                ChangeXferState(XferState.Requesting);
                Resp = (HttpWebResponse)req.EndGetResponse(result);
                ChangeXferState(XferState.Responding);
                InStrm = Resp.GetResponseStream();
                const int RdBufSz = 1024;
                byte[] InBuf = new Byte[RdBufSz];
                while (InStrm.Read(InBuf, 0, RdBufSz) > 0) ; // read till end
            }
             catch (WebException we)
            {
                switch (we.Status)
                {
                    case WebExceptionStatus.Success:
                        ShowFatalError("HEAD request Error getting response.", "Unexpected status == Success");
                        break;
                    case WebExceptionStatus.RequestCanceled:
                        ShowStatus("HEAD request Cancelled by user");
                        break;
                    case WebExceptionStatus.ProtocolError:
                        Resp = (HttpWebResponse)we.Response;
                        ShowFatalError("HEAD request Error getting response.",
                               "Server response error code: " + Resp.StatusCode.ToString(),
                                "Description: " + Resp.StatusDescription);
                        break;
                    default:
                        ShowFatalError("HEAD request Network Error: " + we.Status.ToString("F"));
                        break;
                }
            }
            catch (Exception e)
            {
                ShowFatalError("HEAD request Error getting response.", "Unexpected Exception caught. "
                    + e.GetBaseException().GetType().Name, e.Message);
            }
            finally
            {
                if (InStrm != null)
                    InStrm.Close();
                if (Resp != null)
                {
                    if (Resp.StatusCode == HttpStatusCode.Unauthorized)
                        ChangeXferState(XferState.Unauthorized); // still wrong
                    else
                        ChangeXferState(XferState.HeadAuthorized); // to trigger what was before
                    Resp.Close();
                }
                else // no Resp, that is mostly likely not even connected to server yet
                {
                    ChangeXferState(XferState.Initialized);
                }
            }
        }

        // to be used by GET(Download) operation only
        private void GetRespAsyncCallback(IAsyncResult result)
        {
            HttpWebResponse Resp = null;
             FileStream OutStrm = null;
             Stream InStrm = null;
            HttpWebRequest req = (HttpWebRequest)result.AsyncState;
            try
            {
                ChangeXferState(XferState.Requesting);
                Resp = (HttpWebResponse)req.EndGetResponse(result);
                ChangeXferState(XferState.Responding);
                InStrm = Resp.GetResponseStream();
                ShowStatus(Resp.StatusCode.ToString() + "; " + "CtntLen= " + Resp.ContentLength + "; CtntType=" + Resp.ContentType + "; " + Resp.ContentEncoding);
                const int RdBufSz = 1024;
                byte[] InBuf = new Byte[RdBufSz];
                int TotBytesRd = 0;
                int BytesRd = 0;
                OutStrm = CreateDownloadDest(localDir + Path.DirectorySeparatorChar + localFile);
                if (OutStrm == null)
                {
                    ShowFatalError("Unable to access/create local file",
                        lastExceptionInfo != null ?
                            lastExceptionInfo.InnerException.GetType().Name + "\n" + lastExceptionInfo.Message
                            : "Unknown Reason");
                }
                else
                {
                    while ((BytesRd = InStrm.Read(InBuf, 0, RdBufSz)) > 0)
                    {
                        TotBytesRd += BytesRd;
                        ShowProgress(TotBytesRd, (int)Resp.ContentLength);
                        OutStrm.Write(InBuf, 0, BytesRd);
                    }
                }
                if (Resp.ContentLength > TotBytesRd)
                    ShowStatus("Finished: File size received = " + TotBytesRd + " Bytes",
                        "Warning: actual bytes read is less than stated in content-length header");
                else
                    ShowStatus("Finished: File size received = " + TotBytesRd + "Bytes");
            }
            catch (WebException we)
            {
                switch (we.Status)
                {
                    case WebExceptionStatus.Success:
                        ShowFatalError("Error getting response.", "Unexpected status == Success");
                        break;
                    case WebExceptionStatus.RequestCanceled:
                        ShowStatus("Cancelled by user");
                        break;
                    case WebExceptionStatus.ProtocolError:
                        Resp = (HttpWebResponse)we.Response;
                        ShowFatalError("Error getting response.",
                               "Server response error code: " + Resp.StatusCode.ToString(),
                                "Description: " + Resp.StatusDescription);
                        break;
                    default:
                        ShowFatalError("Network Error: " + we.Status.ToString("F"));
                        break;
                }
            }
            catch (Exception e)
            {
                ShowFatalError("Error getting response.", "Unexpected Exception caught. "
                    + e.GetBaseException().GetType().Name, e.Message);
            }
            finally
            {
                if (InStrm != null)
                    InStrm.Close();
                if (Resp != null)
                {
                    if (Resp.StatusCode == HttpStatusCode.Unauthorized)
                        ChangeXferState(XferState.Unauthorized);
                    else
                        ChangeXferState(XferState.Completed); // TBD
                    Resp.Close();
                }
                else // no Resp, that is mostly likely not even connected to server yet
                {
                    ChangeXferState(XferState.Initialized);
                }
                if (OutStrm != null)
                    OutStrm.Close();
            }
        }

        private void SendFile (FileStream fStrm, Stream outStrm, out int bytesSent)
        {
            bytesSent = 0;
            try
            {
                const int BufSz = 1024;
                Byte[] Buf = new Byte[BufSz];
                int TotBytesToSend = (int)fStrm.Length;
                int BytesRd = 0;
                while ((BytesRd = fStrm.Read(Buf, 0, BufSz)) > 0)
                {
                    outStrm.Write(Buf, 0, BufSz);  
                    bytesSent += BytesRd;
                    ShowProgress(bytesSent, TotBytesToSend);
                }
            }
            catch (Exception ex)
            {
                lastExceptionInfo = ex.GetBaseException();
            }
        }

        // to be used by Upload operation only
        private void ReqAsyncCallback(IAsyncResult result)
        {
            HttpWebRequest req = (HttpWebRequest)result.AsyncState;
            FileStream InStrm = null;
            Stream OutStrm = null;
            HttpWebResponse Resp = null;
            int BytesSent = 0, TotBytesToSend = 0;
            bool FileIOErr = false;
            String FileIOErrMsg = String.Empty;
            String FileIOErrDetailMsg = String.Empty;
            try
            {
                ChangeXferState(XferState.Requesting);
                OutStrm = req.EndGetRequestStream(result);
                ChangeXferState(XferState.Responding);
                // Open Local File Stream
                InStrm = OpenUpLoadFile(localDir + Path.DirectorySeparatorChar + localFile);
                if (InStrm == null)
                {
                    FileIOErr = true;
                    FileIOErrMsg = "Unable to open local file to upload.";
                    FileIOErrDetailMsg =  lastExceptionInfo.GetType().Name + "\n" + lastExceptionInfo.Message;
                }
                else // Write To Request Stream
                {
                    TotBytesToSend = (int)InStrm.Length;
                    SendFile(InStrm, OutStrm, out BytesSent);
                    
                    InStrm.Close();
                    InStrm = null;// to mark it so that it won't be closed again in 'finally' clause
                    if (BytesSent != TotBytesToSend)
                    {
                        FileIOErr = true;
                        FileIOErrMsg = "Incomplete file upload (" + BytesSent + "/" + TotBytesToSend + ").";
                        FileIOErrDetailMsg = lastExceptionInfo.GetType().Name + "\n" + lastExceptionInfo.Message;
                        ShowStatus("Sent only : " + BytesSent + "/" + TotBytesToSend + ". Waiting for server reply...",
                            FileIOErrDetailMsg);
                    }
                    else
                        ShowStatus("Sent. Waiting for server reply...");
                }
                // Close Stream to signal server
                OutStrm.Close(); // nothing to send
                OutStrm = null; // to mark it so that it won't be closed again in 'finally' clause
                Resp = (HttpWebResponse)req.GetResponse(); // can we make synchronous call inside async-callback?
                Stream RespStrm = Resp.GetResponseStream();
                // Read until server close (necessary?)
                const int RespBufSz = 1024;
                Byte[] RespBuf = new Byte[RespBufSz];
                while (RespStrm.Read(RespBuf, 0, RespBufSz) > 0) ;
                // Close Stream
                RespStrm.Close();
            }
            catch (IOException ioe)
            {
                ShowFatalError("Could not complete request. ", "I/O Exception", ioe.Message);
            }
            catch (WebException we)
            {
                switch (we.Status)
                {
                    case WebExceptionStatus.Success:
                        ShowFatalError("Could not complete request.", "Unexpected status == Success");
                        break;
                    case WebExceptionStatus.RequestCanceled:
                        ShowStatus("Cancelled by user");
                        break;
                    case WebExceptionStatus.ProtocolError:
                        Resp = (HttpWebResponse)we.Response;
                        ShowFatalError("Could not complete request.",
                            "Server response error code: " + Resp.StatusCode.ToString(),
                            "Description: " + Resp.StatusDescription);
                        break;
                    default:
                        ShowFatalError("Network Error: " + we.Status.ToString("F"));
                        break;
                }
            }
            catch (InvalidOperationException ioe)
            {
                ShowFatalError("GetResponse failed", ioe.GetType().Name, ioe.Message);
            }
            catch (Exception e)
            {
                ShowFatalError("Could not complete request. ", "Unexpected Exception caught. "
                    + e.GetBaseException().GetType().Name, e.Message);
            }
            finally
            {
                if (InStrm != null)
                    InStrm.Close();
                if (OutStrm != null)
                    OutStrm.Close();
                if (Resp != null)
                {
                    switch (Resp.StatusCode)
                    {
                        case HttpStatusCode.Unauthorized:// Related Fatal Error message already shown in the 'catch block`
                            ChangeXferState(XferState.Unauthorized);
                            break;
                        case HttpStatusCode.Created:
                        case HttpStatusCode.OK:
                        case HttpStatusCode.NoContent:
                            ShowStatus("Finished: " + Resp.StatusCode, "File size uploaded = " + BytesSent + "/" + TotBytesToSend);
                            ChangeXferState(XferState.Completed);
                            break;
                        default: // Related Fatal Error message already shown in the 'catch block`
                            ChangeXferState(XferState.Initialized);
                            break;
                    }
                    Resp.Close();
                }
                else // Not evening getting WebResponse 
                {
                    if (FileIOErr == true)
                    {
                        // Overwrite previous status with File-related error, a more probable root cause
                        ShowFatalError(FileIOErrMsg, FileIOErrDetailMsg);
                    }
                    ChangeXferState(XferState.Initialized);
                }
            }
        }

        private bool MakeGetRequest(HttpWebRequest req)
        {
            bool Succ = false;
            try
            {
                IAsyncResult result = req.BeginGetResponse(GetRespAsyncCallback, req);
                Succ = true;
            }
            catch (ProtocolViolationException pve)
            {
                ShowFatalError("Make GET Request failed", pve.Message);
                Succ = false;
            }
            catch (Exception e)
            {
                ShowFatalError ("Make GET Request failed",  "Unexpected Exception caught. " 
                    + e.GetBaseException().GetType().Name, e.Message);
                Succ = false;
            }

            return Succ;
        }

        private bool MakePutRequest(HttpWebRequest req)
        {
            bool Succ;
             try
            {
                IAsyncResult result = req.BeginGetRequestStream(ReqAsyncCallback, req);
                Succ = true;
            }
            catch (ProtocolViolationException pve)
            {
                ShowFatalError("Make PUT Request failed", pve.Message);
                Succ = false;
            }
            catch (ObjectDisposedException ode)
            {
                ShowFatalError("Make PUT Request failed",  "Trying to PUT content of length 0",
                    ode.Message);
                Succ = false;
            }
            catch (Exception e)
            {
                ShowFatalError("Make PUT Request failed", "Unexpected Exception caught. "
                    + e.GetBaseException().GetType().Name, e.Message);
                Succ = false;
            }

            return Succ;
        }

        private bool MakeHeadRequest(HttpWebRequest req)
        {
            bool Succ;
            try
            {
                IAsyncResult result = req.BeginGetResponse(HeadRespAsyncCallback, req);
                Succ = true;
            }
            catch (ProtocolViolationException pve)
            {
                ShowFatalError("Make HEAD Request failed", pve.Message);
                Succ = false;
            }
            catch (Exception e)
            {
                ShowFatalError("Make HEAD Request failed", "Unexpected Exception caught. "
                    + e.GetBaseException().GetType().Name, e.Message);
                Succ = false;
            }

            return Succ;
        }
      
        
        HttpWebRequest CurXfrReq = null;
        private void UserStartXfer()
        {
            try
            {
                ChangeXferState(XferState.Connecting);
                ShowStatus("Connecting to " + remoteUri.Host + "...");
                ShowProgress(0, 0);
                CurXfrReq = (HttpWebRequest)HttpWebRequest.Create(remoteUri);
                switch (xfrType)
                {
                    case XfrType.Download:
                        SetupDownloadReqHeaders(CurXfrReq);
                        if (MakeGetRequest(CurXfrReq) == false)
                            ChangeXferState(XferState.Initialized);
                        break;
                    case XfrType.Upload:
                        int XfrFileSz = GetFileSize(localDir + Path.DirectorySeparatorChar + localFile);
                        if (XfrFileSz < 0)
                        {
                            ChangeXferState(XferState.Initialized);
                        }
                        else if (XfrFileSz == 0) 
                        {
                            ShowFatalError ("\"" + localFile + "\" is empty file.", 
                                "Transfer aborted");
                            ChangeXferState(XferState.Initialized);
                        }
                        else
                        {
                            SetupUploadReqHeaders(CurXfrReq, XfrFileSz);
                            if (MakePutRequest(CurXfrReq) == false)
                                ChangeXferState(XferState.Initialized);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowFatalError("Unable to establish connection to server", ex.GetBaseException().GetType().Name,
                    ex.Message);
                ChangeXferState(XferState.Initialized);
            }
        }

        private void StartHeadReq()
        {
            try
            {
                switch (state)
                {
                    case XferState.Unauthorized:
                        ChangeXferState(XferState.Connecting);
                        HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(remoteUri);
                        SetupHeadReqHeaders(req);
                        if (MakeHeadRequest(req) == false)
                            ChangeXferState(XferState.Initialized);
                        break;
                    default:
                        ShowFatalError("Software Error: Expected Unauthorized state");
                        ChangeXferState(XferState.Initialized); // reset
                        break;
                }
            }
            catch (Exception ex)
            {
                ShowFatalError("Unable to establish connection to server", ex.GetBaseException().GetType().Name,
                    ex.Message);
                ChangeXferState(XferState.Initialized);
            }
        }

        private void OnStartBttnClicked(object sender, EventArgs e)
        {
            switch (state)
            {
                case XferState.Undefined:
                    Program.ShowWarning("Transfer parameters not set correctly.");
                    break;
                case XferState.Initialized:
                case XferState.Completed:
                    UserStartXfer();
                    break;
                case XferState.Requesting:
                case XferState.Responding:
                    CurXfrReq.Abort();
                    break;
            }

     
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            OnStartBttnClicked(sender, EventArgs.Empty);
        }


        public static bool Download(Uri remoteUri, String localDir, String localFile)
        {
            HttpFileTransferForm DnldFm = new HttpFileTransferForm();

            if (DnldFm.SetXFrParams(XfrType.Download, remoteUri, localDir + Path.DirectorySeparatorChar + localFile) == true)
            {
                DnldFm.ShowDialog();

                DnldFm.Dispose();

                return true;
            }
            return false;
        }

        public static bool Upload(String localDir, String localFile, Uri remoteUri)
        {
            HttpFileTransferForm UpldFm = new HttpFileTransferForm();

            if (UpldFm.SetXFrParams(XfrType.Upload, localDir + Path.DirectorySeparatorChar + localFile, remoteUri) == true)
            {
                UpldFm.ShowDialog();

                UpldFm.Dispose();

                return true;
            }
            return false;
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            switch (state)
            {
                case XferState.Connecting:
                case XferState.Requesting:
                case XferState.Responding:
                case XferState.Unauthorized:
                case XferState.HeadAuthorized:
                    e.Cancel = true; // ignore request until finished
                    break;
            }
        }

        private void OnCredPromptTmrTick(object sender, EventArgs e)
        {
            CredPromptTmr.Enabled = false; // one-shot

            // Ask user to enter username+password
            NtwkCredDlg Dlg = new NtwkCredDlg();
            Dlg.Title = "Basic Authentication";
            String CurUsrName, CurPasswd;
            if (URIComposeForm.ParseUriUserInfoStr(remoteUri.UserInfo, out CurUsrName, out CurPasswd))
            {
                Dlg.UsrName = CurUsrName;
                Dlg.Password = CurPasswd;
                Dlg.Prompt = "Supplied Username/Password is incorrect.\nPlease enter again.";
            }
            else
                Dlg.Prompt = "Remote folder access requires Username/Password";
            DialogResult Res = Dlg.ShowDialog();
            if (Res == DialogResult.OK)
            {
                SetBasicCredential(remoteUri, Dlg.UsrName, Dlg.Password);
                if (xfrType == XfrType.Upload)
                    StartHeadReq(); // borrow HEAD request to authorize (PUT doesn't seem to work alone)
                else
                    UserStartXfer();
            }
            else // if Canceled, reset state to XferState.Initialized
                ChangeXferState(XferState.Initialized);
            Dlg.Dispose();
        }

        private void OnUploadXferStartTmrTick(object sender, EventArgs e)
        {
            UploadXferStartTmr.Enabled = false; // one-shot
            // double-check
            if (state == XferState.HeadAuthorized && xfrType == XfrType.Upload)
            {
                ChangeXferState(XferState.Initialized);
                UserStartXfer(); // re-start
            }
        }

 
    }
}