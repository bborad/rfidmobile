namespace OnRamp
{
    partial class BarCodeScanSelectDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader columnHeader2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BarCodeScanSelectDlg));
             
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.BCListV = new System.Windows.Forms.ListView();
            this.ScanBttn = new System.Windows.Forms.Button();
            this.CntLbl = new System.Windows.Forms.Label();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.OKBttn = new System.Windows.Forms.Button();
            columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // BCListV
            // 
            this.BCListV.Columns.Add(columnHeader2);
            this.BCListV.FullRowSelect = true;
            resources.ApplyResources(this.BCListV, "BCListV");
            this.BCListV.Name = "BCListV";
            this.BCListV.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            resources.ApplyResources(columnHeader2, "columnHeader2");
            // 
            // ScanBttn
            // 
            resources.ApplyResources(this.ScanBttn, "ScanBttn");
            this.ScanBttn.Name = "ScanBttn";
            this.ScanBttn.Click += new System.EventHandler(this.OnScanBttnClicked);
            // 
            // CntLbl
            // 
            resources.ApplyResources(this.CntLbl, "CntLbl");
            this.CntLbl.ForeColor = System.Drawing.Color.Indigo;
            this.CntLbl.Name = "CntLbl";
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            this.OKBttn.Click += new System.EventHandler(this.OnOKBttnClicked);
            // 
            // BarCodeScanSelectDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CntLbl);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.ScanBttn);
            this.Controls.Add(this.BCListV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BarCodeScanSelectDlg";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.Windows.Forms.ColumnHeader columnHeader2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.BarCodeScanSelectDlg));
             
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.BCListV = new System.Windows.Forms.ListView();
            this.ScanBttn = new System.Windows.Forms.Button();
            this.CntLbl = new System.Windows.Forms.Label();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.OKBttn = new System.Windows.Forms.Button();
            columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // BCListV
            // 
            this.BCListV.Columns.Add(columnHeader2);
            this.BCListV.FullRowSelect = true;
            resources.ApplyResources(this.BCListV, "BCListV");
            this.BCListV.Name = "BCListV";
            this.BCListV.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader2
            // 
            resources.ApplyResources(columnHeader2, "columnHeader2");
            // 
            // ScanBttn
            // 
            resources.ApplyResources(this.ScanBttn, "ScanBttn");
            this.ScanBttn.Name = "ScanBttn";
            this.ScanBttn.Click += new System.EventHandler(this.OnScanBttnClicked);
            // 
            // CntLbl
            // 
            resources.ApplyResources(this.CntLbl, "CntLbl");
            this.CntLbl.ForeColor = System.Drawing.Color.Indigo;
            this.CntLbl.Name = "CntLbl";
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            this.OKBttn.Click += new System.EventHandler(this.OnOKBttnClicked);
            // 
            // BarCodeScanSelectDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.CntLbl);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.ScanBttn);
            this.Controls.Add(this.BCListV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BarCodeScanSelectDlg";
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView BCListV;
        private System.Windows.Forms.Button ScanBttn;
        private System.Windows.Forms.Label CntLbl;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.Button OKBttn;
    }
}