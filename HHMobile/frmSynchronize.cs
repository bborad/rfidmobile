/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Synchronize functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmSynchronize : Form
    {
        public bool flagError { get; set; }
        public delegate void stateHandler(Int64 status);
        public frmSynchronize()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        

        private void btnStart_Click(object sender, EventArgs e)
        {
           
            try
            {
                if (!Sync.IsSyncCompleted)
                {
                    MessageBox.Show( "Sync operation is already being performed in background thread. Try later.");
                    return;
                }

                btnStart.Enabled = false;
                Sync ss = new Sync();
                ss.SyncTables();
                SyncProgress.Value = SyncProgress.Maximum;
                btnStart.Enabled = true;
                if (ss.strError.Length != 0)
                {
                    MessageBox.Show("Synchronization completed.Following errors are occured. \r" + ss.strError);
                }
                else
                {
                    MessageBox.Show("Synchronization completed sucessfully.");
                   
                }
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message);
               
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
                flagError = true;
            }
            catch (System.Data.SqlServerCe.SqlCeException epc)
            {
                MessageBox.Show("Datafile is not accessible.\r\n"+ epc.Message);
                Logger.LogError(epc.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Synchronization was not able to complete.","Synchronization");
                Logger.LogError("Web exception occured -- " + wex.Message);
                flagError = true;
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show("Synchronization was not able to complete.\n"  + ep.Message.ToString());
            }
            finally
            {
                this.Close();
            }
        }
        private void frmSynchronize_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;

            //ProgressStatus.progressState += new ProgressStatus.progressStateHandler(ProgressStatus_progressState); 
            ProgressStatus.progressState += new ProgressStatus.progressStateHandler(ProgressStatus_progressState);
            flagError = false;
        }

        void ProgressStatus_progressState(long status)
        {
            if (this.InvokeRequired)
            {
                //ProgressStatus_progressState(status);
                this.Invoke(new stateHandler(this.ProgressStatus_progressState), new Object[] { status });
                return;
            }
            this.SyncProgress.Value = (int)status;
            return;
        }

        private void frmSynchronize_Closing(object sender, CancelEventArgs e)
        {
            ProgressStatus.Status = 0; 
            ProgressStatus.progressState -= new ProgressStatus.progressStateHandler(ProgressStatus_progressState); 
        }
    }
}