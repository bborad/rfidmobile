using System;using HHDeviceInterface.RFIDSp; 
using ReaderTypes;
using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    // This is needed only because a class-event variable could not be
    // dispatched from functions outside that class
    public delegate void TagChildFormOperStateNotify (TagOperEvtType state);

    public partial class TagWrAnyForm : Form
    {
        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion

        private TagChildFormOperStateNotify OpStateChangeNotify;
        private uint numTagsToWr = 0;
        private int fldWrCnt = 0; // TBD: find a better method to store/manage this.
        #region Tag Write Acc Error Data
        private TagAccErrSummary errSummary;
        #endregion

        public TagWrAnyForm(TagChildFormOperStateNotify operStateNotify)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            // Bank 3 Radio Button
            Bnk3Bttn.Enabled =  ((EPCTag.AvailBanks & MemoryBanks4Op.Three) == MemoryBanks4Op.Three);
            // Enable Bank1 (consistent with RadioButton)
            tagWrBnkInput.EnableBnks(MemoryBanks4Op.One);
            tagWrBnkInput.DisableBnks(~MemoryBanks4Op.One);
            // Make all banks write-able
            tagWrBnkInput.SetBnksWriteable(MemoryBanks4Op.Zero 
                | MemoryBanks4Op.One | MemoryBanks4Op.Three);

            // Start Button State
            StartBttnInitState();

            // Result Button State
            ResBttnInitState();

            errSummary = new TagAccErrSummary();

            OpStateChangeNotify = operStateNotify;
        }

        ~TagWrAnyForm()
        {
            if (TGInputFm != null)
                TGInputFm.Dispose();
        }

        #region Start Button State
        private void StartBttnInitState()
        {
            OpState state = OpState.Stopped;
            StartBttn.Tag = state;
        }

        private OpState StartBttnState()
        {
            OpState state = OpState.Stopped;
            if (StartBttn.Tag is OpState)
                state = (OpState)StartBttn.Tag;
            else
                MessageBox.Show("StartBttn Tag is not of type OpState");
            return state;
        }

        private void StartBttnSetState(OpState state)
        {
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrAnyForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagWrAnyForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrAnyForm));
            }

            StartBttn.Tag = state;
            switch (state)
            {
                case OpState.Starting:
                    StartBttn.Enabled = false;
                    DataDispPanel.Enabled = false; // avoid User-Changes interfere with callbacks
                    if (OpStateChangeNotify != null)
                        OpStateChangeNotify(TagOperEvtType.Started);
                    break;
                case OpState.Stopped:
                    StartBttn.Enabled = true;
                    StartBttn.Text = resources.GetString("StartBttn.Text");
                    DataDispPanel.Enabled = true;
                    if (OpStateChangeNotify != null)
                        OpStateChangeNotify(TagOperEvtType.Stopped);
                    break;
                case OpState.Started:
                    StartBttn.Text = "Stop";
                    StartBttn.Enabled = true;
                    if (OpStateChangeNotify != null)
                        OpStateChangeNotify(TagOperEvtType.Updated);
                    break;
                    // no change in OpState.Stopping
            }
        }
        #endregion

        #region Result Button State
        private void ResBttnInitState()
        {
            bool ResultShown = false;
            ResBttn.Tag = ResultShown;
        }

        private bool ResBttnState()
        {
            bool ResultShown = false;

            if (ResBttn.Tag is Boolean)
            {
                ResultShown = (bool)ResBttn.Tag;
            }
            else
                MessageBox.Show("ResBttn Tag is not of type Boolean");

            return ResultShown;
        }

        private void ResBttnSetState(bool shown)
        {
            ResBttn.Tag = shown;
        }
        #endregion

        private void OnBnk0Clicked(object sender, EventArgs e)
        {
            tagWrBnkInput.EnableBnks(MemoryBanks4Op.Zero);
            tagWrBnkInput.DisableBnks(~(MemoryBanks4Op.Zero));

            // Disable AutoIncr
            AutoIncBttn.Checked = false;
            AutoIncBttn.Enabled = false;
        }

        private void OnBnk1Clicked(object sender, EventArgs e)
        {
            tagWrBnkInput.EnableBnks(MemoryBanks4Op.One);
            tagWrBnkInput.DisableBnks(~MemoryBanks4Op.One);

            // Enable AutoIncr
            AutoIncBttn.Enabled = true;
            AutoIncBttn.Checked = true; // default to true
        }

        private void OnBnk3Clicked(object sender, EventArgs e)
        {
            tagWrBnkInput.EnableBnks(MemoryBanks4Op.Three);
            tagWrBnkInput.DisableBnks(~MemoryBanks4Op.Three);
            
            // Disable AutoIncr
            AutoIncBttn.Checked = false;
            AutoIncBttn.Enabled = false;

        }

#if false
        private void OnAutoIncBttnChkChanged(object sender, EventArgs e)
        {
            if (AutoIncBttn.Checked)
            {
                // Stop At First Tag becomes meaningless
                StopAtOneTagBttn.Checked = false;
                StopAtOneTagBttn.Enabled = false;
            }
            else
            {
                StopAtOneTagBttn.Enabled = true;
            }
        }

        private void OnStopAtFrstTagChkChanged(object sender, EventArgs e)
        {
            if (StopAtOneTagBttn.Checked)
            {
                // AutoInc becomes meaningless
                AutoIncBttn.Checked = false;
                AutoIncBttn.Enabled = false;
            }
            else if (Bnk1Bttn.Checked)
            {
                AutoIncBttn.Enabled = true;
            }

        }
#endif
        #region Tag Write Event Handlers

        private bool TagWrBnkAutoIncreDataReqCb(out MemoryBanks4Op bnk,
            out ushort wdOffSet, out String dataStr)
        {
            // repeat until user-stop
            bnk = ChosenBnk2Wr();
            wdOffSet = 0;
            dataStr = null;
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (StartBttnState() == OpState.Stopping || StartBttnState() == OpState.Stopped)
                return false;
            String OrigInputStr;
            if (!tagWrBnkInput.GetBnkStr(bnk, out wdOffSet, out OrigInputStr))
                return false;
            if (numTagsToWr <= 0)
                return false;
            if (wdOffSet == EPCTag.CRC16FldSz) // Include PC to dataStr
            {
                dataStr = OrigInputStr.Substring(0, EPCTag.PCFldSz * 4) + LastWrittenEPC.ToString();
            }
            else if (wdOffSet == (EPCTag.CRC16FldSz + EPCTag.PCFldSz))
            {
                dataStr = LastWrittenEPC.ToString();
            }
            else
                return false; // something's wrong

            return true;
        }

        private bool TagWrBnkDataReqCb(out MemoryBanks4Op bnk, out ushort wdOffSet,
            out String dataStr)
        {
            bnk = MemoryBanks4Op.None;
            wdOffSet = 0;
            dataStr = null;
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            // User-Stop
            if (StartBttnState() == OpState.Stopping || StartBttnState() == OpState.Stopped)
                return false;
            // if stop at first tag, then wait until fldWrCnt > 0
            // if not stop at first tag, then repeat until user issue stop
            bool ContWr = false;
            ContWr = ((StopAtOneTagBttn.Checked) ? 
                (fldWrCnt <= 0 && errSummary.WriteVerifyWarningsOccurred == false) : true);
            if (! ContWr)
            {
                return false; // done
            }
            bnk = ChosenBnk2Wr();
            return tagWrBnkInput.GetBnkStr(bnk, out wdOffSet, out dataStr);
        }

        private void ShowTagAccErr(String errMsg, UInt16 pc, UINT96_T epc)
        {
            MessageBox.Show(errMsg + "\n" + "Tag ID: " + pc.ToString("X4") + "-" + epc.ToString(),
                "Read Bank Failed");
        }

        private void WrOpEvtHandler(object sender, WrOpEventArgs e)
        {
            // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();

             switch (e.Status)
             {
                 case WrOpStatus.configured:
                     Program.RefreshStatusLabel(this, "Setup finished");
                     break;
                 case WrOpStatus.started:
                     StartBttnSetState(OpState.Started);
                     Program.RefreshStatusLabel(this, "Writing Bank" + ChosenBnk2Wr() + "...");
                     if (!ResBttnState())
                         OnResBttnClicked(this, null);
                     break;
                 case WrOpStatus.completed:
                     Program.RefreshStatusLabel(this, "Writing Bank " + ChosenBnk2Wr() + " (" + fldWrCnt + " Tags)");
                     break;
                 case WrOpStatus.errorStopped:
                 case WrOpStatus.stopped:
                     //Rdr.WrOpStEvent -= WrOpEvtHandler;
                     //Rdr.MemBnkWrEvent -= MemBnkWrEvtHandler;
                     Rdr.UnregisterWrOpStEvent(WrOpEvtHandler);
                     Rdr.UnregisterMemBnkWrEvent(MemBnkWrEvtHandler);
                     StartBttnSetState(OpState.Stopped);
                     if (fldWrCnt > 0)
                     {
                         Program.RefreshStatusLabel(this, "Success: " + fldWrCnt + " new Tags");
                     }
                     else
                     {
                         Program.RefreshStatusLabel(this, "Stopped: 0 new Tags");
                         errSummary.DspyErrSummary();
                     }
                     if (!ResBttnState())
                         OnResBttnClicked(this, null);
                     if (e.Status == WrOpStatus.errorStopped)
                     {
                         if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                             RestartRfidDevice();
                         else
                             if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                 MessageBox.Show("Error: " + e.ErrMsg);
                     }
                     break;
                 case WrOpStatus.error:
                     if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                         MessageBox.Show("Error: " + e.ErrMsg);
                     break;
                 case WrOpStatus.tagAccError:
                     // Only collect error summary (before Tag write completed)
                     if (fldWrCnt == 0)
                         errSummary.Rec(e.TagAccErr);
                     // display error immediately upon Unrecoverable Errors
                     switch (e.TagAccErr)
                     {
                         case AccErrorTypes.Unauthorized:
                             ShowTagAccErr("Password Required.", e.AccTagPC, e.AccTagEPC);
                             break;
                         case AccErrorTypes.AccessPasswordError:
                             ShowTagAccErr("Incorrect Bank Access Password", e.AccTagPC, e.AccTagEPC);
                             break;
                         case AccErrorTypes.InvalidAddr:
                             ShowTagAccErr("Invalid Addr: Requested bank does not exist or Bank Size exceeds available memory",
                                 e.AccTagPC, e.AccTagEPC);
                             break;
                         case AccErrorTypes.WriteCmdError:
                         case AccErrorTypes.WriteError:
                         case AccErrorTypes.WriteResponseCrcError:
                         case AccErrorTypes.WriteRetryCountExceeded:
                         case AccErrorTypes.WriteVerifyCrcError:
                                 OpStateChangeNotify(TagOperEvtType.Updated);
                             break;

                     }
                     if (errSummary.IsAnyWriteError(e.TagAccErr) && AutoIncBttn.Checked)
                     {
                           // These types of error could mean that the tag is written byt not verified
                             // Increment EPC to avoid duplicate Tag at all costs
#if false
                         if (ChosenBnk2Wr() == MemoryBanks4Op.One)
                             AddEPCToListV(e.AccTagEPC, "(P)" + LastWrittenEPC.ToString());
#endif
                         // TBD: for other banks
                         LastWrittenEPC.PlusPlus(); // auto-increment
                     }
                     break;
             }
        }

        private void MemBnkWrEvtHandler(object sender, MemBnkWrEventArgs e)
        {
            ListViewItem ExistingItem = null;
            UINT96_T EPC = e.EPC;
            fldWrCnt++;

            if (e.BankNum == MemoryBanks4Op.One)
            {
                ushort WdOffset;
                String Bnk1Str;
                tagWrBnkInput.GetBnkStr(MemoryBanks4Op.One, out WdOffset, out Bnk1Str);
                // only get the PC portion from Bnk1Str, get the EPC from LastWrittenEPC
                if (WdOffset == EPCTag.CRC16FldSz)
                {
                    Bnk1Str = Bnk1Str.Substring(0, 4) + LastWrittenEPC.ToString();
                }
                else
                {
                    Bnk1Str = LastWrittenEPC.ToString();
                }

                if (AutoIncBttn.Checked == false && StopAtOneTagBttn.Checked == false)
                {
                    AddEPCToFixedSizeListV(e.EPC, Bnk1Str, fldWrCnt, (int)TGInputFm.NumTagsToWr);
                    if (IsFixedSizeListVRowsSameID() && EPCLstV.Items.Count == TGInputFm.NumTagsToWr)
                        this.BackColor = Color.Green;
                    else
                        this.BackColor = Color.White;
                }
                else
                {
                    if (TagAlreadyInListV(ref EPC, out ExistingItem))
                    {
                        // move it to top of the ListView
                        EPCLstV.Items.Remove(ExistingItem);
                        EPCLstV.Items.Insert(0, ExistingItem);
                    }
                    else
                        AddEPCToListV(e.EPC, Bnk1Str.PadLeft(Bnk1Str.Length + ((WdOffset - EPCTag.CRC16FldSz) * 4), 'X'));
                }
                if (OpStateChangeNotify != null)
                    OpStateChangeNotify(TagOperEvtType.Updated);
            }
            else
            {
                if (TagAlreadyInListV(ref EPC, out ExistingItem))
                {
                    // move it to top of the ListView
                    EPCLstV.Items.Remove(ExistingItem);
                    EPCLstV.Items.Insert(0, ExistingItem);
                }
                else
                    AddEPCToListV(e.EPC);
                if (OpStateChangeNotify != null)
                    OpStateChangeNotify(TagOperEvtType.Updated);
            }

            if (AutoIncBttn.Checked)
                LastWrittenEPC.PlusPlus(); // auto-increment
            // Decrement on successful write (duplicate or not)
            numTagsToWr--;
        }
        #endregion

        #region EPC ListView
        // Cnt Col + AccessedEPC Col
        private void EPCListVSetupNonEPCWr()
        {
            // Do nothing if the list is already setup for Non-EPC Write
            if (EPCLstV.Columns != null && EPCLstV.Columns.Count == 2)
                return;
            EPCLstV.BeginUpdate();
            EPCLstV.Clear();
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.Windows.Forms.ColumnHeader EPCColHdr;
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.EPCLstV.Columns.Add(RowNumHdr);
            this.EPCLstV.Columns.Add(EPCColHdr);
            this.EPCLstV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            //resources.ApplyResources(this.EPCListV, "EPCListV");
            RowNumHdr.Text = "";
            RowNumHdr.Width = 30;
            EPCColHdr.Text = "Tag Accessed";
            EPCColHdr.Width = 195;

            EPCLstV.EndUpdate();
        }

        // Cnt Col + AccessedEPC Col + NewEPC Col
        private void EPCListVSetupEPCWr()
        {
            // Do nothing if the list is already setup for EPC Write
            if (EPCLstV.Columns != null && EPCLstV.Columns.Count == 3)
                return;
              EPCLstV.BeginUpdate();
            EPCLstV.Clear();
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.Windows.Forms.ColumnHeader EPC1ColHdr;
            System.Windows.Forms.ColumnHeader EPC2ColHdr;
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            EPC1ColHdr = new System.Windows.Forms.ColumnHeader();
            EPC2ColHdr = new System.Windows.Forms.ColumnHeader();
            this.EPCLstV.Columns.Add(RowNumHdr);
            this.EPCLstV.Columns.Add(EPC1ColHdr);
            this.EPCLstV.Columns.Add(EPC2ColHdr);
            this.EPCLstV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            //resources.ApplyResources(this.EPCListV, "EPCListV");
            RowNumHdr.Text = "";
            RowNumHdr.Width = 30;
            EPC1ColHdr.Text = "Tag Accessed";
            EPC1ColHdr.Width = 195;
            EPC2ColHdr.Text = "New ID";
            EPC2ColHdr.Width = 235; // including PC (4 chars)

            EPCLstV.EndUpdate();
        }

        private void AddEPCToListV(UINT96_T EPC)
        {
            // Assuming that the Tag of this EPC is not already in list
            int CurNumRows = EPCLstV.Items != null ? EPCLstV.Items.Count : 0;
            ListViewItem item = new ListViewItem(new String[] {
                 (CurNumRows+1).ToString(), EPC.ToString() });
            EPCLstV.Items.Insert(0, item); // keep it at top of the list
            item.Tag = EPC;
        }

        private void AddEPCToListV(UINT96_T EPC, String newEPCStr)
        {
            int CurNumRows = EPCLstV.Items != null ? EPCLstV.Items.Count : 0;
            ListViewItem item = new ListViewItem(new String[] {
                 (CurNumRows+1).ToString(), EPC.ToString(), newEPCStr });
            EPCLstV.Items.Insert(0, item); // keep it at top of the list
            item.Tag = EPC;
        }

        //Allow duplicates
        private void AddEPCToFixedSizeListV(UINT96_T EPC, String newEPCStr, int cnt, int maxNumRows)
        {
            ListViewItem item = new ListViewItem(new String[] {
                 cnt.ToString(), EPC.ToString(),  newEPCStr });
            EPCLstV.BeginUpdate();
            item = EPCLstV.Items.Insert(0, item);
            item.Tag = EPC;
            if (EPCLstV.Items.Count > maxNumRows)
                EPCLstV.Items.RemoveAt(maxNumRows);
            EPCLstV.EndUpdate();
        }

        private bool IsFixedSizeListVRowsSameID()
        {
            UINT96_T FirstRowEPC = new UINT96_T();
            bool DiffFound = false;
            if (EPCLstV.Items != null && EPCLstV.Items.Count > 0)
            {
                FirstRowEPC = (UINT96_T)EPCLstV.Items[0].Tag;
                UINT96_T EPC = new UINT96_T();
                for (int i = 1; i < EPCLstV.Items.Count; i++)
                {
                    EPC = (UINT96_T)EPCLstV.Items[i].Tag;
                    if (EPC.Equals(FirstRowEPC) == false)
                    {
                        DiffFound = true;
                        break;
                    }
                }
            }
            else
                return false;
            return (DiffFound == false);
        }

        private bool TagAlreadyInListV(ref UINT96_T tag, out ListViewItem foundItem)
        {
            bool Matched = false;
            foundItem = null;
            foreach (ListViewItem item in EPCLstV.Items)
            {
                if (item.Tag is UINT96_T)
                {
                    UINT96_T EPC = (UINT96_T)item.Tag;
                    Matched = (EPC.m_CSB == tag.m_CSB
                        && EPC.m_LSB == tag.m_LSB
                        && EPC.m_MSB == tag.m_MSB);
                    if (Matched)
                    {
                        foundItem = item;
                        break; // from foreach
                    }
                }
                else
                {
                    throw new ApplicationException("ListVItem not of type UINT96_T");
                }
            }
            return Matched;
        }
        #endregion

        private bool ValidateInputData(String usrInputStr)
        {
            if (usrInputStr == null || usrInputStr.Length <= 0)
            {
                MessageBox.Show("No data to write");
                return false;
            }
            if ((usrInputStr.Length & 0x03) != 0)
            {
                MessageBox.Show("Data to write must be in multiple of 1 word(2 bytes)");
                return false;
            }

            return true;
        }

        private MemoryBanks4Op ChosenBnk2Wr()
        {
            MemoryBanks4Op Bnk2Wr = MemoryBanks4Op.None;

            if (Bnk0Bttn.Checked)
                Bnk2Wr = MemoryBanks4Op.Zero;
            else if (Bnk1Bttn.Checked)
                Bnk2Wr = MemoryBanks4Op.One;
            else if (Bnk3Bttn.Checked)
                Bnk2Wr = MemoryBanks4Op.Three;

            return Bnk2Wr;
        }

        private bool ValidateNonEPCBnkWrInputs(MemoryBanks4Op bnk2Wr)
        {
            if (AutoIncBttn.Checked)
            {
                MessageBox.Show("Auto-Increment not supported in this bank");
                return false;
            }
            String Str2Wr;
            ushort WdOffset;
            if (!tagWrBnkInput.GetBnkStr(bnk2Wr, out WdOffset, out Str2Wr))
            {
                MessageBox.Show("Invalid Inputs");
                return false;
            }
            else if (!ValidateInputData(Str2Wr))
                return false;

            return true;
        }

        private bool StartNonEPCBnkWr(MemoryBanks4Op bnk2Wr)
        {
            return StartNonEPCBnkWr(bnk2Wr, 0);
        }

        private bool StartNonEPCBnkWr(MemoryBanks4Op bnk2Wr, UInt32 accPwd)
        {
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            String Str2Wr;
            ushort WdOffset;
            tagWrBnkInput.GetBnkStr(bnk2Wr, out WdOffset, out Str2Wr);
            bool StartSucc = false;

            UserPref Pref = UserPref.GetInstance();
            Rdr.AccessPassword = accPwd;
           // Rdr.TagWriteBankData += TagWrBnkDataReqCb;
            Rdr.RegisterTagWriteBankDataEvent(TagWrBnkDataReqCb);
            
            if (StopAtOneTagBttn.Checked)
            {
                StartSucc = Rdr.TagWriteStart(2);
            }
            else
            {                
                StartSucc = Rdr.TagWriteStart(3);
            }
            if (!StartSucc)
            {
                MessageBox.Show("Start Tag Write Failed");
            }
            return StartSucc;
        }

        private bool ValidateEPCBnkWrInputs()
        {
            if (AutoIncBttn.Checked && StopAtOneTagBttn.Checked)
            {
                MessageBox.Show("Auto-Increment and Stop-At-One-Tag could not be both selected for Bank 1");
                return false;
            }

            if (!StopAtOneTagBttn.Checked && !AutoIncBttn.Checked)
            {
                #if false
                MessageBox.Show("Either Auto-Increment or Stop-At-One-Tag must be selected for Bank 1");
                return false;
                #else
                if (Program.AskUserConfirm("Write the same PC/EPC value to all  RFID Tags. Continue?") != DialogResult.OK)
                    return false;
                #endif
            }
            String EPCStr2Wr;
            ushort WdOffset;
            if (!tagWrBnkInput.GetBnkStr(MemoryBanks4Op.One, out WdOffset, out EPCStr2Wr))
            {
                MessageBox.Show("Invalid Inputs");
                return false;
            }
            else if (!ValidateInputData(EPCStr2Wr))
                return false;
            else // Save the EPCStr to LastWrittenEPC
            {
                // EPC to write must be 'full size'
                int StrIdx = 0; // index to EPCStr2Wr
                if (WdOffset == EPCTag.CRC16FldSz) // PC included
                    StrIdx = (EPCTag.PCFldSz) * 4;
                if (!LastWrittenEPC.ParseString(EPCStr2Wr.Substring(StrIdx)))
                {
                    throw new ApplicationException("Invalid EPC String characters");
                }
            }
            return true;
        }

        private bool StartEPCBnkWr()
        {
            return StartEPCBnkWr(0);
        }

        private UINT96_T LastWrittenEPC = new UINT96_T(); // for auto-increment
        private TagGrpInputForm TGInputFm = new TagGrpInputForm();

        private bool StartEPCBnkWr(UInt32 accPwd)
        {
            bool StartSucc = false;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            UserPref Pref = UserPref.GetInstance();
            if (AutoIncBttn.Checked)
            {
                //  Obtain Tag Group Mask 
                // assuming that LastWrittenEPC is setup from ValidateEPCBnkWrInputs()
                // to save user typing
#if false
                TGInputFm.EPCMask = LastWrittenEPC.ToByteArray();
                DialogResult Res = TGInputFm.ShowDialog();
                if (Res == DialogResult.OK) // user Cancel
                {
                    // Tag Write Inverse Post Match Mask
#if true
                    StartSucc = Rdr.TagWriteStart(TGInputFm.EPCMask, 0,
                        TagWrBnkAutoIncreDataReqCb, (ushort)Pref.QSize, accPwd);
#else
                
                MessageBox.Show ("Tag Write Start with Auto Incr here\nInverse Mask = " + RFIDRdr.ByteArrToHexStr(EPCMask));
                StartSucc = false;
                
#endif
                }
#else
                DialogResult Res = TGInputFm.ShowDialog();
               
                if (Res == DialogResult.OK) // user Cancel
                {
                    if (TGInputFm.NumTagsToWr <= 0)
                    {
                        Program.ShowError("Please input Num of Tags to Write");
                    }
                    else
                    {
                        numTagsToWr = TGInputFm.NumTagsToWr;
                        // NumTags * 2 to give avoid collision (important)
                        Rdr.NumberTags = numTagsToWr;
                        Rdr.AccessPassword = accPwd;
                       // Rdr.TagWriteBankData += TagWrBnkAutoIncreDataReqCb;
                        Rdr.RegisterTagWriteBankDataEvent(TagWrBnkAutoIncreDataReqCb);
                        StartSucc = Rdr.TagWriteStart(5);
                    }
                }
#endif
                
                // Do not dispose TGInputFm, use it to save the last input mask
            }
            else if (StopAtOneTagBttn.Checked)
            {
                Rdr.AccessPassword = accPwd;
                //Rdr.TagWriteBankData += TagWrBnkDataReqCb;
                Rdr.RegisterTagWriteBankDataEvent(TagWrBnkDataReqCb);
                StartSucc = Rdr.TagWriteStart(2);
                //MessageBox.Show("TagWriteStart here");
            }
            else // Write Any Tag without Mask
            {
                // Add Filter to avoid doing write again to those already written
                // (Disable Filtering for now)
                DialogResult Res = TGInputFm.ShowDialog();
                if (Res == DialogResult.OK) // user Cancel
                {
                    if (TGInputFm.NumTagsToWr <= 0)
                    {
                        Program.ShowError("Please input Num of Tags to Write");
                    }
                    else
                    {
                        // use population as hint for Q-Size Setting, but not
                        // for limiting to number of times to perform write
                        uint NumTags = TGInputFm.NumTagsToWr;
                       
                        // NumTags * 2 to give avoid collision (important)
                        Rdr.NumberTags = NumTags;
                        Rdr.AccessPassword = accPwd;
                       // Rdr.TagWriteBankData += TagWrBnkAutoIncreDataReqCb;
                        Rdr.RegisterTagWriteBankDataEvent(TagWrBnkAutoIncreDataReqCb);
                        Rdr.LastWrittenEPC = LastWrittenEPC.ToByteArray();
                        if (TGInputFm.DupWrFilterOn)
                            StartSucc = Rdr.TagWriteStart(6);
                        else
                            StartSucc = Rdr.TagWriteStart(5);
                    }
                }
            }

            return StartSucc;
            
        }

        private void OnStartBttnClicked(object sender, EventArgs e)
        {
            OpState CurState = StartBttnState();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (CurState)
            {
                case OpState.Stopped:
                    fldWrCnt = 0;
                    errSummary.Clear();
                    Program.RefreshStatusLabel(this, "Starting...");
                    StartBttnSetState(OpState.Starting);
                    //Rdr.WrOpStEvent += new EventHandler<WrOpEventArgs>(WrOpEvtHandler);
                    //Rdr.MemBnkWrEvent += new EventHandler<MemBnkWrEventArgs>(MemBnkWrEvtHandler);
                    Rdr.RegisterWrOpStEvent(WrOpEvtHandler);
                    Rdr.RegisterMemBnkWrEvent(MemBnkWrEvtHandler);
                    bool StartSucc = false;
                    UInt32 AccPwd = 0;
                    MemoryBanks4Op Bnk2Wr = ChosenBnk2Wr();
                    switch (Bnk2Wr)
                    {
                        case MemoryBanks4Op.Zero:
                        case MemoryBanks4Op.Three:
                            if (ValidateNonEPCBnkWrInputs(Bnk2Wr))
                            {
                                EPCListVSetupNonEPCWr();

                                if (PwdReqChkBx.Checked)
                                {
                                    if (PasswdInputDlg.GetPassword(out AccPwd))
                                        StartSucc = StartNonEPCBnkWr(Bnk2Wr, AccPwd);
                                    else // user canceled
                                        StartSucc = false;
                                }
                                else
                                    StartSucc = StartNonEPCBnkWr(Bnk2Wr);
                            }
                            else
                                StartSucc = false;
                            break;
                        case MemoryBanks4Op.One:
                            if (ValidateEPCBnkWrInputs())
                            {
                                EPCListVSetupEPCWr();

                                if (PwdReqChkBx.Checked)
                                {
                                    if (PasswdInputDlg.GetPassword(out AccPwd))
                                        StartSucc = StartEPCBnkWr(AccPwd);
                                    else // user canceled
                                        StartSucc = false;
                                }
                                else
                                    StartSucc = StartEPCBnkWr();
                            }
                            else
                                StartSucc = false;
                            break;
                        default:
                            Program.ShowError("Please select a bank to write");
                            StartSucc = false;
                            break;
                    }
                    // Handle StartSucc 
                    if (!StartSucc)
                    {
                        //Rdr.WrOpStEvent -= WrOpEvtHandler;
                        //Rdr.MemBnkWrEvent -= MemBnkWrEvtHandler;
                        Rdr.UnregisterWrOpStEvent(WrOpEvtHandler);
                        Rdr.UnregisterMemBnkWrEvent(MemBnkWrEvtHandler);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        StartBttnSetState(OpState.Stopped);
                    }
                    break;
                case OpState.Started:
                    Program.RefreshStatusLabel(this, "Stopping...");
                    StartBttnSetState(OpState.Stopping);
                    if (!Rdr.TagWriteStop())
                    {
                        Program.ShowError("Unable to Stop Tag-Write Operation");
                        //Rdr.WrOpStEvent -= WrOpEvtHandler;
                        //Rdr.MemBnkWrEvent -= MemBnkWrEvtHandler;
                        Rdr.UnregisterWrOpStEvent(WrOpEvtHandler);
                        Rdr.UnregisterMemBnkWrEvent(MemBnkWrEvtHandler);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        StartBttnSetState(OpState.Stopped);
                    }
                    break;
                case OpState.Starting:
                    Program.RefreshStatusLabel(this, "Stopping...");
                    StartBttnSetState(OpState.Stopping);
                    if (!Rdr.TagWriteStop())
                    {
                        // Restore
                        Program.RefreshStatusLabel(this, "Starting...");
                        StartBttnSetState(OpState.Starting);
                        Program.ShowWarning("Tag Wr Stop failed during Starting phase, please try again");
                    }
                    break;
            }
        }

        private void OnResBttnClicked(object sender, EventArgs e)
        {
            bool ResShown = ResBttnState();
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrAnyForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagWrAnyForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrAnyForm));
            }

            if (ResShown)
            {
                resources.ApplyResources(ResBttn, "ResBttn");
                resources.ApplyResources(EPCLstV, "EPCLstV");
                DataDispPanel.Visible = true;
                ResBttnSetState(false);
            }
            else
            {
                ResBttn.Text = "Hide Results";
                Point NewLoc = DataDispPanel.Location;
                EPCLstV.Location = NewLoc;
                EPCLstV.Height = StartBttn.Location.Y - NewLoc.Y;
                DataDispPanel.Visible = false;
            }
            ResBttnSetState(!ResShown);
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;
            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }

            this.KeyPreview = true;
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (StartBttnState() != OpState.Stopped)
            {
                MessageBox.Show("Please Stop The Write Operation First", "Request Denied");
                e.Cancel = true;
            }
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        #region F11/F4/F5 HotKey Handler
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {

            switch (keyCode)
            {
                case eVKey.VK_F11:
                case eVKey.VK_F19:
                    HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    {
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if (StartBttnState() == OpState.Stopped)
                            {
                                // Do not auto-repeat if Stop-At-One-Tag is selected
                                if (!StopAtOneTagBttn.Checked || !F11Depressed)
                                    OnStartBttnClicked(this, null);
                            }
                            F11Depressed = true;
                        }
                        else
                        {
                            if (StartBttnState() == OpState.Started
                                || StartBttnState() == OpState.Starting)
                            {
                                OnStartBttnClicked(this, null);
                            }
                            F11Depressed = false;
                        }
                       
                    }

                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (StartBttnState() == OpState.Stopped)
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && StartBttnState() == OpState.Stopped)
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
               // case Keys.F7:
               // case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (StartBttnState() != OpState.Stopped)
                    {
                        MessageBox.Show("Please Stop The Write Operation First", "Request Denied");
                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }


        #endregion

        private void OnClrBttnClicked(object sender, EventArgs e)
        {
#if false // debug only
            MemoryBanks4Op Bnk;
            ushort WdOffset;
            String EPCStr;
            if (TagWrBnkAutoIncreDataReqCb(out Bnk, out WdOffset, out EPCStr))
            {
                MessageBox.Show("WdOffset: " + WdOffset.ToString() + "\r\n" + EPCStr);
            }
            else
                MessageBox.Show("AutoIncreData return error");
            LastWrittenEPC.PlusPlus();
#endif
            if (EPCLstV.Items != null)
                EPCLstV.Items.Clear();
        }

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        private void OnFormKeyPressed(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'y' || e.KeyChar == 'Y')
            {
                errSummary.DspyErrSummary();
            }
        }
    }
}