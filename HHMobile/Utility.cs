﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace OnRamp
{
    public class Utility
    {

        #region interop stuff used for autocompletion
        const int CB_SETEDITSEL = 322;
        const int CB_FINDSTRING = 332;
        const int CB_SHOWDROPDOWN = 0x14F;
        const int CB_SETITEMHEIGHT = 339;

        public const int WS_HSCROLL = 0x00100000;
        public const int GWL_STYLE = (-16);
        public const int LB_SETHORIZONTALEXTENT = 0x194;
       
        private const int CB_SETHORIZONTALEXTENT = 0x015E;

        [DllImport("coredll.dll")]
        static extern IntPtr GetCapture();
        [DllImport("coredll.dll")]
        static extern int SendMessage(IntPtr hwnd, int msg, int wParam, string lParam);
        [DllImport("coredll.dll")]
        static extern int SendMessage(IntPtr hwnd, int msg, int wParam, int lParam);
        [DllImport("coredll.dll")]
        public static extern int
            SendMessage(IntPtr hwnd, int wMsg, int wParam, IntPtr lParam);


        [DllImport("coredll.dll")]
        public static extern int
            GetWindowLong(IntPtr hwnd, int nIndex);
        [DllImport("coredll.dll")]
        public static extern int
            SetWindowLong(IntPtr hwnd, int nIndex, int dwNewLong);




        // Set the horizontal extent for the listbox!




        //void keyUpHandler(object sender, KeyEventArgs e)
        //{
        //    AutoComplete(e);
        //}

        private int findString(string s, IntPtr handle)
        {
            //wparam is index before first element to be evaluated, lparam is the string to search for
            return SendMessage(handle, CB_FINDSTRING, -1, s);
        }

        public static void setSelectionStart(short start, IntPtr handle)
        {//High order word indicates selection length. 
            //FFFF will just select everything after the 'start' index.
            //Low order word indicates the start index
            uint lParam = (uint)0xFFFF0000 | (ushort)start;
            SendMessage(handle, CB_SETEDITSEL, 0, (int)lParam);
        }

        public static void setDropDownHeight(IntPtr hwnd, int height)
        {
            SendMessage(hwnd, CB_SETITEMHEIGHT, -1, height);
        }

        public static void setShowDropDown(IntPtr hwnd)
        {
            SendMessage(hwnd, CB_SHOWDROPDOWN, 1, 0);
        }

        public static void setHorizontalScrollBar(IntPtr hwnd, int width)
        {
            int listStyle = GetWindowLong(hwnd, GWL_STYLE);
            listStyle |= WS_HSCROLL;
            listStyle = SetWindowLong(hwnd, GWL_STYLE, listStyle);

            SendMessage(hwnd, CB_SETHORIZONTALEXTENT, width + 1500, IntPtr.Zero);

        }

        #endregion
    }
}
