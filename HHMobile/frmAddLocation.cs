/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Add Location functionality
 **************************************************************************************/


using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmAddLocation : Form
    {
        public bool newLocation;
        public String TagNo
        {
            set
            {
                txtTag.Text = value;
            }
        }

        public String LocationNo
        {
            set
            {
                txtLocNo.Text = value;
            }
        }

        public bool IsDeleting { get; set; }


        public String LocationName
        {
            set
            {
                txtName.Text = value;
            }
        }

        public Int32 Serverkey { get; set; }

        public frmAddLocation()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            newLocation = true;
        }


        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            //if (!(txtTag.Text.Trim().Length <= 24 && txtTag.Text.Trim().Length >= 0))
            //{
            //    MessageBox.Show("Invalid Tag No.");
            //    return;
            //}
            if (txtLocNo.Text.Trim().Length == 0)
            {
                MessageBox.Show("Please Add Unique Location No.");
                return;
            }
            try
            {
                if (newLocation == true)
                    Locations.AddLocation(txtTag.Text.Trim(), txtLocNo.Text.Trim(), txtName.Text.Trim());
                else if (IsDeleting)
                    Locations.DeleteLocation(Serverkey);
                else
                    Locations.EditLocation(txtTag.Text.Trim(), txtLocNo.Text.Trim(), txtName.Text.Trim(), Serverkey);

                MessageBox.Show("Location Sucessfully "+((IsDeleting)?"Deleted.":"Saved."));
                this.Close();
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());

            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void frmAddLocation_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;

            txtName.MaxLength = 100;
            txtLocNo.MaxLength = 100;
            if (newLocation == false)
            {
                txtTag.Enabled = false;
            }
            else
            {
                txtTag.Enabled = true;
            }

            if (IsDeleting)
            {
                btnSave.Text = "Delete";
                txtTag.Enabled = false;
                txtName.Enabled = false;
                txtLocNo.Enabled = false;
                
            }

        }

        private void panel1_GotFocus(object sender, EventArgs e)
        {

        }
    }
}