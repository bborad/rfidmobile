namespace OnRamp
{
    partial class DBMgmtForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBMgmtForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.DBTabCtrl = new System.Windows.Forms.TabControl();
            this.InvtryPg = new System.Windows.Forms.TabPage();
            this.TagInvtryDBPrefInput = new OnRamp.DBPrefInput();
            this.TagRdPg = new System.Windows.Forms.TabPage();
            this.TagRdDBPrefInput = new OnRamp.DBPrefInput();
            this.TagWrPg = new System.Windows.Forms.TabPage();
            this.TagWrDBPrefInput = new OnRamp.DBPrefInput();
            this.AuthenPg = new System.Windows.Forms.TabPage();
            this.TagAuthDBPrefInput = new OnRamp.DBPrefInput();
            this.CommPg = new System.Windows.Forms.TabPage();
            this.TagCommDBPrefInput = new OnRamp.DBPrefInput();
            this.BarcodePg = new System.Windows.Forms.TabPage();
            this.BcdeDBPrefInput = new OnRamp.DBPrefInput();
            this.DLBttn = new System.Windows.Forms.Button();
            this.ULBttn = new System.Windows.Forms.Button();
            this.ConBttn = new System.Windows.Forms.Button();
            this.ViewBttn = new System.Windows.Forms.Button();
            this.DBTabCtrl.SuspendLayout();
            this.InvtryPg.SuspendLayout();
            this.TagRdPg.SuspendLayout();
            this.TagWrPg.SuspendLayout();
            this.AuthenPg.SuspendLayout();
            this.CommPg.SuspendLayout();
            this.BarcodePg.SuspendLayout();
            this.SuspendLayout();
            // 
            // DBTabCtrl
            // 
            this.DBTabCtrl.Controls.Add(this.InvtryPg);
            this.DBTabCtrl.Controls.Add(this.TagRdPg);
            this.DBTabCtrl.Controls.Add(this.TagWrPg);
            this.DBTabCtrl.Controls.Add(this.AuthenPg);
            this.DBTabCtrl.Controls.Add(this.CommPg);
            this.DBTabCtrl.Controls.Add(this.BarcodePg);
            resources.ApplyResources(this.DBTabCtrl, "DBTabCtrl");
            this.DBTabCtrl.Name = "DBTabCtrl";
            this.DBTabCtrl.SelectedIndex = 0;
            // 
            // InvtryPg
            // 
            this.InvtryPg.BackColor = System.Drawing.Color.White;
            this.InvtryPg.Controls.Add(this.TagInvtryDBPrefInput);
            resources.ApplyResources(this.InvtryPg, "InvtryPg");
            this.InvtryPg.Name = "InvtryPg";
            // 
            // TagInvtryDBPrefInput
            // 
            resources.ApplyResources(this.TagInvtryDBPrefInput, "TagInvtryDBPrefInput");
            this.TagInvtryDBPrefInput.Name = "TagInvtryDBPrefInput";
            // 
            // TagRdPg
            // 
            this.TagRdPg.BackColor = System.Drawing.Color.White;
            this.TagRdPg.Controls.Add(this.TagRdDBPrefInput);
            resources.ApplyResources(this.TagRdPg, "TagRdPg");
            this.TagRdPg.Name = "TagRdPg";
            // 
            // TagRdDBPrefInput
            // 
            resources.ApplyResources(this.TagRdDBPrefInput, "TagRdDBPrefInput");
            this.TagRdDBPrefInput.Name = "TagRdDBPrefInput";
            // 
            // TagWrPg
            // 
            this.TagWrPg.BackColor = System.Drawing.Color.White;
            this.TagWrPg.Controls.Add(this.TagWrDBPrefInput);
            resources.ApplyResources(this.TagWrPg, "TagWrPg");
            this.TagWrPg.Name = "TagWrPg";
            // 
            // TagWrDBPrefInput
            // 
            resources.ApplyResources(this.TagWrDBPrefInput, "TagWrDBPrefInput");
            this.TagWrDBPrefInput.Name = "TagWrDBPrefInput";
            // 
            // AuthenPg
            // 
            this.AuthenPg.BackColor = System.Drawing.Color.White;
            this.AuthenPg.Controls.Add(this.TagAuthDBPrefInput);
            resources.ApplyResources(this.AuthenPg, "AuthenPg");
            this.AuthenPg.Name = "AuthenPg";
            // 
            // TagAuthDBPrefInput
            // 
            resources.ApplyResources(this.TagAuthDBPrefInput, "TagAuthDBPrefInput");
            this.TagAuthDBPrefInput.Name = "TagAuthDBPrefInput";
            // 
            // CommPg
            // 
            this.CommPg.BackColor = System.Drawing.Color.White;
            this.CommPg.Controls.Add(this.TagCommDBPrefInput);
            resources.ApplyResources(this.CommPg, "CommPg");
            this.CommPg.Name = "CommPg";
            // 
            // TagCommDBPrefInput
            // 
            resources.ApplyResources(this.TagCommDBPrefInput, "TagCommDBPrefInput");
            this.TagCommDBPrefInput.Name = "TagCommDBPrefInput";
            // 
            // BarcodePg
            // 
            this.BarcodePg.BackColor = System.Drawing.Color.White;
            this.BarcodePg.Controls.Add(this.BcdeDBPrefInput);
            resources.ApplyResources(this.BarcodePg, "BarcodePg");
            this.BarcodePg.Name = "BarcodePg";
            // 
            // BcdeDBPrefInput
            // 
            resources.ApplyResources(this.BcdeDBPrefInput, "BcdeDBPrefInput");
            this.BcdeDBPrefInput.Name = "BcdeDBPrefInput";
            // 
            // DLBttn
            // 
            resources.ApplyResources(this.DLBttn, "DLBttn");
            this.DLBttn.Name = "DLBttn";
            this.DLBttn.Click += new System.EventHandler(this.OnDLBttnClicked);
            // 
            // ULBttn
            // 
            resources.ApplyResources(this.ULBttn, "ULBttn");
            this.ULBttn.Name = "ULBttn";
            this.ULBttn.Click += new System.EventHandler(this.OnULBttnClicked);
            // 
            // ConBttn
            // 
            resources.ApplyResources(this.ConBttn, "ConBttn");
            this.ConBttn.Name = "ConBttn";
            this.ConBttn.Click += new System.EventHandler(this.OnConsBttnClicked);
            // 
            // ViewBttn
            // 
            resources.ApplyResources(this.ViewBttn, "ViewBttn");
            this.ViewBttn.Name = "ViewBttn";
            this.ViewBttn.Click += new System.EventHandler(this.OnViewBttnClicked);
            // 
            // DBMgmtForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ViewBttn);
            this.Controls.Add(this.ConBttn);
            this.Controls.Add(this.ULBttn);
            this.Controls.Add(this.DLBttn);
            this.Controls.Add(this.DBTabCtrl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBMgmtForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.DBTabCtrl.ResumeLayout(false);
            this.InvtryPg.ResumeLayout(false);
            this.TagRdPg.ResumeLayout(false);
            this.TagWrPg.ResumeLayout(false);
            this.AuthenPg.ResumeLayout(false);
            this.CommPg.ResumeLayout(false);
            this.BarcodePg.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.DBMgmtForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.DBTabCtrl = new System.Windows.Forms.TabControl();
            this.InvtryPg = new System.Windows.Forms.TabPage();
            this.TagInvtryDBPrefInput = new OnRamp.DBPrefInput();
            this.TagRdPg = new System.Windows.Forms.TabPage();
            this.TagRdDBPrefInput = new OnRamp.DBPrefInput();
            this.TagWrPg = new System.Windows.Forms.TabPage();
            this.TagWrDBPrefInput = new OnRamp.DBPrefInput();
            this.AuthenPg = new System.Windows.Forms.TabPage();
            this.TagAuthDBPrefInput = new OnRamp.DBPrefInput();
            this.CommPg = new System.Windows.Forms.TabPage();
            this.TagCommDBPrefInput = new OnRamp.DBPrefInput();
            this.BarcodePg = new System.Windows.Forms.TabPage();
            this.BcdeDBPrefInput = new OnRamp.DBPrefInput();
            this.DLBttn = new System.Windows.Forms.Button();
            this.ULBttn = new System.Windows.Forms.Button();
            this.ConBttn = new System.Windows.Forms.Button();
            this.ViewBttn = new System.Windows.Forms.Button();
            this.DBTabCtrl.SuspendLayout();
            this.InvtryPg.SuspendLayout();
            this.TagRdPg.SuspendLayout();
            this.TagWrPg.SuspendLayout();
            this.AuthenPg.SuspendLayout();
            this.CommPg.SuspendLayout();
            this.BarcodePg.SuspendLayout();
            this.SuspendLayout();
            // 
            // DBTabCtrl
            // 
            this.DBTabCtrl.Controls.Add(this.InvtryPg);
            this.DBTabCtrl.Controls.Add(this.TagRdPg);
            this.DBTabCtrl.Controls.Add(this.TagWrPg);
            this.DBTabCtrl.Controls.Add(this.AuthenPg);
            this.DBTabCtrl.Controls.Add(this.CommPg);
            this.DBTabCtrl.Controls.Add(this.BarcodePg);
            resources.ApplyResources(this.DBTabCtrl, "DBTabCtrl");
            this.DBTabCtrl.Name = "DBTabCtrl";
            this.DBTabCtrl.SelectedIndex = 0;
            // 
            // InvtryPg
            // 
            this.InvtryPg.BackColor = System.Drawing.Color.White;
            this.InvtryPg.Controls.Add(this.TagInvtryDBPrefInput);
            resources.ApplyResources(this.InvtryPg, "InvtryPg");
            this.InvtryPg.Name = "InvtryPg";
            // 
            // TagInvtryDBPrefInput
            // 
            resources.ApplyResources(this.TagInvtryDBPrefInput, "TagInvtryDBPrefInput");
            this.TagInvtryDBPrefInput.Name = "TagInvtryDBPrefInput";
            // 
            // TagRdPg
            // 
            this.TagRdPg.BackColor = System.Drawing.Color.White;
            this.TagRdPg.Controls.Add(this.TagRdDBPrefInput);
            resources.ApplyResources(this.TagRdPg, "TagRdPg");
            this.TagRdPg.Name = "TagRdPg";
            // 
            // TagRdDBPrefInput
            // 
            resources.ApplyResources(this.TagRdDBPrefInput, "TagRdDBPrefInput");
            this.TagRdDBPrefInput.Name = "TagRdDBPrefInput";
            // 
            // TagWrPg
            // 
            this.TagWrPg.BackColor = System.Drawing.Color.White;
            this.TagWrPg.Controls.Add(this.TagWrDBPrefInput);
            resources.ApplyResources(this.TagWrPg, "TagWrPg");
            this.TagWrPg.Name = "TagWrPg";
            // 
            // TagWrDBPrefInput
            // 
            resources.ApplyResources(this.TagWrDBPrefInput, "TagWrDBPrefInput");
            this.TagWrDBPrefInput.Name = "TagWrDBPrefInput";
            // 
            // AuthenPg
            // 
            this.AuthenPg.BackColor = System.Drawing.Color.White;
            this.AuthenPg.Controls.Add(this.TagAuthDBPrefInput);
            resources.ApplyResources(this.AuthenPg, "AuthenPg");
            this.AuthenPg.Name = "AuthenPg";
            // 
            // TagAuthDBPrefInput
            // 
            resources.ApplyResources(this.TagAuthDBPrefInput, "TagAuthDBPrefInput");
            this.TagAuthDBPrefInput.Name = "TagAuthDBPrefInput";
            // 
            // CommPg
            // 
            this.CommPg.BackColor = System.Drawing.Color.White;
            this.CommPg.Controls.Add(this.TagCommDBPrefInput);
            resources.ApplyResources(this.CommPg, "CommPg");
            this.CommPg.Name = "CommPg";
            // 
            // TagCommDBPrefInput
            // 
            resources.ApplyResources(this.TagCommDBPrefInput, "TagCommDBPrefInput");
            this.TagCommDBPrefInput.Name = "TagCommDBPrefInput";
            // 
            // BarcodePg
            // 
            this.BarcodePg.BackColor = System.Drawing.Color.White;
            this.BarcodePg.Controls.Add(this.BcdeDBPrefInput);
            resources.ApplyResources(this.BarcodePg, "BarcodePg");
            this.BarcodePg.Name = "BarcodePg";
            // 
            // BcdeDBPrefInput
            // 
            resources.ApplyResources(this.BcdeDBPrefInput, "BcdeDBPrefInput");
            this.BcdeDBPrefInput.Name = "BcdeDBPrefInput";
            // 
            // DLBttn
            // 
            resources.ApplyResources(this.DLBttn, "DLBttn");
            this.DLBttn.Name = "DLBttn";
            this.DLBttn.Click += new System.EventHandler(this.OnDLBttnClicked);
            // 
            // ULBttn
            // 
            resources.ApplyResources(this.ULBttn, "ULBttn");
            this.ULBttn.Name = "ULBttn";
            this.ULBttn.Click += new System.EventHandler(this.OnULBttnClicked);
            // 
            // ConBttn
            // 
            resources.ApplyResources(this.ConBttn, "ConBttn");
            this.ConBttn.Name = "ConBttn";
            this.ConBttn.Click += new System.EventHandler(this.OnConsBttnClicked);
            // 
            // ViewBttn
            // 
            resources.ApplyResources(this.ViewBttn, "ViewBttn");
            this.ViewBttn.Name = "ViewBttn";
            this.ViewBttn.Click += new System.EventHandler(this.OnViewBttnClicked);
            // 
            // DBMgmtForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ViewBttn);
            this.Controls.Add(this.ConBttn);
            this.Controls.Add(this.ULBttn);
            this.Controls.Add(this.DLBttn);
            this.Controls.Add(this.DBTabCtrl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DBMgmtForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.DBTabCtrl.ResumeLayout(false);
            this.InvtryPg.ResumeLayout(false);
            this.TagRdPg.ResumeLayout(false);
            this.TagWrPg.ResumeLayout(false);
            this.AuthenPg.ResumeLayout(false);
            this.CommPg.ResumeLayout(false);
            this.BarcodePg.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl DBTabCtrl;
        private System.Windows.Forms.TabPage InvtryPg;
        private System.Windows.Forms.TabPage TagRdPg;
        private System.Windows.Forms.TabPage TagWrPg;
        private System.Windows.Forms.TabPage AuthenPg;
        private System.Windows.Forms.TabPage CommPg;
        private System.Windows.Forms.Button DLBttn;
        private System.Windows.Forms.Button ULBttn;
        private DBPrefInput TagRdDBPrefInput;
        private DBPrefInput TagInvtryDBPrefInput;
        private DBPrefInput TagWrDBPrefInput;
        private DBPrefInput TagAuthDBPrefInput;
        private DBPrefInput TagCommDBPrefInput;
        private System.Windows.Forms.TabPage BarcodePg;
        private DBPrefInput BcdeDBPrefInput;
        private System.Windows.Forms.Button ConBttn;
        private System.Windows.Forms.Button ViewBttn;
    }
}