﻿

using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;
using ClsReaderLib;
using HHDeviceInterface.BarCode;
using System.Collections;

namespace OnRamp
{
    public partial class frmDDSelection : Form
    {

        public string objectName;
        public int objectID;
        public string defaultName;
        public int defaultID, parentID = -2;
        public bool showDefault;

        object LocationList;

        bool barCodeScanCompleted;

        System.Windows.Forms.Timer tFillLocation, tStopBarcodeScan;
        string strbarCode = "";

        public SelectionObjectType objectType; // Location =1, Groups=2, SubGroup=3, Items=4

        public frmDDSelection(SelectionObjectType objType, string strdefaultName, int defaultID, bool showDefaultInList)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            objectType = objType;
            defaultName = strdefaultName;
            this.defaultID = defaultID;
            showDefault = showDefaultInList;
            objectID = -99999;


        }

        private void frmDDSelection_Load(object sender, EventArgs e)
        {
            barCodeScanCompleted = false;
            this.Text = this.Text + " - " + UserPref.CurVersionNo;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += Scan_KeyDown;
                this.KeyUp += Scan_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }


            UserPref Pref = UserPref.GetInstance();

            recPerPage = Pref.SelectScreen_PageSize;
            curPage = 1;

            pnlTagReads.Location = pnlObjectList.Location;
            pnlTagReads.Size = pnlObjectList.Size;

            pnlObjectList.BringToFront();
            pnlTagReads.SendToBack();

            cbPage.Visible = false;
            try
            {
                if (Pref.DDSelectionOption == 0)
                {      // Pre Load Data

                    Cursor.Current = Cursors.WaitCursor;

                    if (SelectionObjectType.Locations == objectType) // Location Search
                    {
                        try
                        {
                            GC.Collect();
                            LocationList = Locations.getLocationList(lvSelectOption);
                            LoadLocations(1, true);
                        }
                        catch(Exception _ex)
                        {
                            string er = "Exception thrown while getting locations from the database. Error: " + _ex.Message;
                            MessageBox.Show(er);
                            Logger.LogError(er);
                        }
                    }
                    else if (SelectionObjectType.Groups == objectType) // Group Search
                    {
                        AssetGroups.getGroups(0, lvSelectOption);
                    }
                    else if (SelectionObjectType.SubGroups == objectType) // Sub Group Search                            
                    {
                        AssetGroups.getGroups(parentID, lvSelectOption);
                    }

                    Cursor.Current = Cursors.Default;
                }

                if (Pref.IDInputMethod == 0 || objectType != SelectionObjectType.Locations)
                {
                    btnScan.Enabled = false;
                }

                ScanBttnInitState();

                if (showDefault)
                {
                    ListViewItem defaultItem = new ListViewItem(new string[] { defaultID.ToString(), defaultName, "" });
                    lvSelectOption.Items.Insert(0, defaultItem);
                }
            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                Logger.LogError("Loading Selection Screen -- " + ex.Message);
            }

            if (Program.BRdr != null)
            {
                Program.BRdr.notifyee = this;
                Program.BRdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
            }
            tFillLocation = new Timer();
            tFillLocation.Tick += new EventHandler(tFillLocation_Tick);
            tFillLocation.Enabled = false;

            tStopBarcodeScan = new Timer();
            tStopBarcodeScan.Tick += new EventHandler(tStopBarcodeScan_Tick);
            tStopBarcodeScan.Enabled = false;
        }

        void tStopBarcodeScan_Tick(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                /************* 17-8-2012 Bhavesh **************
                 * Check if timer is initialized and is enabled before stopping it
                 *****************  End ***********************/
                if (tStopBarcodeScan != null && tStopBarcodeScan.Enabled)
                    tStopBarcodeScan.Enabled = false;

                if (strbarCode != "")
                {
                    txtEnteredData.Text = strbarCode;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    OnBarcodeScan();

                    if (SelectionObjectType.Locations == objectType) // Location Search
                    {
                        LocationList = Locations.getLocationList(txtEnteredData.Text, true, lvSelectOption);
                        LoadLocations(1, true);
                    }
                }
            }
            catch
            {
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        bool inProcess = false;
        void tFillLocation_Tick(object sender, EventArgs e)
        {
            try
            {
                tFillLocation.Enabled = false;

                txtEnteredData.Enabled = false;
                inProcess = true;
                Cursor.Current = Cursors.WaitCursor;

                Application.DoEvents();

                string typedSoFar = Convert.ToString(txtEnteredData.Tag);
                if (typedSoFar.ToLower() == "all")
                {
                    if (objectType == SelectionObjectType.Locations) // Location Search
                    {

                        LocationList = Locations.getLocationList(lvSelectOption);
                        LoadLocations(1, true);

                    }
                }
                else
                {
                    if (objectType == SelectionObjectType.Locations) // Location Search
                    {
                        LocationList = Locations.getLocationList(typedSoFar.Trim(), false, lvSelectOption);
                        LoadLocations(1, true);

                    }

                }

                if (showDefault)
                {
                    ListViewItem defaultItem = new ListViewItem(new string[] { defaultID.ToString(), defaultName, "" });
                    lvSelectOption.Items.Insert(0, defaultItem);
                }

            }
            catch (Exception ex)
            {
                //throw ex;
                Logger.LogError("Selection Screen -- " + ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
                txtEnteredData.Enabled = true;
                txtEnteredData.Focus();
                inProcess = false;
            }
        }



        private void Scan_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (ScanBttnState() == ScnState.Running)
                    {
                        MessageBox.Show("Please stop barcode scanning process first", "Request Denied");
                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F11, false);
                    break;
                default:
                    break;
            }
        }

        private void Scan_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                // case Keys.F7:
                // case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F11, true);
                    break;
                default:
                    break;
            }
        }

        #region Scan Button routines
        enum ScnState
        {
            Idle,
            Running,
            Stopping,
            Starting,
            Started,
            Stopped
        }

        private void ScanBttnInitState()
        {
            ScnState state = ScnState.Idle;
            UserPref pref = UserPref.GetInstance();

            if (pref.IDInputMethod == 1)
                state = ScnState.Idle;
            else if (pref.IDInputMethod == 2)
                state = ScnState.Stopped;

            btnScan.Tag = state;
        }

        private ScnState ScanBttnState()
        {
            ScnState state = ScnState.Idle;

            if (btnScan.Tag is ScnState)
            {
                state = (ScnState)btnScan.Tag;
            }
            else
            {
                if (btnScan.Text.ToLower().Trim() == "scan")
                    btnScan.Tag = state = ScnState.Idle;
                else
                    btnScan.Tag = state = ScnState.Running;
            }

            return state;
        }

        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        private void ScanBttnSetState(ScnState newState)
        {
            // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BCScanForm));

            //System.ComponentModel.ComponentResourceManager resources;

            //if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            //{
            //    resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.BCScanForm));
            //}
            //else
            //{
            //    resources = new System.ComponentModel.ComponentResourceManager(typeof(BCScanForm));
            //}

            try
            {
                btnScan.Tag = newState;
                switch (newState)
                {
                    case ScnState.Idle:
                    case ScnState.Stopped:
                        btnScan.Text = "Scan";
                        btnScan.Enabled = true;

                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                        break;
                    case ScnState.Running:
                    case ScnState.Starting:
                    case ScnState.Started:
                        btnScan.Text = "Stop";
                        btnScan.Enabled = true;

                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                        break;
                    case ScnState.Stopping:
                        btnScan.Enabled = false;

                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                        break;
                }
            }
            catch
            {
            }

        }
        #endregion

        private void txtEnteredData_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
            }
            catch
            {
            }
        }

        private void txtEnteredData_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                tFillLocation.Enabled = false;

                switch (e.KeyCode)
                {
                    case Keys.Left:
                    case Keys.Right:
                    case Keys.Tab:
                    case Keys.Up:
                    case Keys.Delete:
                    case Keys.Down:
                    case Keys.ShiftKey:
                    case Keys.Shift:
                        return;
                }

                string typedSoFar;

                if (e.KeyCode == Keys.Back)
                {
                    typedSoFar = Convert.ToString(txtEnteredData.Tag);

                    if (txtEnteredData.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = txtEnteredData.Text.Trim();
                    }
                    else
                    {
                        return;
                    }

                    //if (typedSoFar.Length > 0 && typedSoFar.Length == txtEnteredData.Text.Length)
                    //{
                    //    typedSoFar = typedSoFar.Remove(typedSoFar.Length - 1, 1);
                    //}
                }
                else
                {
                    typedSoFar = Convert.ToString(txtEnteredData.Tag);
                    if (txtEnteredData.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = txtEnteredData.Text.Trim();
                    }
                    // typedSoFar = typedSoFar + Convert.ToChar(e.KeyValue);
                }

                txtEnteredData.Tag = typedSoFar;
                DataTable dtList = new DataTable();

                if (typedSoFar.Trim().Length >= 1)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    try
                    {

                        if (typedSoFar.ToLower() == "all")
                        {
                            if (objectType == SelectionObjectType.Locations) // Location Search
                            {

                                //LocationList = Locations.getLocationList(lvSelectOption);
                                //LoadLocations(1, true);

                                tFillLocation.Interval = 2000;
                                tFillLocation.Enabled = true;

                            }
                            else if (SelectionObjectType.Groups == objectType) // Group Search                            
                                AssetGroups.getGroups(0, lvSelectOption);
                            else if (SelectionObjectType.SubGroups == objectType) // Sub Group Search                            
                                AssetGroups.getGroups(parentID, lvSelectOption);


                        }
                        else
                        {
                            if (objectType == SelectionObjectType.Locations) // Location Search
                            {
                                //LocationList = Locations.getLocationList(typedSoFar.Trim(), false, lvSelectOption);
                                //LoadLocations(1, true);

                                tFillLocation.Interval = 2000;
                                tFillLocation.Enabled = true;
                            }
                            else if (SelectionObjectType.Groups == objectType) // Group Search                            
                                AssetGroups.getGroups(0, typedSoFar.Trim(), lvSelectOption);
                            else if (SelectionObjectType.SubGroups == objectType) // Sub Group Search                            
                                AssetGroups.getGroups(parentID, typedSoFar.Trim(), lvSelectOption);
                        }

                        if (showDefault && objectType != SelectionObjectType.Locations)
                        {
                            ListViewItem defaultItem = new ListViewItem(new string[] { defaultID.ToString(), defaultName, "" });
                            lvSelectOption.Items.Insert(0, defaultItem);
                        }

                    }
                    catch (Exception ex)
                    {
                        //throw ex;
                        Logger.LogError("Selection Screen -- " + ex.Message);
                    }
                    finally
                    {
                        Cursor.Current = Cursors.Default;
                    }
                }
                else
                {
                    //dtList.Columns.Add("ID_Location");
                    //dtList.Columns.Add("Name");

                    //DataRow dr = dtList.NewRow();
                    //dr["ID_Location"] = -2;
                    //dr["Name"] = "--SELECT--";
                    //dtList.Rows.Add(dr);
                    //dtList.AcceptChanges();

                    //cboLoc.ValueMember = "ID_Location";
                    //cboLoc.DisplayMember = "Name";
                    //cboLoc.DataSource = dtList;

                    //cboLoc.SelectedValue = -2;


                }
            }
            catch
            {
            }

        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                if (lvSelectOption.Items.Count > 0 && lvSelectOption.SelectedIndices.Count > 0)
                {
                    ListViewItem selItem = lvSelectOption.Items[lvSelectOption.SelectedIndices[0]];
                    if (selItem != null)
                    {
                        objectName = selItem.SubItems[1].Text;
                        objectID = Convert.ToInt32(selItem.SubItems[0].Text);
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("No object selected. Please select an object from the list.");
                }
            }
            catch (Exception ex)
            {
            }

        }

        bool actionComplete = true;

        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            if (keyCode == eVKey.VK_F11)
            {
                if (down)
                {
                    // fake 'Start' key press if not already running
                    if (ScanBttnState() == ScnState.Idle || ScanBttnState() == ScnState.Stopped)
                    {
                        if (actionComplete)
                        {
                            actionComplete = false;
                            btnScan_Click(this, null);
                            actionComplete = true;
                        }
                    }
                }
                else // up
                {
                    if (ScanBttnState() == ScnState.Running || ScanBttnState() == ScnState.Started)
                    {
                        // Stop!
                        if (actionComplete)
                        {
                            actionComplete = false;
                            btnScan_Click(this, null);
                            actionComplete = true;
                        }
                    }
                }
            }
            // ignore other keys
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {

                this.KeyDown -= Scan_KeyDown;
                this.KeyUp -= Scan_KeyUp;
            }
            else
            {
                ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            }
            this.Close();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                UserPref Pref = UserPref.GetInstance();

                lvSelectOption.Items.Clear();

                if (Pref.DDSelectionOption == 0)
                {      // Pre Load Data

                    if (SelectionObjectType.Locations == objectType) // Location Search
                    {
                        LocationList = Locations.getLocationList(lvSelectOption);
                        LoadLocations(1, true);
                    }
                    else if (SelectionObjectType.Groups == objectType) // group Search
                    {
                        AssetGroups.getGroups(0, lvSelectOption);
                    }
                    else if (SelectionObjectType.SubGroups == objectType) // Sub Group Search                            
                    {
                        AssetGroups.getGroups(parentID, lvSelectOption);
                    }
                }

                if (showDefault)
                {

                    if (lvSelectOption.Items.Count > 0)
                    {
                        if (lvSelectOption.Items[0].SubItems[0].Text != defaultID.ToString())
                        {
                            ListViewItem defaultItem = new ListViewItem(new string[] { defaultID.ToString(), defaultName, "" });
                            lvSelectOption.Items.Insert(0, defaultItem);

                        }
                    }
                    else
                    {
                        ListViewItem defaultItem = new ListViewItem(new string[] { defaultID.ToString(), defaultName, "" });
                        lvSelectOption.Items.Insert(0, defaultItem);

                    }
                }


            }
            catch
            {
            }
            finally
            {
                txtEnteredData.Text = "";
                Cursor.Current = Cursors.Default;
            }
        }

        private void OnBarcodeScan()
        {
            //Barcode reader object from Program class
            //BarCodeReader rdr;

            //BarCodeRdr Rdr = null;
            //Rdr = BarCodeFactory.GetBarCodeRdr();
            switch (ScanBttnState())
            {
                case ScnState.Idle:
                    Cursor.Current = Cursors.WaitCursor;
                    Program.RefreshStatusLabel(this, "Setting up...");
                    //Rdr = BarCodeRdr.GetInstance();
                    //rdr = BarCodeFactory.GetBarCodeRdr();

                    Program.RefreshStatusLabel(this, "Scanning...");
                    ScanBttnSetState(ScnState.Running);
                    //if (this.InvokeRequired)
                    //{
                    //    rdr.notifyee =  this;
                    //}
                    //Rdr.ScanStart(BarCodeNotify, this);
                    if (Program.barInitialized)
                    {
                        // Program.BRdr.notifyee = this;
                        //Program.BRdr.UnregisterStopNotificationEvent(ScanStopped);
                        // Program.BRdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);

                        htTagReads.Clear();
                        strbarCode = "";
                        Program.BRdr.StartReader();
                    }
                    else
                    {
                        MessageBox.Show("Barcode scanner not initialized.");
                    }
                    barCodeScanCompleted = false;
                    this.txtEnteredData.Enabled = false;
                    btnClose.Enabled = false;

                    Cursor.Current = Cursors.Default;
                    break;

                case ScnState.Running:
                    // Rdr = BarCodeRdr.GetInstance();
                    //rdr = BarCodeFactory.GetBarCodeRdr();
                    Program.RefreshStatusLabel(this, "Stopping Barcode Scan...");
                    ScanBttnSetState(ScnState.Stopping);
                    //if (this.InvokeRequired)
                    //{
                    //    rdr.stopNotifee = this;
                    //}
                    if (Program.BRdr != null)
                    {
                        //Program.BRdr.stopNotifee = this;
                        // if (Rdr.ScanTryStop(ScanStopped, this)) // use TryStop to avoid deadlock
                        //Program.BRdr.UnregisterCodeRcvdNotificationEvent(BarCodeNotify);
                        //Program.BRdr.RegisterStopNotificationEvent(ScanStopped);

                        if (Program.BRdr.StopReader())
                        {
                            Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
                            ScanBttnSetState(ScnState.Idle);

                        }
                        else
                        {
                            // Scanner busy at the moment
                            // Leave the 'Stopping' State as to return 'not-to-continue'
                            //  on the next bar-code read notification
                            // MessageBox.Show("Scanner Busy", "Stop Denied");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Barcode scanner not initialized.");
                    }
                    barCodeScanCompleted = true;

                    this.txtEnteredData.Enabled = true;
                    btnClose.Enabled = true;

                    break;
            }
        }

        private void OnRFIDScan()
        {
            Reader Rdr;
            //BarCodeRdr Rdr = null;
            //Rdr = BarCodeFactory.GetBarCodeRdr();
            switch (ScanBttnState())
            {
                case ScnState.Stopped:
                    Program.RefreshStatusLabel(this, "Setting up...");
                    //Rdr = BarCodeRdr.GetInstance();

                    Rdr = ReaderFactory.GetReader();
                    Program.RefreshStatusLabel(this, "Starting...");
                    ScanBttnSetState(ScnState.Running);
                    //if (this.InvokeRequired)
                    //{
                    //    rdr.notifyee =  this;
                    //}
                    //Rdr.ScanStart(BarCodeNotify, this);
                    Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);

                    htTagReads.Clear();

                    //Count Status Message
                    if (!Rdr.SetRepeatedTagObsrvMode(true))
                        // Continue despite error
                        Program.ShowError("Disable Repeated Tag Observation mode failed");
                    //UserPref Pref = UserPref.GetInstance();
                    //Byte[] Mask; uint MaskOffset;
                    //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                    bool Succ = Rdr.TagRangeStart();   //Rdr.TagInventoryStart(5);
                    if (!Succ)
                    {
                        ScanBttnSetState(ScnState.Stopping);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        Program.ShowError("Error: TagInventory Failed to Start\n");
                        Application.DoEvents();

                    }
                    else
                    {
                        pnlTagReads.BringToFront();
                        pnlObjectList.SendToBack();
                        btnTagSelect.Enabled = false;
                        btnClose.Enabled = false;
                    }

                    this.txtEnteredData.Enabled = false;

                    break;

                case ScnState.Started:
                    // Rdr = BarCodeRdr.GetInstance();
                    Rdr = ReaderFactory.GetReader();
                    Program.RefreshStatusLabel(this, "Stopping Scan...");
                    ScanBttnSetState(ScnState.Stopping);

                    //SetScanState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (!Rdr.TagInventoryStop())
                    {
                        //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        ScanBttnSetState(ScnState.Stopped); // give up
                        Program.RefreshStatusLabel(this, "Error Stopped...");
                        Program.ShowError("Error: TagRead Failed to Stop\n");
                        // button1.Enabled = false;
                        // btnClear.Enabled = false;

                    }
                    else
                    {
                        //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        //  Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        //Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    }

                    if (lvTagReads.Items.Count > 0)
                    {
                        btnTagSelect.Enabled = true;
                    }

                    this.txtEnteredData.Enabled = true;
                    btnClose.Enabled = true;

                    // btnScnBarcd.Enabled = true;
                    break;

            }
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            try
            {
                // Remove HotKey handler
                ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            }
            catch
            {
            }
        }

        private void ScanStopped()
        {
            Program.RefreshStatusLabel(this, "Barcode Scan stopped...");
            ScanBttnSetState(ScnState.Idle);
        }

        private bool BarCodeNotify(bool succ, String bcStr, String errMsg)
        {
            try
            {
                if (succ)
                {
                    //Program.RefreshStatusLabel(this, "Barcode Captured...");

                    /*********** 17-8-2012 Bhavesh********************
                     * Disabled searching char(29) and changing barcode string. Assign barcode string as it is and search location based on that
                     * Not sure why sing Char(29). It's control char and wont be found in barcode text
                     * Barcode will//should same as RFID TagID.
                     * 
                     * Disabled starting timer to start searching location after 500 ms. 
                     * Do not read countinuously when using barcode. 
                     * Stop reading after first successfull read and search for that location.
                     * Calling tStopBarcodeScan_Tick will stop scanning if strbarCode is not empty;
                     */

                    //int index = -1;

                    //index = bcStr.IndexOf(Convert.ToChar(29));
                    //if (index >= 0)
                    //{
                    //    strbarCode = bcStr.Remove(0, index + 1);
                    //}

                    //tStopBarcodeScan.Interval = 500;
                    //tStopBarcodeScan.Enabled = true;

                    strbarCode = bcStr;
                    tStopBarcodeScan_Tick(null, null);
                    /************* End ***************/

                    //if (this.InvokeRequired)
                    //{
                    //ProgressStatus_progressState(status);
                    //this.Invoke(new EventHandler(StopBarcodeScan), new Object[] { bcStr,null });
                    //return false;
                    //} 

                    //txtEnteredData.Text = bcStr;
                    //TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    //OnBarcodeScan();

                    //if (SelectionObjectType.Locations == objectType) // Location Search
                    //    LocationList = Locations.getLocationList(txtEnteredData.Text, true, lvSelectOption);
                }
                else
                {
                    //Program.RefreshStatusLabel(this, "Barcode Capture Error...");
                    MessageBox.Show(errMsg, "Bar Code Reader Error");
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("Barcode Notify : - " + ex.Message);
            }

            return false;
        }

        void StopBarcodeScan(object o, EventArgs e)
        {
            txtEnteredData.Text = o.ToString();
            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            OnBarcodeScan();

            if (SelectionObjectType.Locations == objectType) // Location Search
                LocationList = Locations.getLocationList(txtEnteredData.Text, true, lvSelectOption);
        }


        Hashtable htTagReads = new Hashtable(100);
        float maxrssi = 0;
        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            // Add new TagID to list (or update occurrence)

            if (e.Cnt != 0)
            {
                float rssi = Math.Abs(e.RSSI);
                //txtEnteredData.Text = e.EPC.ToString();
                ListViewItem lstItem = null;
                if (htTagReads.ContainsKey(e.EPC.ToString()))
                {
                    //string rssi = Convert.ToString(htTagReads[e.EPC.ToString()]);

                    //lstItem = new ListViewItem(new string[2] { e.EPC.ToString(), rssi });

                    int index = Convert.ToInt32(htTagReads[e.EPC.ToString()]);

                    if (index >= 0)
                    {
                        try
                        {
                            if (lvTagReads.Items[index] != null)
                            {
                                lstItem = lvTagReads.Items[index];
                                lstItem.SubItems[1].Text = rssi.ToString("F0");
                            }
                            else
                            {
                                lstItem = new ListViewItem(new string[2] { e.EPC.ToString(), rssi.ToString("F0") });
                                lvTagReads.Items.Insert(index, lstItem);
                            }

                            if (rssi >= 75)
                            {
                                lvTagReads.Items[index].BackColor = System.Drawing.Color.Green;
                            }
                            else if (rssi >= 65)
                            {
                                lvTagReads.Items[index].BackColor = System.Drawing.Color.Yellow;
                            }
                            else
                            {
                                lvTagReads.Items[index].BackColor = System.Drawing.Color.White;
                            }

                            if (rssi >= maxrssi)
                            {
                                maxrssi = rssi;
                                lvTagReads.Items.RemoveAt(index);
                                lvTagReads.Items.Insert(0, lstItem);
                            }

                        }
                        catch
                        {
                        }
                        // TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    }

                    //htTagReads[e.EPC.ToString()] = e.RSSI.ToString("F0");

                }
                else
                {
                    try
                    {
                        htTagReads.Add(e.EPC.ToString(), lvTagReads.Items.Count);
                        lstItem = new ListViewItem(new string[2] { e.EPC.ToString(), rssi.ToString("F0") });

                        if (rssi >= 75)
                        {
                            lstItem.BackColor = System.Drawing.Color.Green;
                        }
                        else if (rssi >= 65)
                        {
                            lstItem.BackColor = System.Drawing.Color.Yellow;
                        }

                        if (rssi >= maxrssi)
                        {
                            maxrssi = rssi;
                            lvTagReads.Items.Insert(0, lstItem);
                        }
                        else
                        {
                            lvTagReads.Items.Add(lstItem);
                        }

                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    }
                    catch
                    {
                    }
                }

                //lvTagReads.Refresh();

                // Application.DoEvents();

                //OnRFIDScan();
                //if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
                //{
                //    AddEPCToListV(e.PC, e.EPC);
                //    RefreshTagCntLabel();
                //    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                //    if (count >= 10)
                //    {
                //        count = 0;
                //        EPCListV.Refresh();
                //        Application.DoEvents();
                //    }
                //}

            }
        }

        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            ScnState State = ScanBttnState();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    ScanBttnSetState(ScnState.Started);
                    // Make the List Long (purpose of scanning)
                    // if (!ListIsLong())
                    //  OnLngLstButtonClicked(this, null);
                    Program.RefreshStatusLabel(this, "Scanning...");
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        // Display error or Restart Radio (if necessary)
                        //if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                        //    //RestartRfidDevice();
                        //else
                        Program.ShowError("Tag Inventory Stopped with Error: " + e.msg);
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped)
                    {
                        // ushort MacErrCode;
                        if (Rdr.GetMacError() && Reader.macerr != 0)
                        {
                            // if fatal mac error, display message
                            if (Rdr.MacErrorIsFatal() || (ScanBttnState() != ScnState.Stopping))
                                Program.ShowError(e.msg);
                        }
                        else
                            Program.ShowError("Unknown HW error. Abort");
                    }
                    // restore StartButton
                    ScanBttnSetState(ScnState.Stopped);

                    Program.RefreshStatusLabel(this, "Finished");
                    //if (ListIsLong())
                    // OnLngLstButtonClicked(this, null);
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (State)
                    {
                        case ScnState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case ScnState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case ScnState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case ScnState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    ScanBttnSetState(ScnState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
            }

            // Re-enable Field Input Textboxes (disabled during inventory operation)
            if (ScanBttnState() == ScnState.Stopped)
            {
                Cursor.Current = Cursors.Default;
                btnScan.Enabled = true;
            }
        }

        private void btnScan_Click(object sender, EventArgs e)
        {
            StartStopScanning(0);
        }

        void StartStopScanning(int _keyStatus)
        {
            if (UserPref.GetInstance().IDInputMethod == 0 || objectType != SelectionObjectType.Locations)
            {
                return;
            }
            else if (UserPref.GetInstance().IDInputMethod == 1)
            {
                if (!Program.barInitialized)
                {

                    Program.InitBarcodeReader();


                    if (!Program.barInitialized)
                    {
                        MessageBox.Show("Barcode scanner initialization error.");
                        return;
                    }
                    Program.BRdr.notifyee = this;
                    Program.BRdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
                }

                OnBarcodeScan();
            }
            else if (UserPref.GetInstance().IDInputMethod == 2)
            {
                OnRFIDScan();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            lvTagReads.Items.Clear();
            htTagReads.Clear();

            pnlObjectList.BringToFront();
            pnlTagReads.SendToBack();
        }

        private void btnTagSelect_Click(object sender, EventArgs e)
        {
            if (lvTagReads.Items.Count > 0)
            {
                if (lvTagReads.SelectedIndices.Count <= 0)
                {
                    MessageBox.Show("Please select Tag ID.");
                }
                else
                {

                    ListViewItem selItem = lvTagReads.Items[lvTagReads.SelectedIndices[0]];
                    txtEnteredData.Text = selItem.SubItems[0].Text;
                    lvTagReads.Items.Clear();

                    pnlObjectList.BringToFront();
                    if (SelectionObjectType.Locations == objectType) // Location Search
                    {
                        LocationList = Locations.getLocationList(txtEnteredData.Text, true, lvSelectOption);
                        LoadLocations(1, true);
                    }
                }
            }
        }

        private void frmDDSelection_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                e.Cancel = false;

                if (ScanBttnState() == ScnState.Running || ScanBttnState() == ScnState.Started)
                {
                    MessageBox.Show("Please stop the scan operation first");
                    e.Cancel = true;
                }
                else
                {
                    if (UserPref.GetInstance().DDSelectionOption == 0)
                    {

                        if (SelectionObjectType.Locations == objectType)
                        {
                            GC.Collect();
                        }
                    }
                }
                if (Program.BRdr != null)
                {
                    Program.BRdr.UnregisterCodeRcvdNotificationEvent(BarCodeNotify);
                }
            }
            catch
            {
            }
        }

        DataTable dtPages;

        int curPage, totalPages;
        public int recPerPage;

        private int SetupPaging(int totalRec)
        {
            //int totalRec = EPCListV.Items.Count;

            if (totalRec <= recPerPage)
            {
                cbPage.Visible = false;
                return 0;
            }

            int totalPages = 0;

            dtPages = new DataTable();
            DataColumn dc = new DataColumn("Page");
            dtPages.Columns.Add(dc);

            DataColumn dcc = new DataColumn("Index");
            dtPages.Columns.Add(dcc);

            if (totalRec > 0)
            {
                totalPages = totalRec / recPerPage;
                if (totalRec % recPerPage != 0)
                    totalPages = totalPages + 1;
            }

            //  cmbPage.Items.Clear();

            DataRow dr;

            //dr = dtPages.NewRow();
            //dr["Page"] = "Select";
            //dr["Index"] = 0;
            //dtPages.Rows.Add(dr);

            for (int i = 1; i <= totalPages; i++)
            {
                dr = dtPages.NewRow();
                dr["Page"] = "Page" + i.ToString();
                dr["Index"] = i;
                dtPages.Rows.Add(dr);
            }

            dtPages.AcceptChanges();

            cbPage.DataSource = dtPages;
            cbPage.DisplayMember = "Page";
            cbPage.ValueMember = "Index";

            if (totalPages > 0)
                cbPage.SelectedValue = 1;

            cbPage.Visible = true;
            // lblPage.Visible = true;
            cbPage.SelectedValueChanged += new EventHandler(cbPage_SelectedValueChanged);

            return totalPages;
        }

        void cbPage_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (curPage == Convert.ToInt32(cbPage.SelectedValue))
                    return;
                curPage = Convert.ToInt32(cbPage.SelectedValue);

                LoadLocations(curPage, false);


            }
            catch
            {
            }
        }

        void LoadLocations(int pageNo, bool setPages)
        {

            int start, end;

            try
            {
                Cursor.Current = Cursors.WaitCursor;


                if (LocationList is Array)
                {
                    DataRow[] rows = LocationList as DataRow[];

                    if (rows != null)
                    {
                        if (rows.Length > recPerPage && recPerPage > 0)
                        {
                            start = recPerPage * (pageNo - 1);
                            end = recPerPage * pageNo;

                            if (end > rows.Length)
                                end = rows.Length;

                            if (setPages)
                            {
                                SetupPaging(rows.Length);
                            }

                            cbPage.Visible = true;
                        }
                        else
                        {
                            start = 0;
                            end = rows.Length;
                            cbPage.Visible = false;
                        }

                        lvSelectOption.Items.Clear();

                        DataRow dr;

                        string[] subitems = new string[3];

                        for (int i = start; i < end; i++)
                        {
                            dr = rows[i];
                            subitems[0] = dr["ServerKey"].ToString();
                            subitems[1] = dr["Name"].ToString();
                            subitems[2] = dr["TagID"].ToString();

                            lvSelectOption.Items.Add(new ListViewItem(subitems));

                        }
                    }

                }
                else if (LocationList is DataTable)
                {
                    DataTable dtLocations = LocationList as DataTable;
                    if (dtLocations != null)
                    {
                        if (dtLocations.Rows.Count > recPerPage && recPerPage > 0)
                        {
                            start = recPerPage * (pageNo - 1);
                            end = recPerPage * pageNo;

                            if (end > dtLocations.Rows.Count)
                                end = dtLocations.Rows.Count;

                            if (setPages)
                            {
                                SetupPaging(dtLocations.Rows.Count);
                            }

                            cbPage.Visible = true;
                        }
                        else
                        {
                            start = 0;
                            end = dtLocations.Rows.Count;

                            cbPage.Visible = false;
                        }

                        lvSelectOption.Items.Clear();

                        DataRow dr;

                        string[] subitems = new string[3];

                        string id = "ID_Location";

                        UserPref pref = UserPref.GetInstance();

                        if (!Login.OnLineMode && pref.UsingNewDBLib)
                        {
                            id = "ServerKey";
                        }

                        for (int i = start; i < end; i++)
                        {
                            dr = dtLocations.Rows[i];
                            subitems[0] = dr[id].ToString();
                            subitems[1] = dr["Name"].ToString();
                            subitems[2] = dr["TagID"].ToString();

                            lvSelectOption.Items.Add(new ListViewItem(subitems));

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LogError("Selection Screen (Load Locations) -- " + ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }


        }

    }

    public enum SelectionObjectType
    {
        Locations = 1,
        Items = 2,
        Groups = 3,
        SubGroups = 4
    }
}