
namespace OnRamp
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.LPmptLabel = new System.Windows.Forms.Label();
            this.PPmptLabel = new System.Windows.Forms.Label();
            this.UsrNameTxtBx = new System.Windows.Forms.TextBox();
            this.PwdTxtBx = new System.Windows.Forms.TextBox();
            this.SkipButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chkOnline = new System.Windows.Forms.CheckBox();
            this.chkRegistration = new System.Windows.Forms.CheckBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.chkUseInMemoryMode = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // LPmptLabel
            // 
            resources.ApplyResources(this.LPmptLabel, "LPmptLabel");
            this.LPmptLabel.Name = "LPmptLabel";
            // 
            // PPmptLabel
            // 
            resources.ApplyResources(this.PPmptLabel, "PPmptLabel");
            this.PPmptLabel.Name = "PPmptLabel";
            // 
            // UsrNameTxtBx
            // 
            resources.ApplyResources(this.UsrNameTxtBx, "UsrNameTxtBx");
            this.UsrNameTxtBx.Name = "UsrNameTxtBx";
            this.UsrNameTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnUsrNameTxtBxKeyPress);
            // 
            // PwdTxtBx
            // 
            resources.ApplyResources(this.PwdTxtBx, "PwdTxtBx");
            this.PwdTxtBx.Name = "PwdTxtBx";
            this.PwdTxtBx.TextChanged += new System.EventHandler(this.PwdTxtBx_TextChanged);
            this.PwdTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPwdTxtBxKeyPress);
            // 
            // SkipButton
            // 
            resources.ApplyResources(this.SkipButton, "SkipButton");
            this.SkipButton.Name = "SkipButton";
            this.SkipButton.Click += new System.EventHandler(this.SkipButtonClicked);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.Click += new System.EventHandler(this.OnOKButtonClicked);
            // 
            // chkOnline
            // 
            resources.ApplyResources(this.chkOnline, "chkOnline");
            this.chkOnline.Name = "chkOnline";
            // 
            // chkRegistration
            // 
            resources.ApplyResources(this.chkRegistration, "chkRegistration");
            this.chkRegistration.Name = "chkRegistration";
            // 
            // lblVersion
            // 
            resources.ApplyResources(this.lblVersion, "lblVersion");
            this.lblVersion.Name = "lblVersion";
            // 
            // chkUseInMemoryMode
            // 
            resources.ApplyResources(this.chkUseInMemoryMode, "chkUseInMemoryMode");
            this.chkUseInMemoryMode.Name = "chkUseInMemoryMode";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.ControlBox = false;
            this.Controls.Add(this.chkUseInMemoryMode);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.chkRegistration);
            this.Controls.Add(this.chkOnline);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SkipButton);
            this.Controls.Add(this.PwdTxtBx);
            this.Controls.Add(this.UsrNameTxtBx);
            this.Controls.Add(this.PPmptLabel);
            this.Controls.Add(this.LPmptLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.Closed += new System.EventHandler(this.LoginForm_Closed);
            this.ResumeLayout(false);

        } 

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.LoginForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.LPmptLabel = new System.Windows.Forms.Label();
            this.PPmptLabel = new System.Windows.Forms.Label();
            this.UsrNameTxtBx = new System.Windows.Forms.TextBox();
            this.PwdTxtBx = new System.Windows.Forms.TextBox();
            this.SkipButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.chkOnline = new System.Windows.Forms.CheckBox();
            this.chkRegistration = new System.Windows.Forms.CheckBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.chkUseInMemoryMode = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // LPmptLabel
            // 
            resources.ApplyResources(this.LPmptLabel, "LPmptLabel");
            this.LPmptLabel.Name = "LPmptLabel";
            // 
            // PPmptLabel
            // 
            resources.ApplyResources(this.PPmptLabel, "PPmptLabel");
            this.PPmptLabel.Name = "PPmptLabel";
            // 
            // UsrNameTxtBx
            // 
            resources.ApplyResources(this.UsrNameTxtBx, "UsrNameTxtBx");
            this.UsrNameTxtBx.Name = "UsrNameTxtBx";
            this.UsrNameTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnUsrNameTxtBxKeyPress);
            // 
            // PwdTxtBx
            // 
            resources.ApplyResources(this.PwdTxtBx, "PwdTxtBx");
            this.PwdTxtBx.Name = "PwdTxtBx";
            this.PwdTxtBx.TextChanged += new System.EventHandler(this.PwdTxtBx_TextChanged);
            this.PwdTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPwdTxtBxKeyPress);
            this.PwdTxtBx.MaxLength = 50;
            // 
            // SkipButton
            // 
            resources.ApplyResources(this.SkipButton, "SkipButton");
            this.SkipButton.Name = "SkipButton";
            this.SkipButton.Click += new System.EventHandler(this.SkipButtonClicked);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.Click += new System.EventHandler(this.OnOKButtonClicked);
            // 
            // chkOnline
            // 
            resources.ApplyResources(this.chkOnline, "chkOnline");
            this.chkOnline.Name = "chkOnline";
            // 
            // chkRegistration
            // 
            resources.ApplyResources(this.chkRegistration, "chkRegistration");
            this.chkRegistration.Name = "chkRegistration";
            // 
            // lblVersion
            // 
            resources.ApplyResources(this.lblVersion, "lblVersion");
            this.lblVersion.Name = "lblVersion";

            // 
            // chkUseInMemoryMode
            // 
            this.chkUseInMemoryMode.Checked = false;
            this.chkUseInMemoryMode.CheckState = System.Windows.Forms.CheckState.Unchecked;
            resources.ApplyResources(this.chkUseInMemoryMode, "chkUseInMemoryMode");
            this.chkUseInMemoryMode.Name = "chkUseInMemoryMode";
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.ControlBox = false;
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.chkUseInMemoryMode);
            this.Controls.Add(this.chkRegistration);
            this.Controls.Add(this.chkOnline);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SkipButton);
            this.Controls.Add(this.PwdTxtBx);
            this.Controls.Add(this.UsrNameTxtBx);
            this.Controls.Add(this.PPmptLabel);
            this.Controls.Add(this.LPmptLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoginForm";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.Closed += new System.EventHandler(this.LoginForm_Closed);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LPmptLabel;
        private System.Windows.Forms.Label PPmptLabel;
        private System.Windows.Forms.TextBox UsrNameTxtBx;
        private System.Windows.Forms.TextBox PwdTxtBx;
        private System.Windows.Forms.Button SkipButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkOnline;
        private System.Windows.Forms.CheckBox chkRegistration;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.CheckBox chkUseInMemoryMode;
    }
}

