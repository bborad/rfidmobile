﻿namespace OnRamp
{
    partial class frmHardwareSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdBtnCS101 = new System.Windows.Forms.RadioButton();
            this.rdBtnAT870 = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.rdBtnAT870);
            this.panel1.Controls.Add(this.rdBtnCS101);
            this.panel1.Location = new System.Drawing.Point(100, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 57);
            // 
            // rdBtnCS101
            // 
            this.rdBtnCS101.Location = new System.Drawing.Point(3, 5);
            this.rdBtnCS101.Name = "rdBtnCS101";
            this.rdBtnCS101.Size = new System.Drawing.Size(110, 20);
            this.rdBtnCS101.TabIndex = 0;
            this.rdBtnCS101.Text = "CS101Reader";
            // 
            // rdBtnAT870
            // 
            this.rdBtnAT870.Location = new System.Drawing.Point(3, 29);
            this.rdBtnAT870.Name = "rdBtnAT870";
            this.rdBtnAT870.Size = new System.Drawing.Size(110, 20);
            this.rdBtnAT870.TabIndex = 1;
            this.rdBtnAT870.Text = "AT870Reader";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(65, 129);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(72, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(73, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 20);
            this.label1.Text = "Select Reader Hardware";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(164, 129);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmHardwareSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(318, 195);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmHardwareSelection";
            this.Text = "Hardware Selection";
            this.Load += new System.EventHandler(this.frmHardwareSelection_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdBtnAT870 = new System.Windows.Forms.RadioButton();
            this.rdBtnCS101 = new System.Windows.Forms.RadioButton();
            this.btnOk = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.rdBtnAT870);
            this.panel1.Controls.Add(this.rdBtnCS101);
            this.panel1.Location = new System.Drawing.Point(61, 86);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(118, 57);
            // 
            // rdBtnAT870
            // 
            this.rdBtnAT870.Location = new System.Drawing.Point(3, 29);
            this.rdBtnAT870.Name = "rdBtnAT870";
            this.rdBtnAT870.Size = new System.Drawing.Size(110, 20);
            this.rdBtnAT870.TabIndex = 1;
            this.rdBtnAT870.Text = "AT870Reader";
            // 
            // rdBtnCS101
            // 
            this.rdBtnCS101.Location = new System.Drawing.Point(3, 5);
            this.rdBtnCS101.Name = "rdBtnCS101";
            this.rdBtnCS101.Size = new System.Drawing.Size(110, 20);
            this.rdBtnCS101.TabIndex = 0;
            this.rdBtnCS101.Text = "CS101Reader";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(26, 159);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(72, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular);
            this.label1.Location = new System.Drawing.Point(34, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 20);
            this.label1.Text = "Select Reader Hardware";
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(125, 159);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(72, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmHardwareSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmHardwareSelection";
            this.Text = "Hardware Selection";
            this.Load += new System.EventHandler(this.frmHardwareSelection_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdBtnAT870;
        private System.Windows.Forms.RadioButton rdBtnCS101;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCancel;
    }
}