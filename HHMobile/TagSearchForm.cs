using System;using HHDeviceInterface.RFIDSp; 
using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ClsRampdb;
using ClsReaderLib;using ClsReaderLib.Devices;

using ClsLibBKLogs;

namespace OnRamp
{
    public partial class TagSearchForm : Form
    {
        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion
        #region Button State Enum
        enum OpState
        {
            Idle,
            SettingUp,
            Starting,
            Running,
            Stopping
        }
        #endregion

        private string _SearchTagNo;
        public string SearchTagNo
        {
            set
            {
                _SearchTagNo = value; 
            }
        }

        private string _AssetNo;
        public string AssetNo
        {
            set
            {
                _AssetNo = value;
            }
        }

        private string _AssetName;
        public string AssetName
        {
            set
            {
                _AssetName = value;
            }
        }

        public string callFrom;

        public bool changeLoc;

        const int NumThresholdLvls = 8;

        static float[] RateLvlThresholds = new float[NumThresholdLvls]
            {
                0.0F,
                5.0F, // slow
                10.0F,
                15.0F,
                20.0F, // medium
                25.0F,
                30.0F, // fast
                35.0F,
            };

#if USE_WBRSSI
        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
            {
                0.0F, // within read range
                100.0F, // low
                100.0F, // 
                100.0F,
                110.0F, // medium
                110.0F,
                110.0F, // 
                120.0F, // high
            };
#else
        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
            {
                0.0F, // within read range
                50.0F, // low
                60.0F, // 
                65.0F,
                70.0F, // medium
                75.0F,
                80.0F, // 
                90.0F, // high
            };
#endif

        static uint[] PulsePeriods = new uint[NumThresholdLvls]
            {
                1000,
                1000,
                1000,
                600,
                400,
                200,
                100,
                100,
            };

        static uint[] LedRadius = new uint[NumThresholdLvls]
            {
                20,
                20,
                20,
                30,
                40,
                50,
                60,
                60,
            };

        static uint[] BuzzerPitch = new uint[NumThresholdLvls]
            {
                200,
                200,
                200,
                400,
                800,
                1600,
                3200,
                3200,
            };
        static RingTone[] RingMelody = new RingTone[NumThresholdLvls]
            {
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T3,
                RingTone.T4,
                RingTone.T5,
                RingTone.T5,
            };

        #region GraphicalLed
        class GraphicalLed
        {
            private Rectangle LedBx;
            private SolidBrush brush;
            private Pen pen;
            private Graphics g;
            private System.Windows.Forms.Timer blinkTmr;
            private System.Windows.Forms.Timer onOffTmr;
            private bool fillState = false;
            private int radius = 10;
            private int buzzerPitch = 200;
            private RingTone ringMelody = RingTone.T1;

            public GraphicalLed(Form surface, Point location)
            {
                LedBx = new Rectangle(location.X - 10, location.Y - 10,
                    radius * 2 , radius * 2);
                pen = new Pen(Color.Green);
                brush = new SolidBrush(Color.Green);
                g = surface.CreateGraphics();
                blinkTmr = new System.Windows.Forms.Timer();
                blinkTmr.Interval = 100; // The Blink pulse
                blinkTmr.Tick += BlinkTmrTick;
                blinkTmr.Enabled = false;
                onOffTmr = new System.Windows.Forms.Timer();
                onOffTmr.Tick += OnOffTmrTick;
                onOffTmr.Enabled = false;

                surface.Paint += new PaintEventHandler(OnPaintReq);
            }

            public int BuzzerPitch
            {
                set
                {
                    if (value > 0)
                        buzzerPitch = value;
                }
            }

            public RingTone RingMelody
            {
                set
                {
                    ringMelody = value;
                }
            }

            public uint Radius
            {
                set
                {
                    if (value != radius)
                    {
                        if (value < radius)
                        {
                            UndrawLed();
                        }
                        LedBx.X += (int)(radius - value);
                        LedBx.Y += (int)(radius - value);
                        radius = (int)value;
                        LedBx.Width = radius * 2;
                        LedBx.Height = radius * 2;
                    }
                }
            }

            void DrawFilled()
            {
                brush.Color = Color.Green;
                g.FillEllipse(brush, LedBx);
            }

            void UndrawLed()
            {
                brush.Color = Color.White;
                Rectangle LedBxInclBorder = new Rectangle(LedBx.X - 1, LedBx.Y - 1,
                    LedBx.Width + 2, LedBx.Height + 2);
                g.FillEllipse(brush, LedBx);
            }

            void DrawLed()
            {
                brush.Color = Color.White;
                g.FillEllipse(brush, LedBx);
                brush.Color = Color.Green;
                g.DrawEllipse(pen, this.LedBx);
            }

            void  OnPaintReq(object sender, PaintEventArgs e)
            {
                if (e.ClipRectangle.IntersectsWith(LedBx))
                {
                    if (fillState == true)
                        DrawFilled();
                    else
                        DrawLed();
                }
            }

            public bool Visible
            {
                set
                {
                    if (value == true)
                    {
                        if (fillState == true)
                            DrawFilled();
                        else
                            DrawLed();
                    }
                    else
                    {
                        brush.Color = Color.White;
                        g.FillEllipse(brush, LedBx);
                    }
                }
            }

            private void LedOn()
            {
                if (fillState == false)
                {
                    DrawFilled();
                    fillState = true;
                }
            }

            private void LedOff()
            {
                if (fillState == true)
                {
                    DrawLed();
                    fillState = false;
                }
            }

            public void StaysOn()
            {
                onOffTmr.Enabled = false;
                LedOn();
            }

            public void StaysOff()
            {
                onOffTmr.Enabled = false;
                LedOff();
            }

            public void BlinkStart(uint interval)
            {
                if (interval < blinkTmr.Interval)
                {
                    throw new ApplicationException("OnOff interval could not be smaller than Blink-Interval");
                }
                if (onOffTmr.Interval != (int)interval)
                    onOffTmr.Interval = (int)interval; // millisec
                if (onOffTmr.Enabled == false)
                    onOffTmr.Enabled = true;
                // now!
                LedOn();
               // RFIDRdr Rdr = RFIDRdr.GetInstance();
                //Reader Rdr = ReaderFactory.GetReader();
                Reader Rdr = ReaderFactory.GetReader();
#if DEBUG // to annoy ppl around me less
//                Rdr.BuzzerBeep(buzzerPitch, UserPref.GetInstance().SndVol);
                Rdr.RingingID = ringMelody;
                Rdr.Svol =  UserPref.GetInstance().SndVol;
                Rdr.MelodyRing();
#else
                Rdr.frequency = buzzerPitch;
                Rdr.Svol = SoundVol.High;
                Rdr.BuzzerBeep(2); // always max. volume
                Rdr.RingingID = ringMelody;
                Rdr.MelodyRing();// always max. volume
#endif
            }

            private long LastTagHitTick = 0;
            public DateTime LastTagHit
            {
                set
                {
                    LastTagHitTick = value.Ticks;
                }
            }

            private bool TagPossiblyOutOfRange()
            {
                return (((DateTime.Now.Ticks - LastTagHitTick) / TimeSpan.TicksPerSecond) > 1);
            }

            private void OnOffTmrTick(object sender, EventArgs e)
            {
                if (TagPossiblyOutOfRange())
                {
                    StaysOff();
                    UndrawLed();
                }
                else
                {
                    //LedOn();
                    //RFIDRdr Rdr = RFIDRdr.GetInstance();
                    Reader Rdr = ReaderFactory.GetReader();
#if DEBUG // to annoy ppl around me less
                    //Rdr.BuzzerBeep(buzzerPitch, UserPref.GetInstance().SndVol);
                    Rdr.RingingID =ringMelody;
                    Rdr.Svol=UserPref.GetInstance().SndVol;
                    Rdr.MelodyRing();
#else
                    Rdr.frequency = buzzerPitch;
                    Rdr.Svol = SoundVol.High;
                    Rdr.BuzzerBeep(2); // always max. volume
                    Rdr.RingingID = ringMelody;
                    Rdr.MelodyRing();// always max. volume
#endif
                    blinkTmr.Enabled = true;
                }
            }

            private void BlinkTmrTick(object sender, EventArgs e)
            {
                //LedOff();
                blinkTmr.Enabled = false;
            }
        }
        #endregion

        #region
        float borderW = 25.0F;
        Rectangle borderRect;
        Rectangle[] borderStrips;
        
        private void InitBorderStrips()
        {
            int FormTitleHt = 20; //?
            int BorderW = Convert.ToInt32(borderW);
            borderRect = new Rectangle(BorderW/2, BorderW/2, 
                this.Width - BorderW, this.Height - BorderW - FormTitleHt);
            borderStrips = new Rectangle[4];
            borderStrips[0] = new Rectangle(0, 0, this.Width, BorderW); // top
            borderStrips[1] = new Rectangle(0, this.Height - BorderW - FormTitleHt, this.Width, BorderW); // bottom
            borderStrips[2] = new Rectangle(0, BorderW, BorderW, this.Height - (BorderW * 2) - FormTitleHt); // left
            borderStrips[3] = new Rectangle(this.Width - BorderW, BorderW, BorderW, this.Height - (BorderW * 2) - FormTitleHt); // right
        }

        private void FlashBorder()
        {
            if (FlshBrdrTmr.Enabled == true) // last blink not off yet, ignore
                return;

            Graphics g = this.CreateGraphics();

            // Draw Border
            Pen DrawPen = new Pen(Color.Green, borderW);
            g.DrawRectangle(DrawPen, borderRect);
            g.Dispose(); // too much overhead?

            FlshBrdrTmr.Enabled = true;
            
        }

        private void OnFlshBrdrTmrTick(object sender, EventArgs e)
        {
            FlshBrdrTmr.Enabled = false; // one-shot

            // Undraw Border
            for (int i = 0; i < borderStrips.Length; i++)
                this.Invalidate(borderStrips[i]);
        }

        #endregion

        public TagSearchForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            SrchBttnInitState();

            // Initialize PC field to 3000 (full mask seems to get better result (regardless of Q-Algo)
            MaskInput.SetPC(true, new Byte[2] { 0x30, 0x00 });

            InitBorderStrips();

#if USE_WBRSSI
            // WbRSSI
            progressBar1.Maximum = 130;
            progressBar1.Minimum = 100; 
#endif
        }


        #region Button State Routines

        private void SrchBttnInitState()
        {
            OpState State = OpState.Idle;

            SrchBttn.Tag = State;
        }

        private OpState SrchBttnState()
        {
            OpState CurState = OpState.Idle;
            if (SrchBttn.Tag is OpState)
                CurState = (OpState)SrchBttn.Tag;
            else
                MessageBox.Show("Programming Error: SrchBttn Tag is not of OpState type");
            return CurState;
        }

        private void SrchBttnSetState(OpState newState)
        {
            SrchBttn.Tag = newState;
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagSearchForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagSearchForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagSearchForm));
            }

            switch (newState)
            {
                case OpState.Idle:
                    SrchBttn.Text = resources.GetString("SrchBttn.Text");
                    //if (Login.appModules != AppModules.RemoveFieldService && Login.appModules != AppModules.RemoveDispatchandFS)

                    if (callFrom == "frmInventory")
                    {
                        btnTask.Visible = false;
                        btnChngLoc.Visible = true;
                    }
                    else if(!AppModules.RemoveFieldService)  
                    {
                        btnTask.Visible = true;
                        btnChngLoc.Visible = false;
                    }
                    SrchBttn.Enabled = true;
                    SrchByRateBttn.Enabled = true;
                    SrchByRSSIBttn.Enabled = true;
                    MaskInput.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
                case OpState.SettingUp:
                case OpState.Starting:
                    btnChngLoc.Visible = false;
                    MaskInput.Enabled = false;
                    SrchBttn.Enabled = false;
                    SrchByRateBttn.Enabled = false;
                    SrchByRSSIBttn.Enabled = false;
                    RssiLbl.Text = resources.GetString("RssiLbl.Text");
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Stopping:
                    SrchBttn.Enabled = false;
                    MaskInput.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Running:
                    SrchBttn.Text = "Stop";
                    SrchBttn.Enabled = true;
                    btnTask.Visible = false;  
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
            }
        }
        #endregion

        #region Search common utility Routines
        // offset in word
        private bool RateSrchValidateInput(out Byte[] mask, out UInt16 wdCnt, out UInt16 wdOffset)
        {
            mask = null; wdOffset = 0; wdCnt = 0;

            Byte[] PCMask;
            bool PCMaskSet = false;
            Byte[] EPCMask;
            bool EPCMaskSet = false;
            PCMaskSet = MaskInput.GetPC(out PCMask);
            EPCMaskSet = MaskInput.GetEPC(out EPCMask);

            // if neither checkbox is checked, error
            if (PCMaskSet == false && EPCMaskSet == false)
            {
                MessageBox.Show("At least PC or EPC must be set", "Invalid Input");
                return false;
            }

            UInt16 NumPCWds = 0, NumEPCWds = 0;
            // if textbox empty, error
            if (PCMaskSet)
            {
                if (PCMask == null || PCMask.Length == 0)
                {
                    MessageBox.Show("PC cannot be empty", "Invalid Input");
                    return false;
                }
                else
                {
                    wdOffset = 1; // CRC
                    NumPCWds = (ushort)(PCMask.Length / 2);
                }
            }
            else
                wdOffset = 2; // CRC + PC

            if (EPCMaskSet)
            {
                if (EPCMask == null || EPCMask.Length == 0)
                {
                    MessageBox.Show("EPC cannot be empty", "Invalid Input");
                    return false;
                }
                else
                {
                    NumEPCWds = (ushort)(EPCMask.Length / 2);
                }
            }

            
            if (NumEPCWds > 0)
            {
                // PC(?) + EPC
                if (NumPCWds > 0)
                {
                    wdCnt = (ushort)(1 + NumEPCWds);
                }
                else
                {
                    wdCnt = NumEPCWds;
                }
            }
            else
            {
                wdCnt = 1;
            }
            mask = new Byte[wdCnt * 2];
            if (NumPCWds > 0)
            {
                Array.Copy(PCMask, 0, mask, 0, NumPCWds * 2);
            }
            if (NumEPCWds > 0)
            {
                Array.Copy(EPCMask, 0, mask, NumPCWds * 2, NumEPCWds * 2);
            }

            return true; // if it comes this far
        }
        #endregion

        #region Search-by-Tag-Rate Routines
        // Relies on Tag Inventory
        private void TagRateInvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState CurState = SrchBttnState();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    Program.RefreshStatusLabel(this, "Searching...");
                    SrchBttnSetState(OpState.Running);
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    StopOORDetectTmr();
                    RssiLbl.Text = String.Empty;
                    //Rdr.TagRateEvent -= TagRateEvtHandler;
                    //Rdr.InventoryOpStEvent -= TagRateInvtryOpEvtHandler;
                    Rdr.UnregisterTagRateEvent(TagRateEvtHandler);
                    Rdr.UnregisterInventoryOpStEvent(TagRateInvtryOpEvtHandler);
                    // restore Search Button
                    switch (e.status)
                    {
                        case InvtryOpStatus.stopped:
                            Program.RefreshStatusLabel(this, "Ready");
                            // Restore to Compact Mode
                            if (!Rdr.SetResponseDataMode(1))
                                MessageBox.Show("Unable to Restore Data Mode back to Compact");
                                break;
                        case InvtryOpStatus.errorStopped:
                            Program.RefreshStatusLabel(this, "Error Stopped");
                            if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                                RadioNotPresentStopNotice();
                            else
                                MessageBox.Show(e.msg, "Tag Search Stopped with Error");
                            break;
                        case InvtryOpStatus.macErrorStopped:
                            Program.RefreshStatusLabel(this, "HW Error Stopped");
                           // ushort MacErrCode;

                            if (Rdr.GetMacError() && Reader.macerr != 0)
                            {

                                // Show error if fatal
                                if (Rdr.MacErrorIsFatal())
                                {
                                    Program.ShowError(e.msg);
                                }
                                // Show message if non-fatal and non-user-stop
                                else if (SrchBttnState() != OpState.Stopping && SrchBttnState() != OpState.Idle)
                                {
                                    Program.ShowError(e.msg);
                                }
                            }
                            else
                                Program.ShowError("Unknown hardware (mac) error");
                            break;
                    }
                    // Restore to Compact Mode
                    if (!Rdr.SetResponseDataMode(1))
                        MessageBox.Show("Unable to Restore Data Mode back to Compact");
                    SrchBttnSetState(OpState.Idle);
                    break;
                case InvtryOpStatus.error:
                    switch (CurState)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.TagRateEvent -= TagRateEvtHandler;
                            //Rdr.InventoryOpStEvent -= TagRateInvtryOpEvtHandler;
                            Rdr.UnregisterTagRateEvent(TagRateEvtHandler);
                            Rdr.UnregisterInventoryOpStEvent(TagRateInvtryOpEvtHandler);
                            break;
                        case OpState.Running:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Search Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Search Stop Error");
                            //Rdr.TagRateEvent -= TagRateEvtHandler;
                            //Rdr.InventoryOpStEvent -= TagRateInvtryOpEvtHandler;
                            Rdr.UnregisterTagRateEvent(TagRateEvtHandler);
                            Rdr.UnregisterInventoryOpStEvent(TagRateInvtryOpEvtHandler);
                            break;
                        case OpState.Idle:
                            throw new ApplicationException("Unexpected error return during Idle State");
                    }
                    SrchBttnSetState(OpState.Idle);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
            }
        }

        // Rate information updates
        //bool firstTag = true;
        private void TagRateEvtHandler(object sender, TagRateEventArgs e)
        {
            try
            {
                float rate = e.TagCnt * 1000F / e.Period_ms;
                RssiLbl.Text = rate.ToString("F0"); // 0 decimal places
                RssiLbl.Refresh(); // must show to the user immediately

                for (int i = 0; i < NumThresholdLvls; i++)
                {
                    if (rate > RateLvlThresholds[i]) // use ">" so not to include '0'
                    {
                        FlashBorder();
#if DEBUG
                        Reader Rdr = ReaderFactory.GetReader();
                        Rdr.RingingID = RingMelody[i];
                        Rdr.Svol = UserPref.GetInstance().SndVol;
                        Rdr.MelodyRing();
                        //RFIDRdr.GetInstance().MelodyRing(RingMelody[i], UserPref.GetInstance().SndVol);
#else
                        Reader rdr = ReaderFactory.GetReader();
                      //  rdr.MelodyRing(RingMelody[i], SoundVol.High);

                      
                        rdr.Svol = SoundVol.High;                     
                        rdr.RingingID = RingMelody[i];
                        rdr.MelodyRing();// always max. volume

#endif
                        break;
                    }
                }
                if (rate <= RateLvlThresholds[0])
                    RssiLbl.Text = String.Empty;
                else
                    RestartOORDetectTmr();
#if false // Testing Only
                if (firstTag)
                {
                 Reader rdr = ReaderFactory.GetReader();
                    rdr.TagInvtryRdRateStop();
                    firstTag = false;
                }
#endif
            }
            catch (ArithmeticException)
            {
                RssiLbl.Text = "Arithmetic Exception caught";
                RssiLbl.Refresh(); // must show to the user immediately
            }
            catch
            {
                RssiLbl.Text = "Exception caught";
                RssiLbl.Refresh(); // must show to the user immediately
            }
        }

        #endregion

        #region OORDetectTmr Routines
        void RestartOORDetectTmr()
        {
            OORDetectTmr.Enabled = false;
            OORDetectTmr.Enabled = true; // OK?
        }

        void StopOORDetectTmr()
        {
            OORDetectTmr.Enabled = false;
        }

        private void OnOORDetectTmrTick(object sender, EventArgs e)
        {
            StopOORDetectTmr();

            RssiLbl.Text = String.Empty;

        }

        #endregion

        #region Search-by-RSSI Routines
        // Relies on Tag Inventory
        private void TagRssiInvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState CurState = SrchBttnState();
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();

            switch (e.status)
            {
                case InvtryOpStatus.started:
                    Program.RefreshStatusLabel(this, "Searching...");
                    SrchBttnSetState(OpState.Running);
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    StopOORDetectTmr();
                    RssiLbl.Text = String.Empty;
                    //Rdr.TagRssiEvent -= TagRssiEvtHandler;
                    //Rdr.InventoryOpStEvent -= TagRssiInvtryOpEvtHandler;
                    Rdr.UnregisterTagRssiEvent(TagRssiEvtHandler);
                    Rdr.UnregisterInventoryOpStEvent(TagRssiInvtryOpEvtHandler);
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            MessageBox.Show(e.msg, "Tag Search Stopped with Error");
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped)
                    {
                        //ushort MacErrCode;
                        if (Rdr.GetMacError() && Reader.macerr != 0)
                        {
                            // Show error if fatal
                            if (Rdr.MacErrorIsFatal())
                            {
                                Program.ShowError(e.msg);
                            }
                            // Show message if non-fatal and non-user-stop
                            else if (SrchBttnState() != OpState.Stopping && SrchBttnState() != OpState.Idle)
                            {
                                Program.ShowError(e.msg);
                            }
                        }
                        else
                            Program.ShowError("Unknown hardware (mac) error");
                    }
                    // restore Search Button
                    SrchBttnSetState(OpState.Idle);
                    Program.RefreshStatusLabel(this, "Ready");
                    break;
                case InvtryOpStatus.error:
                    switch (CurState)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.TagRssiEvent -= TagRssiEvtHandler;
                            //Rdr.InventoryOpStEvent -= TagRssiInvtryOpEvtHandler;
                            Rdr.UnregisterTagRssiEvent(TagRssiEvtHandler);
                            Rdr.UnregisterInventoryOpStEvent(TagRssiInvtryOpEvtHandler);
                            break;
                        case OpState.Running:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Search Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Search Stop Error");
                            //Rdr.TagRssiEvent -= TagRssiEvtHandler;
                            //Rdr.InventoryOpStEvent -= TagRssiInvtryOpEvtHandler;
                            Rdr.UnregisterTagRssiEvent(TagRssiEvtHandler);
                            Rdr.UnregisterInventoryOpStEvent(TagRssiInvtryOpEvtHandler);
                            break;
                        case OpState.Idle:
                            throw new ApplicationException("Unexpected error return during Idle State");
                    }
                    SrchBttnSetState(OpState.Idle);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
            }
        }

        // RSSI information updates
        private void TagRssiEvtHandler(object sender, TagRssiEventArgs e)
        {
            try
            {
                float rssiValue = e.RSSI;

                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    rssiValue = Math.Abs(rssiValue);
                }

                RssiLbl.Text = e.RSSI.ToString("F0"); // zero decimal point
                RssiLbl.Refresh(); // must show to the user immediately

                int PrgBrVal = Convert.ToInt32(rssiValue);
                progressBar1.Value = (PrgBrVal < progressBar1.Minimum) ? progressBar1.Minimum
                    : ((PrgBrVal > progressBar1.Maximum) ? progressBar1.Maximum : PrgBrVal);
                for (int i = (NumThresholdLvls-1); i >= 0; i--)
                {
                    if (rssiValue >= RssiLvlThresholds[i])
                    {

                        FlashBorder();
#if DEBUG
                        Reader Rdr = ReaderFactory.GetReader();
                        Rdr.RingingID = RingMelody[i];
                        Rdr.Svol = UserPref.GetInstance().SndVol;
                        Rdr.MelodyRing();
                       // RFIDRdr.GetInstance().MelodyRing(RingMelody[i], UserPref.GetInstance().SndVol);
#else
                        Reader rdr = ReaderFactory.GetReader();
                        //rdr.MelodyRing(RingMelody[i], SoundVol.High);

                        rdr.Svol = SoundVol.High;
                        rdr.RingingID = RingMelody[i];
                        rdr.MelodyRing();// always max. volume
#endif
                        break;
                    }
                }

                if (rssiValue < RssiLvlThresholds[0])
                    RssiLbl.Text = String.Empty;
                else
                    RestartOORDetectTmr();
#if false // Testing Only
                if (firstTag)
                {
                     Reader rdr = ReaderFactory.GetReader();
                        rdr.TagInvtryRssiStop();
                    firstTag = false;
                }
#endif
            }
            catch (ArithmeticException)
            {
                RssiLbl.Text = "Arithmetic Exception caught";
                RssiLbl.Refresh(); // must show to the user immediately
            }
            catch
            {
                RssiLbl.Text = "Exception caught";
                RssiLbl.Refresh(); // must show to the user immediately
            }
        }

        #endregion

        private bool OperRunning()
        {
            return (SrchBttnState() != OpState.Idle);
        }
        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            // Refuse if operation is running
            if (OperRunning())
            {
                MessageBox.Show("Please stop the Tag Search operation first", "Operation Denied");
                e.Cancel = true;
            }
            else
                e.Cancel = false; // let it close
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;

            //contain TagID
            //MaskInput.searchTagNo = _SearchTagNo;

            lblAssetNo.Text = _AssetNo;
             
            //cpmtaom AssetName, 
            lblAssetName.Text = _AssetName;
             
            
            UserPref upref = UserPref.GetInstance();
            //if (upref.ApplicationMode == UserPref.AppMode.StandAloneMode)
            //{
                //txtTagID.Visible = true;
                txtprefix.Visible = true;
                txtprefix.Text = upref.TagPreFix;
                txtprefix.Text = (string.IsNullOrEmpty(_SearchTagNo) ? upref.TagPreFix : _SearchTagNo);//.Substring(0, txtprefix.Text.Length - txtTagID.Text.Length) + txtTagID.Text;
                MaskInput.Visible = false;
                if (txtprefix.Text.Length == 24)
                    txtprefix.BackColor = System.Drawing.Color.White;
                else
                    txtprefix.BackColor = System.Drawing.Color.Moccasin;
                
            //}
            //else
            //{
            //    //txtTagID.Visible = false;
            //    txtprefix.Visible = false;
            //}

            if (upref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }
           

        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

       #region F11/F4/F5 HotKey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11:
                 case eVKey.VK_F19:
                    HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    {
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if (SrchBttnState() == OpState.Idle)
                            {
                                OnSrchBttnClicked(this, null);
                            }
                        }
                        else // up
                        {
                            if (SrchBttnState() == OpState.SettingUp
                                || SrchBttnState() == OpState.Starting
                                || SrchBttnState() == OpState.Running)
                            {
                                // Stop!
                                OnSrchBttnClicked(this, null);
                            }
                        }
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (down)
                    {
                        if (!OperRunning())
                        {
                            AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                        }
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && !OperRunning())
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                //case Keys.F7:
                //case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (OperRunning())
                    {
                        MessageBox.Show("Please stop the Tag Search operation first", "Operation Denied");                       
                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }

       #endregion

        private bool StopReqDuringSetup = false;

        private void RespModeSetNotify(bool Succ, String errMsg)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            if (StopReqDuringSetup == true)
            {
                StopReqDuringSetup = false; // clear flag
                SrchBttnSetState(OpState.Idle);
                Program.RefreshStatusLabel(this, "Ready");
                return;
            }
            else if (Succ)
            {
                Program.RefreshStatusLabel(this, "Starting...");
                SrchBttnSetState(OpState.Starting);
                Byte[] Mask;
                ushort WdCnt, WdOffset;
                // Assuming that validation is already done when 'Search' Button is pressed
                RateSrchValidateInput(out Mask, out WdCnt, out WdOffset);
                if (!SrchByRateStart(Rdr, Mask, WdCnt, WdOffset))
                {
                    SrchBttnSetState(OpState.Idle);
                    Program.RefreshStatusLabel(this, "Start Failed");
                }
            }
            else
            {
                SrchBttnSetState(OpState.Idle);
                Program.RefreshStatusLabel(this, "Setup Failed");

                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                    RadioNotPresentStopNotice();
                else
                    MessageBox.Show("Setup failed: " + errMsg);
            }
        }

        // Unable to handle restart when data in NORMAL RESPONSE MODE, ask user to
        // stop and re-start application
        private void RadioNotPresentStopNotice()
        {
            Program.ShowError("Radio Handle lost, please re-start program");
        }

        private bool SrchByRateSetup(Reader rdr)
        {
           // rdr.eventRespDataMode += RespModeSetNotify;
            rdr.RegistereventRespDataModeEvent(RespModeSetNotify);
            if (!rdr.SetResponseDataMode(2))
            {
                MessageBox.Show("Unable to set data response mode to NORMAL");
                return false;
            }
            return true;
        }

        private bool SrchByRateStart(Reader rdr, Byte[] mask, ushort wdCnt, ushort wdOffset)
        {
            //rdr.TagRateEvent += TagRateEvtHandler;
            //rdr.InventoryOpStEvent += TagRateInvtryOpEvtHandler;
            rdr.RegisterTagRateEvent(TagRateEvtHandler);
            rdr.RegisterInventoryOpStEvent(TagRateInvtryOpEvtHandler);
            UserPref Pref = UserPref.GetInstance();
            bool match1 = (wdCnt >= (EPCTag.PCFldSz + EPCTag.EPCFldSz));
            if (!rdr.TagInvtryRdRateStart(Pref.InvtrySession, match1, mask, wdOffset))
            {
                //rdr.TagRateEvent -= TagRateEvtHandler;
                //rdr.InventoryOpStEvent -= TagRateInvtryOpEvtHandler;
                rdr.UnregisterTagRateEvent(TagRateEvtHandler);
                rdr.UnregisterInventoryOpStEvent(TagRateInvtryOpEvtHandler);
                return false;
            }
            return true;
        }

        private bool SrchByRSSIStart(Reader rdr, Byte[] mask, ushort wdCnt, ushort wdOffset)
        {
            //rdr.TagRssiEvent += TagRssiEvtHandler;
            //rdr.InventoryOpStEvent += TagRssiInvtryOpEvtHandler;
            rdr.RegisterTagRssiEvent(TagRssiEvtHandler);
            rdr.RegisterInventoryOpStEvent(TagRssiInvtryOpEvtHandler);
            UserPref Pref = UserPref.GetInstance();
            bool match1 = (wdCnt >= (EPCTag.PCFldSz + EPCTag.EPCFldSz));
            if (!rdr.TagInventoryRssiStart(Pref.InvtrySession, match1, mask, wdOffset, FilterResChkBx.Checked))
            {
                rdr.UnregisterTagRssiEvent(TagRssiEvtHandler);
                rdr.UnregisterInventoryOpStEvent(TagRssiInvtryOpEvtHandler);
                return false;
            }
            return true;
        }

        private bool SrchByRateStop(Reader rdr)
        {
            if (!rdr.TagInventoryRdRateStop())
            {
                //rdr.TagRateEvent -= TagRateEvtHandler;
                //rdr.InventoryOpStEvent -= TagRateInvtryOpEvtHandler;
                rdr.UnregisterTagRateEvent(TagRateEvtHandler);
                rdr.UnregisterInventoryOpStEvent(TagRateInvtryOpEvtHandler);
                MessageBox.Show("Get Tag Inventory Rd Rate Stop Failed", "Error");
                return false;
            }
            return true;
        }

        private bool SrchByRatePostStopCleanup(Reader rdr)
        {
            if (!rdr.SetResponseDataMode(1))
            {
                MessageBox.Show("Unable to restore data response mode back to COMPACT");
                return false;
            }
            return true;
        }

        private bool SrchByRSSIStop(Reader rdr)
        {
            if (!rdr.TagInvtryRssiStop())
            {
                MessageBox.Show("Get Tag Inventory Rssi Stop Failed", "Error");
                //rdr.TagRssiEvent -= TagRssiEvtHandler;
                //rdr.InventoryOpStEvent -= TagRssiInvtryOpEvtHandler;
                rdr.UnregisterTagRssiEvent(TagRssiEvtHandler);
                rdr.UnregisterInventoryOpStEvent(TagRssiInvtryOpEvtHandler);
                return false;
            }
            return true;
        }

        private void OnSrchBttnClicked(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (SrchBttnState())
            {
                case OpState.Idle:
                    SrchBttnSetState(OpState.SettingUp);
                    Program.RefreshStatusLabel(this, "Setting up...");
                    Byte[] Mask;
                    ushort WdCnt, WdOffset;
                    if (!RateSrchValidateInput(out Mask, out WdCnt, out WdOffset))
                    {
                        SrchBttnSetState(OpState.Idle);
                        Program.RefreshStatusLabel(this, "Set up Failed");
                    }
                    else if (SrchByRateBttn.Checked)
                    {
                        if (!SrchByRateSetup(Rdr))
                        {
                            SrchBttnSetState(OpState.Idle);
                            Program.RefreshStatusLabel(this, "Set up Failed");
                        }
#if false // put the rest until RespDataRate status is known
                        else
                        {
                            Program.RefreshStatusLabel(this, "Starting...");
                            if (!SrchByRateStart(Rdr, Mask, WdCnt, WdOffset))
                            {
                                SrchBttnSetState(OpState.Idle);
                                Program.RefreshStatusLabel(this, "Start Failed");
                            }
                        }
#endif
                    }
                    else if (SrchByRSSIBttn.Checked)
                    {
                        if (WdCnt < (EPCTag.EPCFldSz + EPCTag.PCFldSz))
                        {
                            if (Program.AskUserConfirm("Full Mask (PC + EPC) search gives better result. Continue?") != DialogResult.OK)
                            {
                                SrchBttnSetState(OpState.Idle);
                                Program.RefreshStatusLabel(this, String.Empty);                                
                                Program.ShowWarning("User Canceled");
                                return;
                            }
                        }
                        Program.RefreshStatusLabel(this, "Starting...");
                        SrchBttnSetState(OpState.Starting);
                        if (!SrchByRSSIStart(Rdr, Mask, WdCnt, WdOffset))
                        {
                            SrchBttnSetState(OpState.Idle);
                            Program.RefreshStatusLabel(this, "Start Failed");
                        }
                    }
                    break;
                case OpState.Running:
                    SrchBttnSetState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (SrchByRateBttn.Checked)
                    {
                        if (!SrchByRateStop(Rdr))
                        {
                            SrchByRatePostStopCleanup(Rdr);
                            SrchBttnSetState(OpState.Idle);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                        }
                    }
                    else if (SrchByRSSIBttn.Checked)
                    {
                        if (!SrchByRSSIStop(Rdr))
                        {
                            SrchBttnSetState(OpState.Idle);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                        }
                    }
                    break;
                case OpState.SettingUp:
                    if (SrchByRateBttn.Checked)
                    {
                        StopReqDuringSetup = true;
                    }
                    break;
                case OpState.Starting:
                    SrchBttnSetState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    bool StopSucc = false;
                    if (SrchByRateBttn.Checked)
                    {
                        StopSucc = SrchByRateStop(Rdr);
                    }
                    else if (SrchByRSSIBttn.Checked)
                    {
                        StopSucc = SrchByRSSIStop(Rdr);
                    }
                    if (!StopSucc)
                    {
                        // Restore
                        SrchBttnSetState(OpState.Starting);
                        Program.RefreshStatusLabel(this, "Starting...");
                    }
                    break;
            }
        }

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        private void MaskInput_Click(object sender, EventArgs e)
        {

        }

        private void btnTask_Click(object sender, EventArgs e)
        {
            Assets Ast;
           
           
                Ast = new Assets(_SearchTagNo);
                frmPerformTask frm = new frmPerformTask();
                frm.ItemID = Ast.ServerKey;
                frm.AssetNo = Ast.AssetNo;
                frm.AssetName = Ast.Name;
                frm.Show();
                // disable form until this new form closed
                this.Enabled = false;
                frm.Closed += new EventHandler(OnSearchFormClose);
            
        }

        void OnSearchFormClose(object sender, EventArgs e)
        {
            this.Enabled = true;
            //throw new NotImplementedException();
        }

        private void btnChngLoc_Click(object sender, EventArgs e)
        {
            if (callFrom == "frmInventory")
            {
                changeLoc = true;
                this.Close();
            }
        }

      
        private void txtTagID_TextChanged(object sender, EventArgs e)
        {
            lblAssetName.Text = txtprefix.Text;
            MaskInput.searchTagNo = lblAssetName.Text;
            if (txtprefix.Text.Length == 24)
              txtprefix.BackColor = System.Drawing.Color.White;
            else
                txtprefix.BackColor = System.Drawing.Color.Moccasin;
              
        }
         

    }
}