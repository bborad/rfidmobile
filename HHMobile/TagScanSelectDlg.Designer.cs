namespace OnRamp
{
    partial class TagScanSelectDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader columnHeader1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagScanSelectDlg));
            System.Windows.Forms.ColumnHeader columnHeader2;
            System.Windows.Forms.ColumnHeader columnHeader3;
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.TagLstV = new System.Windows.Forms.ListView();
            this.ScanBttn = new System.Windows.Forms.Button();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.TagCntLbl = new System.Windows.Forms.Label();
            columnHeader1 = new System.Windows.Forms.ColumnHeader();
            columnHeader2 = new System.Windows.Forms.ColumnHeader();
            columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            resources.ApplyResources(columnHeader1, "columnHeader1");
            // 
            // columnHeader2
            // 
            resources.ApplyResources(columnHeader2, "columnHeader2");
            // 
            // columnHeader3
            // 
            resources.ApplyResources(columnHeader3, "columnHeader3");
            // 
            // TagLstV
            // 
            this.TagLstV.Columns.Add(columnHeader1);
            this.TagLstV.Columns.Add(columnHeader2);
            this.TagLstV.Columns.Add(columnHeader3);
            this.TagLstV.FullRowSelect = true;
            resources.ApplyResources(this.TagLstV, "TagLstV");
            this.TagLstV.Name = "TagLstV";
            this.TagLstV.View = System.Windows.Forms.View.Details;
            // 
            // ScanBttn
            // 
            resources.ApplyResources(this.ScanBttn, "ScanBttn");
            this.ScanBttn.Name = "ScanBttn";
            this.ScanBttn.Click += new System.EventHandler(this.OnScanBttnClicked);
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            this.OKBttn.Click += new System.EventHandler(this.OnOKBttnClicked);
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // TagCntLbl
            // 
            resources.ApplyResources(this.TagCntLbl, "TagCntLbl");
            this.TagCntLbl.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLbl.Name = "TagCntLbl";
            // 
            // TagScanSelectDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.TagCntLbl);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.ScanBttn);
            this.Controls.Add(this.TagLstV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagScanSelectDlg";
            this.Closed += new System.EventHandler(this.OnDlgClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnDlgClosing);
            this.Load += new System.EventHandler(this.OnDlgLoad);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.Windows.Forms.ColumnHeader columnHeader1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagScanSelectDlg));
            System.Windows.Forms.ColumnHeader columnHeader2;
            System.Windows.Forms.ColumnHeader columnHeader3;
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.TagLstV = new System.Windows.Forms.ListView();
            this.ScanBttn = new System.Windows.Forms.Button();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.TagCntLbl = new System.Windows.Forms.Label();
            columnHeader1 = new System.Windows.Forms.ColumnHeader();
            columnHeader2 = new System.Windows.Forms.ColumnHeader();
            columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            resources.ApplyResources(columnHeader1, "columnHeader1");
            // 
            // columnHeader2
            // 
            resources.ApplyResources(columnHeader2, "columnHeader2");
            // 
            // columnHeader3
            // 
            resources.ApplyResources(columnHeader3, "columnHeader3");
            // 
            // TagLstV
            // 
            this.TagLstV.Columns.Add(columnHeader1);
            this.TagLstV.Columns.Add(columnHeader2);
            this.TagLstV.Columns.Add(columnHeader3);
            this.TagLstV.FullRowSelect = true;
            resources.ApplyResources(this.TagLstV, "TagLstV");
            this.TagLstV.Name = "TagLstV";
            this.TagLstV.View = System.Windows.Forms.View.Details;
            // 
            // ScanBttn
            // 
            resources.ApplyResources(this.ScanBttn, "ScanBttn");
            this.ScanBttn.Name = "ScanBttn";
            this.ScanBttn.Click += new System.EventHandler(this.OnScanBttnClicked);
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            this.OKBttn.Click += new System.EventHandler(this.OnOKBttnClicked);
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // TagCntLbl
            // 
            resources.ApplyResources(this.TagCntLbl, "TagCntLbl");
            this.TagCntLbl.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLbl.Name = "TagCntLbl";
            // 
            // TagScanSelectDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.TagCntLbl);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.ScanBttn);
            this.Controls.Add(this.TagLstV);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagScanSelectDlg";
            this.Closed += new System.EventHandler(this.OnDlgClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnDlgClosing);
            this.Load += new System.EventHandler(this.OnDlgLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView TagLstV;
        private System.Windows.Forms.Button ScanBttn;
        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.Label TagCntLbl;
    }
}