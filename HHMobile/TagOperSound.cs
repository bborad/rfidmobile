using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.Text;using CS101UILib;
using ClsReaderLib;using ClsReaderLib.Devices;

using SndSituations = OnRamp.UserPref.SndSituations;

namespace OnRamp
{
    /// <summary>
    /// Monitor events from different TagXXXForm, based on
    /// UserPref, give sound notifications
    /// </summary>
    class TagOperSound
    {
        public bool isPlaySound = true;
        private void LoadMandatorySituations()
        {
            // Bar Code Form
            BCScanForm.TagOperEvt += TagOperEvtHandler;
            frmBCScanForm.TagOperEvt += TagOperEvtHandler;
            // Tag Search Form
            TagSearchForm.TagOperEvt += TagOperEvtHandler;
        }

        private void UnLoadMandatorySituations()
        {
            BCScanForm.TagOperEvt -= TagOperEvtHandler;
            TagSearchForm.TagOperEvt -= TagOperEvtHandler;
            frmBCScanForm.TagOperEvt -= TagOperEvtHandler;
        }

        private void LoadSituations()
        {
            UserPref Pref = UserPref.GetInstance();
            // Register notification events with different classes
            if ((Pref.SndNotifications & SndSituations.Invtry) == SndSituations.Invtry)
            {
                TagInvtryForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
                TagConfirmForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
            }
            else
            {
                TagInvtryForm.TagOperEvt -= TagOperEvtHandler;
                TagConfirmForm.TagOperEvt -= TagOperEvtHandler;  
            }
            
            if ((Pref.SndNotifications & SndSituations.Ranging) == SndSituations.Ranging)
                TagRangingForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
            else
                TagRangingForm.TagOperEvt -= TagOperEvtHandler;
            
            if ((Pref.SndNotifications & SndSituations.RdWr) == SndSituations.RdWr)
            {
                //frmReceiveDispatch.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
                TagRdForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
                TagWrForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
                TagFSForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
                frmDDSelection.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
            }
            else
            {
               // frmReceiveDispatch.TagOperEvt -= TagOperEvtHandler;
                TagRdForm.TagOperEvt -= TagOperEvtHandler;
                TagWrForm.TagOperEvt -= TagOperEvtHandler;
                TagFSForm.TagOperEvt -= TagOperEvtHandler;
                frmDDSelection.TagOperEvt -= new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
            }

            if ((Pref.SndNotifications & SndSituations.SetPerm) == SndSituations.SetPerm)
                TagPermForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
            else
                TagPermForm.TagOperEvt -= TagOperEvtHandler;

            if ((Pref.SndNotifications & SndSituations.Authentication) == SndSituations.Authentication)
                TagAuthenForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
            else
                TagAuthenForm.TagOperEvt -= TagOperEvtHandler;
            
            if ((Pref.SndNotifications & SndSituations.Commission) == SndSituations.Commission)
                TagCommForm.TagOperEvt += new EventHandler<TagOperEvtArgs>(TagOperEvtHandler);
            else
                TagCommForm.TagOperEvt -= TagOperEvtHandler;
        }

        private void UnLoadSituations()
        {
            TagConfirmForm.TagOperEvt -= TagOperEvtHandler; 
            TagInvtryForm.TagOperEvt -= TagOperEvtHandler;
            TagRangingForm.TagOperEvt -= TagOperEvtHandler;
            TagRdForm.TagOperEvt -= TagOperEvtHandler;
            TagFSForm.TagOperEvt -= TagOperEvtHandler;  
            TagWrForm.TagOperEvt -= TagOperEvtHandler;
            TagPermForm.TagOperEvt -= TagOperEvtHandler;
            TagAuthenForm.TagOperEvt -= TagOperEvtHandler;
            TagCommForm.TagOperEvt -= TagOperEvtHandler;
           // frmReceiveDispatch.TagOperEvt -= TagOperEvtHandler;

        }

        public TagOperSound(ref EventHandler PrefChangedEvt)
        {
            PrefChangedEvt += PrefChanged;
            LoadMandatorySituations();
            LoadSituations();
        }
        
        ~TagOperSound()
        {
            UnLoadMandatorySituations();
            UnLoadSituations();
        }

        private void PrefChanged(object sender, EventArgs e)
        {
            UnLoadSituations();
            LoadSituations();
        }

        /// <summary>
        /// To be shared among all events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
#if !GENERIC_PDA_HW
            UserPref Pref = UserPref.GetInstance();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
           // Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            if (true || isPlaySound)
            {
                switch (e.Type)
                {
                    case TagOperEvtType.Started:
                        Rdr.RingingID = Pref.OperStartMldy;
                        Rdr.Svol =Pref.SndVol;
                        Rdr.MelodyRing();
                        break;
                    case TagOperEvtType.Stopped:
                        Rdr.RingingID = Pref.OperEndMldy;
                        Rdr.Svol = Pref.SndVol;
                        Rdr.MelodyRing();
                        break;
                    case TagOperEvtType.Updated:
                        Rdr.Svol = Pref.SndVol;
                        Rdr.BuzzerBeep(1);
                        break;
                }
            }
#endif
        }
                
    }

    /// <summary>
    ///  External Indication for TagXXXForms
    /// </summary>
    public enum TagOperEvtType
    {
        Started,
        Updated,
        Stopped,
        NotValid,
    }

    public class TagOperEvtArgs : EventArgs
    {
        public readonly TagOperEvtType Type;

        public TagOperEvtArgs(TagOperEvtType type)
        {
            Type = type;
        }
    }

    

}
