extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;


using ClsLibBKLogs;


namespace ClsRampdb
{

    public static class Util
    {
        public static DateTime getLastSyncedDate(String tblName)
        {
            //using (CEConn localDB = new CEConn())
            //{
            //    string strSql;
            //    strSql = " select max(Date_Modified) as SyncedDate from " + tblName;
            //    Object o;
            //    o = localDB.getScalerValue(strSql);
            //    if (DBNull.Value == o)
            //        return DateTime.Now.AddYears(-10);
            //    else
            //        return Convert.ToDateTime(o);
            //}

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Util.getLastSyncedDate(tblName);
            }
            else
            {
                return OldLib.Util.getLastSyncedDate(tblName);
            }
        }

        public static int getRecordCount(String tblName)
        {

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Util.getRecordCount(tblName);
            }
            else
            {
                return OldLib.Util.getRecordCount(tblName);
            }


        }

        public static string updateTables(String tblName, DataRow dRow, DateTime modifyDate)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Util.updateTables(tblName, dRow, modifyDate);
            }
            else
            {
                return OldLib.Util.updateTables(tblName, dRow, modifyDate);
            }

        }


    }



    public static class Login
    {
        //http://iibtest/RampService/'

        public static string _urlStr;
       
        public static string webURL = urlStr + "Synchronise.asmx";
        public static string webConURL = urlStr + "RConnection.asmx";
        private static string _TagID;
        private static string _EmpNo;
        private static string _UserName;
        private static string _Password;
        private static Int32 _RoleId;
        private static string _Role;
        private static bool _OnLine;
        private static Int32 _ID;
        public static string err = "";
        public static Int64 _ItemLimit = 0;

        // public static AppModules appModules = new AppModules();  

        public static string urlStr
        {
            get
            {
                return _urlStr;
            }
            set
            {
                _urlStr = value;
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    NewLib.Login.urlStr = _urlStr;
                }
                else
                {
                    OldLib.Login.urlStr = _urlStr;
                }
            }
        }

        public static String TagID
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _TagID = NewLib.Login.TagID;
                }
                else
                {
                    _TagID = OldLib.Login.TagID;

                }
              
                return _TagID;
            }


        }
        public static String EmpNo
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _EmpNo = NewLib.Login.EmpNo;
                }
                else
                {
                    _EmpNo = OldLib.Login.EmpNo;

                } 
                
                return _EmpNo;
            }
        }
        public static String UserName
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _UserName = NewLib.Login.UserName;
                }
                else
                {
                    _UserName = OldLib.Login.UserName;
                    
                }
                return _UserName;
            }
             
        }
        public static String Password
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _Password = NewLib.Login.Password;
                }
                else
                {
                    _Password = OldLib.Login.Password;
                   
                }
                return _Password;
            }
           
        }
        public static Int32 RoleId
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _RoleId = NewLib.Login.RoleId;
                }
                else
                {
                    _RoleId = OldLib.Login.RoleId;

                }

            
                return _RoleId;
            }
        }
        public static String Role
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _Role = NewLib.Login.Role;
                }
                else
                {
                    _Role = OldLib.Login.Role;

                } 
             
                return _Role;
            }
        }
        public static bool OnLineMode
        { 
            get
            {

                return _OnLine;
            }
            set
            {
                _OnLine = value;
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    NewLib.Login.OnLineMode = _OnLine;
                }
                else
                {
                    OldLib.Login.OnLineMode = _OnLine;
                }

            }
        }
        public static Int32 ID
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _ID = NewLib.Login.ID;
                }
                else
                {
                    _ID = OldLib.Login.ID;

                } 
                
                return _ID;
            }
        }

        public static Int64 ItemLimit
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _ItemLimit = NewLib.Login.ItemLimit;
                }
                else
                {
                    _ItemLimit = OldLib.Login.ItemLimit;
                }
                return _ItemLimit;
            }
            set
            {
                _ItemLimit = value;
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    NewLib.Login._ItemLimit = _ItemLimit;
                }
                else
                {
                    OldLib.Login._ItemLimit = _ItemLimit;
                }

            }
        }

        public static bool verifyPassword(string username, string password)
        {

            UserPref Pref = UserPref.GetInstance();

            try
            {
                if (Pref.UsingNewDBLib)
                {
                    return NewLib.Login.verifyPassword(username, password);
                }
                else
                {
                    return OldLib.Login.verifyPassword(username, password);
                }
            } 
            catch (Exception ep) 
            {
                throw ep;
                
            }
        }

        //static bool ValidateUserInOnlineMode(string username, string password)
        //{
        //    bool result = false;
        //    try
        //    {
        //        int serverKey = CheckUserInOnlineDB(username, password);

        //        if (serverKey == -99) // user not found in online DB.
        //        {
        //            result = true;
        //        }
        //        else if (serverKey == _ID) // online and offline DB has same user with same IDs.
        //        {
        //            result = true;
        //        }
        //        else if (serverKey != _ID) // online and offline DB has same user with different IDs, update the ServerKey of offline DB with ID_Employee of online Key.
        //        {
        //            // Update Server Key of Offline DB.
        //            using (CEConn localDB = new CEConn())
        //            {
        //                string strSql;
        //                strSql = "Update Employees Set ServerKey = " + serverKey;
        //                strSql += " where Employees.username='" + username + "' and Employees.password='" + password + "'";

        //                int output = localDB.runQuery(strSql);

        //                if (output > 0)
        //                {
        //                    _ID = serverKey;
        //                    result = true;
        //                }
        //            }

        //        }

        //        // Validate offline user record with online user record 
        //        // if also exist in online db, check and update the serverkey field of offline user record with ID_employee
        //        // field of online db.
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LogError("Error while validating the offline login user with Online database : " + ex.Message);
        //    }
        //    return result;
        //}

        public static int CheckUserInOnlineDB(string username, string password)
        {
            UserPref Pref = UserPref.GetInstance();

            try
            {
                if (Pref.UsingNewDBLib)
                {
                    return NewLib.Login.CheckUserInOnlineDB(username, password);
                }
                else
                {
                    return OldLib.Login.CheckUserInOnlineDB(username, password);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable getMenuOptionTable()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Login.getMenuOptionTable();
            }
            else
            {
                return OldLib.Login.getMenuOptionTable();
            }
        }

    }


}
