﻿extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using ClsLibBKLogs;

namespace ClsRampdb
{
    public class Task
    {
        public delegate void TaskPerformedNotify(String ID_Asset);
        public static event TaskPerformedNotify taskPerformed;

        #region "variables & Procedures."
        //Int32 _ID_Tasks;
        String _Title;
        Int32 _ServerKey;
        Int32 _RowStatus;

        public Int32 ServerKey
        {
            get
            {
                return _ServerKey;
            }
        }

        public String Title
        {
            get
            {
                return _Title;
            }
        }

        //public Int32 ID_Tasks
        //{
        //    get
        //    {
        //        return _ID_Tasks;
        //    }
        //}

        public RowStatus Status
        {
            get
            {
                return (RowStatus)_RowStatus;
            }
        }

        #endregion

        public Task(Int32 TaskID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Task t = new NewLib.Task(TaskID);

                _ServerKey = TaskID;
                _Title = t.Title;
                _RowStatus = Convert.ToInt32(t.Status);
            }
            else
            {
                OldLib.Task t = new OldLib.Task(TaskID);
                _ServerKey = TaskID;
                _Title = t.Title;
                _RowStatus = Convert.ToInt32(t.Status);
            }

        }

        public static DataTable getTasksList()
        {
             UserPref Pref = UserPref.GetInstance();
             if (Pref.UsingNewDBLib)
             {
                 return NewLib.Task.getTasksList();
             }
             else
             {
                 return OldLib.Task.getTasksList();
             }

        }

        public static void AddTaskPerformed(Int32 TaskID, Int32 EmployeeID, Int32 ItemId)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                  NewLib.Task.AddTaskPerformed(TaskID, EmployeeID, ItemId);
            }
            else
            {
                  OldLib.Task.AddTaskPerformed(TaskID, EmployeeID, ItemId);
            }
        }

        public static DataTable getTaskPerformedRows(RowStatus Stat)
        {
              UserPref Pref = UserPref.GetInstance();
              if (Pref.UsingNewDBLib)
              {
                  return NewLib.Task.getTaskPerformedRows(Stat);
              }
              else
              {
                  return OldLib.Task.getTaskPerformedRows(Stat);
              }
        }

        //private static Int32 minServerKey()
        //{


        //    UserPref Pref = UserPref.GetInstance();
        //    if (Pref.UsingNewDBLib)
        //    {
        //        return NewLib.Task.getTaskPerformedRows(Stat);
        //    }
        //    else
        //    {
        //        return OldLib.Task.getTaskPerformedRows(Stat);
        //    }

        //}

    }
}
