extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using System.Collections;

using System.Threading;
using ClsLibBKLogs;

namespace ClsRampdb
{
    public class Assets
    {
        #region "Variable and Properties"
        public string PartCode { get; set; }

        Int32 _ServerKey;
        String _TagID;
        public String Name { get; set; }
        public String AssetNo { get; set; }
        public Int32 ID_Employee { get; set; }
        Int32 _ID_Location;
        Int32 _RowStatus;
        public Int32 ID_AssetGroup { get; set; }
        public DateTime Date_Modified { get; set; }
        String _LotNo = "";

        public String ReferenceNo { get; set; }
        public string Description { get; set; }

        public static String TableName = "assets";

        public Int32 ServerKey
        {
            get { return _ServerKey; }
            set { _ServerKey = value; }
        }

        public String TagID
        {
            get { return _TagID; }
            set { _TagID = value; }
        }

        //public String Name
        //{
        //    get { return _Name; }
        //}

        //public String AssetNo
        //{
        //    get { return _AssetNo; }
        //}

        //public Int32 ID_Employee
        //{
        //    get { return _ID_Employee; }
        //}

        //public Int32 ID_AssetGroup
        //{
        //    get { return _ID_AssetGroup; }
        //}

        //public DateTime Date_Modified
        //{
        //    get { return _Date_Modified;}
        //}

        public Int32 ID_Location
        {
            get
            {
                return _ID_Location;
            }
            set
            {
                _ID_Location = value;
            }
        }

        public RowStatus Status
        {
            get
            {
                return (RowStatus)_RowStatus;
            }
            set
            {
                _RowStatus = Convert.ToInt32(value);
            }

        }

        public String LotNo
        {
            get { return _LotNo; }
            set { _LotNo = value; }
        }

        #endregion

        public Assets()
        {
        }

        public Assets(String TagID)
        {
            this.TagID = TagID;

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Assets t = new NewLib.Assets(TagID);

                ServerKey = t.ServerKey;

                Name = t.Name;

                AssetNo = t.AssetNo;

                ID_Employee = t.ID_Employee;

                ID_Location = t.ID_Location;


                ID_AssetGroup = t.ID_AssetGroup;

                Date_Modified = t.Date_Modified;


                _LotNo = t.LotNo;

                ReferenceNo = t.ReferenceNo;
                Description = t.Description;

                Status = t.Status;
            }
            else
            {
                OldLib.Assets t = new OldLib.Assets(TagID);
                ServerKey = t.ServerKey;

                Name = t.Name;

                AssetNo = t.AssetNo;

                ID_Employee = t.ID_Employee;

                ID_Location = t.ID_Location;


                ID_AssetGroup = t.ID_AssetGroup;

                Date_Modified = t.Date_Modified;


                _LotNo = t.LotNo;

                ReferenceNo = t.ReferenceNo;
                Description = t.Description;

                Status = t.Status;
            }



            //throw new ApplicationException("Online functionality not implemented yet.");

        }

        public static void AddAsset(String TagID, String AssetNo, String AssetName, Int32 LocationKey, string description, string ReferenceNo)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Assets.AddAsset(TagID, AssetNo, AssetName, LocationKey, description, ReferenceNo);
            }
            else
            {
                OldLib.Assets.AddAsset(TagID, AssetNo, AssetName, LocationKey, description, ReferenceNo);
            }

        }

        public static void DeleteAsset(Int32 serverKey)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                //Add method to delte asset
                throw new Exception();
            }
            else
            {
                OldLib.Assets.DeleteAsset(serverKey);
            }
        }

        //public static void RunQuery(object sqlQueryText)
        //{
        //    UserPref Pref = UserPref.GetInstance();
        //    if (Pref.UsingNewDBLib)
        //    {
        //        NewLib.Assets.RunQuery(sqlQueryText);
        //    }
        //    else
        //    {
        //        OldLib.Assets.RunQuery(sqlQueryText);
        //    }
        //}

        public static void EditAsset(String TagID, String AssetNo, String AssetName, Int32 LocationKey, Int32 ServerKey, string description, string ReferenceNo)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Assets.EditAsset(TagID, AssetNo, AssetName, LocationKey, ServerKey, description, ReferenceNo);
            }
            else
            {
                OldLib.Assets.EditAsset(TagID, AssetNo, AssetName, LocationKey, ServerKey, description, ReferenceNo);
            }
        }

        public static Int32 getNoOfItems()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getNoOfItems();
            }
            else
            {
                return OldLib.Assets.getNoOfItems();
            }
        }

        public String getLocationName()
        {
            string result = "";
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Assets aa = new NewLib.Assets();
                aa.ID_Location = ID_Location;
                result = aa.getLocationName();
            }
            else
            {
                OldLib.Assets aa = new OldLib.Assets();
                aa.ID_Location = ID_Location;
                result = aa.getLocationName();
            }
            return result;
        }

        /// <summary>
        /// Retrun List of assets, by retriving data from TagIDs
        /// </summary>
        /// <param name="TagIDs">Accepts Comma seperated Tag IDs</param>
        /// <returns>Array of Assets</returns>
        public static List<Assets> GetAssetsByTagID(String TagIDs)
        {
            List<Assets> lstAsset1 = new List<Assets>();
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                List<NewLib.Assets> lstAsset = new List<NewLib.Assets>();
                lstAsset = NewLib.Assets.GetAssetsByTagID(TagIDs);

                if (lstAsset.Count > 0)
                {
                    foreach (NewLib.Assets objAsset in lstAsset)
                    {
                        lstAsset1.Add(new Assets()
                        {
                            AssetNo = objAsset.AssetNo,
                            Date_Modified = objAsset.Date_Modified,
                            ID_AssetGroup = objAsset.ID_AssetGroup,
                            Description = objAsset.Description,
                            ID_Employee = objAsset.ID_Employee,
                            ID_Location = objAsset.ID_Location,
                            LotNo = objAsset.LotNo,
                            Name = objAsset.Name,
                            PartCode = objAsset.PartCode,
                            ServerKey = objAsset.ServerKey,
                            ReferenceNo = objAsset.ReferenceNo,
                            Status = objAsset.Status,
                            TagID = objAsset.TagID
                        });
                    }
                }
            }
            else
            {
                List<OldLib.Assets> lstAsset = new List<OldLib.Assets>();
                lstAsset = OldLib.Assets.GetAssetsByTagID(TagIDs);

                if (lstAsset.Count > 0)
                {
                    foreach (OldLib.Assets objAsset in lstAsset)
                    {
                        lstAsset1.Add(new Assets()
                        {
                            AssetNo = objAsset.AssetNo,
                            Date_Modified = objAsset.Date_Modified,
                            ID_AssetGroup = objAsset.ID_AssetGroup,
                            Description = objAsset.Description,
                            ID_Employee = objAsset.ID_Employee,
                            ID_Location = objAsset.ID_Location,
                            LotNo = objAsset.LotNo,
                            Name = objAsset.Name,
                            PartCode = objAsset.PartCode,
                            ServerKey = objAsset.ServerKey,
                            ReferenceNo = objAsset.ReferenceNo,
                            Status = objAsset.Status,
                            TagID = objAsset.TagID
                        });
                    }
                }
            }
            return lstAsset1;

        }

        /// <summary>
        /// Retrun List of assets, by retriving data from TagIDs
        /// </summary>
        /// <param name="TagIDs">Accepts Comma seperated Tag IDs</param>
        /// <returns>Array of Assets</returns>
        public static List<Assets> GetAssets_ByTagID(String TagIDs, int Capacity)
        { 
            List<Assets> lstAsset1 = new List<Assets>();
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                List<NewLib.Assets> lstAsset = new List<NewLib.Assets>();
                lstAsset = NewLib.Assets.GetAssets_ByTagID(TagIDs, Capacity);

                if (lstAsset.Count > 0)
                {
                    foreach (NewLib.Assets objAsset in lstAsset)
                    {
                        lstAsset1.Add(new Assets()
                        {
                            AssetNo = objAsset.AssetNo,
                            Date_Modified = objAsset.Date_Modified,
                            ID_AssetGroup = objAsset.ID_AssetGroup,
                            Description = objAsset.Description,
                            ID_Employee = objAsset.ID_Employee,
                            ID_Location = objAsset.ID_Location,
                            LotNo = objAsset.LotNo,
                            Name = objAsset.Name,
                            PartCode = objAsset.PartCode,
                            ServerKey = objAsset.ServerKey,
                            ReferenceNo = objAsset.ReferenceNo,
                            Status = objAsset.Status,
                            TagID = objAsset.TagID
                        });
                    }
                }
            }
            else
            {
                List<OldLib.Assets> lstAsset = new List<OldLib.Assets>();
                lstAsset = OldLib.Assets.GetAssets_ByTagID(TagIDs, Capacity);

                if (lstAsset.Count > 0)
                {
                    foreach (OldLib.Assets objAsset in lstAsset)
                    {
                        lstAsset1.Add(new Assets()
                        {
                            AssetNo = objAsset.AssetNo,
                            Date_Modified = objAsset.Date_Modified,
                            ID_AssetGroup = objAsset.ID_AssetGroup,
                            Description = objAsset.Description,
                            ID_Employee = objAsset.ID_Employee,
                            ID_Location = objAsset.ID_Location,
                            LotNo = objAsset.LotNo,
                            Name = objAsset.Name,
                            PartCode = objAsset.PartCode,
                            ServerKey = objAsset.ServerKey,
                            ReferenceNo = objAsset.ReferenceNo,
                            Status = objAsset.Status,
                            TagID = objAsset.TagID
                        });
                    }
                }
            }
            return lstAsset1;

        }


        /// <summary>
        /// Retrun List of assets, by retriving data from TagIDs
        /// </summary>
        /// <param name="TagIDs">Accepts Comma seperated Tag IDs</param>
        /// <returns>Array of Assets</returns>
        public static DataTable GetLotNoGroups(String TagIDs)
        {
            DataTable dtTotal = null;
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                dtTotal = NewLib.Assets.GetLotNoGroups(TagIDs);

            }
            else
            {
                dtTotal = OldLib.Assets.GetLotNoGroups(TagIDs);
            }
            return dtTotal;

        }

        /// <summary>
        /// The group by clause for Data Table
        /// </summary>
        /// <param name="dt">The main data table to be filtered with the Group By.</param>
        /// <param name="columnNamesInDt">The Fields sent seperated by commas. EG "Field1,Field2,Field3"</param>
        /// <param name="groupByColumnNames">The column name which should be used for group by.</param>
        /// <param name="typeOfCalculation">The calculation type like Sum, Avg Or Count.</param>
        /// <returns></returns>
        //private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        //{
        //    //Return its own if the column names are empty
        //    UserPref Pref = UserPref.GetInstance();
        //    if (Pref.UsingNewDBLib)
        //    {
        //        dtTotal = NewLib.Assets.GetGroupedBy(  dt,   columnNamesInDt,   groupByColumnNames,   typeOfCalculation);

        //    }
        //    else
        //    {
        //        dtTotal = OldLib.Assets.GetLotNoGroups(TagIDs);
        //    }

        //    return _dt;
        //}


        public void AssignNewTag(String TagNo)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Assets aa = new NewLib.Assets(_TagID);
                aa.AssignNewTag(TagNo);
            }
            else
            {
                OldLib.Assets aa = new OldLib.Assets(_TagID);
                aa.AssignNewTag(TagNo);
            }
        }

        public void updateInventory(Int32 locID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Assets aa = new NewLib.Assets(_TagID);
                aa.updateInventory(locID);
            }
            else
            {
                OldLib.Assets aa = new OldLib.Assets(_TagID);
                aa.updateInventory(locID);
            }
        }

        public static int update_Inventory(string TagIDs, Int32 locID)
        {
            int result = 0;
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                result = NewLib.Assets.update_Inventory(TagIDs, locID);

            }
            else
            {
                result = OldLib.Assets.update_Inventory(TagIDs, locID);
            }
            return result;

        }

        public static DataTable getSearchReasult(String AssetNo, String AssetName, Int32 locKey, Int32 GroupKey, Int32 subGroupKey, String LotNo, int option)
        {
            //return getSearchReasult(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, "");

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (option < 0)
                    return NewLib.Assets.getSearchReasult(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, "");
                else
                    return NewLib.Assets.getSearchReasultNew(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, "", option);

            }
            else
            {
                if (option < 0)
                    return OldLib.Assets.getSearchReasult(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, "");
                else
                    return OldLib.Assets.getSearchReasultNew(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, "", option);
            } 
             
        }

        public static DataTable getSearchReasult(String AssetNo, String AssetName, Int32 locKey, Int32 GroupKey, Int32 subGroupKey, String LotNo, String TagID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getSearchReasult(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, TagID);

            }
            else
            {
                return OldLib.Assets.getSearchReasult(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, TagID);
            }
        }

        public static DataTable getSearchReasultNew(String AssetNo, String AssetName, Int32 locKey, Int32 GroupKey, Int32 subGroupKey, String LotNo, String TagID, int option)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getSearchReasultNew(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, TagID,option);

            }
            else
            {
                return OldLib.Assets.getSearchReasultNew(AssetNo, AssetName, locKey, GroupKey, subGroupKey, LotNo, TagID,option);
            }
        }

        public static DataTable getSearchReasultNew(Dictionary<string, string> columnsToSearch, Int32 locKey, Int32 GroupKey, Int32 subGroupKey, int option)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getSearchReasultNew(columnsToSearch, locKey, GroupKey, subGroupKey, option);

            }
            else
            {
                return OldLib.Assets.getSearchReasultNew(columnsToSearch, locKey, GroupKey, subGroupKey, option);
            }
        }

        private static bool isNumeric(string value)
        {
            // return System.Text.RegularExpressions.Regex.IsMatch(value, "[0-9]+");
            return System.Text.RegularExpressions.Regex.IsMatch(value, @"^\d+$");
        }

        //private static Int32 minServerKey()
        //{
        //    //using (CEConn localDB = new CEConn())
        //    //{
        //    //    string strSql;
        //    //    strSql = "select min(serverKey) from Assets";
        //    //    object o;
        //    //    o = localDB.getScalerValue(strSql);
        //    //    if (DBNull.Value == o)
        //    //        return -1;
        //    //    else if (Convert.ToInt32(o) < 0)
        //    //        return (Convert.ToInt32(o) - 1);
        //    //    else
        //    //        return -1;
        //    //}

        //    UserPref Pref = UserPref.GetInstance();
        //    if (Pref.UsingNewDBLib)
        //    {
        //        return NewLib.Assets.minServerKey();

        //    }
        //    else
        //    {
        //        return OldLib.Assets.minServerKey();
        //    }
        //}

        public static DataTable getNewRows()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getNewRows();

            }
            else
            {
                return OldLib.Assets.getNewRows();
            }
        }

        public static DataTable getRows(RowStatus Stat)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getRows( Stat);

            }
            else
            {
                return OldLib.Assets.getRows(  Stat);
            }
        }

        // Get Inventory New Rows
        public static DataTable getInventoryRows(RowStatus Stat)
        {
            //using (CEConn localDB = new CEConn())
            //{
            //    DataTable dtInventory = new DataTable("dtInventory");
            //    string strSql;

            //    strSql = "select TagID,ID_Location,ServerKey from Inventory where RowStatus=" + Convert.ToInt32(Stat);
            //    localDB.FillDataTable(ref dtInventory, strSql);


            //    return dtInventory;
            //}

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getInventoryRows(Stat);

            }
            else
            {
                return OldLib.Assets.getInventoryRows(Stat);
            }

        }

        public static DataTable getAssetsByLocation(Int32 ID_Location)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Assets.getAssetsByLocation(ID_Location);

            }
            else
            {
                return OldLib.Assets.getAssetsByLocation(ID_Location);
            }

        }

        public static DataTable OnLineInventory(ref DataTable dtInventory)
        {
            DataTable dtResult = new DataTable("result");
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                dtResult = NewLib.Assets.OnLineInventory(ref   dtInventory);

            }
            else
            {
                dtResult = OldLib.Assets.OnLineInventory(ref   dtInventory);
            }
            return dtResult;
        }

        public static int UpdateAssetLocation(string TagIDs, Int64 ID_Location, Int64 ID_Reason)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
               return NewLib.Assets.UpdateAssetLocation(TagIDs, ID_Location, ID_Reason);

            }
            else
            {
                return   OldLib.Assets.UpdateAssetLocation(TagIDs, ID_Location, ID_Reason);
            }

        }

        public static int Update_AssetsLocation(string TagIDs, Int64 ID_Location, Int64 ID_Reason)
        {
          
            int result = 0;
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                result = NewLib.Assets.Update_AssetsLocation(TagIDs, ID_Location, ID_Reason);

            }
            else
            {
                result = OldLib.Assets.Update_AssetsLocation(TagIDs, ID_Location, ID_Reason);
            }
            return result;
        }

        public static int UpdateAssetLocation(string TagID, Int64 ID_Location)
        {
            return UpdateAssetLocation(TagID, ID_Location, 0);
        }

        public static int UpdateAssetLocation(string TagIDs, Int64 ID_Location, string ReferenceNo, Boolean IsDispatch)
        {
           
            int result = 0;
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                result = NewLib.Assets.UpdateAssetLocation(TagIDs, ID_Location, ReferenceNo,IsDispatch );

            }
            else
            {
                result = OldLib.Assets.UpdateAssetLocation(TagIDs, ID_Location, ReferenceNo, IsDispatch);
            }

            return result;
        }

        public static int UpdateAssetLocation(string TagID, Int64 ID_Location, string ReferenceNo)
        {
            return UpdateAssetLocation(TagID, ID_Location, ReferenceNo, false);
        }
    }
}
