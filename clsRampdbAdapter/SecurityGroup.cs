extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
 
using ClsLibBKLogs;

namespace  ClsRampdb
{
    public class SecurityGroup
    {
        public static string TableName = "master_securitygroups";

        public static DataTable getSecurityGroups()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.SecurityGroup.getSecurityGroups();
            }
            else
            {
                return OldLib.SecurityGroup.getSecurityGroups();
            }
        }
    }
}
