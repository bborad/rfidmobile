extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ClsLibBKLogs;
using System.Threading;
namespace  ClsRampdb
{
    public class Employee
    {
        String _TagID,_EmpNo,_UserName;
        Int32 _ID_SecurityGroup;
        Int32 _ServerKey;
        Int32 _RowStatus;

        public static string TableName = "employees";

        public Int32 ID_SecurityGroup
        {
            get
            {
                return _ID_SecurityGroup;
            }
        }

        public String TagNo
        {
            get
            {
                return _TagID;
            }
        }

        public String EmpNo
        {
            get
            {
                return _EmpNo;
            }
        }

        public RowStatus Status
        {
            get
            {
                return (RowStatus) _RowStatus;
            }
        }

        public String UserName
        {
            get
            {
                return _UserName;
            }
        }

        public Int32 ServerKey
        {
            get
            {
                return _ServerKey;
            }
        }

        public Employee(Int32 EmpID)
        {
            _ServerKey = EmpID;

              UserPref Pref = UserPref.GetInstance();
              if (Pref.UsingNewDBLib)
              {
                  NewLib.Employee emp = new NewLib.Employee(EmpID);
                  _ID_SecurityGroup = emp.ID_SecurityGroup;
                  _TagID = emp.TagNo;

                  _EmpNo = emp.EmpNo;
                  _UserName = emp.UserName;
                  _ServerKey = emp.ServerKey;
                  _RowStatus = Convert.ToInt32(emp.Status);
              }
              else
              {
                  OldLib.Employee emp = new OldLib.Employee(EmpID);
                  _ID_SecurityGroup = emp.ID_SecurityGroup;
                  _TagID = emp.TagNo;

                  _EmpNo = emp.EmpNo;
                  _UserName = emp.UserName;
                  _ServerKey = emp.ServerKey;
                  _RowStatus = Convert.ToInt32(emp.Status);
              }

          
        }

        public Employee()
        {
            _ServerKey = 0;
        }

        public static DataTable getNewRows()
        {

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Employee.getNewRows();
            }
            else
            {

                return OldLib.Employee.getNewRows();
            }
 
        }

        public static DataTable getModifiedRows()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Employee.getModifiedRows();
            }
            else
            {

                return OldLib.Employee.getModifiedRows();
            }
        }
     
        public void AssignNewTag(String TagNo)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Employee e = new NewLib.Employee();
                e.AssignNewTag(TagNo);
            }
            else
            {

                OldLib.Employee e = new OldLib.Employee();
                e.AssignNewTag(TagNo);
            }
        }

        public static void AddEmployee(String TagID, String EmpNo, String UserName,String Password , Int32 RoleID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                  NewLib.Employee.AddEmployee(TagID, EmpNo, UserName, Password, RoleID);
            }
            else
            {

                OldLib.Employee.AddEmployee(TagID, EmpNo, UserName, Password, RoleID);
            }
        }

        private static Int32 minServerKey()
        {
            //using (CEConn localDB = new CEConn())
            //{
            //    string strSql;
            //    strSql = "select min(serverKey) from Employees";
            //    object o;
            //    o = localDB.getScalerValue(strSql);
            //    if (DBNull.Value == o)
            //        return -1;
            //    else if (Convert.ToInt32(o) < 0)
            //        return (Convert.ToInt32(o) - 1);
            //    else
            //        return -1;
            //}

            DataTable dt = CEConn.dsInMemeoryData.Tables[TableName];

            object value = dt.Compute("Min(ServerKey)", "");

            int serverKey = -1;

            if (value != null)
            {
                serverKey = serverKey - Convert.ToInt32(value);
            }



            //foreach (DataRow dr in dt.Rows)
            //{
            //    if (serverKey >= Convert.ToInt32(dr["ServerKey"]))
            //    {
            //        serverKey = Convert.ToInt32(dr["ServerKey"]) - 1;
            //    }
            //}

            return serverKey;
        }

        //public static void RunQuery(object sqlQueryText)
        //{
        //    try
        //    {
        //        using (CEConn localDB = new CEConn())
        //        {
        //            localDB.runQuery(sqlQueryText.ToString());
        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

    }
}
