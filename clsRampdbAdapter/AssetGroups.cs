extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb; 

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ClsLibBKLogs;
using System.Windows.Forms;

namespace  ClsRampdb
{
    public class AssetGroups
    {
        public static string TableName = "Asset_Groups"; 
        

        public static DataTable getGroups(Int32 ParentID)
        {
            UserPref Pref = UserPref.GetInstance();
            if(Pref.UsingNewDBLib)
                return NewLib.AssetGroups.getGroups(ParentID);
            else
                return OldLib.AssetGroups.getGroups(ParentID);
           
        }

        public static DataTable getGroups(Int32 ParentID,string searchText)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
                return NewLib.AssetGroups.getGroups(ParentID, searchText);
            else
                return OldLib.AssetGroups.getGroups(ParentID, searchText);
        }

        public static DataTable getGroups(Int32 ParentID,ListView lv)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
                return NewLib.AssetGroups.getGroups(ParentID,lv);
            else
                return OldLib.AssetGroups.getGroups(ParentID,lv);

        }

        public static DataTable getGroups(Int32 ParentID, string searchText, ListView lv)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
                return NewLib.AssetGroups.getGroups(ParentID, searchText,lv);
            else
                return OldLib.AssetGroups.getGroups(ParentID, searchText,lv);
        }
    }

    public class AssetStatus
    {
        public static string TableName = "Asset_Status";

        public static DataTable GetAssetStatus()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
                return NewLib.AssetStatus.GetAssetStatus();
            else
                return OldLib.AssetStatus.GetAssetStatus();
        }
    }

}
