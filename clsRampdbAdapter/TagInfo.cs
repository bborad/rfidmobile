extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb; 

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ClsLibBKLogs;

namespace  ClsRampdb
{
    public class TagInfo
    {
        String _TagID;
        Boolean _isLocationTag;
        Boolean _isEmployeeTag;
        Boolean _isDocumentTag;
        Boolean _isAssetTag;
        Int32 _DataKey;
        //Int32 _AssetID;

        public Int32 DataKey
        {
            get
            {
               return _DataKey;
            }
        }

        public TagInfo(String TagID)
        {
            _TagID = TagID;
        }

        public bool isLocationTag()
        {
            if (_isLocationTag)
                return true;

            if (_isEmployeeTag == true)
                return false;

            if (_isAssetTag == true)
                return false;

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.TagInfo t = new NewLib.TagInfo(_TagID);
              
                _isLocationTag = t.isLocationTag();
                _DataKey = t.DataKey;

            }
            else
            {
                OldLib.TagInfo t = new OldLib.TagInfo(_TagID);
              
                _isLocationTag = t.isLocationTag();
                _DataKey = t.DataKey;
            }

            return _isLocationTag;

        }

        public bool isEmployeeTag()
        {
            if (_isLocationTag)
                return false;

            if (_isEmployeeTag == true)
                return _isEmployeeTag;

            if (_isAssetTag == true)
                return false;

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.TagInfo t = new NewLib.TagInfo(_TagID);
               
                _isEmployeeTag = t.isEmployeeTag();
                _DataKey = t.DataKey;

            }
            else
            {
                OldLib.TagInfo t = new OldLib.TagInfo(_TagID);
              
                _isEmployeeTag = t.isEmployeeTag();
                _DataKey = t.DataKey;
            }

            return _isEmployeeTag;
        }

        public bool isDocumentTag()
        {
            if (_isDocumentTag)
                return true; 

            if (_isEmployeeTag)
                return false;

            if (_isAssetTag == true)
                return false; 

            if (_isLocationTag == true)
                return false;


            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.TagInfo t = new NewLib.TagInfo(_TagID);
              
                _isDocumentTag = t.isDocumentTag();
                _DataKey = t.DataKey;

            }
            else
            {
                OldLib.TagInfo t = new OldLib.TagInfo(_TagID);
              
                _isDocumentTag = t.isDocumentTag();
                _DataKey = t.DataKey;
            }

            return _isDocumentTag;
        }

        public bool isAssetTag()
        {
            if (_isLocationTag)
                return false;

            if (_isEmployeeTag == true)
                return false;

            if (_isAssetTag == true)
                return true;

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.TagInfo t = new NewLib.TagInfo(_TagID);
               
                _isAssetTag = t.isAssetTag();
                _DataKey = t.DataKey;
            }
            else
            {
                OldLib.TagInfo t = new OldLib.TagInfo(_TagID);
               
                _isAssetTag = t.isAssetTag();
                _DataKey = t.DataKey;
            }

            return _isAssetTag;
        }

    }
}
