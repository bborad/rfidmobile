﻿using System;

using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

using System.Diagnostics;

namespace RFIDInstallerLauncher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            
                path = path.Substring(0, path.LastIndexOf(Path.DirectorySeparatorChar));


                path = path + "\\RFIDInstaller\\RFIDInstaller.exe";

            Process p = new Process();
            ProcessStartInfo pinfo = new ProcessStartInfo();
            pinfo.FileName = path;

            p.StartInfo = pinfo;

            p.Start();

           

            //Application.Run(new Form1());
        }
    }
}